#!/bin/bash

set -e
set -u
set -o pipefail

# oargen -n Romance -d outputCluster -t 48 -b --run ./trainNewVersion.sh -t Family -l Romance
# sleep 300
oargen -n Germanic -d outputCluster -t 48 -b --run ./trainNewVersion.sh -t Family -l Germanic
sleep 300
# oargen -n Slavic -d outputCluster -t 48 -b --run ./trainNewVersion.sh -t Family -l Slavic
# sleep 300
