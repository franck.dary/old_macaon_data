############Tous les classifiers utilisent les mêmes dictionnaires########

Pas de feature incrementale :
20 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.42  93.29   96.13  87.17  84.54  0.98  0.99  31110
15 iter, batchsize=256
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.47  93.38   96.17  87.49  84.97  0.99  0.99  31110

Avec feature incrementales que pour tagger :
20 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.45  93.26   96.19  87.25  84.85  0.98  0.99  31110
15 iter, batchsize=256
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.40  93.23   96.19  87.34  84.87  0.99  0.99  31110

TODO : Chaque classifier doit utiliser ses propres dictionnaires.

############TODO ok, dictionnaires propres############

Reference ancienne version
50 iter
tagparser  97.64  94.43   96.43  88.43  85.98  0.99  0.99  31110

Pas de feature incrementale :
15 iter, batchsize=256
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.38  93.48   96.15  87.61  85.09  0.99  0.99  31110
15 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.48  93.62   96.13  88.17  85.83  0.99  0.99  31110
5 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.50  93.63   96.13  87.25  84.72  0.99  0.99  31110
10 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.48  93.62   96.13  88.17  85.83  0.99  0.99  31110
tagparser  97.48  93.53   96.14  88.01  85.48  1.00  0.99  31110
tagparser  97.53  93.69   96.15  87.78  85.27  0.99  0.99  31110
tagparser  97.52  93.45   96.15  87.81  85.34  0.99  0.99  31110
tagparser  97.52  93.48   96.17  87.90  85.42  0.99  0.99  31110

Avec feature incrementales que pour tagger :
7 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.42  93.69   96.15  87.37  84.85  0.99  0.99  31110
3 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.43  93.37   96.14  87.92  85.43  0.99  0.99  31110
10 iter, batchsize=10
tool       pos    morpho  lemma  uas    las    srec  sacc  nbWords
tagparser  97.49  93.53   96.17  87.75  85.36  0.99  0.99  31110

5 iter :

Tagger qui voit le futur, pas de feat incr :
tagparser_incr  97.46  93.48   96.11   87.27   84.73   0.99  0.99  31110
Tagger qui voit le futur, feat incr predites :
tagparser_incr  97.48  93.66   96.16   87.79   85.28   0.99  0.99  31110
Tagger qui voit le futur, feat incr gold :
tagparser_incr  97.53  100.00  100.00  100.00  100.00  0.99  0.98  31110

Tagger qui ne voit pas le futur, pas de feat incr :
tagparser_incr  96.50  92.83   96.09  86.59  83.85  0.99  0.99  31110
Tagger qui ne voit pas le futur, feat incr predites :
tagparser_incr  96.38  92.60   96.04  86.20  83.49  0.99  0.99  31110
Tagger qui ne voit pas le futur, feat incr gold :
tagparser_incr  96.54  100.00  100.00 100.00 100.00 0.99  0.99  31110

10 iter :
Tagger qui voit le futur, pas de feat incr :
tagparser_incr  97.54  93.62   96.13  87.65  85.29  0.99  0.99  31110
Tagger qui voit le futur, feat incr predites :
tagparser_incr  97.49  93.64   96.14  87.83  85.46  0.99  0.99  31110
Tagger qui voit le futur, feat incr gold :
tagparser_incr  97.55  100.00  100.00 100.00 100.00 0.99  0.99  31110

Ref avec EOS et ROOT, 10 iter, batch 256
tagparser  97.45  93.51   96.16  87.78  85.36  0.99  0.99  31110
En changeant la condition de fin, 5 iter, batch 256
tagparser  97.45  93.62   96.12  87.55  85.09  0.99  0.99  31110
En changeant la condition de fin, 10 iter, batch 256
tagparser  97.42  93.51   96.11  87.69  85.21  0.99  0.99  31110
Sans EOS, 10 iter, batch 256
tagparser  97.44  93.51   96.12  87.69  85.13  0.99  1.00  31110

Tout a 10 iter, batch 10 :
tagger      97.50   0.00    0.00    3.79   0.00   0.00  0.00  31110
morpho      100.00  95.61   0.00    3.79   0.00   0.00  0.00  31110
lemmatizer  100.00  100.00  85.08   3.79   0.00   0.00  0.00  31110
parser      100.00  100.00  100.00  89.06  86.90  1.00  0.99  31110
tagparser   97.50   93.59   96.18   88.16  85.54  0.99  0.99  31110

Avec futur, sans methode incr pour tagger, 10 iter, batch 10 :
tagparser  97.45  93.43   96.15  88.21  85.73  0.99  0.99  31110
Avec futur, avec methode incr hyp pour tagger, 10 iter, batch 10 :
tagparser  97.50  93.59   96.18  88.16  85.54  0.99  0.99  31110
Avec futur, avec methode incr ref pour tagger, 10 iter, batch 10 :
tagparser  97.55  100.00  100.00 100.00 100.00 1.00  1.00  31110

Tout a 10 iter, batch 10 :
tool        pos     morpho  lemma   uas    las    srec  sacc  nbWords
tagger      97.50   0.00    0.00    3.79   0.00   0.00  0.00  31110
morpho      100.00  95.61   0.00    3.79   0.00   0.00  0.00  31110
lemmatizer  100.00  100.00  96.53   3.79   0.00   0.00  0.00  31110
parser      100.00  100.00  100.00  89.06  86.90  1.00  0.99  31110
tagparser   97.50   93.59   96.18   88.16  85.54  0.99  0.99  31110
t+m+l+p     97.50   93.66   96.16   87.67  85.14  1.00  0.99  31110

Tout a 20 iter, batch 256 :
tool        pos     morpho  lemma   uas    las    srec  sacc  nbWords
tagger      97.34   0.00    0.00    3.79   0.00   0.00  0.00  31110
morpho      100.00  95.41   0.00    3.79   0.00   0.00  0.00  31110
lemmatizer  100.00  100.00  96.53   3.79   0.00   0.00  0.00  31110
parser      100.00  100.00  100.00  89.54  87.39  1.00  0.99  31110
tagparser   97.45   93.52   96.12   87.67  85.14  0.99  1.00  31110
t+m+l+p     97.34   93.37   96.14   87.90  85.31  0.99  0.99  31110

Tests backtrack tagger : 

10 iter, batch 10, avec futur
tool       pos    morpho  lemma  uas   las   srec  sacc  nbWords
tagger     97.39  0.00    0.00   3.79  0.00  0.00  0.00  31110
tagger_bt  97.52  0.00    0.00   3.79  0.00  0.00  0.00  31110
idem, mais sans doublons dans le training set
tool       pos    morpho  lemma  uas   las   srec  sacc  nbWords
tagger     97.55  0.00    0.00   3.79  0.00  0.00  0.00  31110
tagger_bt  97.54  0.00    0.00   3.79  0.00  0.00  0.00  31110

10 iter, batch 10, sans futur, avec doublons
tool       pos    morpho  lemma  uas   las   srec  sacc  nbWords
tagger     96.45  0.00    0.00   3.79  0.00  0.00  0.00  31110
tagger_bt  97.30  0.00    0.00   3.79  0.00  0.00  0.00  31110
10 iter, batch 10, sans futur, sans doublons
tool       pos    morpho  lemma  uas   las   srec  sacc  nbWords
tagger     96.44  0.00    0.00   3.79  0.00  0.00  0.00  31110
tagger_bt  97.37  0.00    0.00   3.79  0.00  0.00  0.00  31110

dynamic parser avec mauvais EOS :
parser  100.00  100.00  100.00  87.19  82.04  0.00  0.00  31110
dynamic parser avec meilleur EOS :
2iter no shuffle
parser  100.00  100.00  100.00  88.36  86.18  1.00  0.99  31110
5iter no shuffle
parser  100.00  100.00  100.00  88.44  86.35  1.00  0.99  31110
5iter avec shuffle
parser  100.00  100.00  100.00  89.57  87.30  1.00  0.99  31110
10iter avec shuffle
parser  100.00  100.00  100.00  89.71  87.69  1.00  0.99  31110

2iter avec shuffle et avec oracle explore
parser  100.00  100.00  100.00  89.28  86.91  1.00  0.99  31110
3iter avec shuffle et avec oracle explore
parser  100.00  100.00  100.00  89.54  87.35  1.00  0.99  31110
5iter avec shuffle et avec oracle explore
parser  100.00  100.00  100.00  89.67  87.57  1.00  0.99  31110
9iter avec shuffle et avec oracle explore
parser  100.00  100.00  100.00  89.93  87.79  1.00  0.99  31110
10iter avec shuffle et avec oracle explore

Idem mais avec parametres comme dans l'article :
10iter :
parser     100.00  100.00  100.00  89.94  87.86  1.00  0.99  31110
30 iter petite learning rate :
parser     100.00  100.00  100.00  90.12  88.05  1.00  0.99  31110
sans feature incr pour tagger
9 iter :
tagparser  97.51   93.72   96.13   88.13  85.71  0.99  0.99  31110
avec feature incr pour tagger
5 iter :
tagparser  97.53   93.69   96.19   88.27  85.68  1.00  0.99  31110
10 iter :
tagparser  97.53   93.69   96.19   88.27  85.68  1.00  0.99  31110

Sans futur :
tagparser_noincr  96.53  90.53   96.07  84.66  82.07  0.99  0.99  31110
tagparser_incr    96.44  90.46   96.05  84.95  82.43  0.99  0.99  31110

Sans futur (dist) :
tagparser_noincr     96.53  90.48   96.06  84.42   82.02   0.99  0.99  31110
tagparser_incr       96.50  90.49   96.01  84.60   82.30   0.99  0.98  31110
tagparser_incr_gold  96.80  90.79   96.14  100.00  100.00  1.00  1.00  31110

10 iter LR normale mais avec feature dist sur b0 s0 :
AVANT DECOUPAGE PARSER
parser  100.00  100.00  100.00  90.22  88.10  1.00  0.99  31110
APRES DECOUPAGE PARSER
TOUT
parser  100.00  100.00  100.00  90.22  88.10  1.00  0.99  31110
UNLABELED

TOUT DECOUPE
15 iter
unlabeled->unlabeled sauf pour shift right
parser2  100.00  100.00  100.00  90.06  87.12  1.00  0.99  31110
labeled->unlabeled

TOUT 15 ITER :
tagger      97.51   0.00    0.00    3.79   0.00   0.00  0.00  31110
morpho      100.00  95.63   0.00    3.79   0.00   0.00  0.00  31110
lemmatizer  100.00  100.00  96.53   3.79   0.00   0.00  0.00  31110
parser      100.00  100.00  100.00  90.27  88.26  1.00  0.99  31110
tagparser   97.48   93.62   96.13   88.11  85.74  0.99  1.00  31110
t+m+l+p     97.51   93.67   96.13   88.68  86.19  0.99  0.99  31110

15 iter vieille version :
parser   100.00  100.00  100.00  90.33  88.26  1.00  0.99  31110

####################################################################################

tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
tagparser_noincr     96.06%     89.99%     96.12%     82.89%     80.13%     99.92%     
tagparser_incrgold   96.57%[±0.13%]  90.35%[±0.12%]  96.17%[±0.02%]  
tagparser_incr       96.29%[±0.13%]  90.05%[±0.08%]  96.11%[±0.01%]  84.30%[±0.13%]  81.72%[±0.20%]  99.93%[±0.01%]  

tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
tagparser_classic    96.55%     90.33%     96.68%     84.51%     81.66%     99.94%     
tagparser_normal     96.27%     90.20%     96.67%     83.84%     80.98%     99.95%     
tagparser_corr       96.59%     92.99%     96.72%     84.24%     81.32%     99.95%     
tagparser_corr_syst  96.98%     93.38%     96.74%     86.21%     83.48%     99.94%     
tagparser_corr_systm 97.20%     93.33%     96.81%     85.35%     82.48%     99.95%     
tagparser_noincr     97.50%     93.73%     96.75%     87.81%     85.26%     99.97%     
tagparser_incr       97.37%     93.42%     96.80%     88.18%     85.51%     99.97%     

tool                            POS     MORPHO  LEMMA   GOV     LABEL|GOV  EOS
tagparser_nofuture_masked       96.41%  90.18%  96.71%  83.36%  80.30%     99.95%
tagparser_nofuture_incr_masked  96.58%  90.51%  96.77%  82.90%  80.03%     99.93%
tagparser_normal_masked         97.53%  93.62%  96.81%  86.23%  83.52%     99.97%
tagparser_incr_masked           97.60%  93.58%  96.83%  88.02%  85.26%     99.95%

tool                            POS     MORPHO  LEMMA   GOV     LABEL|GOV  EOS
tagparser_nofuture_masked       96.41%  90.18%  96.71%  82.79%  79.72%     99.95%
tagparser_nofuture_incr_masked  96.28%  89.94%  96.74%  82.45%  79.61%     99.95%
tagparser_normal_masked         97.53%  93.46%  96.81%  86.24%  83.52%     99.97%
tagparser_incr_masked           97.40%  93.60%  96.88%  87.41%  84.66%     99.95%
tool                            POS     MORPHO  LEMMA   GOV     LABEL|GOV  EOS
tagparser_nofuture_masked       96.33%  90.07%  96.68%  82.32%  79.18%     99.96%
tagparser_nofuture_incr_masked  96.56%  90.21%  96.76%  82.82%  79.95%     99.95%
tagparser_masked                97.42%  93.61%  96.81%  86.28%  83.33%     99.96%
tagparser_incr_masked           97.46%  93.42%  96.87%  86.67%  83.74%     99.97%

BEAM 1
tagparser_classic    96.55%     90.33%     96.68%     84.51%     81.66%     99.94%     
BEAM 2
tagparser_classic    96.54%     90.61%     96.61%     82.48%     79.66%     99.73%     
BEAM 10
tagparser_classic    96.33%     90.25%     96.59%     77.36%     74.54%     98.16%     

BEAM 1 normal
tagparser_classic    96.55%     90.33%     96.68%     84.51%     81.66%     99.94%     
BEAM 1 softmax 
tagparser_classic    96.55%     90.33%     96.68%     84.51%     81.66%     99.94%     
BEAM 2 softmax 
tagparser_classic    96.54%     90.36%     96.59%     84.81%     81.96%     99.93%     
BEAM 5 softmax 
tagparser_classic    96.55%     90.31%     96.62%     84.67%     81.94%     99.92%
BEAM 100 softmax
tagparser_classic    96.54%     90.30%     96.64%     84.07%     81.27%     99.72%

Sans voir la forme :
tool                         POS     MORPHO  LEMMA   GOV     LABEL|GOV  EOS
tagparser_classic            96.55%  90.33%  96.68%  84.51%  81.66%     99.94%
tagparser_corr_systematic_1  97.37%  93.63%  96.80%  88.40%  85.70%     99.96%
tagparser_corr_systematic_2  97.45%  93.72%  96.86%  89.01%  86.41%     99.96%
tagparser_corr_systematic_3  97.51%  93.89%  96.84%  88.96%  86.40%     99.96%

En voyant la forme :

pas bcp d'iterations :
tool                           POS     MORPHO  LEMMA   GOV     LABEL|GOV  EOS
tagparser_classic_2_f          97.43%  93.62%  96.84%  88.90%  86.24%     99.96%
tagparser_corr_systematic_1_f  97.48%  93.63%  96.78%  88.05%  85.37%     99.97%
tagparser_corr_systematic_2_f  97.25%  92.67%  96.79%  86.89%  83.92%     99.95%

tagparser_classic   97.53%     93.59%     96.83%     87.94%     85.26%     99.96%

tagparser           97.09%     91.67%     96.71%     86.54%     83.75%     99.96%     
tagparser           97.34%     93.02%     96.75%     88.37%     85.81%     99.97%     
tagparser           97.40%     93.42%     96.76%     87.76%     85.01%     99.96%     

Apres fix lemma (fplm train et test) :
1 iter
tagparser           97.03%     91.60%     96.84%     86.35%     83.54%     99.96%     
2 iter
tagparser           97.36%     93.01%     96.89%     88.13%     85.63%     99.97%     

Apres fix lemma (sans fplm) :
1 iter
tagparser           97.09%     91.70%     96.23%     86.40%     83.60%     99.96%     
2 iter
tagparser           97.38%     93.06%     96.64%     88.02%     85.47%     99.97%     
3 iter
tagparser           97.36%     93.52%     96.82%     87.56%     84.74%     99.97%     
4 iter
tagparser           97.36%     93.51%     96.86%     87.54%     84.74%     99.97%     
5 iter
tagparser           97.54%     93.60%     96.89%     88.44%     85.75%     99.97%     

# LEMMATIZATION

Train et test avec fplm :
20 iter
lemmatizer           100.00%    100.00%    97.37%     

Train et test avec exceptions :
20 iter
lemmatizer           100.00%    100.00%    97.69%     
Plus gros MLP :
20 iter
lemmatizer           100.00%    100.00%    97.71%     
PLus de features :
lemmatizer           100.00%    100.00%    97.64%     
Form :
lemmatizer           100.00%    100.00%    97.79%     
Form Fasttext :
lemmatizer           100.00%    100.00%    97.72%     
Form + form b-1 :
lemmatizer           100.00%    100.00%    97.64%     
Form sans suffix :
lemmatizer           100.00%    100.00%    97.27%     
Form + 3 suffix :
lemmatizer           100.00%    100.00%    97.57%     
threshold 10 :
04 iter
lemmatizer           100.00%    100.00%    97.63%     
20 iter
lemmatizer           100.00%    100.00%    97.69%     
Plus gros dicts :
2 iter
lemmatizer           100.00%    100.00%    97.60%     
3 iter
lemmatizer           100.00%    100.00%    97.65%     
6 iter
lemmatizer           100.00%    100.00%    97.70%     
Plus gros MLP :
2 iter
lemmatizer           100.00%    100.00%    97.67%     
3 iter
lemmatizer           100.00%    100.00%    97.64%     
4 iter
lemmatizer           100.00%    100.00%    97.72%     
MLP de 500 avec 0.5 de dropout :
4 iter
lemmatizer           100.00%    100.00%    97.64%     
6 iter
lemmatizer           100.00%    100.00%    97.68%     
Base :
lemmatizer           100.00%    100.00%    79.15%     

Nouvelle version :
1iter
lemmatizer           100.00%    100.00%    98.73%     
2 iter
lemmatizer           100.00%    100.00%    98.88%     
5 iter
lemmatizer           100.00%    100.00%    98.93%     

# MORPHO
baseline :
5 iter 
morpho               100.00%    95.66%     
plus gros MLP (500) :
2 iter
morpho               100.00%    95.37%     
4 iter
morpho               100.00%    95.49%     
plus de suffix  :
1 iter
morpho               100.00%    94.56%     
2 iter
morpho               100.00%    95.36%     
5 iter
morpho               100.00%    95.85%     
6 iter
morpho               100.00%    95.82%     

tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
morpho               100.00%    95.47%     
morpho_2             100.00%    95.82%     
morpho_3             100.00%    95.79%     
morpho_4             100.00%    95.88%     

fusion :
morpho               100.00%    96.15%     

tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
morpho               100.00%    96.08%     
morpho_2             100.00%    96.15%     
morpho_3             100.00%    96.14%     

fusion :
morpho               100.00%    96.19%     

Plus gros MLP :
morpho               100.00%    96.02%     

# TAGGER 
tagger               97.50%     

# TAGPARSER

2 iter
tagparser            97.25%     93.31%     96.83%     88.19%     85.72%     99.96%     
3 iter
tagparser            97.43%     93.91%     96.90%     88.22%     85.81%     99.96%     
4 iter
tagparser            97.51%     94.07%     96.90%     88.39%     85.95%     99.97%     
5 iter
tagparser            97.51%     94.14%     96.90%     87.70%     84.94%     99.97%     
6 iter
tagparser            97.51%     94.14%     96.90%     88.24%     85.53%     99.97%     

Sans oracle dynamique :
1 iter
tagparser            96.87%     91.84%     96.29%     86.47%     83.77%     99.96%     
2 iter
tagparser            97.25%     93.31%     96.83%     88.19%     85.72%     99.96%     
3 iter
tagparser            97.43%     93.89%     96.91%     88.12%     85.63%     99.96%     
5 iter
tagparser            97.41%     93.91%     96.93%     88.05%     85.50%     99.97%     
8 iter
tagparser            97.43%     94.06%     96.91%     88.08%     85.52%     99.98%     
10 iter
tagparser            97.43%     94.05%     96.91%     88.14%     85.58%     99.98%     
11 iter
tagparser            97.43%     94.05%     96.92%     88.14%     85.59%     99.98%     

# BACKTRACK

tagparser                   97.51%     94.16%     97.02%     88.18%     85.55%     99.97%
tagparser_corr_systematic_1 97.38%     93.87%     97.08%     87.81%     85.11%     99.97%

tagparser                   97.51%     94.16%     97.02%     88.18%     85.55%     99.97%
tagparser_corr_systematic_1 97.49%     94.06%     97.05%     88.07%     85.53%     99.98%
tagparser_corr_systematic_1f97.48%     93.63%     96.78%     88.05%     85.37%     99.97%

En voyant la forme :

tool                           POS     MORPHO  LEMMA   GOV     LABEL|GOV  EOS
tagparser_classic_2_f          97.43%  93.62%  96.84%  88.90%  86.24%     99.96%
tagparser_classic_2_f_d        97.49%  93.71%  96.83%  88.67%  86.01%     99.96%     
tagparser_corr_systematic_1_f  97.48%  93.81%  96.77%  88.19%  85.68%     99.97%
tagparser_corr_systematic_2_f  97.61%  93.88%  96.86%  89.22%  86.69%     99.97%
tagparser_corr_systematic_3_f  97.57%  93.86%  96.84%  88.49%  85.89%     99.97%     

parser_classic       100.00%    100.00%    100.00%    90.24%     87.92%     99.98%     
parser_fasttext_wiki 100.00%    100.00%    100.00%    90.55%     88.11%     99.98%     

parser_fasttext      100.00%    100.00%    100.00%    89.80%     87.59%     99.98%     
parser_fasttext      100.00%    100.00%    100.00%    90.19%     87.92%     99.98%     

tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
tagparser_classic    97.47%     93.77%     96.82%     89.05%     86.48%     99.95%     
tagparser_fasttext   97.48%     94.07%     96.77%     89.24%     86.63%     99.96%     
tagparser_fasttext_s 97.54%     94.07%     96.80%     89.34%     86.81%     99.96%     

tool                          POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
tagparser_classic_nofuture    96.86%     91.26%     97.97%     84.40%     81.85%     99.96%     
tagparser_corr_systematic_2   97.94%     94.71%     98.24%     87.76%     85.21%     99.97%     
tagparser_classic             97.91%     94.38%     98.20%     88.67%     86.08%     99.96%     
tagparser_incr                97.87%     94.40%     98.21%     88.88%     86.27%     99.97%     
tagparser_corr_systematic_1f  97.77%     94.36%     98.21%     88.97%     86.44%     99.97%     
tagparser_corr_systematic_2f  97.90%     94.50%     98.08%     88.59%     86.07%     99.96%     

tagparser_classic             97.91%     94.38%     98.20%     88.67%     86.08%     99.96%     
tagparser_corr_systematic_2_f 97.83%     94.41%     98.11%     89.59%     87.01%     99.97%

# Parser delexicalise

Ref parser :
parser_normal        100.00%    100.00%    100.00%    90.53%     88.24%     99.98%     

1 iter
parser_delex         100.00%    100.00%    100.00%    87.32%     82.29%     99.98%     
2 iter
parser_delex         100.00%    100.00%    100.00%    88.41%     83.58%     99.98%     
3 iter
parser_delex         100.00%    100.00%    100.00%    87.99%     83.03%     99.98%     
4 - nosave
5 iter
parser_delex         100.00%    100.00%    100.00%    88.42%     83.52%     99.98%     
8 iter
parser_delex         100.00%    100.00%    100.00%    88.80%     83.83%     99.98%     
11 iter :
parser_delex         100.00%    100.00%    100.00%    89.28%     84.54%     99.98%     

# Retour arriere predit

parser_test          100.00%    100.00%    100.00%    90.64%     88.39%     99.98%     
parser_other         100.00%    100.00%    100.00%    90.39%     88.15%     99.98%     

parser_new           100.00%    100.00%    100.00%    90.67%     88.43%     99.99%     

# Nouvelles features parser
Avec LEMMA et pas EOS
5 iter :
parser_new           100.00%    100.00%    100.00%    90.74%     88.43%     99.98%     
Avec FORM fasttext et EOS et dicts séparés ou pas
2 iter :
parser_form          100.00%    100.00%    100.00%    90.74%     88.44%     99.98%     
parser_form_dicts    100.00%    100.00%    100.00%    90.67%     88.34%     99.98%     
4 iter : 
parser_form          100.00%    100.00%    100.00%    90.89%     88.63%     99.98%     
parser_form_dicts    100.00%    100.00%    100.00%    91.03%     88.73%     99.98%     

# LEMMA ou FORM ?
tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
3 iter :
parser_form          100.00%    100.00%    100.00%    90.77%     88.47%     99.98%     
parser_lemma         100.00%    100.00%    100.00%    90.82%     88.49%     99.98%     
6 iter :
parser_form          100.00%    100.00%    100.00%    90.90%     88.63%     99.98%     
parser_lemma         100.00%    100.00%    100.00%    90.82%     88.49%     99.98%     


parser_classic         100.00%    100.00%    100.00%    90.95%   88.60%   99.98% 
parser_classic_feats3  100.00%    100.00%    100.00%    90.73%   88.48%   99.98% 
parser_corr_sys_2      100.00%    100.00%    100.00%    91.04%   88.70%   99.98% 
parser_classic_feats4  100.00%    100.00%    100.00%    91.11%   88.89%   99.98% 
parser_classic_feats2  100.00%    100.00%    100.00%    91.38%   89.18%   99.98% 
parser_classic_feats1  100.00%    100.00%    100.00%    91.50%   89.27%   99.98% 

parser_classic         100.00%    100.00%    100.00%    90.60%   88.31%   99.98% 
parser_classic_feats1  100.00%    100.00%    100.00%    91.25%   88.85%   99.98% 
parser_classic_feats2  100.00%    100.00%    100.00%    91.26%   88.89%   99.98% 
parser_corr_sys_2      100.00%    100.00%    100.00%    91.66%   89.53%   99.98% 

tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
parser_classic       100.00%    100.00%    100.00%    91.33%     89.09%     99.98%     
parser_corr_sys_2    100.00%    100.00%    100.00%    91.48%     89.41%     99.98%     
parser_corr_pred     100.00%    100.00%    100.00%    91.52%     89.34%     99.98%     

ref d'avant :
tagparser_classic    97.91%     94.38%     98.20%     88.67%     86.08%     99.96%     
3 iter :
tool                 POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
tagparser_classic    97.97%     94.39%     98.15%     89.47%     86.84%     99.97%     


tool                  POS        MORPHO     LEMMA      GOV        LABEL|GOV  EOS        
tagparser_classic     97.85%     94.33%     98.05%     89.61%     87.13%     99.97%     
tagparser_incremental 97.94%     94.45%     98.20%     89.74%     87.05%     99.96%     

tagparser_corr_sys_2  97.92%     94.48%     98.10%     89.62%     87.19%     99.98%     
3 iter :
tagparser_corr_pred   97.97%     94.43%     97.86%     73.27%     70.20%     99.97%     
