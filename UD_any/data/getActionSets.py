#! /usr/bin/python3

import sys

def printUsageAndExit() :
  print("USAGE : %s mcd column_1.txt columns_2.txt..."%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

if __name__ == "__main__" :

  sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

  if len(sys.argv) < 3 :
    printUsageAndExit()

  conllMCD = readMCD(sys.argv[1])
  conllMCDr = {v: k for k, v in conllMCD.items()} 

  for colFile in sys.argv[2:] :
    numCol = int(colFile.split('.')[0].split('_')[-1]) - 1
    if not str(numCol) in conllMCD :
      continue
    nameCol = conllMCD[str(numCol)]

    if nameCol == "POS" :
      output = open("tagger.as", 'w', encoding='utf-8')
      for line in open(colFile, "r", encoding='utf-8') :
        striped = line.strip()
        if len(striped) == 0 :
          continue
        print("WRITE b.0 POS " + striped, file=output)
      output.close()

    elif nameCol == "XPOS" :
      output = open("taggerx.as", 'w', encoding='utf-8')
      for line in open(colFile, "r", encoding='utf-8') :
        striped = line.strip()
        if len(striped) == 0 :
          continue
        print("WRITE b.0 XPOS " + striped, file=output)
      output.close()

    elif nameCol == "MORPHO" :
      output = open("morpho_whole.as", 'w', encoding='utf-8')
      for line in open(colFile, "r", encoding='utf-8') :
        striped = line.strip()
        if len(striped) == 0 :
          continue
        print("WRITE b.0 MORPHO " + striped, file=output)
      output.close()
      output = open("morpho_parts.as", 'w', encoding='utf-8')
      allParts = set()
      allPartsList = []
      for line in open(colFile, "r", encoding='utf-8') :
        striped = line.strip()
        if len(striped) == 0 :
          continue
        parts = striped.split('|')
        for part in parts :
          allParts.add(part)
      for part in allParts :
        allPartsList.append(part)
      allPartsList.sort()
      for part in allPartsList :
        print("ADD b.0 MORPHO " + part, file=output)
      print("Default : NOTHING", file=output)
      output.close()

    elif nameCol == "LABEL" :
      output = open("parser_legacy.as", 'w', encoding='utf-8')
      print("REDUCE", file=output)
      labels = set()
      labelsList = []
      for line in open(colFile, "r", encoding='utf-8') :
        striped = line.strip()
        if len(striped) == 0 or striped == "root" or striped == "_" :
          continue
        label = striped.split(':')[0]
        if label not in labels :
          labels.add(striped)
          labelsList.append(striped)
      labelsList.sort()
      for label in labelsList :
        print("LEFT " + label, file=output)
        print("RIGHT " + label, file=output)
      print("EOS s.0", file=output)
      print("Default : SHIFT", file=output)
      output.close()
      output = open("parser.as", 'w', encoding='utf-8')
      print("REDUCE", file=output)
      labels = set()
      labelsList = []
      for line in open(colFile, "r", encoding='utf-8') :
        striped = line.strip()
        if len(striped) == 0 or striped == "root" or striped == "_" :
          continue
        label = striped.split(':')[0]
        if label not in labels :
          labels.add(striped)
          labelsList.append(striped)
      labelsList.sort()
      for label in labelsList :
        print("LEFT " + label, file=output)
        print("RIGHT " + label, file=output)
      print("Default : SHIFT", file=output)
      output.close()

