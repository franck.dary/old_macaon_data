Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
strategy strategy.cla
parser parser.cla
segmenter segmenter.cla
%STATES
strategy strategy
parser parser
segmenter segmenter
%TRANSITIONS
strategy parser MOVE parser
strategy segmenter MOVE segmenter
parser strategy *
segmenter strategy *
