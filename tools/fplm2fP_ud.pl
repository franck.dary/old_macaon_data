#!/usr/bin/perl


$postag{"ADJ"} = 1;
$postag{"ADP"} = 1;
$postag{"ADV"} = 1;
$postag{"AUX"} = 1;
$postag{"CCONJ"} = 1;
$postag{"DET"} = 1;
$postag{"INTJ"} = 1;
$postag{"NOUN"} = 1;
$postag{"NUM"} = 1;
$postag{"PART"} = 1;
$postag{"PRON"} = 1;
$postag{"PROPN"} = 1;
$postag{"PUNCT"} = 1;
$postag{"SCONJ"} = 1;
$postag{"SYM"} = 1;
$postag{"VERB"} = 1;
$postag{"X"} = 1;

while(<>){
    ($form, $pos, $lemma, $morpho) = split /\t/;
    if($postag{$pos}){
	$h_form2pos{$form}{$pos} = 1;
	$h_pos{$pos} += 1;
    }
}

$nbelem = keys %h_form2pos;
print "$nbelem\n";

$nbelem = keys %h_pos;
print "$nbelem\n";
$first = 1;
foreach $pos (keys %h_pos){
    if($first){
	$first = 0;
    }
    else{
	print "\t";
    }
    print $pos;
}
print "\n";

foreach $form (keys %h_form2pos){
    print "$form\t";
    foreach $pos (keys %h_pos){
	if($h_form2pos{$form}{$pos}){
	    print "1";
	}
	else{
	    print "0";
	}
    }
    print "\n";
}
