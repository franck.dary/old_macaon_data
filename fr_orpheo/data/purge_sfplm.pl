#!/usr/bin/perl

my $dir= shift;

my $statut;
my $reste;


my @fichiers =(
"$dir/ADJ.sfplm",
"$dir/ADV.sfplm",
"$dir/CLN.sfplm",
"$dir/CSU.sfplm",
"$dir/INT.sfplm",
"$dir/PCT.sfplm",
"$dir/PRO.sfplm",
"$dir/VNF.sfplm",
"$dir/VPR.sfplm",
"$dir/ADN.sfplm",
"$dir/CLI.sfplm",
"$dir/CLS.sfplm",
"$dir/COO.sfplm",
"$dir/DET.sfplm",
"$dir/NOM.sfplm",
"$dir/PRE.sfplm",
"$dir/PRQ.sfplm",
"$dir/VPP.sfplm",
"$dir/VRB.sfplm"
);


foreach $fichier_sfplm (@fichiers){
    $fichier_fplm = $fichier_sfplm;
#    $fichier_fplm =~ s/sfplm/fplm/;
    print STDERR "processing $fichier_sfplm\n";
#    print "fichier sfplm = $fichier_sfplm\n";
#    print "fichier fplm = $fichier_fplm\n";
#    open(FPLM,">$fichier_fplm");
    open(SFPLM,"<$fichier_sfplm");
    while(<SFPLM>){
	s///g;
	s/ +$//;
	s/\t+$//;
	s/\t +/\t/g;
	s/ +\t/\t/g;
	s/\t+/\t/g;
	/([^\t]*)\t(.*)/;
	$statut = $1;
	$reste = $2;
	if(($statut eq "N") ||($statut eq "A")){
#	    print FPLM "$reste\n";
#	    if((!$reste=~ /\tde\t/)
#	       && (!$reste=~ /\tdu\t/)
#	       && (!$reste=~ /\td\'\t/)
#	       && (!$reste=~ /\tdes\t/)){
	    print "$reste\n";
#	    }
	}
    }
#    close FPLM;
    close SFPLM;
}
