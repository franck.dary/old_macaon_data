#! /usr/bin/python3

import sys
import random

def printUsageAndExit() :
  print("USAGE : %s file.mcf mcf.mcd devRatio train.mcf dev.mcf"%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def main() :
  if len(sys.argv) != 6 :
    printUsageAndExit()

  mcfMCD = readMCD(sys.argv[2])
  mcfMCDr = {v: k for k, v in mcfMCD.items()} 

  if "EOS" not in mcfMCDr :
    print("ERROR : invalid MCD, missing EOS. Aborting.", file=sys.stderr)
    exit(1)

  EOSIndex = int(mcfMCDr["EOS"])

  output = [[]]
  for line in open(sys.argv[1], encoding="utf8") :
    splited = line.strip().split('\t')
    output[-1].append(line.strip())
    if splited[EOSIndex] == "1" :
      output.append([])

  output.pop()
  random.seed(100)
  random.shuffle(output)

  ratio = float(sys.argv[3])

  nbSentencesDev = int(len(output) * ratio)
  trainOutput = open(sys.argv[4], "w", encoding="utf8")
  devOutput = open(sys.argv[5], "w", encoding="utf8")

  for i in range(len(output)) :
    if i < nbSentencesDev :
      for line in output[i] :
        print(line, file=devOutput)
    else :
      for line in output[i] :
        print(line, file=trainOutput)

main()
