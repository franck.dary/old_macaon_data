#!/usr/bin/python
# -*- coding: utf-8 -*-

# python distanceVectMaj.py ../data/wals/matrix_valueReplace_deleteLangUseless 

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import collections
from collections import Counter

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"

def averageVector(vect):
    newVect = vect
    for i in range(len(vect)):
        c = Counter(vect[i])
        majElt = c.most_common()[0][0]
        newVect[i] = majElt
    print(newVect)
    return newVect
        
    
def distanceBetween2Vect(vect1, vect2):
    if(len(vect1) != len(vect2)):
        sys.stderr.write("Error length vectors\n")
    cptDiff = 0
    for i in range(len(vect1)):
        if(vect1[i] != vect2[i]):
            cptDiff+=1
    return cptDiff
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 2):
        sys.stderr("Error, give the matrix of vectors from the wals")
        exit(1)
    filePathMatrixWals = sys.argv[1]
    keyAVG = 'AVGvect'
    vectOfLang = dict()
    flagFirstLang = True
    with open(filePathMatrixWals, "r") as matrix:
        curLine = matrix.readline().strip()
        curLine = matrix.readline().strip()
        while(not fileCompletelyRead(curLine)):
            curLang = curLine.split(",")[0]
            curVect = curLine.split(",")
            curVect.pop(0)                            
            vectOfLang[curLang] = curVect
            if(flagFirstLang):
                vectOfLang[keyAVG] = curVect[:]
                for i in range(len(curVect)):
                    vectOfLang[keyAVG][i] = [curVect[i]]
                flagFirstLang = False
            else:   
                for i in range(len(curVect)):
                    vectOfLang[keyAVG][i].append(curVect[i])

            curLine = matrix.readline().strip()
    vectOfLang = collections.OrderedDict(sorted(vectOfLang.items()))
    vectOfLang[keyAVG] = averageVector(vectOfLang[keyAVG])
    for lang in vectOfLang:
        stringToPrint = lang + "\t" 
        dist = distanceBetween2Vect(vectOfLang[lang], vectOfLang[keyAVG])
        stringToPrint += str(dist) + "\t" + str(vectOfLang[lang])
        print(stringToPrint)
