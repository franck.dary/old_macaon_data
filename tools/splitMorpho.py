#!/usr/bin/python
# -*- coding: utf-8 -*-

# ../../../tools/splitMorpho.py tmp2 3 listKeyMorphoToKeep.txt 

from __future__ import print_function          
import fileinput
import sys
import re
import os

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def main():
    
    if(len(sys.argv) != 4):
        print("Error, give the file to mofified, the number of the column containing moprhology (start to count at 0), and then give a file with the list of morphology features to keep.")
        exit(1)
    
    colMorpho = int(sys.argv[2])
    listMorpho = []
    with open(sys.argv[3], "r") as fileListMorpho:
        for curLine in fileListMorpho:
            if(lineIsAComment(curLine)):
                continue
            listMorpho.append(curLine.rstrip())
    
    with open(sys.argv[1], "r") as conllu:
        for curLine in conllu:
            curLine = curLine.rstrip().split("\t")
            morphoOrig = curLine[colMorpho].split("|")
            newMorpho = ["_"] * len(listMorpho)
            
            for morpho in morphoOrig:
                keyMorpho = morpho.split("=")[0]
                if(keyMorpho in listMorpho):
                    indexCurKey = listMorpho.index(keyMorpho)
                    newMorpho[indexCurKey] = morpho
            
            curLine[colMorpho] = "\t".join(newMorpho)
            print("\t".join(curLine))
        


main()
        
