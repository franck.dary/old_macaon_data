#! /usr/bin/python3

import sys

sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

def getLineAsList(inputFile) :
  line = inputFile.readline()

  if len(line) == 0 :
    return line

  line = line.replace("\n", "")
  line = line.split("\t")

  return line

fplm = open(sys.argv[1], "r", encoding="utf8")

line = []
form2pos = {}
int2form = {}
pos2int = {}
int2pos = {}

while(len(line) != 4) :
  line = getLineAsList(fplm)
  if len(line) == 0 :
    exit(1)

while(len(line) == 4) :
  form = line[0].strip()
  form2 = form.replace(' ', '_')
  pos = line[1]
  if form not in form2pos :
    form2pos[form] = []
    int2form[len(int2form)] = form
  if form2 not in form2pos :
    form2pos[form2] = []
    int2form[len(int2form)] = form2
  if pos not in pos2int :
    pos2int[pos] = len(pos2int)
    int2pos[len(int2pos)] = pos

  form2pos[form].append(pos)
  if form != form2 :
    form2pos[form2].append(pos)

  line = getLineAsList(fplm)

fplm.close()

print(len(form2pos))
print(len(int2pos))

for i in range(len(int2pos)) :
  print(int2pos[i], end="\t")

print("")

for i in range(len(int2form)) :
  form = int2form[i]
  print(form, end='\t')
  for j in range(len(int2pos)) :
    pos = int2pos[j]
    if pos in form2pos[form] :
      print(1, end='')
    else :
      print(0, end='')
  print("")
