#! /bin/bash

LANG=fr_orpheo
MCF=../data/test.mcf
MCD=../data/wpmlgfs.mcd
ARGS="--keepPunct EOS --relative LABEL GOV --ignore FORM --ignore SILENCE --ignore SPKRCHANGE"

exec ../../scripts/eval.py $LANG $MCF $MCD $* $ARGS
