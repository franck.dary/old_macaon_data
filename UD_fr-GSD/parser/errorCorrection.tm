Name : Parser Machine with predicted backtracking
Dicts : parser.dicts
%CLASSIFIERS
strategy strategy.cla
signature signature.cla
parser parser.cla
error_parser error_parser.cla
%STATES
strategy strategy
signature signature
parser parser
error_parser error_parser
%TRANSITIONS
strategy signature MOVE signature
strategy parser MOVE parser
parser error_parser *
error_parser parser BACK
error_parser strategy *
signature strategy *
