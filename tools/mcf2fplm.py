#! /usr/bin/python3

import sys

class mcdFormat :
  form = -1
  pos = -1
  morpho = -1
  lemma = -1
  governor = -1
  label = -1
  eos = -1
  def getTemplate() :
    return (max(mcdFormat.form, mcdFormat.pos,mcdFormat.morpho, mcdFormat.lemma,mcdFormat.governor, mcdFormat.label,mcdFormat.eos)+1)*["_"]

def printUsageAndExit() :
  print("Usage : ",sys.argv[0]," file.mcf columns.mcd")
  exit(1)

def readMCD(filename) :
  for line in open(filename, "r", encoding="utf8") :
    split = line.split()
    if split[1] == "FORM" :
      mcdFormat.form = int(split[0])
    elif split[1] == "POS" :
      mcdFormat.pos = int(split[0])
    elif split[1] == "MORPHO" :
      mcdFormat.morpho = int(split[0])
    elif split[1] == "LEMMA" :
      mcdFormat.lemma = int(split[0])
    elif split[1] == "GOV" :
      mcdFormat.governor = int(split[0])
    elif split[1] == "LABEL" :
      mcdFormat.label = int(split[0])
    elif split[1] == "EOS" :
      mcdFormat.eos = int(split[0])

def generateFplm(mcf) :
  fplm = {}
  for line in open(mcf, "r", encoding="utf8") :
    split = line.split("\t")
    form = split[mcdFormat.form]
    pos = split[mcdFormat.pos]
    lemma = split[mcdFormat.lemma]
    if mcdFormat.morpho == -1 :
      morpho = "_"
    else :
      morpho = split[mcdFormat.morpho]
    fplm[form+pos] = [form, pos, lemma, morpho]

  for entry in fplm :
    print(*fplm[entry],sep="\t")

if len(sys.argv) != 3 :
  printUsageAndExit()

readMCD(sys.argv[2])
generateFplm(sys.argv[1])
