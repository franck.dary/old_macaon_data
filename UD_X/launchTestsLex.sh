#!/bin/bash

set -e
set -u
set -o pipefail

# listlang="ar bg bxr ca cu cs da de el es et eu fa fi fr ga gl got he hi hr hu id it ja kk kmr ko lv nl no nob nno pl pt ro ru sk sl sme sv tr ug uk ur vi zh"
# listLang="fr ga he hi hr hu id it ja kmr ko lv nl no pl pt ro ru sl sme sv tr uk ur vi zh"

listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl nno nob pl pt ro ru sl sme sv tr uk ur vi zh"

# listLang="he hi hr hu id it ja kmr ko lv nl nno nob pl pt ro ru sl sme sv tr uk ur vi zh"


for curLang in $listLang
do
    CUREXPE=L_${curLang}
    oargen -n ${CUREXPE}_lex300align -d outputCluster/LEX -t 24 -b --run ./trainNewVersion.sh -t L -l $curLang -e 300
    sleep 1000
    oargen -n ${CUREXPE} -d outputCluster/LEX -t 24 -b --run ./trainNewVersion.sh -t L -l $curLang
    sleep 1000
    CUREXPE=Lbar_${curLang}
    oargen -n ${CUREXPE} -d outputCluster/LEX -t 96 -b --run ./trainNewVersion.sh -t Lbar -l $curLang
    sleep 1000
    CUREXPE=Lbar_${curLang}_lex300
    oargen -n ${CUREXPE} -d outputCluster/LEX -t 96 -b --run ./trainNewVersion.sh -t Lbar -l $curLang -e 300
    sleep 1000
done


# sleep 300
# oargen -n SIGMA -d outputCluster -t 96 -b --run ./trainNewVersion.sh -t SIGMA 
# sleep 300
# oargen -n SIGMA_W80 -d outputCluster -t 96 -b --run ./trainNewVersion.sh -t SIGMA -w 80
# sleep 300
# oargen -n SIGMA_ID -d outputCluster -t 96 -b --run ./trainNewVersion.sh -t SIGMA -i
# sleep 300
# oargen -H see4c1 -n SIGMA_Wd -d outputCluster -t 96 -b --run ./trainNewVersion.sh -t SIGMA -w d
# sleep 300
# oargen -H see4c1 -n SIGMA_Wdf -d outputCluster -t 96 -b --run ./trainNewVersion.sh -t SIGMA -w df
