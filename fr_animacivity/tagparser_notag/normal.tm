Name : Tagger, Morpho, Lemma and Parser Machine
Dicts : tagparser.dicts
%CLASSIFIERS
tagger tagger.cla
morpho morpho.cla
signature signature.cla
lemma_lookup lemmatizer_lookup.cla
lemma_rules lemmatizer_rules.cla
parser parser.cla
%STATES
signature signature
tagger tagger
morpho morpho
lemma_lookup lemma_lookup
lemma_rules lemma_rules
parser parser
%TRANSITIONS
signature tagger 0 *
tagger morpho 0 *
morpho lemma_lookup 0 *
lemma_lookup parser 0 *
lemma_lookup lemma_rules 0 NOTFOUND
lemma_rules parser 0 *
parser parser 0 LEFT
parser parser 0 EOS
parser parser 0 REDUCE
parser parser 0 ROOT
parser signature +1 SHIFT
parser signature +1 RIGHT
