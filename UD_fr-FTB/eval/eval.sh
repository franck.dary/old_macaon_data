#! /bin/bash

LANG=UD_fr-FTB
MCF=../data/test.mcf
MCD=../data/wpmlgfs.mcd
ARGS="--keepPunct EOS --relative LABEL GOV --ignore FORM"

exec ../../scripts/eval.py $LANG $MCF $MCD $* $ARGS
