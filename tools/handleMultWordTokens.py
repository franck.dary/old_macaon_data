#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 3):
        sys.stderr("Error, give gold and pred file ")
        exit(1)
    goldPath = sys.argv[1]
    predPath = sys.argv[2]
    
    with open(goldPath, "r", encoding="utf8") as gold:
        with open(predPath, "r", encoding="utf8") as pred:
            curLineGold = gold.readline()
            curLinePred = pred.readline()
            while(not fileCompletelyRead(curLinePred) and not fileCompletelyRead(curLineGold)):
                if(curLinePred == "\n"):
                    print()
                    curLinePred = pred.readline()
                    continue
                if(curLineGold == "\n"):
                    curLineGold = gold.readline()
                    continue
                if(lineIsAComment(curLinePred)):
                    print(curLinePred.strip())
                    curLinePred = pred.readline()
                    continue
                if(lineIsAComment(curLineGold)):
                    curLineGold = gold.readline()
                    continue
                    
                
                ID_SENT_GOLD = curLineGold.split("\t")[0]
                
                if("." in ID_SENT_GOLD):
                    curLineGold = gold.readline()
                    continue            

                if(curLineGold.split("\t")[1] != curLinePred.split("\t")[1] and "-" not in ID_SENT_GOLD ):
                    sys.stderr.write("---Error! Files not aligned : %s\n%s\n" %(curLineGold, curLinePred))
                    exit(1)
                    
                if("-" in ID_SENT_GOLD):
                    curLineGold = curLineGold.split("\t")
                    
                    index1 = int(curLineGold[0].split("-")[0])
                    index2 = int(curLineGold[0].split("-")[1])
                    diff = index2 - index1
                    index1 = int(curLinePred.split("\t")[0])
                    index2 = index1 + diff
                    
                    curLineGold[0] = str(index1) + "-" + str(index2)
                    
                    curLineGold = "\t".join(curLineGold)
                    print(curLineGold.strip())
                    print(curLinePred.strip())
                    curLineGold = gold.readline()
                else:
                    print(curLinePred.strip())
                curLineGold = gold.readline()
                curLinePred = pred.readline()
