#! /bin/bash

expName="tagparser_classic"
host="decoda2"
maxtime="10:00:00"
#besteffort="-t besteffort"
besteffort=""
readSize=80
beamSize=100

if [ -z "$host" ]
then
  oarsub $besteffort -n "d_$expName" -E clusterErr.txt -O clusterOut.txt -p "gpu IS NULL" -l walltime=$maxtime "./eval.sh $expName --readSize $readSize --beamSize $beamSize"
else
  oarsub $besteffort -n "d_$expName" -E clusterErr.txt -O clusterOut.txt -p "gpu IS NULL AND host like '$host'" -l walltime=$maxtime "./eval.sh $expName --readSize $readSize --beamSize $beamSize"
fi

