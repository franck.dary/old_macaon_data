#!/usr/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"

def findRoot(root, conllu):
    if(root != -1):
        return root

    maybeRoot = -1
    curLine = conllu.readline() 
   
    while(isInASequence(curLine)):
        curLine = curLine.strip().split("\t")
        ID = curLine[0]
        if('-' in ID):
            curLine = conllu.readline()
            continue
        ID = int(ID)
        LABEL = curLine[7]
        DEP = int(curLine[6])
        if(LABEL == "root"):
            return ID
        elif(DEP == "0"):
            maybeRoot = ID
        curLine = conllu.readline() 

    return maybeRoot

if __name__ == '__main__':
    
    if(len(sys.argv) != 2):
        sys.stderr("Error, give a conllu file")
        exit(1)
    conlluPath = sys.argv[1]
    
    with open(conlluPath, "r", encoding="utf8") as conllu:
        curLine = conllu.readline()
        while(not fileCompletelyRead(curLine)):
            root = -1
            while(isInASequence(curLine)):
                curLine = curLine.strip().split("\t")
                ID = curLine[0]
                if('-' in ID):
                    print("\t".join(curLine))
                    curLine = conllu.readline()
                    continue
                ID = int(ID)
                LABEL = curLine[7]
                DEP = int(curLine[6])
                # sys.stderr.write("%s %s %s\n" %(ID, LABEL, DEP))
                if(LABEL == "root"):
                    if(root == -1):
                        root = ID 
                    elif(root != ID):
                        curLine[6] = str(root)
                        curLine[7] = "_"
                elif(DEP == 0):
                    last_pos = conllu.tell()
                    root = findRoot(root, conllu)
                    if(root == ID or root == -1):
                        curLine[7] = "root"
                    else:
                        curLine[6] = str(root)
                    conllu.seek(last_pos)
                print("\t".join(curLine))
                curLine = conllu.readline()
            print()
            curLine = conllu.readline()
        
