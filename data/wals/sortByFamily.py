#!/usr/bin/python
# -*- coding: utf-8 -*-

# python sortByFamily.py TOTAL.UAS ../../../data/wals/corr_codeUDX_genus > TOTAL.UAS.family 


from __future__ import print_function          
import fileinput
import sys
import re
import os
import random

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def dictFamilyAndLang(filePathFamily):
    family = open(filePathFamily, "r")
    langOfAFamily = dict()
    familyOfALang = dict()
    curLine = family.readline().strip()
    
    while(not fileCompletelyRead(curLine)):
        curLine = curLine.split("\t")
        curLang = curLine[0]
        curFamily = curLine[1]
        curLine = family.readline().strip()
        if(curFamily in langOfAFamily):
            prevList = langOfAFamily[curFamily]
            prevList.append(curLang)
            langOfAFamily[curFamily] = prevList
        else:
            langOfAFamily[curFamily] = [curLang]
        familyOfALang[curLang] = curFamily
        
    return familyOfALang, langOfAFamily
    
def main():
    
    if(len(sys.argv) < 3):
        print("Error, give a file with results, with the code of langue at the end of the line. Then, give a file containing the correspondence betwwen the code and the family.")
        exit(1)
        
    filepathResults = sys.argv[1] 
    filePathFamily = sys.argv[2]
    
    res = open(filepathResults, "r")
    
    familyOfLangDict, langOfAFamily = dictFamilyAndLang(filePathFamily)
    
    curLine = res.readline().rstrip()
    lines = dict()
    
    while(not fileCompletelyRead(curLine)):    
        lines[curLine.split("\t")[-1]] = curLine
        curLine = res.readline().rstrip()
        
    for family in sorted(langOfAFamily.iterkeys()):
        for langOfCurFamily in sorted(langOfAFamily[family]):
            if(langOfCurFamily in lines):
                stringToPrint = lines[langOfCurFamily] + "\t" + family
                print(stringToPrint)
                del lines[langOfCurFamily]
        print()

    for curLang in lines:
        print(lines[curLang])

main()
        
