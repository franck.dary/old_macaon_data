#! /usr/bin/python3

import sys

def printUsageAndExit() :
  print("USAGE : %s file.conllu conllu.mcd output.mcf mcf.mcd"%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def main() :
  if len(sys.argv) != 5 :
    printUsageAndExit()

  conllMCD = readMCD(sys.argv[2])
  mcfMCD = readMCD(sys.argv[4])
  conllMCDr = {v: k for k, v in conllMCD.items()} 
  mcfMCDr = {v: k for k, v in mcfMCD.items()} 

  output = []
  previousId = -1

  currentSentence = ""
  for line in open(sys.argv[1], encoding="utf8") :
    clean = line.strip()
    if len(clean) < 2 :
      continue
    if line.split('=')[0] == "# sent_id " :
      continue
    if line.split('=')[0] == "# text " :
      currentSentence = line[8:].strip()
      continue

    columns = clean.split('\t')

    if len(columns[int(conllMCDr["ID"])].split('-')) > 1 :
      lineInMCF = []
      for index in mcfMCD :
        colName = mcfMCD[index]
        while  len(lineInMCF) < int(index)+1 :
          lineInMCF.append("")
        value = "_"
        if colName == "EOS" :
          if int(columns[int(conllMCDr["ID"])].split('-')[0]) < previousId :
            value = "1"
            previousId = int(columns[int(conllMCDr["ID"])].split('-')[0])
        if mcfMCD[index] in conllMCDr :
          indexInColumns = int(conllMCDr[mcfMCD[index]])
          value = columns[indexInColumns]
        lineInMCF[int(index)] = value;
      output.append(lineInMCF)
      continue

    id = int(columns[int(conllMCDr["ID"])])
    gov = int(columns[int(conllMCDr["GOV"])])
    relGov = gov - id
    if gov == 0 :
      relGov = 0
    eos = "_"
    textValue = "_"
    if id < previousId :
      eos = "1"

    previousId = id
    textValue = currentSentence

    lineInMCF = []
    for index in mcfMCD :
      colName = mcfMCD[index]
      value = ""
      if colName == "EOS" :
        value = eos
      elif colName == "GOV" :
        value = relGov
      elif colName == "TEXT" : 
        value = textValue
      else :
        indexInColumns = int(conllMCDr[mcfMCD[index]])
        value = columns[indexInColumns]
        
      while  len(lineInMCF) < int(index)+1 :
        lineInMCF.append("")
      lineInMCF[int(index)] = value
    output.append(lineInMCF)

  hasText = False
  textIndex = 0
  EOSIndex = int(mcfMCDr["EOS"])

  if "TEXT" in mcfMCDr :
    hasText = True
    textIndex = int(mcfMCDr["TEXT"])

  for i in range(len(output)-1) :
    output[i][EOSIndex] = output[i+1][EOSIndex]

  output[-1][EOSIndex] = "1"

  if hasText :
    for i in range(len(output)) :
      if output[i][EOSIndex] != "1" :
        output[i][textIndex] = "_"

  outputFile = open(sys.argv[3], "w", encoding="utf8")
  for outputLine in output :
    for i in range(len(outputLine)) :
      if i == len(outputLine)-1 :
        print("%s\n"%outputLine[i], file=outputFile, end="")
      else :
        print("%s\t"%outputLine[i], file=outputFile, end="")

main()
