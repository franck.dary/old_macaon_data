#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import pandas as pd
import numpy as np

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    

def createVectOfLang(wals):
    vectOfLang = dict()
    WALS = open(wals, "r")
    
    curLine = WALS.readline()
    print(curLine.strip())
    curLine = WALS.readline()
    
    while(not fileCompletelyRead(curLine)):
        curLine = curLine.strip().split(",")
        curLang = curLine[0]
        curVect = curLine[1:]
        vectOfLang[curLang] = curVect
        curLine = WALS.readline()
        
    for lang in vectOfLang:
        if('-1' in vectOfLang[lang]):
            nearestLang = findNearestLang(vectOfLang, lang)
            for i in range(len(vectOfLang[lang])):
                if(vectOfLang[lang][i] == '-1'):
                    vectOfLang[lang][i] = vectOfLang[nearestLang][i]
            
    return vectOfLang

    
def findNearestLang(vectOfLang, lang): #find the language with the most identical features, and whiwh does not contain -1
    maxIdenticalFeat = 0
    nearestLang = lang
    for potentialNearestLang in vectOfLang:
        curIdenticalFeat = 0
        if((potentialNearestLang == lang) or ('-1' in vectOfLang[potentialNearestLang])):
            continue
        for i in range(len(vectOfLang[lang])):
            if(vectOfLang[lang][i] == vectOfLang[potentialNearestLang][i]):
                curIdenticalFeat += 1
        if(curIdenticalFeat > maxIdenticalFeat):
            maxIdenticalFeat = curIdenticalFeat
            nearestLang = potentialNearestLang   
    return nearestLang
        

if __name__ == '__main__':
    
    if(len(sys.argv) != 2):
        sys.stderr("Error, give a matrix to complete")
        exit(1)
    wals_matrix = sys.argv[1]
    
    vectOfLang = createVectOfLang(wals_matrix)
    
    for k,v in vectOfLang.items():
        stringToPrint = k + "," + ','.join(v)
        print(stringToPrint)
