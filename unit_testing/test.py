#! /usr/bin/python3

import subprocess;
import sys;

logFile = "tests.log"

def cleanFiles() :
  process = subprocess.Popen("rm -r bin/* 2> /dev/null && rm eval/unit_testing.res 2> /dev/null", shell=True)
  process.wait()

def createLogFile() :
  process = subprocess.Popen("echo > "+logFile, shell=True)
  process.wait()

def logStr(s) :
  process = subprocess.Popen("echo \'"+s+"\' >> %s" % logFile, shell=True)
  process.wait()

def getTestName(test) :
  return str(test).split(' ')[1]

def testTrainConvergenceAndEval() :
  process = subprocess.Popen("./train.sh tagparser tagparser --dictCapacity 500 --batchSize 2 -n 25 --interactive 0 2>> %s" % logFile, shell=True)
  exitCode = process.wait()
  if exitCode != 0 :
    return "Training of tagparser failed"

  process = subprocess.Popen("cd eval && ./eval.sh tagparser 2> /dev/null > /dev/null && cat stderr.log >> %s && cd .." % logFile, shell=True)
  exitCode = process.wait()
  if exitCode != 0 :
    return "Eval of tagparser failed"

  for line in open("eval/unit_testing.res", "r") :
    for elem in line.split() :
      if len(elem) > 0 and elem[-1] == '%' :
        if int(float(elem[:-1])) != 100 :
          return "tagparser has not learned properly, some scores ain't 100%"

  return ""

def testFeatureRepresentation() :
  process = subprocess.Popen("./train.sh tagparser tagparser --dictCapacity 500 --batchSize 1 -n 1 --interactive 0 --showFeatureRepresentation 1 2>> %s" % logFile, shell=True)
  exitCode = process.wait()
  if exitCode != 0 :
    return "Training of tagparser failed"

  featureValuesPred = []

  for line in open(logFile, "r") :
    splited = line.split()
    if len(splited) > 0 and splited[0] == "FeatureValue" :
      featureValuesPred.append([splited[2], splited[3]])

  featureValuesModel = []

  for line in open("data/featuresValuesModel.txt", "r") :
    splited = line.split()
    if len(splited) > 0 and splited[0] == "FeatureValue" :
      featureValuesModel.append([splited[2], splited[3]])

  if len(featureValuesModel) != len(featureValuesPred) :
    return "Number of Feature values don't match : %s vs %s"%(str(len(featureValuesModel)),str(len(featureValuesPred)))

  for i in range(len(featureValuesModel)) :
    if featureValuesModel[i] != featureValuesPred[i] :
      return "difference : i=%d %s %s"%(i,str(featureValuesModel[i],str(featureValuesPred[i])))

  if featureValuesModel != featureValuesPred :
    return "Features values are not like the model in data"

  return ""

def main() :
  tests = [testTrainConvergenceAndEval, testFeatureRepresentation]

  cleanFiles()
  createLogFile()

  for test in tests :
    name = getTestName(test)
    print("Testing \'%s\' ... " % name, end="")
    sys.stdout.flush()
    logStr(25*"-"+" %s "%name+25*"-")
    message = test()
    logStr("")
    if len(message) == 0 :
      print("OK")
    else :
      print("\n\tERROR (\'%s\')" % message)


main()
