#! /usr/bin/python3

import sys
import random

def printUsageAndExit() :
  print("Usage : %s mcd nbLines"%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def main() :
  if len(sys.argv) != 3 :
    printUsageAndExit()

  random.seed(100)

  mcfMCD = readMCD(sys.argv[1])
  mcfMCDr = {v: k for k, v in mcfMCD.items()}

  data = []

  nbLines = int(sys.argv[2])
  for i in range(nbLines) :
    word = random.randint(0,5)
    pos = word + random.randint(-2,2)
    morpho = (pos+word) % 6
    data += [[word,pos,morpho]]

  for i in range(len(data)) :
    print(data[i][0],end="\t")
    print(data[i][1],end="\t")
    print(data[i][2],end="\n")

if __name__ == "__main__" :
  main()

