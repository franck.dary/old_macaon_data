Name : Lemmatizer Machine
Dicts : lemmatizer.dicts
%CLASSIFIERS
lemmatizer_lookup lemmatizer_lookup.cla
lemmatizer_rules lemmatizer_rules.cla
lemmatizer_case lemmatizer_case.cla
strategy strategy.cla
%STATES
strategy strategy
lemmatizer_lookup lemmatizer_lookup
lemmatizer_rules lemmatizer_rules
lemmatizer_case lemmatizer_case
%TRANSITIONS
lemmatizer_lookup strategy *
lemmatizer_rules strategy *
lemmatizer_case strategy *
strategy lemmatizer_lookup MOVE lemmatizer_lookup
strategy lemmatizer_rules MOVE lemmatizer_rules
strategy lemmatizer_case MOVE lemmatizer_case
