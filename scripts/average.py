#! /usr/bin/python3

import sys

COLSIZE1 = 21
COLSIZE = 16
nbCols = 0

resByExpName = {}
header = ""
indexesToIgnore = [i for i in range(100)]
for line in open(sys.argv[1], 'r') :
  if len(header) == 0 :
    header = line
    continue
  cols = line.split()
  name = cols[0]
  while len(name) > 0 and not name[-1] == '_' :
    if name[-1].isdigit() :
      name = name[:-1]
    else :
      break
  if name[-1] == '_' :
    name = name[:-1]
  else : 
    name = cols[0]
  if name not in resByExpName :
    resByExpName[name] = [[0.0,[]] for _ in cols]
  resByExpName[name][0][0] += 1
  nbCols = max(nbCols, len(cols)-1)
  for i in range(1,len(cols)) :
    col = cols[i]
    while not col[-1].isdigit() :
      col = col[:-1]
    col = float(col)
    if not col == 100.0 and i in indexesToIgnore :
      indexesToIgnore.remove(i)
    resByExpName[name][i][0] += col
    resByExpName[name][i][1].append(col)

print(header.split(" ")[0],end=" "*(COLSIZE1-len(header.split(" ")[0])))
colCounter = 0
for col in header.split(" ")[1:-1] :
  if len(col) == 0 :
    continue
  colCounter += 1
  if colCounter in indexesToIgnore :
    continue
  print(col,end=" "*(COLSIZE-len(col)))
for experience in resByExpName :
  print("\n"+experience,end=" "*(COLSIZE1-len(experience)))
  nbSamples = resByExpName[experience][0][0]
  for i in range(1,len(resByExpName[experience])) :
    if i in indexesToIgnore :
      continue
    col = resByExpName[experience][i]
    avg = col[0] / nbSamples
    variance = 0.0
    for val in col[1] :
      variance += (avg-val)**2
    variance /= nbSamples
    deviation = (variance)**0.5
    toPrint = "%.2f%%"%avg
    toPrint += "["+u"\u00B1"+"%.2f%%]"%deviation
    print(toPrint,end=" "*(COLSIZE-len(toPrint)))
print()
