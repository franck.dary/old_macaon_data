Name : Tagger, Morpho, Lemmatizer and Parser sequential Machine
Dicts : tagparser.dicts
%CLASSIFIERS
strategy strategy.cla
tagger tagger.cla
morpho morpho.cla
lemmatizer_lookup lemmatizer_lookup.cla
lemmatizer_rules lemmatizer_rules.cla
lemmatizer_case lemmatizer_case.cla
parser parser.cla
segmenter segmenter.cla
%STATES
strategy strategy
tagger tagger
morpho morpho
lemmatizer_lookup lemmatizer_lookup
lemmatizer_rules lemmatizer_rules
lemmatizer_case lemmatizer_case
parser parser
segmenter segmenter
%TRANSITIONS
strategy tagger MOVE tagger
strategy morpho MOVE morpho
strategy lemmatizer_lookup MOVE lemmatizer_lookup
strategy lemmatizer_rules MOVE lemmatizer_rules
strategy lemmatizer_case MOVE lemmatizer_case
strategy parser MOVE parser
strategy segmenter MOVE segmenter
tagger strategy *
morpho strategy *
lemmatizer_lookup strategy *
lemmatizer_case strategy *
lemmatizer_rules strategy *
parser strategy *
segmenter strategy *
