#!/bin/bash

if [ "`which crfsuite 2>/dev/null`" == "" ]
then
    echo "Error: could not find crfsuite. Please download it from http://www.chokkan.org/software/crfsuite/" >&2
    exit 1
fi

if [ $# != 3 ]
then
    echo "Usage: $0 <conll_corpus> <crf_template> <output_model>" >&2
    exit 1
fi

conll_corpus=$1
template=$2
model=$3
dir=`dirname $(readlink -f $0)`
python=`which python2 || which python 2>/dev/null`

cut -f 2,5 $conll_corpus | $python $dir/add-morpho-features.py | apply_template_crfsuite $template > tagger.train.crfsuite
crfsuite learn -m tagger.model.crfsuite -p feature.minfreq=2 -p max_iterations=60 -a l2sgd tagger.train.crfsuite 
crfsuite dump tagger.model.crfsuite | $python $dir/crfsuite2crfpp.py $template > $model
