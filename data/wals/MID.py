#!/usr/bin/python
# -*- coding: utf-8 -*-

# ./MID.py ../../../data/wals/matrix_valueReplace_deleteLangUseless listFamily.txt

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import collections
import random

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def distanceBetween2Vect(vect1, vect2):
    if(len(vect1) != len(vect2)):
        sys.stderr.write("Error length vectors\n")
    cptDiff = 0
    for i in range(len(vect1)):
        if(vect1[i] != vect2[i]):
            cptDiff+=1
    return cptDiff
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 3):
        sys.stderr("Error, give the matrix of vectors from the wals, and a file containing a list of language, define by their UD code (a family)")
        exit(1)
    filePathMatrixWals = sys.argv[1]
    filepathFamily = sys.argv[2]
    nbAleaFamilies = 50000
    vectOfLang = dict()
    
    with open(filePathMatrixWals, "r") as matrix:
        curLine = matrix.readline().strip()
        curLine = matrix.readline().strip()
        while(not fileCompletelyRead(curLine)):
            curLang = curLine.split(",")[0]
            curVect = curLine.split(",")
            curVect.pop(0)
            vectOfLang[curLang]=curVect
            curLine = matrix.readline().strip()
    vectOfLang = collections.OrderedDict(sorted(vectOfLang.items()))
    
    families = []
    meanLenFamily = 0
    
    with open(filepathFamily, "r") as listFamilies:
        curLine = listFamilies.readline().strip()
        while(not fileCompletelyRead(curLine)):
            curFamily = curLine.split("\t")
            families.append(curFamily)
            meanLenFamily+=len(curFamily)
            curLine = listFamilies.readline().strip()
            
    meanLenFamily /= len(families)        

    for family in families:
        sumMID = 0
        for vectlang1 in family:
            for vectlang2 in family:
                sumMID += distanceBetween2Vect(vectOfLang[vectlang1],vectOfLang[vectlang2])
        sumMID /= len(family)*1.0*len(family)-len(family)
        print(family, sumMID)
        
    familiesAlea = [] 
    for i in range(nbAleaFamilies):
        familyAlea = []
        for i in range(meanLenFamily):
            newLang = random.choice(vectOfLang.keys())
            while(newLang in familyAlea):
                newLang = random.choice(vectOfLang.keys())
            familyAlea.append(newLang)
        familiesAlea.append(familyAlea)
    
    meanMID = 0
    bestFamily = []
    MIDbest = 10
    for family in familiesAlea:
        sumMID = 0
        for vectlang1 in family:
            for vectlang2 in family:
                sumMID += distanceBetween2Vect(vectOfLang[vectlang1],vectOfLang[vectlang2])
        sumMID /= len(family)*1.0*len(family)-len(family)
        meanMID+=sumMID
        if(sumMID < MIDbest):
            bestFamily = family
            MIDbest = sumMID
    meanMID /= nbAleaFamilies
    print("MID mean :", meanMID)
    print("Best Family : ", bestFamily, "MID : ", MIDbest)
    
    
    
    
    
