---------------------------------------------------------------------------------------------------
Sans les features de 'speaker change' et 'silence duration'

tool                 POS             LEMMA           GOV             LABEL|GOV       EOS             
tagparser_noaudio    98.22%[±0.04%]  97.18%[±0.02%]  81.09%[±0.18%]  77.82%[±0.26%]  90.83%[±0.16%]  

/\ EOS 90.61% Accuracy :
0   95.41% Acc  93.61% Rec  94.50% F1 # 32249
1   64.15% Acc  71.75% Rec  67.74% F1 # 5137


---------------------------------------------------------------------------------------------------
Avec les features de 'speaker change' et 'silence duration'

tool                 POS             LEMMA           GOV             LABEL|GOV       EOS             
tagparser_audio      98.25%[±0.03%]  97.18%[±0.02%]  83.93%[±0.26%]  80.45%[±0.33%]  94.46%[±0.16%]  

tagparser_noaudio    98.14%     97.21%     79.79%     77.51%     90.76%     

tagparser_audio_2    98.19%     97.20%     82.97%     80.65%     94.62%     

/\ EOS 94.39% Accuracy :
0   96.60% Acc  96.91% Rec  96.76% F1 # 32249
1   80.20% Acc  78.61% Rec  79.39% F1 # 5137

---------------------------------------------------------------------------------------------------

Légende :
  [±x%] = écart type de x% (sur une dizaine d'entrainements)
  0     = étiquette pas de fin de phrase (ce token ne termine pas une phrase)
  1     = étiquette de fin de phrase (ce token termine une phrase)
  Acc   = précision
  Rec   = rappel
  F1    = f1-score
  #     = occurences de cette étiquette dans le corpus gold

Features 'speaker change' et 'silence duration' : 
  b.0.SPKRCHANGE
  b.0.SILENCE
  b.-1.SPKRCHANGE
  b.-1.SILENCE
  b.1.SPKRCHANGE
  b.1.SILENCE
  s.0.SPKRCHANGE
  s.0.SILENCE

