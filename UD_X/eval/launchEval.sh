#!/bin/bash

if [ "$#" -lt 2 ]; then
		echo "Usage : $0 pred.mcf gold.conllu mcd"
		exit
fi

PRED_MCF=$1
GOLD=$2
MCD=$3

python3 ../../tools/concatMorpho.py ${PRED_MCF} ${MCD} 8 > pred.morphConcat.mcf 2> newMCD.mcd 

python3 ../../tools/mcf2conllu.py pred.morphConcat.mcf newMCD.mcd ../data/MCDfiles/conllu.mcd > pred.conllu

python3 ../../tools/handleMultWordTokens.py $GOLD pred.conllu > pred_multTokens.conllu

python3 ../../tools/handleMultRoot.py pred_multTokens.conllu > tmp

python evaluation_script/conll17_ud_eval.py $GOLD tmp --weights evaluation_script/weights.clas 
