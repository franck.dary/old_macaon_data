Name : Dumb Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
signature signature.cla
parser_gov parser_gov.cla
parser_label parser_label.cla
parser_eos parser_eos.cla
%STATES
sgn1 signature
parser_gov parser_gov
parser_label parser_label
parser_eos parser_eos
%TRANSITIONS
sgn1 parser_gov 0 *
parser_gov parser_label 0 *
parser_label parser_eos 0 *
parser_eos sgn1 +1 *
