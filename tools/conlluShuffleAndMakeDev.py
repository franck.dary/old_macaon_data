#! /usr/bin/python3

import sys
import random

def printUsageAndExit() :
  print("USAGE : %s input.conllu ratio (outputRest.conllu)"%sys.argv[0])
  exit(1)

if __name__ == "__main__" :

  sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

  if len(sys.argv) != 3 and len(sys.argv) != 4 :
    printUsageAndExit()

  inputFile = sys.argv[1]
  ratio = sys.argv[2]

  sentences = []

  for line in open(inputFile, "r", encoding="utf8") :
    if len(line.strip()) < 3 :
      continue
    if line.strip().split('=')[0] == "# sent_id " :
      sentences += [[]]
    sentences[-1] += [line.strip()]

  random.shuffle(sentences)

  for sentence in sentences[:int(len(sentences)*float(ratio))] :
    for word in sentence :
      print(word)
    print("")

  if len(sys.argv) == 3 :
    exit(0)

  outputRest = open(sys.argv[3], "w", encoding="utf8")
  for sentence in sentences[int(len(sentences)*float(ratio))+1:] :
    for word in sentence :
      print(word, file=outputRest)
    print("", file=outputRest)

