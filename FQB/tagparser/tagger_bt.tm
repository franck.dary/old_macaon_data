Name : Tagger with backtrack Machine
Dicts : tagparser.dicts
%CLASSIFIERS
tagger tagger.cla
tagger_bt tagger_bt.cla
signature signature.cla
%STATES
signature1 signature
tagger1 tagger
tagger2 tagger
tagger_bt1 tagger_bt
%TRANSITIONS
signature1 tagger1 0 *
tagger1 tagger2 +1 *
tagger2 tagger_bt1 -1 *
tagger_bt1 signature1 +1 *
