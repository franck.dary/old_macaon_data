#!/bin/bash


set -e
set -u
set -o pipefail


function in_array {
  ARRAY=$2
  for e in ${ARRAY[*]}
  do
    if [[ "$e" == "$1" ]]
    then
      return 0
    fi
  done
  return 1
}


sed -i s/^FEAT_ID/#FEAT_ID/ data/Makefile # ignore ID col
sed -i -E "s/feats_WALS_.+$/feats_WALS_Wn/" data/Makefile
sed -i -E "s/^COMPLETE_WALS.+/COMPLETE_WALS=\$(WALS_DIR)\/language.csv/" data/Makefile		

cluster=false
W_representation=false
#error_usage="script usage: $(basename $0) [-i] [-c] [-t [SIGMA or L]] [-w [n or 80 or None]]"
error_usage="script usage: $(basename $0) [-i] [-c] [-t [SIGMA or L or Lbar]] [-l [valueLang]] [-w [n or 80 or d or df or None]]"
id=false
listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl no pl pt ro ru sl sme sv tr uk ur vi zh"
clusterLaunch=""
langToTest=false
NAMEMODEL="SIGMA"
typeTest="SIGMA"
while getopts 'cit:w:l:b:' OPTION; do
    case "$OPTION" in
	c)
	    cluster=true
	    ;;
	i)
	    sed -i s/^#FEAT_ID/FEAT_ID/ data/Makefile # take ID col
	    id=true
	    ;;
	w)
	    if [[ "$OPTARG" =~ ^(n|80|d|df|None)$ ]]; then
		if [[ "$OPTARG" != "None" ]]; then
		    W_representation="$OPTARG"
		    sed -i -E "s/feats_WALS_.+$/feats_WALS_W${W_representation}/" data/Makefile
		    if [[ "$OPTARG" =~ ^(d|df) ]]; then
			sed -i -E "s/^COMPLETE_WALS.+/COMPLETE_WALS=\$(WALS_DIR)\/Wdf.csv/" data/Makefile
		    else
			sed -i -E "s/^COMPLETE_WALS.+/COMPLETE_WALS=\$(WALS_DIR)\/language.csv/" data/Makefile			
		    fi
		fi
	    else
		echo ${error_usage} >&2
		exit 1
	    fi		
	    ;;
	t)
	    if in_array "$OPTARG" "SIGMA L Lbar" ; then
		NAMEMODEL="$OPTARG"
		typeTest="$OPTARG"
	    else
		echo ${error_usage} >&2
		exit 1
	    fi
	    ;;
	l)
	    if in_array "$OPTARG" "$listLang" ; then
		langToTest=$OPTARG
	    else
		echo ${error_usage} >&2
		echo "Argument of -l option must be in the following list: "$listLang  >&2
		exit 1
	    fi
	    ;;
	?)
	    echo ${error_usage} >&2
	    exit 1
	    ;;
    esac
done
shift "$(($OPTIND -1))"

if [ "$langToTest" = false ]; then
    if in_array "$NAMEMODEL" "L Lbar" ; then
	echo "Models L and Lbar need the argument -l"
	echo ${error_usage}
	exit 1
    fi
elif [ "$typeTest" = "SIGMA" ]; then
    echo "The argument -l require the argument -t with L or Lbar"
    echo ${error_usage}
    exit 1
else
    NAMEMODEL+=_${langToTest}
fi

if [ "${W_representation}" != false ]; then
    NAMEMODEL+=_W${W_representation}
fi

if [ "${id}" = true ]; then
    NAMEMODEL+=_ID
fi

if [ "${cluster}" = true ]; then
    clusterLaunch="oargen -n $NAMEMODEL -d ../outputCluster -t 96 --run"
fi

echo "Name of the model = "$NAMEMODEL >&2


cd ../data/multilingual_manon/

if [ "$langToTest" = false ]; then
    cp langToIgnoreEachTime.txt langToIgnore_train.txt
    cp langToIgnoreEachTime.txt langToIgnore_dev.txt
    cp langToIgnoreEachTime.txt langToIgnore_test.txt
elif [ "$typeTest" = "L" ]; then
    cat langToIgnore_all.txt | sed 's/'$langToTest'//' > langToIgnore_train.txt
    cat langToIgnore_all.txt | sed 's/'$langToTest'//' > langToIgnore_dev.txt
    cat langToIgnore_all.txt | sed 's/'$langToTest'//' > langToIgnore_test.txt
elif [ "$typeTest" = "Lbar" ]; then
    echo $(head -1 langToIgnoreEachTime.txt) $langToTest > langToIgnore_train.txt
    cp langToIgnore_train.txt langToIgnore_dev.txt
    cat langToIgnore_all.txt | sed 's/'$langToTest'//' > langToIgnore_test.txt
else
    echo "Error type of the test (SIGMA, L or Lbar support only)"
    echo ${error_usage}
    exit 1
fi

cd ../../UD_X/data/

make clean
if [ "${W_representation}" != false ]; then
    make
else
    make without_w
fi

mv train.mcf MCFfiles/train/train_$NAMEMODEL.mcf 
mv dev.mcf MCFfiles/dev/dev_$NAMEMODEL.mcf
mv test.mcf MCFfiles/test/test_$NAMEMODEL.mcf
mv wpmlgfs.mcd MCDfiles/$NAMEMODEL.mcd

cd ../parser 

mv train.bd dir_bd/train_$NAMEMODEL.bd
mv test.bd dir_bd/test_$NAMEMODEL.bd
mv parser.dicts dicts/$NAMEMODEL.dicts
mv parser.fm fm/$NAMEMODEL.fm

$clusterLaunch macaon_train --tm machine.tm --bd dir_bd/train_$NAMEMODEL.bd --mcd ../../data/MCDfiles/$NAMEMODEL.mcd -T ../../data/MCFfiles/train/train_$NAMEMODEL.mcf --dev ../../data/MCFfiles/dev/dev_$NAMEMODEL.mcf --dicts dicts/$NAMEMODEL.dicts --featureModels Parser=fm/$NAMEMODEL.fm --expName parser_$NAMEMODEL --lang UD_X -n 10 --templateName parser

cd ../

exit 1 ##############################################################################"
