Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
strategy strategy.cla
signature signature.cla
parser parser.cla
%STATES
strategy strategy
signature signature
parser parser
%TRANSITIONS
strategy signature MOVE signature
strategy parser MOVE parser
parser strategy *
signature strategy *
