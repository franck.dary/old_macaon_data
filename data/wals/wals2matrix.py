#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import pandas
import numpy as np

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def corr_UDX_WALS(filepathcorrespondanceUDX_WALS):
    dict_corr_UDX_WALS = dict()
    with open(filepathcorrespondanceUDX_WALS, "r") as corr:
        curLine = corr.readline()
        while(not fileCompletelyRead(curLine)):
            if(lineIsAComment(curLine)):
                curLine = corr.readline()
                continue
            
            curLine = curLine.split("\t")
            UDXcode = curLine[0]
            WALScode = curLine[1]
            dict_corr_UDX_WALS[WALScode] = UDXcode
            curLine = corr.readline()
    return dict_corr_UDX_WALS
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 3):
        sys.stderr("Error, give the file containing the wals, and the file with the correspondances UD_WALS and eventually the option --complete to replace Nan values with values of the nearest neighbour")
        exit(1)
    wals = sys.argv[1]
    dict_corr_UDX_WALS = corr_UDX_WALS(sys.argv[2])
    df = pandas.read_csv(wals)
    idColumns = df.columns.values
    
    codeLangWals = df[df.columns[0]]
    
    for indexLine in range(len(codeLangWals)):
        curLangWals = codeLangWals.get(indexLine)
        curLangUDX = dict_corr_UDX_WALS[curLangWals]
        codeLangWals.update(pandas.Series([curLangUDX], index=[indexLine]))
    df.update(codeLangWals)
    
    
    for i in range(len(df.columns)):
        curCol = df[df.columns[i]]
        for indexLine in range(len(curCol)):
            if(pandas.isnull(curCol.get(indexLine))):
                newValue = -1
            else:
                newValue = curCol.get(indexLine).strip().split(" ")[0]
            curCol.update(pandas.Series([newValue], index=[indexLine]))
        df.update(curCol)
    
    df.to_csv(sys.stdout, index=False)
        
