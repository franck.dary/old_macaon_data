#! /usr/bin/python3

import sys
import os

def printUsageAndExit() :
  print("USAGE : %s rawMcfDirectory metaDataDirectory"%(sys.argv[0]), file=sys.stderr)
  exit(1)

def cleanString(s) :
  result = s.strip()

  return result.replace("\"", "").replace("/>", "")

def treatDirectory(mcfs, metadatas) :
  trains = []
  tests = []
  metas = {}
  features = ["nbLocuteurs", "milieu", "type", "secteur"]
  featuresDecoda = ["2", "assistance", "finalise", "professionnel"]

  for entry in os.listdir(mcfs) :
    if os.path.isfile(os.path.join(mcfs, entry)) :
      if entry.endswith(".train") :
        trains.append(entry)
      elif entry.endswith(".test") :
        tests.append(entry)
      else :
        print("ERROR : unknown file %s"%entry, file=sys.stderr)
        exit(1)

  for entry in os.listdir(metadatas) :
    if os.path.isfile(os.path.join(metadatas, entry)) :
      splited = entry.split('.')
      if len(splited) != 2 or splited[1] != "xml" :
        continue
      name = splited[0]
      for line in open(metadatas+entry, "r") :
        if "corresp" in line :
          splited = line.split(' ')
          target = ""
          corresp = ""
          for s in splited :
            splited2 = s.split('=')
            if len(splited2) != 2 :
              continue
            if splited2[0] == "target" :
              target = cleanString(splited2[1])
            elif splited2[0] == "corresp" :
              corresp = cleanString(splited2[1])
            else :
              print("ERROR : wrong line \'%s\'."%line, file=sys.stderr)
              exit(1)
          if name not in metas :
            metas[name] = {}
          metas[name][corresp] = target

  output = open("train.conll", "w")

  for mcf in trains :
    featsForFile = list.copy(features)
    name = mcf.split(".")[0]
    if name not in metas :
      if "RATP" in name :
        featsForFile = featuresDecoda
      else :
        print("ERROR : metadata unknown for file %s."%mcf, file=sys.stderr)
        exit(1)
    if "RATP" not in name :
      for i in range(len(features)) :
        featValue = "n/a"
        if features[i] in metas[name] :
          featValue = metas[name][features[i]]

        featsForFile[i] = featValue

    for line in open(mcfs+mcf, "r") :
      clean = line.strip()
      if len(line) <= 2 :
        continue
      completeLine = clean
      for feat in featsForFile :
        completeLine += "\t" + feat
      print(completeLine,file=output)

  output = open("test.conll", "w")

  for mcf in tests :
    featsForFile = list.copy(features)
    name = mcf.split(".")[0]
    if name not in metas :
      if "RATP" in name :
        featsForFile = featuresDecoda
      else :
        print("ERROR : metadata unknown for file %s."%mcf, file=sys.stderr)
        exit(1)
    if "RATP" not in name :
      for i in range(len(features)) :
        featValue = "n/a"
        if features[i] in metas[name] :
          featValue = metas[name][features[i]]

        featsForFile[i] = featValue

    for line in open(mcfs+mcf, "r") :
      clean = line.strip()
      if len(line) <= 2 :
        continue
      completeLine = clean
      for feat in featsForFile :
        completeLine += "\t" + feat
      print(completeLine,file=output)

def main() :
  if len(sys.argv) != 3 :
    printUsageAndExit()

  treatDirectory(sys.argv[1], sys.argv[2])

main()
