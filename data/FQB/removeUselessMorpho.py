#! /usr/bin/python3

import sys
import re

def printUsageAndExit() :
  print("Usage : "+sys.argv[0]+" input")
  exit(1)

def removeUselessTags(filename) :
  lines = []
  for fetch in open(filename, "r", encoding="UTF-8") :
    col = 5
    split = fetch.split('\t')
    if len(split) > 2 :
      line = split[col]
      line = re.sub(r'sentid=([^\|]+)', '_', line)
      line = re.sub(r'dl=([^\|]+)', '_', line)
      line = re.sub(r'fctpath=([^\|]+)', '_', line)
      for i in range(4) :
        line = re.sub(r'\|\|', '|', line)
        line = re.sub(r'^\|', '', line)
        line = re.sub(r'^_', '', line)
        if len(line) > 0 and (line[-1] == '_' or line[-1] == '|') :
          line = line[:-1]
      if len(line) == 0 :
        line = "_"
      split[col] = line
    lines.append(split)

  f = open(filename, "w", encoding="UTF-8")
  for line in lines :
    print(*line,file=f,end="",sep="\t")

############################################################################


if len(sys.argv) < 2 :
  printUsageAndExit()

for i in range(1,len(sys.argv)) :
  removeUselessTags(sys.argv[i])
