#!/bin/bash

set -e
set -u
set -o pipefail

listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl no pl pt ro ru sl sme sv tr uk ur vi zh"

oargen -n Lbar_${curLang} -d outputCluster -t 72 -H see4c1 --run ./train.sh -t SIGMA -i
oargen -n SIGMA_ID_bis -d outputCluster -t 72 -H see4c1 --run macaon_train --tm machine.tm --bd dir_bd/train_SIGMA_ID.bd --mcd ../../data/MCDfiles/SIGMA_ID.mcd -T ../../data/MCFfiles/train/train_SIGMA_ID.mcf --dev ../../data/MCFfiles/dev/dev_SIGMA_ID.mcf --dicts dicts/SIGMA_ID.dicts --featureModels Parser=fm/SIGMA_ID.fm --expName parser_SIGMA_ID_bis --lang UD_X -n 10 --templateName parser
