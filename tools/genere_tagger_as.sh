#! /bin/bash

if [ "$#" -lt 2 ]; then
		echo "Usage : $0 mcd.mcd output.as toolsPath file.mcf"
		exit
fi

MCD=$1
OUTPUT=$2
TOOLS=$3
MCFFILE=$4


shift
shift
shift

ARGS=""
for arg in "$@"
do
		ARGS="$ARGS $arg"
done

POS="$(grep POS $MCD | cut -f1 -d ' ')"

if [ -z "$POS" ]; then
		echo "ERROR : MCD file does not contain a POS column"
		exit
fi

POS=$((POS+1))

rm -f tmp

cut -f $POS $MCFFILE > tmp

for arg in $ARGS
do
		cut -f $POS $arg >> tmp
done

python3 $TOOLS/sortUniq.py tmp > tmp2

sed -e 's/^/WRITE b.0 POS /' tmp2 > $OUTPUT

rm tmp tmp2
