#!/usr/bin/python
# -*- coding: utf-8 -*-

#./extractLangWals.py fichiers_config/codesLangExtractWals wals.csv > WALSwithLangOnly.csv

from __future__ import print_function          
import fileinput
import sys
import re
import os

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def extractLanguages(filepathCodes):
    
    codesFile = open(filepathCodes, "r")
    codes = []
    
    curCode = codesFile.readline().rstrip()
    while(not fileCompletelyRead(curCode)):
        if(lineIsAComment(curCode)):
            curCode = codesFile.readline().rstrip()
            continue
        codes.append(curCode)
        curCode = codesFile.readline().rstrip()
    return codes
    
    
    
if __name__ == '__main__':
        
    if(len(sys.argv) != 3):
        print("Error, give a file with the code of languages you want to keep, and the file containing the entire WALS")
        exit(1)
        

    
    codes = extractLanguages(sys.argv[1])
    
    WALS = open(sys.argv[2], "r")
    print(WALS.readline().rstrip()) #print columns id
    curLang = WALS.readline().rstrip()
    while(not fileCompletelyRead(curLang)):
        codeCurLang = curLang.split(',')[0]
        if(codeCurLang in codes):
            print(curLang)
        curLang = WALS.readline().rstrip()
        
        
