#!/bin/bash

fileLangToIgnore=$1
fileToModify=$2
if [ -s $fileLangToIgnore ]
then
	read -r line < $fileLangToIgnore
	for lang in $line
	do
			stringToMatch+=$lang"|"
	done
	stringToMatch="("${stringToMatch::-1}")(_[a-z]+)?$"
	grep -E -v "$stringToMatch" $fileToModify | sed '/^$/N;/^\n$/D' | sed '1{/^$/d}' 	
fi
