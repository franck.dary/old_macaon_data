&	100	coo	[pred="et_____1<arg1,arg2>",cat = coo]	et_____1	Default		%default
&amp;	100	coo	[pred="et_____1<arg1,arg2>",cat = coo]	et_____1	Default		%default
+	100	coo	[pred="plus_____1<arg1,arg2>",cat = coo]	plus_____1	Default		%default
,	100	ponctw	[pred="__virgule<arg1,arg2>",cat = coo,form=virgule]	,_____1	Default		%default
/	100	coo	[pred="ou_____1<arg1,arg2>",cat = coo]	ou_____1	Default		%default
ainsi qu'	100	coo	[pred="ainsi que_____1<arg1,arg2>",cat = coo,@e]	ainsi que_____1	Default	e	%default
ainsi que	100	coo	[pred="ainsi que_____1<arg1,arg2>",cat = coo]	ainsi que_____1	Default		%default
au même titre qu'	100	coo	[pred="au même titre que_____1<arg1,arg2>",cat = coo,@e]	au même titre que_____1	Default	e	%default
au même titre que	100	coo	[pred="au même titre que_____1<arg1,arg2>",cat = coo]	au même titre que_____1	Default		%default
c'est pourquoi	100	coo	[pred="c'est pourquoi_____1<arg1,arg2>",cat = coo]	c'est pourquoi_____1	Default		%default
c'est à dire	100	coo	[pred="c'est à dire_____1<arg1,arg2>",cat = coo]	c'est à dire_____1	Default		%default
c'est-à-dire	100	coo	[pred="c'est-à-dire_____1<arg1,arg2>",cat = coo]	c'est-à-dire_____1	Default		%default
c-à-d	100	coo	[pred="c'est-à-dire_____1<arg1,arg2>",cat = coo]	c'est-à-dire_____1	Default		%default
c.-à.-d.	100	coo	[pred="c'est-à-dire_____1<arg1,arg2>",cat = coo]	c'est-à-dire_____1	Default		%default
car	100	coo	[pred="car_____1<arg1,arg2>",RDexplication =? []2,arg2 =? []2,cat = coo]	car_____1	Default		%default
comme	100	coo	[pred="comme_____1<arg1,arg2>",cat = coo]	comme_____1	Default		%default
càd	100	coo	[pred="c'est-à-dire_____1<arg1,arg2>",cat = coo]	c'est-à-dire_____1	Default		%default
d'où	100	coo	[pred="d'où_____1<arg1,arg2>",cat = coo]	d'où_____1	Default		%default
donc	100	coo	[pred="donc_____1<arg1,arg2>",cat = coo]	donc_____1	Default		%default
et	100	coo	[pred="et_____1<arg1,arg2>",cat = coo]	et_____1	Default		%default
et alors	100	coo	[pred="et alors_____1<arg1,arg2>",cat = coo]	et alors_____1	Default		%default
et aussi	100	coo	[pred="et aussi_____1<arg1,arg2>",cat = coo]	et aussi_____1	Default		%default
et donc	100	coo	[pred="et donc_____1<arg1,arg2>",cat = coo]	et donc_____1	Default		%default
et encore	100	coo	[pred="et encore_____1<arg1,arg2>",cat = coo]	et encore_____1	Default		%default
et même	100	coo	[pred="et même_____1<arg1,arg2>",cat = coo]	et même_____1	Default		%default
et puis	100	coo	[pred="et puis_____1<arg1,arg2>",cat = coo]	et puis_____1	Default		%default
et-ou	100	coo	[pred="et-ou_____1<arg1,arg2>",cat = coo]	et-ou_____1	Default		%default
et/ou	100	coo	[pred="et-ou_____1<arg1,arg2>",cat = coo]	et-ou_____1	Default		%default
et|ou	100	coo	[pred="et-ou_____1<arg1,arg2>",cat = coo]	et-ou_____1	Default		%default
i.e.	100	coo	[pred="id est_____1<arg1,arg2>",cat = coo]	id est_____1	Default		%default
id est	100	coo	[pred="id est_____1<arg1,arg2>",cat = coo]	id est_____1	Default		%default
ie	100	coo	[pred="id est_____1<arg1,arg2>",cat = coo]	id est_____1	Default		%default
mais	100	coo	[pred="mais_____1<arg1,arg2>",cat = coo]	mais_____1	Default		%default
mais alors	100	coo	[pred="mais alors_____1<arg1,arg2>",cat = coo]	mais alors_____1	Default		%default
mais aussi	100	coo	[pred="mais aussi_____1<arg1,arg2>",cat = coo]	mais aussi_____1	Default		%default
mais encore	100	coo	[pred="mais encore_____1<arg1,arg2>",cat = coo]	mais encore_____1	Default		%default
ni	100	coo	[pred="ni_____1<arg1,arg2>",cat = coo]	ni_____1	Default		%default
or	100	coo	[pred="or_____1<arg1,arg2>",cat = coo]	or_____1	Default		%default
ou	100	coo	[pred="ou_____1<arg1,arg2>",cat = coo]	ou_____1	Default		%default
ou alors	100	coo	[pred="ou alors_____1<arg1,arg2>",cat = coo]	ou alors_____1	Default		%default
ou bien	100	coo	[pred="ou bien_____1<arg1,arg2>",cat = coo]	ou bien_____1	Default		%default
ou bien encore	100	coo	[pred="ou bien encore_____1<arg1,arg2>",cat = coo]	ou bien encore_____1	Default		%default
ou encore	100	coo	[pred="ou encore_____1<arg1,arg2>",cat = coo]	ou encore_____1	Default		%default
ou/et	100	coo	[pred="et-ou_____1<arg1,arg2>",cat = coo]	et-ou_____1	Default		%default
plus	100	coo	[pred="plus_____1<arg1,arg2>",cat = coo]	plus_____1	Default		%default
puis	100	coo	[pred="puis_____1<arg1,arg2>",cat = coo]	puis_____1	Default		%default
sinon	100	coo	[pred="sinon_____1<arg1,arg2>",cat = coo]	sinon_____1	Default		%default
soit	100	coo	[pred="soit_____1<arg1,arg2>",cat = coo]	soit_____1	Default		%default
tout comme	100	coo	[pred="tout comme_____1<arg1,arg2>",cat = coo]	tout comme_____1	Default		%default
voire	100	coo	[pred="voire_____1<arg1,arg2>",cat = coo]	voire_____1	Default		%default
voire même	100	coo	[pred="voire même_____1<arg1,arg2>",cat = coo]	voire même_____1	Default		%default
à savoir	100	coo	[pred="à savoir_____1<arg1,arg2>",cat = coo]	à savoir_____1	Default		%default
