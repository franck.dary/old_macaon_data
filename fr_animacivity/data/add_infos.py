import sys

def printUsageAndExit() :
  print("USAGE : %s file.mcf file.mcd output.mcf output.mcd lexicon_animacy lexicon_massivity"%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def readDict(dictFilename) :
  res = {}

  for line in open(dictFilename, "r") :
    splited = line.strip().split()
    if len(splited) != 2 :
      print("ERROR : wrong line \'%s\' in file \'%s\'. Aborting."%(line,dictFilename),file=sys.stderr)
      exit(1)

    res[splited[0]] = splited[1]

  return res

def discretize(f) :
  return int(2*float(f))

def getFinalValue(d, s, pos) :
  if not s in d : 
    return "_"

  if pos != "nc" : 
    return "_"

  return str(discretize(d[s]))
  
def main() :
  if len(sys.argv) != 7 :
    printUsageAndExit()

  inputFilename = sys.argv[1]
  outputFilename = sys.argv[3]
  animacyFilename = sys.argv[5]
  massivityFilename = sys.argv[6]

  inputMCD = readMCD(sys.argv[2])
  outputMCD = readMCD(sys.argv[4])
  inputMCDr = {v: k for k, v in inputMCD.items()} 
  outputMCDr = {v: k for k, v in outputMCD.items()} 

  animacy = readDict(animacyFilename)
  massivity = readDict(massivityFilename)

  massivityIndex = int(outputMCDr["MASSIVITY"])
  animacyIndex = int(outputMCDr["ANIMACY"])
  lemmaIndex = int(inputMCDr["LEMMA"])
  posIndex = int(inputMCDr["POS"])

  outputs = []
  for line in open(inputFilename, "r") :
    splited = line.strip().split('\t')
    output = []
    for i in range(len(splited)) :
      colName = inputMCD[str(i)]
      if not colName in outputMCDr :
        print("ERROR : column \'%s\' is not in \'%s\'. Aborting."%(colName,sys.argv[4]), file=sys.stderr)
        exit(1)
      indexInOutput = int(outputMCDr[colName])
      while len(output) <= indexInOutput :
        output.append("")
      output[indexInOutput] = splited[i]
    while len(output) <= max(massivityIndex, animacyIndex) :
      output.append("")
    output[massivityIndex] = getFinalValue(massivity, splited[lemmaIndex], splited[posIndex])
    output[animacyIndex] = getFinalValue(animacy, splited[lemmaIndex], splited[posIndex])

    outputs.append(output)

  outFile = open(outputFilename, "w")
  for output in outputs :
    for j in range(len(output)) :
      if j == len(output)-1 :
        print(output[j],end="\n",file=outFile)
      else :
        print(output[j],end="\t",file=outFile)

main()
