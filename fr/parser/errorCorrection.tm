Name : Parser Machine with systematic backtracking
Dicts : parser.dicts
%CLASSIFIERS
signature signature.cla
parser parser.cla
error_parser error_parser.cla
%STATES
signature signature
parser parser
error_parser_stay error_parser
error_parser_move error_parser
%TRANSITIONS
signature parser 0 *
parser error_parser_stay 0 LEFT
parser error_parser_stay 0 EOS
parser error_parser_stay 0 REDUCE
parser error_parser_stay 0 ROOT
parser error_parser_move 0 SHIFT
parser error_parser_move 0 RIGHT
error_parser_stay parser 0 *
error_parser_move parser 0 BACK
error_parser_move signature +1 *
