#! /bin/bash

LANG=fr_animacivity
MCF=../data/test.mcf
MCD=../data/wpmlgfs.mcd
ARGS="--keepPunct EOS --relative LABEL GOV --ignore FORM --ignore MASSIVITY --ignore ANIMACY"

exec ../../scripts/eval.py $LANG $MCF $MCD $* $ARGS
