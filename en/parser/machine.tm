Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
signature signature.cla
parser parser.cla
%STATES
sgn1 signature
parser1 parser
%TRANSITIONS
sgn1 parser1 0 *
parser1 parser1 0 LEFT
parser1 parser1 0 REDUCE
parser1 parser1 0 EOS
parser1 sgn1 +1 SHIFT
parser1 sgn1 +1 RIGHT
