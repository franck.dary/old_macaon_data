#!/usr/bin/perl

$conll = shift;
$threshold = shift;

open(my $CONLL, "<", $conll)
	or die "Can't open < $conll: $!";

while(<$CONLL>){
    if(!/^#/){
	($index, $form, $lemma, $cpos, $pos, $morpho, $gov, $label, $C9, $C10, $language) = split /\t/;
	$hash_morpho{$morpho}++;
    }
}

close($CONLL);

open(my $CONLL, "<", $conll)
	or die "Can't open < $conll: $!";

while(<$CONLL>){
    if(/^\n/){
	print "\n";
    }
    else{
	chop;
	if(!/^#/){
	($index, $form, $lemma, $cpos, $pos, $morpho, $gov, $label, $C9, $C10, $language) = split /\t/;
	    if($hash_morpho{$morpho} < $threshold){
		$morpho = "_";
	    }
	    print "$index\t$form\t$lemma\t$cpos\t$pos\t$morpho\t$gov\t$label\t$C9\t$C10\t$language\n";
	}
    }
}

close($CONLL);
