+	100	adv	[pred="plus_____1<Obl:(scompl|sn|sa|sinf)>",adv_kind=intens,cat=adv]	plus_____1	Default		%default
N.B..	100	adv	[pred="nota bene_____1",cat=adv]	nota bene_____1	Default		%default
_NUMBER à _NUMBER	100	adv	[pred="_NUMBER à _NUMBER_____1",cat=adv]	_NUMBER à _NUMBER_____1	Default		%default
a capella	100	adv	[pred="a capella_____1",chunk_type=GP,cat=adv]	a capella_____1	Default		%default
a contrario	100	adv	[pred="a contrario_____1",cat=adv]	a contrario_____1	Default		%default
a fortiori	100	adv	[pred="a fortiori_____1",cat=adv]	a fortiori_____1	Default		%default
a latere	100	adv	[pred="a latere_____1",cat=adv]	a latere_____1	Default		%default
a minima	100	adv	[pred="a minima_____1",cat=adv]	a minima_____1	Default		%default
a posteriori	100	adv	[pred="a posteriori_____1",cat=adv]	a posteriori_____1	Default		%default
a priori	100	adv	[pred="a priori_____1",cat=adv]	a priori_____1	Default		%default
ab absurdo	100	adv	[pred="ab absurdo_____1",cat=adv]	ab absurdo_____1	Default		%default
ab origine	100	adv	[pred="ab origine_____1",cat=adv]	ab origine_____1	Default		%default
abandonnément	100	advm	[pred="abandonnément_____1",clivee=+,detach_neg = +,cat=adv]	abandonnément_____1	Default		%default
abjectement	100	advm	[pred="abjectement_____1",clivee=+,detach_neg = +,cat=adv]	abjectement_____1	Default		%default
abjectement	100	advp	[pred="abjectement_____2",detach = +,detach_neg = +,cat=adv]	abjectement_____2	Default		%default
abominablement	100	advm	[pred="abominablement_____1",advi=combien,clivee=+,cat=adv]	abominablement_____1	Default		%default
abondamment	100	advm	[pred="abondamment_____1",advi=combien,cat=adv]	abondamment_____1	Default		%default
aboralement	100	advm	[pred="aboralement_____1",cat=adv]	aboralement_____1	Default		%default
abruptement	100	advm	[pred="abruptement_____1",clivee=+,detach_neg = +,cat=adv]	abruptement_____1	Default		%default
abrégément	100	advm	[pred="abrégément_____1",clivee=+,cat=adv]	abrégément_____1	Default		%default
abréviativement	100	advm	[pred="abréviativement_____1",clivee=+,cat=adv]	abréviativement_____1	Default		%default
absconsément	100	advm	[pred="absconsément_____1",clivee=+,detach_neg = +,cat=adv]	absconsément_____1	Default		%default
absolument	100	advm	[pred="absolument_____1",advi=combien,cat=adv]	absolument_____1	Default		%default
abstractivement	100	advm	[pred="abstractivement_____1",clivee=+,cat=adv]	abstractivement_____1	Default		%default
abstraitement	100	advm	[pred="abstraitement_____1",clivee=+,detach_neg = +,cat=adv]	abstraitement_____1	Default		%default
abstrusément	100	advm	[pred="abstrusément_____1",clivee=+,detach_neg = +,cat=adv]	abstrusément_____1	Default		%default
absurdement	100	advm	[pred="absurdement_____1",clivee=+,detach_neg = +,cat=adv]	absurdement_____1	Default		%default
abusivement	100	advm	[pred="abusivement_____1",clivee=+,cat=adv]	abusivement_____1	Default		%default
académiquement	100	advm	[pred="académiquement_____1",clivee=+,detach_neg = +,cat=adv]	académiquement_____1	Default		%default
acariâtrement	100	advm	[pred="acariâtrement_____1",clivee=+,detach_neg = +,cat=adv]	acariâtrement_____1	Default		%default
accelerando	100	adv	[pred="accelerando_____1",cat=adv]	accelerando_____1	Default		%default
acceptablement	100	advm	[pred="acceptablement_____1",clivee=+,cat=adv]	acceptablement_____1	Default		%default
accessoirement	100	advm	[pred="accessoirement_____1",clivee=+,cat=adv]	accessoirement_____1	Default		%default
accidentellement	100	advm	[pred="accidentellement_____1",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	accidentellement_____1	Default		%default
accidentellement	100	advm	[pred="accidentellement_____2",clivee=+,cat=adv]	accidentellement_____2	Default		%default
accortement	100	advm	[pred="accortement_____1",clivee=+,detach_neg = +,cat=adv]	accortement_____1	Default		%default
acerbement	100	advm	[pred="acerbement_____1",clivee=+,detach_neg = +,cat=adv]	acerbement_____1	Default		%default
acidement	100	advm	[pred="acidement_____1",clivee=+,detach_neg = +,cat=adv]	acidement_____1	Default		%default
acoustiquement	100	advm	[pred="acoustiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	acoustiquement_____1	Default		%default
acoustiquement	100	advm	[pred="acoustiquement_____2",clivee=+,cat=adv]	acoustiquement_____2	Default		%default
acrimonieusement	100	advm	[pred="acrimonieusement_____1",clivee=+,detach_neg = +,cat=adv]	acrimonieusement_____1	Default		%default
acrobatiquement	100	advm	[pred="acrobatiquement_____1",cat=adv]	acrobatiquement_____1	Default		%default
actiniquement	100	advm	[pred="actiniquement_____1",cat=adv]	actiniquement_____1	Default		%default
activement	100	advm	[pred="activement_____1",clivee=+,detach_neg = +,cat=adv]	activement_____1	Default		%default
actuellement	100	advm	[pred="actuellement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	actuellement_____1	Default		%default
ad hoc	100	adv	[pred="ad hoc_____1",cat=adv]	ad hoc_____1	Default		%default
ad hominem	100	adv	[pred="ad hominem_____1",cat=adv]	ad hominem_____1	Default		%default
ad libitum	100	adv	[pred="ad libitum_____1",cat=adv]	ad libitum_____1	Default		%default
ad limina	100	adv	[pred="ad limina_____1",cat=adv]	ad limina_____1	Default		%default
ad litem	100	adv	[pred="ad litem_____1",cat=adv]	ad litem_____1	Default		%default
ad litteram	100	adv	[pred="ad litteram_____1",cat=adv]	ad litteram_____1	Default		%default
ad nauseam	100	adv	[pred="ad nauseam_____1",cat=adv]	ad nauseam_____1	Default		%default
ad nutum	100	adv	[pred="ad nutum_____1",cat=adv]	ad nutum_____1	Default		%default
ad patres	100	adv	[pred="ad patres_____1",cat=adv]	ad patres_____1	Default		%default
ad personam	100	adv	[pred="ad personam_____1",cat=adv]	ad personam_____1	Default		%default
ad valorem	100	adv	[pred="ad valorem_____1",cat=adv]	ad valorem_____1	Default		%default
ad vitam aeternam	100	adv	[pred="ad vitam aeternam_____1",cat=adv]	ad vitam aeternam_____1	Default		%default
adagio	100	adv	[pred="adagio_____1",cat=adv]	adagio_____1	Default		%default
additivement	100	advm	[pred="additivement_____1",clivee=+,cat=adv]	additivement_____1	Default		%default
adiabatiquement	100	advm	[pred="adiabatiquement_____1",cat=adv]	adiabatiquement_____1	Default		%default
adipeusement	100	advm	[pred="adipeusement_____1",clivee=+,detach_neg = +,cat=adv]	adipeusement_____1	Default		%default
adjectivement	100	advm	[pred="adjectivement_____1",clivee=+,cat=adv]	adjectivement_____1	Default		%default
administrativement	100	advm	[pred="administrativement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	administrativement_____1	Default		%default
administrativement	100	advm	[pred="administrativement_____2",clivee=+,cat=adv]	administrativement_____2	Default		%default
admirablement	100	advm	[pred="admirablement_____1",advi=combien,clivee=+,cat=adv]	admirablement_____1	Default		%default
admirativement	100	advm	[pred="admirativement_____1",clivee=+,detach_neg = +,cat=adv]	admirativement_____1	Default		%default
adorablement	100	advm	[pred="adorablement_____1",advi=combien,clivee=+,cat=adv]	adorablement_____1	Default		%default
adroitement	100	advm	[pred="adroitement_____1",clivee=+,detach_neg = +,cat=adv]	adroitement_____1	Default		%default
adroitement	100	advp	[pred="adroitement_____2",detach = +,detach_neg = +,cat=adv]	adroitement_____2	Default		%default
adultérieurement	100	advm	[pred="adultérieurement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	adultérieurement_____1	Default		%default
adverbialement	100	advm	[pred="adverbialement_____1",clivee=+,cat=adv]	adverbialement_____1	Default		%default
adversativement	100	advp	[pred="adversativement_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	adversativement_____1	Default		%default
adéquatement	100	advm	[pred="adéquatement_____1",clivee=+,cat=adv]	adéquatement_____1	Default		%default
affablement	100	advm	[pred="affablement_____1",clivee=+,detach_neg = +,cat=adv]	affablement_____1	Default		%default
affaireusement	100	advm	[pred="affaireusement_____1",clivee=+,detach_neg = +,cat=adv]	affaireusement_____1	Default		%default
affectionnément	100	advm	[pred="affectionnément_____1",clivee=+,detach_neg = +,cat=adv]	affectionnément_____1	Default		%default
affectivement	100	advm	[pred="affectivement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	affectivement_____1	Default		%default
affectivement	100	advm	[pred="affectivement_____2",clivee=+,cat=adv]	affectivement_____2	Default		%default
affectueusement	100	advm	[pred="affectueusement_____1",clivee=+,detach_neg = +,cat=adv]	affectueusement_____1	Default		%default
affinement	100	advm	[pred="affinement_____1",clivee=+,cat=adv]	affinement_____1	Default		%default
affirmativement	100	advm	[pred="affirmativement_____1",clivee=+,cat=adv]	affirmativement_____1	Default		%default
affreusement	100	advm	[pred="affreusement_____1",advi=combien,clivee=+,cat=adv]	affreusement_____1	Default		%default
agilement	100	advm	[pred="agilement_____1",clivee=+,detach_neg = +,cat=adv]	agilement_____1	Default		%default
agitato	100	adv	[pred="agitato_____1",cat=adv]	agitato_____1	Default		%default
agogiquement	100	advm	[pred="agogiquement_____1",cat=adv]	agogiquement_____1	Default		%default
agressivement	100	advm	[pred="agressivement_____1",clivee=+,detach_neg = +,cat=adv]	agressivement_____1	Default		%default
agronomiquement	100	advm	[pred="agronomiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	agronomiquement_____1	Default		%default
agronomiquement	100	advm	[pred="agronomiquement_____2",clivee=+,cat=adv]	agronomiquement_____2	Default		%default
agréablement	100	advm	[pred="agréablement_____1",clivee=+,cat=adv]	agréablement_____1	Default		%default
aguicheusement	100	advm	[pred="aguicheusement_____1",clivee=+,detach_neg = +,cat=adv]	aguicheusement_____1	Default		%default
aigrement	100	advm	[pred="aigrement_____1",clivee=+,detach_neg = +,cat=adv]	aigrement_____1	Default		%default
aigûment	100	advm	[pred="aigûment_____1",clivee=+,cat=adv]	aigûment_____1	Default		%default
ailleurs	100	adv	[pred="ailleurs_____1",cat=adv]	ailleurs_____1	Default		%default
aimablement	100	advm	[pred="aimablement_____1",clivee=+,detach_neg = +,cat=adv]	aimablement_____1	Default		%default
ainsi	100	adv	[pred="ainsi_____1",cat=adv]	ainsi_____1	Default		%default
ainsi de suite	100	adv	[pred="ainsi de suite_____1",cat=adv]	ainsi de suite_____1	Default		%default
aisément	100	advm	[pred="aisément_____1",clivee=+,cat=adv]	aisément_____1	Default		%default
alchimiquement	100	advm	[pred="alchimiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	alchimiquement_____1	Default		%default
alchimiquement	100	advm	[pred="alchimiquement_____2",clivee=+,cat=adv]	alchimiquement_____2	Default		%default
alcooliquement	100	advm	[pred="alcooliquement_____1",clivee=+,cat=adv]	alcooliquement_____1	Default		%default
alentour	100	adv	[pred="alentour_____1",cat=adv]	alentour_____1	Default		%default
alertement	100	advm	[pred="alertement_____1",clivee=+,detach_neg = +,cat=adv]	alertement_____1	Default		%default
algorithmiquement	100	advm	[pred="algorithmiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	algorithmiquement_____1	Default		%default
algorithmiquement	100	advm	[pred="algorithmiquement_____2",clivee=+,cat=adv]	algorithmiquement_____2	Default		%default
algébriquement	100	advm	[pred="algébriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	algébriquement_____1	Default		%default
algébriquement	100	advm	[pred="algébriquement_____2",clivee=+,cat=adv]	algébriquement_____2	Default		%default
alias	100	adv	[pred="alias_____1",cat=adv]	alias_____1	Default		%default
alimentairement	100	advm	[pred="alimentairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	alimentairement_____1	Default		%default
allegretto	100	adv	[pred="allegretto_____1",cat=adv]	allegretto_____1	Default		%default
allegro	100	adv	[pred="allegro_____1",cat=adv]	allegro_____1	Default		%default
allergiquement	100	advm	[pred="allergiquement_____1",clivee=+,cat=adv]	allergiquement_____1	Default		%default
allopathiquement	100	advm	[pred="allopathiquement_____1",clivee=+,cat=adv]	allopathiquement_____1	Default		%default
allusivement	100	advm	[pred="allusivement_____1",clivee=+,detach_neg = +,cat=adv]	allusivement_____1	Default		%default
allègrement	100	advm	[pred="allègrement_____1",clivee=+,detach_neg = +,cat=adv]	allègrement_____1	Default		%default
allégoriquement	100	advm	[pred="allégoriquement_____1",clivee=+,cat=adv]	allégoriquement_____1	Default		%default
allô	100	adv	[pred="allô_____1",cat=adv]	allô_____1	Default		%default
alors	100	adv	[pred="alors_____1",cat=adv]	alors_____1	Default		%default
alphabétiquement	100	advm	[pred="alphabétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	alphabétiquement_____1	Default		%default
alphabétiquement	100	advm	[pred="alphabétiquement_____2",clivee=+,cat=adv]	alphabétiquement_____2	Default		%default
alternativement	100	advm	[pred="alternativement_____1",clivee=+,cat=adv]	alternativement_____1	Default		%default
altièrement	100	advm	[pred="altièrement_____1",clivee=+,detach_neg = +,cat=adv]	altièrement_____1	Default		%default
aléatoirement	100	advm	[pred="aléatoirement_____1",clivee=+,cat=adv]	aléatoirement_____1	Default		%default
ambigument	100	advm	[pred="ambigument_____1",clivee=+,detach_neg = +,cat=adv]	ambigument_____1	Default		%default
ambitieusement	100	advm	[pred="ambitieusement_____1",clivee=+,detach_neg = +,cat=adv]	ambitieusement_____1	Default		%default
amiablement	100	advm	[pred="amiablement_____1",clivee=+,cat=adv]	amiablement_____1	Default		%default
amicalement	100	advm	[pred="amicalement_____1",clivee=+,detach_neg = +,cat=adv]	amicalement_____1	Default		%default
amiteusement	100	advm	[pred="amiteusement_____1",clivee=+,detach_neg = +,cat=adv]	amiteusement_____1	Default		%default
amoralement	100	advm	[pred="amoralement_____1",clivee=+,detach_neg = +,cat=adv]	amoralement_____1	Default		%default
amoroso	100	adv	[pred="amoroso_____1",cat=adv]	amoroso_____1	Default		%default
amoureusement	100	advm	[pred="amoureusement_____1",clivee=+,detach_neg = +,cat=adv]	amoureusement_____1	Default		%default
amphibologiquement	100	advm	[pred="amphibologiquement_____1",clivee=+,cat=adv]	amphibologiquement_____1	Default		%default
amphigouriquement	100	advm	[pred="amphigouriquement_____1",clivee=+,cat=adv]	amphigouriquement_____1	Default		%default
amplement	100	advm	[pred="amplement_____1",advi=combien,cat=adv]	amplement_____1	Default		%default
ampoulément	100	advm	[pred="ampoulément_____1",clivee=+,detach_neg = +,cat=adv]	ampoulément_____1	Default		%default
amusamment	100	advm	[pred="amusamment_____1",clivee=+,detach_neg = +,cat=adv]	amusamment_____1	Default		%default
amusément	100	advm	[pred="amusément_____1",clivee=+,detach_neg = +,cat=adv]	amusément_____1	Default		%default
amènement	100	advm	[pred="amènement_____1",clivee=+,detach_neg = +,cat=adv]	amènement_____1	Default		%default
amèrement	100	advm	[pred="amèrement_____1",clivee=+,detach_neg = +,cat=adv]	amèrement_____1	Default		%default
anachroniquement	100	advm	[pred="anachroniquement_____1",clivee=+,cat=adv]	anachroniquement_____1	Default		%default
analogiquement	100	advm	[pred="analogiquement_____1",clivee=+,cat=adv]	analogiquement_____1	Default		%default
analytiquement	100	advm	[pred="analytiquement_____1",clivee=+,cat=adv]	analytiquement_____1	Default		%default
anaphoriquement	100	advm	[pred="anaphoriquement_____1",clivee=+,cat=adv]	anaphoriquement_____1	Default		%default
anarchiquement	100	advm	[pred="anarchiquement_____1",clivee=+,cat=adv]	anarchiquement_____1	Default		%default
anatomiquement	100	advm	[pred="anatomiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	anatomiquement_____1	Default		%default
anatomiquement	100	advm	[pred="anatomiquement_____2",clivee=+,cat=adv]	anatomiquement_____2	Default		%default
ancestralement	100	advm	[pred="ancestralement_____1",clivee=+,cat=adv]	ancestralement_____1	Default		%default
anciennement	100	advm	[pred="anciennement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	anciennement_____1	Default		%default
andante	100	adv	[pred="andante_____1",cat=adv]	andante_____1	Default		%default
andantino	100	adv	[pred="andantino_____1",cat=adv]	andantino_____1	Default		%default
anecdotiquement	100	advm	[pred="anecdotiquement_____1",clivee=+,cat=adv]	anecdotiquement_____1	Default		%default
angulairement	100	advm	[pred="angulairement_____1",clivee=+,cat=adv]	angulairement_____1	Default		%default
anguleusement	100	advm	[pred="anguleusement_____1",clivee=+,cat=adv]	anguleusement_____1	Default		%default
angéliquement	100	advm	[pred="angéliquement_____1",clivee=+,detach_neg = +,cat=adv]	angéliquement_____1	Default		%default
animalement	100	advm	[pred="animalement_____1",clivee=+,cat=adv]	animalement_____1	Default		%default
anno domini	100	adv	[pred="anno domini_____1",cat=adv]	anno domini_____1	Default		%default
annuellement	100	advm	[pred="annuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	annuellement_____1	Default		%default
anodinement	100	advm	[pred="anodinement_____1",clivee=+,cat=adv]	anodinement_____1	Default		%default
anonymement	100	advm	[pred="anonymement_____1",clivee=+,cat=adv]	anonymement_____1	Default		%default
anormalement	100	advm	[pred="anormalement_____1",advi=combien,clivee=+,cat=adv]	anormalement_____1	Default		%default
anthropocentriquement	100	advm	[pred="anthropocentriquement_____1",clivee=+,cat=adv]	anthropocentriquement_____1	Default		%default
anthropologiquement	100	advm	[pred="anthropologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	anthropologiquement_____1	Default		%default
anthropologiquement	100	advm	[pred="anthropologiquement_____2",clivee=+,cat=adv]	anthropologiquement_____2	Default		%default
anthropomorphiquement	100	advm	[pred="anthropomorphiquement_____1",clivee=+,cat=adv]	anthropomorphiquement_____1	Default		%default
anthropométriquement	100	advm	[pred="anthropométriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	anthropométriquement_____1	Default		%default
anthropométriquement	100	advm	[pred="anthropométriquement_____2",clivee=+,cat=adv]	anthropométriquement_____2	Default		%default
anticipativement	100	advm	[pred="anticipativement_____1",clivee=+,cat=adv]	anticipativement_____1	Default		%default
anticonstitutionnellement	100	advm	[pred="anticonstitutionnellement_____1",clivee=+,cat=adv]	anticonstitutionnellement_____1	Default		%default
antidémocratiquement	100	advm	[pred="antidémocratiquement_____1",clivee=+,cat=adv]	antidémocratiquement_____1	Default		%default
antigéniquement	100	advm	[pred="antigéniquement_____1",clivee=+,cat=adv]	antigéniquement_____1	Default		%default
antinomiquement	100	advm	[pred="antinomiquement_____1",clivee=+,cat=adv]	antinomiquement_____1	Default		%default
antipathiquement	100	advm	[pred="antipathiquement_____1",clivee=+,detach_neg = +,cat=adv]	antipathiquement_____1	Default		%default
antiquement	100	advm	[pred="antiquement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	antiquement_____1	Default		%default
antisportivement	100	advm	[pred="antisportivement_____1",clivee=+,detach_neg = +,cat=adv]	antisportivement_____1	Default		%default
antithétiquement	100	advm	[pred="antithétiquement_____1",clivee=+,cat=adv]	antithétiquement_____1	Default		%default
antécédement	100	advm	[pred="antécédement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	antécédement_____1	Default		%default
antécédemment	100	advm	[pred="antécédemment_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	antécédemment_____1	Default		%default
antérieurement	100	advm	[pred="antérieurement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	antérieurement_____1	Default		%default
anxieusement	100	advm	[pred="anxieusement_____1",clivee=+,detach_neg = +,cat=adv]	anxieusement_____1	Default		%default
apathiquement	100	advm	[pred="apathiquement_____1",clivee=+,detach_neg = +,cat=adv]	apathiquement_____1	Default		%default
apertement	100	advm	[pred="apertement_____1",clivee=+,cat=adv]	apertement_____1	Default		%default
apicalement	100	advm	[pred="apicalement_____1",clivee=+,cat=adv]	apicalement_____1	Default		%default
apocalyptiquement	100	advm	[pred="apocalyptiquement_____1",clivee=+,cat=adv]	apocalyptiquement_____1	Default		%default
apodictiquement	100	advm	[pred="apodictiquement_____1",clivee=+,cat=adv]	apodictiquement_____1	Default		%default
apologétiquement	100	advm	[pred="apologétiquement_____1",clivee=+,cat=adv]	apologétiquement_____1	Default		%default
apostoliquement	100	advm	[pred="apostoliquement_____1",clivee=+,cat=adv]	apostoliquement_____1	Default		%default
apparemment	100	advp	[pred="apparemment_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	apparemment_____1	Default		%default
appassionato	100	adv	[pred="appassionato_____1",cat=adv]	appassionato_____1	Default		%default
approbativement	100	advm	[pred="approbativement_____1",clivee=+,detach_neg = +,cat=adv]	approbativement_____1	Default		%default
approchant	100	adv	[pred="approchant_____1",cat=adv]	approchant_____1	Default		%default
approximativement	100	advm	[pred="approximativement_____1",detach_neg = +,modnc = +,cat=adv]	approximativement_____1	Default		%default
approximativement	100	advm	[pred="approximativement_____2",clivee=+,cat=adv]	approximativement_____2	Default		%default
appréciablement	100	advm	[pred="appréciablement_____1",advi=combien,clivee=+,cat=adv]	appréciablement_____1	Default		%default
après	100	adv	[pred="après_____1",cat=adv]	après_____1	Default		%default
après coup	100	adv	[pred="après coup_____1",cat=adv]	après coup_____1	Default		%default
après tout	100	adv	[pred="après tout_____1",cat=adv]	après tout_____1	Default		%default
après-demain	100	adv	[pred="après-demain_____1",cat=adv]	après-demain_____1	Default		%default
arbitrairement	100	advm	[pred="arbitrairement_____1",clivee=+,cat=adv]	arbitrairement_____1	Default		%default
arbitralement	100	advm	[pred="arbitralement_____1",clivee=+,cat=adv]	arbitralement_____1	Default		%default
archaïquement	100	advm	[pred="archaïquement_____1",clivee=+,detach_neg = +,cat=adv]	archaïquement_____1	Default		%default
architectoniquement	100	advm	[pred="architectoniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	architectoniquement_____1	Default		%default
architectoniquement	100	advm	[pred="architectoniquement_____2",clivee=+,cat=adv]	architectoniquement_____2	Default		%default
architecturalement	100	advm	[pred="architecturalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	architecturalement_____1	Default		%default
architecturalement	100	advm	[pred="architecturalement_____2",clivee=+,cat=adv]	architecturalement_____2	Default		%default
archéologiquement	100	advm	[pred="archéologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	archéologiquement_____1	Default		%default
archéologiquement	100	advm	[pred="archéologiquement_____2",clivee=+,cat=adv]	archéologiquement_____2	Default		%default
ardemment	100	advm	[pred="ardemment_____1",advi=combien,cat=adv]	ardemment_____1	Default		%default
ardûment	100	advm	[pred="ardûment_____1",clivee=+,cat=adv]	ardûment_____1	Default		%default
argotiquement	100	advm	[pred="argotiquement_____1",clivee=+,cat=adv]	argotiquement_____1	Default		%default
aristocratiquement	100	advm	[pred="aristocratiquement_____1",clivee=+,cat=adv]	aristocratiquement_____1	Default		%default
arithmétiquement	100	advm	[pred="arithmétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	arithmétiquement_____1	Default		%default
arithmétiquement	100	advm	[pred="arithmétiquement_____2",clivee=+,cat=adv]	arithmétiquement_____2	Default		%default
arrière	100	adv	[pred="arrière_____1",cat=adv]	arrière_____1	Default		%default
arrogamment	100	advm	[pred="arrogamment_____1",clivee=+,detach_neg = +,cat=adv]	arrogamment_____1	Default		%default
artificiellement	100	advm	[pred="artificiellement_____1",clivee=+,cat=adv]	artificiellement_____1	Default		%default
artificieusement	100	advm	[pred="artificieusement_____1",clivee=+,detach_neg = +,cat=adv]	artificieusement_____1	Default		%default
artisanalement	100	advm	[pred="artisanalement_____1",clivee=+,cat=adv]	artisanalement_____1	Default		%default
artistement	100	advm	[pred="artistement_____1",clivee=+,detach_neg = +,cat=adv]	artistement_____1	Default		%default
artistiquement	100	advm	[pred="artistiquement_____1",clivee=+,cat=adv]	artistiquement_____1	Default		%default
asap	100	adv	[pred="dès que possible_____1",cat=adv]	dès que possible_____1	Default		%default
ascétiquement	100	advm	[pred="ascétiquement_____1",clivee=+,detach_neg = +,cat=adv]	ascétiquement_____1	Default		%default
aseptiquement	100	advm	[pred="aseptiquement_____1",clivee=+,cat=adv]	aseptiquement_____1	Default		%default
asiatiquement	100	advm	[pred="asiatiquement_____1",clivee=+,cat=adv]	asiatiquement_____1	Default		%default
assai	100	adv	[pred="assai_____1",cat=adv]	assai_____1	Default		%default
assertivement	100	advm	[pred="assertivement_____1",clivee=+,cat=adv]	assertivement_____1	Default		%default
assertoriquement	100	advm	[pred="assertoriquement_____1",clivee=+,cat=adv]	assertoriquement_____1	Default		%default
assez	100	adv	[pred="assez_____1<Obl:(pour-sn|pour-sinf|pour-scompl)>",adv_kind=intens,cat=adv]	assez_____1	Default		%default
assidûment	100	advm	[pred="assidûment_____1",clivee=+,detach_neg = +,cat=adv]	assidûment_____1	Default		%default
associativement	100	advm	[pred="associativement_____1",clivee=+,cat=adv]	associativement_____1	Default		%default
assurément	100	advp	[pred="assurément_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	assurément_____1	Default		%default
astrologiquement	100	advm	[pred="astrologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	astrologiquement_____1	Default		%default
astrologiquement	100	advm	[pred="astrologiquement_____2",clivee=+,cat=adv]	astrologiquement_____2	Default		%default
astronomiquement	100	advm	[pred="astronomiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	astronomiquement_____1	Default		%default
astronomiquement	100	advm	[pred="astronomiquement_____2",advi=combien,cat=adv]	astronomiquement_____2	Default		%default
astronomiquement	100	advm	[pred="astronomiquement_____3",clivee=+,cat=adv]	astronomiquement_____3	Default		%default
astrophysiquement	100	advm	[pred="astrophysiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	astrophysiquement_____1	Default		%default
astrophysiquement	100	advm	[pred="astrophysiquement_____2",clivee=+,cat=adv]	astrophysiquement_____2	Default		%default
astucieusement	100	advm	[pred="astucieusement_____1",clivee=+,detach_neg = +,cat=adv]	astucieusement_____1	Default		%default
astucieusement	100	advp	[pred="astucieusement_____2",detach = +,detach_neg = +,cat=adv]	astucieusement_____2	Default		%default
asymptotiquement	100	advm	[pred="asymptotiquement_____1",cat=adv]	asymptotiquement_____1	Default		%default
asymétriquement	100	advm	[pred="asymétriquement_____1",clivee=+,cat=adv]	asymétriquement_____1	Default		%default
ataviquement	100	advm	[pred="ataviquement_____1",clivee=+,cat=adv]	ataviquement_____1	Default		%default
athlétiquement	100	advm	[pred="athlétiquement_____1",clivee=+,cat=adv]	athlétiquement_____1	Default		%default
atlantiquement	100	advm	[pred="atlantiquement_____1",clivee=+,cat=adv]	atlantiquement_____1	Default		%default
atomiquement	100	advm	[pred="atomiquement_____1",cat=adv]	atomiquement_____1	Default		%default
atrocement	100	advm	[pred="atrocement_____1",advi=combien,clivee=+,cat=adv]	atrocement_____1	Default		%default
atrocement	100	advm	[pred="atrocement_____2",clivee=+,cat=adv]	atrocement_____2	Default		%default
attenant	100	adv	[pred="attenant_____1",cat=adv]	attenant_____1	Default		%default
attentatoirement	100	advm	[pred="attentatoirement_____1",clivee=+,cat=adv]	attentatoirement_____1	Default		%default
attentivement	100	advm	[pred="attentivement_____1",clivee=+,detach_neg = +,cat=adv]	attentivement_____1	Default		%default
attiquement	100	advm	[pred="attiquement_____1",clivee=+,cat=adv]	attiquement_____1	Default		%default
au black	100	advp	[pred="au black_____1",cat=adv]	au black_____1	Default		%default
au bout du compte	100	adv	[pred="au bout du compte_____1",cat=adv]	au bout du compte_____1	Default		%default
au centre	100	adv	[pred="au centre_____1",cat=adv]	au centre_____1	Default		%default
au contraire	100	adv	[pred="au contraire_____1",cat=adv]	au contraire_____1	Default		%default
au dedans	100	adv	[pred="au dedans_____1",cat=adv]	au dedans_____1	Default		%default
au dehors	100	adv	[pred="au dehors_____1",cat=adv]	au dehors_____1	Default		%default
au delà	100	adv	[pred="au-delà_____1",cat=adv]	au-delà_____1	Default		%default
au demeurant	100	adv	[pred="au demeurant_____1",cat=adv]	au demeurant_____1	Default		%default
au diable Vauvert	100	adv	[pred="au diable Vauvert_____1",chunk_type=GP,cat=adv]	au diable Vauvert_____1	Default		%default
au débotté	100	advp	[pred="au débotté_____1",cat=adv]	au débotté_____1	Default		%default
au fait	100	adv	[pred="au fait_____1",cat=adv]	au fait_____1	Default		%default
au fil de l'eau	100	adv	[pred="au fil de l'eau_____1",chunk_type=GP,cat=adv]	au fil de l'eau_____1	Default		%default
au fur et à mesure	100	adv	[pred="au fur et à mesure_____1",cat=adv]	au fur et à mesure_____1	Default		%default
au jour le jour	100	adv	[pred="au jour le jour_____1",cat=adv]	au jour le jour_____1	Default		%default
au lieu	100	adv	[pred="au lieu_____1",cat=adv]	au lieu_____1	Default		%default
au mieux	100	adv	[pred="au mieux_____1",cat=adv]	au mieux_____1	Default		%default
au moins	100	adv	[pred="au moins_____1",cat=adv]	au moins_____1	Default		%default
au noir	100	advp	[pred="au noir_____1",cat=adv]	au noir_____1	Default		%default
au petit bonheur la chance	100	adv	[pred="au petit bonheur la chance_____1",chunk_type=GP,cat=adv]	au petit bonheur la chance_____1	Default		%default
au plus	100	adv	[pred="au plus_____1",cat=adv]	au plus_____1	Default		%default
au plus tard	100	adv	[pred="au plus tard_____1",cat=adv]	au plus tard_____1	Default		%default
au plus tôt	100	adv	[pred="au plus tôt_____1",cat=adv]	au plus tôt_____1	Default		%default
au plus vite	100	adv	[pred="au plus vite_____1",cat=adv]	au plus vite_____1	Default		%default
au préalable	100	adv	[pred="au préalable_____1",cat=adv]	au préalable_____1	Default		%default
au quart de tour	100	adv	[pred="au quart de tour_____1",chunk_type=GP,cat=adv]	au quart de tour_____1	Default		%default
au total	100	adv	[pred="au total_____1",adv_kind=modnc,cat=adv]	au total_____1	Default		%default
au total	100	adv	[pred="au total_____2",cat=adv]	au total_____2	Default		%default
au-dedans	100	adv	[pred="au-dedans_____1",cat=adv]	au-dedans_____1	Default		%default
au-dehors	100	adv	[pred="au-dehors_____1",cat=adv]	au-dehors_____1	Default		%default
au-delà	100	adv	[pred="au-delà_____1",cat=adv]	au-delà_____1	Default		%default
au-dessous	100	adv	[pred="au-dessous_____1",cat=adv]	au-dessous_____1	Default		%default
au-dessus	100	adv	[pred="au-dessus_____1",cat=adv]	au-dessus_____1	Default		%default
au-devant	100	adv	[pred="au-devant_____1",cat=adv]	au-devant_____1	Default		%default
aucunement	100	advm	[pred="aucunement_____1",advi=combien,cat=adv]	aucunement_____1	Default		%default
audacieusement	100	advm	[pred="audacieusement_____1",clivee=+,detach_neg = +,cat=adv]	audacieusement_____1	Default		%default
audacieusement	100	advp	[pred="audacieusement_____2",detach = +,detach_neg = +,cat=adv]	audacieusement_____2	Default		%default
audiblement	100	advm	[pred="audiblement_____1",clivee=+,cat=adv]	audiblement_____1	Default		%default
auditivement	100	advm	[pred="auditivement_____1",clivee=+,cat=adv]	auditivement_____1	Default		%default
auguralement	100	advm	[pred="auguralement_____1",clivee=+,cat=adv]	auguralement_____1	Default		%default
augustement	100	advm	[pred="augustement_____1",clivee=+,detach_neg = +,cat=adv]	augustement_____1	Default		%default
aujourd'hui	100	adv	[pred="aujourd'hui_____1",cat=adv]	aujourd'hui_____1	Default		%default
auparavant	100	adv	[pred="auparavant_____1",cat=adv]	auparavant_____1	Default		%default
auprès	100	adv	[pred="auprès_____1",cat=adv]	auprès_____1	Default		%default
auriculairement	100	advm	[pred="auriculairement_____1",cat=adv]	auriculairement_____1	Default		%default
aussi	100	adv	[pred="aussi_____1<Obl:(scompl|sn|sa|sinf)>",cat=adv]	aussi_____1	Default		%default
aussi bien	100	adv	[pred="aussi bien_____1",cat=adv]	aussi bien_____1	Default		%default
aussi sec	100	adv	[pred="aussi sec_____1",cat=adv]	aussi sec_____1	Default		%default
aussitôt	100	adv	[pred="aussitôt_____1",cat=adv]	aussitôt_____1	Default		%default
austèrement	100	advm	[pred="austèrement_____1",clivee=+,detach_neg = +,cat=adv]	austèrement_____1	Default		%default
autant	100	adv	[pred="autant_____1<Obl:(scompl|sn|sa|sinf)>",cat=adv]	autant_____1	Default		%default
autarciquement	100	advm	[pred="autarciquement_____1",clivee=+,cat=adv]	autarciquement_____1	Default		%default
authentiquement	100	advm	[pred="authentiquement_____1",advi=combien|comment,cat=adv]	authentiquement_____1	Default		%default
authentiquement	100	advm	[pred="authentiquement_____2",clivee=+,cat=adv]	authentiquement_____2	Default		%default
autocratiquement	100	advm	[pred="autocratiquement_____1",clivee=+,cat=adv]	autocratiquement_____1	Default		%default
automatiquement	100	advm	[pred="automatiquement_____1",clivee=+,cat=adv]	automatiquement_____1	Default		%default
autonomement	100	advm	[pred="autonomement_____1",clivee=+,detach_neg = +,cat=adv]	autonomement_____1	Default		%default
autoritairement	100	advm	[pred="autoritairement_____1",clivee=+,detach_neg = +,cat=adv]	autoritairement_____1	Default		%default
autour	100	adv	[pred="autour_____1",cat=adv]	autour_____1	Default		%default
autrefois	100	adv	[pred="autrefois_____1",cat=adv]	autrefois_____1	Default		%default
autrement	100	advm	[pred="autrement_____1",advi=combien,cat=adv]	autrement_____1	Default		%default
autrement	100	advm	[pred="autrement_____2",clivee=+,cat=adv]	autrement_____2	Default		%default
autrement	100	advp	[pred="autrement_____3",detach = +,detach_neg = +,cat=adv]	autrement_____3	Default		%default
autrement dit	100	adv	[pred="autrement dit_____1",cat=adv]	autrement dit_____1	Default		%default
auxiliairement	100	advm	[pred="auxiliairement_____1",clivee=+,cat=adv]	auxiliairement_____1	Default		%default
avant	100	adv	[pred="avant_____1",cat=adv]	avant_____1	Default		%default
avant tout	100	adv	[pred="avant tout_____1",cat=adv]	avant tout_____1	Default		%default
avant-hier	100	adv	[pred="avant-hier_____1",cat=adv]	avant-hier_____1	Default		%default
avantageusement	100	advm	[pred="avantageusement_____1",clivee=+,cat=adv]	avantageusement_____1	Default		%default
avarement	100	advm	[pred="avarement_____1",clivee=+,detach_neg = +,cat=adv]	avarement_____1	Default		%default
avaricieusement	100	advm	[pred="avaricieusement_____1",clivee=+,detach_neg = +,cat=adv]	avaricieusement_____1	Default		%default
avec	100	adv	[pred="avec_____1",cat=adv]	avec_____1	Default		%default
aventureusement	100	advm	[pred="aventureusement_____1",clivee=+,detach_neg = +,cat=adv]	aventureusement_____1	Default		%default
aventurément	100	advm	[pred="aventurément_____1",clivee=+,cat=adv]	aventurément_____1	Default		%default
aveuglément	100	advm	[pred="aveuglément_____1",clivee=+,cat=adv]	aveuglément_____1	Default		%default
avidement	100	advm	[pred="avidement_____1",clivee=+,detach_neg = +,cat=adv]	avidement_____1	Default		%default
avunculairement	100	advm	[pred="avunculairement_____1",clivee=+,cat=adv]	avunculairement_____1	Default		%default
axialement	100	advm	[pred="axialement_____1",clivee=+,cat=adv]	axialement_____1	Default		%default
axiologiquement	100	advm	[pred="axiologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	axiologiquement_____1	Default		%default
axiologiquement	100	advm	[pred="axiologiquement_____2",clivee=+,cat=adv]	axiologiquement_____2	Default		%default
axiomatiquement	100	advm	[pred="axiomatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	axiomatiquement_____1	Default		%default
axiomatiquement	100	advm	[pred="axiomatiquement_____2",clivee=+,cat=adv]	axiomatiquement_____2	Default		%default
ayé	100	adv	[pred="ayé_____1",cat=adv]	ayé_____1	Default		%default
aériennement	100	advm	[pred="aériennement_____1",clivee=+,cat=adv]	aériennement_____1	Default		%default
aérodynamiquement	100	advm	[pred="aérodynamiquement_____1",cat=adv]	aérodynamiquement_____1	Default		%default
babéliquement	100	advm	[pred="babéliquement_____1",clivee=+,cat=adv]	babéliquement_____1	Default		%default
bachiquement	100	advm	[pred="bachiquement_____1",clivee=+,cat=adv]	bachiquement_____1	Default		%default
bactériologiquement	100	advm	[pred="bactériologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bactériologiquement_____1	Default		%default
bactériologiquement	100	advm	[pred="bactériologiquement_____2",clivee=+,cat=adv]	bactériologiquement_____2	Default		%default
badaudement	100	advm	[pred="badaudement_____1",clivee=+,detach_neg = +,cat=adv]	badaudement_____1	Default		%default
badinement	100	advm	[pred="badinement_____1",clivee=+,detach_neg = +,cat=adv]	badinement_____1	Default		%default
balistiquement	100	advm	[pred="balistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	balistiquement_____1	Default		%default
balourdement	100	advm	[pred="balourdement_____1",clivee=+,detach_neg = +,cat=adv]	balourdement_____1	Default		%default
balsamiquement	100	advm	[pred="balsamiquement_____1",clivee=+,cat=adv]	balsamiquement_____1	Default		%default
banalement	100	advm	[pred="banalement_____1",clivee=+,detach_neg = +,cat=adv]	banalement_____1	Default		%default
banalement	100	advm	[pred="banalement_____2",clivee=+,cat=adv]	banalement_____2	Default		%default
barbarement	100	advm	[pred="barbarement_____1",clivee=+,detach_neg = +,cat=adv]	barbarement_____1	Default		%default
barométriquement	100	advm	[pred="barométriquement_____1",clivee=+,cat=adv]	barométriquement_____1	Default		%default
baroquement	100	advm	[pred="baroquement_____1",clivee=+,cat=adv]	baroquement_____1	Default		%default
bas	100	adv	[pred="bas_____1",cat=adv]	bas_____1	Default		%default
basalement	100	advm	[pred="basalement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	basalement_____1	Default		%default
bassement	100	advp	[pred="bassement_____1",detach = +,detach_neg = +,cat=adv]	bassement_____1	Default		%default
batailleusement	100	advm	[pred="batailleusement_____1",clivee=+,detach_neg = +,cat=adv]	batailleusement_____1	Default		%default
bavardement	100	advm	[pred="bavardement_____1",clivee=+,detach_neg = +,cat=adv]	bavardement_____1	Default		%default
baveusement	100	advm	[pred="baveusement_____1",clivee=+,detach_neg = +,cat=adv]	baveusement_____1	Default		%default
bcp	100	adv	[pred="beaucoup_____1",adv_kind=intens,cat=adv]	beaucoup_____1	Default		%default
beau	100	adv	[pred="beau_____1",cat=adv]	beau_____1	Default		%default
beaucoup	100	adv	[pred="beaucoup_____1",adv_kind=intens,cat=adv]	beaucoup_____1	Default		%default
bel et bien	100	adv	[pred="bel et bien_____1",cat=adv]	bel et bien_____1	Default		%default
belle lurette	100	adv	[pred="belle lurette_____1",cat=adv]	belle lurette_____1	Default		%default
bellement	100	advm	[pred="bellement_____1",cat=adv]	bellement_____1	Default		%default
belliqueusement	100	advm	[pred="belliqueusement_____1",clivee=+,detach_neg = +,cat=adv]	belliqueusement_____1	Default		%default
bene	100	adv	[pred="bene_____1",cat=adv]	bene_____1	Default		%default
benoîtement	100	advm	[pred="benoîtement_____1",clivee=+,detach_neg = +,cat=adv]	benoîtement_____1	Default		%default
besogneusement	100	advm	[pred="besogneusement_____1",clivee=+,detach_neg = +,cat=adv]	besogneusement_____1	Default		%default
bestialement	100	advm	[pred="bestialement_____1",clivee=+,detach_neg = +,cat=adv]	bestialement_____1	Default		%default
bibliographiquement	100	advm	[pred="bibliographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bibliographiquement_____1	Default		%default
bibliographiquement	100	advm	[pred="bibliographiquement_____2",clivee=+,cat=adv]	bibliographiquement_____2	Default		%default
bibliquement	100	advm	[pred="bibliquement_____1",clivee=+,cat=adv]	bibliquement_____1	Default		%default
bien	100	adv	[pred="bien_____1",cat=adv]	bien_____1	Default		%default
bien	100	adv	[pred="bien_____2",adv_kind=modprep,cat=adv]	bien_____2	Default		%default
bien entendu	100	adv	[pred="bien entendu_____1",cat=adv]	bien entendu_____1	Default		%default
bien sûr	100	adv	[pred="bien sûr_____1",cat=adv]	bien sûr_____1	Default		%default
bienheureusement	100	advm	[pred="bienheureusement_____1",clivee=+,detach_neg = +,cat=adv]	bienheureusement_____1	Default		%default
bientôt	100	adv	[pred="bientôt_____1",cat=adv]	bientôt_____1	Default		%default
bienveillamment	100	advm	[pred="bienveillamment_____1",clivee=+,detach_neg = +,cat=adv]	bienveillamment_____1	Default		%default
bigotement	100	advm	[pred="bigotement_____1",clivee=+,detach_neg = +,cat=adv]	bigotement_____1	Default		%default
bigrement	100	advm	[pred="bigrement_____1",advi=combien,cat=adv]	bigrement_____1	Default		%default
bihebdomadairement	100	advm	[pred="bihebdomadairement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	bihebdomadairement_____1	Default		%default
bijectivement	100	advm	[pred="bijectivement_____1",clivee=+,cat=adv]	bijectivement_____1	Default		%default
bilatéralement	100	advm	[pred="bilatéralement_____1",clivee=+,cat=adv]	bilatéralement_____1	Default		%default
bileusement	100	advm	[pred="bileusement_____1",clivee=+,detach_neg = +,cat=adv]	bileusement_____1	Default		%default
bilieusement	100	advm	[pred="bilieusement_____1",clivee=+,detach_neg = +,cat=adv]	bilieusement_____1	Default		%default
bille en tête	100	adv	[pred="bille en tête_____1",cat=adv]	bille en tête_____1	Default		%default
bimensuellement	100	advm	[pred="bimensuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	bimensuellement_____1	Default		%default
bimestriellement	100	advm	[pred="bimestriellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	bimestriellement_____1	Default		%default
binairement	100	advm	[pred="binairement_____1",clivee=+,cat=adv]	binairement_____1	Default		%default
bioacoustiquement	100	advm	[pred="bioacoustiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bioacoustiquement_____1	Default		%default
bioacoustiquement	100	advm	[pred="bioacoustiquement_____2",clivee=+,cat=adv]	bioacoustiquement_____2	Default		%default
biochimiquement	100	advm	[pred="biochimiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	biochimiquement_____1	Default		%default
biochimiquement	100	advm	[pred="biochimiquement_____2",clivee=+,cat=adv]	biochimiquement_____2	Default		%default
bioclimatiquement	100	advm	[pred="bioclimatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bioclimatiquement_____1	Default		%default
bioclimatiquement	100	advm	[pred="bioclimatiquement_____2",clivee=+,cat=adv]	bioclimatiquement_____2	Default		%default
biographiquement	100	advm	[pred="biographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	biographiquement_____1	Default		%default
biographiquement	100	advm	[pred="biographiquement_____2",clivee=+,cat=adv]	biographiquement_____2	Default		%default
biologiquement	100	advm	[pred="biologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	biologiquement_____1	Default		%default
biologiquement	100	advm	[pred="biologiquement_____2",clivee=+,cat=adv]	biologiquement_____2	Default		%default
biomédicalement	100	advm	[pred="biomédicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	biomédicalement_____1	Default		%default
biomédicalement	100	advm	[pred="biomédicalement_____2",clivee=+,cat=adv]	biomédicalement_____2	Default		%default
bioniquement	100	advm	[pred="bioniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bioniquement_____1	Default		%default
bioniquement	100	advm	[pred="bioniquement_____2",clivee=+,cat=adv]	bioniquement_____2	Default		%default
biophysiquement	100	advm	[pred="biophysiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	biophysiquement_____1	Default		%default
biophysiquement	100	advm	[pred="biophysiquement_____2",clivee=+,cat=adv]	biophysiquement_____2	Default		%default
bioélectriquement	100	advm	[pred="bioélectriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bioélectriquement_____1	Default		%default
bioélectriquement	100	advm	[pred="bioélectriquement_____2",clivee=+,cat=adv]	bioélectriquement_____2	Default		%default
bioélectroniquement	100	advm	[pred="bioélectroniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	bioélectroniquement_____1	Default		%default
bioélectroniquement	100	advm	[pred="bioélectroniquement_____2",clivee=+,cat=adv]	bioélectroniquement_____2	Default		%default
biquotidiennement	100	advm	[pred="biquotidiennement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	biquotidiennement_____1	Default		%default
bis	100	adv	[pred="bis_____1",cat=adv]	bis_____1	Default		%default
bisannuellement	100	advm	[pred="bisannuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	bisannuellement_____1	Default		%default
biunivoquement	100	advm	[pred="biunivoquement_____1",clivee=+,cat=adv]	biunivoquement_____1	Default		%default
bizarrement	100	advm	[pred="bizarrement_____1",clivee=+,detach_neg = +,cat=adv]	bizarrement_____1	Default		%default
bizarrement	100	advp	[pred="bizarrement_____2",detach = +,detach_neg = +,cat=adv]	bizarrement_____2	Default		%default
blafardement	100	advm	[pred="blafardement_____1",clivee=+,cat=adv]	blafardement_____1	Default		%default
blagueusement	100	advm	[pred="blagueusement_____1",clivee=+,detach_neg = +,cat=adv]	blagueusement_____1	Default		%default
blanchement	100	advm	[pred="blanchement_____1",clivee=+,cat=adv]	blanchement_____1	Default		%default
bleuâtrement	100	advm	[pred="bleuâtrement_____1",clivee=+,cat=adv]	bleuâtrement_____1	Default		%default
blondement	100	advm	[pred="blondement_____1",clivee=+,cat=adv]	blondement_____1	Default		%default
blâmablement	100	advm	[pred="blâmablement_____1",clivee=+,detach_neg = +,cat=adv]	blâmablement_____1	Default		%default
bon	100	adv	[pred="bon_____1",cat=adv]	bon_____1	Default		%default
bon an mal an	100	advp	[pred="bon an mal an_____1",cat=adv]	bon an mal an_____1	Default		%default
bonassement	100	advm	[pred="bonassement_____1",clivee=+,detach_neg = +,cat=adv]	bonassement_____1	Default		%default
bonifacement	100	advm	[pred="bonifacement_____1",clivee=+,detach_neg = +,cat=adv]	bonifacement_____1	Default		%default
bonnement	100	advm	[pred="bonnement_____1",clivee=+,cat=adv]	bonnement_____1	Default		%default
bordéliquement	100	advm	[pred="bordéliquement_____1",clivee=+,detach_neg = +,cat=adv]	bordéliquement_____1	Default		%default
botaniquement	100	advm	[pred="botaniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	botaniquement_____1	Default		%default
botaniquement	100	advm	[pred="botaniquement_____2",clivee=+,cat=adv]	botaniquement_____2	Default		%default
boudeusement	100	advm	[pred="boudeusement_____1",clivee=+,detach_neg = +,cat=adv]	boudeusement_____1	Default		%default
boueusement	100	advm	[pred="boueusement_____1",clivee=+,cat=adv]	boueusement_____1	Default		%default
bouffonnement	100	advm	[pred="bouffonnement_____1",clivee=+,detach_neg = +,cat=adv]	bouffonnement_____1	Default		%default
bougonnement	100	advm	[pred="bougonnement_____1",clivee=+,detach_neg = +,cat=adv]	bougonnement_____1	Default		%default
bougrement	100	advm	[pred="bougrement_____1",advi=combien,cat=adv]	bougrement_____1	Default		%default
boulimiquement	100	advm	[pred="boulimiquement_____1",clivee=+,detach_neg = +,cat=adv]	boulimiquement_____1	Default		%default
bourgeoisement	100	advm	[pred="bourgeoisement_____1",clivee=+,detach_neg = +,cat=adv]	bourgeoisement_____1	Default		%default
bourrument	100	advm	[pred="bourrument_____1",clivee=+,detach_neg = +,cat=adv]	bourrument_____1	Default		%default
bout à bout	100	adv	[pred="bout à bout_____1",cat=adv]	bout à bout_____1	Default		%default
bravachement	100	advm	[pred="bravachement_____1",clivee=+,detach_neg = +,cat=adv]	bravachement_____1	Default		%default
bravement	100	advm	[pred="bravement_____1",clivee=+,detach_neg = +,cat=adv]	bravement_____1	Default		%default
bredouilleusement	100	advm	[pred="bredouilleusement_____1",clivee=+,detach_neg = +,cat=adv]	bredouilleusement_____1	Default		%default
bref	100	adv	[pred="bref_____1",cat=adv]	bref_____1	Default		%default
brillamment	100	advm	[pred="brillamment_____1",clivee=+,detach_neg = +,cat=adv]	brillamment_____1	Default		%default
brièvement	100	advm	[pred="brièvement_____1",clivee=+,detach_neg = +,cat=adv]	brièvement_____1	Default		%default
brouillonnement	100	advm	[pred="brouillonnement_____1",clivee=+,detach_neg = +,cat=adv]	brouillonnement_____1	Default		%default
brumeusement	100	advm	[pred="brumeusement_____1",clivee=+,detach_neg = +,cat=adv]	brumeusement_____1	Default		%default
brusquement	100	advm	[pred="brusquement_____1",clivee=+,cat=adv]	brusquement_____1	Default		%default
brutalement	100	advm	[pred="brutalement_____1",clivee=+,detach_neg = +,cat=adv]	brutalement_____1	Default		%default
brutalement	100	advm	[pred="brutalement_____2",clivee=+,cat=adv]	brutalement_____2	Default		%default
bruyamment	100	advm	[pred="bruyamment_____1",clivee=+,detach_neg = +,cat=adv]	bruyamment_____1	Default		%default
bucoliquement	100	advm	[pred="bucoliquement_____1",clivee=+,cat=adv]	bucoliquement_____1	Default		%default
budgétairement	100	advm	[pred="budgétairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	budgétairement_____1	Default		%default
budgétairement	100	advm	[pred="budgétairement_____2",clivee=+,cat=adv]	budgétairement_____2	Default		%default
bureaucratiquement	100	advm	[pred="bureaucratiquement_____1",clivee=+,cat=adv]	bureaucratiquement_____1	Default		%default
burlesquement	100	advm	[pred="burlesquement_____1",clivee=+,detach_neg = +,cat=adv]	burlesquement_____1	Default		%default
byzantinement	100	advm	[pred="byzantinement_____1",clivee=+,detach_neg = +,cat=adv]	byzantinement_____1	Default		%default
béatement	100	advm	[pred="béatement_____1",clivee=+,detach_neg = +,cat=adv]	béatement_____1	Default		%default
bégueulement	100	advm	[pred="bégueulement_____1",clivee=+,detach_neg = +,cat=adv]	bégueulement_____1	Default		%default
bénignement	100	advm	[pred="bénignement_____1",clivee=+,detach_neg = +,cat=adv]	bénignement_____1	Default		%default
bénignement	100	advm	[pred="bénignement_____2",clivee=+,cat=adv]	bénignement_____2	Default		%default
bénéficiairement	100	advm	[pred="bénéficiairement_____1",clivee=+,cat=adv]	bénéficiairement_____1	Default		%default
bénéfiquement	100	advm	[pred="bénéfiquement_____1",clivee=+,cat=adv]	bénéfiquement_____1	Default		%default
bénévolement	100	advm	[pred="bénévolement_____1",clivee=+,detach_neg = +,cat=adv]	bénévolement_____1	Default		%default
béotiennement	100	advm	[pred="béotiennement_____1",clivee=+,detach_neg = +,cat=adv]	béotiennement_____1	Default		%default
bésef	100	adv	[pred="bésef_____1",cat=adv]	bésef_____1	Default		%default
bézef	100	adv	[pred="bézef_____1",cat=adv]	bézef_____1	Default		%default
bêtement	100	advm	[pred="bêtement_____1",clivee=+,detach_neg = +,cat=adv]	bêtement_____1	Default		%default
bêtement	100	advp	[pred="bêtement_____2",detach = +,detach_neg = +,cat=adv]	bêtement_____2	Default		%default
c'est pourquoi	100	advp	[pred="c'est pourquoi_____1",cat=adv]	c'est pourquoi_____1	Default		%default
c'est-à-dire	100	advp	[pred="c'est-à-dire_____1",cat=adv]	c'est-à-dire_____1	Default		%default
cabalistiquement	100	advm	[pred="cabalistiquement_____1",clivee=+,cat=adv]	cabalistiquement_____1	Default		%default
cabotinement	100	advm	[pred="cabotinement_____1",clivee=+,detach_neg = +,cat=adv]	cabotinement_____1	Default		%default
cachottièrement	100	advm	[pred="cachottièrement_____1",clivee=+,detach_neg = +,cat=adv]	cachottièrement_____1	Default		%default
cachément	100	advm	[pred="cachément_____1",clivee=+,cat=adv]	cachément_____1	Default		%default
cacophoniquement	100	advm	[pred="cacophoniquement_____1",clivee=+,cat=adv]	cacophoniquement_____1	Default		%default
cafardeusement	100	advm	[pred="cafardeusement_____1",clivee=+,detach_neg = +,cat=adv]	cafardeusement_____1	Default		%default
cafouilleusement	100	advm	[pred="cafouilleusement_____1",clivee=+,detach_neg = +,cat=adv]	cafouilleusement_____1	Default		%default
cahin caha	100	adv	[pred="cahin-caha_____1",cat=adv]	cahin-caha_____1	Default		%default
cahin-caha	100	adv	[pred="cahin-caha_____1",cat=adv]	cahin-caha_____1	Default		%default
cajoleusement	100	advm	[pred="cajoleusement_____1",clivee=+,detach_neg = +,cat=adv]	cajoleusement_____1	Default		%default
calamiteusement	100	advm	[pred="calamiteusement_____1",clivee=+,cat=adv]	calamiteusement_____1	Default		%default
calligraphiquement	100	advm	[pred="calligraphiquement_____1",clivee=+,cat=adv]	calligraphiquement_____1	Default		%default
calmement	100	advm	[pred="calmement_____1",clivee=+,detach_neg = +,cat=adv]	calmement_____1	Default		%default
calomnieusement	100	advm	[pred="calomnieusement_____1",clivee=+,detach_neg = +,cat=adv]	calomnieusement_____1	Default		%default
calorimétriquement	100	advm	[pred="calorimétriquement_____1",clivee=+,cat=adv]	calorimétriquement_____1	Default		%default
caloriquement	100	advm	[pred="caloriquement_____1",clivee=+,cat=adv]	caloriquement_____1	Default		%default
canaillement	100	advm	[pred="canaillement_____1",clivee=+,detach_neg = +,cat=adv]	canaillement_____1	Default		%default
candidement	100	advm	[pred="candidement_____1",clivee=+,detach_neg = +,cat=adv]	candidement_____1	Default		%default
canoniquement	100	advm	[pred="canoniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	canoniquement_____1	Default		%default
canoniquement	100	advm	[pred="canoniquement_____2",clivee=+,cat=adv]	canoniquement_____2	Default		%default
cantabile	100	adv	[pred="cantabile_____1",cat=adv]	cantabile_____1	Default		%default
capablement	100	advm	[pred="capablement_____1",clivee=+,detach_neg = +,cat=adv]	capablement_____1	Default		%default
capillairement	100	advm	[pred="capillairement_____1",clivee=+,cat=adv]	capillairement_____1	Default		%default
capitalement	100	advm	[pred="capitalement_____1",advi=combien,cat=adv]	capitalement_____1	Default		%default
capitulairement	100	advm	[pred="capitulairement_____1",clivee=+,cat=adv]	capitulairement_____1	Default		%default
capricieusement	100	advm	[pred="capricieusement_____1",clivee=+,detach_neg = +,cat=adv]	capricieusement_____1	Default		%default
captieusement	100	advm	[pred="captieusement_____1",clivee=+,detach_neg = +,cat=adv]	captieusement_____1	Default		%default
caractériellement	100	advm	[pred="caractériellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	caractériellement_____1	Default		%default
caractériellement	100	advm	[pred="caractériellement_____2",clivee=+,detach_neg = +,cat=adv]	caractériellement_____2	Default		%default
caractéristiquement	100	advm	[pred="caractéristiquement_____1",advi=combien,cat=adv]	caractéristiquement_____1	Default		%default
caractérologiquement	100	advm	[pred="caractérologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	caractérologiquement_____1	Default		%default
caractérologiquement	100	advm	[pred="caractérologiquement_____2",clivee=+,cat=adv]	caractérologiquement_____2	Default		%default
caricaturalement	100	advm	[pred="caricaturalement_____1",clivee=+,cat=adv]	caricaturalement_____1	Default		%default
carrément	100	advm	[pred="carrément_____1",clivee=+,cat=adv]	carrément_____1	Default		%default
cartographiquement	100	advm	[pred="cartographiquement_____1",clivee=+,cat=adv]	cartographiquement_____1	Default		%default
cartésiennement	100	advm	[pred="cartésiennement_____1",clivee=+,detach_neg = +,cat=adv]	cartésiennement_____1	Default		%default
casanièrement	100	advm	[pred="casanièrement_____1",clivee=+,detach_neg = +,cat=adv]	casanièrement_____1	Default		%default
cash	100	adv	[pred="cash_____1",cat=adv]	cash_____1	Default		%default
casuellement	100	advm	[pred="casuellement_____1",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	casuellement_____1	Default		%default
catalytiquement	100	advm	[pred="catalytiquement_____1",clivee=+,cat=adv]	catalytiquement_____1	Default		%default
catastrophiquement	100	advm	[pred="catastrophiquement_____1",advi=combien,clivee=+,cat=adv]	catastrophiquement_____1	Default		%default
catholiquement	100	advm	[pred="catholiquement_____1",clivee=+,cat=adv]	catholiquement_____1	Default		%default
catégoriellement	100	advm	[pred="catégoriellement_____1",clivee=+,cat=adv]	catégoriellement_____1	Default		%default
catégoriquement	100	advm	[pred="catégoriquement_____1",clivee=+,cat=adv]	catégoriquement_____1	Default		%default
causalement	100	advm	[pred="causalement_____1",clivee=+,cat=adv]	causalement_____1	Default		%default
caustiquement	100	advm	[pred="caustiquement_____1",clivee=+,detach_neg = +,cat=adv]	caustiquement_____1	Default		%default
cauteleusement	100	advm	[pred="cauteleusement_____1",clivee=+,detach_neg = +,cat=adv]	cauteleusement_____1	Default		%default
cavalièrement	100	advm	[pred="cavalièrement_____1",clivee=+,detach_neg = +,cat=adv]	cavalièrement_____1	Default		%default
caverneusement	100	advm	[pred="caverneusement_____1",clivee=+,cat=adv]	caverneusement_____1	Default		%default
ce faisant	100	adv	[pred="ce faisant_____1",chunk_type=NV,cat=adv]	ce faisant_____1	Default		%default
ceci dit	100	adv	[pred="ceci dit_____1",cat=adv]	ceci dit_____1	Default		%default
ceci étant dit	100	adv	[pred="ceci étant dit_____1",cat=adv]	ceci étant dit_____1	Default		%default
cela dit	100	adv	[pred="cela dit_____1",cat=adv]	cela dit_____1	Default		%default
cellulairement	100	advm	[pred="cellulairement_____1",clivee=+,cat=adv]	cellulairement_____1	Default		%default
censitairement	100	advm	[pred="censitairement_____1",clivee=+,cat=adv]	censitairement_____1	Default		%default
censément	100	advm	[pred="censément_____1",advi=combien,cat=adv]	censément_____1	Default		%default
centralement	100	advm	[pred="centralement_____1",clivee=+,cat=adv]	centralement_____1	Default		%default
cependant	100	adv	[pred="cependant_____1",cat=adv]	cependant_____1	Default		%default
certainement	100	advp	[pred="certainement_____1<Suj:(scompl)>",detach = +,detach_neg = +,modnc = +,cat=adv]	certainement_____1	Default		%default
certes	100	adv	[pred="certes_____1",cat=adv]	certes_____1	Default		%default
chafouinement	100	advm	[pred="chafouinement_____1",clivee=+,detach_neg = +,cat=adv]	chafouinement_____1	Default		%default
chagrinement	100	advm	[pred="chagrinement_____1",clivee=+,detach_neg = +,cat=adv]	chagrinement_____1	Default		%default
chaleureusement	100	advm	[pred="chaleureusement_____1",clivee=+,detach_neg = +,cat=adv]	chaleureusement_____1	Default		%default
chanceusement	100	advm	[pred="chanceusement_____1",clivee=+,detach_neg = +,cat=adv]	chanceusement_____1	Default		%default
chaotiquement	100	advm	[pred="chaotiquement_____1",clivee=+,cat=adv]	chaotiquement_____1	Default		%default
charismatiquement	100	advm	[pred="charismatiquement_____1",clivee=+,detach_neg = +,cat=adv]	charismatiquement_____1	Default		%default
charitablement	100	advm	[pred="charitablement_____1",clivee=+,detach_neg = +,cat=adv]	charitablement_____1	Default		%default
charitablement	100	advp	[pred="charitablement_____2",detach = +,detach_neg = +,cat=adv]	charitablement_____2	Default		%default
charnellement	100	advm	[pred="charnellement_____1",clivee=+,cat=adv]	charnellement_____1	Default		%default
chastement	100	advm	[pred="chastement_____1",clivee=+,detach_neg = +,cat=adv]	chastement_____1	Default		%default
chatouilleusement	100	advm	[pred="chatouilleusement_____1",clivee=+,detach_neg = +,cat=adv]	chatouilleusement_____1	Default		%default
chattement	100	advm	[pred="chattement_____1",clivee=+,detach_neg = +,cat=adv]	chattement_____1	Default		%default
chaud	100	adv	[pred="chaud_____1",cat=adv]	chaud_____1	Default		%default
chaudement	100	advm	[pred="chaudement_____1",clivee=+,detach_neg = +,cat=adv]	chaudement_____1	Default		%default
chaudement	100	advm	[pred="chaudement_____2",clivee=+,cat=adv]	chaudement_____2	Default		%default
chauvinement	100	advm	[pred="chauvinement_____1",clivee=+,detach_neg = +,cat=adv]	chauvinement_____1	Default		%default
chemin faisant	100	advp	[pred="chemin faisant_____1",cat=adv]	chemin faisant_____1	Default		%default
chenuement	100	advm	[pred="chenuement_____1",advi=combien,clivee=+,cat=adv]	chenuement_____1	Default		%default
cher	100	adv	[pred="cher_____1",cat=adv]	cher_____1	Default		%default
chevaleresquement	100	advm	[pred="chevaleresquement_____1",clivee=+,detach_neg = +,cat=adv]	chevaleresquement_____1	Default		%default
chicanièrement	100	advm	[pred="chicanièrement_____1",clivee=+,detach_neg = +,cat=adv]	chicanièrement_____1	Default		%default
chichement	100	advm	[pred="chichement_____1",advi=combien,clivee=+,cat=adv]	chichement_____1	Default		%default
chichement	100	advm	[pred="chichement_____2",clivee=+,cat=adv]	chichement_____2	Default		%default
chichiteusement	100	advm	[pred="chichiteusement_____1",clivee=+,detach_neg = +,cat=adv]	chichiteusement_____1	Default		%default
chiennement	100	advm	[pred="chiennement_____1",clivee=+,detach_neg = +,cat=adv]	chiennement_____1	Default		%default
chimiquement	100	advm	[pred="chimiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	chimiquement_____1	Default		%default
chimiquement	100	advm	[pred="chimiquement_____2",cat=adv]	chimiquement_____2	Default		%default
chimériquement	100	advm	[pred="chimériquement_____1",clivee=+,cat=adv]	chimériquement_____1	Default		%default
chinoisement	100	advm	[pred="chinoisement_____1",clivee=+,detach_neg = +,cat=adv]	chinoisement_____1	Default		%default
chipoteusement	100	advm	[pred="chipoteusement_____1",clivee=+,detach_neg = +,cat=adv]	chipoteusement_____1	Default		%default
chiquement	100	advm	[pred="chiquement_____1",clivee=+,detach_neg = +,cat=adv]	chiquement_____1	Default		%default
chirographairement	100	advm	[pred="chirographairement_____1",clivee=+,cat=adv]	chirographairement_____1	Default		%default
chirographiquement	100	advm	[pred="chirographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	chirographiquement_____1	Default		%default
chirographiquement	100	advm	[pred="chirographiquement_____2",clivee=+,cat=adv]	chirographiquement_____2	Default		%default
chirurgicalement	100	advm	[pred="chirurgicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	chirurgicalement_____1	Default		%default
chirurgicalement	100	advm	[pred="chirurgicalement_____2",clivee=+,cat=adv]	chirurgicalement_____2	Default		%default
chiément	100	advm	[pred="chiément_____1",advi=combien,cat=adv]	chiément_____1	Default		%default
chorégraphiquement	100	advm	[pred="chorégraphiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	chorégraphiquement_____1	Default		%default
chorégraphiquement	100	advm	[pred="chorégraphiquement_____2",clivee=+,cat=adv]	chorégraphiquement_____2	Default		%default
chouettement	100	advm	[pred="chouettement_____1",advi=combien,cat=adv]	chouettement_____1	Default		%default
chromatiquement	100	advm	[pred="chromatiquement_____1",clivee=+,cat=adv]	chromatiquement_____1	Default		%default
chroniquement	100	advm	[pred="chroniquement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	chroniquement_____1	Default		%default
chronologiquement	100	advm	[pred="chronologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	chronologiquement_____1	Default		%default
chronologiquement	100	advm	[pred="chronologiquement_____2",clivee=+,cat=adv]	chronologiquement_____2	Default		%default
chronométriquement	100	advm	[pred="chronométriquement_____1",clivee=+,cat=adv]	chronométriquement_____1	Default		%default
chrétiennement	100	advm	[pred="chrétiennement_____1",clivee=+,detach_neg = +,cat=adv]	chrétiennement_____1	Default		%default
chtou	100	adv	[pred="tout_____1",cat=adv]	tout_____1	Default		%default
chtoute	100	adv	[pred="tout_____1",cat=adv]	tout_____1	Default		%default
chèrement	100	advm	[pred="chèrement_____1",clivee=+,cat=adv]	chèrement_____1	Default		%default
chèrement	100	advm	[pred="chèrement_____2",clivee=+,cat=adv]	chèrement_____2	Default		%default
chétivement	100	advm	[pred="chétivement_____1",clivee=+,cat=adv]	chétivement_____1	Default		%default
ci-après	100	adv	[pred="ci-après_____1",cat=adv]	ci-après_____1	Default		%default
ci-contre	100	adv	[pred="ci-contre_____1",cat=adv]	ci-contre_____1	Default		%default
ci-dessous	100	adv	[pred="ci-dessous_____1",cat=adv]	ci-dessous_____1	Default		%default
ci-dessus	100	adv	[pred="ci-dessus_____1",cat=adv]	ci-dessus_____1	Default		%default
ci-gît	100	adv	[pred="ci-gît_____1",cat=adv]	ci-gît_____1	Default		%default
ci-inclus	100	adv	[pred="ci-inclus_____1",cat=adv]	ci-inclus_____1	Default		%default
ci-joint	100	adv	[pred="ci-joint_____1",cat=adv]	ci-joint_____1	Default		%default
cinquièmement	100	advp	[pred="cinquièmement_____1",detach = +,detach_neg = +,cat=adv]	cinquièmement_____1	Default		%default
cinématiquement	100	advm	[pred="cinématiquement_____1",clivee=+,cat=adv]	cinématiquement_____1	Default		%default
cinématographiquement	100	advm	[pred="cinématographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cinématographiquement_____1	Default		%default
cinématographiquement	100	advm	[pred="cinématographiquement_____2",clivee=+,cat=adv]	cinématographiquement_____2	Default		%default
cinétiquement	100	advm	[pred="cinétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cinétiquement_____1	Default		%default
cinétiquement	100	advm	[pred="cinétiquement_____2",clivee=+,cat=adv]	cinétiquement_____2	Default		%default
circonspectement	100	advm	[pred="circonspectement_____1",clivee=+,detach_neg = +,cat=adv]	circonspectement_____1	Default		%default
circulairement	100	advm	[pred="circulairement_____1",clivee=+,cat=adv]	circulairement_____1	Default		%default
citadinement	100	advm	[pred="citadinement_____1",clivee=+,detach_neg = +,cat=adv]	citadinement_____1	Default		%default
civilement	100	advm	[pred="civilement_____1",clivee=+,detach_neg = +,cat=adv]	civilement_____1	Default		%default
civilement	100	advm	[pred="civilement_____2",cat=adv]	civilement_____2	Default		%default
civiquement	100	advm	[pred="civiquement_____1",clivee=+,cat=adv]	civiquement_____1	Default		%default
clairement	100	advm	[pred="clairement_____1",clivee=+,detach_neg = +,cat=adv]	clairement_____1	Default		%default
clairement	100	advm	[pred="clairement_____2",clivee=+,cat=adv]	clairement_____2	Default		%default
clandestinement	100	advm	[pred="clandestinement_____1",clivee=+,cat=adv]	clandestinement_____1	Default		%default
classiquement	100	advm	[pred="classiquement_____1",clivee=+,detach_neg = +,cat=adv]	classiquement_____1	Default		%default
climatiquement	100	advm	[pred="climatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	climatiquement_____1	Default		%default
climatiquement	100	advm	[pred="climatiquement_____2",clivee=+,cat=adv]	climatiquement_____2	Default		%default
climatologiquement	100	advm	[pred="climatologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	climatologiquement_____1	Default		%default
climatologiquement	100	advm	[pred="climatologiquement_____2",clivee=+,cat=adv]	climatologiquement_____2	Default		%default
cliniquement	100	advm	[pred="cliniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cliniquement_____1	Default		%default
cliniquement	100	advm	[pred="cliniquement_____2",cat=adv]	cliniquement_____2	Default		%default
clopin-clopant	100	adv	[pred="clopin-clopant_____1",cat=adv]	clopin-clopant_____1	Default		%default
cléricalement	100	advm	[pred="cléricalement_____1",clivee=+,cat=adv]	cléricalement_____1	Default		%default
cocassement	100	advm	[pred="cocassement_____1",clivee=+,detach_neg = +,cat=adv]	cocassement_____1	Default		%default
cochonnement	100	advm	[pred="cochonnement_____1",clivee=+,detach_neg = +,cat=adv]	cochonnement_____1	Default		%default
cochonnément	100	advm	[pred="cochonnément_____1",clivee=+,detach_neg = +,cat=adv]	cochonnément_____1	Default		%default
coercitivement	100	advm	[pred="coercitivement_____1",clivee=+,detach_neg = +,cat=adv]	coercitivement_____1	Default		%default
coitement	100	advm	[pred="coitement_____1",clivee=+,detach_neg = +,cat=adv]	coitement_____1	Default		%default
collatéralement	100	advm	[pred="collatéralement_____1",clivee=+,cat=adv]	collatéralement_____1	Default		%default
collectivement	100	advm	[pred="collectivement_____1",clivee=+,cat=adv]	collectivement_____1	Default		%default
collusoirement	100	advm	[pred="collusoirement_____1",clivee=+,cat=adv]	collusoirement_____1	Default		%default
collégialement	100	advm	[pred="collégialement_____1",clivee=+,cat=adv]	collégialement_____1	Default		%default
colossalement	100	advm	[pred="colossalement_____1",advi=combien,cat=adv]	colossalement_____1	Default		%default
coléreusement	100	advm	[pred="coléreusement_____1",clivee=+,detach_neg = +,cat=adv]	coléreusement_____1	Default		%default
colériquement	100	advm	[pred="colériquement_____1",clivee=+,detach_neg = +,cat=adv]	colériquement_____1	Default		%default
comiquement	100	advm	[pred="comiquement_____1",clivee=+,detach_neg = +,cat=adv]	comiquement_____1	Default		%default
comme chien et chat	100	adv	[pred="comme chien et chat_____1",chunk_type=GP,cat=adv]	comme chien et chat_____1	Default		%default
comme de rien	100	advp	[pred="comme de rien_____1",cat=adv]	comme de rien_____1	Default		%default
comme pas deux	100	adv	[pred="comme pas deux_____1",chunk_type=GP,cat=adv]	comme pas deux_____1	Default		%default
comme un coq en pâte	100	adv	[pred="comme un coq en pâte_____1",chunk_type=GP,cat=adv]	comme un coq en pâte_____1	Default		%default
comme ça	100	adv	[pred="comme ça_____1",cat=adv]	comme ça_____1	Default		%default
commercialement	100	advm	[pred="commercialement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	commercialement_____1	Default		%default
commercialement	100	advm	[pred="commercialement_____2",clivee=+,cat=adv]	commercialement_____2	Default		%default
comminatoirement	100	advm	[pred="comminatoirement_____1",clivee=+,cat=adv]	comminatoirement_____1	Default		%default
commodément	100	advm	[pred="commodément_____1",clivee=+,cat=adv]	commodément_____1	Default		%default
communalement	100	advm	[pred="communalement_____1",clivee=+,cat=adv]	communalement_____1	Default		%default
communautairement	100	advm	[pred="communautairement_____1",clivee=+,cat=adv]	communautairement_____1	Default		%default
communément	100	advp	[pred="communément_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	communément_____1	Default		%default
comparablement	100	advm	[pred="comparablement_____1",clivee=+,cat=adv]	comparablement_____1	Default		%default
comparativement	100	advp	[pred="comparativement_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	comparativement_____1	Default		%default
compatiblement	100	advm	[pred="compatiblement_____1",clivee=+,cat=adv]	compatiblement_____1	Default		%default
compendieusement	100	advm	[pred="compendieusement_____1",clivee=+,detach_neg = +,cat=adv]	compendieusement_____1	Default		%default
compensativement	100	advm	[pred="compensativement_____1",clivee=+,cat=adv]	compensativement_____1	Default		%default
compensatoirement	100	advm	[pred="compensatoirement_____1",clivee=+,cat=adv]	compensatoirement_____1	Default		%default
complaisamment	100	advm	[pred="complaisamment_____1",clivee=+,detach_neg = +,cat=adv]	complaisamment_____1	Default		%default
complexement	100	advm	[pred="complexement_____1",clivee=+,cat=adv]	complexement_____1	Default		%default
complètement	100	advm	[pred="complètement_____1",advi=combien,cat=adv]	complètement_____1	Default		%default
complémentairement	100	advm	[pred="complémentairement_____1",clivee=+,cat=adv]	complémentairement_____1	Default		%default
componctueusement	100	advm	[pred="componctueusement_____1",clivee=+,detach_neg = +,cat=adv]	componctueusement_____1	Default		%default
compréhensivement	100	advm	[pred="compréhensivement_____1",clivee=+,detach_neg = +,cat=adv]	compréhensivement_____1	Default		%default
compréhensivement	100	advm	[pred="compréhensivement_____2",clivee=+,cat=adv]	compréhensivement_____2	Default		%default
comptablement	100	advm	[pred="comptablement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	comptablement_____1	Default		%default
comptant	100	adv	[pred="comptant_____1",cat=adv]	comptant_____1	Default		%default
compulsivement	100	advm	[pred="compulsivement_____1",clivee=+,cat=adv]	compulsivement_____1	Default		%default
concentriquement	100	advm	[pred="concentriquement_____1",clivee=+,cat=adv]	concentriquement_____1	Default		%default
conceptuellement	100	advm	[pred="conceptuellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	conceptuellement_____1	Default		%default
conceptuellement	100	advm	[pred="conceptuellement_____2",clivee=+,cat=adv]	conceptuellement_____2	Default		%default
concevablement	100	advm	[pred="concevablement_____1",clivee=+,cat=adv]	concevablement_____1	Default		%default
conciliairement	100	advm	[pred="conciliairement_____1",cat=adv]	conciliairement_____1	Default		%default
concisément	100	advm	[pred="concisément_____1",clivee=+,detach_neg = +,cat=adv]	concisément_____1	Default		%default
concomitamment	100	advp	[pred="concomitamment_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	concomitamment_____1	Default		%default
concrètement	100	advm	[pred="concrètement_____1",clivee=+,detach_neg = +,cat=adv]	concrètement_____1	Default		%default
concrètement	100	advp	[pred="concrètement_____2",detach = +,detach_neg = +,cat=adv]	concrètement_____2	Default		%default
concurremment	100	advm	[pred="concurremment_____1",clivee=+,cat=adv]	concurremment_____1	Default		%default
concussionnairement	100	advm	[pred="concussionnairement_____1",clivee=+,detach_neg = +,cat=adv]	concussionnairement_____1	Default		%default
condamnablement	100	advm	[pred="condamnablement_____1",clivee=+,detach_neg = +,cat=adv]	condamnablement_____1	Default		%default
conditionnellement	100	advm	[pred="conditionnellement_____1",clivee=+,cat=adv]	conditionnellement_____1	Default		%default
confer	100	adv	[pred="confer_____1",cat=adv]	confer_____1	Default		%default
confessionnellement	100	advm	[pred="confessionnellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	confessionnellement_____1	Default		%default
confidemment	100	advm	[pred="confidemment_____1",clivee=+,cat=adv]	confidemment_____1	Default		%default
confidentiellement	100	advm	[pred="confidentiellement_____1",clivee=+,cat=adv]	confidentiellement_____1	Default		%default
confidentiellement	100	advp	[pred="confidentiellement_____2",detach = +,detach_neg = +,cat=adv]	confidentiellement_____2	Default		%default
conflictuellement	100	advm	[pred="conflictuellement_____1",clivee=+,cat=adv]	conflictuellement_____1	Default		%default
conformément	100	advm	[pred="conformément_____1",clivee=+,cat=adv]	conformément_____1	Default		%default
conformément	100	advp	[pred="conformément_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	conformément_____2	Default		%default
confortablement	100	advm	[pred="confortablement_____1",clivee=+,cat=adv]	confortablement_____1	Default		%default
confraternellement	100	advm	[pred="confraternellement_____1",clivee=+,cat=adv]	confraternellement_____1	Default		%default
confusément	100	advm	[pred="confusément_____1",clivee=+,detach_neg = +,cat=adv]	confusément_____1	Default		%default
confusément	100	advm	[pred="confusément_____2",clivee=+,cat=adv]	confusément_____2	Default		%default
confédéralement	100	advm	[pred="confédéralement_____1",clivee=+,cat=adv]	confédéralement_____1	Default		%default
congrument	100	advm	[pred="congrument_____1",clivee=+,cat=adv]	congrument_____1	Default		%default
congrûment	100	advm	[pred="congrûment_____1",clivee=+,cat=adv]	congrûment_____1	Default		%default
congénitalement	100	advm	[pred="congénitalement_____1",advi=combien,cat=adv]	congénitalement_____1	Default		%default
coniquement	100	advm	[pred="coniquement_____1",clivee=+,cat=adv]	coniquement_____1	Default		%default
conjecturalement	100	advm	[pred="conjecturalement_____1",clivee=+,cat=adv]	conjecturalement_____1	Default		%default
conjointement	100	advm	[pred="conjointement_____1",clivee=+,cat=adv]	conjointement_____1	Default		%default
conjointement	100	advp	[pred="conjointement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	conjointement_____2	Default		%default
conjonctivement	100	advm	[pred="conjonctivement_____1",clivee=+,cat=adv]	conjonctivement_____1	Default		%default
conjoncturellement	100	advm	[pred="conjoncturellement_____1",clivee=+,cat=adv]	conjoncturellement_____1	Default		%default
conjugalement	100	advm	[pred="conjugalement_____1",clivee=+,cat=adv]	conjugalement_____1	Default		%default
connement	100	advm	[pred="connement_____1",clivee=+,detach_neg = +,cat=adv]	connement_____1	Default		%default
connement	100	advp	[pred="connement_____2",detach = +,detach_neg = +,cat=adv]	connement_____2	Default		%default
connotativement	100	advm	[pred="connotativement_____1",clivee=+,cat=adv]	connotativement_____1	Default		%default
consciemment	100	advm	[pred="consciemment_____1",clivee=+,detach_neg = +,cat=adv]	consciemment_____1	Default		%default
consciencieusement	100	advm	[pred="consciencieusement_____1",clivee=+,detach_neg = +,cat=adv]	consciencieusement_____1	Default		%default
consensuellement	100	advm	[pred="consensuellement_____1",clivee=+,cat=adv]	consensuellement_____1	Default		%default
conservatoirement	100	advm	[pred="conservatoirement_____1",clivee=+,cat=adv]	conservatoirement_____1	Default		%default
considérablement	100	advm	[pred="considérablement_____1",advi=combien,cat=adv]	considérablement_____1	Default		%default
considérément	100	advm	[pred="considérément_____1",clivee=+,detach_neg = +,cat=adv]	considérément_____1	Default		%default
consistorialement	100	advm	[pred="consistorialement_____1",clivee=+,cat=adv]	consistorialement_____1	Default		%default
constamment	100	advm	[pred="constamment_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	constamment_____1	Default		%default
constitutionnellement	100	advm	[pred="constitutionnellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	constitutionnellement_____1	Default		%default
constitutionnellement	100	advm	[pred="constitutionnellement_____2",clivee=+,cat=adv]	constitutionnellement_____2	Default		%default
constitutivement	100	advm	[pred="constitutivement_____1",clivee=+,cat=adv]	constitutivement_____1	Default		%default
consubstantiellement	100	advm	[pred="consubstantiellement_____1",clivee=+,cat=adv]	consubstantiellement_____1	Default		%default
consulairement	100	advm	[pred="consulairement_____1",clivee=+,cat=adv]	consulairement_____1	Default		%default
consécutivement	100	advm	[pred="consécutivement_____1",clivee=+,cat=adv]	consécutivement_____1	Default		%default
consécutivement	100	advp	[pred="consécutivement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	consécutivement_____2	Default		%default
conséquemment	100	advm	[pred="conséquemment_____1",clivee=+,detach_neg = +,cat=adv]	conséquemment_____1	Default		%default
conséquemment	100	advp	[pred="conséquemment_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	conséquemment_____2	Default		%default
contagieusement	100	advm	[pred="contagieusement_____1",clivee=+,cat=adv]	contagieusement_____1	Default		%default
contemplativement	100	advm	[pred="contemplativement_____1",clivee=+,detach_neg = +,cat=adv]	contemplativement_____1	Default		%default
contemporainement	100	advm	[pred="contemporainement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	contemporainement_____1	Default		%default
contentieusement	100	advm	[pred="contentieusement_____1",clivee=+,cat=adv]	contentieusement_____1	Default		%default
contestablement	100	advm	[pred="contestablement_____1",clivee=+,cat=adv]	contestablement_____1	Default		%default
contextuellement	100	advm	[pred="contextuellement_____1",clivee=+,cat=adv]	contextuellement_____1	Default		%default
continuellement	100	advm	[pred="continuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	continuellement_____1	Default		%default
continûment	100	advm	[pred="continûment_____1",clivee=+,cat=adv]	continûment_____1	Default		%default
contractuellement	100	advm	[pred="contractuellement_____1",clivee=+,cat=adv]	contractuellement_____1	Default		%default
contradictoirement	100	advm	[pred="contradictoirement_____1",clivee=+,cat=adv]	contradictoirement_____1	Default		%default
contrairement	100	advm	[pred="contrairement_____1",clivee=+,cat=adv]	contrairement_____1	Default		%default
contrairement	100	advp	[pred="contrairement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	contrairement_____2	Default		%default
contrapuntictement	100	advm	[pred="contrapuntictement_____1",clivee=+,cat=adv]	contrapuntictement_____1	Default		%default
contre	100	adv	[pred="contre_____1",cat=adv]	contre_____1	Default		%default
contributoirement	100	advm	[pred="contributoirement_____1",clivee=+,cat=adv]	contributoirement_____1	Default		%default
contumélieusement	100	advm	[pred="contumélieusement_____1",clivee=+,cat=adv]	contumélieusement_____1	Default		%default
convenablement	100	advm	[pred="convenablement_____1",advi=combien,clivee=+,cat=adv]	convenablement_____1	Default		%default
conventionnellement	100	advm	[pred="conventionnellement_____1",clivee=+,cat=adv]	conventionnellement_____1	Default		%default
conventuellement	100	advm	[pred="conventuellement_____1",clivee=+,cat=adv]	conventuellement_____1	Default		%default
convivialement	100	advm	[pred="convivialement_____1",clivee=+,cat=adv]	convivialement_____1	Default		%default
convulsivement	100	advm	[pred="convulsivement_____1",clivee=+,cat=adv]	convulsivement_____1	Default		%default
coopérativement	100	advm	[pred="coopérativement_____1",clivee=+,cat=adv]	coopérativement_____1	Default		%default
copieusement	100	advm	[pred="copieusement_____1",advi=combien,cat=adv]	copieusement_____1	Default		%default
coquettement	100	advm	[pred="coquettement_____1",clivee=+,detach_neg = +,cat=adv]	coquettement_____1	Default		%default
coquinement	100	advm	[pred="coquinement_____1",clivee=+,detach_neg = +,cat=adv]	coquinement_____1	Default		%default
cordialement	100	advm	[pred="cordialement_____1",clivee=+,detach_neg = +,cat=adv]	cordialement_____1	Default		%default
coriacement	100	advm	[pred="coriacement_____1",clivee=+,detach_neg = +,cat=adv]	coriacement_____1	Default		%default
corollairement	100	advp	[pred="corollairement_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	corollairement_____1	Default		%default
corporativement	100	advm	[pred="corporativement_____1",clivee=+,cat=adv]	corporativement_____1	Default		%default
corporellement	100	advm	[pred="corporellement_____1",cat=adv]	corporellement_____1	Default		%default
corpusculairement	100	advm	[pred="corpusculairement_____1",clivee=+,cat=adv]	corpusculairement_____1	Default		%default
correct	100	adv	[pred="correct_____1",cat=adv]	correct_____1	Default		%default
correctement	100	advm	[pred="correctement_____1",clivee=+,cat=adv]	correctement_____1	Default		%default
correctionnellement	100	advm	[pred="correctionnellement_____1",clivee=+,cat=adv]	correctionnellement_____1	Default		%default
corrosivement	100	advm	[pred="corrosivement_____1",clivee=+,detach_neg = +,cat=adv]	corrosivement_____1	Default		%default
corrélativement	100	advm	[pred="corrélativement_____1",clivee=+,cat=adv]	corrélativement_____1	Default		%default
corrélativement	100	advp	[pred="corrélativement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	corrélativement_____2	Default		%default
cosmiquement	100	advm	[pred="cosmiquement_____1",clivee=+,cat=adv]	cosmiquement_____1	Default		%default
cosmogoniquement	100	advm	[pred="cosmogoniquement_____1",clivee=+,cat=adv]	cosmogoniquement_____1	Default		%default
cosmographiquement	100	advm	[pred="cosmographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cosmographiquement_____1	Default		%default
cosmographiquement	100	advm	[pred="cosmographiquement_____2",clivee=+,cat=adv]	cosmographiquement_____2	Default		%default
cosmologiquement	100	advm	[pred="cosmologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cosmologiquement_____1	Default		%default
cosmologiquement	100	advm	[pred="cosmologiquement_____2",clivee=+,cat=adv]	cosmologiquement_____2	Default		%default
cossument	100	advm	[pred="cossument_____1",clivee=+,cat=adv]	cossument_____1	Default		%default
cotonneusement	100	advm	[pred="cotonneusement_____1",clivee=+,cat=adv]	cotonneusement_____1	Default		%default
couardement	100	advm	[pred="couardement_____1",clivee=+,detach_neg = +,cat=adv]	couardement_____1	Default		%default
couci-couci	100	advp	[pred="couci-couci_____1",cat=adv]	couci-couci_____1	Default		%default
couci-couça	100	adv	[pred="couci-couça_____1",cat=adv]	couci-couça_____1	Default		%default
coude à coude	100	advp	[pred="coude à coude_____1",cat=adv]	coude à coude_____1	Default		%default
coulamment	100	advm	[pred="coulamment_____1",clivee=+,cat=adv]	coulamment_____1	Default		%default
coup sur coup	100	advp	[pred="coup sur coup_____1",cat=adv]	coup sur coup_____1	Default		%default
coupablement	100	advm	[pred="coupablement_____1",clivee=+,detach_neg = +,cat=adv]	coupablement_____1	Default		%default
courageusement	100	advm	[pred="courageusement_____1",clivee=+,detach_neg = +,cat=adv]	courageusement_____1	Default		%default
courageusement	100	advp	[pred="courageusement_____2",detach = +,detach_neg = +,cat=adv]	courageusement_____2	Default		%default
couramment	100	advm	[pred="couramment_____1",clivee=+,cat=adv]	couramment_____1	Default		%default
couramment	100	advm	[pred="couramment_____2",clivee=+,cat=adv]	couramment_____2	Default		%default
court	100	adv	[pred="court_____1",cat=adv]	court_____1	Default		%default
courtement	100	advm	[pred="courtement_____1",clivee=+,cat=adv]	courtement_____1	Default		%default
courtoisement	100	advm	[pred="courtoisement_____1",clivee=+,detach_neg = +,cat=adv]	courtoisement_____1	Default		%default
coutumièrement	100	advp	[pred="coutumièrement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	coutumièrement_____1	Default		%default
coéternellement	100	advm	[pred="coéternellement_____1",advi=en_combien_de_temps,clivee=+,detach_neg = +,cat=adv]	coéternellement_____1	Default		%default
coûte que coûte	100	advp	[pred="coûte que coûte_____1",cat=adv]	coûte que coûte_____1	Default		%default
coûteusement	100	advm	[pred="coûteusement_____1",clivee=+,cat=adv]	coûteusement_____1	Default		%default
cpdt	100	adv	[pred="cependant_____1",cat=adv]	cependant_____1	Default		%default
craintivement	100	advm	[pred="craintivement_____1",clivee=+,detach_neg = +,cat=adv]	craintivement_____1	Default		%default
crapuleusement	100	advm	[pred="crapuleusement_____1",clivee=+,detach_neg = +,cat=adv]	crapuleusement_____1	Default		%default
crescendo	100	adv	[pred="crescendo_____1",cat=adv]	crescendo_____1	Default		%default
criardement	100	advm	[pred="criardement_____1",clivee=+,cat=adv]	criardement_____1	Default		%default
criminellement	100	advm	[pred="criminellement_____1",clivee=+,detach_neg = +,cat=adv]	criminellement_____1	Default		%default
criminellement	100	advm	[pred="criminellement_____2",clivee=+,cat=adv]	criminellement_____2	Default		%default
criminellement	100	advp	[pred="criminellement_____3",detach = +,detach_neg = +,cat=adv]	criminellement_____3	Default		%default
cristallographiquement	100	advm	[pred="cristallographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cristallographiquement_____1	Default		%default
cristallographiquement	100	advm	[pred="cristallographiquement_____2",clivee=+,cat=adv]	cristallographiquement_____2	Default		%default
critiquablement	100	advm	[pred="critiquablement_____1",clivee=+,detach_neg = +,cat=adv]	critiquablement_____1	Default		%default
critiquement	100	advm	[pred="critiquement_____1",clivee=+,detach_neg = +,cat=adv]	critiquement_____1	Default		%default
crucialement	100	advm	[pred="crucialement_____1",cat=adv]	crucialement_____1	Default		%default
cruellement	100	advm	[pred="cruellement_____1",advi=combien,clivee=+,cat=adv]	cruellement_____1	Default		%default
cruellement	100	advm	[pred="cruellement_____2",clivee=+,detach_neg = +,cat=adv]	cruellement_____2	Default		%default
cruellement	100	advm	[pred="cruellement_____3",clivee=+,cat=adv]	cruellement_____3	Default		%default
cruellement	100	advp	[pred="cruellement_____4",detach = +,detach_neg = +,cat=adv]	cruellement_____4	Default		%default
cryptographiquement	100	advm	[pred="cryptographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cryptographiquement_____1	Default		%default
cryptographiquement	100	advm	[pred="cryptographiquement_____2",clivee=+,cat=adv]	cryptographiquement_____2	Default		%default
crânement	100	advm	[pred="crânement_____1",clivee=+,detach_neg = +,cat=adv]	crânement_____1	Default		%default
crédiblement	100	advm	[pred="crédiblement_____1",clivee=+,detach_neg = +,cat=adv]	crédiblement_____1	Default		%default
crédulement	100	advm	[pred="crédulement_____1",clivee=+,detach_neg = +,cat=adv]	crédulement_____1	Default		%default
crépusculairement	100	advm	[pred="crépusculairement_____1",clivee=+,cat=adv]	crépusculairement_____1	Default		%default
crétinement	100	advm	[pred="crétinement_____1",clivee=+,detach_neg = +,cat=adv]	crétinement_____1	Default		%default
crûment	100	advm	[pred="crûment_____1",clivee=+,detach_neg = +,cat=adv]	crûment_____1	Default		%default
cuisamment	100	advm	[pred="cuisamment_____1",advi=combien,cat=adv]	cuisamment_____1	Default		%default
cuistrement	100	advm	[pred="cuistrement_____1",clivee=+,detach_neg = +,cat=adv]	cuistrement_____1	Default		%default
culinairement	100	advm	[pred="culinairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	culinairement_____1	Default		%default
culinairement	100	advm	[pred="culinairement_____2",clivee=+,cat=adv]	culinairement_____2	Default		%default
cultuellement	100	advm	[pred="cultuellement_____1",clivee=+,cat=adv]	cultuellement_____1	Default		%default
culturellement	100	advm	[pred="culturellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	culturellement_____1	Default		%default
culturellement	100	advm	[pred="culturellement_____2",clivee=+,cat=adv]	culturellement_____2	Default		%default
cumulativement	100	advm	[pred="cumulativement_____1",clivee=+,cat=adv]	cumulativement_____1	Default		%default
cupidement	100	advm	[pred="cupidement_____1",clivee=+,detach_neg = +,cat=adv]	cupidement_____1	Default		%default
curativement	100	advm	[pred="curativement_____1",clivee=+,cat=adv]	curativement_____1	Default		%default
curieusement	100	advm	[pred="curieusement_____1",clivee=+,detach_neg = +,cat=adv]	curieusement_____1	Default		%default
curieusement	100	advm	[pred="curieusement_____2",clivee=+,cat=adv]	curieusement_____2	Default		%default
curieusement	100	advp	[pred="curieusement_____3",detach = +,detach_neg = +,cat=adv]	curieusement_____3	Default		%default
cursivement	100	advm	[pred="cursivement_____1",clivee=+,cat=adv]	cursivement_____1	Default		%default
cybernétiquement	100	advm	[pred="cybernétiquement_____1",clivee=+,cat=adv]	cybernétiquement_____1	Default		%default
cycliquement	100	advm	[pred="cycliquement_____1",clivee=+,cat=adv]	cycliquement_____1	Default		%default
cylindriquement	100	advm	[pred="cylindriquement_____1",clivee=+,cat=adv]	cylindriquement_____1	Default		%default
cyniquement	100	advm	[pred="cyniquement_____1",clivee=+,detach_neg = +,cat=adv]	cyniquement_____1	Default		%default
cytogéniquement	100	advm	[pred="cytogéniquement_____1",clivee=+,cat=adv]	cytogéniquement_____1	Default		%default
cytogénétiquement	100	advm	[pred="cytogénétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cytogénétiquement_____1	Default		%default
cytologiquement	100	advm	[pred="cytologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cytologiquement_____1	Default		%default
cytologiquement	100	advm	[pred="cytologiquement_____2",clivee=+,cat=adv]	cytologiquement_____2	Default		%default
câlinement	100	advm	[pred="câlinement_____1",clivee=+,detach_neg = +,cat=adv]	câlinement_____1	Default		%default
céans	100	adv	[pred="céans_____1",cat=adv]	céans_____1	Default		%default
célestement	100	advm	[pred="célestement_____1",advi=combien,cat=adv]	célestement_____1	Default		%default
célibatairement	100	advm	[pred="célibatairement_____1",clivee=+,detach_neg = +,cat=adv]	célibatairement_____1	Default		%default
cérébralement	100	advm	[pred="cérébralement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	cérébralement_____1	Default		%default
cérébralement	100	advm	[pred="cérébralement_____2",clivee=+,cat=adv]	cérébralement_____2	Default		%default
cérémoniellement	100	advm	[pred="cérémoniellement_____1",clivee=+,cat=adv]	cérémoniellement_____1	Default		%default
cérémonieusement	100	advm	[pred="cérémonieusement_____1",clivee=+,detach_neg = +,cat=adv]	cérémonieusement_____1	Default		%default
d'abord	100	adv	[pred="d'abord_____1",cat=adv]	d'abord_____1	Default		%default
d'abord (...ensuite)	100	adv	[pred="d'abord (...ensuite)_____1",cat=adv]	d'abord (...ensuite)_____1	Default		%default
d'ailleurs	100	adv	[pred="d'ailleurs_____1",cat=adv]	d'ailleurs_____1	Default		%default
d'arrache pied	100	advp	[pred="d'arrache pied_____1",chunk_type=GN,cat=adv]	d'arrache pied_____1	Default		%default
d'assez près	100	adv	[pred="d'assez près_____1",cat=adv]	d'assez près_____1	Default		%default
d'autant	100	adv	[pred="d'autant_____1",cat=adv]	d'autant_____1	Default		%default
d'autant moins	100	adv	[pred="d'autant moins_____1",cat=adv]	d'autant moins_____1	Default		%default
d'autant plus	100	adv	[pred="d'autant plus_____1",cat=adv]	d'autant plus_____1	Default		%default
d'autre part	100	adv	[pred="d'autre part_____1",cat=adv]	d'autre part_____1	Default		%default
d'avance	100	adv	[pred="d'avance_____1",cat=adv]	d'avance_____1	Default		%default
d'emblée	100	adv	[pred="d'emblée_____1",cat=adv]	d'emblée_____1	Default		%default
d'ici là	100	adv	[pred="d'ici là_____1",cat=adv]	d'ici là_____1	Default		%default
d'ores et déjà	100	adv	[pred="d'ores et déjà_____1",cat=adv]	d'ores et déjà_____1	Default		%default
d'un autre côté	100	adv	[pred="d'un autre côté_____1",cat=adv]	d'un autre côté_____1	Default		%default
d'un coup	100	adv	[pred="d'un coup_____1",cat=adv]	d'un coup_____1	Default		%default
d'un côté (...d'un autre côté)	100	adv	[pred="d'un côté (...d'un autre côté)_____1",cat=adv]	d'un côté (...d'un autre côté)_____1	Default		%default
d'un seul coup	100	adv	[pred="d'un seul coup_____1",cat=adv]	d'un seul coup_____1	Default		%default
d'une façon générale	100	adv	[pred="d'une façon générale_____1",cat=adv]	d'une façon générale_____1	Default		%default
d'une part	100	adv	[pred="d'une part_____1",cat=adv]	d'une part_____1	Default		%default
d'une part (...d'autre part)	100	adv	[pred="d'une part (...d'autre part)_____1",cat=adv]	d'une part (...d'autre part)_____1	Default		%default
d'âge en âge	100	adv	[pred="d'âge en âge_____1",chunk_type=GP,cat=adv]	d'âge en âge_____1	Default		%default
dactylographiquement	100	advm	[pred="dactylographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	dactylographiquement_____1	Default		%default
damnablement	100	advm	[pred="damnablement_____1",clivee=+,detach_neg = +,cat=adv]	damnablement_____1	Default		%default
dangereusement	100	advm	[pred="dangereusement_____1",advi=combien,clivee=+,cat=adv]	dangereusement_____1	Default		%default
dangereusement	100	advm	[pred="dangereusement_____2",clivee=+,cat=adv]	dangereusement_____2	Default		%default
dans ce cas	100	adv	[pred="dans ce cas_____1",cat=adv]	dans ce cas_____1	Default		%default
dans ce cas-là	100	adv	[pred="dans ce cas-là_____1",cat=adv]	dans ce cas-là_____1	Default		%default
dans l'immédiat	100	adv	[pred="dans l'immédiat_____1",chunk_type=GP,cat=adv]	dans l'immédiat_____1	Default		%default
dans le coup	100	adv	[pred="dans le coup_____1",cat=adv]	dans le coup_____1	Default		%default
dans le même temps	100	adv	[pred="dans le même temps_____1",chunk_type=GP,cat=adv]	dans le même temps_____1	Default		%default
dans tous les cas	100	adv	[pred="dans tous les cas_____1",cat=adv]	dans tous les cas_____1	Default		%default
dare dare	100	adv	[pred="dare dare_____1",cat=adv]	dare dare_____1	Default		%default
dare-dare	100	adv	[pred="dare dare_____1",cat=adv]	dare dare_____1	Default		%default
davantage	100	adv	[pred="davantage_____1",cat=adv]	davantage_____1	Default		%default
de A à Z	100	adv	[pred="de A à Z_____1",chunk_type=GP,cat=adv]	de A à Z_____1	Default		%default
de auditu	100	adv	[pred="de auditu_____1",cat=adv]	de auditu_____1	Default		%default
de beaucoup trop près	100	adv	[pred="de beaucoup trop près_____1",cat=adv]	de beaucoup trop près_____1	Default		%default
de but en blanc	100	adv	[pred="de but en blanc_____1",chunk_type=GP,cat=adv]	de but en blanc_____1	Default		%default
de ce fait	100	adv	[pred="de ce fait_____1",cat=adv]	de ce fait_____1	Default		%default
de ce pas	100	adv	[pred="de ce pas_____1",cat=adv]	de ce pas_____1	Default		%default
de ce pas	100	adv	[pred="de ce pas_____2",chunk_type=GP,cat=adv]	de ce pas_____2	Default		%default
de commodo	100	adv	[pred="de commodo_____1",cat=adv]	de commodo_____1	Default		%default
de conserve	100	adv	[pred="de conserve_____1",chunk_type=GP,cat=adv]	de conserve_____1	Default		%default
de cujus	100	adv	[pred="de cujus_____1",cat=adv]	de cujus_____1	Default		%default
de côté	100	adv	[pred="de côté_____1",cat=adv]	de côté_____1	Default		%default
de facto	100	adv	[pred="de facto_____1",cat=adv]	de facto_____1	Default		%default
de fait	100	adv	[pred="de fait_____1",cat=adv]	de fait_____1	Default		%default
de fil en aiguille	100	adv	[pred="de fil en aiguille_____1",chunk_type=GP,cat=adv]	de fil en aiguille_____1	Default		%default
de force	100	adv	[pred="de force_____1",cat=adv]	de force_____1	Default		%default
de front	100	adv	[pred="de front_____1",cat=adv]	de front_____1	Default		%default
de gustibus	100	adv	[pred="de gustibus_____1",cat=adv]	de gustibus_____1	Default		%default
de jure	100	adv	[pred="de jure_____1",cat=adv]	de jure_____1	Default		%default
de la main à la main	100	adv	[pred="de la main à la main_____1",chunk_type=GP,cat=adv]	de la main à la main_____1	Default		%default
de la même façon	100	adv	[pred="de la même façon_____1",cat=adv]	de la même façon_____1	Default		%default
de la même manière	100	adv	[pred="de la même manière_____1",cat=adv]	de la même manière_____1	Default		%default
de la sorte	100	adv	[pred="de la sorte_____1",cat=adv]	de la sorte_____1	Default		%default
de loin	100	adv	[pred="de loin_____1",cat=adv]	de loin_____1	Default		%default
de loin en loin	100	adv	[pred="de loin en loin_____1",cat=adv]	de loin en loin_____1	Default		%default
de moins	100	adv	[pred="de moins_____1",chunk_type=GP,cat=adv]	de moins_____1	Default		%default
de moins en moins	100	adv	[pred="de moins en moins_____1",cat=adv]	de moins en moins_____1	Default		%default
de moitié	100	adv	[pred="de moitié_____1",cat=adv]	de moitié_____1	Default		%default
de même	100	adv	[pred="de même_____1",cat=adv]	de même_____1	Default		%default
de nouveau	100	adv	[pred="de nouveau_____1",cat=adv]	de nouveau_____1	Default		%default
de part en part	100	adv	[pred="de part en part_____1",cat=adv]	de part en part_____1	Default		%default
de part et d'autre	100	adv	[pred="de part et d'autre_____1",cat=adv]	de part et d'autre_____1	Default		%default
de plano	100	adv	[pred="de plano_____1",cat=adv]	de plano_____1	Default		%default
de plus	100	adv	[pred="de plus_____1",cat=adv]	de plus_____1	Default		%default
de plus belle	100	adv	[pred="de plus belle_____1",cat=adv]	de plus belle_____1	Default		%default
de plus en plus	100	adv	[pred="de plus en plus_____1",cat=adv]	de plus en plus_____1	Default		%default
de plus près	100	adv	[pred="de plus près_____1",cat=adv]	de plus près_____1	Default		%default
de prime abord	100	adv	[pred="de prime abord_____1",cat=adv]	de prime abord_____1	Default		%default
de proche en proche	100	adv	[pred="de proche en proche_____1",chunk_type=GP,cat=adv]	de proche en proche_____1	Default		%default
de profundis	100	adv	[pred="de profundis_____1",cat=adv]	de profundis_____1	Default		%default
de près	100	adv	[pred="de près_____1",cat=adv]	de près_____1	Default		%default
de suite	100	adv	[pred="de suite_____1",cat=adv]	de suite_____1	Default		%default
de surcroît	100	adv	[pred="de surcroît_____1",cat=adv]	de surcroît_____1	Default		%default
de temps à autre	100	adv	[pred="de temps à autre_____1",cat=adv]	de temps à autre_____1	Default		%default
de toute façon	100	adv	[pred="de toute façon_____1",cat=adv]	de toute façon_____1	Default		%default
de toute manière	100	adv	[pred="de toute manière_____1",cat=adv]	de toute manière_____1	Default		%default
de toutes façons	100	adv	[pred="de toutes façons_____1",cat=adv]	de toutes façons_____1	Default		%default
de toutes parts	100	adv	[pred="de toutes parts_____1",cat=adv]	de toutes parts_____1	Default		%default
de trop	100	adv	[pred="de trop_____1",adv_kind=modnc,cat=adv]	de trop_____1	Default		%default
de trop	100	adv	[pred="de trop_____2",cat=adv]	de trop_____2	Default		%default
de trop près	100	adv	[pred="de trop près_____1",cat=adv]	de trop près_____1	Default		%default
de très près	100	adv	[pred="de très près_____1",cat=adv]	de très près_____1	Default		%default
de très très près	100	adv	[pred="de très très près_____1",cat=adv]	de très très près_____1	Default		%default
de tête	100	adv	[pred="de tête_____1",chunk_type=GP,cat=adv]	de tête_____1	Default		%default
de visu	100	adv	[pred="de visu_____1",cat=adv]	de visu_____1	Default		%default
de vive voix	100	adv	[pred="de vive voix_____1",chunk_type=GP,cat=adv]	de vive voix_____1	Default		%default
debout	100	adv	[pred="debout_____1",cat=adv]	debout_____1	Default		%default
dedans	100	adv	[pred="dedans_____1",cat=adv]	dedans_____1	Default		%default
dehors	100	adv	[pred="dehors_____1",cat=adv]	dehors_____1	Default		%default
demain	100	adv	[pred="demain_____1",cat=adv]	demain_____1	Default		%default
densément	100	advm	[pred="densément_____1",clivee=+,cat=adv]	densément_____1	Default		%default
deo gratias	100	adv	[pred="deo gratias_____1",cat=adv]	deo gratias_____1	Default		%default
depuis	100	adv	[pred="depuis_____1",cat=adv]	depuis_____1	Default		%default
depuis beaucoup trop longtemps	100	adv	[pred="depuis beaucoup trop longtemps_____1",cat=adv]	depuis beaucoup trop longtemps_____1	Default		%default
depuis longtemps	100	adv	[pred="depuis longtemps_____1",cat=adv]	depuis longtemps_____1	Default		%default
depuis trop longtemps	100	adv	[pred="depuis trop longtemps_____1",cat=adv]	depuis trop longtemps_____1	Default		%default
depuis très longtemps	100	adv	[pred="depuis très longtemps_____1",cat=adv]	depuis très longtemps_____1	Default		%default
depuis très très longtemps	100	adv	[pred="depuis très très longtemps_____1",cat=adv]	depuis très très longtemps_____1	Default		%default
derechef	100	adv	[pred="derechef_____1",cat=adv]	derechef_____1	Default		%default
dermatologiquement	100	advm	[pred="dermatologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	dermatologiquement_____1	Default		%default
dernièrement	100	advm	[pred="dernièrement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	dernièrement_____1	Default		%default
derrière	100	adv	[pred="derrière_____1",cat=adv]	derrière_____1	Default		%default
descriptivement	100	advm	[pred="descriptivement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	descriptivement_____1	Default		%default
descriptivement	100	advm	[pred="descriptivement_____2",clivee=+,cat=adv]	descriptivement_____2	Default		%default
despotiquement	100	advm	[pred="despotiquement_____1",clivee=+,detach_neg = +,cat=adv]	despotiquement_____1	Default		%default
dessous	100	adv	[pred="dessous_____1",cat=adv]	dessous_____1	Default		%default
dessus	100	adv	[pred="dessus_____1",cat=adv]	dessus_____1	Default		%default
deux à deux	100	adv	[pred="deux à deux_____1",cat=adv]	deux à deux_____1	Default		%default
deuxièmement	100	advp	[pred="deuxièmement_____1",detach = +,detach_neg = +,cat=adv]	deuxièmement_____1	Default		%default
devant	100	adv	[pred="devant_____1",cat=adv]	devant_____1	Default		%default
dextrement	100	advm	[pred="dextrement_____1",clivee=+,detach_neg = +,cat=adv]	dextrement_____1	Default		%default
dextrorsum	100	adv	[pred="dextrorsum_____1",cat=adv]	dextrorsum_____1	Default		%default
diablement	100	advm	[pred="diablement_____1",advi=combien,cat=adv]	diablement_____1	Default		%default
diaboliquement	100	advm	[pred="diaboliquement_____1",advi=combien,clivee=+,cat=adv]	diaboliquement_____1	Default		%default
diachroniquement	100	advm	[pred="diachroniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	diachroniquement_____1	Default		%default
diachroniquement	100	advm	[pred="diachroniquement_____2",clivee=+,cat=adv]	diachroniquement_____2	Default		%default
diagonalement	100	advm	[pred="diagonalement_____1",clivee=+,cat=adv]	diagonalement_____1	Default		%default
dialectalement	100	advm	[pred="dialectalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	dialectalement_____1	Default		%default
dialectalement	100	advm	[pred="dialectalement_____2",clivee=+,cat=adv]	dialectalement_____2	Default		%default
dialectiquement	100	advm	[pred="dialectiquement_____1",clivee=+,cat=adv]	dialectiquement_____1	Default		%default
dialectologiquement	100	advm	[pred="dialectologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	dialectologiquement_____1	Default		%default
dialectologiquement	100	advm	[pred="dialectologiquement_____2",clivee=+,cat=adv]	dialectologiquement_____2	Default		%default
dialogiquement	100	advm	[pred="dialogiquement_____1",clivee=+,cat=adv]	dialogiquement_____1	Default		%default
diamétralement	100	advm	[pred="diamétralement_____1",advi=combien,cat=adv]	diamétralement_____1	Default		%default
diantrement	100	advm	[pred="diantrement_____1",advi=combien,cat=adv]	diantrement_____1	Default		%default
diatoniquement	100	advm	[pred="diatoniquement_____1",clivee=+,cat=adv]	diatoniquement_____1	Default		%default
dichotomiquement	100	advm	[pred="dichotomiquement_____1",clivee=+,cat=adv]	dichotomiquement_____1	Default		%default
dictatorialement	100	advm	[pred="dictatorialement_____1",clivee=+,cat=adv]	dictatorialement_____1	Default		%default
didactiquement	100	advm	[pred="didactiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	didactiquement_____1	Default		%default
didactiquement	100	advm	[pred="didactiquement_____2",clivee=+,cat=adv]	didactiquement_____2	Default		%default
difficilement	100	advm	[pred="difficilement_____1",clivee=+,cat=adv]	difficilement_____1	Default		%default
difficultueusement	100	advm	[pred="difficultueusement_____1",clivee=+,cat=adv]	difficultueusement_____1	Default		%default
diffusément	100	advm	[pred="diffusément_____1",clivee=+,cat=adv]	diffusément_____1	Default		%default
différemment	100	advm	[pred="différemment_____1",clivee=+,cat=adv]	différemment_____1	Default		%default
dignement	100	advm	[pred="dignement_____1",clivee=+,detach_neg = +,cat=adv]	dignement_____1	Default		%default
dilatoirement	100	advm	[pred="dilatoirement_____1",clivee=+,cat=adv]	dilatoirement_____1	Default		%default
diligemment	100	advm	[pred="diligemment_____1",clivee=+,detach_neg = +,cat=adv]	diligemment_____1	Default		%default
dimanche	100	adv	[pred="dimanche_____1",cat=adv]	dimanche_____1	Default		%default
diminuendo	100	adv	[pred="diminuendo_____1",cat=adv]	diminuendo_____1	Default		%default
dinguement	100	advm	[pred="dinguement_____1",clivee=+,detach_neg = +,cat=adv]	dinguement_____1	Default		%default
diplomatiquement	100	advm	[pred="diplomatiquement_____1",clivee=+,cat=adv]	diplomatiquement_____1	Default		%default
diplomatiquement	100	advm	[pred="diplomatiquement_____2",clivee=+,cat=adv]	diplomatiquement_____2	Default		%default
directement	100	advm	[pred="directement_____1",clivee=+,cat=adv]	directement_____1	Default		%default
disciplinairement	100	advm	[pred="disciplinairement_____1",clivee=+,cat=adv]	disciplinairement_____1	Default		%default
discographiquement	100	advm	[pred="discographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	discographiquement_____1	Default		%default
discontinûment	100	advm	[pred="discontinûment_____1",clivee=+,cat=adv]	discontinûment_____1	Default		%default
discourtoisement	100	advm	[pred="discourtoisement_____1",clivee=+,detach_neg = +,cat=adv]	discourtoisement_____1	Default		%default
discriminatoirement	100	advm	[pred="discriminatoirement_____1",clivee=+,cat=adv]	discriminatoirement_____1	Default		%default
discrètement	100	advm	[pred="discrètement_____1",clivee=+,detach_neg = +,cat=adv]	discrètement_____1	Default		%default
discrétionnairement	100	advm	[pred="discrétionnairement_____1",clivee=+,cat=adv]	discrétionnairement_____1	Default		%default
discursivement	100	advm	[pred="discursivement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	discursivement_____1	Default		%default
discursivement	100	advm	[pred="discursivement_____2",clivee=+,cat=adv]	discursivement_____2	Default		%default
disertement	100	advm	[pred="disertement_____1",clivee=+,detach_neg = +,cat=adv]	disertement_____1	Default		%default
disgracieusement	100	advm	[pred="disgracieusement_____1",clivee=+,detach_neg = +,cat=adv]	disgracieusement_____1	Default		%default
disjonctivement	100	advm	[pred="disjonctivement_____1",clivee=+,cat=adv]	disjonctivement_____1	Default		%default
dispendieusement	100	advm	[pred="dispendieusement_____1",clivee=+,cat=adv]	dispendieusement_____1	Default		%default
disproportionnellement	100	advm	[pred="disproportionnellement_____1",clivee=+,cat=adv]	disproportionnellement_____1	Default		%default
disproportionnément	100	advm	[pred="disproportionnément_____1",advi=combien,clivee=+,cat=adv]	disproportionnément_____1	Default		%default
dissemblablement	100	advm	[pred="dissemblablement_____1",clivee=+,cat=adv]	dissemblablement_____1	Default		%default
dissolument	100	advm	[pred="dissolument_____1",clivee=+,detach_neg = +,cat=adv]	dissolument_____1	Default		%default
dissymétriquement	100	advm	[pred="dissymétriquement_____1",clivee=+,cat=adv]	dissymétriquement_____1	Default		%default
distinctement	100	advm	[pred="distinctement_____1",clivee=+,cat=adv]	distinctement_____1	Default		%default
distinctivement	100	advm	[pred="distinctivement_____1",clivee=+,cat=adv]	distinctivement_____1	Default		%default
distraitement	100	advm	[pred="distraitement_____1",clivee=+,detach_neg = +,cat=adv]	distraitement_____1	Default		%default
distributionnellement	100	advm	[pred="distributionnellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	distributionnellement_____1	Default		%default
distributionnellement	100	advm	[pred="distributionnellement_____2",cat=adv]	distributionnellement_____2	Default		%default
distributivement	100	advm	[pred="distributivement_____1",clivee=+,cat=adv]	distributivement_____1	Default		%default
dithyrambiquement	100	advm	[pred="dithyrambiquement_____1",clivee=+,detach_neg = +,cat=adv]	dithyrambiquement_____1	Default		%default
dito	100	adv	[pred="dito_____1",cat=adv]	dito_____1	Default		%default
diurnement	100	advm	[pred="diurnement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	diurnement_____1	Default		%default
diversement	100	advm	[pred="diversement_____1",clivee=+,cat=adv]	diversement_____1	Default		%default
divinement	100	advm	[pred="divinement_____1",advi=combien,clivee=+,cat=adv]	divinement_____1	Default		%default
dix-huitièmement	100	advp	[pred="dix-huitièmement_____1",detach = +,detach_neg = +,cat=adv]	dix-huitièmement_____1	Default		%default
dix-neuvièmement	100	advp	[pred="dix-neuvièmement_____1",detach = +,detach_neg = +,cat=adv]	dix-neuvièmement_____1	Default		%default
dix-septièmement	100	advp	[pred="dix-septièmement_____1",detach = +,detach_neg = +,cat=adv]	dix-septièmement_____1	Default		%default
dixièmement	100	advp	[pred="dixièmement_____1",detach = +,detach_neg = +,cat=adv]	dixièmement_____1	Default		%default
diététiquement	100	advm	[pred="diététiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	diététiquement_____1	Default		%default
diététiquement	100	advm	[pred="diététiquement_____2",clivee=+,cat=adv]	diététiquement_____2	Default		%default
docilement	100	advm	[pred="docilement_____1",clivee=+,detach_neg = +,cat=adv]	docilement_____1	Default		%default
docimologiquement	100	advm	[pred="docimologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	docimologiquement_____1	Default		%default
doctement	100	advm	[pred="doctement_____1",clivee=+,detach_neg = +,cat=adv]	doctement_____1	Default		%default
doctoralement	100	advm	[pred="doctoralement_____1",clivee=+,detach_neg = +,cat=adv]	doctoralement_____1	Default		%default
doctrinairement	100	advm	[pred="doctrinairement_____1",clivee=+,detach_neg = +,cat=adv]	doctrinairement_____1	Default		%default
doctrinalement	100	advm	[pred="doctrinalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	doctrinalement_____1	Default		%default
doctrinalement	100	advm	[pred="doctrinalement_____2",clivee=+,cat=adv]	doctrinalement_____2	Default		%default
documentairement	100	advm	[pred="documentairement_____1",clivee=+,cat=adv]	documentairement_____1	Default		%default
dodécaphoniquement	100	advm	[pred="dodécaphoniquement_____1",clivee=+,cat=adv]	dodécaphoniquement_____1	Default		%default
dogmatiquement	100	advm	[pred="dogmatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	dogmatiquement_____1	Default		%default
dogmatiquement	100	advm	[pred="dogmatiquement_____2",clivee=+,detach_neg = +,cat=adv]	dogmatiquement_____2	Default		%default
dolce	100	adv	[pred="dolce_____1",cat=adv]	dolce_____1	Default		%default
dolcissimo	100	adv	[pred="dolcissimo_____1",cat=adv]	dolcissimo_____1	Default		%default
dolemment	100	advm	[pred="dolemment_____1",clivee=+,detach_neg = +,cat=adv]	dolemment_____1	Default		%default
dolentement	100	advm	[pred="dolentement_____1",clivee=+,detach_neg = +,cat=adv]	dolentement_____1	Default		%default
dolosivement	100	advm	[pred="dolosivement_____1",clivee=+,cat=adv]	dolosivement_____1	Default		%default
domestiquement	100	advm	[pred="domestiquement_____1",clivee=+,detach_neg = +,cat=adv]	domestiquement_____1	Default		%default
domestiquement	100	advm	[pred="domestiquement_____2",clivee=+,cat=adv]	domestiquement_____2	Default		%default
dommageablement	100	advm	[pred="dommageablement_____1",clivee=+,cat=adv]	dommageablement_____1	Default		%default
donc	100	adv	[pred="donc_____1",cat=adv]	donc_____1	Default		%default
doriquement	100	advm	[pred="doriquement_____1",clivee=+,cat=adv]	doriquement_____1	Default		%default
dorsalement	100	advm	[pred="dorsalement_____1",clivee=+,cat=adv]	dorsalement_____1	Default		%default
dorénavant	100	adv	[pred="dorénavant_____1",cat=adv]	dorénavant_____1	Default		%default
dos à dos	100	adv	[pred="dos à dos_____1",cat=adv]	dos à dos_____1	Default		%default
doublement	100	advm	[pred="doublement_____1",advi=combien,clivee=+,cat=adv]	doublement_____1	Default		%default
doucement	100	advm	[pred="doucement_____1",clivee=+,detach_neg = +,cat=adv]	doucement_____1	Default		%default
doucement	100	advm	[pred="doucement_____2",clivee=+,cat=adv]	doucement_____2	Default		%default
doucereusement	100	advm	[pred="doucereusement_____1",clivee=+,detach_neg = +,cat=adv]	doucereusement_____1	Default		%default
doucettement	100	advm	[pred="doucettement_____1",clivee=+,cat=adv]	doucettement_____1	Default		%default
douillettement	100	advm	[pred="douillettement_____1",clivee=+,cat=adv]	douillettement_____1	Default		%default
douloureusement	100	advm	[pred="douloureusement_____1",clivee=+,cat=adv]	douloureusement_____1	Default		%default
douteusement	100	advm	[pred="douteusement_____1",clivee=+,cat=adv]	douteusement_____1	Default		%default
doux	100	adv	[pred="doux_____1",cat=adv]	doux_____1	Default		%default
douzièmement	100	advp	[pred="douzièmement_____1",detach = +,detach_neg = +,cat=adv]	douzièmement_____1	Default		%default
draconiennement	100	advm	[pred="draconiennement_____1",advi=combien,clivee=+,cat=adv]	draconiennement_____1	Default		%default
dramatiquement	100	advm	[pred="dramatiquement_____1",clivee=+,cat=adv]	dramatiquement_____1	Default		%default
drastiquement	100	advm	[pred="drastiquement_____1",advi=combien,clivee=+,cat=adv]	drastiquement_____1	Default		%default
driographiquement	100	advm	[pred="driographiquement_____1",clivee=+,cat=adv]	driographiquement_____1	Default		%default
droit	100	adv	[pred="droit_____1",cat=adv]	droit_____1	Default		%default
droitement	100	advm	[pred="droitement_____1",clivee=+,detach_neg = +,cat=adv]	droitement_____1	Default		%default
droitement	100	advm	[pred="droitement_____2",clivee=+,cat=adv]	droitement_____2	Default		%default
drolatiquement	100	advm	[pred="drolatiquement_____1",clivee=+,cat=adv]	drolatiquement_____1	Default		%default
dru	100	adv	[pred="dru_____1",cat=adv]	dru_____1	Default		%default
drument	100	advm	[pred="drument_____1",clivee=+,cat=adv]	drument_____1	Default		%default
drôlement	100	advm	[pred="drôlement_____1",advi=combien,cat=adv]	drôlement_____1	Default		%default
drôlement	100	advm	[pred="drôlement_____2",clivee=+,detach_neg = +,cat=adv]	drôlement_____2	Default		%default
du coup	100	adv	[pred="du coup_____1",cat=adv]	du coup_____1	Default		%default
du moins	100	adv	[pred="du moins_____1",cat=adv]	du moins_____1	Default		%default
du reste	100	adv	[pred="du reste_____1",cat=adv]	du reste_____1	Default		%default
du tout	100	adv	[pred="du tout_____1",cat=adv]	du tout_____1	Default		%default
dubitativement	100	advm	[pred="dubitativement_____1",clivee=+,detach_neg = +,cat=adv]	dubitativement_____1	Default		%default
dur	100	adv	[pred="dur_____1",cat=adv]	dur_____1	Default		%default
durablement	100	advm	[pred="durablement_____1",advi=en_combien_de_temps,clivee=+,cat=adv]	durablement_____1	Default		%default
durement	100	advm	[pred="durement_____1",advi=combien,clivee=+,cat=adv]	durement_____1	Default		%default
durement	100	advm	[pred="durement_____2",clivee=+,detach_neg = +,cat=adv]	durement_____2	Default		%default
dynamiquement	100	advm	[pred="dynamiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	dynamiquement_____1	Default		%default
dynamiquement	100	advm	[pred="dynamiquement_____2",clivee=+,detach_neg = +,cat=adv]	dynamiquement_____2	Default		%default
dynamogéniquement	100	advm	[pred="dynamogéniquement_____1",clivee=+,cat=adv]	dynamogéniquement_____1	Default		%default
dysharmoniquement	100	advm	[pred="dysharmoniquement_____1",clivee=+,cat=adv]	dysharmoniquement_____1	Default		%default
dès avant	100	adv	[pred="dès avant_____1",cat=adv]	dès avant_____1	Default		%default
dès lors	100	adv	[pred="dès lors_____1",cat=adv]	dès lors_____1	Default		%default
dès que possible	100	adv	[pred="dès que possible_____1",cat=adv]	dès que possible_____1	Default		%default
débilement	100	advm	[pred="débilement_____1",clivee=+,detach_neg = +,cat=adv]	débilement_____1	Default		%default
débonnairement	100	advm	[pred="débonnairement_____1",clivee=+,detach_neg = +,cat=adv]	débonnairement_____1	Default		%default
décemment	100	advm	[pred="décemment_____1",clivee=+,detach_neg = +,cat=adv]	décemment_____1	Default		%default
décemment	100	advp	[pred="décemment_____2",detach = +,detach_neg = +,cat=adv]	décemment_____2	Default		%default
décidément	100	advm	[pred="décidément_____1",clivee=+,detach_neg = +,cat=adv]	décidément_____1	Default		%default
décidément	100	advp	[pred="décidément_____2",detach = +,detach_neg = +,cat=adv]	décidément_____2	Default		%default
décimalement	100	advm	[pred="décimalement_____1",clivee=+,cat=adv]	décimalement_____1	Default		%default
décisivement	100	advm	[pred="décisivement_____1",clivee=+,cat=adv]	décisivement_____1	Default		%default
déclamatoirement	100	advm	[pred="déclamatoirement_____1",clivee=+,cat=adv]	déclamatoirement_____1	Default		%default
décorativement	100	advm	[pred="décorativement_____1",clivee=+,cat=adv]	décorativement_____1	Default		%default
dédaigneusement	100	advm	[pred="dédaigneusement_____1",clivee=+,detach_neg = +,cat=adv]	dédaigneusement_____1	Default		%default
déductivement	100	advm	[pred="déductivement_____1",clivee=+,cat=adv]	déductivement_____1	Default		%default
défavorablement	100	advm	[pred="défavorablement_____1",clivee=+,detach_neg = +,cat=adv]	défavorablement_____1	Default		%default
défectueusement	100	advm	[pred="défectueusement_____1",clivee=+,cat=adv]	défectueusement_____1	Default		%default
défensivement	100	advm	[pred="défensivement_____1",clivee=+,cat=adv]	défensivement_____1	Default		%default
définitivement	100	advm	[pred="définitivement_____1",advi=en_combien_de_temps,clivee=+,cat=adv]	définitivement_____1	Default		%default
dégoûtamment	100	advm	[pred="dégoûtamment_____1",clivee=+,detach_neg = +,cat=adv]	dégoûtamment_____1	Default		%default
dégressivement	100	advm	[pred="dégressivement_____1",clivee=+,cat=adv]	dégressivement_____1	Default		%default
dégueulassement	100	advm	[pred="dégueulassement_____1",clivee=+,detach_neg = +,cat=adv]	dégueulassement_____1	Default		%default
déictiquement	100	advm	[pred="déictiquement_____1",clivee=+,cat=adv]	déictiquement_____1	Default		%default
déjà	100	adv	[pred="déjà_____1",cat=adv]	déjà_____1	Default		%default
délectablement	100	advm	[pred="délectablement_____1",advi=combien,clivee=+,cat=adv]	délectablement_____1	Default		%default
délibérément	100	advm	[pred="délibérément_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	délibérément_____1	Default		%default
délicatement	100	advm	[pred="délicatement_____1",clivee=+,detach_neg = +,cat=adv]	délicatement_____1	Default		%default
délicatement	100	advm	[pred="délicatement_____2",clivee=+,cat=adv]	délicatement_____2	Default		%default
délicieusement	100	advm	[pred="délicieusement_____1",advi=combien,clivee=+,cat=adv]	délicieusement_____1	Default		%default
déloyalement	100	advm	[pred="déloyalement_____1",clivee=+,detach_neg = +,cat=adv]	déloyalement_____1	Default		%default
démagogiquement	100	advm	[pred="démagogiquement_____1",clivee=+,cat=adv]	démagogiquement_____1	Default		%default
démentiellement	100	advm	[pred="démentiellement_____1",advi=combien,clivee=+,cat=adv]	démentiellement_____1	Default		%default
démesurément	100	advm	[pred="démesurément_____1",advi=combien,cat=adv]	démesurément_____1	Default		%default
démocratiquement	100	advm	[pred="démocratiquement_____1",clivee=+,cat=adv]	démocratiquement_____1	Default		%default
démographiquement	100	advm	[pred="démographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	démographiquement_____1	Default		%default
démographiquement	100	advm	[pred="démographiquement_____2",clivee=+,cat=adv]	démographiquement_____2	Default		%default
démoniaquement	100	advm	[pred="démoniaquement_____1",clivee=+,detach_neg = +,cat=adv]	démoniaquement_____1	Default		%default
démonstrativement	100	advm	[pred="démonstrativement_____1",clivee=+,cat=adv]	démonstrativement_____1	Default		%default
déontologiquement	100	advm	[pred="déontologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	déontologiquement_____1	Default		%default
déontologiquement	100	advm	[pred="déontologiquement_____2",clivee=+,cat=adv]	déontologiquement_____2	Default		%default
départementalement	100	advm	[pred="départementalement_____1",clivee=+,cat=adv]	départementalement_____1	Default		%default
dépendamment	100	advm	[pred="dépendamment_____1",clivee=+,detach_neg = +,cat=adv]	dépendamment_____1	Default		%default
déplaisamment	100	advm	[pred="déplaisamment_____1",clivee=+,detach_neg = +,cat=adv]	déplaisamment_____1	Default		%default
déplorablement	100	advm	[pred="déplorablement_____1",advi=combien,clivee=+,cat=adv]	déplorablement_____1	Default		%default
dépréciativement	100	advm	[pred="dépréciativement_____1",clivee=+,cat=adv]	dépréciativement_____1	Default		%default
déraisonnablement	100	advm	[pred="déraisonnablement_____1",clivee=+,detach_neg = +,cat=adv]	déraisonnablement_____1	Default		%default
dérisoirement	100	advm	[pred="dérisoirement_____1",advi=combien,clivee=+,cat=adv]	dérisoirement_____1	Default		%default
dérivationnellement	100	advm	[pred="dérivationnellement_____1",clivee=+,cat=adv]	dérivationnellement_____1	Default		%default
dérogativement	100	advm	[pred="dérogativement_____1",clivee=+,cat=adv]	dérogativement_____1	Default		%default
dérogatoirement	100	advm	[pred="dérogatoirement_____1",clivee=+,cat=adv]	dérogatoirement_____1	Default		%default
désagréablement	100	advm	[pred="désagréablement_____1",clivee=+,cat=adv]	désagréablement_____1	Default		%default
désapprobativement	100	advm	[pred="désapprobativement_____1",clivee=+,detach_neg = +,cat=adv]	désapprobativement_____1	Default		%default
désastreusement	100	advm	[pred="désastreusement_____1",advi=combien,clivee=+,cat=adv]	désastreusement_____1	Default		%default
désavantageusement	100	advm	[pred="désavantageusement_____1",clivee=+,cat=adv]	désavantageusement_____1	Default		%default
désespéramment	100	advm	[pred="désespéramment_____1",advi=combien,clivee=+,cat=adv]	désespéramment_____1	Default		%default
désespérément	100	advm	[pred="désespérément_____1",advi=combien,clivee=+,cat=adv]	désespérément_____1	Default		%default
désespérément	100	advm	[pred="désespérément_____2",clivee=+,detach_neg = +,cat=adv]	désespérément_____2	Default		%default
déshonnêtement	100	advm	[pred="déshonnêtement_____1",clivee=+,detach_neg = +,cat=adv]	déshonnêtement_____1	Default		%default
désinvoltement	100	advm	[pred="désinvoltement_____1",clivee=+,detach_neg = +,cat=adv]	désinvoltement_____1	Default		%default
désobligeamment	100	advm	[pred="désobligeamment_____1",clivee=+,detach_neg = +,cat=adv]	désobligeamment_____1	Default		%default
désolamment	100	advm	[pred="désolamment_____1",advi=combien,clivee=+,cat=adv]	désolamment_____1	Default		%default
désordonnément	100	advm	[pred="désordonnément_____1",clivee=+,detach_neg = +,cat=adv]	désordonnément_____1	Default		%default
désormais	100	adv	[pred="désormais_____1",cat=adv]	désormais_____1	Default		%default
déterminément	100	advm	[pred="déterminément_____1",clivee=+,detach_neg = +,cat=adv]	déterminément_____1	Default		%default
détestablement	100	advm	[pred="détestablement_____1",advi=combien,clivee=+,cat=adv]	détestablement_____1	Default		%default
dévotement	100	advm	[pred="dévotement_____1",clivee=+,detach_neg = +,cat=adv]	dévotement_____1	Default		%default
dévotieusement	100	advm	[pred="dévotieusement_____1",clivee=+,detach_neg = +,cat=adv]	dévotieusement_____1	Default		%default
dûment	100	advm	[pred="dûment_____1",cat=adv]	dûment_____1	Default		%default
e.a..	100	adv	[pred="et alii_____1",cat=adv]	et alii_____1	Default		%default
e.g.	100	adv	[pred="exempli gratia_____1",cat=adv]	exempli gratia_____1	Default		%default
ecce homo	100	adv	[pred="ecce homo_____1",cat=adv]	ecce homo_____1	Default		%default
ecclésiastiquement	100	advm	[pred="ecclésiastiquement_____1",clivee=+,cat=adv]	ecclésiastiquement_____1	Default		%default
effectivement	100	advp	[pred="effectivement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	effectivement_____1	Default		%default
efficacement	100	advm	[pred="efficacement_____1",clivee=+,detach_neg = +,cat=adv]	efficacement_____1	Default		%default
effrayamment	100	advm	[pred="effrayamment_____1",advi=combien,clivee=+,cat=adv]	effrayamment_____1	Default		%default
effrontément	100	advm	[pred="effrontément_____1",clivee=+,detach_neg = +,cat=adv]	effrontément_____1	Default		%default
effroyablement	100	advm	[pred="effroyablement_____1",advi=combien,clivee=+,cat=adv]	effroyablement_____1	Default		%default
effrénément	100	advm	[pred="effrénément_____1",clivee=+,detach_neg = +,cat=adv]	effrénément_____1	Default		%default
eh	100	adv	[pred="eh_____1",cat=adv]	eh_____1	Default		%default
elliptiquement	100	advm	[pred="elliptiquement_____1",clivee=+,cat=adv]	elliptiquement_____1	Default		%default
emblématiquement	100	advm	[pred="emblématiquement_____1",clivee=+,cat=adv]	emblématiquement_____1	Default		%default
embryologiquement	100	advm	[pred="embryologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	embryologiquement_____1	Default		%default
embryologiquement	100	advm	[pred="embryologiquement_____2",clivee=+,cat=adv]	embryologiquement_____2	Default		%default
embryonnairement	100	advm	[pred="embryonnairement_____1",clivee=+,cat=adv]	embryonnairement_____1	Default		%default
empathiquement	100	advm	[pred="empathiquement_____1",clivee=+,cat=adv]	empathiquement_____1	Default		%default
emphatiquement	100	advm	[pred="emphatiquement_____1",clivee=+,detach_neg = +,cat=adv]	emphatiquement_____1	Default		%default
empiriquement	100	advm	[pred="empiriquement_____1",clivee=+,cat=adv]	empiriquement_____1	Default		%default
en arrière	100	adv	[pred="en arrière_____1",chunk_type=GP,cat=adv]	en arrière_____1	Default		%default
en avant	100	adv	[pred="en avant_____1",chunk_type=GP,cat=adv]	en avant_____1	Default		%default
en bas	100	adv	[pred="en bas_____1",chunk_type=GP,cat=adv]	en bas_____1	Default		%default
en bref	100	adv	[pred="en bref_____1",cat=adv]	en bref_____1	Default		%default
en catimini	100	adv	[pred="en catimini_____1",chunk_type=GP,cat=adv]	en catimini_____1	Default		%default
en ce cas	100	adv	[pred="en ce cas_____1",cat=adv]	en ce cas_____1	Default		%default
en chien de fusil	100	adv	[pred="en chien de fusil_____1",chunk_type=GP,cat=adv]	en chien de fusil_____1	Default		%default
en commun	100	adv	[pred="en commun_____1",chunk_type=GP,cat=adv]	en commun_____1	Default		%default
en comparaison	100	adv	[pred="en comparaison_____1",cat=adv]	en comparaison_____1	Default		%default
en conséquence	100	adv	[pred="en conséquence_____1",chunk_type=GP,cat=adv]	en conséquence_____1	Default		%default
en cours	100	adv	[pred="en cours_____1",chunk_type=GP,cat=adv]	en cours_____1	Default		%default
en d'autres termes	100	adv	[pred="en d'autres termes_____1",cat=adv]	en d'autres termes_____1	Default		%default
en dedans	100	adv	[pred="en dedans_____1",chunk_type=GP,cat=adv]	en dedans_____1	Default		%default
en dessous	100	adv	[pred="en dessous_____1",chunk_type=GP,cat=adv]	en dessous_____1	Default		%default
en dessus	100	adv	[pred="en dessus_____1",chunk_type=GP,cat=adv]	en dessus_____1	Default		%default
en deçà	100	adv	[pred="en-deçà_____1",cat=adv]	en-deçà_____1	Default		%default
en diable	100	adv	[pred="en diable_____1",chunk_type=GP,cat=adv]	en diable_____1	Default		%default
en définitive	100	adv	[pred="en définitive_____1",chunk_type=GP,cat=adv]	en définitive_____1	Default		%default
en effet	100	adv	[pred="en effet_____1",chunk_type=GP,cat=adv]	en effet_____1	Default		%default
en entier	100	adv	[pred="en entier_____1",chunk_type=GP,cat=adv]	en entier_____1	Default		%default
en fait	100	adv	[pred="en fait_____1",cat=adv]	en fait_____1	Default		%default
en gros	100	adv	[pred="en gros_____1",cat=adv]	en gros_____1	Default		%default
en général	100	adv	[pred="en général_____1",chunk_type=GP,cat=adv]	en général_____1	Default		%default
en haut	100	adv	[pred="en haut_____1",chunk_type=GP,cat=adv]	en haut_____1	Default		%default
en majorité	100	adv	[pred="en majorité_____1",chunk_type=GP,cat=adv]	en majorité_____1	Default		%default
en moins	100	adv	[pred="en moins_____1",adv_kind=modnc,cat=adv]	en moins_____1	Default		%default
en moins	100	adv	[pred="en moins_____2",cat=adv]	en moins_____2	Default		%default
en moyenne	100	adv	[pred="en moyenne_____1",chunk_type=GP,cat=adv]	en moyenne_____1	Default		%default
en même temps	100	adv	[pred="en même temps_____1",cat=adv]	en même temps_____1	Default		%default
en outre	100	adv	[pred="en outre_____1",cat=adv]	en outre_____1	Default		%default
en particulier	100	adv	[pred="en particulier_____1",adv_kind=modnc,cat=adv]	en particulier_____1	Default		%default
en particulier	100	adv	[pred="en particulier_____2",cat=adv]	en particulier_____2	Default		%default
en partie	100	adv	[pred="en partie_____1",cat=adv]	en partie_____1	Default		%default
en plus	100	adv	[pred="en plus_____1",adv_kind=modnc,cat=adv]	en plus_____1	Default		%default
en plus	100	adv	[pred="en plus_____2",cat=adv]	en plus_____2	Default		%default
en porte à faux	100	adv	[pred="en porte à faux_____1",chunk_type=GP,cat=adv]	en porte à faux_____1	Default		%default
en principe	100	adv	[pred="en principe_____1",chunk_type=GP,cat=adv]	en principe_____1	Default		%default
en quelque sorte	100	adv	[pred="en quelque sorte_____1",chunk_type=GP,cat=adv]	en quelque sorte_____1	Default		%default
en quelques sortes	100	adv	[pred="en quelques sortes_____1",chunk_type=GP,cat=adv]	en quelques sortes_____1	Default		%default
en revanche	100	adv	[pred="en revanche_____1",cat=adv]	en revanche_____1	Default		%default
en rien	100	adv	[pred="en rien_____1",chunk_type=GP,cat=adv]	en rien_____1	Default		%default
en rond	100	adv	[pred="en rond_____1",chunk_type=GP,cat=adv]	en rond_____1	Default		%default
en réalité	100	adv	[pred="en réalité_____1",cat=adv]	en réalité_____1	Default		%default
en résumé	100	adv	[pred="en résumé_____1",cat=adv]	en résumé_____1	Default		%default
en somme	100	adv	[pred="en somme_____1",chunk_type=GP,cat=adv]	en somme_____1	Default		%default
en stand-by	100	adv	[pred="en stand-by_____1",chunk_type=GP,cat=adv]	en stand-by_____1	Default		%default
en sus	100	adv	[pred="en sus_____1",adv_kind=modnc,cat=adv]	en sus_____1	Default		%default
en sus	100	adv	[pred="en sus_____2",cat=adv]	en sus_____2	Default		%default
en tous les cas	100	adv	[pred="en tous les cas_____1",cat=adv]	en tous les cas_____1	Default		%default
en tout	100	adv	[pred="en tout_____1",chunk_type=GP,cat=adv]	en tout_____1	Default		%default
en tout cas	100	adv	[pred="en tout cas_____1",cat=adv]	en tout cas_____1	Default		%default
en tout et pour tout	100	adv	[pred="en tout et pour tout_____1",chunk_type=GP,cat=adv]	en tout et pour tout_____1	Default		%default
en tout état de cause	100	adv	[pred="en tout état de cause_____1",cat=adv]	en tout état de cause_____1	Default		%default
en tête à tête	100	adv	[pred="en tête à tête_____1",chunk_type=GP,cat=adv]	en tête à tête_____1	Default		%default
en un clin d'oeil	100	adv	[pred="en un clin d'oeil_____1",chunk_type=GP,cat=adv]	en un clin d'oeil_____1	Default		%default
en vain	100	adv	[pred="en vain_____1",cat=adv]	en vain_____1	Default		%default
en vigueur	100	adv	[pred="en vigueur_____1",chunk_type=GP,cat=adv]	en vigueur_____1	Default		%default
en vrac	100	adv	[pred="en vrac_____1",chunk_type=GP,cat=adv]	en vrac_____1	Default		%default
en vérité	100	adv	[pred="en vérité_____1",chunk_type=GP,cat=adv]	en vérité_____1	Default		%default
en-deçà	100	adv	[pred="en-deçà_____1",cat=adv]	en-deçà_____1	Default		%default
encore	100	adv	[pred="encore_____1",cat=adv]	encore_____1	Default		%default
encyclopédiquement	100	advm	[pred="encyclopédiquement_____1",clivee=+,cat=adv]	encyclopédiquement_____1	Default		%default
endémiquement	100	advm	[pred="endémiquement_____1",clivee=+,cat=adv]	endémiquement_____1	Default		%default
enfantinement	100	advm	[pred="enfantinement_____1",clivee=+,detach_neg = +,cat=adv]	enfantinement_____1	Default		%default
enfin	100	adv	[pred="enfin_____1",cat=adv]	enfin_____1	Default		%default
enjôleusement	100	advm	[pred="enjôleusement_____1",clivee=+,detach_neg = +,cat=adv]	enjôleusement_____1	Default		%default
ennuyeusement	100	advm	[pred="ennuyeusement_____1",clivee=+,detach_neg = +,cat=adv]	ennuyeusement_____1	Default		%default
ensemble	100	adv	[pred="ensemble_____1",cat=adv]	ensemble_____1	Default		%default
ensorceleusement	100	advm	[pred="ensorceleusement_____1",clivee=+,detach_neg = +,cat=adv]	ensorceleusement_____1	Default		%default
ensuite	100	adv	[pred="ensuite_____1",cat=adv]	ensuite_____1	Default		%default
enthousiastement	100	advm	[pred="enthousiastement_____1",clivee=+,detach_neg = +,cat=adv]	enthousiastement_____1	Default		%default
entièrement	100	advm	[pred="entièrement_____1",advi=combien,cat=adv]	entièrement_____1	Default		%default
entomologiquement	100	advm	[pred="entomologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	entomologiquement_____1	Default		%default
entomologiquement	100	advm	[pred="entomologiquement_____2",clivee=+,cat=adv]	entomologiquement_____2	Default		%default
entre autre	100	adv	[pred="entre autre_____1",chunk_type=GP,cat=adv]	entre autre_____1	Default		%default
entre autres	100	adv	[pred="entre autres_____1",chunk_type=GP,cat=adv]	entre autres_____1	Default		%default
entre temps	100	adv	[pred="entre temps_____1",cat=adv]	entre temps_____1	Default		%default
entre-temps	100	adv	[pred="entre-temps_____1",cat=adv]	entre-temps_____1	Default		%default
envieusement	100	advm	[pred="envieusement_____1",clivee=+,detach_neg = +,cat=adv]	envieusement_____1	Default		%default
environ	100	adv	[pred="environ_____1",adv_kind=modnc,cat=adv]	environ_____1	Default		%default
ergonomiquement	100	advm	[pred="ergonomiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ergonomiquement_____1	Default		%default
erronément	100	advm	[pred="erronément_____1",clivee=+,cat=adv]	erronément_____1	Default		%default
espièglement	100	advm	[pred="espièglement_____1",clivee=+,detach_neg = +,cat=adv]	espièglement_____1	Default		%default
espressivo	100	adv	[pred="espressivo_____1",cat=adv]	espressivo_____1	Default		%default
essentiellement	100	advm	[pred="essentiellement_____1",detach_neg = +,modnc = +,cat=adv]	essentiellement_____1	Default		%default
essentiellement	100	advm	[pred="essentiellement_____2",advi=combien,cat=adv]	essentiellement_____2	Default		%default
esthétiquement	100	advm	[pred="esthétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	esthétiquement_____1	Default		%default
esthétiquement	100	advm	[pred="esthétiquement_____2",clivee=+,cat=adv]	esthétiquement_____2	Default		%default
estimablement	100	advm	[pred="estimablement_____1",clivee=+,detach_neg = +,cat=adv]	estimablement_____1	Default		%default
et al..	100	adv	[pred="et alii_____1",cat=adv]	et alii_____1	Default		%default
et alii	100	adv	[pred="et alii_____1",cat=adv]	et alii_____1	Default		%default
et caetera	100	adv	[pred="et caetera_____1",cat=adv]	et caetera_____1	Default		%default
et dire que	100	adv	[pred="et dire que_____1",cat=adv]	et dire que_____1	Default		%default
et tutti quanti	100	adv	[pred="et tutti quanti_____1",cat=adv]	et tutti quanti_____1	Default		%default
ethniquement	100	advm	[pred="ethniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ethniquement_____1	Default		%default
ethnographiquement	100	advm	[pred="ethnographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ethnographiquement_____1	Default		%default
ethnographiquement	100	advm	[pred="ethnographiquement_____2",clivee=+,cat=adv]	ethnographiquement_____2	Default		%default
ethnolinguistiquement	100	advm	[pred="ethnolinguistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ethnolinguistiquement_____1	Default		%default
ethnolinguistiquement	100	advm	[pred="ethnolinguistiquement_____2",clivee=+,cat=adv]	ethnolinguistiquement_____2	Default		%default
ethnologiquement	100	advm	[pred="ethnologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ethnologiquement_____1	Default		%default
ethnologiquement	100	advm	[pred="ethnologiquement_____2",clivee=+,cat=adv]	ethnologiquement_____2	Default		%default
eucharistiquement	100	advm	[pred="eucharistiquement_____1",clivee=+,cat=adv]	eucharistiquement_____1	Default		%default
euphoniquement	100	advm	[pred="euphoniquement_____1",clivee=+,cat=adv]	euphoniquement_____1	Default		%default
euphoriquement	100	advm	[pred="euphoriquement_____1",clivee=+,detach_neg = +,cat=adv]	euphoriquement_____1	Default		%default
euphémiquement	100	advm	[pred="euphémiquement_____1",clivee=+,cat=adv]	euphémiquement_____1	Default		%default
européennement	100	advm	[pred="européennement_____1",clivee=+,cat=adv]	européennement_____1	Default		%default
eurythmiquement	100	advm	[pred="eurythmiquement_____1",clivee=+,cat=adv]	eurythmiquement_____1	Default		%default
eurêka	100	adv	[pred="eurêka_____1",cat=adv]	eurêka_____1	Default		%default
ex abrupto	100	adv	[pred="ex abrupto_____1",cat=adv]	ex abrupto_____1	Default		%default
ex aequo	100	adv	[pred="ex aequo_____1",cat=adv]	ex aequo_____1	Default		%default
ex cathedra	100	adv	[pred="ex cathedra_____1",cat=adv]	ex cathedra_____1	Default		%default
ex nihilo	100	adv	[pred="ex nihilo_____1",cat=adv]	ex nihilo_____1	Default		%default
ex professo	100	adv	[pred="ex professo_____1",cat=adv]	ex professo_____1	Default		%default
ex-libris	100	adv	[pred="ex-libris_____1",cat=adv]	ex-libris_____1	Default		%default
exactement	100	advm	[pred="exactement_____1",detach_neg = +,modnc = +,cat=adv]	exactement_____1	Default		%default
exactement	100	advm	[pred="exactement_____2",clivee=+,detach_neg = +,cat=adv]	exactement_____2	Default		%default
exagérément	100	advm	[pred="exagérément_____1",advi=combien,clivee=+,cat=adv]	exagérément_____1	Default		%default
exaspérément	100	advm	[pred="exaspérément_____1",clivee=+,detach_neg = +,cat=adv]	exaspérément_____1	Default		%default
excellemment	100	advm	[pred="excellemment_____1",advi=combien,clivee=+,cat=adv]	excellemment_____1	Default		%default
excentriquement	100	advm	[pred="excentriquement_____1",clivee=+,detach_neg = +,cat=adv]	excentriquement_____1	Default		%default
excentriquement	100	advm	[pred="excentriquement_____2",clivee=+,cat=adv]	excentriquement_____2	Default		%default
exceptionnellement	100	advm	[pred="exceptionnellement_____1",advi=combien,clivee=+,cat=adv]	exceptionnellement_____1	Default		%default
exceptionnellement	100	advm	[pred="exceptionnellement_____2",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	exceptionnellement_____2	Default		%default
excessivement	100	advm	[pred="excessivement_____1",advi=combien,clivee=+,cat=adv]	excessivement_____1	Default		%default
exclamativement	100	advm	[pred="exclamativement_____1",clivee=+,cat=adv]	exclamativement_____1	Default		%default
exclusivement	100	advm	[pred="exclusivement_____1",modnc = +,cat=adv]	exclusivement_____1	Default		%default
exclusivement	100	advm	[pred="exclusivement_____2",clivee=+,detach_neg = +,cat=adv]	exclusivement_____2	Default		%default
excédentairement	100	advm	[pred="excédentairement_____1",advi=combien,clivee=+,cat=adv]	excédentairement_____1	Default		%default
exemplairement	100	advm	[pred="exemplairement_____1",clivee=+,detach_neg = +,cat=adv]	exemplairement_____1	Default		%default
exempli gratia	100	adv	[pred="exempli gratia_____1",cat=adv]	exempli gratia_____1	Default		%default
exhaustivement	100	advm	[pred="exhaustivement_____1",clivee=+,cat=adv]	exhaustivement_____1	Default		%default
exigûment	100	advm	[pred="exigûment_____1",clivee=+,cat=adv]	exigûment_____1	Default		%default
existentiellement	100	advm	[pred="existentiellement_____1",clivee=+,cat=adv]	existentiellement_____1	Default		%default
exorbitamment	100	advm	[pred="exorbitamment_____1",advi=combien,clivee=+,cat=adv]	exorbitamment_____1	Default		%default
exotiquement	100	advm	[pred="exotiquement_____1",clivee=+,cat=adv]	exotiquement_____1	Default		%default
expansivement	100	advm	[pred="expansivement_____1",clivee=+,detach_neg = +,cat=adv]	expansivement_____1	Default		%default
expertement	100	advm	[pred="expertement_____1",clivee=+,detach_neg = +,cat=adv]	expertement_____1	Default		%default
expiatoirement	100	advm	[pred="expiatoirement_____1",clivee=+,cat=adv]	expiatoirement_____1	Default		%default
explicativement	100	advm	[pred="explicativement_____1",clivee=+,cat=adv]	explicativement_____1	Default		%default
explicitement	100	advm	[pred="explicitement_____1",clivee=+,detach_neg = +,cat=adv]	explicitement_____1	Default		%default
explosivement	100	advm	[pred="explosivement_____1",clivee=+,cat=adv]	explosivement_____1	Default		%default
explétivement	100	advm	[pred="explétivement_____1",clivee=+,cat=adv]	explétivement_____1	Default		%default
exponentiellement	100	advm	[pred="exponentiellement_____1",clivee=+,cat=adv]	exponentiellement_____1	Default		%default
expressivement	100	advm	[pred="expressivement_____1",clivee=+,cat=adv]	expressivement_____1	Default		%default
expressément	100	advm	[pred="expressément_____1",clivee=+,cat=adv]	expressément_____1	Default		%default
exprès	100	adv	[pred="exprès_____1",cat=adv]	exprès_____1	Default		%default
expéditivement	100	advm	[pred="expéditivement_____1",clivee=+,detach_neg = +,cat=adv]	expéditivement_____1	Default		%default
expérimentalement	100	advm	[pred="expérimentalement_____1",clivee=+,cat=adv]	expérimentalement_____1	Default		%default
exquisement	100	advm	[pred="exquisement_____1",advi=combien,clivee=+,cat=adv]	exquisement_____1	Default		%default
exquisément	100	advm	[pred="exquisément_____1",advi=combien,clivee=+,cat=adv]	exquisément_____1	Default		%default
extatiquement	100	advm	[pred="extatiquement_____1",clivee=+,cat=adv]	extatiquement_____1	Default		%default
extemporanément	100	advm	[pred="extemporanément_____1",clivee=+,cat=adv]	extemporanément_____1	Default		%default
extensionnellement	100	advm	[pred="extensionnellement_____1",clivee=+,cat=adv]	extensionnellement_____1	Default		%default
extensivement	100	advm	[pred="extensivement_____1",clivee=+,cat=adv]	extensivement_____1	Default		%default
extra-muros	100	adv	[pred="extra-muros_____1",cat=adv]	extra-muros_____1	Default		%default
extra-territorialement	100	advm	[pred="extra-territorialement_____1",clivee=+,cat=adv]	extra-territorialement_____1	Default		%default
extrajudiciairement	100	advm	[pred="extrajudiciairement_____1",clivee=+,cat=adv]	extrajudiciairement_____1	Default		%default
extraordinairement	100	advm	[pred="extraordinairement_____1",advi=combien,clivee=+,cat=adv]	extraordinairement_____1	Default		%default
extravagamment	100	advm	[pred="extravagamment_____1",clivee=+,cat=adv]	extravagamment_____1	Default		%default
extrinsèquement	100	advm	[pred="extrinsèquement_____1",clivee=+,cat=adv]	extrinsèquement_____1	Default		%default
extrêmement	100	advm	[pred="extrêmement_____1",advi=combien,cat=adv]	extrêmement_____1	Default		%default
extérieurement	100	advm	[pred="extérieurement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	extérieurement_____1	Default		%default
exécrablement	100	advm	[pred="exécrablement_____1",advi=combien,clivee=+,cat=adv]	exécrablement_____1	Default		%default
exécutoirement	100	advm	[pred="exécutoirement_____1",clivee=+,cat=adv]	exécutoirement_____1	Default		%default
exégétiquement	100	advm	[pred="exégétiquement_____1",clivee=+,cat=adv]	exégétiquement_____1	Default		%default
fabuleusement	100	advm	[pred="fabuleusement_____1",advi=combien,clivee=+,cat=adv]	fabuleusement_____1	Default		%default
face à face	100	advp	[pred="face à face_____1",cat=adv]	face à face_____1	Default		%default
facile	100	adv	[pred="facile_____1",cat=adv]	facile_____1	Default		%default
facilement	100	advm	[pred="facilement_____1",clivee=+,cat=adv]	facilement_____1	Default		%default
facticement	100	advm	[pred="facticement_____1",clivee=+,cat=adv]	facticement_____1	Default		%default
factieusement	100	advm	[pred="factieusement_____1",clivee=+,detach_neg = +,cat=adv]	factieusement_____1	Default		%default
factitivement	100	advm	[pred="factitivement_____1",clivee=+,cat=adv]	factitivement_____1	Default		%default
factuellement	100	advm	[pred="factuellement_____1",clivee=+,cat=adv]	factuellement_____1	Default		%default
facultativement	100	advm	[pred="facultativement_____1",clivee=+,cat=adv]	facultativement_____1	Default		%default
facétieusement	100	advm	[pred="facétieusement_____1",clivee=+,detach_neg = +,cat=adv]	facétieusement_____1	Default		%default
fadement	100	advm	[pred="fadement_____1",clivee=+,detach_neg = +,cat=adv]	fadement_____1	Default		%default
faiblement	100	advm	[pred="faiblement_____1",advi=combien,cat=adv]	faiblement_____1	Default		%default
fallacieusement	100	advm	[pred="fallacieusement_____1",clivee=+,detach_neg = +,cat=adv]	fallacieusement_____1	Default		%default
falotement	100	advm	[pred="falotement_____1",clivee=+,detach_neg = +,cat=adv]	falotement_____1	Default		%default
fameusement	100	advm	[pred="fameusement_____1",advi=combien,clivee=+,cat=adv]	fameusement_____1	Default		%default
familialement	100	advm	[pred="familialement_____1",clivee=+,cat=adv]	familialement_____1	Default		%default
familièrement	100	advm	[pred="familièrement_____1",clivee=+,detach_neg = +,cat=adv]	familièrement_____1	Default		%default
familièrement	100	advm	[pred="familièrement_____2",clivee=+,cat=adv]	familièrement_____2	Default		%default
faméliquement	100	advm	[pred="faméliquement_____1",clivee=+,detach_neg = +,cat=adv]	faméliquement_____1	Default		%default
fanatiquement	100	advm	[pred="fanatiquement_____1",clivee=+,detach_neg = +,cat=adv]	fanatiquement_____1	Default		%default
fanfaronnement	100	advm	[pred="fanfaronnement_____1",clivee=+,detach_neg = +,cat=adv]	fanfaronnement_____1	Default		%default
fangeusement	100	advm	[pred="fangeusement_____1",clivee=+,cat=adv]	fangeusement_____1	Default		%default
fantaisistement	100	advm	[pred="fantaisistement_____1",clivee=+,detach_neg = +,cat=adv]	fantaisistement_____1	Default		%default
fantasmagoriquement	100	advm	[pred="fantasmagoriquement_____1",clivee=+,cat=adv]	fantasmagoriquement_____1	Default		%default
fantasmatiquement	100	advm	[pred="fantasmatiquement_____1",clivee=+,cat=adv]	fantasmatiquement_____1	Default		%default
fantasquement	100	advm	[pred="fantasquement_____1",clivee=+,detach_neg = +,cat=adv]	fantasquement_____1	Default		%default
fantastiquement	100	advm	[pred="fantastiquement_____1",advi=combien,clivee=+,cat=adv]	fantastiquement_____1	Default		%default
fantômatiquement	100	advm	[pred="fantômatiquement_____1",clivee=+,cat=adv]	fantômatiquement_____1	Default		%default
faramineusement	100	advm	[pred="faramineusement_____1",advi=combien,cat=adv]	faramineusement_____1	Default		%default
faraudement	100	advm	[pred="faraudement_____1",clivee=+,detach_neg = +,cat=adv]	faraudement_____1	Default		%default
farouchement	100	advm	[pred="farouchement_____1",advi=combien,cat=adv]	farouchement_____1	Default		%default
fascistement	100	advm	[pred="fascistement_____1",clivee=+,detach_neg = +,cat=adv]	fascistement_____1	Default		%default
fashionablement	100	advm	[pred="fashionablement_____1",clivee=+,detach_neg = +,cat=adv]	fashionablement_____1	Default		%default
fastidieusement	100	advm	[pred="fastidieusement_____1",clivee=+,detach_neg = +,cat=adv]	fastidieusement_____1	Default		%default
fastueusement	100	advm	[pred="fastueusement_____1",clivee=+,detach_neg = +,cat=adv]	fastueusement_____1	Default		%default
fatalement	100	advp	[pred="fatalement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	fatalement_____1	Default		%default
fatidiquement	100	advm	[pred="fatidiquement_____1",clivee=+,cat=adv]	fatidiquement_____1	Default		%default
faussement	100	advm	[pred="faussement_____1",clivee=+,cat=adv]	faussement_____1	Default		%default
faute de mieux	100	advp	[pred="faute de mieux_____1",cat=adv]	faute de mieux_____1	Default		%default
fautivement	100	advm	[pred="fautivement_____1",clivee=+,detach_neg = +,cat=adv]	fautivement_____1	Default		%default
faux	100	adv	[pred="faux_____1",cat=adv]	faux_____1	Default		%default
favorablement	100	advm	[pred="favorablement_____1",clivee=+,detach_neg = +,cat=adv]	favorablement_____1	Default		%default
ferme	100	adv	[pred="ferme_____1",cat=adv]	ferme_____1	Default		%default
fermement	100	advm	[pred="fermement_____1",clivee=+,detach_neg = +,cat=adv]	fermement_____1	Default		%default
fertilement	100	advm	[pred="fertilement_____1",clivee=+,cat=adv]	fertilement_____1	Default		%default
fervemment	100	advm	[pred="fervemment_____1",clivee=+,detach_neg = +,cat=adv]	fervemment_____1	Default		%default
fi	100	adv	[pred="fi_____1",cat=adv]	fi_____1	Default		%default
fiablement	100	advm	[pred="fiablement_____1",clivee=+,detach_neg = +,cat=adv]	fiablement_____1	Default		%default
fichtrement	100	advm	[pred="fichtrement_____1",advi=combien,cat=adv]	fichtrement_____1	Default		%default
fichument	100	advm	[pred="fichument_____1",advi=combien,cat=adv]	fichument_____1	Default		%default
fictivement	100	advm	[pred="fictivement_____1",clivee=+,cat=adv]	fictivement_____1	Default		%default
fiduciairement	100	advm	[pred="fiduciairement_____1",clivee=+,cat=adv]	fiduciairement_____1	Default		%default
fidèlement	100	advm	[pred="fidèlement_____1",clivee=+,detach_neg = +,cat=adv]	fidèlement_____1	Default		%default
fidèlement	100	advm	[pred="fidèlement_____2",clivee=+,cat=adv]	fidèlement_____2	Default		%default
fielleusement	100	advm	[pred="fielleusement_____1",clivee=+,detach_neg = +,cat=adv]	fielleusement_____1	Default		%default
fifty-fifty	100	adv	[pred="fifty-fifty_____1",cat=adv]	fifty-fifty_____1	Default		%default
figurativement	100	advm	[pred="figurativement_____1",clivee=+,cat=adv]	figurativement_____1	Default		%default
figurément	100	advm	[pred="figurément_____1",clivee=+,cat=adv]	figurément_____1	Default		%default
filandreusement	100	advm	[pred="filandreusement_____1",clivee=+,cat=adv]	filandreusement_____1	Default		%default
filialement	100	advm	[pred="filialement_____1",clivee=+,cat=adv]	filialement_____1	Default		%default
filmiquement	100	advm	[pred="filmiquement_____1",clivee=+,cat=adv]	filmiquement_____1	Default		%default
fin	100	adv	[pred="fin_____1",cat=adv]	fin_____1	Default		%default
finalement	100	advp	[pred="finalement_____1",detach = +,detach_neg = +,cat=adv]	finalement_____1	Default		%default
financièrement	100	advm	[pred="financièrement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	financièrement_____1	Default		%default
financièrement	100	advm	[pred="financièrement_____2",clivee=+,cat=adv]	financièrement_____2	Default		%default
finaudement	100	advm	[pred="finaudement_____1",clivee=+,detach_neg = +,cat=adv]	finaudement_____1	Default		%default
finement	100	advm	[pred="finement_____1",clivee=+,detach_neg = +,cat=adv]	finement_____1	Default		%default
finement	100	advm	[pred="finement_____2",clivee=+,cat=adv]	finement_____2	Default		%default
fiscalement	100	advm	[pred="fiscalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	fiscalement_____1	Default		%default
fiscalement	100	advm	[pred="fiscalement_____2",clivee=+,cat=adv]	fiscalement_____2	Default		%default
fixement	100	advm	[pred="fixement_____1",clivee=+,cat=adv]	fixement_____1	Default		%default
fixement	100	advm	[pred="fixement_____2",clivee=+,cat=adv]	fixement_____2	Default		%default
fièrement	100	advm	[pred="fièrement_____1",clivee=+,detach_neg = +,cat=adv]	fièrement_____1	Default		%default
fiévreusement	100	advm	[pred="fiévreusement_____1",clivee=+,detach_neg = +,cat=adv]	fiévreusement_____1	Default		%default
flagorneusement	100	advm	[pred="flagorneusement_____1",clivee=+,detach_neg = +,cat=adv]	flagorneusement_____1	Default		%default
flasquement	100	advm	[pred="flasquement_____1",clivee=+,cat=adv]	flasquement_____1	Default		%default
flatteusement	100	advm	[pred="flatteusement_____1",clivee=+,detach_neg = +,cat=adv]	flatteusement_____1	Default		%default
flegmatiquement	100	advm	[pred="flegmatiquement_____1",clivee=+,detach_neg = +,cat=adv]	flegmatiquement_____1	Default		%default
flexionnellement	100	advm	[pred="flexionnellement_____1",clivee=+,cat=adv]	flexionnellement_____1	Default		%default
flexueusement	100	advm	[pred="flexueusement_____1",clivee=+,cat=adv]	flexueusement_____1	Default		%default
floristiquement	100	advm	[pred="floristiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	floristiquement_____1	Default		%default
flou	100	adv	[pred="flou_____1",cat=adv]	flou_____1	Default		%default
fluidement	100	advm	[pred="fluidement_____1",clivee=+,cat=adv]	fluidement_____1	Default		%default
flâneusement	100	advm	[pred="flâneusement_____1",clivee=+,detach_neg = +,cat=adv]	flâneusement_____1	Default		%default
foireusement	100	advm	[pred="foireusement_____1",clivee=+,cat=adv]	foireusement_____1	Default		%default
folkloriquement	100	advm	[pred="folkloriquement_____1",clivee=+,detach_neg = +,cat=adv]	folkloriquement_____1	Default		%default
follement	100	advm	[pred="follement_____1",advi=combien,clivee=+,cat=adv]	follement_____1	Default		%default
follement	100	advm	[pred="follement_____2",clivee=+,detach_neg = +,cat=adv]	follement_____2	Default		%default
folâtrement	100	advm	[pred="folâtrement_____1",clivee=+,detach_neg = +,cat=adv]	folâtrement_____1	Default		%default
foncièrement	100	advm	[pred="foncièrement_____1",advi=combien,cat=adv]	foncièrement_____1	Default		%default
fonctionnellement	100	advm	[pred="fonctionnellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	fonctionnellement_____1	Default		%default
fonctionnellement	100	advm	[pred="fonctionnellement_____2",clivee=+,cat=adv]	fonctionnellement_____2	Default		%default
fondamentalement	100	advm	[pred="fondamentalement_____1",advi=combien,cat=adv]	fondamentalement_____1	Default		%default
forcément	100	advp	[pred="forcément_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	forcément_____1	Default		%default
forestièrement	100	advm	[pred="forestièrement_____1",clivee=+,cat=adv]	forestièrement_____1	Default		%default
forfaitairement	100	advm	[pred="forfaitairement_____1",clivee=+,cat=adv]	forfaitairement_____1	Default		%default
formellement	100	advm	[pred="formellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	formellement_____1	Default		%default
formellement	100	advm	[pred="formellement_____2",clivee=+,cat=adv]	formellement_____2	Default		%default
formidablement	100	advm	[pred="formidablement_____1",advi=combien,clivee=+,cat=adv]	formidablement_____1	Default		%default
fort	100	adv	[pred="fort_____1",adv_kind=intens,cat=adv]	fort_____1	Default		%default
forte	100	adv	[pred="forte_____1",cat=adv]	forte_____1	Default		%default
fortement	100	advm	[pred="fortement_____1",advi=combien,clivee=+,cat=adv]	fortement_____1	Default		%default
fortement	100	advm	[pred="fortement_____2",clivee=+,cat=adv]	fortement_____2	Default		%default
fortissimo	100	adv	[pred="fortissimo_____1",cat=adv]	fortissimo_____1	Default		%default
fortuitement	100	advm	[pred="fortuitement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	fortuitement_____1	Default		%default
fougueusement	100	advm	[pred="fougueusement_____1",clivee=+,detach_neg = +,cat=adv]	fougueusement_____1	Default		%default
fourbement	100	advm	[pred="fourbement_____1",clivee=+,detach_neg = +,cat=adv]	fourbement_____1	Default		%default
foutrement	100	advm	[pred="foutrement_____1",advi=combien,cat=adv]	foutrement_____1	Default		%default
foutument	100	advm	[pred="foutument_____1",advi=combien,cat=adv]	foutument_____1	Default		%default
fragilement	100	advm	[pred="fragilement_____1",clivee=+,cat=adv]	fragilement_____1	Default		%default
fragmentairement	100	advm	[pred="fragmentairement_____1",clivee=+,cat=adv]	fragmentairement_____1	Default		%default
franc	100	adv	[pred="franc_____1",cat=adv]	franc_____1	Default		%default
franchement	100	advm	[pred="franchement_____1",advi=combien,cat=adv]	franchement_____1	Default		%default
franchement	100	advm	[pred="franchement_____2",clivee=+,detach_neg = +,cat=adv]	franchement_____2	Default		%default
franchement	100	advp	[pred="franchement_____3",detach = +,detach_neg = +,cat=adv]	franchement_____3	Default		%default
franco	100	adv	[pred="franco_____1",cat=adv]	franco_____1	Default		%default
fraternellement	100	advm	[pred="fraternellement_____1",clivee=+,detach_neg = +,cat=adv]	fraternellement_____1	Default		%default
frauduleusement	100	advm	[pred="frauduleusement_____1",clivee=+,cat=adv]	frauduleusement_____1	Default		%default
fraîchement	100	advm	[pred="fraîchement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	fraîchement_____1	Default		%default
fraîchement	100	advm	[pred="fraîchement_____2",clivee=+,cat=adv]	fraîchement_____2	Default		%default
frigidement	100	advm	[pred="frigidement_____1",clivee=+,detach_neg = +,cat=adv]	frigidement_____1	Default		%default
frigo	100	adv	[pred="frigo_____1",cat=adv]	frigo_____1	Default		%default
frileusement	100	advm	[pred="frileusement_____1",clivee=+,detach_neg = +,cat=adv]	frileusement_____1	Default		%default
friponnement	100	advm	[pred="friponnement_____1",clivee=+,detach_neg = +,cat=adv]	friponnement_____1	Default		%default
frisquet	100	adv	[pred="frisquet_____1",cat=adv]	frisquet_____1	Default		%default
frivolement	100	advm	[pred="frivolement_____1",clivee=+,detach_neg = +,cat=adv]	frivolement_____1	Default		%default
froidement	100	advm	[pred="froidement_____1",clivee=+,detach_neg = +,cat=adv]	froidement_____1	Default		%default
frontalement	100	advm	[pred="frontalement_____1",clivee=+,cat=adv]	frontalement_____1	Default		%default
fructueusement	100	advm	[pred="fructueusement_____1",clivee=+,cat=adv]	fructueusement_____1	Default		%default
frugalement	100	advm	[pred="frugalement_____1",clivee=+,detach_neg = +,cat=adv]	frugalement_____1	Default		%default
frustement	100	advm	[pred="frustement_____1",clivee=+,detach_neg = +,cat=adv]	frustement_____1	Default		%default
frénétiquement	100	advm	[pred="frénétiquement_____1",clivee=+,detach_neg = +,cat=adv]	frénétiquement_____1	Default		%default
fréquemment	100	advm	[pred="fréquemment_____1",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	fréquemment_____1	Default		%default
frêlement	100	advm	[pred="frêlement_____1",clivee=+,detach_neg = +,cat=adv]	frêlement_____1	Default		%default
fugacement	100	advm	[pred="fugacement_____1",clivee=+,cat=adv]	fugacement_____1	Default		%default
fugitivement	100	advm	[pred="fugitivement_____1",clivee=+,cat=adv]	fugitivement_____1	Default		%default
fuligineusement	100	advm	[pred="fuligineusement_____1",clivee=+,cat=adv]	fuligineusement_____1	Default		%default
fumeusement	100	advm	[pred="fumeusement_____1",clivee=+,detach_neg = +,cat=adv]	fumeusement_____1	Default		%default
funestement	100	advm	[pred="funestement_____1",clivee=+,detach_neg = +,cat=adv]	funestement_____1	Default		%default
funèbrement	100	advm	[pred="funèbrement_____1",clivee=+,cat=adv]	funèbrement_____1	Default		%default
funérairement	100	advm	[pred="funérairement_____1",clivee=+,cat=adv]	funérairement_____1	Default		%default
furibondement	100	advm	[pred="furibondement_____1",clivee=+,detach_neg = +,cat=adv]	furibondement_____1	Default		%default
furieusement	100	advm	[pred="furieusement_____1",advi=combien,cat=adv]	furieusement_____1	Default		%default
furieusement	100	advm	[pred="furieusement_____2",clivee=+,detach_neg = +,cat=adv]	furieusement_____2	Default		%default
furioso	100	adv	[pred="furioso_____1",cat=adv]	furioso_____1	Default		%default
furtivement	100	advm	[pred="furtivement_____1",clivee=+,cat=adv]	furtivement_____1	Default		%default
futilement	100	advm	[pred="futilement_____1",clivee=+,detach_neg = +,cat=adv]	futilement_____1	Default		%default
futurement	100	advm	[pred="futurement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	futurement_____1	Default		%default
fâcheusement	100	advm	[pred="fâcheusement_____1",clivee=+,cat=adv]	fâcheusement_____1	Default		%default
fâcheusement	100	advp	[pred="fâcheusement_____2",detach = +,detach_neg = +,cat=adv]	fâcheusement_____2	Default		%default
fébrilement	100	advm	[pred="fébrilement_____1",clivee=+,detach_neg = +,cat=adv]	fébrilement_____1	Default		%default
félinement	100	advm	[pred="félinement_____1",clivee=+,detach_neg = +,cat=adv]	félinement_____1	Default		%default
félonnement	100	advm	[pred="félonnement_____1",clivee=+,detach_neg = +,cat=adv]	félonnement_____1	Default		%default
fémininement	100	advm	[pred="fémininement_____1",clivee=+,detach_neg = +,cat=adv]	fémininement_____1	Default		%default
féodalement	100	advm	[pred="féodalement_____1",clivee=+,cat=adv]	féodalement_____1	Default		%default
férocement	100	advm	[pred="férocement_____1",advi=combien,clivee=+,cat=adv]	férocement_____1	Default		%default
férocement	100	advm	[pred="férocement_____2",clivee=+,detach_neg = +,cat=adv]	férocement_____2	Default		%default
fétidement	100	advm	[pred="fétidement_____1",clivee=+,cat=adv]	fétidement_____1	Default		%default
féériquement	100	advm	[pred="féériquement_____1",clivee=+,cat=adv]	féériquement_____1	Default		%default
gaillardement	100	advm	[pred="gaillardement_____1",clivee=+,detach_neg = +,cat=adv]	gaillardement_____1	Default		%default
galamment	100	advm	[pred="galamment_____1",clivee=+,detach_neg = +,cat=adv]	galamment_____1	Default		%default
gallicanement	100	advm	[pred="gallicanement_____1",clivee=+,detach_neg = +,cat=adv]	gallicanement_____1	Default		%default
galvaniquement	100	advm	[pred="galvaniquement_____1",clivee=+,cat=adv]	galvaniquement_____1	Default		%default
gaminement	100	advm	[pred="gaminement_____1",clivee=+,detach_neg = +,cat=adv]	gaminement_____1	Default		%default
gammathérapiquement	100	advm	[pred="gammathérapiquement_____1",clivee=+,cat=adv]	gammathérapiquement_____1	Default		%default
gargantualement	100	advm	[pred="gargantualement_____1",clivee=+,cat=adv]	gargantualement_____1	Default		%default
gastronomiquement	100	advm	[pred="gastronomiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	gastronomiquement_____1	Default		%default
gastronomiquement	100	advm	[pred="gastronomiquement_____2",clivee=+,cat=adv]	gastronomiquement_____2	Default		%default
gauchement	100	advm	[pred="gauchement_____1",clivee=+,detach_neg = +,cat=adv]	gauchement_____1	Default		%default
gauloisement	100	advm	[pred="gauloisement_____1",clivee=+,detach_neg = +,cat=adv]	gauloisement_____1	Default		%default
gaîment	100	advm	[pred="gaîment_____1",clivee=+,detach_neg = +,cat=adv]	gaîment_____1	Default		%default
geignardement	100	advm	[pred="geignardement_____1",clivee=+,detach_neg = +,cat=adv]	geignardement_____1	Default		%default
gentement	100	advm	[pred="gentement_____1",clivee=+,detach_neg = +,cat=adv]	gentement_____1	Default		%default
gentiment	100	advm	[pred="gentiment_____1",clivee=+,detach_neg = +,cat=adv]	gentiment_____1	Default		%default
gigantesquement	100	advm	[pred="gigantesquement_____1",advi=combien,cat=adv]	gigantesquement_____1	Default		%default
giratoirement	100	advm	[pred="giratoirement_____1",clivee=+,cat=adv]	giratoirement_____1	Default		%default
glacialement	100	advm	[pred="glacialement_____1",clivee=+,detach_neg = +,cat=adv]	glacialement_____1	Default		%default
glaireusement	100	advm	[pred="glaireusement_____1",clivee=+,cat=adv]	glaireusement_____1	Default		%default
glandulairement	100	advm	[pred="glandulairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	glandulairement_____1	Default		%default
glauquement	100	advm	[pred="glauquement_____1",clivee=+,cat=adv]	glauquement_____1	Default		%default
globalement	100	advm	[pred="globalement_____1",clivee=+,cat=adv]	globalement_____1	Default		%default
glorieusement	100	advm	[pred="glorieusement_____1",clivee=+,detach_neg = +,cat=adv]	glorieusement_____1	Default		%default
gloutonnement	100	advm	[pred="gloutonnement_____1",clivee=+,detach_neg = +,cat=adv]	gloutonnement_____1	Default		%default
gnostiquement	100	advm	[pred="gnostiquement_____1",clivee=+,cat=adv]	gnostiquement_____1	Default		%default
gnoséologiquement	100	advm	[pred="gnoséologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	gnoséologiquement_____1	Default		%default
godichement	100	advm	[pred="godichement_____1",clivee=+,detach_neg = +,cat=adv]	godichement_____1	Default		%default
goguenardement	100	advm	[pred="goguenardement_____1",clivee=+,detach_neg = +,cat=adv]	goguenardement_____1	Default		%default
goniométriquement	100	advm	[pred="goniométriquement_____1",clivee=+,cat=adv]	goniométriquement_____1	Default		%default
gothiquement	100	advm	[pred="gothiquement_____1",clivee=+,cat=adv]	gothiquement_____1	Default		%default
gouailleusement	100	advm	[pred="gouailleusement_____1",clivee=+,detach_neg = +,cat=adv]	gouailleusement_____1	Default		%default
goujatement	100	advm	[pred="goujatement_____1",clivee=+,detach_neg = +,cat=adv]	goujatement_____1	Default		%default
goulûment	100	advm	[pred="goulûment_____1",clivee=+,detach_neg = +,cat=adv]	goulûment_____1	Default		%default
gourdement	100	advm	[pred="gourdement_____1",clivee=+,detach_neg = +,cat=adv]	gourdement_____1	Default		%default
gourmandement	100	advm	[pred="gourmandement_____1",clivee=+,detach_neg = +,cat=adv]	gourmandement_____1	Default		%default
goutte à goutte	100	adv	[pred="goutte à goutte_____1",cat=adv]	goutte à goutte_____1	Default		%default
goutteusement	100	advm	[pred="goutteusement_____1",clivee=+,cat=adv]	goutteusement_____1	Default		%default
gouvernementalement	100	advm	[pred="gouvernementalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	gouvernementalement_____1	Default		%default
gouvernementalement	100	advm	[pred="gouvernementalement_____2",clivee=+,cat=adv]	gouvernementalement_____2	Default		%default
gracieusement	100	advm	[pred="gracieusement_____1",clivee=+,detach_neg = +,cat=adv]	gracieusement_____1	Default		%default
gracieusement	100	advm	[pred="gracieusement_____2",clivee=+,cat=adv]	gracieusement_____2	Default		%default
gracilement	100	advm	[pred="gracilement_____1",clivee=+,cat=adv]	gracilement_____1	Default		%default
graduellement	100	advm	[pred="graduellement_____1",clivee=+,cat=adv]	graduellement_____1	Default		%default
grammaticalement	100	advm	[pred="grammaticalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	grammaticalement_____1	Default		%default
grammaticalement	100	advm	[pred="grammaticalement_____2",clivee=+,cat=adv]	grammaticalement_____2	Default		%default
grandement	100	advm	[pred="grandement_____1",advi=combien,cat=adv]	grandement_____1	Default		%default
grandement	100	advm	[pred="grandement_____2",clivee=+,cat=adv]	grandement_____2	Default		%default
grandiosement	100	advm	[pred="grandiosement_____1",clivee=+,cat=adv]	grandiosement_____1	Default		%default
graphiquement	100	advm	[pred="graphiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	graphiquement_____1	Default		%default
graphiquement	100	advm	[pred="graphiquement_____2",clivee=+,cat=adv]	graphiquement_____2	Default		%default
graphologiquement	100	advm	[pred="graphologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	graphologiquement_____1	Default		%default
graphologiquement	100	advm	[pred="graphologiquement_____2",clivee=+,cat=adv]	graphologiquement_____2	Default		%default
gras	100	adv	[pred="gras_____1",cat=adv]	gras_____1	Default		%default
grassement	100	advm	[pred="grassement_____1",advi=combien,clivee=+,cat=adv]	grassement_____1	Default		%default
grassement	100	advm	[pred="grassement_____2",clivee=+,cat=adv]	grassement_____2	Default		%default
gratis	100	adv	[pred="gratis_____1",cat=adv]	gratis_____1	Default		%default
gratis pro deo	100	adv	[pred="gratis_____1",cat=adv]	gratis_____1	Default		%default
gratuitement	100	advm	[pred="gratuitement_____1",clivee=+,cat=adv]	gratuitement_____1	Default		%default
grave	100	adv	[pred="grave_____1",cat=adv]	grave_____1	Default		%default
graveleusement	100	advm	[pred="graveleusement_____1",clivee=+,detach_neg = +,cat=adv]	graveleusement_____1	Default		%default
gravement	100	advm	[pred="gravement_____1",advi=combien,clivee=+,cat=adv]	gravement_____1	Default		%default
gravement	100	advm	[pred="gravement_____2",clivee=+,detach_neg = +,cat=adv]	gravement_____2	Default		%default
grazioso	100	adv	[pred="grazioso_____1",cat=adv]	grazioso_____1	Default		%default
grincheusement	100	advm	[pred="grincheusement_____1",clivee=+,detach_neg = +,cat=adv]	grincheusement_____1	Default		%default
grivoisement	100	advm	[pred="grivoisement_____1",clivee=+,detach_neg = +,cat=adv]	grivoisement_____1	Default		%default
grièvement	100	advm	[pred="grièvement_____1",advi=combien,clivee=+,cat=adv]	grièvement_____1	Default		%default
grossement	100	advm	[pred="grossement_____1",clivee=+,cat=adv]	grossement_____1	Default		%default
grossièrement	100	advm	[pred="grossièrement_____1",clivee=+,detach_neg = +,cat=adv]	grossièrement_____1	Default		%default
grossièrement	100	advm	[pred="grossièrement_____2",clivee=+,cat=adv]	grossièrement_____2	Default		%default
grosso merdo	100	adv	[pred="grosso modo_____1",cat=adv]	grosso modo_____1	Default		%default
grosso modo	100	adv	[pred="grosso modo_____1",cat=adv]	grosso modo_____1	Default		%default
grotesquement	100	advm	[pred="grotesquement_____1",clivee=+,detach_neg = +,cat=adv]	grotesquement_____1	Default		%default
grégairement	100	advm	[pred="grégairement_____1",clivee=+,cat=adv]	grégairement_____1	Default		%default
guillerettement	100	advm	[pred="guillerettement_____1",clivee=+,detach_neg = +,cat=adv]	guillerettement_____1	Default		%default
gutturalement	100	advm	[pred="gutturalement_____1",clivee=+,cat=adv]	gutturalement_____1	Default		%default
guères	100	adv	[pred="guères_____1",cat=adv]	guères_____1	Default		%default
gymniquement	100	advm	[pred="gymniquement_____1",clivee=+,cat=adv]	gymniquement_____1	Default		%default
gynécologiquement	100	advm	[pred="gynécologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	gynécologiquement_____1	Default		%default
gynécologiquement	100	advm	[pred="gynécologiquement_____2",clivee=+,cat=adv]	gynécologiquement_____2	Default		%default
gyroscopiquement	100	advm	[pred="gyroscopiquement_____1",clivee=+,cat=adv]	gyroscopiquement_____1	Default		%default
gâteusement	100	advm	[pred="gâteusement_____1",clivee=+,detach_neg = +,cat=adv]	gâteusement_____1	Default		%default
gélatineusement	100	advm	[pred="gélatineusement_____1",clivee=+,cat=adv]	gélatineusement_____1	Default		%default
génialement	100	advm	[pred="génialement_____1",advi=combien,clivee=+,cat=adv]	génialement_____1	Default		%default
génitalement	100	advm	[pred="génitalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	génitalement_____1	Default		%default
génitalement	100	advm	[pred="génitalement_____2",clivee=+,cat=adv]	génitalement_____2	Default		%default
généalogiquement	100	advm	[pred="généalogiquement_____1",clivee=+,cat=adv]	généalogiquement_____1	Default		%default
généralement	100	advp	[pred="généralement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	généralement_____1	Default		%default
généreusement	100	advm	[pred="généreusement_____1",clivee=+,detach_neg = +,cat=adv]	généreusement_____1	Default		%default
généreusement	100	advp	[pred="généreusement_____2",detach = +,detach_neg = +,cat=adv]	généreusement_____2	Default		%default
génériquement	100	advm	[pred="génériquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	génériquement_____1	Default		%default
génériquement	100	advm	[pred="génériquement_____2",clivee=+,cat=adv]	génériquement_____2	Default		%default
génétiquement	100	advm	[pred="génétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	génétiquement_____1	Default		%default
génétiquement	100	advm	[pred="génétiquement_____2",clivee=+,cat=adv]	génétiquement_____2	Default		%default
géodynamiquement	100	advm	[pred="géodynamiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	géodynamiquement_____1	Default		%default
géodynamiquement	100	advm	[pred="géodynamiquement_____2",clivee=+,cat=adv]	géodynamiquement_____2	Default		%default
géodésiquement	100	advm	[pred="géodésiquement_____1",clivee=+,cat=adv]	géodésiquement_____1	Default		%default
géographiquement	100	advm	[pred="géographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	géographiquement_____1	Default		%default
géographiquement	100	advm	[pred="géographiquement_____2",clivee=+,cat=adv]	géographiquement_____2	Default		%default
géologiquement	100	advm	[pred="géologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	géologiquement_____1	Default		%default
géologiquement	100	advm	[pred="géologiquement_____2",clivee=+,cat=adv]	géologiquement_____2	Default		%default
géométralement	100	advm	[pred="géométralement_____1",clivee=+,cat=adv]	géométralement_____1	Default		%default
géométriquement	100	advm	[pred="géométriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	géométriquement_____1	Default		%default
géométriquement	100	advm	[pred="géométriquement_____2",clivee=+,cat=adv]	géométriquement_____2	Default		%default
géophysiquement	100	advm	[pred="géophysiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	géophysiquement_____1	Default		%default
géophysiquement	100	advm	[pred="géophysiquement_____2",clivee=+,cat=adv]	géophysiquement_____2	Default		%default
géopolitiquement	100	advm	[pred="géopolitiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	géopolitiquement_____1	Default		%default
géopolitiquement	100	advm	[pred="géopolitiquement_____2",clivee=+,cat=adv]	géopolitiquement_____2	Default		%default
ha	100	adv	[pred="ha_____1",cat=adv]	ha_____1	Default		%default
habilement	100	advm	[pred="habilement_____1",clivee=+,detach_neg = +,cat=adv]	habilement_____1	Default		%default
habilement	100	advp	[pred="habilement_____2",detach = +,detach_neg = +,cat=adv]	habilement_____2	Default		%default
habituellement	100	advp	[pred="habituellement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	habituellement_____1	Default		%default
hagardement	100	advm	[pred="hagardement_____1",clivee=+,detach_neg = +,cat=adv]	hagardement_____1	Default		%default
hagiographiquement	100	advm	[pred="hagiographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hagiographiquement_____1	Default		%default
hagiographiquement	100	advm	[pred="hagiographiquement_____2",clivee=+,cat=adv]	hagiographiquement_____2	Default		%default
haineusement	100	advm	[pred="haineusement_____1",clivee=+,detach_neg = +,cat=adv]	haineusement_____1	Default		%default
hardiment	100	advm	[pred="hardiment_____1",clivee=+,detach_neg = +,cat=adv]	hardiment_____1	Default		%default
hargneusement	100	advm	[pred="hargneusement_____1",clivee=+,detach_neg = +,cat=adv]	hargneusement_____1	Default		%default
harmonieusement	100	advm	[pred="harmonieusement_____1",clivee=+,cat=adv]	harmonieusement_____1	Default		%default
harmoniquement	100	advm	[pred="harmoniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	harmoniquement_____1	Default		%default
harmoniquement	100	advm	[pred="harmoniquement_____2",clivee=+,cat=adv]	harmoniquement_____2	Default		%default
hasardeusement	100	advm	[pred="hasardeusement_____1",clivee=+,cat=adv]	hasardeusement_____1	Default		%default
haut	100	adv	[pred="haut_____1",cat=adv]	haut_____1	Default		%default
haut la main	100	advp	[pred="haut la main_____1",cat=adv]	haut la main_____1	Default		%default
hautainement	100	advm	[pred="hautainement_____1",clivee=+,detach_neg = +,cat=adv]	hautainement_____1	Default		%default
hautement	100	advm	[pred="hautement_____1",advi=combien,cat=adv]	hautement_____1	Default		%default
hautement	100	advm	[pred="hautement_____2",clivee=+,cat=adv]	hautement_____2	Default		%default
haïssablement	100	advm	[pred="haïssablement_____1",advi=combien,clivee=+,cat=adv]	haïssablement_____1	Default		%default
haïssablement	100	advm	[pred="haïssablement_____2",clivee=+,detach_neg = +,cat=adv]	haïssablement_____2	Default		%default
hebdomadairement	100	advm	[pred="hebdomadairement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	hebdomadairement_____1	Default		%default
heptagonalement	100	advm	[pred="heptagonalement_____1",clivee=+,cat=adv]	heptagonalement_____1	Default		%default
herméneutiquement	100	advm	[pred="herméneutiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	herméneutiquement_____1	Default		%default
herméneutiquement	100	advm	[pred="herméneutiquement_____2",clivee=+,cat=adv]	herméneutiquement_____2	Default		%default
hermétiquement	100	advm	[pred="hermétiquement_____1",clivee=+,cat=adv]	hermétiquement_____1	Default		%default
heureusement	100	advm	[pred="heureusement_____1",clivee=+,detach_neg = +,cat=adv]	heureusement_____1	Default		%default
heureusement	100	advp	[pred="heureusement_____2<Suj:(scompl)>",detach = +,detach_neg = +,cat=adv]	heureusement_____2	Default		%default
heuristiquement	100	advm	[pred="heuristiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	heuristiquement_____1	Default		%default
hexagonalement	100	advm	[pred="hexagonalement_____1",clivee=+,cat=adv]	hexagonalement_____1	Default		%default
hic et nunc	100	adv	[pred="hic et nunc_____1",cat=adv]	hic et nunc_____1	Default		%default
hideusement	100	advm	[pred="hideusement_____1",advi=combien,clivee=+,cat=adv]	hideusement_____1	Default		%default
hier	100	adv	[pred="hier_____1",cat=adv]	hier_____1	Default		%default
hilarement	100	advm	[pred="hilarement_____1",clivee=+,detach_neg = +,cat=adv]	hilarement_____1	Default		%default
hippiatriquement	100	advm	[pred="hippiatriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hippiatriquement_____1	Default		%default
hippologiquement	100	advm	[pred="hippologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hippologiquement_____1	Default		%default
histologiquement	100	advm	[pred="histologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	histologiquement_____1	Default		%default
histologiquement	100	advm	[pred="histologiquement_____2",clivee=+,cat=adv]	histologiquement_____2	Default		%default
historiquement	100	advm	[pred="historiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	historiquement_____1	Default		%default
historiquement	100	advm	[pred="historiquement_____2",clivee=+,cat=adv]	historiquement_____2	Default		%default
hiérarchiquement	100	advm	[pred="hiérarchiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hiérarchiquement_____1	Default		%default
hiérarchiquement	100	advm	[pred="hiérarchiquement_____2",clivee=+,cat=adv]	hiérarchiquement_____2	Default		%default
hiératiquement	100	advm	[pred="hiératiquement_____1",clivee=+,cat=adv]	hiératiquement_____1	Default		%default
hiéroglyphiquement	100	advm	[pred="hiéroglyphiquement_____1",clivee=+,cat=adv]	hiéroglyphiquement_____1	Default		%default
homocentriquement	100	advm	[pred="homocentriquement_____1",clivee=+,cat=adv]	homocentriquement_____1	Default		%default
homographiquement	100	advm	[pred="homographiquement_____1",clivee=+,cat=adv]	homographiquement_____1	Default		%default
homogènement	100	advm	[pred="homogènement_____1",clivee=+,cat=adv]	homogènement_____1	Default		%default
homologiquement	100	advm	[pred="homologiquement_____1",clivee=+,cat=adv]	homologiquement_____1	Default		%default
homothétiquement	100	advm	[pred="homothétiquement_____1",clivee=+,cat=adv]	homothétiquement_____1	Default		%default
homéopathiquement	100	advm	[pred="homéopathiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	homéopathiquement_____1	Default		%default
homéopathiquement	100	advm	[pred="homéopathiquement_____2",clivee=+,cat=adv]	homéopathiquement_____2	Default		%default
homériquement	100	advm	[pred="homériquement_____1",clivee=+,cat=adv]	homériquement_____1	Default		%default
honnêtement	100	advm	[pred="honnêtement_____1",clivee=+,detach_neg = +,cat=adv]	honnêtement_____1	Default		%default
honnêtement	100	advp	[pred="honnêtement_____2",detach = +,detach_neg = +,cat=adv]	honnêtement_____2	Default		%default
honorablement	100	advm	[pred="honorablement_____1",clivee=+,cat=adv]	honorablement_____1	Default		%default
honorifiquement	100	advm	[pred="honorifiquement_____1",clivee=+,cat=adv]	honorifiquement_____1	Default		%default
honoris causa	100	adv	[pred="honoris causa_____1",cat=adv]	honoris causa_____1	Default		%default
honteusement	100	advm	[pred="honteusement_____1",clivee=+,cat=adv]	honteusement_____1	Default		%default
horizontalement	100	advm	[pred="horizontalement_____1",clivee=+,cat=adv]	horizontalement_____1	Default		%default
hormonalement	100	advm	[pred="hormonalement_____1",clivee=+,cat=adv]	hormonalement_____1	Default		%default
horriblement	100	advm	[pred="horriblement_____1",advi=combien,clivee=+,cat=adv]	horriblement_____1	Default		%default
hors	100	adv	[pred="hors_____1",cat=adv]	hors_____1	Default		%default
hospitalièrement	100	advm	[pred="hospitalièrement_____1",clivee=+,detach_neg = +,cat=adv]	hospitalièrement_____1	Default		%default
hostilement	100	advm	[pred="hostilement_____1",clivee=+,detach_neg = +,cat=adv]	hostilement_____1	Default		%default
houleusement	100	advm	[pred="houleusement_____1",clivee=+,cat=adv]	houleusement_____1	Default		%default
huileusement	100	advm	[pred="huileusement_____1",clivee=+,cat=adv]	huileusement_____1	Default		%default
huitièmement	100	advp	[pred="huitièmement_____1",detach = +,detach_neg = +,cat=adv]	huitièmement_____1	Default		%default
humainement	100	advm	[pred="humainement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	humainement_____1	Default		%default
humainement	100	advm	[pred="humainement_____2",clivee=+,detach_neg = +,cat=adv]	humainement_____2	Default		%default
humanitairement	100	advm	[pred="humanitairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	humanitairement_____1	Default		%default
humanitairement	100	advm	[pred="humanitairement_____2",clivee=+,cat=adv]	humanitairement_____2	Default		%default
humblement	100	advm	[pred="humblement_____1",clivee=+,detach_neg = +,cat=adv]	humblement_____1	Default		%default
humidement	100	advm	[pred="humidement_____1",clivee=+,cat=adv]	humidement_____1	Default		%default
humoristiquement	100	advm	[pred="humoristiquement_____1",clivee=+,cat=adv]	humoristiquement_____1	Default		%default
humoureusement	100	advm	[pred="humoureusement_____1",clivee=+,cat=adv]	humoureusement_____1	Default		%default
hydrauliquement	100	advm	[pred="hydrauliquement_____1",clivee=+,cat=adv]	hydrauliquement_____1	Default		%default
hydrodynamiquement	100	advm	[pred="hydrodynamiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hydrodynamiquement_____1	Default		%default
hydrodynamiquement	100	advm	[pred="hydrodynamiquement_____2",clivee=+,cat=adv]	hydrodynamiquement_____2	Default		%default
hydrographiquement	100	advm	[pred="hydrographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hydrographiquement_____1	Default		%default
hydrographiquement	100	advm	[pred="hydrographiquement_____2",clivee=+,cat=adv]	hydrographiquement_____2	Default		%default
hydrologiquement	100	advm	[pred="hydrologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hydrologiquement_____1	Default		%default
hydrologiquement	100	advm	[pred="hydrologiquement_____2",clivee=+,cat=adv]	hydrologiquement_____2	Default		%default
hydrométriquement	100	advm	[pred="hydrométriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hydrométriquement_____1	Default		%default
hydrométriquement	100	advm	[pred="hydrométriquement_____2",clivee=+,cat=adv]	hydrométriquement_____2	Default		%default
hydropneumatiquement	100	advm	[pred="hydropneumatiquement_____1",clivee=+,cat=adv]	hydropneumatiquement_____1	Default		%default
hydrostatiquement	100	advm	[pred="hydrostatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hydrostatiquement_____1	Default		%default
hydrostatiquement	100	advm	[pred="hydrostatiquement_____2",clivee=+,cat=adv]	hydrostatiquement_____2	Default		%default
hydrothérapiquement	100	advm	[pred="hydrothérapiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hydrothérapiquement_____1	Default		%default
hydrothérapiquement	100	advm	[pred="hydrothérapiquement_____2",clivee=+,cat=adv]	hydrothérapiquement_____2	Default		%default
hygiéniquement	100	advm	[pred="hygiéniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hygiéniquement_____1	Default		%default
hygiéniquement	100	advm	[pred="hygiéniquement_____2",clivee=+,cat=adv]	hygiéniquement_____2	Default		%default
hyperboliquement	100	advm	[pred="hyperboliquement_____1",clivee=+,detach_neg = +,cat=adv]	hyperboliquement_____1	Default		%default
hypercorrectement	100	advm	[pred="hypercorrectement_____1",clivee=+,cat=adv]	hypercorrectement_____1	Default		%default
hypnotiquement	100	advm	[pred="hypnotiquement_____1",clivee=+,cat=adv]	hypnotiquement_____1	Default		%default
hypocondriaquement	100	advm	[pred="hypocondriaquement_____1",clivee=+,detach_neg = +,cat=adv]	hypocondriaquement_____1	Default		%default
hypocoristiquement	100	advm	[pred="hypocoristiquement_____1",clivee=+,cat=adv]	hypocoristiquement_____1	Default		%default
hypocritement	100	advm	[pred="hypocritement_____1",clivee=+,detach_neg = +,cat=adv]	hypocritement_____1	Default		%default
hypodermiquement	100	advm	[pred="hypodermiquement_____1",clivee=+,cat=adv]	hypodermiquement_____1	Default		%default
hypostatiquement	100	advm	[pred="hypostatiquement_____1",clivee=+,cat=adv]	hypostatiquement_____1	Default		%default
hypothécairement	100	advm	[pred="hypothécairement_____1",cat=adv]	hypothécairement_____1	Default		%default
hypothétiquement	100	advm	[pred="hypothétiquement_____1",clivee=+,cat=adv]	hypothétiquement_____1	Default		%default
hystériquement	100	advm	[pred="hystériquement_____1",clivee=+,detach_neg = +,cat=adv]	hystériquement_____1	Default		%default
hâtivement	100	advm	[pred="hâtivement_____1",clivee=+,cat=adv]	hâtivement_____1	Default		%default
hébraïquement	100	advm	[pred="hébraïquement_____1",clivee=+,cat=adv]	hébraïquement_____1	Default		%default
héliaquement	100	advm	[pred="héliaquement_____1",clivee=+,cat=adv]	héliaquement_____1	Default		%default
hélicoïdalement	100	advm	[pred="hélicoïdalement_____1",clivee=+,cat=adv]	hélicoïdalement_____1	Default		%default
héliographiquement	100	advm	[pred="héliographiquement_____1",clivee=+,cat=adv]	héliographiquement_____1	Default		%default
hématologiquement	100	advm	[pred="hématologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hématologiquement_____1	Default		%default
hématologiquement	100	advm	[pred="hématologiquement_____2",clivee=+,cat=adv]	hématologiquement_____2	Default		%default
hémodynamiquement	100	advm	[pred="hémodynamiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	hémodynamiquement_____1	Default		%default
hémodynamiquement	100	advm	[pred="hémodynamiquement_____2",clivee=+,cat=adv]	hémodynamiquement_____2	Default		%default
héraldiquement	100	advm	[pred="héraldiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	héraldiquement_____1	Default		%default
héroïquement	100	advm	[pred="héroïquement_____1",clivee=+,detach_neg = +,cat=adv]	héroïquement_____1	Default		%default
héroïquement	100	advp	[pred="héroïquement_____2",detach = +,detach_neg = +,cat=adv]	héroïquement_____2	Default		%default
héréditairement	100	advm	[pred="héréditairement_____1",clivee=+,cat=adv]	héréditairement_____1	Default		%default
hérétiquement	100	advm	[pred="hérétiquement_____1",clivee=+,detach_neg = +,cat=adv]	hérétiquement_____1	Default		%default
hétéroclitement	100	advm	[pred="hétéroclitement_____1",clivee=+,cat=adv]	hétéroclitement_____1	Default		%default
i.e.	100	adv	[pred="id est_____1",cat=adv]	id est_____1	Default		%default
ibid..	100	adv	[pred="ibidem_____1",cat=adv]	ibidem_____1	Default		%default
ibidem	100	adv	[pred="ibidem_____1",cat=adv]	ibidem_____1	Default		%default
ichtyologiquement	100	advm	[pred="ichtyologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ichtyologiquement_____1	Default		%default
ici	100	adv	[pred="ici_____1",cat=adv]	ici_____1	Default		%default
ici-bas	100	adv	[pred="ici-bas_____1",cat=adv]	ici-bas_____1	Default		%default
iconiquement	100	advm	[pred="iconiquement_____1",clivee=+,cat=adv]	iconiquement_____1	Default		%default
iconographiquement	100	advm	[pred="iconographiquement_____1",clivee=+,cat=adv]	iconographiquement_____1	Default		%default
id est	100	adv	[pred="id est_____1",cat=adv]	id est_____1	Default		%default
id..	100	adv	[pred="idem_____1",cat=adv]	idem_____1	Default		%default
idem	100	adv	[pred="idem_____1",cat=adv]	idem_____1	Default		%default
identiquement	100	advm	[pred="identiquement_____1",clivee=+,cat=adv]	identiquement_____1	Default		%default
idiomatiquement	100	advm	[pred="idiomatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	idiomatiquement_____1	Default		%default
idiomatiquement	100	advm	[pred="idiomatiquement_____2",clivee=+,cat=adv]	idiomatiquement_____2	Default		%default
idiosyncrasiquement	100	advm	[pred="idiosyncrasiquement_____1",clivee=+,cat=adv]	idiosyncrasiquement_____1	Default		%default
idiosyncratiquement	100	advm	[pred="idiosyncratiquement_____1",clivee=+,cat=adv]	idiosyncratiquement_____1	Default		%default
idiotement	100	advm	[pred="idiotement_____1",clivee=+,detach_neg = +,cat=adv]	idiotement_____1	Default		%default
idiotement	100	advp	[pred="idiotement_____2",detach = +,detach_neg = +,cat=adv]	idiotement_____2	Default		%default
idolatriquement	100	advm	[pred="idolatriquement_____1",clivee=+,cat=adv]	idolatriquement_____1	Default		%default
idolâtrement	100	advm	[pred="idolâtrement_____1",clivee=+,detach_neg = +,cat=adv]	idolâtrement_____1	Default		%default
idylliquement	100	advm	[pred="idylliquement_____1",clivee=+,cat=adv]	idylliquement_____1	Default		%default
idéalement	100	advm	[pred="idéalement_____1",advi=combien,clivee=+,cat=adv]	idéalement_____1	Default		%default
idéalement	100	advm	[pred="idéalement_____2",clivee=+,cat=adv]	idéalement_____2	Default		%default
idéalistement	100	advm	[pred="idéalistement_____1",clivee=+,detach_neg = +,cat=adv]	idéalistement_____1	Default		%default
idéellement	100	advm	[pred="idéellement_____1",clivee=+,cat=adv]	idéellement_____1	Default		%default
idéographiquement	100	advm	[pred="idéographiquement_____1",clivee=+,cat=adv]	idéographiquement_____1	Default		%default
idéologiquement	100	advm	[pred="idéologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	idéologiquement_____1	Default		%default
ignarement	100	advm	[pred="ignarement_____1",clivee=+,detach_neg = +,cat=adv]	ignarement_____1	Default		%default
ignoblement	100	advm	[pred="ignoblement_____1",clivee=+,detach_neg = +,cat=adv]	ignoblement_____1	Default		%default
ignominieusement	100	advm	[pred="ignominieusement_____1",clivee=+,cat=adv]	ignominieusement_____1	Default		%default
ignoramment	100	advm	[pred="ignoramment_____1",clivee=+,detach_neg = +,cat=adv]	ignoramment_____1	Default		%default
illicitement	100	advm	[pred="illicitement_____1",clivee=+,cat=adv]	illicitement_____1	Default		%default
illico	100	adv	[pred="illico_____1",cat=adv]	illico_____1	Default		%default
illisiblement	100	advm	[pred="illisiblement_____1",clivee=+,cat=adv]	illisiblement_____1	Default		%default
illogiquement	100	advm	[pred="illogiquement_____1",clivee=+,cat=adv]	illogiquement_____1	Default		%default
illusoirement	100	advm	[pred="illusoirement_____1",clivee=+,cat=adv]	illusoirement_____1	Default		%default
illustrement	100	advm	[pred="illustrement_____1",clivee=+,cat=adv]	illustrement_____1	Default		%default
illégalement	100	advm	[pred="illégalement_____1",clivee=+,cat=adv]	illégalement_____1	Default		%default
illégitimement	100	advm	[pred="illégitimement_____1",clivee=+,cat=adv]	illégitimement_____1	Default		%default
imaginairement	100	advm	[pred="imaginairement_____1",clivee=+,cat=adv]	imaginairement_____1	Default		%default
imaginativement	100	advm	[pred="imaginativement_____1",clivee=+,cat=adv]	imaginativement_____1	Default		%default
imbécilement	100	advm	[pred="imbécilement_____1",clivee=+,detach_neg = +,cat=adv]	imbécilement_____1	Default		%default
imbécilement	100	advp	[pred="imbécilement_____2",detach = +,detach_neg = +,cat=adv]	imbécilement_____2	Default		%default
immaculément	100	advm	[pred="immaculément_____1",advi=combien,cat=adv]	immaculément_____1	Default		%default
immanquablement	100	advp	[pred="immanquablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	immanquablement_____1	Default		%default
immatériellement	100	advm	[pred="immatériellement_____1",clivee=+,cat=adv]	immatériellement_____1	Default		%default
immensément	100	advm	[pred="immensément_____1",advi=combien,cat=adv]	immensément_____1	Default		%default
imminement	100	advm	[pred="imminement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	imminement_____1	Default		%default
immobilement	100	advm	[pred="immobilement_____1",clivee=+,cat=adv]	immobilement_____1	Default		%default
immodestement	100	advm	[pred="immodestement_____1",clivee=+,detach_neg = +,cat=adv]	immodestement_____1	Default		%default
immodérément	100	advm	[pred="immodérément_____1",advi=combien,clivee=+,cat=adv]	immodérément_____1	Default		%default
immondement	100	advm	[pred="immondement_____1",clivee=+,detach_neg = +,cat=adv]	immondement_____1	Default		%default
immoralement	100	advm	[pred="immoralement_____1",clivee=+,detach_neg = +,cat=adv]	immoralement_____1	Default		%default
immortellement	100	advm	[pred="immortellement_____1",clivee=+,detach_neg = +,cat=adv]	immortellement_____1	Default		%default
immuablement	100	advm	[pred="immuablement_____1",clivee=+,cat=adv]	immuablement_____1	Default		%default
immunitairement	100	advm	[pred="immunitairement_____1",clivee=+,cat=adv]	immunitairement_____1	Default		%default
immunologiquement	100	advm	[pred="immunologiquement_____1",clivee=+,cat=adv]	immunologiquement_____1	Default		%default
immédiatement	100	advm	[pred="immédiatement_____1",clivee=+,cat=adv]	immédiatement_____1	Default		%default
immémoriablement	100	advm	[pred="immémoriablement_____1",clivee=+,cat=adv]	immémoriablement_____1	Default		%default
immémorialement	100	advm	[pred="immémorialement_____1",clivee=+,cat=adv]	immémorialement_____1	Default		%default
impairement	100	advm	[pred="impairement_____1",clivee=+,cat=adv]	impairement_____1	Default		%default
impalpablement	100	advm	[pred="impalpablement_____1",clivee=+,cat=adv]	impalpablement_____1	Default		%default
imparablement	100	advm	[pred="imparablement_____1",clivee=+,cat=adv]	imparablement_____1	Default		%default
impardonnablement	100	advm	[pred="impardonnablement_____1",clivee=+,detach_neg = +,cat=adv]	impardonnablement_____1	Default		%default
imparfaitement	100	advm	[pred="imparfaitement_____1",clivee=+,cat=adv]	imparfaitement_____1	Default		%default
impartialement	100	advm	[pred="impartialement_____1",clivee=+,detach_neg = +,cat=adv]	impartialement_____1	Default		%default
impassiblement	100	advm	[pred="impassiblement_____1",clivee=+,detach_neg = +,cat=adv]	impassiblement_____1	Default		%default
impatiemment	100	advm	[pred="impatiemment_____1",clivee=+,detach_neg = +,cat=adv]	impatiemment_____1	Default		%default
impavidement	100	advm	[pred="impavidement_____1",clivee=+,detach_neg = +,cat=adv]	impavidement_____1	Default		%default
impayablement	100	advm	[pred="impayablement_____1",advi=combien,clivee=+,cat=adv]	impayablement_____1	Default		%default
impeccablement	100	advm	[pred="impeccablement_____1",advi=combien,clivee=+,cat=adv]	impeccablement_____1	Default		%default
impensablement	100	advm	[pred="impensablement_____1",clivee=+,cat=adv]	impensablement_____1	Default		%default
imperceptiblement	100	advm	[pred="imperceptiblement_____1",advi=combien,clivee=+,cat=adv]	imperceptiblement_____1	Default		%default
impersonnellement	100	advm	[pred="impersonnellement_____1",clivee=+,detach_neg = +,cat=adv]	impersonnellement_____1	Default		%default
impertinemment	100	advm	[pred="impertinemment_____1",clivee=+,detach_neg = +,cat=adv]	impertinemment_____1	Default		%default
imperturbablement	100	advm	[pred="imperturbablement_____1",clivee=+,detach_neg = +,cat=adv]	imperturbablement_____1	Default		%default
impitoyablement	100	advm	[pred="impitoyablement_____1",clivee=+,detach_neg = +,cat=adv]	impitoyablement_____1	Default		%default
implacablement	100	advm	[pred="implacablement_____1",advi=combien,clivee=+,cat=adv]	implacablement_____1	Default		%default
implacablement	100	advm	[pred="implacablement_____2",clivee=+,detach_neg = +,cat=adv]	implacablement_____2	Default		%default
implicitement	100	advm	[pred="implicitement_____1",clivee=+,cat=adv]	implicitement_____1	Default		%default
impoliment	100	advm	[pred="impoliment_____1",clivee=+,detach_neg = +,cat=adv]	impoliment_____1	Default		%default
impolitiquement	100	advm	[pred="impolitiquement_____1",clivee=+,cat=adv]	impolitiquement_____1	Default		%default
impopulairement	100	advm	[pred="impopulairement_____1",clivee=+,detach_neg = +,cat=adv]	impopulairement_____1	Default		%default
importunément	100	advm	[pred="importunément_____1",clivee=+,detach_neg = +,cat=adv]	importunément_____1	Default		%default
impossiblement	100	advm	[pred="impossiblement_____1",clivee=+,cat=adv]	impossiblement_____1	Default		%default
imprescriptiblement	100	advm	[pred="imprescriptiblement_____1",clivee=+,cat=adv]	imprescriptiblement_____1	Default		%default
impressivement	100	advm	[pred="impressivement_____1",advi=combien,clivee=+,cat=adv]	impressivement_____1	Default		%default
improbablement	100	advm	[pred="improbablement_____1",clivee=+,cat=adv]	improbablement_____1	Default		%default
improductivement	100	advm	[pred="improductivement_____1",clivee=+,cat=adv]	improductivement_____1	Default		%default
impromptu	100	adv	[pred="impromptu_____1",cat=adv]	impromptu_____1	Default		%default
improprement	100	advm	[pred="improprement_____1",clivee=+,cat=adv]	improprement_____1	Default		%default
imprudemment	100	advm	[pred="imprudemment_____1",clivee=+,detach_neg = +,cat=adv]	imprudemment_____1	Default		%default
imprudemment	100	advp	[pred="imprudemment_____2",detach = +,detach_neg = +,cat=adv]	imprudemment_____2	Default		%default
imprécisément	100	advm	[pred="imprécisément_____1",clivee=+,detach_neg = +,cat=adv]	imprécisément_____1	Default		%default
imprévisiblement	100	advm	[pred="imprévisiblement_____1",clivee=+,cat=adv]	imprévisiblement_____1	Default		%default
impudemment	100	advm	[pred="impudemment_____1",clivee=+,detach_neg = +,cat=adv]	impudemment_____1	Default		%default
impudiquement	100	advm	[pred="impudiquement_____1",clivee=+,detach_neg = +,cat=adv]	impudiquement_____1	Default		%default
impulsivement	100	advm	[pred="impulsivement_____1",clivee=+,detach_neg = +,cat=adv]	impulsivement_____1	Default		%default
impunément	100	advm	[pred="impunément_____1",clivee=+,cat=adv]	impunément_____1	Default		%default
impurement	100	advm	[pred="impurement_____1",clivee=+,detach_neg = +,cat=adv]	impurement_____1	Default		%default
impénétrablement	100	advm	[pred="impénétrablement_____1",clivee=+,detach_neg = +,cat=adv]	impénétrablement_____1	Default		%default
impérativement	100	advm	[pred="impérativement_____1",clivee=+,cat=adv]	impérativement_____1	Default		%default
impérialement	100	advm	[pred="impérialement_____1",clivee=+,cat=adv]	impérialement_____1	Default		%default
impérieusement	100	advm	[pred="impérieusement_____1",clivee=+,detach_neg = +,cat=adv]	impérieusement_____1	Default		%default
impérissablement	100	advm	[pred="impérissablement_____1",clivee=+,cat=adv]	impérissablement_____1	Default		%default
impétueusement	100	advm	[pred="impétueusement_____1",clivee=+,detach_neg = +,cat=adv]	impétueusement_____1	Default		%default
in abstracto	100	adv	[pred="in abstracto_____1",cat=adv]	in abstracto_____1	Default		%default
in extenso	100	adv	[pred="in extenso_____1",chunk_type=GP,cat=adv]	in extenso_____1	Default		%default
in extremis	100	adv	[pred="in extremis_____1",cat=adv]	in extremis_____1	Default		%default
in fine	100	adv	[pred="in fine_____1",cat=adv]	in fine_____1	Default		%default
in petto	100	adv	[pred="in petto_____1",cat=adv]	in petto_____1	Default		%default
in praesenti	100	adv	[pred="in praesenti_____1",cat=adv]	in praesenti_____1	Default		%default
in praesentia	100	adv	[pred="in praesentia_____1",cat=adv]	in praesentia_____1	Default		%default
in sillico	100	adv	[pred="in sillico_____1",cat=adv]	in sillico_____1	Default		%default
in situ	100	adv	[pred="in situ_____1",cat=adv]	in situ_____1	Default		%default
in spiritu	100	adv	[pred="in spiritu_____1",cat=adv]	in spiritu_____1	Default		%default
in utero	100	adv	[pred="in utero_____1",cat=adv]	in utero_____1	Default		%default
in vitro	100	adv	[pred="in vitro_____1",cat=adv]	in vitro_____1	Default		%default
in vivo	100	adv	[pred="in vivo_____1",cat=adv]	in vivo_____1	Default		%default
inacceptablement	100	advm	[pred="inacceptablement_____1",clivee=+,cat=adv]	inacceptablement_____1	Default		%default
inaccessiblement	100	advm	[pred="inaccessiblement_____1",clivee=+,cat=adv]	inaccessiblement_____1	Default		%default
inactivement	100	advm	[pred="inactivement_____1",clivee=+,detach_neg = +,cat=adv]	inactivement_____1	Default		%default
inadmissiblement	100	advm	[pred="inadmissiblement_____1",advi=combien,clivee=+,cat=adv]	inadmissiblement_____1	Default		%default
inadéquatement	100	advm	[pred="inadéquatement_____1",clivee=+,cat=adv]	inadéquatement_____1	Default		%default
inaliénablement	100	advm	[pred="inaliénablement_____1",clivee=+,cat=adv]	inaliénablement_____1	Default		%default
inaltérablement	100	advm	[pred="inaltérablement_____1",clivee=+,cat=adv]	inaltérablement_____1	Default		%default
inamicalement	100	advm	[pred="inamicalement_____1",clivee=+,detach_neg = +,cat=adv]	inamicalement_____1	Default		%default
inamoviblement	100	advm	[pred="inamoviblement_____1",clivee=+,detach_neg = +,cat=adv]	inamoviblement_____1	Default		%default
inappréciablement	100	advm	[pred="inappréciablement_____1",advi=combien,clivee=+,cat=adv]	inappréciablement_____1	Default		%default
inattaquablement	100	advm	[pred="inattaquablement_____1",clivee=+,detach_neg = +,cat=adv]	inattaquablement_____1	Default		%default
inattentivement	100	advm	[pred="inattentivement_____1",clivee=+,detach_neg = +,cat=adv]	inattentivement_____1	Default		%default
inaudiblement	100	advm	[pred="inaudiblement_____1",clivee=+,cat=adv]	inaudiblement_____1	Default		%default
inauguralement	100	advm	[pred="inauguralement_____1",clivee=+,cat=adv]	inauguralement_____1	Default		%default
inauthentiquement	100	advm	[pred="inauthentiquement_____1",clivee=+,detach_neg = +,cat=adv]	inauthentiquement_____1	Default		%default
inavouablement	100	advm	[pred="inavouablement_____1",clivee=+,cat=adv]	inavouablement_____1	Default		%default
incalculablement	100	advm	[pred="incalculablement_____1",advi=combien,cat=adv]	incalculablement_____1	Default		%default
incapablement	100	advm	[pred="incapablement_____1",clivee=+,detach_neg = +,cat=adv]	incapablement_____1	Default		%default
incertainement	100	advm	[pred="incertainement_____1",clivee=+,cat=adv]	incertainement_____1	Default		%default
incessamment	100	advm	[pred="incessamment_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	incessamment_____1	Default		%default
incestueusement	100	advm	[pred="incestueusement_____1",clivee=+,detach_neg = +,cat=adv]	incestueusement_____1	Default		%default
incidemment	100	advm	[pred="incidemment_____1",clivee=+,cat=adv]	incidemment_____1	Default		%default
incisivement	100	advm	[pred="incisivement_____1",clivee=+,detach_neg = +,cat=adv]	incisivement_____1	Default		%default
incivilement	100	advm	[pred="incivilement_____1",clivee=+,detach_neg = +,cat=adv]	incivilement_____1	Default		%default
inciviquement	100	advm	[pred="inciviquement_____1",clivee=+,detach_neg = +,cat=adv]	inciviquement_____1	Default		%default
inclusivement	100	advm	[pred="inclusivement_____1",modnc = +,cat=adv]	inclusivement_____1	Default		%default
incoerciblement	100	advm	[pred="incoerciblement_____1",clivee=+,cat=adv]	incoerciblement_____1	Default		%default
incognito	100	adv	[pred="incognito_____1",cat=adv]	incognito_____1	Default		%default
incommensurablement	100	advm	[pred="incommensurablement_____1",advi=combien,cat=adv]	incommensurablement_____1	Default		%default
incommodément	100	advm	[pred="incommodément_____1",clivee=+,cat=adv]	incommodément_____1	Default		%default
incommutablement	100	advm	[pred="incommutablement_____1",clivee=+,cat=adv]	incommutablement_____1	Default		%default
incomparablement	100	advm	[pred="incomparablement_____1",advi=combien,cat=adv]	incomparablement_____1	Default		%default
incomplètement	100	advm	[pred="incomplètement_____1",advi=combien,cat=adv]	incomplètement_____1	Default		%default
incompréhensiblement	100	advm	[pred="incompréhensiblement_____1",clivee=+,cat=adv]	incompréhensiblement_____1	Default		%default
incompétemment	100	advm	[pred="incompétemment_____1",clivee=+,detach_neg = +,cat=adv]	incompétemment_____1	Default		%default
inconcevablement	100	advm	[pred="inconcevablement_____1",advi=combien,clivee=+,cat=adv]	inconcevablement_____1	Default		%default
inconciliablement	100	advm	[pred="inconciliablement_____1",clivee=+,cat=adv]	inconciliablement_____1	Default		%default
inconditionnellement	100	advm	[pred="inconditionnellement_____1",clivee=+,cat=adv]	inconditionnellement_____1	Default		%default
inconfortablement	100	advm	[pred="inconfortablement_____1",clivee=+,cat=adv]	inconfortablement_____1	Default		%default
incongrûment	100	advm	[pred="incongrûment_____1",clivee=+,detach_neg = +,cat=adv]	incongrûment_____1	Default		%default
inconsciemment	100	advm	[pred="inconsciemment_____1",clivee=+,detach_neg = +,cat=adv]	inconsciemment_____1	Default		%default
inconsidérément	100	advm	[pred="inconsidérément_____1",clivee=+,cat=adv]	inconsidérément_____1	Default		%default
inconsolablement	100	advm	[pred="inconsolablement_____1",clivee=+,detach_neg = +,cat=adv]	inconsolablement_____1	Default		%default
inconstamment	100	advm	[pred="inconstamment_____1",clivee=+,detach_neg = +,cat=adv]	inconstamment_____1	Default		%default
inconstitutionnellement	100	advm	[pred="inconstitutionnellement_____1",clivee=+,cat=adv]	inconstitutionnellement_____1	Default		%default
inconséquemment	100	advm	[pred="inconséquemment_____1",clivee=+,detach_neg = +,cat=adv]	inconséquemment_____1	Default		%default
incontestablement	100	advp	[pred="incontestablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	incontestablement_____1	Default		%default
incontinent	100	adv	[pred="incontinent_____1",cat=adv]	incontinent_____1	Default		%default
incontournablement	100	advm	[pred="incontournablement_____1",clivee=+,cat=adv]	incontournablement_____1	Default		%default
incontrôlablement	100	advm	[pred="incontrôlablement_____1",clivee=+,cat=adv]	incontrôlablement_____1	Default		%default
inconvenablement	100	advm	[pred="inconvenablement_____1",clivee=+,cat=adv]	inconvenablement_____1	Default		%default
incorporellement	100	advm	[pred="incorporellement_____1",clivee=+,cat=adv]	incorporellement_____1	Default		%default
incorrectement	100	advm	[pred="incorrectement_____1",clivee=+,detach_neg = +,cat=adv]	incorrectement_____1	Default		%default
incorrectement	100	advm	[pred="incorrectement_____2",clivee=+,cat=adv]	incorrectement_____2	Default		%default
incorrigiblement	100	advm	[pred="incorrigiblement_____1",advi=combien,clivee=+,cat=adv]	incorrigiblement_____1	Default		%default
incorruptiblement	100	advm	[pred="incorruptiblement_____1",advi=combien,clivee=+,cat=adv]	incorruptiblement_____1	Default		%default
increvablement	100	advm	[pred="increvablement_____1",clivee=+,detach_neg = +,cat=adv]	increvablement_____1	Default		%default
incroyablement	100	advm	[pred="incroyablement_____1",advi=combien,clivee=+,cat=adv]	incroyablement_____1	Default		%default
incrédulement	100	advm	[pred="incrédulement_____1",clivee=+,detach_neg = +,cat=adv]	incrédulement_____1	Default		%default
incurablement	100	advm	[pred="incurablement_____1",advi=combien,clivee=+,cat=adv]	incurablement_____1	Default		%default
indescriptiblement	100	advm	[pred="indescriptiblement_____1",advi=combien,clivee=+,cat=adv]	indescriptiblement_____1	Default		%default
indestructiblement	100	advm	[pred="indestructiblement_____1",advi=combien,clivee=+,cat=adv]	indestructiblement_____1	Default		%default
indicativement	100	advm	[pred="indicativement_____1",clivee=+,cat=adv]	indicativement_____1	Default		%default
indiciblement	100	advm	[pred="indiciblement_____1",advi=combien,clivee=+,cat=adv]	indiciblement_____1	Default		%default
indifféremment	100	advm	[pred="indifféremment_____1",clivee=+,cat=adv]	indifféremment_____1	Default		%default
indigemment	100	advm	[pred="indigemment_____1",clivee=+,detach_neg = +,cat=adv]	indigemment_____1	Default		%default
indigestement	100	advm	[pred="indigestement_____1",clivee=+,cat=adv]	indigestement_____1	Default		%default
indignement	100	advm	[pred="indignement_____1",clivee=+,cat=adv]	indignement_____1	Default		%default
indirectement	100	advm	[pred="indirectement_____1",clivee=+,cat=adv]	indirectement_____1	Default		%default
indiscernablement	100	advm	[pred="indiscernablement_____1",clivee=+,cat=adv]	indiscernablement_____1	Default		%default
indiscontinûment	100	advm	[pred="indiscontinûment_____1",clivee=+,cat=adv]	indiscontinûment_____1	Default		%default
indiscrètement	100	advm	[pred="indiscrètement_____1",clivee=+,detach_neg = +,cat=adv]	indiscrètement_____1	Default		%default
indiscutablement	100	advp	[pred="indiscutablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	indiscutablement_____1	Default		%default
indispensablement	100	advm	[pred="indispensablement_____1",clivee=+,cat=adv]	indispensablement_____1	Default		%default
indissociablement	100	advm	[pred="indissociablement_____1",clivee=+,cat=adv]	indissociablement_____1	Default		%default
indissolublement	100	advm	[pred="indissolublement_____1",clivee=+,cat=adv]	indissolublement_____1	Default		%default
indistinctement	100	advm	[pred="indistinctement_____1",clivee=+,cat=adv]	indistinctement_____1	Default		%default
indistinctement	100	advm	[pred="indistinctement_____2",clivee=+,cat=adv]	indistinctement_____2	Default		%default
individuellement	100	advm	[pred="individuellement_____1",clivee=+,cat=adv]	individuellement_____1	Default		%default
indivisiblement	100	advm	[pred="indivisiblement_____1",clivee=+,cat=adv]	indivisiblement_____1	Default		%default
indivisément	100	advm	[pred="indivisément_____1",clivee=+,cat=adv]	indivisément_____1	Default		%default
indocilement	100	advm	[pred="indocilement_____1",clivee=+,detach_neg = +,cat=adv]	indocilement_____1	Default		%default
indolemment	100	advm	[pred="indolemment_____1",clivee=+,detach_neg = +,cat=adv]	indolemment_____1	Default		%default
indomptablement	100	advm	[pred="indomptablement_____1",advi=combien,clivee=+,cat=adv]	indomptablement_____1	Default		%default
indubitablement	100	advp	[pred="indubitablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	indubitablement_____1	Default		%default
inductivement	100	advm	[pred="inductivement_____1",clivee=+,cat=adv]	inductivement_____1	Default		%default
indulgemment	100	advm	[pred="indulgemment_____1",clivee=+,detach_neg = +,cat=adv]	indulgemment_____1	Default		%default
industriellement	100	advm	[pred="industriellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	industriellement_____1	Default		%default
industriellement	100	advm	[pred="industriellement_____2",clivee=+,cat=adv]	industriellement_____2	Default		%default
industrieusement	100	advm	[pred="industrieusement_____1",clivee=+,detach_neg = +,cat=adv]	industrieusement_____1	Default		%default
indécelablement	100	advm	[pred="indécelablement_____1",clivee=+,cat=adv]	indécelablement_____1	Default		%default
indécemment	100	advm	[pred="indécemment_____1",clivee=+,detach_neg = +,cat=adv]	indécemment_____1	Default		%default
indécrottablement	100	advm	[pred="indécrottablement_____1",advi=combien,clivee=+,cat=adv]	indécrottablement_____1	Default		%default
indéfectiblement	100	advm	[pred="indéfectiblement_____1",advi=combien,clivee=+,cat=adv]	indéfectiblement_____1	Default		%default
indéfiniment	100	advm	[pred="indéfiniment_____1",advi=en_combien_de_temps,clivee=+,detach_neg = +,cat=adv]	indéfiniment_____1	Default		%default
indéfiniment	100	advm	[pred="indéfiniment_____2",clivee=+,cat=adv]	indéfiniment_____2	Default		%default
indéfinissablement	100	advm	[pred="indéfinissablement_____1",clivee=+,cat=adv]	indéfinissablement_____1	Default		%default
indélicatement	100	advm	[pred="indélicatement_____1",clivee=+,detach_neg = +,cat=adv]	indélicatement_____1	Default		%default
indélébilement	100	advm	[pred="indélébilement_____1",clivee=+,cat=adv]	indélébilement_____1	Default		%default
indémontablement	100	advm	[pred="indémontablement_____1",advi=combien,clivee=+,cat=adv]	indémontablement_____1	Default		%default
indéniablement	100	advp	[pred="indéniablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	indéniablement_____1	Default		%default
indépassablement	100	advm	[pred="indépassablement_____1",advi=combien,clivee=+,cat=adv]	indépassablement_____1	Default		%default
indépendamment	100	advm	[pred="indépendamment_____1",clivee=+,detach_neg = +,cat=adv]	indépendamment_____1	Default		%default
indépendamment	100	advp	[pred="indépendamment_____2<Objde:(de-scompl|de-scompl|de-sinf|de-sn|de-sn)>",detach = +,detach_neg = +,cat=adv]	indépendamment_____2	Default		%default
indéracinablement	100	advm	[pred="indéracinablement_____1",advi=combien,clivee=+,cat=adv]	indéracinablement_____1	Default		%default
indésirablement	100	advm	[pred="indésirablement_____1",clivee=+,cat=adv]	indésirablement_____1	Default		%default
indéterminément	100	advm	[pred="indéterminément_____1",clivee=+,cat=adv]	indéterminément_____1	Default		%default
indévotement	100	advm	[pred="indévotement_____1",clivee=+,detach_neg = +,cat=adv]	indévotement_____1	Default		%default
indûment	100	advm	[pred="indûment_____1",clivee=+,cat=adv]	indûment_____1	Default		%default
ineffablement	100	advm	[pred="ineffablement_____1",advi=combien,clivee=+,cat=adv]	ineffablement_____1	Default		%default
ineffaçablement	100	advm	[pred="ineffaçablement_____1",clivee=+,cat=adv]	ineffaçablement_____1	Default		%default
inefficacement	100	advm	[pred="inefficacement_____1",clivee=+,cat=adv]	inefficacement_____1	Default		%default
ineptement	100	advm	[pred="ineptement_____1",clivee=+,cat=adv]	ineptement_____1	Default		%default
inertement	100	advm	[pred="inertement_____1",clivee=+,detach_neg = +,cat=adv]	inertement_____1	Default		%default
inespérément	100	advm	[pred="inespérément_____1",clivee=+,cat=adv]	inespérément_____1	Default		%default
inesthétiquement	100	advm	[pred="inesthétiquement_____1",clivee=+,cat=adv]	inesthétiquement_____1	Default		%default
inestimablement	100	advm	[pred="inestimablement_____1",advi=combien,clivee=+,cat=adv]	inestimablement_____1	Default		%default
inexactement	100	advm	[pred="inexactement_____1",clivee=+,cat=adv]	inexactement_____1	Default		%default
inexcusablement	100	advm	[pred="inexcusablement_____1",clivee=+,detach_neg = +,cat=adv]	inexcusablement_____1	Default		%default
inexorablement	100	advm	[pred="inexorablement_____1",clivee=+,cat=adv]	inexorablement_____1	Default		%default
inexpertement	100	advm	[pred="inexpertement_____1",clivee=+,detach_neg = +,cat=adv]	inexpertement_____1	Default		%default
inexpiablement	100	advm	[pred="inexpiablement_____1",advi=combien,clivee=+,cat=adv]	inexpiablement_____1	Default		%default
inexplicablement	100	advp	[pred="inexplicablement_____1",detach = +,detach_neg = +,cat=adv]	inexplicablement_____1	Default		%default
inexpliquablement	100	advm	[pred="inexpliquablement_____1",clivee=+,cat=adv]	inexpliquablement_____1	Default		%default
inexpressivement	100	advm	[pred="inexpressivement_____1",clivee=+,cat=adv]	inexpressivement_____1	Default		%default
inexprimablement	100	advm	[pred="inexprimablement_____1",advi=combien,clivee=+,cat=adv]	inexprimablement_____1	Default		%default
inexprimablement	100	advm	[pred="inexprimablement_____2",clivee=+,cat=adv]	inexprimablement_____2	Default		%default
inexpugnablement	100	advm	[pred="inexpugnablement_____1",clivee=+,cat=adv]	inexpugnablement_____1	Default		%default
inextinguiblement	100	advm	[pred="inextinguiblement_____1",advi=combien,clivee=+,cat=adv]	inextinguiblement_____1	Default		%default
inextirpablement	100	advm	[pred="inextirpablement_____1",advi=combien,clivee=+,cat=adv]	inextirpablement_____1	Default		%default
inextricablement	100	advm	[pred="inextricablement_____1",advi=combien,clivee=+,cat=adv]	inextricablement_____1	Default		%default
infailliblement	100	advp	[pred="infailliblement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	infailliblement_____1	Default		%default
infantilement	100	advm	[pred="infantilement_____1",clivee=+,cat=adv]	infantilement_____1	Default		%default
infatigablement	100	advm	[pred="infatigablement_____1",clivee=+,detach_neg = +,cat=adv]	infatigablement_____1	Default		%default
infectement	100	advm	[pred="infectement_____1",clivee=+,detach_neg = +,cat=adv]	infectement_____1	Default		%default
infernalement	100	advm	[pred="infernalement_____1",advi=combien,clivee=+,cat=adv]	infernalement_____1	Default		%default
infidèlement	100	advm	[pred="infidèlement_____1",clivee=+,detach_neg = +,cat=adv]	infidèlement_____1	Default		%default
infiniment	100	advm	[pred="infiniment_____1",advi=combien,cat=adv]	infiniment_____1	Default		%default
infinitésimalement	100	advm	[pred="infinitésimalement_____1",clivee=+,cat=adv]	infinitésimalement_____1	Default		%default
inflexiblement	100	advm	[pred="inflexiblement_____1",clivee=+,detach_neg = +,cat=adv]	inflexiblement_____1	Default		%default
informatiquement	100	advm	[pred="informatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	informatiquement_____1	Default		%default
informatiquement	100	advm	[pred="informatiquement_____2",clivee=+,cat=adv]	informatiquement_____2	Default		%default
informellement	100	advm	[pred="informellement_____1",clivee=+,cat=adv]	informellement_____1	Default		%default
infra	100	adv	[pred="infra_____1",cat=adv]	infra_____1	Default		%default
infructueusement	100	advm	[pred="infructueusement_____1",clivee=+,cat=adv]	infructueusement_____1	Default		%default
infâmement	100	advm	[pred="infâmement_____1",clivee=+,detach_neg = +,cat=adv]	infâmement_____1	Default		%default
inférieurement	100	advm	[pred="inférieurement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	inférieurement_____1	Default		%default
inglorieusement	100	advm	[pred="inglorieusement_____1",clivee=+,detach_neg = +,cat=adv]	inglorieusement_____1	Default		%default
ingratement	100	advm	[pred="ingratement_____1",clivee=+,detach_neg = +,cat=adv]	ingratement_____1	Default		%default
inguérissablement	100	advm	[pred="inguérissablement_____1",advi=combien,clivee=+,cat=adv]	inguérissablement_____1	Default		%default
ingénieusement	100	advm	[pred="ingénieusement_____1",clivee=+,detach_neg = +,cat=adv]	ingénieusement_____1	Default		%default
ingénument	100	advm	[pred="ingénument_____1",clivee=+,detach_neg = +,cat=adv]	ingénument_____1	Default		%default
inhabilement	100	advm	[pred="inhabilement_____1",clivee=+,detach_neg = +,cat=adv]	inhabilement_____1	Default		%default
inharmonieusement	100	advm	[pred="inharmonieusement_____1",clivee=+,cat=adv]	inharmonieusement_____1	Default		%default
inharmoniquement	100	advm	[pred="inharmoniquement_____1",clivee=+,cat=adv]	inharmoniquement_____1	Default		%default
inhospitalièrement	100	advm	[pred="inhospitalièrement_____1",clivee=+,detach_neg = +,cat=adv]	inhospitalièrement_____1	Default		%default
inhumainement	100	advm	[pred="inhumainement_____1",clivee=+,detach_neg = +,cat=adv]	inhumainement_____1	Default		%default
inimaginablement	100	advm	[pred="inimaginablement_____1",advi=combien,clivee=+,cat=adv]	inimaginablement_____1	Default		%default
inimitablement	100	advm	[pred="inimitablement_____1",advi=combien,clivee=+,cat=adv]	inimitablement_____1	Default		%default
inintelligemment	100	advm	[pred="inintelligemment_____1",clivee=+,detach_neg = +,cat=adv]	inintelligemment_____1	Default		%default
inintelligemment	100	advp	[pred="inintelligemment_____2",detach = +,detach_neg = +,cat=adv]	inintelligemment_____2	Default		%default
inintelligiblement	100	advm	[pred="inintelligiblement_____1",clivee=+,cat=adv]	inintelligiblement_____1	Default		%default
inintentionnellement	100	advm	[pred="inintentionnellement_____1",clivee=+,cat=adv]	inintentionnellement_____1	Default		%default
iniquement	100	advm	[pred="iniquement_____1",clivee=+,detach_neg = +,cat=adv]	iniquement_____1	Default		%default
initialement	100	advm	[pred="initialement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	initialement_____1	Default		%default
initiatiquement	100	advm	[pred="initiatiquement_____1",clivee=+,cat=adv]	initiatiquement_____1	Default		%default
injurieusement	100	advm	[pred="injurieusement_____1",clivee=+,detach_neg = +,cat=adv]	injurieusement_____1	Default		%default
injustement	100	advm	[pred="injustement_____1",clivee=+,detach_neg = +,cat=adv]	injustement_____1	Default		%default
inlassablement	100	advm	[pred="inlassablement_____1",clivee=+,detach_neg = +,cat=adv]	inlassablement_____1	Default		%default
innocemment	100	advm	[pred="innocemment_____1",clivee=+,detach_neg = +,cat=adv]	innocemment_____1	Default		%default
innombrablement	100	advm	[pred="innombrablement_____1",advi=combien,clivee=+,cat=adv]	innombrablement_____1	Default		%default
inoffensivement	100	advm	[pred="inoffensivement_____1",clivee=+,detach_neg = +,cat=adv]	inoffensivement_____1	Default		%default
inopinément	100	advm	[pred="inopinément_____1",clivee=+,cat=adv]	inopinément_____1	Default		%default
inopportunément	100	advm	[pred="inopportunément_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	inopportunément_____1	Default		%default
inoubliablement	100	advm	[pred="inoubliablement_____1",advi=combien,clivee=+,cat=adv]	inoubliablement_____1	Default		%default
inqualifiablement	100	advm	[pred="inqualifiablement_____1",advi=combien,clivee=+,cat=adv]	inqualifiablement_____1	Default		%default
inquisitorialement	100	advm	[pred="inquisitorialement_____1",clivee=+,cat=adv]	inquisitorialement_____1	Default		%default
inquiètement	100	advm	[pred="inquiètement_____1",clivee=+,detach_neg = +,cat=adv]	inquiètement_____1	Default		%default
insaisissablement	100	advm	[pred="insaisissablement_____1",clivee=+,cat=adv]	insaisissablement_____1	Default		%default
insalubrement	100	advm	[pred="insalubrement_____1",clivee=+,cat=adv]	insalubrement_____1	Default		%default
insanement	100	advm	[pred="insanement_____1",clivee=+,detach_neg = +,cat=adv]	insanement_____1	Default		%default
insatiablement	100	advm	[pred="insatiablement_____1",advi=combien,clivee=+,cat=adv]	insatiablement_____1	Default		%default
insciemment	100	advm	[pred="insciemment_____1",clivee=+,cat=adv]	insciemment_____1	Default		%default
insensiblement	100	advm	[pred="insensiblement_____1",clivee=+,cat=adv]	insensiblement_____1	Default		%default
insensément	100	advm	[pred="insensément_____1",clivee=+,detach_neg = +,cat=adv]	insensément_____1	Default		%default
insidieusement	100	advm	[pred="insidieusement_____1",clivee=+,cat=adv]	insidieusement_____1	Default		%default
insignement	100	advm	[pred="insignement_____1",advi=combien,clivee=+,cat=adv]	insignement_____1	Default		%default
insincèrement	100	advm	[pred="insincèrement_____1",clivee=+,detach_neg = +,cat=adv]	insincèrement_____1	Default		%default
insipidement	100	advm	[pred="insipidement_____1",clivee=+,detach_neg = +,cat=adv]	insipidement_____1	Default		%default
insolemment	100	advm	[pred="insolemment_____1",clivee=+,detach_neg = +,cat=adv]	insolemment_____1	Default		%default
insolitement	100	advm	[pred="insolitement_____1",clivee=+,cat=adv]	insolitement_____1	Default		%default
insondablement	100	advm	[pred="insondablement_____1",advi=combien,clivee=+,cat=adv]	insondablement_____1	Default		%default
insouciamment	100	advm	[pred="insouciamment_____1",clivee=+,detach_neg = +,cat=adv]	insouciamment_____1	Default		%default
insoucieusement	100	advm	[pred="insoucieusement_____1",clivee=+,detach_neg = +,cat=adv]	insoucieusement_____1	Default		%default
insoupçonnablement	100	advm	[pred="insoupçonnablement_____1",advi=combien,clivee=+,cat=adv]	insoupçonnablement_____1	Default		%default
insoutenablement	100	advm	[pred="insoutenablement_____1",advi=combien,clivee=+,cat=adv]	insoutenablement_____1	Default		%default
instablement	100	advm	[pred="instablement_____1",clivee=+,cat=adv]	instablement_____1	Default		%default
instamment	100	advm	[pred="instamment_____1",advi=combien,cat=adv]	instamment_____1	Default		%default
instantanément	100	advm	[pred="instantanément_____1",clivee=+,cat=adv]	instantanément_____1	Default		%default
instinctivement	100	advm	[pred="instinctivement_____1",clivee=+,cat=adv]	instinctivement_____1	Default		%default
institutionnellement	100	advm	[pred="institutionnellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	institutionnellement_____1	Default		%default
institutionnellement	100	advm	[pred="institutionnellement_____2",clivee=+,cat=adv]	institutionnellement_____2	Default		%default
instructivement	100	advm	[pred="instructivement_____1",clivee=+,cat=adv]	instructivement_____1	Default		%default
instrumentalement	100	advm	[pred="instrumentalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	instrumentalement_____1	Default		%default
instrumentalement	100	advm	[pred="instrumentalement_____2",clivee=+,cat=adv]	instrumentalement_____2	Default		%default
insuffisamment	100	advm	[pred="insuffisamment_____1<Obl:(pour-sn|pour-sinf|pour-scompl)>",adv_kind=intens,advi=combien,cat=adv]	insuffisamment_____1	Default		%default
insupportablement	100	advm	[pred="insupportablement_____1",advi=combien,clivee=+,cat=adv]	insupportablement_____1	Default		%default
insurmontablement	100	advm	[pred="insurmontablement_____1",advi=combien,clivee=+,cat=adv]	insurmontablement_____1	Default		%default
insurpassablement	100	advm	[pred="insurpassablement_____1",advi=combien,clivee=+,cat=adv]	insurpassablement_____1	Default		%default
insurrectionnellement	100	advm	[pred="insurrectionnellement_____1",clivee=+,cat=adv]	insurrectionnellement_____1	Default		%default
inséparablement	100	advm	[pred="inséparablement_____1",clivee=+,cat=adv]	inséparablement_____1	Default		%default
intangiblement	100	advm	[pred="intangiblement_____1",clivee=+,cat=adv]	intangiblement_____1	Default		%default
intarissablement	100	advm	[pred="intarissablement_____1",advi=combien,clivee=+,cat=adv]	intarissablement_____1	Default		%default
intellectuellement	100	advm	[pred="intellectuellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	intellectuellement_____1	Default		%default
intellectuellement	100	advm	[pred="intellectuellement_____2",clivee=+,cat=adv]	intellectuellement_____2	Default		%default
intelligemment	100	advm	[pred="intelligemment_____1",clivee=+,detach_neg = +,cat=adv]	intelligemment_____1	Default		%default
intelligemment	100	advp	[pred="intelligemment_____2",detach = +,detach_neg = +,cat=adv]	intelligemment_____2	Default		%default
intelligiblement	100	advm	[pred="intelligiblement_____1",clivee=+,cat=adv]	intelligiblement_____1	Default		%default
intempestivement	100	advm	[pred="intempestivement_____1",clivee=+,cat=adv]	intempestivement_____1	Default		%default
intemporellement	100	advm	[pred="intemporellement_____1",clivee=+,cat=adv]	intemporellement_____1	Default		%default
intenablement	100	advm	[pred="intenablement_____1",advi=combien,clivee=+,cat=adv]	intenablement_____1	Default		%default
intensionnellement	100	advm	[pred="intensionnellement_____1",clivee=+,cat=adv]	intensionnellement_____1	Default		%default
intensivement	100	advm	[pred="intensivement_____1",advi=combien,clivee=+,cat=adv]	intensivement_____1	Default		%default
intensément	100	advm	[pred="intensément_____1",advi=combien,clivee=+,cat=adv]	intensément_____1	Default		%default
intentionnellement	100	advm	[pred="intentionnellement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	intentionnellement_____1	Default		%default
intercalairement	100	advm	[pred="intercalairement_____1",clivee=+,cat=adv]	intercalairement_____1	Default		%default
interdisciplinairement	100	advm	[pred="interdisciplinairement_____1",clivee=+,cat=adv]	interdisciplinairement_____1	Default		%default
interlopement	100	advm	[pred="interlopement_____1",clivee=+,detach_neg = +,cat=adv]	interlopement_____1	Default		%default
interminablement	100	advm	[pred="interminablement_____1",advi=en_combien_de_temps,clivee=+,detach_neg = +,cat=adv]	interminablement_____1	Default		%default
intermusculairement	100	advm	[pred="intermusculairement_____1",clivee=+,cat=adv]	intermusculairement_____1	Default		%default
internationalement	100	advm	[pred="internationalement_____1",clivee=+,cat=adv]	internationalement_____1	Default		%default
interplanétairement	100	advm	[pred="interplanétairement_____1",clivee=+,cat=adv]	interplanétairement_____1	Default		%default
interprofessionnellement	100	advm	[pred="interprofessionnellement_____1",clivee=+,cat=adv]	interprofessionnellement_____1	Default		%default
interprétativement	100	advm	[pred="interprétativement_____1",clivee=+,cat=adv]	interprétativement_____1	Default		%default
interrogativement	100	advm	[pred="interrogativement_____1",clivee=+,cat=adv]	interrogativement_____1	Default		%default
intersyndicalement	100	advm	[pred="intersyndicalement_____1",clivee=+,cat=adv]	intersyndicalement_____1	Default		%default
intervocaliquement	100	advm	[pred="intervocaliquement_____1",clivee=+,cat=adv]	intervocaliquement_____1	Default		%default
intimement	100	advm	[pred="intimement_____1",clivee=+,cat=adv]	intimement_____1	Default		%default
intimement	100	advm	[pred="intimement_____2",clivee=+,cat=adv]	intimement_____2	Default		%default
intolérablement	100	advm	[pred="intolérablement_____1",advi=combien,clivee=+,cat=adv]	intolérablement_____1	Default		%default
intra muros	100	adv	[pred="intra muros_____1",cat=adv]	intra muros_____1	Default		%default
intra-muros	100	adv	[pred="intra-muros_____1",cat=adv]	intra-muros_____1	Default		%default
intra-utérinement	100	advm	[pred="intra-utérinement_____1",clivee=+,cat=adv]	intra-utérinement_____1	Default		%default
intra-utérinement	100	advm	[pred="intra-utérinement_____2",clivee=+,cat=adv]	intra-utérinement_____2	Default		%default
intradermiquement	100	advm	[pred="intradermiquement_____1",clivee=+,cat=adv]	intradermiquement_____1	Default		%default
intraitablement	100	advm	[pred="intraitablement_____1",clivee=+,detach_neg = +,cat=adv]	intraitablement_____1	Default		%default
intramusculairement	100	advm	[pred="intramusculairement_____1",clivee=+,cat=adv]	intramusculairement_____1	Default		%default
intransitivement	100	advm	[pred="intransitivement_____1",clivee=+,cat=adv]	intransitivement_____1	Default		%default
intransitivement	100	advm	[pred="intransitivement_____2",clivee=+,cat=adv]	intransitivement_____2	Default		%default
intraveineusement	100	advm	[pred="intraveineusement_____1",clivee=+,cat=adv]	intraveineusement_____1	Default		%default
intrinsèquement	100	advm	[pred="intrinsèquement_____1",advi=combien,cat=adv]	intrinsèquement_____1	Default		%default
introspectivement	100	advm	[pred="introspectivement_____1",clivee=+,cat=adv]	introspectivement_____1	Default		%default
intrépidement	100	advm	[pred="intrépidement_____1",clivee=+,detach_neg = +,cat=adv]	intrépidement_____1	Default		%default
intuitivement	100	advm	[pred="intuitivement_____1",clivee=+,cat=adv]	intuitivement_____1	Default		%default
intègrement	100	advm	[pred="intègrement_____1",clivee=+,detach_neg = +,cat=adv]	intègrement_____1	Default		%default
intégralement	100	advm	[pred="intégralement_____1",advi=combien,cat=adv]	intégralement_____1	Default		%default
intérieurement	100	advm	[pred="intérieurement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	intérieurement_____1	Default		%default
intérimairement	100	advm	[pred="intérimairement_____1",cat=adv]	intérimairement_____1	Default		%default
inutilement	100	advm	[pred="inutilement_____1",clivee=+,cat=adv]	inutilement_____1	Default		%default
invalidement	100	advm	[pred="invalidement_____1",clivee=+,cat=adv]	invalidement_____1	Default		%default
invariablement	100	advp	[pred="invariablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	invariablement_____1	Default		%default
inventivement	100	advm	[pred="inventivement_____1",clivee=+,detach_neg = +,cat=adv]	inventivement_____1	Default		%default
inversement	100	advm	[pred="inversement_____1",clivee=+,cat=adv]	inversement_____1	Default		%default
inversement	100	advp	[pred="inversement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	inversement_____2	Default		%default
invinciblement	100	advm	[pred="invinciblement_____1",advi=combien,clivee=+,cat=adv]	invinciblement_____1	Default		%default
inviolablement	100	advm	[pred="inviolablement_____1",advi=combien,clivee=+,cat=adv]	inviolablement_____1	Default		%default
invisiblement	100	advm	[pred="invisiblement_____1",clivee=+,cat=adv]	invisiblement_____1	Default		%default
involontairement	100	advm	[pred="involontairement_____1",clivee=+,cat=adv]	involontairement_____1	Default		%default
invraisemblablement	100	advm	[pred="invraisemblablement_____1",advi=combien,clivee=+,cat=adv]	invraisemblablement_____1	Default		%default
invulnérablement	100	advm	[pred="invulnérablement_____1",advi=combien,cat=adv]	invulnérablement_____1	Default		%default
inébranlablement	100	advm	[pred="inébranlablement_____1",advi=combien,clivee=+,cat=adv]	inébranlablement_____1	Default		%default
inégalablement	100	advm	[pred="inégalablement_____1",advi=combien,clivee=+,cat=adv]	inégalablement_____1	Default		%default
inégalement	100	advm	[pred="inégalement_____1",clivee=+,cat=adv]	inégalement_____1	Default		%default
inéluctablement	100	advp	[pred="inéluctablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	inéluctablement_____1	Default		%default
inélégamment	100	advm	[pred="inélégamment_____1",clivee=+,detach_neg = +,cat=adv]	inélégamment_____1	Default		%default
inénarrablement	100	advm	[pred="inénarrablement_____1",advi=combien,clivee=+,cat=adv]	inénarrablement_____1	Default		%default
inépuisablement	100	advm	[pred="inépuisablement_____1",advi=combien,clivee=+,cat=adv]	inépuisablement_____1	Default		%default
inéquitablement	100	advm	[pred="inéquitablement_____1",clivee=+,cat=adv]	inéquitablement_____1	Default		%default
inévitablement	100	advp	[pred="inévitablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	inévitablement_____1	Default		%default
ipso facto	100	adv	[pred="ipso facto_____1",cat=adv]	ipso facto_____1	Default		%default
irasciblement	100	advm	[pred="irasciblement_____1",clivee=+,detach_neg = +,cat=adv]	irasciblement_____1	Default		%default
ironiquement	100	advm	[pred="ironiquement_____1",clivee=+,detach_neg = +,cat=adv]	ironiquement_____1	Default		%default
irraisonnablement	100	advm	[pred="irraisonnablement_____1",clivee=+,detach_neg = +,cat=adv]	irraisonnablement_____1	Default		%default
irrationnellement	100	advm	[pred="irrationnellement_____1",clivee=+,detach_neg = +,cat=adv]	irrationnellement_____1	Default		%default
irrespectueusement	100	advm	[pred="irrespectueusement_____1",clivee=+,detach_neg = +,cat=adv]	irrespectueusement_____1	Default		%default
irrespirablement	100	advm	[pred="irrespirablement_____1",advi=combien,clivee=+,cat=adv]	irrespirablement_____1	Default		%default
irresponsablement	100	advm	[pred="irresponsablement_____1",clivee=+,detach_neg = +,cat=adv]	irresponsablement_____1	Default		%default
irréconciliablement	100	advm	[pred="irréconciliablement_____1",advi=combien,clivee=+,cat=adv]	irréconciliablement_____1	Default		%default
irrécupérablement	100	advm	[pred="irrécupérablement_____1",clivee=+,cat=adv]	irrécupérablement_____1	Default		%default
irrécusablement	100	advm	[pred="irrécusablement_____1",advi=combien,clivee=+,cat=adv]	irrécusablement_____1	Default		%default
irréductiblement	100	advm	[pred="irréductiblement_____1",advi=combien,clivee=+,cat=adv]	irréductiblement_____1	Default		%default
irréellement	100	advm	[pred="irréellement_____1",clivee=+,cat=adv]	irréellement_____1	Default		%default
irréfragablement	100	advm	[pred="irréfragablement_____1",advi=combien,clivee=+,cat=adv]	irréfragablement_____1	Default		%default
irréfutablement	100	advm	[pred="irréfutablement_____1",advi=combien,clivee=+,cat=adv]	irréfutablement_____1	Default		%default
irrégulièrement	100	advm	[pred="irrégulièrement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	irrégulièrement_____1	Default		%default
irrégulièrement	100	advm	[pred="irrégulièrement_____2",clivee=+,cat=adv]	irrégulièrement_____2	Default		%default
irrégulièrement	100	advm	[pred="irrégulièrement_____3",clivee=+,cat=adv]	irrégulièrement_____3	Default		%default
irréligieusement	100	advm	[pred="irréligieusement_____1",clivee=+,cat=adv]	irréligieusement_____1	Default		%default
irrémissiblement	100	advm	[pred="irrémissiblement_____1",advi=combien,clivee=+,cat=adv]	irrémissiblement_____1	Default		%default
irrémédiablement	100	advm	[pred="irrémédiablement_____1",advi=combien,clivee=+,cat=adv]	irrémédiablement_____1	Default		%default
irréparablement	100	advm	[pred="irréparablement_____1",advi=combien,clivee=+,cat=adv]	irréparablement_____1	Default		%default
irrépressiblement	100	advm	[pred="irrépressiblement_____1",advi=combien,clivee=+,cat=adv]	irrépressiblement_____1	Default		%default
irréprochablement	100	advm	[pred="irréprochablement_____1",clivee=+,detach_neg = +,cat=adv]	irréprochablement_____1	Default		%default
irrépréhensiblement	100	advm	[pred="irrépréhensiblement_____1",clivee=+,detach_neg = +,cat=adv]	irrépréhensiblement_____1	Default		%default
irrésistiblement	100	advm	[pred="irrésistiblement_____1",advi=combien,clivee=+,cat=adv]	irrésistiblement_____1	Default		%default
irrésolument	100	advm	[pred="irrésolument_____1",clivee=+,detach_neg = +,cat=adv]	irrésolument_____1	Default		%default
irréversiblement	100	advm	[pred="irréversiblement_____1",clivee=+,cat=adv]	irréversiblement_____1	Default		%default
irrévocablement	100	advm	[pred="irrévocablement_____1",clivee=+,cat=adv]	irrévocablement_____1	Default		%default
irrévéremment	100	advm	[pred="irrévéremment_____1",clivee=+,cat=adv]	irrévéremment_____1	Default		%default
irrévérencieusement	100	advm	[pred="irrévérencieusement_____1",clivee=+,detach_neg = +,cat=adv]	irrévérencieusement_____1	Default		%default
isolément	100	advm	[pred="isolément_____1",clivee=+,cat=adv]	isolément_____1	Default		%default
isothermiquement	100	advm	[pred="isothermiquement_____1",clivee=+,cat=adv]	isothermiquement_____1	Default		%default
isoédriquement	100	advm	[pred="isoédriquement_____1",clivee=+,cat=adv]	isoédriquement_____1	Default		%default
item	100	adv	[pred="item_____1",cat=adv]	item_____1	Default		%default
itou	100	adv	[pred="itou_____1",cat=adv]	itou_____1	Default		%default
itérativement	100	advm	[pred="itérativement_____1",clivee=+,cat=adv]	itérativement_____1	Default		%default
jacobinement	100	advm	[pred="jacobinement_____1",clivee=+,detach_neg = +,cat=adv]	jacobinement_____1	Default		%default
jadis	100	adv	[pred="jadis_____1",cat=adv]	jadis_____1	Default		%default
jalousement	100	advm	[pred="jalousement_____1",clivee=+,detach_neg = +,cat=adv]	jalousement_____1	Default		%default
jamais	100	adv	[pred="jamais_____1",cat=adv]	jamais_____1	Default		%default
jamais plus	100	adv	[pred="jamais plus_____1",cat=adv]	jamais plus_____1	Default		%default
janséniquement	100	advm	[pred="janséniquement_____1",clivee=+,detach_neg = +,cat=adv]	janséniquement_____1	Default		%default
jaune	100	adv	[pred="jaune_____1",cat=adv]	jaune_____1	Default		%default
jeudi	100	adv	[pred="jeudi_____1",cat=adv]	jeudi_____1	Default		%default
jeunement	100	advm	[pred="jeunement_____1",clivee=+,cat=adv]	jeunement_____1	Default		%default
jobardement	100	advm	[pred="jobardement_____1",clivee=+,detach_neg = +,cat=adv]	jobardement_____1	Default		%default
jointement	100	advp	[pred="jointement_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	jointement_____1	Default		%default
jointement	100	advm	[pred="jointement_____2",clivee=+,cat=adv]	jointement_____2	Default		%default
jointivement	100	advm	[pred="jointivement_____1",clivee=+,cat=adv]	jointivement_____1	Default		%default
joliment	100	advm	[pred="joliment_____1",advi=combien,cat=adv]	joliment_____1	Default		%default
joliment	100	advm	[pred="joliment_____2",cat=adv]	joliment_____2	Default		%default
journalistiquement	100	advm	[pred="journalistiquement_____1",clivee=+,cat=adv]	journalistiquement_____1	Default		%default
journellement	100	advm	[pred="journellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	journellement_____1	Default		%default
jovialement	100	advm	[pred="jovialement_____1",clivee=+,detach_neg = +,cat=adv]	jovialement_____1	Default		%default
joyeusement	100	advm	[pred="joyeusement_____1",clivee=+,detach_neg = +,cat=adv]	joyeusement_____1	Default		%default
judaïquement	100	advm	[pred="judaïquement_____1",clivee=+,cat=adv]	judaïquement_____1	Default		%default
judiciairement	100	advm	[pred="judiciairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	judiciairement_____1	Default		%default
judiciairement	100	advm	[pred="judiciairement_____2",clivee=+,cat=adv]	judiciairement_____2	Default		%default
judicieusement	100	advm	[pred="judicieusement_____1",clivee=+,detach_neg = +,cat=adv]	judicieusement_____1	Default		%default
judicieusement	100	advp	[pred="judicieusement_____2",detach = +,detach_neg = +,cat=adv]	judicieusement_____2	Default		%default
juridiquement	100	advm	[pred="juridiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	juridiquement_____1	Default		%default
juridiquement	100	advm	[pred="juridiquement_____2",clivee=+,cat=adv]	juridiquement_____2	Default		%default
jusqu'alors	100	adv	[pred="jusqu'alors_____1",cat=adv]	jusqu'alors_____1	Default		%default
jusqu'ici	100	adv	[pred="jusqu'ici_____1",cat=adv]	jusqu'ici_____1	Default		%default
jusqu'là	100	adv	[pred="jusqu'là_____1",cat=adv]	jusqu'là_____1	Default		%default
jusqu'à présent	100	adv	[pred="jusqu'à présent_____1",cat=adv]	jusqu'à présent_____1	Default		%default
jusque-là	100	adv	[pred="jusque-là_____1",cat=adv]	jusque-là_____1	Default		%default
juste	100	adv	[pred="juste_____1",adv_kind=modnc,cat=adv]	juste_____1	Default		%default
juste	100	adv	[pred="juste_____2",cat=adv]	juste_____2	Default		%default
justement	100	advm	[pred="justement_____1",clivee=+,detach_neg = +,cat=adv]	justement_____1	Default		%default
justement	100	advm	[pred="justement_____2",clivee=+,cat=adv]	justement_____2	Default		%default
justement	100	advp	[pred="justement_____3",detach = +,detach_neg = +,cat=adv]	justement_____3	Default		%default
juvénilement	100	advm	[pred="juvénilement_____1",clivee=+,cat=adv]	juvénilement_____1	Default		%default
juxtalinéairement	100	advm	[pred="juxtalinéairement_____1",clivee=+,cat=adv]	juxtalinéairement_____1	Default		%default
jésuitement	100	advm	[pred="jésuitement_____1",clivee=+,detach_neg = +,cat=adv]	jésuitement_____1	Default		%default
jésuitiquement	100	advm	[pred="jésuitiquement_____1",clivee=+,cat=adv]	jésuitiquement_____1	Default		%default
kaléidoscopiquement	100	advm	[pred="kaléidoscopiquement_____1",clivee=+,cat=adv]	kaléidoscopiquement_____1	Default		%default
kilométriquement	100	advm	[pred="kilométriquement_____1",clivee=+,cat=adv]	kilométriquement_____1	Default		%default
la plupart du temps	100	adv	[pred="la plupart du temps_____1",cat=adv]	la plupart du temps_____1	Default		%default
la preuve	100	adv	[pred="la preuve_____1",cat=adv]	la preuve_____1	Default		%default
labialement	100	advm	[pred="labialement_____1",clivee=+,cat=adv]	labialement_____1	Default		%default
laborieusement	100	advm	[pred="laborieusement_____1",clivee=+,cat=adv]	laborieusement_____1	Default		%default
labyrinthiquement	100	advm	[pred="labyrinthiquement_____1",clivee=+,cat=adv]	labyrinthiquement_____1	Default		%default
laconiquement	100	advm	[pred="laconiquement_____1",clivee=+,detach_neg = +,cat=adv]	laconiquement_____1	Default		%default
lactiquement	100	advm	[pred="lactiquement_____1",clivee=+,cat=adv]	lactiquement_____1	Default		%default
lacunairement	100	advm	[pred="lacunairement_____1",clivee=+,cat=adv]	lacunairement_____1	Default		%default
ladrement	100	advm	[pred="ladrement_____1",clivee=+,detach_neg = +,cat=adv]	ladrement_____1	Default		%default
laidement	100	advm	[pred="laidement_____1",clivee=+,cat=adv]	laidement_____1	Default		%default
laiteusement	100	advm	[pred="laiteusement_____1",clivee=+,cat=adv]	laiteusement_____1	Default		%default
lamentablement	100	advm	[pred="lamentablement_____1",advi=combien,clivee=+,cat=adv]	lamentablement_____1	Default		%default
langagièrement	100	advm	[pred="langagièrement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	langagièrement_____1	Default		%default
langagièrement	100	advm	[pred="langagièrement_____2",clivee=+,cat=adv]	langagièrement_____2	Default		%default
langoureusement	100	advm	[pred="langoureusement_____1",clivee=+,detach_neg = +,cat=adv]	langoureusement_____1	Default		%default
languidement	100	advm	[pred="languidement_____1",clivee=+,cat=adv]	languidement_____1	Default		%default
languissamment	100	advm	[pred="languissamment_____1",clivee=+,cat=adv]	languissamment_____1	Default		%default
lapidairement	100	advm	[pred="lapidairement_____1",clivee=+,cat=adv]	lapidairement_____1	Default		%default
large	100	adv	[pred="large_____1",cat=adv]	large_____1	Default		%default
largement	100	advm	[pred="largement_____1",advi=combien,cat=adv]	largement_____1	Default		%default
larghetto	100	adv	[pred="larghetto_____1",cat=adv]	larghetto_____1	Default		%default
largo	100	adv	[pred="largo_____1",cat=adv]	largo_____1	Default		%default
lascivement	100	advm	[pred="lascivement_____1",clivee=+,detach_neg = +,cat=adv]	lascivement_____1	Default		%default
lassement	100	advm	[pred="lassement_____1",clivee=+,detach_neg = +,cat=adv]	lassement_____1	Default		%default
last but not least	100	advp	[pred="last but not least_____1",chunk_type=GN,cat=adv]	last but not least_____1	Default		%default
latinement	100	advm	[pred="latinement_____1",clivee=+,cat=adv]	latinement_____1	Default		%default
lato sensu	100	adv	[pred="lato sensu_____1",cat=adv]	lato sensu_____1	Default		%default
latéralement	100	advm	[pred="latéralement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	latéralement_____1	Default		%default
laxistement	100	advm	[pred="laxistement_____1",clivee=+,cat=adv]	laxistement_____1	Default		%default
laïquement	100	advm	[pred="laïquement_____1",clivee=+,cat=adv]	laïquement_____1	Default		%default
le cas échéant	100	adv	[pred="le cas échéant_____1",cat=adv]	le cas échéant_____1	Default		%default
le fait est que	100	adv	[pred="le fait est que_____1",cat=adv]	le fait est que_____1	Default		%default
le mieux	100	adv	[pred="le mieux_____1",cat=adv]	le mieux_____1	Default		%default
le moins	100	adv	[pred="le moins_____1",cat=adv]	le moins_____1	Default		%default
le plus	100	adv	[pred="le plus_____1",cat=adv]	le plus_____1	Default		%default
legato	100	adv	[pred="legato_____1",cat=adv]	legato_____1	Default		%default
lentement	100	advm	[pred="lentement_____1",clivee=+,cat=adv]	lentement_____1	Default		%default
lento	100	adv	[pred="lento_____1",cat=adv]	lento_____1	Default		%default
lerche	100	adv	[pred="lerche_____1",cat=adv]	lerche_____1	Default		%default
les doigts dans le nez	100	adv	[pred="les doigts dans le nez_____1",cat=adv]	les doigts dans le nez_____1	Default		%default
les yeux fermés	100	adv	[pred="les yeux fermés_____1",cat=adv]	les yeux fermés_____1	Default		%default
lestement	100	advm	[pred="lestement_____1",clivee=+,detach_neg = +,cat=adv]	lestement_____1	Default		%default
lexicalement	100	advm	[pred="lexicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	lexicalement_____1	Default		%default
lexicalement	100	advm	[pred="lexicalement_____2",clivee=+,cat=adv]	lexicalement_____2	Default		%default
lexicographiquement	100	advm	[pred="lexicographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	lexicographiquement_____1	Default		%default
lexicologiquement	100	advm	[pred="lexicologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	lexicologiquement_____1	Default		%default
libertinement	100	advm	[pred="libertinement_____1",clivee=+,detach_neg = +,cat=adv]	libertinement_____1	Default		%default
libidineusement	100	advm	[pred="libidineusement_____1",clivee=+,detach_neg = +,cat=adv]	libidineusement_____1	Default		%default
librement	100	advm	[pred="librement_____1",clivee=+,detach_neg = +,cat=adv]	librement_____1	Default		%default
librement	100	advm	[pred="librement_____2",clivee=+,cat=adv]	librement_____2	Default		%default
libéralement	100	advm	[pred="libéralement_____1",clivee=+,detach_neg = +,cat=adv]	libéralement_____1	Default		%default
licencieusement	100	advm	[pred="licencieusement_____1",clivee=+,detach_neg = +,cat=adv]	licencieusement_____1	Default		%default
licitement	100	advm	[pred="licitement_____1",clivee=+,cat=adv]	licitement_____1	Default		%default
ligamentairement	100	advm	[pred="ligamentairement_____1",clivee=+,cat=adv]	ligamentairement_____1	Default		%default
liminairement	100	advm	[pred="liminairement_____1",clivee=+,cat=adv]	liminairement_____1	Default		%default
limitativement	100	advm	[pred="limitativement_____1",clivee=+,cat=adv]	limitativement_____1	Default		%default
limpidement	100	advm	[pred="limpidement_____1",clivee=+,cat=adv]	limpidement_____1	Default		%default
linguistiquement	100	advm	[pred="linguistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	linguistiquement_____1	Default		%default
linguistiquement	100	advm	[pred="linguistiquement_____2",clivee=+,cat=adv]	linguistiquement_____2	Default		%default
linéairement	100	advm	[pred="linéairement_____1",clivee=+,cat=adv]	linéairement_____1	Default		%default
linéalement	100	advm	[pred="linéalement_____1",clivee=+,cat=adv]	linéalement_____1	Default		%default
liquidement	100	advm	[pred="liquidement_____1",clivee=+,cat=adv]	liquidement_____1	Default		%default
lisiblement	100	advm	[pred="lisiblement_____1",clivee=+,detach_neg = +,cat=adv]	lisiblement_____1	Default		%default
lithographiquement	100	advm	[pred="lithographiquement_____1",clivee=+,cat=adv]	lithographiquement_____1	Default		%default
litigieusement	100	advm	[pred="litigieusement_____1",clivee=+,cat=adv]	litigieusement_____1	Default		%default
littérairement	100	advm	[pred="littérairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	littérairement_____1	Default		%default
littéralement	100	advm	[pred="littéralement_____1",advi=combien,cat=adv]	littéralement_____1	Default		%default
littéralement	100	advm	[pred="littéralement_____2",cat=adv]	littéralement_____2	Default		%default
liturgiquement	100	advm	[pred="liturgiquement_____1",clivee=+,cat=adv]	liturgiquement_____1	Default		%default
lividement	100	advm	[pred="lividement_____1",clivee=+,cat=adv]	lividement_____1	Default		%default
livresquement	100	advm	[pred="livresquement_____1",clivee=+,cat=adv]	livresquement_____1	Default		%default
loc. cit.	100	adv	[pred="loc. cit._____1",cat=adv]	loc. cit._____1	Default		%default
localement	100	advm	[pred="localement_____1",clivee=+,cat=adv]	localement_____1	Default		%default
locus citatus	100	adv	[pred="locus citatus_____1",cat=adv]	locus citatus_____1	Default		%default
logarithmiquement	100	advm	[pred="logarithmiquement_____1",clivee=+,cat=adv]	logarithmiquement_____1	Default		%default
logiquement	100	advm	[pred="logiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	logiquement_____1	Default		%default
logiquement	100	advm	[pred="logiquement_____2",clivee=+,detach_neg = +,cat=adv]	logiquement_____2	Default		%default
logiquement	100	advm	[pred="logiquement_____3",clivee=+,cat=adv]	logiquement_____3	Default		%default
logistiquement	100	advm	[pred="logistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	logistiquement_____1	Default		%default
loin	100	adv	[pred="loin_____1<Objde:(de-scompl|de-sinf|de-sn)>",cat=adv]	loin_____1	Default		%default
lointainement	100	advm	[pred="lointainement_____1",clivee=+,cat=adv]	lointainement_____1	Default		%default
loisiblement	100	advm	[pred="loisiblement_____1",clivee=+,cat=adv]	loisiblement_____1	Default		%default
long-temps	100	adv	[pred="long-temps_____1",cat=adv]	long-temps_____1	Default		%default
longanimement	100	advm	[pred="longanimement_____1",clivee=+,detach_neg = +,cat=adv]	longanimement_____1	Default		%default
longitudinalement	100	advm	[pred="longitudinalement_____1",clivee=+,cat=adv]	longitudinalement_____1	Default		%default
longtemps	100	adv	[pred="longtemps_____1",cat=adv]	longtemps_____1	Default		%default
longuement	100	advm	[pred="longuement_____1",clivee=+,cat=adv]	longuement_____1	Default		%default
loquacement	100	advm	[pred="loquacement_____1",clivee=+,detach_neg = +,cat=adv]	loquacement_____1	Default		%default
lors	100	adv	[pred="lors_____1",cat=adv]	lors_____1	Default		%default
louablement	100	advm	[pred="louablement_____1",clivee=+,cat=adv]	louablement_____1	Default		%default
louangeusement	100	advm	[pred="louangeusement_____1",clivee=+,detach_neg = +,cat=adv]	louangeusement_____1	Default		%default
louchement	100	advm	[pred="louchement_____1",clivee=+,detach_neg = +,cat=adv]	louchement_____1	Default		%default
loufoquement	100	advm	[pred="loufoquement_____1",clivee=+,detach_neg = +,cat=adv]	loufoquement_____1	Default		%default
lourd	100	adv	[pred="lourd_____1",cat=adv]	lourd_____1	Default		%default
lourdaudement	100	advm	[pred="lourdaudement_____1",clivee=+,detach_neg = +,cat=adv]	lourdaudement_____1	Default		%default
lourdement	100	advm	[pred="lourdement_____1",clivee=+,detach_neg = +,cat=adv]	lourdement_____1	Default		%default
lourdement	100	advm	[pred="lourdement_____2",clivee=+,cat=adv]	lourdement_____2	Default		%default
loyalement	100	advm	[pred="loyalement_____1",clivee=+,detach_neg = +,cat=adv]	loyalement_____1	Default		%default
lubriquement	100	advm	[pred="lubriquement_____1",clivee=+,detach_neg = +,cat=adv]	lubriquement_____1	Default		%default
lucidement	100	advm	[pred="lucidement_____1",clivee=+,detach_neg = +,cat=adv]	lucidement_____1	Default		%default
lucrativement	100	advm	[pred="lucrativement_____1",clivee=+,cat=adv]	lucrativement_____1	Default		%default
ludiquement	100	advm	[pred="ludiquement_____1",clivee=+,cat=adv]	ludiquement_____1	Default		%default
lugubrement	100	advm	[pred="lugubrement_____1",clivee=+,cat=adv]	lugubrement_____1	Default		%default
lumineusement	100	advm	[pred="lumineusement_____1",clivee=+,cat=adv]	lumineusement_____1	Default		%default
lunairement	100	advm	[pred="lunairement_____1",clivee=+,cat=adv]	lunairement_____1	Default		%default
lunatiquement	100	advm	[pred="lunatiquement_____1",clivee=+,detach_neg = +,cat=adv]	lunatiquement_____1	Default		%default
lundi	100	adv	[pred="lundi_____1",cat=adv]	lundi_____1	Default		%default
lustralement	100	advm	[pred="lustralement_____1",clivee=+,cat=adv]	lustralement_____1	Default		%default
luxueusement	100	advm	[pred="luxueusement_____1",clivee=+,cat=adv]	luxueusement_____1	Default		%default
luxurieusement	100	advm	[pred="luxurieusement_____1",clivee=+,detach_neg = +,cat=adv]	luxurieusement_____1	Default		%default
lymphatiquement	100	advm	[pred="lymphatiquement_____1",clivee=+,detach_neg = +,cat=adv]	lymphatiquement_____1	Default		%default
lyriquement	100	advm	[pred="lyriquement_____1",clivee=+,detach_neg = +,cat=adv]	lyriquement_____1	Default		%default
là	100	adv	[pred="là_____1",cat=adv]	là_____1	Default		%default
là-bas	100	adv	[pred="là-bas_____1",cat=adv]	là-bas_____1	Default		%default
là-dedans	100	adv	[pred="là-dedans_____1",cat=adv]	là-dedans_____1	Default		%default
là-dessous	100	adv	[pred="là-dessous_____1",cat=adv]	là-dessous_____1	Default		%default
là-dessus	100	adv	[pred="là-dessus_____1",cat=adv]	là-dessus_____1	Default		%default
là-haut	100	adv	[pred="là-haut_____1",cat=adv]	là-haut_____1	Default		%default
lâchement	100	advm	[pred="lâchement_____1",clivee=+,detach_neg = +,cat=adv]	lâchement_____1	Default		%default
lâchement	100	advp	[pred="lâchement_____2",detach = +,detach_neg = +,cat=adv]	lâchement_____2	Default		%default
légalement	100	advm	[pred="légalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	légalement_____1	Default		%default
légalement	100	advm	[pred="légalement_____2",clivee=+,cat=adv]	légalement_____2	Default		%default
légendairement	100	advm	[pred="légendairement_____1",clivee=+,cat=adv]	légendairement_____1	Default		%default
législativement	100	advm	[pred="législativement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	législativement_____1	Default		%default
législativement	100	advm	[pred="législativement_____2",clivee=+,cat=adv]	législativement_____2	Default		%default
légitimement	100	advm	[pred="légitimement_____1",clivee=+,cat=adv]	légitimement_____1	Default		%default
légèrement	100	advm	[pred="légèrement_____1",advi=combien,cat=adv]	légèrement_____1	Default		%default
légèrement	100	advm	[pred="légèrement_____2",clivee=+,detach_neg = +,cat=adv]	légèrement_____2	Default		%default
légèrement	100	advm	[pred="légèrement_____3",clivee=+,cat=adv]	légèrement_____3	Default		%default
léthargiquement	100	advm	[pred="léthargiquement_____1",clivee=+,detach_neg = +,cat=adv]	léthargiquement_____1	Default		%default
macabrement	100	advm	[pred="macabrement_____1",clivee=+,detach_neg = +,cat=adv]	macabrement_____1	Default		%default
macache	100	adv	[pred="macache_____1",cat=adv]	macache_____1	Default		%default
macaroniquement	100	advm	[pred="macaroniquement_____1",clivee=+,cat=adv]	macaroniquement_____1	Default		%default
machiavéliquement	100	advm	[pred="machiavéliquement_____1",clivee=+,detach_neg = +,cat=adv]	machiavéliquement_____1	Default		%default
machinalement	100	advm	[pred="machinalement_____1",clivee=+,cat=adv]	machinalement_____1	Default		%default
macrobiotiquement	100	advm	[pred="macrobiotiquement_____1",clivee=+,cat=adv]	macrobiotiquement_____1	Default		%default
macroscopiquement	100	advm	[pred="macroscopiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	macroscopiquement_____1	Default		%default
maestoso	100	adv	[pred="maestoso_____1",cat=adv]	maestoso_____1	Default		%default
magiquement	100	advm	[pred="magiquement_____1",clivee=+,cat=adv]	magiquement_____1	Default		%default
magister dixit	100	adv	[pred="magister dixit_____1",cat=adv]	magister dixit_____1	Default		%default
magistralement	100	advm	[pred="magistralement_____1",clivee=+,detach_neg = +,cat=adv]	magistralement_____1	Default		%default
magnanimement	100	advm	[pred="magnanimement_____1",clivee=+,detach_neg = +,cat=adv]	magnanimement_____1	Default		%default
magnanimement	100	advp	[pred="magnanimement_____2",detach = +,detach_neg = +,cat=adv]	magnanimement_____2	Default		%default
magnifiquement	100	advm	[pred="magnifiquement_____1",advi=combien,clivee=+,cat=adv]	magnifiquement_____1	Default		%default
magnétiquement	100	advm	[pred="magnétiquement_____1",clivee=+,cat=adv]	magnétiquement_____1	Default		%default
magnétohydrodynamiquement	100	advm	[pred="magnétohydrodynamiquement_____1",clivee=+,cat=adv]	magnétohydrodynamiquement_____1	Default		%default
magnétoélectriquement	100	advm	[pred="magnétoélectriquement_____1",clivee=+,cat=adv]	magnétoélectriquement_____1	Default		%default
maigrement	100	advm	[pred="maigrement_____1",advi=combien,cat=adv]	maigrement_____1	Default		%default
main dans la main	100	adv	[pred="main dans la main_____1",cat=adv]	main dans la main_____1	Default		%default
maintenant	100	adv	[pred="maintenant_____1",cat=adv]	maintenant_____1	Default		%default
maintes fois	100	advp	[pred="maintes fois_____1",cat=adv]	maintes fois_____1	Default		%default
mais	100	adv	[pred="mais_____1",cat=adv]	mais_____1	Default		%default
majestueusement	100	advm	[pred="majestueusement_____1",clivee=+,detach_neg = +,cat=adv]	majestueusement_____1	Default		%default
majoritairement	100	advm	[pred="majoritairement_____1",clivee=+,cat=adv]	majoritairement_____1	Default		%default
mal	100	adv	[pred="mal_____1",cat=adv]	mal_____1	Default		%default
mal à propos	100	adv	[pred="mal à propos_____1",cat=adv]	mal à propos_____1	Default		%default
maladivement	100	advm	[pred="maladivement_____1",clivee=+,detach_neg = +,cat=adv]	maladivement_____1	Default		%default
maladroitement	100	advm	[pred="maladroitement_____1",clivee=+,detach_neg = +,cat=adv]	maladroitement_____1	Default		%default
malaisément	100	advm	[pred="malaisément_____1",clivee=+,cat=adv]	malaisément_____1	Default		%default
malcommodément	100	advm	[pred="malcommodément_____1",clivee=+,cat=adv]	malcommodément_____1	Default		%default
malencontreusement	100	advm	[pred="malencontreusement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	malencontreusement_____1	Default		%default
malgracieusement	100	advm	[pred="malgracieusement_____1",clivee=+,detach_neg = +,cat=adv]	malgracieusement_____1	Default		%default
malgré tout	100	adv	[pred="malgré tout_____1",cat=adv]	malgré tout_____1	Default		%default
malhabilement	100	advm	[pred="malhabilement_____1",clivee=+,detach_neg = +,cat=adv]	malhabilement_____1	Default		%default
malheureusement	100	advm	[pred="malheureusement_____1",clivee=+,detach_neg = +,cat=adv]	malheureusement_____1	Default		%default
malheureusement	100	advp	[pred="malheureusement_____2",detach = +,detach_neg = +,cat=adv]	malheureusement_____2	Default		%default
malhonnêtement	100	advm	[pred="malhonnêtement_____1",clivee=+,detach_neg = +,cat=adv]	malhonnêtement_____1	Default		%default
malicieusement	100	advm	[pred="malicieusement_____1",clivee=+,detach_neg = +,cat=adv]	malicieusement_____1	Default		%default
malignement	100	advm	[pred="malignement_____1",clivee=+,detach_neg = +,cat=adv]	malignement_____1	Default		%default
malproprement	100	advm	[pred="malproprement_____1",clivee=+,detach_neg = +,cat=adv]	malproprement_____1	Default		%default
malsainement	100	advm	[pred="malsainement_____1",clivee=+,cat=adv]	malsainement_____1	Default		%default
malveillamment	100	advm	[pred="malveillamment_____1",clivee=+,detach_neg = +,cat=adv]	malveillamment_____1	Default		%default
maléfiquement	100	advm	[pred="maléfiquement_____1",clivee=+,cat=adv]	maléfiquement_____1	Default		%default
maniaquement	100	advm	[pred="maniaquement_____1",clivee=+,detach_neg = +,cat=adv]	maniaquement_____1	Default		%default
manifestement	100	advp	[pred="manifestement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	manifestement_____1	Default		%default
manu militari	100	adv	[pred="manu militari_____1",cat=adv]	manu militari_____1	Default		%default
manuellement	100	advm	[pred="manuellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	manuellement_____1	Default		%default
manuellement	100	advm	[pred="manuellement_____2",clivee=+,cat=adv]	manuellement_____2	Default		%default
mardi	100	adv	[pred="mardi_____1",cat=adv]	mardi_____1	Default		%default
marginalement	100	advm	[pred="marginalement_____1",clivee=+,detach_neg = +,cat=adv]	marginalement_____1	Default		%default
maritalement	100	advm	[pred="maritalement_____1",clivee=+,cat=adv]	maritalement_____1	Default		%default
marmiteusement	100	advm	[pred="marmiteusement_____1",clivee=+,detach_neg = +,cat=adv]	marmiteusement_____1	Default		%default
marotiquement	100	advm	[pred="marotiquement_____1",clivee=+,cat=adv]	marotiquement_____1	Default		%default
marre	100	adv	[pred="marre_____1",cat=adv]	marre_____1	Default		%default
martialement	100	advm	[pred="martialement_____1",clivee=+,detach_neg = +,cat=adv]	martialement_____1	Default		%default
masculinement	100	advm	[pred="masculinement_____1",clivee=+,detach_neg = +,cat=adv]	masculinement_____1	Default		%default
masochistement	100	advm	[pred="masochistement_____1",clivee=+,detach_neg = +,cat=adv]	masochistement_____1	Default		%default
massivement	100	advm	[pred="massivement_____1",clivee=+,cat=adv]	massivement_____1	Default		%default
maternellement	100	advm	[pred="maternellement_____1",clivee=+,detach_neg = +,cat=adv]	maternellement_____1	Default		%default
mathématiquement	100	advm	[pred="mathématiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	mathématiquement_____1	Default		%default
mathématiquement	100	advm	[pred="mathématiquement_____2",clivee=+,cat=adv]	mathématiquement_____2	Default		%default
matin	100	adv	[pred="matin_____1",cat=adv]	matin_____1	Default		%default
matinalement	100	advm	[pred="matinalement_____1",clivee=+,cat=adv]	matinalement_____1	Default		%default
matoisement	100	advm	[pred="matoisement_____1",clivee=+,detach_neg = +,cat=adv]	matoisement_____1	Default		%default
matriarcalement	100	advm	[pred="matriarcalement_____1",clivee=+,cat=adv]	matriarcalement_____1	Default		%default
matrimonialement	100	advm	[pred="matrimonialement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	matrimonialement_____1	Default		%default
matrimonialement	100	advm	[pred="matrimonialement_____2",clivee=+,cat=adv]	matrimonialement_____2	Default		%default
matérialistement	100	advm	[pred="matérialistement_____1",clivee=+,detach_neg = +,cat=adv]	matérialistement_____1	Default		%default
matériellement	100	advm	[pred="matériellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	matériellement_____1	Default		%default
matériellement	100	advm	[pred="matériellement_____2",clivee=+,cat=adv]	matériellement_____2	Default		%default
maupiteusement	100	advm	[pred="maupiteusement_____1",clivee=+,detach_neg = +,cat=adv]	maupiteusement_____1	Default		%default
mauresquement	100	advm	[pred="mauresquement_____1",clivee=+,cat=adv]	mauresquement_____1	Default		%default
maussadement	100	advm	[pred="maussadement_____1",clivee=+,detach_neg = +,cat=adv]	maussadement_____1	Default		%default
mauvaisement	100	advm	[pred="mauvaisement_____1",clivee=+,cat=adv]	mauvaisement_____1	Default		%default
maximalement	100	advm	[pred="maximalement_____1",advi=combien,cat=adv]	maximalement_____1	Default		%default
mensongèrement	100	advm	[pred="mensongèrement_____1",clivee=+,cat=adv]	mensongèrement_____1	Default		%default
mensuellement	100	advm	[pred="mensuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	mensuellement_____1	Default		%default
mentalement	100	advm	[pred="mentalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	mentalement_____1	Default		%default
mentalement	100	advm	[pred="mentalement_____2",clivee=+,cat=adv]	mentalement_____2	Default		%default
menteusement	100	advm	[pred="menteusement_____1",clivee=+,detach_neg = +,cat=adv]	menteusement_____1	Default		%default
menu	100	adv	[pred="menu_____1",cat=adv]	menu_____1	Default		%default
mercantilement	100	advm	[pred="mercantilement_____1",clivee=+,cat=adv]	mercantilement_____1	Default		%default
mercredi	100	adv	[pred="mercredi_____1",cat=adv]	mercredi_____1	Default		%default
merdeusement	100	advm	[pred="merdeusement_____1",clivee=+,cat=adv]	merdeusement_____1	Default		%default
merveilleusement	100	advm	[pred="merveilleusement_____1",advi=combien,cat=adv]	merveilleusement_____1	Default		%default
mesquinement	100	advm	[pred="mesquinement_____1",clivee=+,detach_neg = +,cat=adv]	mesquinement_____1	Default		%default
mesurément	100	advm	[pred="mesurément_____1",clivee=+,detach_neg = +,cat=adv]	mesurément_____1	Default		%default
metallurgiquement	100	advm	[pred="metallurgiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	metallurgiquement_____1	Default		%default
mezza voce	100	adv	[pred="mezza voce_____1",cat=adv]	mezza voce_____1	Default		%default
mezzo	100	adv	[pred="mezzo_____1",cat=adv]	mezzo_____1	Default		%default
microbiologiquement	100	advm	[pred="microbiologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	microbiologiquement_____1	Default		%default
microchimiquement	100	advm	[pred="microchimiquement_____1",clivee=+,cat=adv]	microchimiquement_____1	Default		%default
micrographiquement	100	advm	[pred="micrographiquement_____1",clivee=+,cat=adv]	micrographiquement_____1	Default		%default
micrométriquement	100	advm	[pred="micrométriquement_____1",clivee=+,cat=adv]	micrométriquement_____1	Default		%default
microphysiquement	100	advm	[pred="microphysiquement_____1",clivee=+,cat=adv]	microphysiquement_____1	Default		%default
microscopiquement	100	advm	[pred="microscopiquement_____1",clivee=+,cat=adv]	microscopiquement_____1	Default		%default
mielleusement	100	advm	[pred="mielleusement_____1",clivee=+,detach_neg = +,cat=adv]	mielleusement_____1	Default		%default
miette	100	adv	[pred="miette_____1",cat=adv]	miette_____1	Default		%default
mieux	100	adv	[pred="mieux_____1",cat=adv]	mieux_____1	Default		%default
mignardement	100	advm	[pred="mignardement_____1",clivee=+,detach_neg = +,cat=adv]	mignardement_____1	Default		%default
mignonnement	100	advm	[pred="mignonnement_____1",clivee=+,detach_neg = +,cat=adv]	mignonnement_____1	Default		%default
militairement	100	advm	[pred="militairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	militairement_____1	Default		%default
militairement	100	advm	[pred="militairement_____2",clivee=+,cat=adv]	militairement_____2	Default		%default
millimétriquement	100	advm	[pred="millimétriquement_____1",clivee=+,cat=adv]	millimétriquement_____1	Default		%default
mimétiquement	100	advm	[pred="mimétiquement_____1",clivee=+,cat=adv]	mimétiquement_____1	Default		%default
minablement	100	advm	[pred="minablement_____1",clivee=+,cat=adv]	minablement_____1	Default		%default
mincement	100	advm	[pred="mincement_____1",clivee=+,cat=adv]	mincement_____1	Default		%default
minimement	100	advm	[pred="minimement_____1",advi=combien,cat=adv]	minimement_____1	Default		%default
minimum minimorum	100	adv	[pred="minimum minimorum_____1",cat=adv]	minimum minimorum_____1	Default		%default
ministériellement	100	advm	[pred="ministériellement_____1",clivee=+,cat=adv]	ministériellement_____1	Default		%default
minoritairement	100	advm	[pred="minoritairement_____1",clivee=+,cat=adv]	minoritairement_____1	Default		%default
minusculement	100	advm	[pred="minusculement_____1",clivee=+,cat=adv]	minusculement_____1	Default		%default
minutieusement	100	advm	[pred="minutieusement_____1",clivee=+,detach_neg = +,cat=adv]	minutieusement_____1	Default		%default
minéralogiquement	100	advm	[pred="minéralogiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	minéralogiquement_____1	Default		%default
minéralogiquement	100	advm	[pred="minéralogiquement_____2",clivee=+,cat=adv]	minéralogiquement_____2	Default		%default
miraculeusement	100	advm	[pred="miraculeusement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	miraculeusement_____1	Default		%default
mirifiquement	100	advm	[pred="mirifiquement_____1",advi=combien,clivee=+,cat=adv]	mirifiquement_____1	Default		%default
mirobolamment	100	advm	[pred="mirobolamment_____1",advi=combien,clivee=+,cat=adv]	mirobolamment_____1	Default		%default
misanthropiquement	100	advm	[pred="misanthropiquement_____1",clivee=+,cat=adv]	misanthropiquement_____1	Default		%default
misogynement	100	advm	[pred="misogynement_____1",clivee=+,detach_neg = +,cat=adv]	misogynement_____1	Default		%default
missi dominici	100	adv	[pred="missi dominici_____1",cat=adv]	missi dominici_____1	Default		%default
misérablement	100	advm	[pred="misérablement_____1",clivee=+,detach_neg = +,cat=adv]	misérablement_____1	Default		%default
miséreusement	100	advm	[pred="miséreusement_____1",clivee=+,detach_neg = +,cat=adv]	miséreusement_____1	Default		%default
miséricordieusement	100	advm	[pred="miséricordieusement_____1",clivee=+,detach_neg = +,cat=adv]	miséricordieusement_____1	Default		%default
miteusement	100	advm	[pred="miteusement_____1",clivee=+,cat=adv]	miteusement_____1	Default		%default
mièvrement	100	advm	[pred="mièvrement_____1",clivee=+,detach_neg = +,cat=adv]	mièvrement_____1	Default		%default
mnémotechniquement	100	advm	[pred="mnémotechniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	mnémotechniquement_____1	Default		%default
mnémotechniquement	100	advm	[pred="mnémotechniquement_____2",clivee=+,cat=adv]	mnémotechniquement_____2	Default		%default
mobilièrement	100	advm	[pred="mobilièrement_____1",clivee=+,cat=adv]	mobilièrement_____1	Default		%default
mochement	100	advm	[pred="mochement_____1",clivee=+,detach_neg = +,cat=adv]	mochement_____1	Default		%default
modalement	100	advm	[pred="modalement_____1",clivee=+,cat=adv]	modalement_____1	Default		%default
moderato	100	adv	[pred="moderato_____1",cat=adv]	moderato_____1	Default		%default
modernement	100	advm	[pred="modernement_____1",clivee=+,cat=adv]	modernement_____1	Default		%default
modestement	100	advm	[pred="modestement_____1",clivee=+,detach_neg = +,cat=adv]	modestement_____1	Default		%default
modiquement	100	advm	[pred="modiquement_____1",advi=combien,cat=adv]	modiquement_____1	Default		%default
modo grosso	100	adv	[pred="modo grosso_____1",cat=adv]	modo grosso_____1	Default		%default
modulairement	100	advm	[pred="modulairement_____1",clivee=+,cat=adv]	modulairement_____1	Default		%default
modérément	100	advm	[pred="modérément_____1",advi=combien,cat=adv]	modérément_____1	Default		%default
moelleusement	100	advm	[pred="moelleusement_____1",clivee=+,cat=adv]	moelleusement_____1	Default		%default
moindrement	100	advm	[pred="moindrement_____1",advi=combien,cat=adv]	moindrement_____1	Default		%default
moins	100	adv	[pred="moins_____1<Obl:(scompl|sn|sa|sinf)>",adv_kind=intens,cat=adv]	moins_____1	Default		%default
moins de	100	adv	[pred="moins de_____1",cat=adv]	moins de_____1	Default		%default
mollassement	100	advm	[pred="mollassement_____1",clivee=+,detach_neg = +,cat=adv]	mollassement_____1	Default		%default
mollement	100	advm	[pred="mollement_____1",clivee=+,detach_neg = +,cat=adv]	mollement_____1	Default		%default
mollo	100	adv	[pred="mollo_____1",cat=adv]	mollo_____1	Default		%default
moléculairement	100	advm	[pred="moléculairement_____1",clivee=+,cat=adv]	moléculairement_____1	Default		%default
momentanément	100	advm	[pred="momentanément_____1",advi=en_combien_de_temps,clivee=+,detach = +,detach_neg = +,cat=adv]	momentanément_____1	Default		%default
monacalement	100	advm	[pred="monacalement_____1",clivee=+,cat=adv]	monacalement_____1	Default		%default
monarchiquement	100	advm	[pred="monarchiquement_____1",clivee=+,cat=adv]	monarchiquement_____1	Default		%default
monastiquement	100	advm	[pred="monastiquement_____1",clivee=+,cat=adv]	monastiquement_____1	Default		%default
mondainement	100	advm	[pred="mondainement_____1",clivee=+,detach_neg = +,cat=adv]	mondainement_____1	Default		%default
mondialement	100	advm	[pred="mondialement_____1",clivee=+,cat=adv]	mondialement_____1	Default		%default
monographiquement	100	advm	[pred="monographiquement_____1",clivee=+,cat=adv]	monographiquement_____1	Default		%default
monolithiquement	100	advm	[pred="monolithiquement_____1",clivee=+,cat=adv]	monolithiquement_____1	Default		%default
monophoniquement	100	advm	[pred="monophoniquement_____1",clivee=+,cat=adv]	monophoniquement_____1	Default		%default
monotonement	100	advm	[pred="monotonement_____1",clivee=+,cat=adv]	monotonement_____1	Default		%default
monstrueusement	100	advm	[pred="monstrueusement_____1",advi=combien,clivee=+,cat=adv]	monstrueusement_____1	Default		%default
montre en main	100	adv	[pred="montre en main_____1",cat=adv]	montre en main_____1	Default		%default
monumentalement	100	advm	[pred="monumentalement_____1",advi=combien,cat=adv]	monumentalement_____1	Default		%default
monétairement	100	advm	[pred="monétairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	monétairement_____1	Default		%default
monétairement	100	advm	[pred="monétairement_____2",clivee=+,cat=adv]	monétairement_____2	Default		%default
moqueusement	100	advm	[pred="moqueusement_____1",clivee=+,detach_neg = +,cat=adv]	moqueusement_____1	Default		%default
moralement	100	advm	[pred="moralement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	moralement_____1	Default		%default
moralement	100	advm	[pred="moralement_____2",clivee=+,detach_neg = +,cat=adv]	moralement_____2	Default		%default
moralistement	100	advm	[pred="moralistement_____1",clivee=+,cat=adv]	moralistement_____1	Default		%default
morbidement	100	advm	[pred="morbidement_____1",clivee=+,cat=adv]	morbidement_____1	Default		%default
mordicus	100	adv	[pred="mordicus_____1",cat=adv]	mordicus_____1	Default		%default
morganatiquement	100	advm	[pred="morganatiquement_____1",clivee=+,cat=adv]	morganatiquement_____1	Default		%default
mornement	100	advm	[pred="mornement_____1",clivee=+,cat=adv]	mornement_____1	Default		%default
morosement	100	advm	[pred="morosement_____1",clivee=+,detach_neg = +,cat=adv]	morosement_____1	Default		%default
morphologiquement	100	advm	[pred="morphologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	morphologiquement_____1	Default		%default
morphologiquement	100	advm	[pred="morphologiquement_____2",clivee=+,cat=adv]	morphologiquement_____2	Default		%default
mortellement	100	advm	[pred="mortellement_____1",advi=combien,clivee=+,cat=adv]	mortellement_____1	Default		%default
mortellement	100	advm	[pred="mortellement_____2",clivee=+,cat=adv]	mortellement_____2	Default		%default
mortuairement	100	advm	[pred="mortuairement_____1",clivee=+,cat=adv]	mortuairement_____1	Default		%default
morveusement	100	advm	[pred="morveusement_____1",clivee=+,detach_neg = +,cat=adv]	morveusement_____1	Default		%default
mot à mot	100	adv	[pred="mot à mot_____1",cat=adv]	mot à mot_____1	Default		%default
motu proprio	100	adv	[pred="motu proprio_____1",cat=adv]	motu proprio_____1	Default		%default
moult	100	adv	[pred="moult_____1",cat=adv]	moult_____1	Default		%default
moutonnièrement	100	advm	[pred="moutonnièrement_____1",clivee=+,cat=adv]	moutonnièrement_____1	Default		%default
moyennement	100	advm	[pred="moyennement_____1",advi=combien,cat=adv]	moyennement_____1	Default		%default
moyenâgeusement	100	advm	[pred="moyenâgeusement_____1",clivee=+,cat=adv]	moyenâgeusement_____1	Default		%default
muettement	100	advm	[pred="muettement_____1",clivee=+,detach_neg = +,cat=adv]	muettement_____1	Default		%default
multidisciplinairement	100	advm	[pred="multidisciplinairement_____1",clivee=+,cat=adv]	multidisciplinairement_____1	Default		%default
multilatéralement	100	advm	[pred="multilatéralement_____1",clivee=+,cat=adv]	multilatéralement_____1	Default		%default
multinationalement	100	advm	[pred="multinationalement_____1",clivee=+,cat=adv]	multinationalement_____1	Default		%default
multiplement	100	advm	[pred="multiplement_____1",advi=combien,cat=adv]	multiplement_____1	Default		%default
multiplicativement	100	advm	[pred="multiplicativement_____1",clivee=+,cat=adv]	multiplicativement_____1	Default		%default
municipalement	100	advm	[pred="municipalement_____1",clivee=+,cat=adv]	municipalement_____1	Default		%default
musculairement	100	advm	[pred="musculairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	musculairement_____1	Default		%default
musculairement	100	advm	[pred="musculairement_____2",clivee=+,cat=adv]	musculairement_____2	Default		%default
musculeusement	100	advm	[pred="musculeusement_____1",clivee=+,cat=adv]	musculeusement_____1	Default		%default
musicalement	100	advm	[pred="musicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	musicalement_____1	Default		%default
musicalement	100	advm	[pred="musicalement_____2",clivee=+,cat=adv]	musicalement_____2	Default		%default
mutatis mutandis	100	adv	[pred="mutatis mutandis_____1",cat=adv]	mutatis mutandis_____1	Default		%default
mutinement	100	advm	[pred="mutinement_____1",clivee=+,detach_neg = +,cat=adv]	mutinement_____1	Default		%default
mutuellement	100	advm	[pred="mutuellement_____1",clivee=+,cat=adv]	mutuellement_____1	Default		%default
mystiquement	100	advm	[pred="mystiquement_____1",clivee=+,cat=adv]	mystiquement_____1	Default		%default
mystérieusement	100	advm	[pred="mystérieusement_____1",clivee=+,cat=adv]	mystérieusement_____1	Default		%default
mythiquement	100	advm	[pred="mythiquement_____1",clivee=+,cat=adv]	mythiquement_____1	Default		%default
mythologiquement	100	advm	[pred="mythologiquement_____1",clivee=+,cat=adv]	mythologiquement_____1	Default		%default
mécaniquement	100	advm	[pred="mécaniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	mécaniquement_____1	Default		%default
mécaniquement	100	advm	[pred="mécaniquement_____2",clivee=+,cat=adv]	mécaniquement_____2	Default		%default
mécanographiquement	100	advm	[pred="mécanographiquement_____1",clivee=+,cat=adv]	mécanographiquement_____1	Default		%default
méchamment	100	advm	[pred="méchamment_____1",clivee=+,detach_neg = +,cat=adv]	méchamment_____1	Default		%default
médiatement	100	advm	[pred="médiatement_____1",clivee=+,cat=adv]	médiatement_____1	Default		%default
médiatiquement	100	advm	[pred="médiatiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	médiatiquement_____1	Default		%default
médiatiquement	100	advm	[pred="médiatiquement_____2",clivee=+,cat=adv]	médiatiquement_____2	Default		%default
médicalement	100	advm	[pred="médicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	médicalement_____1	Default		%default
médicalement	100	advm	[pred="médicalement_____2",clivee=+,cat=adv]	médicalement_____2	Default		%default
médicinalement	100	advm	[pred="médicinalement_____1",clivee=+,cat=adv]	médicinalement_____1	Default		%default
médiocrement	100	advm	[pred="médiocrement_____1",advi=combien,cat=adv]	médiocrement_____1	Default		%default
méditativement	100	advm	[pred="méditativement_____1",clivee=+,detach_neg = +,cat=adv]	méditativement_____1	Default		%default
mélancoliquement	100	advm	[pred="mélancoliquement_____1",clivee=+,detach_neg = +,cat=adv]	mélancoliquement_____1	Default		%default
mélodieusement	100	advm	[pred="mélodieusement_____1",clivee=+,cat=adv]	mélodieusement_____1	Default		%default
mélodiquement	100	advm	[pred="mélodiquement_____1",clivee=+,cat=adv]	mélodiquement_____1	Default		%default
mélodramatiquement	100	advm	[pred="mélodramatiquement_____1",clivee=+,cat=adv]	mélodramatiquement_____1	Default		%default
mémorablement	100	advm	[pred="mémorablement_____1",clivee=+,cat=adv]	mémorablement_____1	Default		%default
méphistophéliquement	100	advm	[pred="méphistophéliquement_____1",clivee=+,detach_neg = +,cat=adv]	méphistophéliquement_____1	Default		%default
méprisablement	100	advm	[pred="méprisablement_____1",clivee=+,detach_neg = +,cat=adv]	méprisablement_____1	Default		%default
méprisamment	100	advm	[pred="méprisamment_____1",clivee=+,detach_neg = +,cat=adv]	méprisamment_____1	Default		%default
méridionalement	100	advm	[pred="méridionalement_____1",clivee=+,cat=adv]	méridionalement_____1	Default		%default
méritoirement	100	advm	[pred="méritoirement_____1",clivee=+,cat=adv]	méritoirement_____1	Default		%default
métaboliquement	100	advm	[pred="métaboliquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	métaboliquement_____1	Default		%default
métalinguistiquement	100	advm	[pred="métalinguistiquement_____1",clivee=+,cat=adv]	métalinguistiquement_____1	Default		%default
métalliquement	100	advm	[pred="métalliquement_____1",clivee=+,cat=adv]	métalliquement_____1	Default		%default
métallographiquement	100	advm	[pred="métallographiquement_____1",clivee=+,cat=adv]	métallographiquement_____1	Default		%default
métaphoriquement	100	advm	[pred="métaphoriquement_____1",clivee=+,cat=adv]	métaphoriquement_____1	Default		%default
métaphysiquement	100	advm	[pred="métaphysiquement_____1",clivee=+,cat=adv]	métaphysiquement_____1	Default		%default
méthodiquement	100	advm	[pred="méthodiquement_____1",clivee=+,detach_neg = +,cat=adv]	méthodiquement_____1	Default		%default
méthodologiquement	100	advm	[pred="méthodologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	méthodologiquement_____1	Default		%default
méticuleusement	100	advm	[pred="méticuleusement_____1",clivee=+,detach_neg = +,cat=adv]	méticuleusement_____1	Default		%default
métonymiquement	100	advm	[pred="métonymiquement_____1",clivee=+,cat=adv]	métonymiquement_____1	Default		%default
météorologiquement	100	advm	[pred="météorologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	météorologiquement_____1	Default		%default
même	100	adv	[pred="même_____1",adv_kind=modnc,cat=adv]	même_____1	Default		%default
même	100	adv	[pred="même_____2",cat=adv]	même_____2	Default		%default
même que	100	adv	[pred="même que_____1",cat=adv]	même que_____1	Default		%default
mêmement	100	advp	[pred="mêmement_____1",detach = +,detach_neg = +,cat=adv]	mêmement_____1	Default		%default
mûrement	100	advm	[pred="mûrement_____1",clivee=+,cat=adv]	mûrement_____1	Default		%default
n fois	100	advp	[pred="n fois_____1",chunk_type=GN,cat=adv]	n fois_____1	Default		%default
n'importe où	100	adv	[pred="n'importe où_____1",cat=adv]	n'importe où_____1	Default		%default
na	100	adv	[pred="na_____1",cat=adv]	na_____1	Default		%default
naguère	100	adv	[pred="naguère_____1",cat=adv]	naguère_____1	Default		%default
nan	100	adv	[pred="non_____1",cat=adv]	non_____1	Default		%default
narcissiquement	100	advm	[pred="narcissiquement_____1",clivee=+,detach_neg = +,cat=adv]	narcissiquement_____1	Default		%default
narquoisement	100	advm	[pred="narquoisement_____1",clivee=+,detach_neg = +,cat=adv]	narquoisement_____1	Default		%default
nasalement	100	advm	[pred="nasalement_____1",clivee=+,cat=adv]	nasalement_____1	Default		%default
nasillardement	100	advm	[pred="nasillardement_____1",clivee=+,cat=adv]	nasillardement_____1	Default		%default
natalement	100	advm	[pred="natalement_____1",clivee=+,cat=adv]	natalement_____1	Default		%default
nationalement	100	advm	[pred="nationalement_____1",clivee=+,cat=adv]	nationalement_____1	Default		%default
nativement	100	advm	[pred="nativement_____1",advi=combien,cat=adv]	nativement_____1	Default		%default
nature	100	adv	[pred="nature_____1",cat=adv]	nature_____1	Default		%default
naturellement	100	advm	[pred="naturellement_____1",clivee=+,detach_neg = +,cat=adv]	naturellement_____1	Default		%default
naturellement	100	advm	[pred="naturellement_____2",clivee=+,cat=adv]	naturellement_____2	Default		%default
naturellement	100	advp	[pred="naturellement_____3",detach = +,detach_neg = +,modnc = +,cat=adv]	naturellement_____3	Default		%default
naïvement	100	advm	[pred="naïvement_____1",clivee=+,detach_neg = +,cat=adv]	naïvement_____1	Default		%default
ne fut -ce qu'	100	adv	[pred="ne fut -ce que_____1",cat=adv,@e]	ne fut -ce que_____1	Default	e	%default
ne fut -ce que	100	adv	[pred="ne fut -ce que_____1",cat=adv]	ne fut -ce que_____1	Default		%default
ne serait -ce qu'	100	adv	[pred="ne serait -ce que_____1",cat=adv,@e]	ne serait -ce que_____1	Default	e	%default
ne serait -ce que	100	adv	[pred="ne serait -ce que_____1",cat=adv]	ne serait -ce que_____1	Default		%default
ne varietur	100	adv	[pred="ne varietur_____1",cat=adv]	ne varietur_____1	Default		%default
nenni	100	adv	[pred="nenni_____1",cat=adv]	nenni_____1	Default		%default
nerveusement	100	advm	[pred="nerveusement_____1",clivee=+,detach_neg = +,cat=adv]	nerveusement_____1	Default		%default
net	100	adv	[pred="net_____1",cat=adv]	net_____1	Default		%default
nettement	100	advm	[pred="nettement_____1",advi=combien,cat=adv]	nettement_____1	Default		%default
nettement	100	advm	[pred="nettement_____2",clivee=+,detach_neg = +,cat=adv]	nettement_____2	Default		%default
neurochirurgicalement	100	advm	[pred="neurochirurgicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	neurochirurgicalement_____1	Default		%default
neurochirurgicalement	100	advm	[pred="neurochirurgicalement_____2",clivee=+,cat=adv]	neurochirurgicalement_____2	Default		%default
neurolinguistiquement	100	advm	[pred="neurolinguistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	neurolinguistiquement_____1	Default		%default
neurolinguistiquement	100	advm	[pred="neurolinguistiquement_____2",clivee=+,cat=adv]	neurolinguistiquement_____2	Default		%default
neurologiquement	100	advm	[pred="neurologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	neurologiquement_____1	Default		%default
neurologiquement	100	advm	[pred="neurologiquement_____2",clivee=+,cat=adv]	neurologiquement_____2	Default		%default
neurophysiologiquement	100	advm	[pred="neurophysiologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	neurophysiologiquement_____1	Default		%default
neurophysiologiquement	100	advm	[pred="neurophysiologiquement_____2",clivee=+,cat=adv]	neurophysiologiquement_____2	Default		%default
neuropsychiatriquement	100	advm	[pred="neuropsychiatriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	neuropsychiatriquement_____1	Default		%default
neuropsychiatriquement	100	advm	[pred="neuropsychiatriquement_____2",clivee=+,cat=adv]	neuropsychiatriquement_____2	Default		%default
neuvièmement	100	advp	[pred="neuvièmement_____1",detach = +,detach_neg = +,cat=adv]	neuvièmement_____1	Default		%default
nez à nez	100	adv	[pred="nez à nez_____1",cat=adv]	nez à nez_____1	Default		%default
ni peu ni prou	100	adv	[pred="ni peu ni prou_____1",cat=adv]	ni peu ni prou_____1	Default		%default
ni plus ni moins	100	adv	[pred="ni plus ni moins_____1",cat=adv]	ni plus ni moins_____1	Default		%default
ni vu ni connu	100	adv	[pred="ni vu ni connu_____1",cat=adv]	ni vu ni connu_____1	Default		%default
niaisement	100	advm	[pred="niaisement_____1",clivee=+,detach_neg = +,cat=adv]	niaisement_____1	Default		%default
nib	100	adv	[pred="nib_____1",cat=adv]	nib_____1	Default		%default
nigaudement	100	advm	[pred="nigaudement_____1",clivee=+,detach_neg = +,cat=adv]	nigaudement_____1	Default		%default
nihil obstat	100	adv	[pred="nihil obstat_____1",cat=adv]	nihil obstat_____1	Default		%default
noblement	100	advm	[pred="noblement_____1",clivee=+,detach_neg = +,cat=adv]	noblement_____1	Default		%default
nocivement	100	advm	[pred="nocivement_____1",clivee=+,cat=adv]	nocivement_____1	Default		%default
nocturnement	100	advm	[pred="nocturnement_____1",clivee=+,cat=adv]	nocturnement_____1	Default		%default
noirement	100	advm	[pred="noirement_____1",clivee=+,cat=adv]	noirement_____1	Default		%default
nombreusement	100	advm	[pred="nombreusement_____1",clivee=+,cat=adv]	nombreusement_____1	Default		%default
nominalement	100	advm	[pred="nominalement_____1",clivee=+,cat=adv]	nominalement_____1	Default		%default
nominativement	100	advm	[pred="nominativement_____1",clivee=+,cat=adv]	nominativement_____1	Default		%default
nommément	100	advm	[pred="nommément_____1",clivee=+,cat=adv]	nommément_____1	Default		%default
non	100	adv	[pred="non_____1",cat=adv]	non_____1	Default		%default
non plus	100	adv	[pred="non plus_____1",cat=adv]	non plus_____1	Default		%default
nonchalamment	100	advm	[pred="nonchalamment_____1",clivee=+,detach_neg = +,cat=adv]	nonchalamment_____1	Default		%default
nonchalemment	100	advm	[pred="nonchalemment_____1",clivee=+,detach_neg = +,cat=adv]	nonchalemment_____1	Default		%default
nonobstant	100	adv	[pred="nonobstant_____1",cat=adv]	nonobstant_____1	Default		%default
normalement	100	advm	[pred="normalement_____1",clivee=+,cat=adv]	normalement_____1	Default		%default
normalement	100	advp	[pred="normalement_____2",detach = +,detach_neg = +,modnc = +,cat=adv]	normalement_____2	Default		%default
normativement	100	advm	[pred="normativement_____1",clivee=+,cat=adv]	normativement_____1	Default		%default
nostalgiquement	100	advm	[pred="nostalgiquement_____1",clivee=+,detach_neg = +,cat=adv]	nostalgiquement_____1	Default		%default
nota bene	100	adv	[pred="nota bene_____1",cat=adv]	nota bene_____1	Default		%default
notablement	100	advm	[pred="notablement_____1",advi=combien,cat=adv]	notablement_____1	Default		%default
notamment	100	advm	[pred="notamment_____1",detach_neg = +,modnc = +,cat=adv]	notamment_____1	Default		%default
notarialement	100	advm	[pred="notarialement_____1",clivee=+,cat=adv]	notarialement_____1	Default		%default
notoirement	100	advm	[pred="notoirement_____1",advi=combien,cat=adv]	notoirement_____1	Default		%default
notoirement	100	advm	[pred="notoirement_____2",clivee=+,cat=adv]	notoirement_____2	Default		%default
nouménalement	100	advm	[pred="nouménalement_____1",clivee=+,cat=adv]	nouménalement_____1	Default		%default
nouvellement	100	advm	[pred="nouvellement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	nouvellement_____1	Default		%default
noétiquement	100	advm	[pred="noétiquement_____1",clivee=+,cat=adv]	noétiquement_____1	Default		%default
nuageusement	100	advm	[pred="nuageusement_____1",clivee=+,cat=adv]	nuageusement_____1	Default		%default
nucléairement	100	advm	[pred="nucléairement_____1",clivee=+,cat=adv]	nucléairement_____1	Default		%default
nuisiblement	100	advm	[pred="nuisiblement_____1",clivee=+,cat=adv]	nuisiblement_____1	Default		%default
nuitamment	100	advm	[pred="nuitamment_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	nuitamment_____1	Default		%default
nulle part	100	adv	[pred="nulle part_____1",cat=adv]	nulle part_____1	Default		%default
nullement	100	advm	[pred="nullement_____1",advi=combien,cat=adv]	nullement_____1	Default		%default
numériquement	100	advm	[pred="numériquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	numériquement_____1	Default		%default
nuncupativement	100	advm	[pred="nuncupativement_____1",clivee=+,cat=adv]	nuncupativement_____1	Default		%default
nuptialement	100	advm	[pred="nuptialement_____1",clivee=+,cat=adv]	nuptialement_____1	Default		%default
néanmoins	100	adv	[pred="néanmoins_____1",cat=adv]	néanmoins_____1	Default		%default
nébuleusement	100	advm	[pred="nébuleusement_____1",clivee=+,detach_neg = +,cat=adv]	nébuleusement_____1	Default		%default
nécessairement	100	advp	[pred="nécessairement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	nécessairement_____1	Default		%default
néfastement	100	advm	[pred="néfastement_____1",clivee=+,cat=adv]	néfastement_____1	Default		%default
négatif	100	adv	[pred="négatif_____1",cat=adv]	négatif_____1	Default		%default
négativement	100	advm	[pred="négativement_____1",clivee=+,cat=adv]	négativement_____1	Default		%default
négligemment	100	advm	[pred="négligemment_____1",clivee=+,detach_neg = +,cat=adv]	négligemment_____1	Default		%default
névrotiquement	100	advm	[pred="névrotiquement_____1",clivee=+,cat=adv]	névrotiquement_____1	Default		%default
nûment	100	advm	[pred="nûment_____1",clivee=+,cat=adv]	nûment_____1	Default		%default
objectivement	100	advm	[pred="objectivement_____1",clivee=+,detach_neg = +,cat=adv]	objectivement_____1	Default		%default
objectivement	100	advp	[pred="objectivement_____2",detach = +,detach_neg = +,cat=adv]	objectivement_____2	Default		%default
oblativement	100	advm	[pred="oblativement_____1",clivee=+,cat=adv]	oblativement_____1	Default		%default
obligatoirement	100	advp	[pred="obligatoirement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	obligatoirement_____1	Default		%default
obligeamment	100	advm	[pred="obligeamment_____1",clivee=+,detach_neg = +,cat=adv]	obligeamment_____1	Default		%default
obliquement	100	advm	[pred="obliquement_____1",clivee=+,cat=adv]	obliquement_____1	Default		%default
obrepticement	100	advm	[pred="obrepticement_____1",clivee=+,cat=adv]	obrepticement_____1	Default		%default
obscurément	100	advm	[pred="obscurément_____1",clivee=+,cat=adv]	obscurément_____1	Default		%default
obscènement	100	advm	[pred="obscènement_____1",clivee=+,detach_neg = +,cat=adv]	obscènement_____1	Default		%default
obsessionnellement	100	advm	[pred="obsessionnellement_____1",clivee=+,cat=adv]	obsessionnellement_____1	Default		%default
obsessivement	100	advm	[pred="obsessivement_____1",clivee=+,cat=adv]	obsessivement_____1	Default		%default
obstinément	100	advm	[pred="obstinément_____1",clivee=+,cat=adv]	obstinément_____1	Default		%default
obséquieusement	100	advm	[pred="obséquieusement_____1",clivee=+,detach_neg = +,cat=adv]	obséquieusement_____1	Default		%default
obtusément	100	advm	[pred="obtusément_____1",clivee=+,detach_neg = +,cat=adv]	obtusément_____1	Default		%default
obèsement	100	advm	[pred="obèsement_____1",clivee=+,detach_neg = +,cat=adv]	obèsement_____1	Default		%default
occasionnellement	100	advm	[pred="occasionnellement_____1",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	occasionnellement_____1	Default		%default
occultement	100	advm	[pred="occultement_____1",clivee=+,cat=adv]	occultement_____1	Default		%default
octavo	100	adv	[pred="octavo_____1",cat=adv]	octavo_____1	Default		%default
octogonalement	100	advm	[pred="octogonalement_____1",clivee=+,cat=adv]	octogonalement_____1	Default		%default
oculairement	100	advm	[pred="oculairement_____1",clivee=+,cat=adv]	oculairement_____1	Default		%default
odieusement	100	advm	[pred="odieusement_____1",clivee=+,detach_neg = +,cat=adv]	odieusement_____1	Default		%default
oecuméniquement	100	advm	[pred="oecuméniquement_____1",clivee=+,cat=adv]	oecuméniquement_____1	Default		%default
oenologiquement	100	advm	[pred="oenologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	oenologiquement_____1	Default		%default
oenologiquement	100	advm	[pred="oenologiquement_____2",clivee=+,cat=adv]	oenologiquement_____2	Default		%default
off	100	adv	[pred="off_____1",cat=adv]	off_____1	Default		%default
offensivement	100	advm	[pred="offensivement_____1",clivee=+,cat=adv]	offensivement_____1	Default		%default
officiellement	100	advm	[pred="officiellement_____1",clivee=+,cat=adv]	officiellement_____1	Default		%default
officiellement	100	advp	[pred="officiellement_____2",detach = +,detach_neg = +,cat=adv]	officiellement_____2	Default		%default
officieusement	100	advm	[pred="officieusement_____1",clivee=+,cat=adv]	officieusement_____1	Default		%default
officieusement	100	advp	[pred="officieusement_____2",detach = +,detach_neg = +,cat=adv]	officieusement_____2	Default		%default
oh	100	adv	[pred="oh_____1",cat=adv]	oh_____1	Default		%default
oiseusement	100	advm	[pred="oiseusement_____1",clivee=+,cat=adv]	oiseusement_____1	Default		%default
oisivement	100	advm	[pred="oisivement_____1",clivee=+,detach_neg = +,cat=adv]	oisivement_____1	Default		%default
olfactivement	100	advm	[pred="olfactivement_____1",clivee=+,cat=adv]	olfactivement_____1	Default		%default
oligarchiquement	100	advm	[pred="oligarchiquement_____1",clivee=+,cat=adv]	oligarchiquement_____1	Default		%default
ombrageusement	100	advm	[pred="ombrageusement_____1",clivee=+,detach_neg = +,cat=adv]	ombrageusement_____1	Default		%default
on ne peut moins	100	adv	[pred="on ne peut moins_____1",adv_kind=intens,cat=adv]	on ne peut moins_____1	Default		%default
on ne peut plus	100	adv	[pred="on ne peut plus_____1",adv_kind=intens,cat=adv]	on ne peut plus_____1	Default		%default
onc	100	adv	[pred="onc_____1",cat=adv]	onc_____1	Default		%default
oncques	100	adv	[pred="oncques_____1",cat=adv]	oncques_____1	Default		%default
onctueusement	100	advm	[pred="onctueusement_____1",clivee=+,detach_neg = +,cat=adv]	onctueusement_____1	Default		%default
onduleusement	100	advm	[pred="onduleusement_____1",clivee=+,cat=adv]	onduleusement_____1	Default		%default
oniriquement	100	advm	[pred="oniriquement_____1",clivee=+,cat=adv]	oniriquement_____1	Default		%default
onomatopéiquement	100	advm	[pred="onomatopéiquement_____1",clivee=+,cat=adv]	onomatopéiquement_____1	Default		%default
onques	100	adv	[pred="onques_____1",cat=adv]	onques_____1	Default		%default
ontologiquement	100	advm	[pred="ontologiquement_____1",clivee=+,cat=adv]	ontologiquement_____1	Default		%default
onzièmement	100	advp	[pred="onzièmement_____1",detach = +,detach_neg = +,cat=adv]	onzièmement_____1	Default		%default
onéreusement	100	advm	[pred="onéreusement_____1",clivee=+,cat=adv]	onéreusement_____1	Default		%default
op. cit..	100	adv	[pred="opus citatum_____1",cat=adv]	opus citatum_____1	Default		%default
opaquement	100	advm	[pred="opaquement_____1",clivee=+,cat=adv]	opaquement_____1	Default		%default
ophtalmologiquement	100	advm	[pred="ophtalmologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ophtalmologiquement_____1	Default		%default
ophtalmologiquement	100	advm	[pred="ophtalmologiquement_____2",clivee=+,cat=adv]	ophtalmologiquement_____2	Default		%default
opiniâtrement	100	advm	[pred="opiniâtrement_____1",clivee=+,detach_neg = +,cat=adv]	opiniâtrement_____1	Default		%default
opportunistement	100	advm	[pred="opportunistement_____1",clivee=+,detach_neg = +,cat=adv]	opportunistement_____1	Default		%default
opportunément	100	advm	[pred="opportunément_____1",detach = +,detach_neg = +,cat=adv]	opportunément_____1	Default		%default
opposément	100	advm	[pred="opposément_____1",clivee=+,cat=adv]	opposément_____1	Default		%default
oppressivement	100	advm	[pred="oppressivement_____1",clivee=+,cat=adv]	oppressivement_____1	Default		%default
optimalement	100	advm	[pred="optimalement_____1",clivee=+,cat=adv]	optimalement_____1	Default		%default
optimistement	100	advm	[pred="optimistement_____1",clivee=+,detach_neg = +,cat=adv]	optimistement_____1	Default		%default
optionnellement	100	advm	[pred="optionnellement_____1",clivee=+,cat=adv]	optionnellement_____1	Default		%default
optiquement	100	advm	[pred="optiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	optiquement_____1	Default		%default
opulemment	100	advm	[pred="opulemment_____1",clivee=+,cat=adv]	opulemment_____1	Default		%default
opus citatum	100	adv	[pred="opus citatum_____1",cat=adv]	opus citatum_____1	Default		%default
opérationnellement	100	advm	[pred="opérationnellement_____1",clivee=+,cat=adv]	opérationnellement_____1	Default		%default
opératoirement	100	advm	[pred="opératoirement_____1",clivee=+,cat=adv]	opératoirement_____1	Default		%default
orageusement	100	advm	[pred="orageusement_____1",clivee=+,cat=adv]	orageusement_____1	Default		%default
oralement	100	advm	[pred="oralement_____1",clivee=+,cat=adv]	oralement_____1	Default		%default
oratoirement	100	advm	[pred="oratoirement_____1",clivee=+,cat=adv]	oratoirement_____1	Default		%default
orbiculairement	100	advm	[pred="orbiculairement_____1",clivee=+,cat=adv]	orbiculairement_____1	Default		%default
orchestralement	100	advm	[pred="orchestralement_____1",clivee=+,cat=adv]	orchestralement_____1	Default		%default
ordinairement	100	advp	[pred="ordinairement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	ordinairement_____1	Default		%default
ordurièrement	100	advm	[pred="ordurièrement_____1",clivee=+,detach_neg = +,cat=adv]	ordurièrement_____1	Default		%default
ores	100	adv	[pred="ores_____1",cat=adv]	ores_____1	Default		%default
organiquement	100	advm	[pred="organiquement_____1",clivee=+,cat=adv]	organiquement_____1	Default		%default
organoleptiquement	100	advm	[pred="organoleptiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	organoleptiquement_____1	Default		%default
organoleptiquement	100	advm	[pred="organoleptiquement_____2",clivee=+,cat=adv]	organoleptiquement_____2	Default		%default
orgiaquement	100	advm	[pred="orgiaquement_____1",clivee=+,cat=adv]	orgiaquement_____1	Default		%default
orgiastiquement	100	advm	[pred="orgiastiquement_____1",clivee=+,cat=adv]	orgiastiquement_____1	Default		%default
orgueilleusement	100	advm	[pred="orgueilleusement_____1",clivee=+,detach_neg = +,cat=adv]	orgueilleusement_____1	Default		%default
orientalement	100	advm	[pred="orientalement_____1",clivee=+,cat=adv]	orientalement_____1	Default		%default
originairement	100	advm	[pred="originairement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	originairement_____1	Default		%default
originalement	100	advm	[pred="originalement_____1",clivee=+,detach_neg = +,cat=adv]	originalement_____1	Default		%default
originellement	100	advm	[pred="originellement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	originellement_____1	Default		%default
ornithologiquement	100	advm	[pred="ornithologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	ornithologiquement_____1	Default		%default
orographiquement	100	advm	[pred="orographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	orographiquement_____1	Default		%default
orographiquement	100	advm	[pred="orographiquement_____2",clivee=+,cat=adv]	orographiquement_____2	Default		%default
orthodoxement	100	advm	[pred="orthodoxement_____1",clivee=+,detach_neg = +,cat=adv]	orthodoxement_____1	Default		%default
orthogonalement	100	advm	[pred="orthogonalement_____1",clivee=+,cat=adv]	orthogonalement_____1	Default		%default
orthographiquement	100	advm	[pred="orthographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	orthographiquement_____1	Default		%default
orthographiquement	100	advm	[pred="orthographiquement_____2",clivee=+,cat=adv]	orthographiquement_____2	Default		%default
orthopédiquement	100	advm	[pred="orthopédiquement_____1",clivee=+,cat=adv]	orthopédiquement_____1	Default		%default
osmotiquement	100	advm	[pred="osmotiquement_____1",clivee=+,cat=adv]	osmotiquement_____1	Default		%default
ostensiblement	100	advm	[pred="ostensiblement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	ostensiblement_____1	Default		%default
ostentatoirement	100	advm	[pred="ostentatoirement_____1",clivee=+,cat=adv]	ostentatoirement_____1	Default		%default
oublieusement	100	advm	[pred="oublieusement_____1",clivee=+,detach_neg = +,cat=adv]	oublieusement_____1	Default		%default
ouf	100	adv	[pred="ouf_____1",cat=adv]	ouf_____1	Default		%default
oui	100	adv	[pred="oui_____1",cat=adv]	oui_____1	Default		%default
ouille	100	adv	[pred="ouille_____1",cat=adv]	ouille_____1	Default		%default
out	100	adv	[pred="out_____1",cat=adv]	out_____1	Default		%default
outrageusement	100	advm	[pred="outrageusement_____1",advi=combien,clivee=+,cat=adv]	outrageusement_____1	Default		%default
outrancièrement	100	advm	[pred="outrancièrement_____1",advi=combien,clivee=+,cat=adv]	outrancièrement_____1	Default		%default
outre	100	adv	[pred="outre_____1",cat=adv]	outre_____1	Default		%default
outre mesure	100	adv	[pred="outre mesure_____1",cat=adv]	outre mesure_____1	Default		%default
outre-Atlantique	100	adv	[pred="outre-Atlantique_____1",cat=adv]	outre-Atlantique_____1	Default		%default
outre-Manche	100	adv	[pred="outre-Manche_____1",cat=adv]	outre-Manche_____1	Default		%default
outre-Rhin	100	adv	[pred="outre-Rhin_____1",cat=adv]	outre-Rhin_____1	Default		%default
outre-mer	100	adv	[pred="outre-mer_____1",cat=adv]	outre-mer_____1	Default		%default
outre-tombe	100	adv	[pred="outre-tombe_____1",cat=adv]	outre-tombe_____1	Default		%default
outrecuidamment	100	advm	[pred="outrecuidamment_____1",clivee=+,detach_neg = +,cat=adv]	outrecuidamment_____1	Default		%default
ouvertement	100	advm	[pred="ouvertement_____1",clivee=+,cat=adv]	ouvertement_____1	Default		%default
ovalement	100	advm	[pred="ovalement_____1",clivee=+,cat=adv]	ovalement_____1	Default		%default
oviparement	100	advm	[pred="oviparement_____1",clivee=+,cat=adv]	oviparement_____1	Default		%default
ovoviviparement	100	advm	[pred="ovoviviparement_____1",clivee=+,cat=adv]	ovoviviparement_____1	Default		%default
p-ê	100	adv	[pred="peut-être_____1",cat=adv]	peut-être_____1	Default		%default
pachaliquement	100	advm	[pred="pachaliquement_____1",clivee=+,cat=adv]	pachaliquement_____1	Default		%default
pacifiquement	100	advm	[pred="pacifiquement_____1",clivee=+,detach_neg = +,cat=adv]	pacifiquement_____1	Default		%default
paillardement	100	advm	[pred="paillardement_____1",clivee=+,detach_neg = +,cat=adv]	paillardement_____1	Default		%default
pairement	100	advm	[pred="pairement_____1",cat=adv]	pairement_____1	Default		%default
paisiblement	100	advm	[pred="paisiblement_____1",clivee=+,detach_neg = +,cat=adv]	paisiblement_____1	Default		%default
palingénésiquement	100	advm	[pred="palingénésiquement_____1",clivee=+,cat=adv]	palingénésiquement_____1	Default		%default
palpablement	100	advm	[pred="palpablement_____1",clivee=+,cat=adv]	palpablement_____1	Default		%default
paléographiquement	100	advm	[pred="paléographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	paléographiquement_____1	Default		%default
paléographiquement	100	advm	[pred="paléographiquement_____2",clivee=+,cat=adv]	paléographiquement_____2	Default		%default
paléontologiquement	100	advm	[pred="paléontologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	paléontologiquement_____1	Default		%default
paléontologiquement	100	advm	[pred="paléontologiquement_____2",clivee=+,cat=adv]	paléontologiquement_____2	Default		%default
panoramiquement	100	advm	[pred="panoramiquement_____1",clivee=+,cat=adv]	panoramiquement_____1	Default		%default
pantagruéliquement	100	advm	[pred="pantagruéliquement_____1",clivee=+,cat=adv]	pantagruéliquement_____1	Default		%default
papelardement	100	advm	[pred="papelardement_____1",clivee=+,detach_neg = +,cat=adv]	papelardement_____1	Default		%default
par ailleurs	100	adv	[pred="par ailleurs_____1",cat=adv]	par ailleurs_____1	Default		%default
par après	100	adv	[pred="par après_____1",cat=adv]	par après_____1	Default		%default
par avance	100	adv	[pred="par avance_____1",cat=adv]	par avance_____1	Default		%default
par coeur	100	adv	[pred="par coeur_____1",cat=adv]	par coeur_____1	Default		%default
par comparaison	100	adv	[pred="par comparaison_____1",cat=adv]	par comparaison_____1	Default		%default
par conséquent	100	adv	[pred="par conséquent_____1",cat=adv]	par conséquent_____1	Default		%default
par contre	100	adv	[pred="par contre_____1",cat=adv]	par contre_____1	Default		%default
par exemple	100	adv	[pred="par exemple_____1",cat=adv]	par exemple_____1	Default		%default
par hasard	100	adv	[pred="par hasard_____1",cat=adv]	par hasard_____1	Default		%default
par moments	100	adv	[pred="par moments_____1",cat=adv]	par moments_____1	Default		%default
par suite	100	adv	[pred="par suite_____1",cat=adv]	par suite_____1	Default		%default
par trop	100	adv	[pred="par trop_____1",cat=adv]	par trop_____1	Default		%default
par-ci	100	adv	[pred="par-ci_____1",cat=adv]	par-ci_____1	Default		%default
par-derrière	100	adv	[pred="par-derrière_____1",cat=adv]	par-derrière_____1	Default		%default
par-dessous	100	adv	[pred="par-dessous_____1",cat=adv]	par-dessous_____1	Default		%default
par-dessus	100	adv	[pred="par-dessus_____1",cat=adv]	par-dessus_____1	Default		%default
par-dessus tout	100	adv	[pred="par-dessus tout_____1",cat=adv]	par-dessus tout_____1	Default		%default
par-devant	100	adv	[pred="par-devant_____1",cat=adv]	par-devant_____1	Default		%default
par-devers	100	adv	[pred="par-devers_____1",cat=adv]	par-devers_____1	Default		%default
par-là	100	adv	[pred="par-là_____1",cat=adv]	par-là_____1	Default		%default
paraboliquement	100	advm	[pred="paraboliquement_____1",clivee=+,cat=adv]	paraboliquement_____1	Default		%default
paradigmatiquement	100	advm	[pred="paradigmatiquement_____1",clivee=+,cat=adv]	paradigmatiquement_____1	Default		%default
paradoxalement	100	advm	[pred="paradoxalement_____1",clivee=+,detach_neg = +,cat=adv]	paradoxalement_____1	Default		%default
paradoxalement	100	advp	[pred="paradoxalement_____2",detach = +,detach_neg = +,cat=adv]	paradoxalement_____2	Default		%default
parallactiquement	100	advm	[pred="parallactiquement_____1",clivee=+,cat=adv]	parallactiquement_____1	Default		%default
parallèlement	100	advm	[pred="parallèlement_____1",clivee=+,cat=adv]	parallèlement_____1	Default		%default
parallèlement	100	advp	[pred="parallèlement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	parallèlement_____2	Default		%default
paranoïaquement	100	advm	[pred="paranoïaquement_____1",clivee=+,detach_neg = +,cat=adv]	paranoïaquement_____1	Default		%default
parasitairement	100	advm	[pred="parasitairement_____1",clivee=+,cat=adv]	parasitairement_____1	Default		%default
parcellairement	100	advm	[pred="parcellairement_____1",clivee=+,cat=adv]	parcellairement_____1	Default		%default
parcellement	100	advm	[pred="parcellement_____1",clivee=+,cat=adv]	parcellement_____1	Default		%default
parcimonieusement	100	advm	[pred="parcimonieusement_____1",clivee=+,detach_neg = +,cat=adv]	parcimonieusement_____1	Default		%default
pardon	100	adv	[pred="pardon_____1",cat=adv]	pardon_____1	Default		%default
pareil	100	adv	[pred="pareil_____1",cat=adv]	pareil_____1	Default		%default
pareillement	100	advm	[pred="pareillement_____1",clivee=+,cat=adv]	pareillement_____1	Default		%default
pareillement	100	advp	[pred="pareillement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	pareillement_____2	Default		%default
paresseusement	100	advm	[pred="paresseusement_____1",clivee=+,detach_neg = +,cat=adv]	paresseusement_____1	Default		%default
parfaitement	100	advm	[pred="parfaitement_____1",advi=combien,cat=adv]	parfaitement_____1	Default		%default
parfaitement	100	advm	[pred="parfaitement_____2",clivee=+,cat=adv]	parfaitement_____2	Default		%default
parfois	100	adv	[pred="parfois_____1",cat=adv]	parfois_____1	Default		%default
parisiennement	100	advm	[pred="parisiennement_____1",clivee=+,cat=adv]	parisiennement_____1	Default		%default
paritairement	100	advm	[pred="paritairement_____1",clivee=+,cat=adv]	paritairement_____1	Default		%default
parlementairement	100	advm	[pred="parlementairement_____1",clivee=+,cat=adv]	parlementairement_____1	Default		%default
parodiquement	100	advm	[pred="parodiquement_____1",clivee=+,cat=adv]	parodiquement_____1	Default		%default
paroxystiquement	100	advm	[pred="paroxystiquement_____1",clivee=+,cat=adv]	paroxystiquement_____1	Default		%default
parthénogénétiquement	100	advm	[pred="parthénogénétiquement_____1",clivee=+,cat=adv]	parthénogénétiquement_____1	Default		%default
partialement	100	advm	[pred="partialement_____1",clivee=+,detach_neg = +,cat=adv]	partialement_____1	Default		%default
particulièrement	100	advm	[pred="particulièrement_____1",modnc = +,cat=adv]	particulièrement_____1	Default		%default
particulièrement	100	advm	[pred="particulièrement_____2",advi=combien,cat=adv]	particulièrement_____2	Default		%default
partiellement	100	advm	[pred="partiellement_____1",advi=combien,cat=adv]	partiellement_____1	Default		%default
partout	100	adv	[pred="partout_____1",cat=adv]	partout_____1	Default		%default
pas du tout	100	adv	[pred="pas du tout_____1",cat=adv]	pas du tout_____1	Default		%default
pas encore	100	adv	[pred="pas encore_____1",cat=adv]	pas encore_____1	Default		%default
pas moins de	100	adv	[pred="pas moins de_____1",cat=adv]	pas moins de_____1	Default		%default
pas plus de	100	adv	[pred="pas plus de_____1",cat=adv]	pas plus de_____1	Default		%default
pas à pas	100	adv	[pred="pas à pas_____1",cat=adv]	pas à pas_____1	Default		%default
passablement	100	advm	[pred="passablement_____1",advi=combien,cat=adv]	passablement_____1	Default		%default
passablement	100	advm	[pred="passablement_____2",cat=adv]	passablement_____2	Default		%default
passagèrement	100	advm	[pred="passagèrement_____1",advi=en_combien_de_temps,clivee=+,detach = +,detach_neg = +,cat=adv]	passagèrement_____1	Default		%default
passim	100	adv	[pred="passim_____1",cat=adv]	passim_____1	Default		%default
passionnellement	100	advm	[pred="passionnellement_____1",clivee=+,cat=adv]	passionnellement_____1	Default		%default
passionnément	100	advm	[pred="passionnément_____1",advi=combien,clivee=+,cat=adv]	passionnément_____1	Default		%default
passivement	100	advm	[pred="passivement_____1",clivee=+,detach_neg = +,cat=adv]	passivement_____1	Default		%default
pastoralement	100	advm	[pred="pastoralement_____1",clivee=+,cat=adv]	pastoralement_____1	Default		%default
patati et patata	100	adv	[pred="patati et patata_____1",cat=adv]	patati et patata_____1	Default		%default
pataudement	100	advm	[pred="pataudement_____1",clivee=+,detach_neg = +,cat=adv]	pataudement_____1	Default		%default
patelinement	100	advm	[pred="patelinement_____1",clivee=+,detach_neg = +,cat=adv]	patelinement_____1	Default		%default
patemment	100	advm	[pred="patemment_____1",clivee=+,cat=adv]	patemment_____1	Default		%default
paternellement	100	advm	[pred="paternellement_____1",clivee=+,detach_neg = +,cat=adv]	paternellement_____1	Default		%default
paternement	100	advm	[pred="paternement_____1",clivee=+,detach_neg = +,cat=adv]	paternement_____1	Default		%default
pathologiquement	100	advm	[pred="pathologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pathologiquement_____1	Default		%default
pathologiquement	100	advm	[pred="pathologiquement_____2",clivee=+,cat=adv]	pathologiquement_____2	Default		%default
pathétiquement	100	advm	[pred="pathétiquement_____1",clivee=+,detach_neg = +,cat=adv]	pathétiquement_____1	Default		%default
patibulairement	100	advm	[pred="patibulairement_____1",clivee=+,detach_neg = +,cat=adv]	patibulairement_____1	Default		%default
patiemment	100	advm	[pred="patiemment_____1",clivee=+,detach_neg = +,cat=adv]	patiemment_____1	Default		%default
patriarcalement	100	advm	[pred="patriarcalement_____1",clivee=+,detach_neg = +,cat=adv]	patriarcalement_____1	Default		%default
patrilinéairement	100	advm	[pred="patrilinéairement_____1",clivee=+,cat=adv]	patrilinéairement_____1	Default		%default
patrimonialement	100	advm	[pred="patrimonialement_____1",clivee=+,cat=adv]	patrimonialement_____1	Default		%default
patriotiquement	100	advm	[pred="patriotiquement_____1",clivee=+,cat=adv]	patriotiquement_____1	Default		%default
pauvrement	100	advm	[pred="pauvrement_____1",clivee=+,detach_neg = +,cat=adv]	pauvrement_____1	Default		%default
paysannement	100	advm	[pred="paysannement_____1",clivee=+,detach_neg = +,cat=adv]	paysannement_____1	Default		%default
païennement	100	advm	[pred="païennement_____1",clivee=+,detach_neg = +,cat=adv]	païennement_____1	Default		%default
peinardement	100	advm	[pred="peinardement_____1",clivee=+,detach_neg = +,cat=adv]	peinardement_____1	Default		%default
peineusement	100	advm	[pred="peineusement_____1",clivee=+,detach_neg = +,cat=adv]	peineusement_____1	Default		%default
penaudement	100	advm	[pred="penaudement_____1",clivee=+,detach_neg = +,cat=adv]	penaudement_____1	Default		%default
pendablement	100	advm	[pred="pendablement_____1",clivee=+,detach_neg = +,cat=adv]	pendablement_____1	Default		%default
pendulairement	100	advm	[pred="pendulairement_____1",clivee=+,cat=adv]	pendulairement_____1	Default		%default
pensivement	100	advm	[pred="pensivement_____1",clivee=+,detach_neg = +,cat=adv]	pensivement_____1	Default		%default
pentatoniquement	100	advm	[pred="pentatoniquement_____1",clivee=+,cat=adv]	pentatoniquement_____1	Default		%default
per capita	100	adv	[pred="per capita_____1",cat=adv]	per capita_____1	Default		%default
perceptiblement	100	advm	[pred="perceptiblement_____1",clivee=+,cat=adv]	perceptiblement_____1	Default		%default
perceptivement	100	advm	[pred="perceptivement_____1",clivee=+,cat=adv]	perceptivement_____1	Default		%default
perdurablement	100	advm	[pred="perdurablement_____1",advi=en_combien_de_temps,clivee=+,cat=adv]	perdurablement_____1	Default		%default
perfidement	100	advm	[pred="perfidement_____1",clivee=+,detach_neg = +,cat=adv]	perfidement_____1	Default		%default
permissivement	100	advm	[pred="permissivement_____1",clivee=+,detach_neg = +,cat=adv]	permissivement_____1	Default		%default
pernicieusement	100	advm	[pred="pernicieusement_____1",clivee=+,detach_neg = +,cat=adv]	pernicieusement_____1	Default		%default
perpendiculairement	100	advm	[pred="perpendiculairement_____1",clivee=+,cat=adv]	perpendiculairement_____1	Default		%default
perplexement	100	advm	[pred="perplexement_____1",clivee=+,detach_neg = +,cat=adv]	perplexement_____1	Default		%default
perpétuellement	100	advm	[pred="perpétuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	perpétuellement_____1	Default		%default
persifleusement	100	advm	[pred="persifleusement_____1",clivee=+,detach_neg = +,cat=adv]	persifleusement_____1	Default		%default
personnellement	100	advm	[pred="personnellement_____1",modnc = +,cat=adv]	personnellement_____1	Default		%default
personnellement	100	advm	[pred="personnellement_____2",clivee=+,cat=adv]	personnellement_____2	Default		%default
personnellement	100	advp	[pred="personnellement_____3",detach = +,detach_neg = +,cat=adv]	personnellement_____3	Default		%default
perspicacement	100	advm	[pred="perspicacement_____1",clivee=+,detach_neg = +,cat=adv]	perspicacement_____1	Default		%default
persuasivement	100	advm	[pred="persuasivement_____1",clivee=+,detach_neg = +,cat=adv]	persuasivement_____1	Default		%default
persévéramment	100	advm	[pred="persévéramment_____1",clivee=+,detach_neg = +,cat=adv]	persévéramment_____1	Default		%default
pertinemment	100	advm	[pred="pertinemment_____1",clivee=+,cat=adv]	pertinemment_____1	Default		%default
pertinemment	100	advm	[pred="pertinemment_____2",clivee=+,cat=adv]	pertinemment_____2	Default		%default
perversement	100	advm	[pred="perversement_____1",clivee=+,detach_neg = +,cat=adv]	perversement_____1	Default		%default
perversement	100	advp	[pred="perversement_____2",detach = +,detach_neg = +,cat=adv]	perversement_____2	Default		%default
pesamment	100	advm	[pred="pesamment_____1",clivee=+,detach_neg = +,cat=adv]	pesamment_____1	Default		%default
pesamment	100	advm	[pred="pesamment_____2",clivee=+,cat=adv]	pesamment_____2	Default		%default
pessimistement	100	advm	[pred="pessimistement_____1",clivee=+,detach_neg = +,cat=adv]	pessimistement_____1	Default		%default
petit	100	adv	[pred="petit_____1",cat=adv]	petit_____1	Default		%default
petit à petit	100	adv	[pred="petit à petit_____1",cat=adv]	petit à petit_____1	Default		%default
petit à petit	100	adv	[pred="petit-à-petit_____1",cat=adv]	petit-à-petit_____1	Default		%default
petit-à-petit	100	adv	[pred="petit-à-petit_____1",cat=adv]	petit-à-petit_____1	Default		%default
petitement	100	advm	[pred="petitement_____1",clivee=+,detach_neg = +,cat=adv]	petitement_____1	Default		%default
petitement	100	advm	[pred="petitement_____2",clivee=+,cat=adv]	petitement_____2	Default		%default
peu	100	adv	[pred="peu_____1",adv_kind=intens,cat=adv]	peu_____1	Default		%default
peu	100	adv	[pred="peu_____2",adv_kind=modprep,cat=adv]	peu_____2	Default		%default
peu importe	100	adv	[pred="peu importe_____1",cat=adv]	peu importe_____1	Default		%default
peu ou prou	100	adv	[pred="peu ou prou_____1",cat=adv]	peu ou prou_____1	Default		%default
peu à peu	100	adv	[pred="peu à peu_____1",cat=adv]	peu à peu_____1	Default		%default
peureusement	100	advm	[pred="peureusement_____1",clivee=+,detach_neg = +,cat=adv]	peureusement_____1	Default		%default
peut-être	100	adv	[pred="peut-être_____1",cat=adv]	peut-être_____1	Default		%default
pharisaïquement	100	advm	[pred="pharisaïquement_____1",clivee=+,cat=adv]	pharisaïquement_____1	Default		%default
pharmacologiquement	100	advm	[pred="pharmacologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pharmacologiquement_____1	Default		%default
philanthropiquement	100	advm	[pred="philanthropiquement_____1",clivee=+,cat=adv]	philanthropiquement_____1	Default		%default
philistinement	100	advm	[pred="philistinement_____1",clivee=+,detach_neg = +,cat=adv]	philistinement_____1	Default		%default
philologiquement	100	advm	[pred="philologiquement_____1",clivee=+,cat=adv]	philologiquement_____1	Default		%default
philosophiquement	100	advm	[pred="philosophiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	philosophiquement_____1	Default		%default
philosophiquement	100	advm	[pred="philosophiquement_____2",clivee=+,cat=adv]	philosophiquement_____2	Default		%default
philosophiquement	100	advm	[pred="philosophiquement_____3",clivee=+,cat=adv]	philosophiquement_____3	Default		%default
phobiquement	100	advm	[pred="phobiquement_____1",clivee=+,detach_neg = +,cat=adv]	phobiquement_____1	Default		%default
phoniquement	100	advm	[pred="phoniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phoniquement_____1	Default		%default
phoniquement	100	advm	[pred="phoniquement_____2",clivee=+,cat=adv]	phoniquement_____2	Default		%default
phonographiquement	100	advm	[pred="phonographiquement_____1",clivee=+,cat=adv]	phonographiquement_____1	Default		%default
phonologiquement	100	advm	[pred="phonologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phonologiquement_____1	Default		%default
phonologiquement	100	advm	[pred="phonologiquement_____2",clivee=+,cat=adv]	phonologiquement_____2	Default		%default
phonématiquement	100	advm	[pred="phonématiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phonématiquement_____1	Default		%default
phonématiquement	100	advm	[pred="phonématiquement_____2",clivee=+,cat=adv]	phonématiquement_____2	Default		%default
phonémiquement	100	advm	[pred="phonémiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phonémiquement_____1	Default		%default
phonémiquement	100	advm	[pred="phonémiquement_____2",clivee=+,cat=adv]	phonémiquement_____2	Default		%default
phonétiquement	100	advm	[pred="phonétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phonétiquement_____1	Default		%default
phonétiquement	100	advm	[pred="phonétiquement_____2",clivee=+,cat=adv]	phonétiquement_____2	Default		%default
photographiquement	100	advm	[pred="photographiquement_____1",clivee=+,cat=adv]	photographiquement_____1	Default		%default
photométriquement	100	advm	[pred="photométriquement_____1",clivee=+,cat=adv]	photométriquement_____1	Default		%default
phrénologiquement	100	advm	[pred="phrénologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phrénologiquement_____1	Default		%default
phrénologiquement	100	advm	[pred="phrénologiquement_____2",clivee=+,cat=adv]	phrénologiquement_____2	Default		%default
phylogénétiquement	100	advm	[pred="phylogénétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phylogénétiquement_____1	Default		%default
phylogénétiquement	100	advm	[pred="phylogénétiquement_____2",clivee=+,cat=adv]	phylogénétiquement_____2	Default		%default
phylétiquement	100	advm	[pred="phylétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phylétiquement_____1	Default		%default
physiologiquement	100	advm	[pred="physiologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	physiologiquement_____1	Default		%default
physiologiquement	100	advm	[pred="physiologiquement_____2",clivee=+,cat=adv]	physiologiquement_____2	Default		%default
physionomiquement	100	advm	[pred="physionomiquement_____1",clivee=+,cat=adv]	physionomiquement_____1	Default		%default
physiquement	100	advm	[pred="physiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	physiquement_____1	Default		%default
physiquement	100	advm	[pred="physiquement_____2",clivee=+,cat=adv]	physiquement_____2	Default		%default
phénoménalement	100	advm	[pred="phénoménalement_____1",advi=combien,cat=adv]	phénoménalement_____1	Default		%default
phénoméniquement	100	advm	[pred="phénoméniquement_____1",clivee=+,cat=adv]	phénoméniquement_____1	Default		%default
phénoménologiquement	100	advm	[pred="phénoménologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	phénoménologiquement_____1	Default		%default
phénoménologiquement	100	advm	[pred="phénoménologiquement_____2",clivee=+,cat=adv]	phénoménologiquement_____2	Default		%default
pianissimo	100	adv	[pred="pianissimo_____1",cat=adv]	pianissimo_____1	Default		%default
pianistiquement	100	advm	[pred="pianistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pianistiquement_____1	Default		%default
piano	100	adv	[pred="piano_____1",cat=adv]	piano_____1	Default		%default
pictographiquement	100	advm	[pred="pictographiquement_____1",clivee=+,cat=adv]	pictographiquement_____1	Default		%default
picturalement	100	advm	[pred="picturalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	picturalement_____1	Default		%default
pied à pied	100	adv	[pred="pied à pied_____1",cat=adv]	pied à pied_____1	Default		%default
pieusement	100	advm	[pred="pieusement_____1",clivee=+,detach_neg = +,cat=adv]	pieusement_____1	Default		%default
pile	100	adv	[pred="pile_____1",cat=adv]	pile_____1	Default		%default
pinailleusement	100	advm	[pred="pinailleusement_____1",clivee=+,detach_neg = +,cat=adv]	pinailleusement_____1	Default		%default
pingrement	100	advm	[pred="pingrement_____1",clivee=+,detach_neg = +,cat=adv]	pingrement_____1	Default		%default
piteusement	100	advm	[pred="piteusement_____1",clivee=+,detach_neg = +,cat=adv]	piteusement_____1	Default		%default
pitoyablement	100	advm	[pred="pitoyablement_____1",clivee=+,detach_neg = +,cat=adv]	pitoyablement_____1	Default		%default
pittoresquement	100	advm	[pred="pittoresquement_____1",clivee=+,cat=adv]	pittoresquement_____1	Default		%default
piètrement	100	advm	[pred="piètrement_____1",clivee=+,cat=adv]	piètrement_____1	Default		%default
placidement	100	advm	[pred="placidement_____1",clivee=+,detach_neg = +,cat=adv]	placidement_____1	Default		%default
plaignardement	100	advm	[pred="plaignardement_____1",clivee=+,detach_neg = +,cat=adv]	plaignardement_____1	Default		%default
plaintivement	100	advm	[pred="plaintivement_____1",clivee=+,detach_neg = +,cat=adv]	plaintivement_____1	Default		%default
plaisamment	100	advm	[pred="plaisamment_____1",clivee=+,detach_neg = +,cat=adv]	plaisamment_____1	Default		%default
plantureusement	100	advm	[pred="plantureusement_____1",advi=combien,cat=adv]	plantureusement_____1	Default		%default
planétairement	100	advm	[pred="planétairement_____1",clivee=+,cat=adv]	planétairement_____1	Default		%default
plastiquement	100	advm	[pred="plastiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	plastiquement_____1	Default		%default
plastiquement	100	advm	[pred="plastiquement_____2",clivee=+,cat=adv]	plastiquement_____2	Default		%default
plat	100	adv	[pred="plat_____1",cat=adv]	plat_____1	Default		%default
platement	100	advm	[pred="platement_____1",clivee=+,detach_neg = +,cat=adv]	platement_____1	Default		%default
platoniquement	100	advm	[pred="platoniquement_____1",clivee=+,cat=adv]	platoniquement_____1	Default		%default
plausiblement	100	advp	[pred="plausiblement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	plausiblement_____1	Default		%default
pleinement	100	advm	[pred="pleinement_____1",advi=combien,cat=adv]	pleinement_____1	Default		%default
pleutrement	100	advm	[pred="pleutrement_____1",clivee=+,detach_neg = +,cat=adv]	pleutrement_____1	Default		%default
pluriannuellement	100	advm	[pred="pluriannuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	pluriannuellement_____1	Default		%default
pluridisciplinairement	100	advm	[pred="pluridisciplinairement_____1",clivee=+,cat=adv]	pluridisciplinairement_____1	Default		%default
plurilatéralement	100	advm	[pred="plurilatéralement_____1",clivee=+,cat=adv]	plurilatéralement_____1	Default		%default
plurinationalement	100	advm	[pred="plurinationalement_____1",clivee=+,cat=adv]	plurinationalement_____1	Default		%default
plus	100	adv	[pred="plus_____1<Obl:(scompl|sn|sa|sinf)>",adv_kind=intens,cat=adv]	plus_____1	Default		%default
plus avant	100	adv	[pred="plus avant_____1",cat=adv]	plus avant_____1	Default		%default
plus jamais	100	adv	[pred="plus jamais_____1",cat=adv]	plus jamais_____1	Default		%default
plus ou moins	100	adv	[pred="plus ou moins_____1",cat=adv]	plus ou moins_____1	Default		%default
plus qu'	100	adv	[pred="plus que_____1",cat=adv,@e]	plus que_____1	Default	e	%default
plus que	100	adv	[pred="plus que_____1",cat=adv]	plus que_____1	Default		%default
plus que jamais	100	adv	[pred="plus que jamais_____1",cat=adv]	plus que jamais_____1	Default		%default
plus tard	100	adv	[pred="plus tard_____1",cat=adv]	plus tard_____1	Default		%default
plutoniquement	100	advm	[pred="plutoniquement_____1",clivee=+,cat=adv]	plutoniquement_____1	Default		%default
plutôt	100	adv	[pred="plutôt_____1",cat=adv]	plutôt_____1	Default		%default
plébéiennement	100	advm	[pred="plébéiennement_____1",clivee=+,detach_neg = +,cat=adv]	plébéiennement_____1	Default		%default
plénièrement	100	advm	[pred="plénièrement_____1",advi=combien,cat=adv]	plénièrement_____1	Default		%default
pléonastiquement	100	advm	[pred="pléonastiquement_____1",clivee=+,cat=adv]	pléonastiquement_____1	Default		%default
pléthoriquement	100	advm	[pred="pléthoriquement_____1",advi=combien,cat=adv]	pléthoriquement_____1	Default		%default
pneumatiquement	100	advm	[pred="pneumatiquement_____1",clivee=+,cat=adv]	pneumatiquement_____1	Default		%default
point	100	adv	[pred="point_____1",cat=adv]	point_____1	Default		%default
point pour point	100	adv	[pred="point pour point_____1",cat=adv]	point pour point_____1	Default		%default
pointilleusement	100	advm	[pred="pointilleusement_____1",clivee=+,detach_neg = +,cat=adv]	pointilleusement_____1	Default		%default
pointu	100	adv	[pred="pointu_____1",cat=adv]	pointu_____1	Default		%default
poisseusement	100	advm	[pred="poisseusement_____1",clivee=+,cat=adv]	poisseusement_____1	Default		%default
policièrement	100	advm	[pred="policièrement_____1",clivee=+,cat=adv]	policièrement_____1	Default		%default
poliment	100	advm	[pred="poliment_____1",clivee=+,detach_neg = +,cat=adv]	poliment_____1	Default		%default
polissonnement	100	advm	[pred="polissonnement_____1",clivee=+,detach_neg = +,cat=adv]	polissonnement_____1	Default		%default
politiquement	100	advm	[pred="politiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	politiquement_____1	Default		%default
politiquement	100	advm	[pred="politiquement_____2",clivee=+,cat=adv]	politiquement_____2	Default		%default
poltronnement	100	advm	[pred="poltronnement_____1",clivee=+,detach_neg = +,cat=adv]	poltronnement_____1	Default		%default
polygonalement	100	advm	[pred="polygonalement_____1",clivee=+,cat=adv]	polygonalement_____1	Default		%default
polyphoniquement	100	advm	[pred="polyphoniquement_____1",clivee=+,cat=adv]	polyphoniquement_____1	Default		%default
polyédriquement	100	advm	[pred="polyédriquement_____1",clivee=+,cat=adv]	polyédriquement_____1	Default		%default
polémiquement	100	advm	[pred="polémiquement_____1",clivee=+,cat=adv]	polémiquement_____1	Default		%default
pompeusement	100	advm	[pred="pompeusement_____1",clivee=+,detach_neg = +,cat=adv]	pompeusement_____1	Default		%default
ponctuellement	100	advm	[pred="ponctuellement_____1",clivee=+,detach_neg = +,cat=adv]	ponctuellement_____1	Default		%default
ponctuellement	100	advm	[pred="ponctuellement_____2",clivee=+,cat=adv]	ponctuellement_____2	Default		%default
pondérément	100	advm	[pred="pondérément_____1",clivee=+,detach_neg = +,cat=adv]	pondérément_____1	Default		%default
pontificalement	100	advm	[pred="pontificalement_____1",clivee=+,cat=adv]	pontificalement_____1	Default		%default
populairement	100	advm	[pred="populairement_____1",clivee=+,detach_neg = +,cat=adv]	populairement_____1	Default		%default
positionnellement	100	advm	[pred="positionnellement_____1",clivee=+,cat=adv]	positionnellement_____1	Default		%default
positivement	100	advm	[pred="positivement_____1",advi=combien,cat=adv]	positivement_____1	Default		%default
positivement	100	advm	[pred="positivement_____2",clivee=+,cat=adv]	positivement_____2	Default		%default
positivement	100	advm	[pred="positivement_____3",clivee=+,cat=adv]	positivement_____3	Default		%default
possessivement	100	advm	[pred="possessivement_____1",clivee=+,detach_neg = +,cat=adv]	possessivement_____1	Default		%default
possessoirement	100	advm	[pred="possessoirement_____1",clivee=+,cat=adv]	possessoirement_____1	Default		%default
possiblement	100	advp	[pred="possiblement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	possiblement_____1	Default		%default
post mortem	100	adv	[pred="post mortem_____1",cat=adv]	post mortem_____1	Default		%default
post scriptum	100	adv	[pred="post scriptum_____1",cat=adv]	post scriptum_____1	Default		%default
posthumément	100	advm	[pred="posthumément_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	posthumément_____1	Default		%default
postérieurement	100	advm	[pred="postérieurement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	postérieurement_____1	Default		%default
posément	100	advm	[pred="posément_____1",clivee=+,detach_neg = +,cat=adv]	posément_____1	Default		%default
potentiellement	100	advm	[pred="potentiellement_____1",clivee=+,cat=adv]	potentiellement_____1	Default		%default
pour autant	100	adv	[pred="pour autant_____1",cat=adv]	pour autant_____1	Default		%default
pour de bon	100	adv	[pred="pour de bon_____1",cat=adv]	pour de bon_____1	Default		%default
pour la plupart	100	adv	[pred="pour la plupart_____1",cat=adv]	pour la plupart_____1	Default		%default
pour le moins	100	adv	[pred="pour le moins_____1",cat=adv]	pour le moins_____1	Default		%default
pour preuve	100	adv	[pred="pour preuve_____1",cat=adv]	pour preuve_____1	Default		%default
pour sûr	100	adv	[pred="pour sûr_____1",cat=adv]	pour sûr_____1	Default		%default
pour toujours	100	adv	[pred="pour toujours_____1",cat=adv]	pour toujours_____1	Default		%default
pour un peu	100	adv	[pred="pour un peu_____1",cat=adv]	pour un peu_____1	Default		%default
pourtant	100	adv	[pred="pourtant_____1",cat=adv]	pourtant_____1	Default		%default
poussivement	100	advm	[pred="poussivement_____1",clivee=+,detach_neg = +,cat=adv]	poussivement_____1	Default		%default
poétiquement	100	advm	[pred="poétiquement_____1",clivee=+,cat=adv]	poétiquement_____1	Default		%default
pragmatiquement	100	advm	[pred="pragmatiquement_____1",clivee=+,detach_neg = +,cat=adv]	pragmatiquement_____1	Default		%default
pratiquement	100	advm	[pred="pratiquement_____1",modnc = +,cat=adv]	pratiquement_____1	Default		%default
pratiquement	100	advm	[pred="pratiquement_____2",clivee=+,cat=adv]	pratiquement_____2	Default		%default
pratiquement	100	advp	[pred="pratiquement_____3",detach = +,detach_neg = +,cat=adv]	pratiquement_____3	Default		%default
premièrement	100	advp	[pred="premièrement_____1",detach = +,detach_neg = +,cat=adv]	premièrement_____1	Default		%default
presqu'	100	adv	[pred="presque_____1",adv_kind=modnc,cat=adv,@e]	presque_____1	Default	e	%default
presqu'	100	adv	[pred="presque_____2",cat=adv,@e]	presque_____2	Default	e	%default
presque	100	adv	[pred="presque_____1",adv_kind=modnc,cat=adv]	presque_____1	Default		%default
presque	100	adv	[pred="presque_____2",cat=adv]	presque_____2	Default		%default
prestement	100	advm	[pred="prestement_____1",clivee=+,detach_neg = +,cat=adv]	prestement_____1	Default		%default
prestissimo	100	adv	[pred="prestissimo_____1",cat=adv]	prestissimo_____1	Default		%default
presto	100	adv	[pred="presto_____1",cat=adv]	presto_____1	Default		%default
primairement	100	advm	[pred="primairement_____1",clivee=+,detach_neg = +,cat=adv]	primairement_____1	Default		%default
primesautièrement	100	advm	[pred="primesautièrement_____1",clivee=+,detach_neg = +,cat=adv]	primesautièrement_____1	Default		%default
primitivement	100	advm	[pred="primitivement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	primitivement_____1	Default		%default
primo	100	adv	[pred="primo_____1",cat=adv]	primo_____1	Default		%default
primordialement	100	advm	[pred="primordialement_____1",clivee=+,cat=adv]	primordialement_____1	Default		%default
principalement	100	advm	[pred="principalement_____1",detach_neg = +,modnc = +,cat=adv]	principalement_____1	Default		%default
princièrement	100	advm	[pred="princièrement_____1",clivee=+,cat=adv]	princièrement_____1	Default		%default
printanièrement	100	advm	[pred="printanièrement_____1",clivee=+,cat=adv]	printanièrement_____1	Default		%default
prioritairement	100	advm	[pred="prioritairement_____1",clivee=+,cat=adv]	prioritairement_____1	Default		%default
privativement	100	advm	[pred="privativement_____1",clivee=+,cat=adv]	privativement_____1	Default		%default
privément	100	advm	[pred="privément_____1",clivee=+,cat=adv]	privément_____1	Default		%default
pro domo	100	adv	[pred="pro domo_____1",cat=adv]	pro domo_____1	Default		%default
pro forma	100	adv	[pred="pro forma_____1",cat=adv]	pro forma_____1	Default		%default
probablement	100	advp	[pred="probablement_____1<Suj:(scompl)>",detach = +,detach_neg = +,modnc = +,cat=adv]	probablement_____1	Default		%default
probement	100	advm	[pred="probement_____1",clivee=+,detach_neg = +,cat=adv]	probement_____1	Default		%default
problématiquement	100	advm	[pred="problématiquement_____1",clivee=+,cat=adv]	problématiquement_____1	Default		%default
processionnellement	100	advm	[pred="processionnellement_____1",clivee=+,cat=adv]	processionnellement_____1	Default		%default
prochainement	100	advm	[pred="prochainement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	prochainement_____1	Default		%default
proche	100	adv	[pred="proche_____1",cat=adv]	proche_____1	Default		%default
proconsulairement	100	advm	[pred="proconsulairement_____1",clivee=+,cat=adv]	proconsulairement_____1	Default		%default
prodigalement	100	advm	[pred="prodigalement_____1",clivee=+,cat=adv]	prodigalement_____1	Default		%default
prodigieusement	100	advm	[pred="prodigieusement_____1",advi=combien,clivee=+,cat=adv]	prodigieusement_____1	Default		%default
prodiguement	100	advm	[pred="prodiguement_____1",clivee=+,detach_neg = +,cat=adv]	prodiguement_____1	Default		%default
productivement	100	advm	[pred="productivement_____1",clivee=+,cat=adv]	productivement_____1	Default		%default
profanement	100	advm	[pred="profanement_____1",clivee=+,detach_neg = +,cat=adv]	profanement_____1	Default		%default
professionnellement	100	advm	[pred="professionnellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	professionnellement_____1	Default		%default
professionnellement	100	advm	[pred="professionnellement_____2",clivee=+,cat=adv]	professionnellement_____2	Default		%default
professoralement	100	advm	[pred="professoralement_____1",clivee=+,cat=adv]	professoralement_____1	Default		%default
profitablement	100	advm	[pred="profitablement_____1",clivee=+,cat=adv]	profitablement_____1	Default		%default
profond	100	adv	[pred="profond_____1",cat=adv]	profond_____1	Default		%default
profondément	100	advm	[pred="profondément_____1",advi=combien,clivee=+,cat=adv]	profondément_____1	Default		%default
profondément	100	advm	[pred="profondément_____2",clivee=+,cat=adv]	profondément_____2	Default		%default
profusément	100	advm	[pred="profusément_____1",clivee=+,cat=adv]	profusément_____1	Default		%default
progressivement	100	advm	[pred="progressivement_____1",clivee=+,cat=adv]	progressivement_____1	Default		%default
projectivement	100	advm	[pred="projectivement_____1",clivee=+,cat=adv]	projectivement_____1	Default		%default
proleptiquement	100	advm	[pred="proleptiquement_____1",clivee=+,cat=adv]	proleptiquement_____1	Default		%default
prolixement	100	advm	[pred="prolixement_____1",clivee=+,detach_neg = +,cat=adv]	prolixement_____1	Default		%default
prolétairement	100	advm	[pred="prolétairement_____1",clivee=+,detach_neg = +,cat=adv]	prolétairement_____1	Default		%default
promptement	100	advm	[pred="promptement_____1",clivee=+,cat=adv]	promptement_____1	Default		%default
pronominalement	100	advm	[pred="pronominalement_____1",clivee=+,cat=adv]	pronominalement_____1	Default		%default
prophylactiquement	100	advm	[pred="prophylactiquement_____1",clivee=+,cat=adv]	prophylactiquement_____1	Default		%default
prophétiquement	100	advm	[pred="prophétiquement_____1",clivee=+,cat=adv]	prophétiquement_____1	Default		%default
propicement	100	advm	[pred="propicement_____1",clivee=+,cat=adv]	propicement_____1	Default		%default
propitiatoirement	100	advm	[pred="propitiatoirement_____1",clivee=+,cat=adv]	propitiatoirement_____1	Default		%default
proportionnellement	100	advm	[pred="proportionnellement_____1",clivee=+,cat=adv]	proportionnellement_____1	Default		%default
proportionnément	100	advm	[pred="proportionnément_____1",clivee=+,cat=adv]	proportionnément_____1	Default		%default
proprement	100	advm	[pred="proprement_____1",advi=combien,cat=adv]	proprement_____1	Default		%default
proprement	100	advm	[pred="proprement_____2",clivee=+,detach_neg = +,cat=adv]	proprement_____2	Default		%default
proprement	100	advm	[pred="proprement_____3",clivee=+,cat=adv]	proprement_____3	Default		%default
prosaïquement	100	advm	[pred="prosaïquement_____1",clivee=+,cat=adv]	prosaïquement_____1	Default		%default
prosodiquement	100	advm	[pred="prosodiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	prosodiquement_____1	Default		%default
prosodiquement	100	advm	[pred="prosodiquement_____2",clivee=+,cat=adv]	prosodiquement_____2	Default		%default
prospectivement	100	advm	[pred="prospectivement_____1",clivee=+,cat=adv]	prospectivement_____1	Default		%default
prospèrement	100	advm	[pred="prospèrement_____1",clivee=+,cat=adv]	prospèrement_____1	Default		%default
protocolairement	100	advm	[pred="protocolairement_____1",clivee=+,cat=adv]	protocolairement_____1	Default		%default
prou	100	adv	[pred="prou_____1",cat=adv]	prou_____1	Default		%default
proverbialement	100	advm	[pred="proverbialement_____1",clivee=+,cat=adv]	proverbialement_____1	Default		%default
providentiellement	100	advm	[pred="providentiellement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	providentiellement_____1	Default		%default
provincialement	100	advm	[pred="provincialement_____1",clivee=+,detach_neg = +,cat=adv]	provincialement_____1	Default		%default
provisionnellement	100	advm	[pred="provisionnellement_____1",clivee=+,cat=adv]	provisionnellement_____1	Default		%default
provisoirement	100	advm	[pred="provisoirement_____1",advi=en_combien_de_temps,clivee=+,detach = +,detach_neg = +,cat=adv]	provisoirement_____1	Default		%default
prudement	100	advm	[pred="prudement_____1",clivee=+,detach_neg = +,cat=adv]	prudement_____1	Default		%default
prudemment	100	advm	[pred="prudemment_____1",clivee=+,detach_neg = +,cat=adv]	prudemment_____1	Default		%default
prudemment	100	advp	[pred="prudemment_____2",detach = +,detach_neg = +,cat=adv]	prudemment_____2	Default		%default
prudhommesquement	100	advm	[pred="prudhommesquement_____1",clivee=+,detach_neg = +,cat=adv]	prudhommesquement_____1	Default		%default
près	100	adv	[pred="près_____1",cat=adv]	près_____1	Default		%default
préalablement	100	advm	[pred="préalablement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	préalablement_____1	Default		%default
précairement	100	advm	[pred="précairement_____1",clivee=+,cat=adv]	précairement_____1	Default		%default
précairement	100	advm	[pred="précairement_____2",clivee=+,cat=adv]	précairement_____2	Default		%default
précautionneusement	100	advm	[pred="précautionneusement_____1",clivee=+,detach_neg = +,cat=adv]	précautionneusement_____1	Default		%default
précieusement	100	advm	[pred="précieusement_____1",clivee=+,detach_neg = +,cat=adv]	précieusement_____1	Default		%default
précieusement	100	advm	[pred="précieusement_____2",clivee=+,cat=adv]	précieusement_____2	Default		%default
précipitamment	100	advm	[pred="précipitamment_____1",clivee=+,cat=adv]	précipitamment_____1	Default		%default
préciputairement	100	advm	[pred="préciputairement_____1",clivee=+,cat=adv]	préciputairement_____1	Default		%default
précisément	100	advm	[pred="précisément_____1",modnc = +,cat=adv]	précisément_____1	Default		%default
précisément	100	advm	[pred="précisément_____2",clivee=+,detach_neg = +,cat=adv]	précisément_____2	Default		%default
précisément	100	advp	[pred="précisément_____3",detach = +,detach_neg = +,cat=adv]	précisément_____3	Default		%default
précocement	100	advm	[pred="précocement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	précocement_____1	Default		%default
précédemment	100	advm	[pred="précédemment_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	précédemment_____1	Default		%default
préférablement	100	advm	[pred="préférablement_____1",clivee=+,cat=adv]	préférablement_____1	Default		%default
préférentiellement	100	advm	[pred="préférentiellement_____1",clivee=+,cat=adv]	préférentiellement_____1	Default		%default
préhistoriquement	100	advm	[pred="préhistoriquement_____1",clivee=+,cat=adv]	préhistoriquement_____1	Default		%default
préliminairement	100	advm	[pred="préliminairement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	préliminairement_____1	Default		%default
prématurément	100	advm	[pred="prématurément_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	prématurément_____1	Default		%default
prémonitoirement	100	advm	[pred="prémonitoirement_____1",clivee=+,cat=adv]	prémonitoirement_____1	Default		%default
prépositivement	100	advm	[pred="prépositivement_____1",clivee=+,cat=adv]	prépositivement_____1	Default		%default
présentement	100	advm	[pred="présentement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	présentement_____1	Default		%default
présomptivement	100	advm	[pred="présomptivement_____1",clivee=+,cat=adv]	présomptivement_____1	Default		%default
présomptueusement	100	advm	[pred="présomptueusement_____1",clivee=+,detach_neg = +,cat=adv]	présomptueusement_____1	Default		%default
présumablement	100	advp	[pred="présumablement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	présumablement_____1	Default		%default
prétendument	100	advm	[pred="prétendument_____1",advi=combien,cat=adv]	prétendument_____1	Default		%default
prétentieusement	100	advm	[pred="prétentieusement_____1",clivee=+,detach_neg = +,cat=adv]	prétentieusement_____1	Default		%default
préventivement	100	advm	[pred="préventivement_____1",clivee=+,cat=adv]	préventivement_____1	Default		%default
prévisionnellement	100	advm	[pred="prévisionnellement_____1",clivee=+,cat=adv]	prévisionnellement_____1	Default		%default
prévôtalement	100	advm	[pred="prévôtalement_____1",clivee=+,cat=adv]	prévôtalement_____1	Default		%default
psychanalytiquement	100	advm	[pred="psychanalytiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychanalytiquement_____1	Default		%default
psychanalytiquement	100	advm	[pred="psychanalytiquement_____2",clivee=+,cat=adv]	psychanalytiquement_____2	Default		%default
psychiatriquement	100	advm	[pred="psychiatriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychiatriquement_____1	Default		%default
psychiatriquement	100	advm	[pred="psychiatriquement_____2",clivee=+,cat=adv]	psychiatriquement_____2	Default		%default
psychiquement	100	advm	[pred="psychiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychiquement_____1	Default		%default
psychiquement	100	advm	[pred="psychiquement_____2",clivee=+,cat=adv]	psychiquement_____2	Default		%default
psycholinguistiquement	100	advm	[pred="psycholinguistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psycholinguistiquement_____1	Default		%default
psycholinguistiquement	100	advm	[pred="psycholinguistiquement_____2",clivee=+,cat=adv]	psycholinguistiquement_____2	Default		%default
psychologiquement	100	advm	[pred="psychologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychologiquement_____1	Default		%default
psychologiquement	100	advm	[pred="psychologiquement_____2",clivee=+,cat=adv]	psychologiquement_____2	Default		%default
psychométriquement	100	advm	[pred="psychométriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychométriquement_____1	Default		%default
psychométriquement	100	advm	[pred="psychométriquement_____2",clivee=+,cat=adv]	psychométriquement_____2	Default		%default
psychopathologiquement	100	advm	[pred="psychopathologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychopathologiquement_____1	Default		%default
psychopathologiquement	100	advm	[pred="psychopathologiquement_____2",clivee=+,cat=adv]	psychopathologiquement_____2	Default		%default
psychophysiologiquement	100	advm	[pred="psychophysiologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	psychophysiologiquement_____1	Default		%default
psychophysiologiquement	100	advm	[pred="psychophysiologiquement_____2",clivee=+,cat=adv]	psychophysiologiquement_____2	Default		%default
psychosomatiquement	100	advm	[pred="psychosomatiquement_____1",clivee=+,cat=adv]	psychosomatiquement_____1	Default		%default
psychothérapiquement	100	advm	[pred="psychothérapiquement_____1",clivee=+,cat=adv]	psychothérapiquement_____1	Default		%default
puamment	100	advm	[pred="puamment_____1",clivee=+,cat=adv]	puamment_____1	Default		%default
publicitairement	100	advm	[pred="publicitairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	publicitairement_____1	Default		%default
publicitairement	100	advm	[pred="publicitairement_____2",clivee=+,cat=adv]	publicitairement_____2	Default		%default
publiquement	100	advm	[pred="publiquement_____1",clivee=+,cat=adv]	publiquement_____1	Default		%default
pudibondement	100	advm	[pred="pudibondement_____1",clivee=+,detach_neg = +,cat=adv]	pudibondement_____1	Default		%default
pudiquement	100	advm	[pred="pudiquement_____1",clivee=+,detach_neg = +,cat=adv]	pudiquement_____1	Default		%default
pugnacement	100	advm	[pred="pugnacement_____1",clivee=+,detach_neg = +,cat=adv]	pugnacement_____1	Default		%default
puissamment	100	advm	[pred="puissamment_____1",advi=combien,cat=adv]	puissamment_____1	Default		%default
purement	100	advm	[pred="purement_____1",advi=combien,cat=adv]	purement_____1	Default		%default
purement	100	advm	[pred="purement_____2",clivee=+,detach_neg = +,cat=adv]	purement_____2	Default		%default
puritainement	100	advm	[pred="puritainement_____1",clivee=+,detach_neg = +,cat=adv]	puritainement_____1	Default		%default
pusillanimement	100	advm	[pred="pusillanimement_____1",clivee=+,detach_neg = +,cat=adv]	pusillanimement_____1	Default		%default
putativement	100	advm	[pred="putativement_____1",clivee=+,cat=adv]	putativement_____1	Default		%default
puérilement	100	advm	[pred="puérilement_____1",clivee=+,detach_neg = +,cat=adv]	puérilement_____1	Default		%default
pyramidalement	100	advm	[pred="pyramidalement_____1",clivee=+,cat=adv]	pyramidalement_____1	Default		%default
pâlement	100	advm	[pred="pâlement_____1",clivee=+,cat=adv]	pâlement_____1	Default		%default
pâteusement	100	advm	[pred="pâteusement_____1",clivee=+,cat=adv]	pâteusement_____1	Default		%default
pécuniairement	100	advm	[pred="pécuniairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pécuniairement_____1	Default		%default
pécuniairement	100	advm	[pred="pécuniairement_____2",clivee=+,cat=adv]	pécuniairement_____2	Default		%default
pédagogiquement	100	advm	[pred="pédagogiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pédagogiquement_____1	Default		%default
pédamment	100	advm	[pred="pédamment_____1",clivee=+,detach_neg = +,cat=adv]	pédamment_____1	Default		%default
pédantement	100	advm	[pred="pédantement_____1",clivee=+,detach_neg = +,cat=adv]	pédantement_____1	Default		%default
pédantesquement	100	advm	[pred="pédantesquement_____1",clivee=+,cat=adv]	pédantesquement_____1	Default		%default
pédestrement	100	advm	[pred="pédestrement_____1",clivee=+,cat=adv]	pédestrement_____1	Default		%default
péjorativement	100	advm	[pred="péjorativement_____1",clivee=+,cat=adv]	péjorativement_____1	Default		%default
pénalement	100	advm	[pred="pénalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pénalement_____1	Default		%default
pénalement	100	advm	[pred="pénalement_____2",clivee=+,cat=adv]	pénalement_____2	Default		%default
pénardement	100	advm	[pred="pénardement_____1",clivee=+,detach_neg = +,cat=adv]	pénardement_____1	Default		%default
péniblement	100	advm	[pred="péniblement_____1",clivee=+,cat=adv]	péniblement_____1	Default		%default
pénitentiairement	100	advm	[pred="pénitentiairement_____1",clivee=+,cat=adv]	pénitentiairement_____1	Default		%default
pépèrement	100	advm	[pred="pépèrement_____1",clivee=+,detach_neg = +,cat=adv]	pépèrement_____1	Default		%default
péremptoirement	100	advm	[pred="péremptoirement_____1",clivee=+,detach_neg = +,cat=adv]	péremptoirement_____1	Default		%default
périlleusement	100	advm	[pred="périlleusement_____1",clivee=+,cat=adv]	périlleusement_____1	Default		%default
périodiquement	100	advm	[pred="périodiquement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	périodiquement_____1	Default		%default
périphériquement	100	advm	[pred="périphériquement_____1",clivee=+,cat=adv]	périphériquement_____1	Default		%default
pétrochimiquement	100	advm	[pred="pétrochimiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pétrochimiquement_____1	Default		%default
pétrographiquement	100	advm	[pred="pétrographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	pétrographiquement_____1	Default		%default
pétrographiquement	100	advm	[pred="pétrographiquement_____2",clivee=+,cat=adv]	pétrographiquement_____2	Default		%default
pêle-mêle	100	adv	[pred="pêle-mêle_____1",cat=adv]	pêle-mêle_____1	Default		%default
q.e.d..	100	adv	[pred="quod erat demonstrandum_____1",cat=adv]	quod erat demonstrandum_____1	Default		%default
qed	100	adv	[pred="quod erat demonstrandum_____1",cat=adv]	quod erat demonstrandum_____1	Default		%default
qqpart	100	adv	[pred="quelque part_____1",cat=adv]	quelque part_____1	Default		%default
quadrangulairement	100	advm	[pred="quadrangulairement_____1",clivee=+,cat=adv]	quadrangulairement_____1	Default		%default
quadruplement	100	advm	[pred="quadruplement_____1",advi=combien,cat=adv]	quadruplement_____1	Default		%default
qualitativement	100	advm	[pred="qualitativement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	qualitativement_____1	Default		%default
quand même	100	adv	[pred="quand même_____1",cat=adv]	quand même_____1	Default		%default
quantitativement	100	advm	[pred="quantitativement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	quantitativement_____1	Default		%default
quarante	100	adv	[pred="quarante_____1",cat=adv]	quarante_____1	Default		%default
quarantièmement	100	advp	[pred="quarantièmement_____1",detach = +,detach_neg = +,cat=adv]	quarantièmement_____1	Default		%default
quarto	100	adv	[pred="quarto_____1",cat=adv]	quarto_____1	Default		%default
quasi	100	adv	[pred="quasi_____1",cat=adv]	quasi_____1	Default		%default
quasiment	100	advm	[pred="quasiment_____1",advi=combien,cat=adv]	quasiment_____1	Default		%default
quater	100	adv	[pred="quater_____1",cat=adv]	quater_____1	Default		%default
quatorzièmement	100	advp	[pred="quatorzièmement_____1",detach = +,detach_neg = +,cat=adv]	quatorzièmement_____1	Default		%default
quatre à quatre	100	adv	[pred="quatre à quatre_____1",cat=adv]	quatre à quatre_____1	Default		%default
quatrièmement	100	advp	[pred="quatrièmement_____1",detach = +,detach_neg = +,cat=adv]	quatrièmement_____1	Default		%default
quellement	100	advm	[pred="quellement_____1",advi=combien,cat=adv]	quellement_____1	Default		%default
quelque part	100	adv	[pred="quelque part_____1",cat=adv]	quelque part_____1	Default		%default
quelque peu	100	adv	[pred="quelque peu_____1",cat=adv]	quelque peu_____1	Default		%default
quelquefois	100	adv	[pred="quelquefois_____1",cat=adv]	quelquefois_____1	Default		%default
quinto	100	adv	[pred="quinto_____1",cat=adv]	quinto_____1	Default		%default
quintuplement	100	advm	[pred="quintuplement_____1",advi=combien,cat=adv]	quintuplement_____1	Default		%default
quinzièmement	100	advp	[pred="quinzièmement_____1",detach = +,detach_neg = +,cat=adv]	quinzièmement_____1	Default		%default
quittement	100	advm	[pred="quittement_____1",clivee=+,cat=adv]	quittement_____1	Default		%default
quiètement	100	advm	[pred="quiètement_____1",clivee=+,detach_neg = +,cat=adv]	quiètement_____1	Default		%default
quod erat demonstrandum	100	adv	[pred="quod erat demonstrandum_____1",cat=adv]	quod erat demonstrandum_____1	Default		%default
quoi qu'il en soit	100	adv	[pred="quoi qu'il en soit_____1",cat=adv]	quoi qu'il en soit_____1	Default		%default
quoique	100	adv	[pred="quoique_____1",cat=adv]	quoique_____1	Default		%default
quotidiennement	100	advm	[pred="quotidiennement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	quotidiennement_____1	Default		%default
racialement	100	advm	[pred="racialement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	racialement_____1	Default		%default
racoleusement	100	advm	[pred="racoleusement_____1",clivee=+,detach_neg = +,cat=adv]	racoleusement_____1	Default		%default
radiairement	100	advm	[pred="radiairement_____1",clivee=+,cat=adv]	radiairement_____1	Default		%default
radialement	100	advm	[pred="radialement_____1",clivee=+,cat=adv]	radialement_____1	Default		%default
radicalement	100	advm	[pred="radicalement_____1",advi=combien,cat=adv]	radicalement_____1	Default		%default
radieusement	100	advm	[pred="radieusement_____1",advi=combien,clivee=+,cat=adv]	radieusement_____1	Default		%default
radinement	100	advm	[pred="radinement_____1",clivee=+,detach_neg = +,cat=adv]	radinement_____1	Default		%default
radiographiquement	100	advm	[pred="radiographiquement_____1",clivee=+,cat=adv]	radiographiquement_____1	Default		%default
radiologiquement	100	advm	[pred="radiologiquement_____1",clivee=+,cat=adv]	radiologiquement_____1	Default		%default
radiophoniquement	100	advm	[pred="radiophoniquement_____1",clivee=+,cat=adv]	radiophoniquement_____1	Default		%default
radioélectriquement	100	advm	[pred="radioélectriquement_____1",clivee=+,cat=adv]	radioélectriquement_____1	Default		%default
rageusement	100	advm	[pred="rageusement_____1",clivee=+,detach_neg = +,cat=adv]	rageusement_____1	Default		%default
raidement	100	advm	[pred="raidement_____1",clivee=+,detach_neg = +,cat=adv]	raidement_____1	Default		%default
railleusement	100	advm	[pred="railleusement_____1",clivee=+,detach_neg = +,cat=adv]	railleusement_____1	Default		%default
raisonnablement	100	advm	[pred="raisonnablement_____1",advi=combien,clivee=+,cat=adv]	raisonnablement_____1	Default		%default
raisonnablement	100	advm	[pred="raisonnablement_____2",clivee=+,detach_neg = +,cat=adv]	raisonnablement_____2	Default		%default
rapacement	100	advm	[pred="rapacement_____1",clivee=+,detach_neg = +,cat=adv]	rapacement_____1	Default		%default
rapidement	100	advm	[pred="rapidement_____1",clivee=+,cat=adv]	rapidement_____1	Default		%default
rarement	100	advm	[pred="rarement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	rarement_____1	Default		%default
rarissimement	100	advm	[pred="rarissimement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	rarissimement_____1	Default		%default
ras	100	adv	[pred="ras_____1",cat=adv]	ras_____1	Default		%default
rasibus	100	adv	[pred="rasibus_____1",cat=adv]	rasibus_____1	Default		%default
rationnellement	100	advm	[pred="rationnellement_____1",clivee=+,cat=adv]	rationnellement_____1	Default		%default
reconventionnellement	100	advm	[pred="reconventionnellement_____1",clivee=+,cat=adv]	reconventionnellement_____1	Default		%default
recta	100	adv	[pred="recta_____1",cat=adv]	recta_____1	Default		%default
rectangulairement	100	advm	[pred="rectangulairement_____1",clivee=+,cat=adv]	rectangulairement_____1	Default		%default
rectilignement	100	advm	[pred="rectilignement_____1",clivee=+,cat=adv]	rectilignement_____1	Default		%default
redoutablement	100	advm	[pred="redoutablement_____1",advi=combien,clivee=+,cat=adv]	redoutablement_____1	Default		%default
regrettablement	100	advp	[pred="regrettablement_____1",detach = +,detach_neg = +,cat=adv]	regrettablement_____1	Default		%default
relativement	100	advm	[pred="relativement_____1",advi=combien,cat=adv]	relativement_____1	Default		%default
religieusement	100	advm	[pred="religieusement_____1",cat=adv]	religieusement_____1	Default		%default
remarquablement	100	advm	[pred="remarquablement_____1",advi=combien,clivee=+,cat=adv]	remarquablement_____1	Default		%default
remarque	100	adv	[pred="remarque_____1",cat=adv]	remarque_____1	Default		%default
rentablement	100	advm	[pred="rentablement_____1",clivee=+,cat=adv]	rentablement_____1	Default		%default
reproductivement	100	advm	[pred="reproductivement_____1",clivee=+,cat=adv]	reproductivement_____1	Default		%default
représentativement	100	advm	[pred="représentativement_____1",clivee=+,cat=adv]	représentativement_____1	Default		%default
respectablement	100	advm	[pred="respectablement_____1",clivee=+,detach_neg = +,cat=adv]	respectablement_____1	Default		%default
respectivement	100	advm	[pred="respectivement_____1",modnc = +,cat=adv]	respectivement_____1	Default		%default
respectueusement	100	advm	[pred="respectueusement_____1",clivee=+,detach_neg = +,cat=adv]	respectueusement_____1	Default		%default
restrictivement	100	advm	[pred="restrictivement_____1",clivee=+,cat=adv]	restrictivement_____1	Default		%default
revêchement	100	advm	[pred="revêchement_____1",clivee=+,detach_neg = +,cat=adv]	revêchement_____1	Default		%default
ric-rac	100	adv	[pred="ric-rac_____1",cat=adv]	ric-rac_____1	Default		%default
richement	100	advm	[pred="richement_____1",clivee=+,cat=adv]	richement_____1	Default		%default
richissimement	100	advm	[pred="richissimement_____1",clivee=+,cat=adv]	richissimement_____1	Default		%default
ridiculement	100	advm	[pred="ridiculement_____1",advi=combien,clivee=+,cat=adv]	ridiculement_____1	Default		%default
ridiculement	100	advm	[pred="ridiculement_____2",clivee=+,detach_neg = +,cat=adv]	ridiculement_____2	Default		%default
rieusement	100	advm	[pred="rieusement_____1",clivee=+,detach_neg = +,cat=adv]	rieusement_____1	Default		%default
rigidement	100	advm	[pred="rigidement_____1",clivee=+,cat=adv]	rigidement_____1	Default		%default
rigoureusement	100	advm	[pred="rigoureusement_____1",advi=combien,cat=adv]	rigoureusement_____1	Default		%default
rigoureusement	100	advm	[pred="rigoureusement_____2",clivee=+,detach_neg = +,cat=adv]	rigoureusement_____2	Default		%default
rinforzando	100	adv	[pred="rinforzando_____1",cat=adv]	rinforzando_____1	Default		%default
ringardement	100	advm	[pred="ringardement_____1",clivee=+,detach_neg = +,cat=adv]	ringardement_____1	Default		%default
risiblement	100	advm	[pred="risiblement_____1",cat=adv]	risiblement_____1	Default		%default
ritardando	100	adv	[pred="ritardando_____1",cat=adv]	ritardando_____1	Default		%default
rituellement	100	advm	[pred="rituellement_____1",cat=adv]	rituellement_____1	Default		%default
rituellement	100	advp	[pred="rituellement_____2",detach = +,detach_neg = +,modnc = +,cat=adv]	rituellement_____2	Default		%default
robustement	100	advm	[pred="robustement_____1",clivee=+,detach_neg = +,cat=adv]	robustement_____1	Default		%default
rocailleusement	100	advm	[pred="rocailleusement_____1",clivee=+,cat=adv]	rocailleusement_____1	Default		%default
rocambolesquement	100	advm	[pred="rocambolesquement_____1",clivee=+,cat=adv]	rocambolesquement_____1	Default		%default
rogatoirement	100	advm	[pred="rogatoirement_____1",cat=adv]	rogatoirement_____1	Default		%default
roguement	100	advm	[pred="roguement_____1",clivee=+,detach_neg = +,cat=adv]	roguement_____1	Default		%default
roidement	100	advm	[pred="roidement_____1",clivee=+,detach_neg = +,cat=adv]	roidement_____1	Default		%default
romainement	100	advm	[pred="romainement_____1",clivee=+,detach_neg = +,cat=adv]	romainement_____1	Default		%default
romancièrement	100	advm	[pred="romancièrement_____1",clivee=+,cat=adv]	romancièrement_____1	Default		%default
romanesquement	100	advm	[pred="romanesquement_____1",clivee=+,detach_neg = +,cat=adv]	romanesquement_____1	Default		%default
romantiquement	100	advm	[pred="romantiquement_____1",clivee=+,cat=adv]	romantiquement_____1	Default		%default
rond	100	adv	[pred="rond_____1",cat=adv]	rond_____1	Default		%default
rondement	100	advm	[pred="rondement_____1",clivee=+,cat=adv]	rondement_____1	Default		%default
rondouillardement	100	advm	[pred="rondouillardement_____1",clivee=+,cat=adv]	rondouillardement_____1	Default		%default
rosement	100	advm	[pred="rosement_____1",clivee=+,cat=adv]	rosement_____1	Default		%default
rotativement	100	advm	[pred="rotativement_____1",clivee=+,cat=adv]	rotativement_____1	Default		%default
roturièrement	100	advm	[pred="roturièrement_____1",clivee=+,detach_neg = +,cat=adv]	roturièrement_____1	Default		%default
roublardement	100	advm	[pred="roublardement_____1",clivee=+,detach_neg = +,cat=adv]	roublardement_____1	Default		%default
rouge	100	adv	[pred="rouge_____1",cat=adv]	rouge_____1	Default		%default
rougement	100	advm	[pred="rougement_____1",clivee=+,cat=adv]	rougement_____1	Default		%default
routinièrement	100	advm	[pred="routinièrement_____1",clivee=+,cat=adv]	routinièrement_____1	Default		%default
royalement	100	advm	[pred="royalement_____1",clivee=+,cat=adv]	royalement_____1	Default		%default
rubato	100	adv	[pred="rubato_____1",cat=adv]	rubato_____1	Default		%default
rudement	100	advm	[pred="rudement_____1",advi=combien,cat=adv]	rudement_____1	Default		%default
rudement	100	advm	[pred="rudement_____2",clivee=+,detach_neg = +,cat=adv]	rudement_____2	Default		%default
rudimentairement	100	advm	[pred="rudimentairement_____1",clivee=+,cat=adv]	rudimentairement_____1	Default		%default
ruineusement	100	advm	[pred="ruineusement_____1",clivee=+,cat=adv]	ruineusement_____1	Default		%default
ruralement	100	advm	[pred="ruralement_____1",clivee=+,cat=adv]	ruralement_____1	Default		%default
rustaudement	100	advm	[pred="rustaudement_____1",clivee=+,detach_neg = +,cat=adv]	rustaudement_____1	Default		%default
rustiquement	100	advm	[pred="rustiquement_____1",clivee=+,cat=adv]	rustiquement_____1	Default		%default
rustrement	100	advm	[pred="rustrement_____1",clivee=+,detach_neg = +,cat=adv]	rustrement_____1	Default		%default
rythmiquement	100	advm	[pred="rythmiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	rythmiquement_____1	Default		%default
rythmiquement	100	advm	[pred="rythmiquement_____2",clivee=+,cat=adv]	rythmiquement_____2	Default		%default
règlementairement	100	advm	[pred="règlementairement_____1",clivee=+,cat=adv]	règlementairement_____1	Default		%default
réactionnairement	100	advm	[pred="réactionnairement_____1",clivee=+,detach_neg = +,cat=adv]	réactionnairement_____1	Default		%default
réalistement	100	advm	[pred="réalistement_____1",clivee=+,detach_neg = +,cat=adv]	réalistement_____1	Default		%default
rébarbativement	100	advm	[pred="rébarbativement_____1",clivee=+,detach_neg = +,cat=adv]	rébarbativement_____1	Default		%default
récemment	100	advm	[pred="récemment_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	récemment_____1	Default		%default
réciproquement	100	advm	[pred="réciproquement_____1",clivee=+,cat=adv]	réciproquement_____1	Default		%default
récursivement	100	advm	[pred="récursivement_____1",clivee=+,cat=adv]	récursivement_____1	Default		%default
réellement	100	advm	[pred="réellement_____1",advi=combien,cat=adv]	réellement_____1	Default		%default
réellement	100	advm	[pred="réellement_____2",clivee=+,cat=adv]	réellement_____2	Default		%default
réellement	100	advp	[pred="réellement_____3",detach = +,detach_neg = +,cat=adv]	réellement_____3	Default		%default
réflexion faite	100	adv	[pred="réflexion faite_____1",cat=adv]	réflexion faite_____1	Default		%default
réflexivement	100	advm	[pred="réflexivement_____1",clivee=+,cat=adv]	réflexivement_____1	Default		%default
régionalement	100	advm	[pred="régionalement_____1",clivee=+,cat=adv]	régionalement_____1	Default		%default
réglo	100	adv	[pred="réglo_____1",cat=adv]	réglo_____1	Default		%default
régressivement	100	advm	[pred="régressivement_____1",clivee=+,cat=adv]	régressivement_____1	Default		%default
régulièrement	100	advm	[pred="régulièrement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	régulièrement_____1	Default		%default
répressivement	100	advm	[pred="répressivement_____1",clivee=+,detach_neg = +,cat=adv]	répressivement_____1	Default		%default
répréhensiblement	100	advm	[pred="répréhensiblement_____1",clivee=+,cat=adv]	répréhensiblement_____1	Default		%default
répulsivement	100	advm	[pred="répulsivement_____1",clivee=+,cat=adv]	répulsivement_____1	Default		%default
répétitivement	100	advm	[pred="répétitivement_____1",clivee=+,cat=adv]	répétitivement_____1	Default		%default
résidentiellement	100	advm	[pred="résidentiellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	résidentiellement_____1	Default		%default
résiduellement	100	advm	[pred="résiduellement_____1",clivee=+,cat=adv]	résiduellement_____1	Default		%default
résineusement	100	advm	[pred="résineusement_____1",clivee=+,cat=adv]	résineusement_____1	Default		%default
résolument	100	advm	[pred="résolument_____1",advi=combien,clivee=+,cat=adv]	résolument_____1	Default		%default
résolument	100	advm	[pred="résolument_____2",clivee=+,detach_neg = +,cat=adv]	résolument_____2	Default		%default
résultat	100	adv	[pred="résultat_____1",cat=adv]	résultat_____1	Default		%default
rétroactivement	100	advm	[pred="rétroactivement_____1",clivee=+,cat=adv]	rétroactivement_____1	Default		%default
rétrospectivement	100	advm	[pred="rétrospectivement_____1",clivee=+,cat=adv]	rétrospectivement_____1	Default		%default
réversiblement	100	advm	[pred="réversiblement_____1",clivee=+,cat=adv]	réversiblement_____1	Default		%default
révocablement	100	advm	[pred="révocablement_____1",clivee=+,cat=adv]	révocablement_____1	Default		%default
révolutionnairement	100	advm	[pred="révolutionnairement_____1",clivee=+,cat=adv]	révolutionnairement_____1	Default		%default
révéremment	100	advm	[pred="révéremment_____1",clivee=+,detach_neg = +,cat=adv]	révéremment_____1	Default		%default
révérencieusement	100	advm	[pred="révérencieusement_____1",clivee=+,detach_neg = +,cat=adv]	révérencieusement_____1	Default		%default
rêveusement	100	advm	[pred="rêveusement_____1",clivee=+,detach_neg = +,cat=adv]	rêveusement_____1	Default		%default
s'il te plaît	100	adv	[pred="s'il te plaît_____1",cat=adv]	s'il te plaît_____1	Default		%default
s'il vous plaît	100	adv	[pred="s'il vous plaît_____1",cat=adv]	s'il vous plaît_____1	Default		%default
sacerdotalement	100	advm	[pred="sacerdotalement_____1",clivee=+,cat=adv]	sacerdotalement_____1	Default		%default
sacramentellement	100	advm	[pred="sacramentellement_____1",clivee=+,cat=adv]	sacramentellement_____1	Default		%default
sacrilègement	100	advm	[pred="sacrilègement_____1",clivee=+,detach_neg = +,cat=adv]	sacrilègement_____1	Default		%default
sacrément	100	advm	[pred="sacrément_____1",advi=combien,cat=adv]	sacrément_____1	Default		%default
sadiquement	100	advm	[pred="sadiquement_____1",clivee=+,detach_neg = +,cat=adv]	sadiquement_____1	Default		%default
sadiquement	100	advp	[pred="sadiquement_____2",detach = +,detach_neg = +,cat=adv]	sadiquement_____2	Default		%default
sagacement	100	advm	[pred="sagacement_____1",clivee=+,detach_neg = +,cat=adv]	sagacement_____1	Default		%default
sagement	100	advm	[pred="sagement_____1",clivee=+,detach_neg = +,cat=adv]	sagement_____1	Default		%default
sagement	100	advp	[pred="sagement_____2",detach = +,detach_neg = +,cat=adv]	sagement_____2	Default		%default
sagittalement	100	advm	[pred="sagittalement_____1",clivee=+,cat=adv]	sagittalement_____1	Default		%default
sainement	100	advm	[pred="sainement_____1",clivee=+,cat=adv]	sainement_____1	Default		%default
saintement	100	advm	[pred="saintement_____1",clivee=+,detach_neg = +,cat=adv]	saintement_____1	Default		%default
saisonnièrement	100	advm	[pred="saisonnièrement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	saisonnièrement_____1	Default		%default
salacement	100	advm	[pred="salacement_____1",clivee=+,detach_neg = +,cat=adv]	salacement_____1	Default		%default
salarialement	100	advm	[pred="salarialement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	salarialement_____1	Default		%default
salaudement	100	advm	[pred="salaudement_____1",clivee=+,detach_neg = +,cat=adv]	salaudement_____1	Default		%default
salement	100	advm	[pred="salement_____1",clivee=+,detach_neg = +,cat=adv]	salement_____1	Default		%default
salubrement	100	advm	[pred="salubrement_____1",clivee=+,cat=adv]	salubrement_____1	Default		%default
salutairement	100	advm	[pred="salutairement_____1",clivee=+,cat=adv]	salutairement_____1	Default		%default
samedi	100	adv	[pred="samedi_____1",cat=adv]	samedi_____1	Default		%default
sanguinairement	100	advm	[pred="sanguinairement_____1",clivee=+,detach_neg = +,cat=adv]	sanguinairement_____1	Default		%default
sanitairement	100	advm	[pred="sanitairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sanitairement_____1	Default		%default
sanitairement	100	advm	[pred="sanitairement_____2",clivee=+,cat=adv]	sanitairement_____2	Default		%default
sans cesse	100	adv	[pred="sans cesse_____1",cat=adv]	sans cesse_____1	Default		%default
sans conteste	100	adv	[pred="sans conteste_____1",cat=adv]	sans conteste_____1	Default		%default
sans coup férir	100	adv	[pred="sans coup férir_____1",cat=adv]	sans coup férir_____1	Default		%default
sans coup férir	100	adv	[pred="sans coup férir_____2",chunk_type=GP,cat=adv]	sans coup férir_____2	Default		%default
sans doute	100	adv	[pred="sans doute_____1",cat=adv]	sans doute_____1	Default		%default
sans tambour ni trompette	100	adv	[pred="sans tambour ni trompette_____1",cat=adv]	sans tambour ni trompette_____1	Default		%default
saphiquement	100	advm	[pred="saphiquement_____1",clivee=+,cat=adv]	saphiquement_____1	Default		%default
sapristi	100	adv	[pred="sapristi_____1",cat=adv]	sapristi_____1	Default		%default
sarcastiquement	100	advm	[pred="sarcastiquement_____1",clivee=+,detach_neg = +,cat=adv]	sarcastiquement_____1	Default		%default
sardoniquement	100	advm	[pred="sardoniquement_____1",clivee=+,cat=adv]	sardoniquement_____1	Default		%default
sataniquement	100	advm	[pred="sataniquement_____1",clivee=+,detach_neg = +,cat=adv]	sataniquement_____1	Default		%default
satiriquement	100	advm	[pred="satiriquement_____1",clivee=+,cat=adv]	satiriquement_____1	Default		%default
satisfaisamment	100	advm	[pred="satisfaisamment_____1",advi=combien,clivee=+,cat=adv]	satisfaisamment_____1	Default		%default
saumâtrement	100	advm	[pred="saumâtrement_____1",clivee=+,cat=adv]	saumâtrement_____1	Default		%default
sauvagement	100	advm	[pred="sauvagement_____1",clivee=+,detach_neg = +,cat=adv]	sauvagement_____1	Default		%default
savamment	100	advm	[pred="savamment_____1",clivee=+,detach_neg = +,cat=adv]	savamment_____1	Default		%default
savoureusement	100	advm	[pred="savoureusement_____1",clivee=+,cat=adv]	savoureusement_____1	Default		%default
scabreusement	100	advm	[pred="scabreusement_____1",clivee=+,detach_neg = +,cat=adv]	scabreusement_____1	Default		%default
scalairement	100	advm	[pred="scalairement_____1",clivee=+,cat=adv]	scalairement_____1	Default		%default
scandaleusement	100	advm	[pred="scandaleusement_____1",advi=combien,clivee=+,cat=adv]	scandaleusement_____1	Default		%default
scatologiquement	100	advm	[pred="scatologiquement_____1",clivee=+,cat=adv]	scatologiquement_____1	Default		%default
sceptiquement	100	advm	[pred="sceptiquement_____1",clivee=+,detach_neg = +,cat=adv]	sceptiquement_____1	Default		%default
schuss	100	adv	[pred="schuss_____1",cat=adv]	schuss_____1	Default		%default
schématiquement	100	advm	[pred="schématiquement_____1",clivee=+,detach_neg = +,cat=adv]	schématiquement_____1	Default		%default
sciemment	100	advm	[pred="sciemment_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	sciemment_____1	Default		%default
scientifiquement	100	advm	[pred="scientifiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	scientifiquement_____1	Default		%default
scientifiquement	100	advm	[pred="scientifiquement_____2",clivee=+,cat=adv]	scientifiquement_____2	Default		%default
scolairement	100	advm	[pred="scolairement_____1",clivee=+,detach_neg = +,cat=adv]	scolairement_____1	Default		%default
scolastiquement	100	advm	[pred="scolastiquement_____1",clivee=+,cat=adv]	scolastiquement_____1	Default		%default
scrupuleusement	100	advm	[pred="scrupuleusement_____1",clivee=+,detach_neg = +,cat=adv]	scrupuleusement_____1	Default		%default
sculpturalement	100	advm	[pred="sculpturalement_____1",clivee=+,cat=adv]	sculpturalement_____1	Default		%default
scélératement	100	advm	[pred="scélératement_____1",clivee=+,detach_neg = +,cat=adv]	scélératement_____1	Default		%default
scéniquement	100	advm	[pred="scéniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	scéniquement_____1	Default		%default
sec	100	adv	[pred="sec_____1",cat=adv]	sec_____1	Default		%default
secondairement	100	advm	[pred="secondairement_____1",clivee=+,cat=adv]	secondairement_____1	Default		%default
secondement	100	advp	[pred="secondement_____1",detach = +,detach_neg = +,cat=adv]	secondement_____1	Default		%default
secourablement	100	advm	[pred="secourablement_____1",clivee=+,cat=adv]	secourablement_____1	Default		%default
secrètement	100	advm	[pred="secrètement_____1",clivee=+,cat=adv]	secrètement_____1	Default		%default
sectairement	100	advm	[pred="sectairement_____1",clivee=+,detach_neg = +,cat=adv]	sectairement_____1	Default		%default
sectoriellement	100	advm	[pred="sectoriellement_____1",clivee=+,cat=adv]	sectoriellement_____1	Default		%default
secundo	100	adv	[pred="secundo_____1",cat=adv]	secundo_____1	Default		%default
seigneurialement	100	advm	[pred="seigneurialement_____1",clivee=+,cat=adv]	seigneurialement_____1	Default		%default
seizièmement	100	advp	[pred="seizièmement_____1",detach = +,detach_neg = +,cat=adv]	seizièmement_____1	Default		%default
semblablement	100	advm	[pred="semblablement_____1",clivee=+,cat=adv]	semblablement_____1	Default		%default
semblablement	100	advp	[pred="semblablement_____2<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	semblablement_____2	Default		%default
semestriellement	100	advm	[pred="semestriellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	semestriellement_____1	Default		%default
sempiternellement	100	advm	[pred="sempiternellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	sempiternellement_____1	Default		%default
senestrorsum	100	adv	[pred="senestrorsum_____1",cat=adv]	senestrorsum_____1	Default		%default
sensationnellement	100	advm	[pred="sensationnellement_____1",advi=combien,clivee=+,cat=adv]	sensationnellement_____1	Default		%default
sensiblement	100	advm	[pred="sensiblement_____1",advi=combien,cat=adv]	sensiblement_____1	Default		%default
sensiblement	100	advm	[pred="sensiblement_____2",clivee=+,cat=adv]	sensiblement_____2	Default		%default
sensuellement	100	advm	[pred="sensuellement_____1",clivee=+,detach_neg = +,cat=adv]	sensuellement_____1	Default		%default
sensément	100	advm	[pred="sensément_____1",clivee=+,detach_neg = +,cat=adv]	sensément_____1	Default		%default
sentencieusement	100	advm	[pred="sentencieusement_____1",clivee=+,detach_neg = +,cat=adv]	sentencieusement_____1	Default		%default
sentimentalement	100	advm	[pred="sentimentalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sentimentalement_____1	Default		%default
sentimentalement	100	advm	[pred="sentimentalement_____2",clivee=+,detach_neg = +,cat=adv]	sentimentalement_____2	Default		%default
septimo	100	adv	[pred="septimo_____1",cat=adv]	septimo_____1	Default		%default
septièmement	100	advp	[pred="septièmement_____1",detach = +,detach_neg = +,cat=adv]	septièmement_____1	Default		%default
sereinement	100	advm	[pred="sereinement_____1",clivee=+,detach_neg = +,cat=adv]	sereinement_____1	Default		%default
serré	100	adv	[pred="serré_____1",cat=adv]	serré_____1	Default		%default
serviablement	100	advm	[pred="serviablement_____1",clivee=+,detach_neg = +,cat=adv]	serviablement_____1	Default		%default
servilement	100	advm	[pred="servilement_____1",clivee=+,detach_neg = +,cat=adv]	servilement_____1	Default		%default
seul	100	adv	[pred="seul_____1",cat=adv]	seul_____1	Default		%default
seul à seul	100	adv	[pred="seul à seul_____1",cat=adv]	seul à seul_____1	Default		%default
seulement	100	advm	[pred="seulement_____1",detach_neg = +,modnc = +,cat=adv]	seulement_____1	Default		%default
seulement	100	advp	[pred="seulement_____2",detach = +,detach_neg = +,cat=adv]	seulement_____2	Default		%default
sexto	100	adv	[pred="sexto_____1",cat=adv]	sexto_____1	Default		%default
sextuplement	100	advm	[pred="sextuplement_____1",advi=combien,cat=adv]	sextuplement_____1	Default		%default
sexuellement	100	advm	[pred="sexuellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sexuellement_____1	Default		%default
sexuellement	100	advm	[pred="sexuellement_____2",cat=adv]	sexuellement_____2	Default		%default
sforzando	100	adv	[pred="sforzando_____1",cat=adv]	sforzando_____1	Default		%default
si besoin	100	adv	[pred="si besoin_____1",cat=adv]	si besoin_____1	Default		%default
si besoin est	100	adv	[pred="si besoin est_____1",cat=adv]	si besoin est_____1	Default		%default
si fait	100	adv	[pred="si fait_____1",cat=adv]	si fait_____1	Default		%default
si nécessaire	100	adv	[pred="si nécessaire_____1",cat=adv]	si nécessaire_____1	Default		%default
si peu	100	adv	[pred="si peu_____1",adv_kind=intens,cat=adv]	si peu_____1	Default		%default
sibyllinement	100	advm	[pred="sibyllinement_____1",clivee=+,detach_neg = +,cat=adv]	sibyllinement_____1	Default		%default
sic	100	adv	[pred="sic_____1",cat=adv]	sic_____1	Default		%default
sidéralement	100	advm	[pred="sidéralement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sidéralement_____1	Default		%default
significativement	100	advm	[pred="significativement_____1",advi=combien,clivee=+,cat=adv]	significativement_____1	Default		%default
silencieusement	100	advm	[pred="silencieusement_____1",clivee=+,detach_neg = +,cat=adv]	silencieusement_____1	Default		%default
similairement	100	advm	[pred="similairement_____1",clivee=+,cat=adv]	similairement_____1	Default		%default
simplement	100	advm	[pred="simplement_____1",detach_neg = +,modnc = +,cat=adv]	simplement_____1	Default		%default
simplement	100	advm	[pred="simplement_____2",clivee=+,detach_neg = +,cat=adv]	simplement_____2	Default		%default
simplement	100	advp	[pred="simplement_____3",detach = +,detach_neg = +,cat=adv]	simplement_____3	Default		%default
simplement	100	advp	[pred="simplement_____4",detach = +,detach_neg = +,cat=adv]	simplement_____4	Default		%default
simultanément	100	advm	[pred="simultanément_____1",clivee=+,cat=adv]	simultanément_____1	Default		%default
sincèrement	100	advm	[pred="sincèrement_____1",clivee=+,detach_neg = +,cat=adv]	sincèrement_____1	Default		%default
sincèrement	100	advp	[pred="sincèrement_____2",detach = +,detach_neg = +,cat=adv]	sincèrement_____2	Default		%default
sine die	100	adv	[pred="sine die_____1",cat=adv]	sine die_____1	Default		%default
sine qua non	100	adv	[pred="sine qua non_____1",cat=adv]	sine qua non_____1	Default		%default
singulièrement	100	advm	[pred="singulièrement_____1",advi=combien,cat=adv]	singulièrement_____1	Default		%default
singulièrement	100	advm	[pred="singulièrement_____2",clivee=+,detach_neg = +,cat=adv]	singulièrement_____2	Default		%default
sinistrement	100	advm	[pred="sinistrement_____1",clivee=+,detach_neg = +,cat=adv]	sinistrement_____1	Default		%default
sinon	100	adv	[pred="sinon_____1",cat=adv]	sinon_____1	Default		%default
sinueusement	100	advm	[pred="sinueusement_____1",clivee=+,cat=adv]	sinueusement_____1	Default		%default
siouxement	100	advm	[pred="siouxement_____1",clivee=+,detach_neg = +,cat=adv]	siouxement_____1	Default		%default
sitôt	100	adv	[pred="sitôt_____1",cat=adv]	sitôt_____1	Default		%default
sixièmement	100	advp	[pred="sixièmement_____1",detach = +,detach_neg = +,cat=adv]	sixièmement_____1	Default		%default
smorzando	100	adv	[pred="smorzando_____1",cat=adv]	smorzando_____1	Default		%default
sobrement	100	advm	[pred="sobrement_____1",clivee=+,detach_neg = +,cat=adv]	sobrement_____1	Default		%default
sociablement	100	advm	[pred="sociablement_____1",clivee=+,detach_neg = +,cat=adv]	sociablement_____1	Default		%default
socialement	100	advm	[pred="socialement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	socialement_____1	Default		%default
sociolinguistiquement	100	advm	[pred="sociolinguistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sociolinguistiquement_____1	Default		%default
sociolinguistiquement	100	advm	[pred="sociolinguistiquement_____2",clivee=+,cat=adv]	sociolinguistiquement_____2	Default		%default
sociologiquement	100	advm	[pred="sociologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sociologiquement_____1	Default		%default
sociologiquement	100	advm	[pred="sociologiquement_____2",clivee=+,cat=adv]	sociologiquement_____2	Default		%default
sociétairement	100	advm	[pred="sociétairement_____1",clivee=+,cat=adv]	sociétairement_____1	Default		%default
socratiquement	100	advm	[pred="socratiquement_____1",clivee=+,cat=adv]	socratiquement_____1	Default		%default
soi-disant	100	adv	[pred="soi-disant_____1",cat=adv]	soi-disant_____1	Default		%default
soigneusement	100	advm	[pred="soigneusement_____1",clivee=+,detach_neg = +,cat=adv]	soigneusement_____1	Default		%default
soir	100	adv	[pred="soir_____1",cat=adv]	soir_____1	Default		%default
soit dit en passant	100	adv	[pred="soit dit en passant_____1",cat=adv]	soit dit en passant_____1	Default		%default
soixantièmement	100	advp	[pred="soixantièmement_____1",detach = +,detach_neg = +,cat=adv]	soixantièmement_____1	Default		%default
solairement	100	advm	[pred="solairement_____1",clivee=+,cat=adv]	solairement_____1	Default		%default
soldatesquement	100	advm	[pred="soldatesquement_____1",clivee=+,cat=adv]	soldatesquement_____1	Default		%default
solennellement	100	advm	[pred="solennellement_____1",clivee=+,cat=adv]	solennellement_____1	Default		%default
solidairement	100	advm	[pred="solidairement_____1",clivee=+,cat=adv]	solidairement_____1	Default		%default
solidement	100	advm	[pred="solidement_____1",clivee=+,cat=adv]	solidement_____1	Default		%default
solitairement	100	advm	[pred="solitairement_____1",clivee=+,detach_neg = +,cat=adv]	solitairement_____1	Default		%default
somatiquement	100	advm	[pred="somatiquement_____1",clivee=+,cat=adv]	somatiquement_____1	Default		%default
sombrement	100	advm	[pred="sombrement_____1",clivee=+,detach_neg = +,cat=adv]	sombrement_____1	Default		%default
sommairement	100	advm	[pred="sommairement_____1",clivee=+,cat=adv]	sommairement_____1	Default		%default
somme toute	100	adv	[pred="somme toute_____1",cat=adv]	somme toute_____1	Default		%default
somnambuliquement	100	advm	[pred="somnambuliquement_____1",clivee=+,cat=adv]	somnambuliquement_____1	Default		%default
somptueusement	100	advm	[pred="somptueusement_____1",clivee=+,cat=adv]	somptueusement_____1	Default		%default
songeusement	100	advm	[pred="songeusement_____1",clivee=+,detach_neg = +,cat=adv]	songeusement_____1	Default		%default
sonorement	100	advm	[pred="sonorement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sonorement_____1	Default		%default
sonorement	100	advm	[pred="sonorement_____2",clivee=+,cat=adv]	sonorement_____2	Default		%default
sophistiquement	100	advm	[pred="sophistiquement_____1",clivee=+,cat=adv]	sophistiquement_____1	Default		%default
sorboniquement	100	advm	[pred="sorboniquement_____1",clivee=+,cat=adv]	sorboniquement_____1	Default		%default
sordidement	100	advm	[pred="sordidement_____1",clivee=+,cat=adv]	sordidement_____1	Default		%default
sororalement	100	advm	[pred="sororalement_____1",clivee=+,cat=adv]	sororalement_____1	Default		%default
sostenuto	100	adv	[pred="sostenuto_____1",cat=adv]	sostenuto_____1	Default		%default
sottement	100	advm	[pred="sottement_____1",clivee=+,detach_neg = +,cat=adv]	sottement_____1	Default		%default
sottement	100	advp	[pred="sottement_____2",detach = +,detach_neg = +,cat=adv]	sottement_____2	Default		%default
soucieusement	100	advm	[pred="soucieusement_____1",clivee=+,detach_neg = +,cat=adv]	soucieusement_____1	Default		%default
soudain	100	adv	[pred="soudain_____1",cat=adv]	soudain_____1	Default		%default
soudainement	100	advm	[pred="soudainement_____1",clivee=+,cat=adv]	soudainement_____1	Default		%default
souplement	100	advm	[pred="souplement_____1",clivee=+,detach_neg = +,cat=adv]	souplement_____1	Default		%default
soupçonneusement	100	advm	[pred="soupçonneusement_____1",clivee=+,detach_neg = +,cat=adv]	soupçonneusement_____1	Default		%default
sourcilleusement	100	advm	[pred="sourcilleusement_____1",clivee=+,detach_neg = +,cat=adv]	sourcilleusement_____1	Default		%default
sourdement	100	advm	[pred="sourdement_____1",clivee=+,cat=adv]	sourdement_____1	Default		%default
sournoisement	100	advm	[pred="sournoisement_____1",clivee=+,detach_neg = +,cat=adv]	sournoisement_____1	Default		%default
sous la main	100	adv	[pred="sous la main_____1",chunk_type=GP,cat=adv]	sous la main_____1	Default		%default
souterrainement	100	advm	[pred="souterrainement_____1",clivee=+,cat=adv]	souterrainement_____1	Default		%default
souvent	100	adv	[pred="souvent_____1",cat=adv]	souvent_____1	Default		%default
souverainement	100	advm	[pred="souverainement_____1",advi=combien,clivee=+,cat=adv]	souverainement_____1	Default		%default
souverainement	100	advm	[pred="souverainement_____2",clivee=+,detach_neg = +,cat=adv]	souverainement_____2	Default		%default
soyeusement	100	advm	[pred="soyeusement_____1",clivee=+,cat=adv]	soyeusement_____1	Default		%default
spacieusement	100	advm	[pred="spacieusement_____1",clivee=+,cat=adv]	spacieusement_____1	Default		%default
spartiatement	100	advm	[pred="spartiatement_____1",clivee=+,detach_neg = +,cat=adv]	spartiatement_____1	Default		%default
spasmodiquement	100	advm	[pred="spasmodiquement_____1",clivee=+,cat=adv]	spasmodiquement_____1	Default		%default
spatialement	100	advm	[pred="spatialement_____1",clivee=+,cat=adv]	spatialement_____1	Default		%default
spectaculairement	100	advm	[pred="spectaculairement_____1",advi=combien,clivee=+,cat=adv]	spectaculairement_____1	Default		%default
spectralement	100	advm	[pred="spectralement_____1",clivee=+,cat=adv]	spectralement_____1	Default		%default
sphériquement	100	advm	[pred="sphériquement_____1",clivee=+,cat=adv]	sphériquement_____1	Default		%default
spirituellement	100	advm	[pred="spirituellement_____1",clivee=+,detach_neg = +,cat=adv]	spirituellement_____1	Default		%default
spirituellement	100	advm	[pred="spirituellement_____2",clivee=+,cat=adv]	spirituellement_____2	Default		%default
splendidement	100	advm	[pred="splendidement_____1",advi=combien,clivee=+,cat=adv]	splendidement_____1	Default		%default
spontanément	100	advm	[pred="spontanément_____1",clivee=+,detach_neg = +,cat=adv]	spontanément_____1	Default		%default
sporadiquement	100	advm	[pred="sporadiquement_____1",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	sporadiquement_____1	Default		%default
sportivement	100	advm	[pred="sportivement_____1",clivee=+,detach_neg = +,cat=adv]	sportivement_____1	Default		%default
spécialement	100	advm	[pred="spécialement_____1",modnc = +,cat=adv]	spécialement_____1	Default		%default
spécialement	100	advm	[pred="spécialement_____2",advi=combien,cat=adv]	spécialement_____2	Default		%default
spécieusement	100	advm	[pred="spécieusement_____1",clivee=+,cat=adv]	spécieusement_____1	Default		%default
spécifiquement	100	advm	[pred="spécifiquement_____1",advi=combien,cat=adv]	spécifiquement_____1	Default		%default
spécifiquement	100	advm	[pred="spécifiquement_____2",clivee=+,cat=adv]	spécifiquement_____2	Default		%default
spéculairement	100	advm	[pred="spéculairement_____1",clivee=+,cat=adv]	spéculairement_____1	Default		%default
spéculativement	100	advm	[pred="spéculativement_____1",clivee=+,cat=adv]	spéculativement_____1	Default		%default
staccato	100	adv	[pred="staccato_____1",cat=adv]	staccato_____1	Default		%default
staliniennement	100	advm	[pred="staliniennement_____1",clivee=+,detach_neg = +,cat=adv]	staliniennement_____1	Default		%default
stationnairement	100	advm	[pred="stationnairement_____1",clivee=+,cat=adv]	stationnairement_____1	Default		%default
statiquement	100	advm	[pred="statiquement_____1",clivee=+,cat=adv]	statiquement_____1	Default		%default
statistiquement	100	advm	[pred="statistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	statistiquement_____1	Default		%default
statistiquement	100	advm	[pred="statistiquement_____2",clivee=+,cat=adv]	statistiquement_____2	Default		%default
statutairement	100	advm	[pred="statutairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	statutairement_____1	Default		%default
statutairement	100	advm	[pred="statutairement_____2",clivee=+,cat=adv]	statutairement_____2	Default		%default
stochastiquement	100	advm	[pred="stochastiquement_____1",clivee=+,cat=adv]	stochastiquement_____1	Default		%default
stoïquement	100	advm	[pred="stoïquement_____1",clivee=+,detach_neg = +,cat=adv]	stoïquement_____1	Default		%default
stp	100	adv	[pred="s'il te plaît_____1",cat=adv]	s'il te plaît_____1	Default		%default
stratigraphiquement	100	advm	[pred="stratigraphiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	stratigraphiquement_____1	Default		%default
stratégiquement	100	advm	[pred="stratégiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	stratégiquement_____1	Default		%default
stratégiquement	100	advm	[pred="stratégiquement_____2",clivee=+,cat=adv]	stratégiquement_____2	Default		%default
strictement	100	advm	[pred="strictement_____1",advi=combien,cat=adv]	strictement_____1	Default		%default
strictement	100	advm	[pred="strictement_____2",clivee=+,detach_neg = +,cat=adv]	strictement_____2	Default		%default
stricto sensu	100	adv	[pred="stricto sensu_____1",cat=adv]	stricto sensu_____1	Default		%default
stridemment	100	advm	[pred="stridemment_____1",clivee=+,cat=adv]	stridemment_____1	Default		%default
strophiquement	100	advm	[pred="strophiquement_____1",clivee=+,cat=adv]	strophiquement_____1	Default		%default
structuralement	100	advm	[pred="structuralement_____1",clivee=+,cat=adv]	structuralement_____1	Default		%default
structurellement	100	advm	[pred="structurellement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	structurellement_____1	Default		%default
structurellement	100	advm	[pred="structurellement_____2",clivee=+,cat=adv]	structurellement_____2	Default		%default
studieusement	100	advm	[pred="studieusement_____1",clivee=+,detach_neg = +,cat=adv]	studieusement_____1	Default		%default
stupidement	100	advm	[pred="stupidement_____1",clivee=+,detach_neg = +,cat=adv]	stupidement_____1	Default		%default
stupidement	100	advp	[pred="stupidement_____2",detach = +,detach_neg = +,cat=adv]	stupidement_____2	Default		%default
stylistiquement	100	advm	[pred="stylistiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	stylistiquement_____1	Default		%default
sténographiquement	100	advm	[pred="sténographiquement_____1",clivee=+,cat=adv]	sténographiquement_____1	Default		%default
stérilement	100	advm	[pred="stérilement_____1",clivee=+,cat=adv]	stérilement_____1	Default		%default
stéréographiquement	100	advm	[pred="stéréographiquement_____1",clivee=+,cat=adv]	stéréographiquement_____1	Default		%default
stéréophoniquement	100	advm	[pred="stéréophoniquement_____1",clivee=+,cat=adv]	stéréophoniquement_____1	Default		%default
suavement	100	advm	[pred="suavement_____1",clivee=+,cat=adv]	suavement_____1	Default		%default
subalternement	100	advm	[pred="subalternement_____1",clivee=+,cat=adv]	subalternement_____1	Default		%default
subconsciemment	100	advm	[pred="subconsciemment_____1",clivee=+,cat=adv]	subconsciemment_____1	Default		%default
subitement	100	advm	[pred="subitement_____1",clivee=+,cat=adv]	subitement_____1	Default		%default
subito	100	adv	[pred="subito_____1",cat=adv]	subito_____1	Default		%default
subjectivement	100	advm	[pred="subjectivement_____1",clivee=+,cat=adv]	subjectivement_____1	Default		%default
sublimement	100	advm	[pred="sublimement_____1",advi=combien,clivee=+,cat=adv]	sublimement_____1	Default		%default
subordinément	100	advp	[pred="subordinément_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	subordinément_____1	Default		%default
subordonnément	100	advm	[pred="subordonnément_____1",clivee=+,cat=adv]	subordonnément_____1	Default		%default
subrepticement	100	advm	[pred="subrepticement_____1",clivee=+,cat=adv]	subrepticement_____1	Default		%default
subsidiairement	100	advm	[pred="subsidiairement_____1",clivee=+,cat=adv]	subsidiairement_____1	Default		%default
substantiellement	100	advm	[pred="substantiellement_____1",advi=combien,clivee=+,cat=adv]	substantiellement_____1	Default		%default
substantivement	100	advm	[pred="substantivement_____1",clivee=+,cat=adv]	substantivement_____1	Default		%default
subséquemment	100	advp	[pred="subséquemment_____1<Objà:(à-sn)>",detach = +,detach_neg = +,cat=adv]	subséquemment_____1	Default		%default
subtilement	100	advm	[pred="subtilement_____1",clivee=+,detach_neg = +,cat=adv]	subtilement_____1	Default		%default
subversivement	100	advm	[pred="subversivement_____1",clivee=+,detach_neg = +,cat=adv]	subversivement_____1	Default		%default
successivement	100	advm	[pred="successivement_____1",clivee=+,cat=adv]	successivement_____1	Default		%default
succinctement	100	advm	[pred="succinctement_____1",clivee=+,detach_neg = +,cat=adv]	succinctement_____1	Default		%default
succulemment	100	advm	[pred="succulemment_____1",clivee=+,cat=adv]	succulemment_____1	Default		%default
suffisamment	100	advm	[pred="suffisamment_____1<Obl:(pour-sn|pour-sinf|pour-scompl)>",adv_kind=intens,advi=combien,cat=adv]	suffisamment_____1	Default		%default
suggestivement	100	advm	[pred="suggestivement_____1",clivee=+,cat=adv]	suggestivement_____1	Default		%default
sui generis	100	adv	[pred="sui generis_____1",cat=adv]	sui generis_____1	Default		%default
suicidairement	100	advm	[pred="suicidairement_____1",clivee=+,detach_neg = +,cat=adv]	suicidairement_____1	Default		%default
sultanesquement	100	advm	[pred="sultanesquement_____1",clivee=+,cat=adv]	sultanesquement_____1	Default		%default
superbement	100	advm	[pred="superbement_____1",advi=combien,clivee=+,cat=adv]	superbement_____1	Default		%default
superficiellement	100	advm	[pred="superficiellement_____1",clivee=+,detach_neg = +,cat=adv]	superficiellement_____1	Default		%default
superficiellement	100	advm	[pred="superficiellement_____2",clivee=+,detach = +,detach_neg = +,cat=adv]	superficiellement_____2	Default		%default
superfinement	100	advm	[pred="superfinement_____1",clivee=+,detach_neg = +,cat=adv]	superfinement_____1	Default		%default
superfétatoirement	100	advm	[pred="superfétatoirement_____1",clivee=+,cat=adv]	superfétatoirement_____1	Default		%default
superlativement	100	advm	[pred="superlativement_____1",advi=combien,cat=adv]	superlativement_____1	Default		%default
superstitieusement	100	advm	[pred="superstitieusement_____1",clivee=+,detach_neg = +,cat=adv]	superstitieusement_____1	Default		%default
supplémentairement	100	advm	[pred="supplémentairement_____1",clivee=+,cat=adv]	supplémentairement_____1	Default		%default
supplétivement	100	advm	[pred="supplétivement_____1",clivee=+,cat=adv]	supplétivement_____1	Default		%default
supportablement	100	advm	[pred="supportablement_____1",clivee=+,cat=adv]	supportablement_____1	Default		%default
supposément	100	advp	[pred="supposément_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	supposément_____1	Default		%default
supra	100	adv	[pred="supra_____1",cat=adv]	supra_____1	Default		%default
supranationalement	100	advm	[pred="supranationalement_____1",clivee=+,cat=adv]	supranationalement_____1	Default		%default
suprêmement	100	advm	[pred="suprêmement_____1",advi=combien,cat=adv]	suprêmement_____1	Default		%default
supérieurement	100	advm	[pred="supérieurement_____1",advi=combien,cat=adv]	supérieurement_____1	Default		%default
supérieurement	100	advm	[pred="supérieurement_____2",clivee=+,cat=adv]	supérieurement_____2	Default		%default
sur ce	100	adv	[pred="sur ce_____1",cat=adv]	sur ce_____1	Default		%default
sur ces entrefaites	100	adv	[pred="sur ces entrefaites_____1",chunk_type=GP,cat=adv]	sur ces entrefaites_____1	Default		%default
sur le bout des doigts	100	adv	[pred="sur le bout des doigts_____1",cat=adv]	sur le bout des doigts_____1	Default		%default
sur les chapeaux de roue	100	adv	[pred="sur les chapeaux de roues_____1",chunk_type=GP,cat=adv]	sur les chapeaux de roues_____1	Default		%default
sur les chapeaux de roues	100	adv	[pred="sur les chapeaux de roues_____1",chunk_type=GP,cat=adv]	sur les chapeaux de roues_____1	Default		%default
sur-le-champ	100	adv	[pred="sur-le-champ_____1",cat=adv]	sur-le-champ_____1	Default		%default
surabondamment	100	advm	[pred="surabondamment_____1",advi=combien,cat=adv]	surabondamment_____1	Default		%default
surhumainement	100	advm	[pred="surhumainement_____1",advi=combien,clivee=+,cat=adv]	surhumainement_____1	Default		%default
surnaturellement	100	advm	[pred="surnaturellement_____1",clivee=+,cat=adv]	surnaturellement_____1	Default		%default
surréellement	100	advm	[pred="surréellement_____1",advi=combien,clivee=+,cat=adv]	surréellement_____1	Default		%default
surtout	100	adv	[pred="surtout_____1",cat=adv]	surtout_____1	Default		%default
surérogatoirement	100	advm	[pred="surérogatoirement_____1",clivee=+,cat=adv]	surérogatoirement_____1	Default		%default
sus	100	adv	[pred="sus_____1",cat=adv]	sus_____1	Default		%default
suspectement	100	advm	[pred="suspectement_____1",clivee=+,cat=adv]	suspectement_____1	Default		%default
suspens	100	adv	[pred="suspens_____1",cat=adv]	suspens_____1	Default		%default
suspicieusement	100	advm	[pred="suspicieusement_____1",clivee=+,detach_neg = +,cat=adv]	suspicieusement_____1	Default		%default
svp	100	adv	[pred="s'il vous plaît_____1",cat=adv]	s'il vous plaît_____1	Default		%default
svt	100	adv	[pred="souvent_____1",cat=adv]	souvent_____1	Default		%default
sybaritiquement	100	advm	[pred="sybaritiquement_____1",clivee=+,cat=adv]	sybaritiquement_____1	Default		%default
syllabiquement	100	advm	[pred="syllabiquement_____1",clivee=+,cat=adv]	syllabiquement_____1	Default		%default
syllogistiquement	100	advm	[pred="syllogistiquement_____1",clivee=+,cat=adv]	syllogistiquement_____1	Default		%default
symbiotiquement	100	advm	[pred="symbiotiquement_____1",clivee=+,cat=adv]	symbiotiquement_____1	Default		%default
symboliquement	100	advm	[pred="symboliquement_____1",clivee=+,cat=adv]	symboliquement_____1	Default		%default
sympathiquement	100	advm	[pred="sympathiquement_____1",clivee=+,detach_neg = +,cat=adv]	sympathiquement_____1	Default		%default
symphoniquement	100	advm	[pred="symphoniquement_____1",clivee=+,cat=adv]	symphoniquement_____1	Default		%default
symptomatiquement	100	advm	[pred="symptomatiquement_____1",clivee=+,cat=adv]	symptomatiquement_____1	Default		%default
symétriquement	100	advm	[pred="symétriquement_____1",clivee=+,cat=adv]	symétriquement_____1	Default		%default
synchroniquement	100	advm	[pred="synchroniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	synchroniquement_____1	Default		%default
synchroniquement	100	advm	[pred="synchroniquement_____2",clivee=+,cat=adv]	synchroniquement_____2	Default		%default
syncrétiquement	100	advm	[pred="syncrétiquement_____1",clivee=+,cat=adv]	syncrétiquement_____1	Default		%default
syndicalement	100	advm	[pred="syndicalement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	syndicalement_____1	Default		%default
syndicalement	100	advm	[pred="syndicalement_____2",clivee=+,cat=adv]	syndicalement_____2	Default		%default
synergiquement	100	advm	[pred="synergiquement_____1",clivee=+,cat=adv]	synergiquement_____1	Default		%default
synoptiquement	100	advm	[pred="synoptiquement_____1",clivee=+,cat=adv]	synoptiquement_____1	Default		%default
syntaxiquement	100	advm	[pred="syntaxiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	syntaxiquement_____1	Default		%default
syntaxiquement	100	advm	[pred="syntaxiquement_____2",clivee=+,cat=adv]	syntaxiquement_____2	Default		%default
synthétiquement	100	advm	[pred="synthétiquement_____1",clivee=+,cat=adv]	synthétiquement_____1	Default		%default
systématiquement	100	advm	[pred="systématiquement_____1",clivee=+,cat=adv]	systématiquement_____1	Default		%default
sèchement	100	advm	[pred="sèchement_____1",clivee=+,detach_neg = +,cat=adv]	sèchement_____1	Default		%default
séculairement	100	advm	[pred="séculairement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	séculairement_____1	Default		%default
séculièrement	100	advm	[pred="séculièrement_____1",clivee=+,cat=adv]	séculièrement_____1	Default		%default
sédentairement	100	advm	[pred="sédentairement_____1",clivee=+,detach_neg = +,cat=adv]	sédentairement_____1	Default		%default
séditieusement	100	advm	[pred="séditieusement_____1",clivee=+,detach_neg = +,cat=adv]	séditieusement_____1	Default		%default
sélectivement	100	advm	[pred="sélectivement_____1",clivee=+,cat=adv]	sélectivement_____1	Default		%default
sémantiquement	100	advm	[pred="sémantiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sémantiquement_____1	Default		%default
sémantiquement	100	advm	[pred="sémantiquement_____2",clivee=+,cat=adv]	sémantiquement_____2	Default		%default
sémiologiquement	100	advm	[pred="sémiologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sémiologiquement_____1	Default		%default
sémiologiquement	100	advm	[pred="sémiologiquement_____2",clivee=+,cat=adv]	sémiologiquement_____2	Default		%default
sémiotiquement	100	advm	[pred="sémiotiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	sémiotiquement_____1	Default		%default
sémiotiquement	100	advm	[pred="sémiotiquement_____2",clivee=+,cat=adv]	sémiotiquement_____2	Default		%default
sémiquement	100	advm	[pred="sémiquement_____1",clivee=+,cat=adv]	sémiquement_____1	Default		%default
sénilement	100	advm	[pred="sénilement_____1",clivee=+,detach_neg = +,cat=adv]	sénilement_____1	Default		%default
séparément	100	advm	[pred="séparément_____1",clivee=+,cat=adv]	séparément_____1	Default		%default
séquentiellement	100	advm	[pred="séquentiellement_____1",clivee=+,cat=adv]	séquentiellement_____1	Default		%default
séraphiquement	100	advm	[pred="séraphiquement_____1",clivee=+,cat=adv]	séraphiquement_____1	Default		%default
sériellement	100	advm	[pred="sériellement_____1",clivee=+,cat=adv]	sériellement_____1	Default		%default
sérieusement	100	advm	[pred="sérieusement_____1",advi=combien,clivee=+,cat=adv]	sérieusement_____1	Default		%default
sérieusement	100	advm	[pred="sérieusement_____2",clivee=+,detach_neg = +,cat=adv]	sérieusement_____2	Default		%default
sérieusement	100	advp	[pred="sérieusement_____3",detach = +,detach_neg = +,cat=adv]	sérieusement_____3	Default		%default
sévèrement	100	advm	[pred="sévèrement_____1",clivee=+,detach_neg = +,cat=adv]	sévèrement_____1	Default		%default
sûr	100	adv	[pred="sûr_____1",cat=adv]	sûr_____1	Default		%default
sûrement	100	advm	[pred="sûrement_____1",clivee=+,detach_neg = +,cat=adv]	sûrement_____1	Default		%default
sûrement	100	advm	[pred="sûrement_____2",clivee=+,cat=adv]	sûrement_____2	Default		%default
sûrement	100	advp	[pred="sûrement_____3<Suj:(scompl)>",detach = +,detach_neg = +,modnc = +,cat=adv]	sûrement_____3	Default		%default
tabulairement	100	advm	[pred="tabulairement_____1",clivee=+,cat=adv]	tabulairement_____1	Default		%default
tacitement	100	advm	[pred="tacitement_____1",clivee=+,cat=adv]	tacitement_____1	Default		%default
taciturnement	100	advm	[pred="taciturnement_____1",clivee=+,detach_neg = +,cat=adv]	taciturnement_____1	Default		%default
tactiquement	100	advm	[pred="tactiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	tactiquement_____1	Default		%default
tactiquement	100	advm	[pred="tactiquement_____2",clivee=+,cat=adv]	tactiquement_____2	Default		%default
talentueusement	100	advm	[pred="talentueusement_____1",clivee=+,detach_neg = +,cat=adv]	talentueusement_____1	Default		%default
talmudiquement	100	advm	[pred="talmudiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	talmudiquement_____1	Default		%default
tangentiellement	100	advm	[pred="tangentiellement_____1",clivee=+,cat=adv]	tangentiellement_____1	Default		%default
tangiblement	100	advm	[pred="tangiblement_____1",clivee=+,cat=adv]	tangiblement_____1	Default		%default
tant	100	adv	[pred="tant_____1",adv_kind=intens,cat=adv]	tant_____1	Default		%default
tant bien que mal	100	adv	[pred="tant bien que mal_____1",cat=adv]	tant bien que mal_____1	Default		%default
tant qu'à faire	100	advp	[pred="tant qu'à faire_____1",cat=adv]	tant qu'à faire_____1	Default		%default
tant soit peu	100	adv	[pred="tant soit peu_____1",adv_kind=intens,cat=adv]	tant soit peu_____1	Default		%default
tantôt	100	adv	[pred="tantôt_____1",cat=adv]	tantôt_____1	Default		%default
tapageusement	100	advm	[pred="tapageusement_____1",clivee=+,cat=adv]	tapageusement_____1	Default		%default
taquinement	100	advm	[pred="taquinement_____1",clivee=+,detach_neg = +,cat=adv]	taquinement_____1	Default		%default
tard	100	adv	[pred="tard_____1",cat=adv]	tard_____1	Default		%default
tardivement	100	advm	[pred="tardivement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	tardivement_____1	Default		%default
tatillonnement	100	advm	[pred="tatillonnement_____1",clivee=+,detach_neg = +,cat=adv]	tatillonnement_____1	Default		%default
taxinomiquement	100	advm	[pred="taxinomiquement_____1",clivee=+,cat=adv]	taxinomiquement_____1	Default		%default
taxonomiquement	100	advm	[pred="taxonomiquement_____1",clivee=+,cat=adv]	taxonomiquement_____1	Default		%default
techniquement	100	advm	[pred="techniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	techniquement_____1	Default		%default
techniquement	100	advm	[pred="techniquement_____2",clivee=+,cat=adv]	techniquement_____2	Default		%default
technocratiquement	100	advm	[pred="technocratiquement_____1",clivee=+,cat=adv]	technocratiquement_____1	Default		%default
technologiquement	100	advm	[pred="technologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	technologiquement_____1	Default		%default
tectoniquement	100	advm	[pred="tectoniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	tectoniquement_____1	Default		%default
teigneusement	100	advm	[pred="teigneusement_____1",clivee=+,detach_neg = +,cat=adv]	teigneusement_____1	Default		%default
tel quel	100	adv	[pred="tel quel_____1",cat=adv]	tel quel_____1	Default		%default
tellement	100	advm	[pred="tellement_____1",advi=combien,cat=adv]	tellement_____1	Default		%default
tels quels	100	adv	[pred="tels quels_____1",cat=adv]	tels quels_____1	Default		%default
temporairement	100	advm	[pred="temporairement_____1",advi=en_combien_de_temps,clivee=+,detach = +,detach_neg = +,cat=adv]	temporairement_____1	Default		%default
temporellement	100	advm	[pred="temporellement_____1",clivee=+,cat=adv]	temporellement_____1	Default		%default
tempétueusement	100	advm	[pred="tempétueusement_____1",clivee=+,cat=adv]	tempétueusement_____1	Default		%default
tenacement	100	advm	[pred="tenacement_____1",clivee=+,detach_neg = +,cat=adv]	tenacement_____1	Default		%default
tendanciellement	100	advm	[pred="tendanciellement_____1",clivee=+,cat=adv]	tendanciellement_____1	Default		%default
tendancieusement	100	advm	[pred="tendancieusement_____1",clivee=+,cat=adv]	tendancieusement_____1	Default		%default
tendrement	100	advm	[pred="tendrement_____1",clivee=+,detach_neg = +,cat=adv]	tendrement_____1	Default		%default
tentaculairement	100	advm	[pred="tentaculairement_____1",clivee=+,cat=adv]	tentaculairement_____1	Default		%default
tenuto	100	adv	[pred="tenuto_____1",cat=adv]	tenuto_____1	Default		%default
ter	100	adv	[pred="ter_____1",cat=adv]	ter_____1	Default		%default
terminologiquement	100	advm	[pred="terminologiquement_____1",clivee=+,cat=adv]	terminologiquement_____1	Default		%default
terrible	100	adv	[pred="terrible_____1",cat=adv]	terrible_____1	Default		%default
terriblement	100	advm	[pred="terriblement_____1",advi=combien,clivee=+,cat=adv]	terriblement_____1	Default		%default
territorialement	100	advm	[pred="territorialement_____1",clivee=+,cat=adv]	territorialement_____1	Default		%default
tertio	100	adv	[pred="tertio_____1",cat=adv]	tertio_____1	Default		%default
testimonialement	100	advm	[pred="testimonialement_____1",clivee=+,cat=adv]	testimonialement_____1	Default		%default
textuellement	100	advm	[pred="textuellement_____1",clivee=+,cat=adv]	textuellement_____1	Default		%default
thermiquement	100	advm	[pred="thermiquement_____1",clivee=+,cat=adv]	thermiquement_____1	Default		%default
thermodynamiquement	100	advm	[pred="thermodynamiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	thermodynamiquement_____1	Default		%default
thermodynamiquement	100	advm	[pred="thermodynamiquement_____2",clivee=+,cat=adv]	thermodynamiquement_____2	Default		%default
thermométriquement	100	advm	[pred="thermométriquement_____1",clivee=+,cat=adv]	thermométriquement_____1	Default		%default
thermonucléairement	100	advm	[pred="thermonucléairement_____1",clivee=+,cat=adv]	thermonucléairement_____1	Default		%default
thématiquement	100	advm	[pred="thématiquement_____1",clivee=+,cat=adv]	thématiquement_____1	Default		%default
théocratiquement	100	advm	[pred="théocratiquement_____1",clivee=+,cat=adv]	théocratiquement_____1	Default		%default
théologiquement	100	advm	[pred="théologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	théologiquement_____1	Default		%default
théologiquement	100	advm	[pred="théologiquement_____2",clivee=+,cat=adv]	théologiquement_____2	Default		%default
théoriquement	100	advm	[pred="théoriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	théoriquement_____1	Default		%default
théoriquement	100	advm	[pred="théoriquement_____2",clivee=+,cat=adv]	théoriquement_____2	Default		%default
théoriquement	100	advp	[pred="théoriquement_____3",detach = +,detach_neg = +,cat=adv]	théoriquement_____3	Default		%default
thérapeutiquement	100	advm	[pred="thérapeutiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	thérapeutiquement_____1	Default		%default
thérapeutiquement	100	advm	[pred="thérapeutiquement_____2",cat=adv]	thérapeutiquement_____2	Default		%default
thétiquement	100	advm	[pred="thétiquement_____1",clivee=+,cat=adv]	thétiquement_____1	Default		%default
théâtralement	100	advm	[pred="théâtralement_____1",clivee=+,cat=adv]	théâtralement_____1	Default		%default
timidement	100	advm	[pred="timidement_____1",clivee=+,detach_neg = +,cat=adv]	timidement_____1	Default		%default
titulairement	100	advm	[pred="titulairement_____1",clivee=+,cat=adv]	titulairement_____1	Default		%default
tièdement	100	advm	[pred="tièdement_____1",clivee=+,detach_neg = +,cat=adv]	tièdement_____1	Default		%default
tjrs	100	adv	[pred="toujours_____1",cat=adv]	toujours_____1	Default		%default
tjs	100	adv	[pred="toujours_____1",cat=adv]	toujours_____1	Default		%default
tolérablement	100	advm	[pred="tolérablement_____1",clivee=+,cat=adv]	tolérablement_____1	Default		%default
tonalement	100	advm	[pred="tonalement_____1",clivee=+,cat=adv]	tonalement_____1	Default		%default
toniquement	100	advm	[pred="toniquement_____1",clivee=+,cat=adv]	toniquement_____1	Default		%default
topiquement	100	advm	[pred="topiquement_____1",clivee=+,cat=adv]	topiquement_____1	Default		%default
topographiquement	100	advm	[pred="topographiquement_____1",clivee=+,cat=adv]	topographiquement_____1	Default		%default
topologiquement	100	advm	[pred="topologiquement_____1",clivee=+,cat=adv]	topologiquement_____1	Default		%default
toponymiquement	100	advm	[pred="toponymiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	toponymiquement_____1	Default		%default
toponymiquement	100	advm	[pred="toponymiquement_____2",clivee=+,cat=adv]	toponymiquement_____2	Default		%default
torrentiellement	100	advm	[pred="torrentiellement_____1",clivee=+,cat=adv]	torrentiellement_____1	Default		%default
torrentueusement	100	advm	[pred="torrentueusement_____1",clivee=+,cat=adv]	torrentueusement_____1	Default		%default
tortueusement	100	advm	[pred="tortueusement_____1",clivee=+,detach_neg = +,cat=adv]	tortueusement_____1	Default		%default
total	100	adv	[pred="total_____1",cat=adv]	total_____1	Default		%default
totalement	100	advm	[pred="totalement_____1",advi=combien,cat=adv]	totalement_____1	Default		%default
totalitairement	100	advm	[pred="totalitairement_____1",clivee=+,cat=adv]	totalitairement_____1	Default		%default
toujours	100	adv	[pred="toujours_____1",cat=adv]	toujours_____1	Default		%default
toujours autant	100	adv	[pred="toujours autant_____1",cat=adv]	toujours autant_____1	Default		%default
toujours mieux	100	adv	[pred="toujours mieux_____1",cat=adv]	toujours mieux_____1	Default		%default
toujours moins	100	adv	[pred="toujours moins_____1",cat=adv]	toujours moins_____1	Default		%default
toujours plus	100	adv	[pred="toujours plus_____1",cat=adv]	toujours plus_____1	Default		%default
tour à tour	100	adv	[pred="tour à tour_____1",cat=adv]	tour à tour_____1	Default		%default
touristiquement	100	advm	[pred="touristiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	touristiquement_____1	Default		%default
tous azimuts	100	advp	[pred="tous azimuts_____1",chunk_type=GN,cat=adv]	tous azimuts_____1	Default		%default
tous les quatre matins	100	advp	[pred="tous les quatre matins_____1",chunk_type=GN,cat=adv]	tous les quatre matins_____1	Default		%default
tout	100	adv	[pred="tout_____1",cat=adv]	tout_____1	Default		%default
tout au moins	100	adv	[pred="tout au moins_____1",cat=adv]	tout au moins_____1	Default		%default
tout au plus	100	adv	[pred="tout au plus_____1",cat=adv]	tout au plus_____1	Default		%default
tout bas	100	adv	[pred="tout bas_____1",cat=adv]	tout bas_____1	Default		%default
tout bien considéré	100	adv	[pred="tout bien considéré_____1",cat=adv]	tout bien considéré_____1	Default		%default
tout bonnement	100	advm	[pred="tout bonnement_____1",clivee=+,cat=adv]	tout bonnement_____1	Default		%default
tout compte fait	100	adv	[pred="tout compte fait_____1",cat=adv]	tout compte fait_____1	Default		%default
tout court	100	adv	[pred="tout court_____1",cat=adv]	tout court_____1	Default		%default
tout d'abord	100	adv	[pred="tout d'abord_____1",cat=adv]	tout d'abord_____1	Default		%default
tout d'un coup	100	adv	[pred="tout d'un coup_____1",cat=adv]	tout d'un coup_____1	Default		%default
tout de go	100	adv	[pred="tout de go_____1",cat=adv]	tout de go_____1	Default		%default
tout de même	100	adv	[pred="tout de même_____1",cat=adv]	tout de même_____1	Default		%default
tout de suite	100	adv	[pred="tout de suite_____1",cat=adv]	tout de suite_____1	Default		%default
tout du moins	100	adv	[pred="tout du moins_____1",cat=adv]	tout du moins_____1	Default		%default
tout juste	100	adv	[pred="tout juste_____1",adv_kind=modnc,cat=adv]	tout juste_____1	Default		%default
tout juste	100	adv	[pred="tout juste_____2",cat=adv]	tout juste_____2	Default		%default
tout le temps	100	adv	[pred="tout le temps_____1",cat=adv]	tout le temps_____1	Default		%default
tout pareil	100	adv	[pred="tout pareil_____1",cat=adv]	tout pareil_____1	Default		%default
tout particulièrement	100	advm	[pred="tout particulièrement_____1",modnc = +,cat=adv]	tout particulièrement_____1	Default		%default
tout à coup	100	adv	[pred="tout à coup_____1",cat=adv]	tout à coup_____1	Default		%default
tout à fait	100	adv	[pred="tout à fait_____1",cat=adv]	tout à fait_____1	Default		%default
tout à l'heure	100	adv	[pred="tout à l'heure_____1",cat=adv]	tout à l'heure_____1	Default		%default
tout à la fois	100	adv	[pred="tout à la fois_____1",cat=adv]	tout à la fois_____1	Default		%default
tout-à-coup	100	adv	[pred="tout-à-coup_____1",cat=adv]	tout-à-coup_____1	Default		%default
toute	100	adv	[pred="toute_____1",cat=adv]	toute_____1	Default		%default
toutefois	100	adv	[pred="toutefois_____1",cat=adv]	toutefois_____1	Default		%default
tracassièrement	100	advm	[pred="tracassièrement_____1",clivee=+,detach_neg = +,cat=adv]	tracassièrement_____1	Default		%default
traditionnellement	100	advp	[pred="traditionnellement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	traditionnellement_____1	Default		%default
tragiquement	100	advm	[pred="tragiquement_____1",clivee=+,cat=adv]	tragiquement_____1	Default		%default
tranquillement	100	advm	[pred="tranquillement_____1",clivee=+,detach_neg = +,cat=adv]	tranquillement_____1	Default		%default
transactionnellement	100	advm	[pred="transactionnellement_____1",clivee=+,cat=adv]	transactionnellement_____1	Default		%default
transcendantalement	100	advm	[pred="transcendantalement_____1",clivee=+,cat=adv]	transcendantalement_____1	Default		%default
transformationnellement	100	advm	[pred="transformationnellement_____1",clivee=+,cat=adv]	transformationnellement_____1	Default		%default
transitivement	100	advm	[pred="transitivement_____1",clivee=+,cat=adv]	transitivement_____1	Default		%default
transitoirement	100	advm	[pred="transitoirement_____1",advi=en_combien_de_temps,clivee=+,detach = +,detach_neg = +,cat=adv]	transitoirement_____1	Default		%default
transversalement	100	advm	[pred="transversalement_____1",clivee=+,cat=adv]	transversalement_____1	Default		%default
traînardement	100	advm	[pred="traînardement_____1",clivee=+,cat=adv]	traînardement_____1	Default		%default
traîtreusement	100	advm	[pred="traîtreusement_____1",clivee=+,cat=adv]	traîtreusement_____1	Default		%default
treizièmement	100	advp	[pred="treizièmement_____1",detach = +,detach_neg = +,cat=adv]	treizièmement_____1	Default		%default
trentièmement	100	advp	[pred="trentièmement_____1",detach = +,detach_neg = +,cat=adv]	trentièmement_____1	Default		%default
triangulairement	100	advm	[pred="triangulairement_____1",clivee=+,cat=adv]	triangulairement_____1	Default		%default
tribalement	100	advm	[pred="tribalement_____1",clivee=+,cat=adv]	tribalement_____1	Default		%default
tridimensionnellement	100	advm	[pred="tridimensionnellement_____1",clivee=+,cat=adv]	tridimensionnellement_____1	Default		%default
trigonométriquement	100	advm	[pred="trigonométriquement_____1",clivee=+,cat=adv]	trigonométriquement_____1	Default		%default
trimestriellement	100	advm	[pred="trimestriellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	trimestriellement_____1	Default		%default
triomphalement	100	advm	[pred="triomphalement_____1",clivee=+,cat=adv]	triomphalement_____1	Default		%default
triplement	100	advm	[pred="triplement_____1",advi=combien,cat=adv]	triplement_____1	Default		%default
trisannuellement	100	advm	[pred="trisannuellement_____1",advi=à_quelle_fréquence,clivee=+,detach_neg = +,cat=adv]	trisannuellement_____1	Default		%default
tristement	100	advm	[pred="tristement_____1",clivee=+,detach_neg = +,cat=adv]	tristement_____1	Default		%default
tristement	100	advm	[pred="tristement_____2",clivee=+,cat=adv]	tristement_____2	Default		%default
trivialement	100	advm	[pred="trivialement_____1",clivee=+,detach_neg = +,cat=adv]	trivialement_____1	Default		%default
troisièmement	100	advp	[pred="troisièmement_____1",detach = +,detach_neg = +,cat=adv]	troisièmement_____1	Default		%default
trompeusement	100	advm	[pred="trompeusement_____1",clivee=+,cat=adv]	trompeusement_____1	Default		%default
trop	100	adv	[pred="trop_____1<Obl:(pour-sn|pour-sinf|pour-scompl)>",adv_kind=intens,cat=adv]	trop_____1	Default		%default
très	100	adv	[pred="très_____1",adv_kind=très,cat=adv]	très_____1	Default		%default
tumultuairement	100	advm	[pred="tumultuairement_____1",clivee=+,cat=adv]	tumultuairement_____1	Default		%default
tumultueusement	100	advm	[pred="tumultueusement_____1",clivee=+,cat=adv]	tumultueusement_____1	Default		%default
turpidement	100	advm	[pred="turpidement_____1",clivee=+,detach_neg = +,cat=adv]	turpidement_____1	Default		%default
tutélairement	100	advm	[pred="tutélairement_____1",clivee=+,cat=adv]	tutélairement_____1	Default		%default
typiquement	100	advm	[pred="typiquement_____1",advi=combien,cat=adv]	typiquement_____1	Default		%default
typographiquement	100	advm	[pred="typographiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	typographiquement_____1	Default		%default
typographiquement	100	advm	[pred="typographiquement_____2",clivee=+,cat=adv]	typographiquement_____2	Default		%default
typologiquement	100	advm	[pred="typologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	typologiquement_____1	Default		%default
tyranniquement	100	advm	[pred="tyranniquement_____1",clivee=+,detach_neg = +,cat=adv]	tyranniquement_____1	Default		%default
télégraphiquement	100	advm	[pred="télégraphiquement_____1",clivee=+,cat=adv]	télégraphiquement_____1	Default		%default
télématiquement	100	advm	[pred="télématiquement_____1",clivee=+,cat=adv]	télématiquement_____1	Default		%default
téléologiquement	100	advm	[pred="téléologiquement_____1",clivee=+,cat=adv]	téléologiquement_____1	Default		%default
télépathiquement	100	advm	[pred="télépathiquement_____1",clivee=+,cat=adv]	télépathiquement_____1	Default		%default
téléphoniquement	100	advm	[pred="téléphoniquement_____1",clivee=+,cat=adv]	téléphoniquement_____1	Default		%default
témérairement	100	advm	[pred="témérairement_____1",clivee=+,detach_neg = +,cat=adv]	témérairement_____1	Default		%default
témérairement	100	advp	[pred="témérairement_____2",detach = +,detach_neg = +,cat=adv]	témérairement_____2	Default		%default
ténébreusement	100	advm	[pred="ténébreusement_____1",clivee=+,detach_neg = +,cat=adv]	ténébreusement_____1	Default		%default
tétaniquement	100	advm	[pred="tétaniquement_____1",clivee=+,cat=adv]	tétaniquement_____1	Default		%default
tétralogiquement	100	advm	[pred="tétralogiquement_____1",clivee=+,cat=adv]	tétralogiquement_____1	Default		%default
tétraédriquement	100	advm	[pred="tétraédriquement_____1",clivee=+,cat=adv]	tétraédriquement_____1	Default		%default
tête à tête	100	adv	[pred="tête à tête_____1",cat=adv]	tête à tête_____1	Default		%default
tête-bêche	100	adv	[pred="tête-bêche_____1",cat=adv]	tête-bêche_____1	Default		%default
tôt	100	adv	[pred="tôt_____1",cat=adv]	tôt_____1	Default		%default
ultimement	100	advm	[pred="ultimement_____1",advi=à_quel_moment,clivee=+,detach_neg = +,cat=adv]	ultimement_____1	Default		%default
ultimo	100	adv	[pred="ultimo_____1",cat=adv]	ultimo_____1	Default		%default
ultérieurement	100	advm	[pred="ultérieurement_____1",advi=à_quel_moment,clivee=+,detach = +,detach_neg = +,cat=adv]	ultérieurement_____1	Default		%default
un de ces _NUM	100	advp	[pred="un de ces _NUM_____1",chunk_type=GN,cat=adv]	un de ces _NUM_____1	Default		%default
un jour	100	adv	[pred="un jour_____1",cat=adv]	un jour_____1	Default		%default
un peu	100	adv	[pred="un peu_____1",adv_kind=intens,cat=adv]	un peu_____1	Default		%default
un peu plus tard	100	adv	[pred="un peu plus tard_____1",cat=adv]	un peu plus tard_____1	Default		%default
un tant soit peu	100	adv	[pred="un tant soit peu_____1",adv_kind=intens,cat=adv]	un tant soit peu_____1	Default		%default
un tantinet	100	adv	[pred="un tantinet_____1",adv_kind=intens,cat=adv]	un tantinet_____1	Default		%default
un tout petit peu	100	adv	[pred="un tout petit peu_____1",adv_kind=intens,cat=adv]	un tout petit peu_____1	Default		%default
unanimement	100	advm	[pred="unanimement_____1",clivee=+,cat=adv]	unanimement_____1	Default		%default
une fois n'est pas coutume	100	adv	[pred="une fois n'est pas coutume_____1",cat=adv]	une fois n'est pas coutume_____1	Default		%default
une fois pour toute	100	adv	[pred="une fois pour toute_____1",cat=adv]	une fois pour toute_____1	Default		%default
une à une	100	adv	[pred="une à une_____1",cat=adv]	une à une_____1	Default		%default
uniformément	100	advm	[pred="uniformément_____1",clivee=+,cat=adv]	uniformément_____1	Default		%default
unilatéralement	100	advm	[pred="unilatéralement_____1",clivee=+,cat=adv]	unilatéralement_____1	Default		%default
uniment	100	advm	[pred="uniment_____1",clivee=+,cat=adv]	uniment_____1	Default		%default
uninominalement	100	advm	[pred="uninominalement_____1",clivee=+,cat=adv]	uninominalement_____1	Default		%default
uniquement	100	advm	[pred="uniquement_____1",detach_neg = +,modnc = +,cat=adv]	uniquement_____1	Default		%default
unitairement	100	advm	[pred="unitairement_____1",clivee=+,cat=adv]	unitairement_____1	Default		%default
universellement	100	advm	[pred="universellement_____1",clivee=+,cat=adv]	universellement_____1	Default		%default
univoquement	100	advm	[pred="univoquement_____1",clivee=+,cat=adv]	univoquement_____1	Default		%default
unièmement	100	advp	[pred="unièmement_____1",detach = +,detach_neg = +,cat=adv]	unièmement_____1	Default		%default
urbainement	100	advm	[pred="urbainement_____1",clivee=+,detach_neg = +,cat=adv]	urbainement_____1	Default		%default
urbi et orbi	100	adv	[pred="urbi et orbi_____1",cat=adv]	urbi et orbi_____1	Default		%default
urgemment	100	advm	[pred="urgemment_____1",clivee=+,cat=adv]	urgemment_____1	Default		%default
usuellement	100	advm	[pred="usuellement_____1",clivee=+,cat=adv]	usuellement_____1	Default		%default
usuellement	100	advp	[pred="usuellement_____2",detach = +,detach_neg = +,modnc = +,cat=adv]	usuellement_____2	Default		%default
usurairement	100	advm	[pred="usurairement_____1",cat=adv]	usurairement_____1	Default		%default
utilement	100	advm	[pred="utilement_____1",clivee=+,cat=adv]	utilement_____1	Default		%default
utilitairement	100	advm	[pred="utilitairement_____1",clivee=+,cat=adv]	utilitairement_____1	Default		%default
utopiquement	100	advm	[pred="utopiquement_____1",clivee=+,cat=adv]	utopiquement_____1	Default		%default
uvulairement	100	advm	[pred="uvulairement_____1",clivee=+,cat=adv]	uvulairement_____1	Default		%default
vachement	100	advm	[pred="vachement_____1",advi=combien,cat=adv]	vachement_____1	Default		%default
vaginalement	100	advm	[pred="vaginalement_____1",clivee=+,cat=adv]	vaginalement_____1	Default		%default
vaguement	100	advm	[pred="vaguement_____1",clivee=+,cat=adv]	vaguement_____1	Default		%default
vaillamment	100	advm	[pred="vaillamment_____1",clivee=+,detach_neg = +,cat=adv]	vaillamment_____1	Default		%default
vaille que vaille	100	adv	[pred="vaille que vaille_____1",cat=adv]	vaille que vaille_____1	Default		%default
vainement	100	advm	[pred="vainement_____1",clivee=+,cat=adv]	vainement_____1	Default		%default
valablement	100	advm	[pred="valablement_____1",clivee=+,cat=adv]	valablement_____1	Default		%default
valeureusement	100	advm	[pred="valeureusement_____1",clivee=+,detach_neg = +,cat=adv]	valeureusement_____1	Default		%default
validement	100	advm	[pred="validement_____1",clivee=+,cat=adv]	validement_____1	Default		%default
vaniteusement	100	advm	[pred="vaniteusement_____1",clivee=+,detach_neg = +,cat=adv]	vaniteusement_____1	Default		%default
vaporeusement	100	advm	[pred="vaporeusement_____1",clivee=+,cat=adv]	vaporeusement_____1	Default		%default
variablement	100	advm	[pred="variablement_____1",clivee=+,cat=adv]	variablement_____1	Default		%default
vaseusement	100	advm	[pred="vaseusement_____1",clivee=+,detach_neg = +,cat=adv]	vaseusement_____1	Default		%default
vastement	100	advm	[pred="vastement_____1",clivee=+,cat=adv]	vastement_____1	Default		%default
velléitairement	100	advm	[pred="velléitairement_____1",clivee=+,detach_neg = +,cat=adv]	velléitairement_____1	Default		%default
vendredi	100	adv	[pred="vendredi_____1",cat=adv]	vendredi_____1	Default		%default
venimeusement	100	advm	[pred="venimeusement_____1",clivee=+,detach_neg = +,cat=adv]	venimeusement_____1	Default		%default
ventralement	100	advm	[pred="ventralement_____1",clivee=+,cat=adv]	ventralement_____1	Default		%default
ventre saint gris	100	adv	[pred="ventre saint gris_____1",cat=adv]	ventre saint gris_____1	Default		%default
ventre à terre	100	adv	[pred="ventre à terre_____1",cat=adv]	ventre à terre_____1	Default		%default
verbalement	100	advm	[pred="verbalement_____1",clivee=+,cat=adv]	verbalement_____1	Default		%default
verbeusement	100	advm	[pred="verbeusement_____1",clivee=+,detach_neg = +,cat=adv]	verbeusement_____1	Default		%default
verbi gratia	100	adv	[pred="verbi gratia_____1",cat=adv]	verbi gratia_____1	Default		%default
versatilement	100	advm	[pred="versatilement_____1",clivee=+,detach_neg = +,cat=adv]	versatilement_____1	Default		%default
vertement	100	advm	[pred="vertement_____1",clivee=+,cat=adv]	vertement_____1	Default		%default
verticalement	100	advm	[pred="verticalement_____1",clivee=+,cat=adv]	verticalement_____1	Default		%default
vertigineusement	100	advm	[pred="vertigineusement_____1",advi=combien,cat=adv]	vertigineusement_____1	Default		%default
vertueusement	100	advm	[pred="vertueusement_____1",clivee=+,detach_neg = +,cat=adv]	vertueusement_____1	Default		%default
verveusement	100	advm	[pred="verveusement_____1",clivee=+,detach_neg = +,cat=adv]	verveusement_____1	Default		%default
vestimentairement	100	advm	[pred="vestimentairement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	vestimentairement_____1	Default		%default
veulement	100	advm	[pred="veulement_____1",clivee=+,detach_neg = +,cat=adv]	veulement_____1	Default		%default
vi	100	adv	[pred="oui_____1",cat=adv]	oui_____1	Default		%default
vice versa	100	adv	[pred="vice versa_____1",cat=adv]	vice versa_____1	Default		%default
vice-versa	100	adv	[pred="vice-versa_____1",cat=adv]	vice-versa_____1	Default		%default
vicieusement	100	advm	[pred="vicieusement_____1",clivee=+,detach_neg = +,cat=adv]	vicieusement_____1	Default		%default
victorieusement	100	advm	[pred="victorieusement_____1",clivee=+,detach_neg = +,cat=adv]	victorieusement_____1	Default		%default
vieillottement	100	advm	[pred="vieillottement_____1",clivee=+,detach_neg = +,cat=adv]	vieillottement_____1	Default		%default
vigesimo	100	adv	[pred="vigesimo_____1",cat=adv]	vigesimo_____1	Default		%default
vigilamment	100	advm	[pred="vigilamment_____1",clivee=+,detach_neg = +,cat=adv]	vigilamment_____1	Default		%default
vigoureusement	100	advm	[pred="vigoureusement_____1",clivee=+,detach_neg = +,cat=adv]	vigoureusement_____1	Default		%default
vilain	100	adv	[pred="vilain_____1",cat=adv]	vilain_____1	Default		%default
vilainement	100	advm	[pred="vilainement_____1",clivee=+,detach_neg = +,cat=adv]	vilainement_____1	Default		%default
vilement	100	advm	[pred="vilement_____1",clivee=+,detach_neg = +,cat=adv]	vilement_____1	Default		%default
vindicativement	100	advm	[pred="vindicativement_____1",clivee=+,detach_neg = +,cat=adv]	vindicativement_____1	Default		%default
vingtièmement	100	advp	[pred="vingtièmement_____1",detach = +,detach_neg = +,cat=adv]	vingtièmement_____1	Default		%default
violemment	100	advm	[pred="violemment_____1",clivee=+,detach_neg = +,cat=adv]	violemment_____1	Default		%default
virginalement	100	advm	[pred="virginalement_____1",clivee=+,cat=adv]	virginalement_____1	Default		%default
virilement	100	advm	[pred="virilement_____1",clivee=+,detach_neg = +,cat=adv]	virilement_____1	Default		%default
virtuellement	100	advm	[pred="virtuellement_____1",advi=combien,cat=adv]	virtuellement_____1	Default		%default
virulemment	100	advm	[pred="virulemment_____1",clivee=+,detach_neg = +,cat=adv]	virulemment_____1	Default		%default
viscéralement	100	advm	[pred="viscéralement_____1",advi=combien,clivee=+,cat=adv]	viscéralement_____1	Default		%default
visiblement	100	advm	[pred="visiblement_____1",clivee=+,cat=adv]	visiblement_____1	Default		%default
visiblement	100	advp	[pred="visiblement_____2",detach = +,detach_neg = +,modnc = +,cat=adv]	visiblement_____2	Default		%default
visqueusement	100	advm	[pred="visqueusement_____1",clivee=+,cat=adv]	visqueusement_____1	Default		%default
visuellement	100	advm	[pred="visuellement_____1",clivee=+,cat=adv]	visuellement_____1	Default		%default
vitalement	100	advm	[pred="vitalement_____1",clivee=+,cat=adv]	vitalement_____1	Default		%default
vite	100	adv	[pred="vite_____1",cat=adv]	vite_____1	Default		%default
vitement	100	advm	[pred="vitement_____1",clivee=+,cat=adv]	vitement_____1	Default		%default
vivace	100	adv	[pred="vivace_____1",cat=adv]	vivace_____1	Default		%default
vivacement	100	advm	[pred="vivacement_____1",clivee=+,cat=adv]	vivacement_____1	Default		%default
vivement	100	advm	[pred="vivement_____1",advi=combien,cat=adv]	vivement_____1	Default		%default
vivement	100	advm	[pred="vivement_____2",clivee=+,detach_neg = +,cat=adv]	vivement_____2	Default		%default
vivi	100	adv	[pred="oui_____1",cat=adv]	oui_____1	Default		%default
viviparement	100	advm	[pred="viviparement_____1",clivee=+,cat=adv]	viviparement_____1	Default		%default
vocalement	100	advm	[pred="vocalement_____1",clivee=+,cat=adv]	vocalement_____1	Default		%default
vocaliquement	100	advm	[pred="vocaliquement_____1",clivee=+,cat=adv]	vocaliquement_____1	Default		%default
volagement	100	advm	[pred="volagement_____1",clivee=+,detach_neg = +,cat=adv]	volagement_____1	Default		%default
volcaniquement	100	advm	[pred="volcaniquement_____1",clivee=+,cat=adv]	volcaniquement_____1	Default		%default
volens nolens	100	adv	[pred="volens nolens_____1",cat=adv]	volens nolens_____1	Default		%default
volontairement	100	advm	[pred="volontairement_____1",clivee=+,detach = +,detach_neg = +,cat=adv]	volontairement_____1	Default		%default
volontiers	100	adv	[pred="volontiers_____1",cat=adv]	volontiers_____1	Default		%default
volubilement	100	advm	[pred="volubilement_____1",clivee=+,detach_neg = +,cat=adv]	volubilement_____1	Default		%default
volumétriquement	100	advm	[pred="volumétriquement_____1",clivee=+,cat=adv]	volumétriquement_____1	Default		%default
voluptueusement	100	advm	[pred="voluptueusement_____1",clivee=+,detach_neg = +,cat=adv]	voluptueusement_____1	Default		%default
voracement	100	advm	[pred="voracement_____1",clivee=+,detach_neg = +,cat=adv]	voracement_____1	Default		%default
voui	100	adv	[pred="oui_____1",cat=adv]	oui_____1	Default		%default
vrai	100	adv	[pred="vrai_____1",cat=adv]	vrai_____1	Default		%default
vraiment	100	advm	[pred="vraiment_____1",advi=combien,cat=adv]	vraiment_____1	Default		%default
vraiment	100	advm	[pred="vraiment_____2",clivee=+,cat=adv]	vraiment_____2	Default		%default
vraiment	100	advp	[pred="vraiment_____3",detach = +,detach_neg = +,cat=adv]	vraiment_____3	Default		%default
vraisemblablement	100	advm	[pred="vraisemblablement_____1",clivee=+,cat=adv]	vraisemblablement_____1	Default		%default
vraisemblablement	100	advp	[pred="vraisemblablement_____2<Suj:(scompl)>",detach = +,detach_neg = +,modnc = +,cat=adv]	vraisemblablement_____2	Default		%default
vulgairement	100	advm	[pred="vulgairement_____1",clivee=+,detach_neg = +,cat=adv]	vulgairement_____1	Default		%default
vulgo	100	adv	[pred="vulgo_____1",cat=adv]	vulgo_____1	Default		%default
végétalement	100	advm	[pred="végétalement_____1",clivee=+,cat=adv]	végétalement_____1	Default		%default
végétativement	100	advm	[pred="végétativement_____1",clivee=+,cat=adv]	végétativement_____1	Default		%default
véhémentement	100	advm	[pred="véhémentement_____1",clivee=+,detach_neg = +,cat=adv]	véhémentement_____1	Default		%default
vélairement	100	advm	[pred="vélairement_____1",clivee=+,cat=adv]	vélairement_____1	Default		%default
vélocement	100	advm	[pred="vélocement_____1",clivee=+,cat=adv]	vélocement_____1	Default		%default
vénalement	100	advm	[pred="vénalement_____1",clivee=+,cat=adv]	vénalement_____1	Default		%default
véniellement	100	advm	[pred="véniellement_____1",clivee=+,cat=adv]	véniellement_____1	Default		%default
vénéneusement	100	advm	[pred="vénéneusement_____1",clivee=+,cat=adv]	vénéneusement_____1	Default		%default
vénérablement	100	advm	[pred="vénérablement_____1",clivee=+,cat=adv]	vénérablement_____1	Default		%default
véracement	100	advm	[pred="véracement_____1",clivee=+,cat=adv]	véracement_____1	Default		%default
véridiquement	100	advm	[pred="véridiquement_____1",clivee=+,cat=adv]	véridiquement_____1	Default		%default
véritablement	100	advm	[pred="véritablement_____1",advi=combien,cat=adv]	véritablement_____1	Default		%default
véritablement	100	advm	[pred="véritablement_____2",clivee=+,cat=adv]	véritablement_____2	Default		%default
véritablement	100	advp	[pred="véritablement_____3",detach = +,detach_neg = +,cat=adv]	véritablement_____3	Default		%default
vésaniquement	100	advm	[pred="vésaniquement_____1",clivee=+,cat=adv]	vésaniquement_____1	Default		%default
vétilleusement	100	advm	[pred="vétilleusement_____1",clivee=+,detach_neg = +,cat=adv]	vétilleusement_____1	Default		%default
vétustement	100	advm	[pred="vétustement_____1",clivee=+,cat=adv]	vétustement_____1	Default		%default
xylographiquement	100	advm	[pred="xylographiquement_____1",clivee=+,cat=adv]	xylographiquement_____1	Default		%default
xérographiquement	100	advm	[pred="xérographiquement_____1",clivee=+,cat=adv]	xérographiquement_____1	Default		%default
yeah	100	adv	[pred="oui_____1",cat=adv]	oui_____1	Default		%default
zoologiquement	100	advm	[pred="zoologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	zoologiquement_____1	Default		%default
~	100	adv	[pred="environ_____1",adv_kind=modnc,cat=adv]	environ_____1	Default		%default
±	100	adv	[pred="plus ou moins_____1",cat=adv]	plus ou moins_____1	Default		%default
à bon marché	100	adv	[pred="à bon marché_____1",chunk_type=GP,cat=adv]	à bon marché_____1	Default		%default
à bout portant	100	adv	[pred="à bout portant_____1",chunk_type=GP,cat=adv]	à bout portant_____1	Default		%default
à brûle-pourpoint	100	adv	[pred="à brûle-pourpoint_____1",chunk_type=GP,cat=adv]	à brûle-pourpoint_____1	Default		%default
à ce moment-là	100	adv	[pred="à ce moment-là_____1",cat=adv]	à ce moment-là_____1	Default		%default
à ce propos	100	adv	[pred="à ce propos_____1",cat=adv]	à ce propos_____1	Default		%default
à ces mots	100	adv	[pred="à ces mots_____1",chunk_type=GP,cat=adv]	à ces mots_____1	Default		%default
à cet égard	100	adv	[pred="à cet égard_____1",cat=adv]	à cet égard_____1	Default		%default
à contre-pied	100	adv	[pred="à contre-pied_____1",chunk_type=GP,cat=adv]	à contre-pied_____1	Default		%default
à contrecoeur	100	adv	[pred="à contrecoeur_____1",chunk_type=GP,cat=adv]	à contrecoeur_____1	Default		%default
à cor et à cri	100	adv	[pred="à cor et à cri_____1",cat=adv]	à cor et à cri_____1	Default		%default
à cors et à cri	100	adv	[pred="à cors et à cri_____1",cat=adv]	à cors et à cri_____1	Default		%default
à cors et à cris	100	adv	[pred="à cors et à cris_____1",cat=adv]	à cors et à cris_____1	Default		%default
à coup sûr	100	adv	[pred="à coup sûr_____1",chunk_type=GP,cat=adv]	à coup sûr_____1	Default		%default
à court terme	100	adv	[pred="à court terme_____1",chunk_type=GP,cat=adv]	à court terme_____1	Default		%default
à demi	100	adv	[pred="à demi_____1",cat=adv]	à demi_____1	Default		%default
à demi-voix	100	adv	[pred="à demi-voix_____1",chunk_type=GP,cat=adv]	à demi-voix_____1	Default		%default
à dire vrai	100	adv	[pred="à dire vrai_____1",cat=adv]	à dire vrai_____1	Default		%default
à donf	100	adv	[pred="à donf_____1",chunk_type=GP,cat=adv]	à donf_____1	Default		%default
à droite	100	adv	[pred="à droite_____1",chunk_type=GP,cat=adv]	à droite_____1	Default		%default
à fond	100	adv	[pred="à fond_____1",chunk_type=GP,cat=adv]	à fond_____1	Default		%default
à force	100	adv	[pred="à force_____1",cat=adv]	à force_____1	Default		%default
à gauche	100	adv	[pred="à gauche_____1",chunk_type=GP,cat=adv]	à gauche_____1	Default		%default
à gogo	100	adv	[pred="à gogo_____1",chunk_type=GP,cat=adv]	à gogo_____1	Default		%default
à gorge déployée	100	adv	[pred="à gorge déployée_____1",cat=adv]	à gorge déployée_____1	Default		%default
à hue et à dia	100	adv	[pred="à hue et à dia_____1",chunk_type=GP,cat=adv]	à hue et à dia_____1	Default		%default
à jamais	100	adv	[pred="à jamais_____1",cat=adv]	à jamais_____1	Default		%default
à juste titre	100	adv	[pred="à juste titre_____1",cat=adv]	à juste titre_____1	Default		%default
à l'arrache	100	advp	[pred="à l'arrache_____1",cat=adv]	à l'arrache_____1	Default		%default
à l'inverse	100	adv	[pred="à l'inverse_____1",cat=adv]	à l'inverse_____1	Default		%default
à l'oeil nu	100	adv	[pred="à l'oeil nu_____1",chunk_type=GP,cat=adv]	à l'oeil nu_____1	Default		%default
à la bourre	100	adv	[pred="à la bourre_____1",chunk_type=GP,cat=adv]	à la bourre_____1	Default		%default
à la cantonnade	100	adv	[pred="à la cantonnade_____1",cat=adv]	à la cantonnade_____1	Default		%default
à la fois	100	adv	[pred="à la fois_____1",cat=adv]	à la fois_____1	Default		%default
à la louche	100	adv	[pred="à la louche_____1",chunk_type=GP,cat=adv]	à la louche_____1	Default		%default
à la one again	100	advp	[pred="à la one again_____1",cat=adv]	à la one again_____1	Default		%default
à la place	100	adv	[pred="à la place_____1",cat=adv]	à la place_____1	Default		%default
à la réflexion	100	adv	[pred="à la réflexion_____1",cat=adv]	à la réflexion_____1	Default		%default
à la six-quatre-deux	100	adv	[pred="à la six-quatre-deux_____1",chunk_type=GP,cat=adv]	à la six-quatre-deux_____1	Default		%default
à la volée	100	adv	[pred="à la volée_____1",chunk_type=GP,cat=adv]	à la volée_____1	Default		%default
à loisir	100	adv	[pred="à loisir_____1",cat=adv]	à loisir_____1	Default		%default
à long terme	100	adv	[pred="à long terme_____1",chunk_type=GP,cat=adv]	à long terme_____1	Default		%default
à merveille	100	adv	[pred="à merveille_____1",chunk_type=GP,cat=adv]	à merveille_____1	Default		%default
à mi-chemin	100	adv	[pred="à mi-chemin_____1",chunk_type=GP,cat=adv]	à mi-chemin_____1	Default		%default
à moitié	100	adv	[pred="à moitié_____1",chunk_type=GP,cat=adv]	à moitié_____1	Default		%default
à moyen terme	100	adv	[pred="à moyen terme_____1",chunk_type=GP,cat=adv]	à moyen terme_____1	Default		%default
à n'en pas douter	100	advp	[pred="à n'en pas douter_____1",chunk_type=GN,cat=adv]	à n'en pas douter_____1	Default		%default
à nouveau	100	adv	[pred="à nouveau_____1",cat=adv]	à nouveau_____1	Default		%default
à outrance	100	adv	[pred="à outrance_____1",chunk_type=GP,cat=adv]	à outrance_____1	Default		%default
à part	100	adv	[pred="à part_____1",chunk_type=GP,cat=adv]	à part_____1	Default		%default
à part ça	100	adv	[pred="à part ça_____1",cat=adv]	à part ça_____1	Default		%default
à peine	100	adv	[pred="à peine_____1",adv_kind=modnc,cat=adv]	à peine_____1	Default		%default
à peine	100	adv	[pred="à peine_____2",cat=adv]	à peine_____2	Default		%default
à petit feu	100	adv	[pred="à petit feu_____1",chunk_type=GP,cat=adv]	à petit feu_____1	Default		%default
à peu près	100	adv	[pred="à peu près_____1",adv_kind=modnc,cat=adv]	à peu près_____1	Default		%default
à peu près	100	adv	[pred="à peu près_____2",cat=adv]	à peu près_____2	Default		%default
à pic	100	adv	[pred="à pic_____1",chunk_type=GP,cat=adv]	à pic_____1	Default		%default
à preuve	100	adv	[pred="à preuve_____1",cat=adv]	à preuve_____1	Default		%default
à priori	100	adv	[pred="à priori_____1",cat=adv]	à priori_____1	Default		%default
à propos	100	adv	[pred="à propos_____1",cat=adv]	à propos_____1	Default		%default
à présent	100	adv	[pred="à présent_____1",chunk_type=GP,cat=adv]	à présent_____1	Default		%default
à qui mieux mieux	100	adv	[pred="à qui mieux mieux_____1",cat=adv]	à qui mieux mieux_____1	Default		%default
à reculons	100	advp	[pred="à reculons_____1",chunk_type=GN,cat=adv]	à reculons_____1	Default		%default
à temps	100	adv	[pred="à temps_____1",cat=adv]	à temps_____1	Default		%default
à terme	100	adv	[pred="à terme_____1",chunk_type=GP,cat=adv]	à terme_____1	Default		%default
à tire-larigot	100	adv	[pred="à tire-larigot_____1",chunk_type=GP,cat=adv]	à tire-larigot_____1	Default		%default
à tort	100	adv	[pred="à tort_____1",cat=adv]	à tort_____1	Default		%default
à tort et à travers	100	adv	[pred="à tort et à travers_____1",chunk_type=GP,cat=adv]	à tort et à travers_____1	Default		%default
à tous les coups	100	advp	[pred="à tous les coups_____1",chunk_type=GN,cat=adv]	à tous les coups_____1	Default		%default
à tout jamais	100	adv	[pred="à tout jamais_____1",cat=adv]	à tout jamais_____1	Default		%default
à tout le moins	100	adv	[pred="à tout le moins_____1",cat=adv]	à tout le moins_____1	Default		%default
à tout prix	100	adv	[pred="à tout prix_____1",cat=adv]	à tout prix_____1	Default		%default
à tout va	100	adv	[pred="à tout va_____1",cat=adv]	à tout va_____1	Default		%default
à tout-va	100	adv	[pred="à tout va_____1",cat=adv]	à tout va_____1	Default		%default
à toutes jambes	100	adv	[pred="à toutes jambes_____1",chunk_type=GP,cat=adv]	à toutes jambes_____1	Default		%default
à tue-tête	100	adv	[pred="à tue-tête_____1",cat=adv]	à tue-tête_____1	Default		%default
à tâtons	100	adv	[pred="à tâtons_____1",chunk_type=GP,cat=adv]	à tâtons_____1	Default		%default
à tête reposée	100	adv	[pred="à tête reposée_____1",chunk_type=GP,cat=adv]	à tête reposée_____1	Default		%default
à vau-l'eau	100	adv	[pred="à vau-l'eau_____1",chunk_type=GP,cat=adv]	à vau-l'eau_____1	Default		%default
à vol d'oiseau	100	adv	[pred="à vol d'oiseau_____1",chunk_type=GP,cat=adv]	à vol d'oiseau_____1	Default		%default
à vrai dire	100	adv	[pred="à vrai dire_____1",cat=adv]	à vrai dire_____1	Default		%default
à vue d'oeil	100	adv	[pred="à vue d'oeil_____1",chunk_type=GP,cat=adv]	à vue d'oeil_____1	Default		%default
à-tout-va	100	adv	[pred="à tout va_____1",cat=adv]	à tout va_____1	Default		%default
âcrement	100	advm	[pred="âcrement_____1",clivee=+,detach_neg = +,cat=adv]	âcrement_____1	Default		%default
âprement	100	advm	[pred="âprement_____1",clivee=+,detach_neg = +,cat=adv]	âprement_____1	Default		%default
çà et là	100	adv	[pred="çà et là_____1",cat=adv]	çà et là_____1	Default		%default
éclatamment	100	advm	[pred="éclatamment_____1",advi=combien,clivee=+,cat=adv]	éclatamment_____1	Default		%default
éclectiquement	100	advm	[pred="éclectiquement_____1",clivee=+,detach_neg = +,cat=adv]	éclectiquement_____1	Default		%default
écologiquement	100	advm	[pred="écologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	écologiquement_____1	Default		%default
écologiquement	100	advm	[pred="écologiquement_____2",clivee=+,cat=adv]	écologiquement_____2	Default		%default
économement	100	advm	[pred="économement_____1",clivee=+,detach_neg = +,cat=adv]	économement_____1	Default		%default
économiquement	100	advm	[pred="économiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	économiquement_____1	Default		%default
économiquement	100	advm	[pred="économiquement_____2",clivee=+,cat=adv]	économiquement_____2	Default		%default
économétriquement	100	advm	[pred="économétriquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	économétriquement_____1	Default		%default
économétriquement	100	advm	[pred="économétriquement_____2",clivee=+,cat=adv]	économétriquement_____2	Default		%default
édéniquement	100	advm	[pred="édéniquement_____1",clivee=+,cat=adv]	édéniquement_____1	Default		%default
également	100	advm	[pred="également_____1",clivee=+,cat=adv]	également_____1	Default		%default
également	100	advp	[pred="également_____2",detach = +,detach_neg = +,cat=adv]	également_____2	Default		%default
égalitairement	100	advm	[pred="égalitairement_____1",clivee=+,cat=adv]	égalitairement_____1	Default		%default
égocentriquement	100	advm	[pred="égocentriquement_____1",clivee=+,detach_neg = +,cat=adv]	égocentriquement_____1	Default		%default
égoïstement	100	advm	[pred="égoïstement_____1",clivee=+,detach_neg = +,cat=adv]	égoïstement_____1	Default		%default
égoïstement	100	advp	[pred="égoïstement_____2",detach = +,detach_neg = +,cat=adv]	égoïstement_____2	Default		%default
égrillardement	100	advm	[pred="égrillardement_____1",clivee=+,detach_neg = +,cat=adv]	égrillardement_____1	Default		%default
éhontément	100	advm	[pred="éhontément_____1",advi=combien,clivee=+,cat=adv]	éhontément_____1	Default		%default
élastiquement	100	advm	[pred="élastiquement_____1",clivee=+,cat=adv]	élastiquement_____1	Default		%default
électivement	100	advm	[pred="électivement_____1",clivee=+,cat=adv]	électivement_____1	Default		%default
électoralement	100	advm	[pred="électoralement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	électoralement_____1	Default		%default
électoralement	100	advm	[pred="électoralement_____2",cat=adv]	électoralement_____2	Default		%default
électriquement	100	advm	[pred="électriquement_____1",clivee=+,cat=adv]	électriquement_____1	Default		%default
électrochimiquement	100	advm	[pred="électrochimiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	électrochimiquement_____1	Default		%default
électrochimiquement	100	advm	[pred="électrochimiquement_____2",clivee=+,cat=adv]	électrochimiquement_____2	Default		%default
électrodynamiquement	100	advm	[pred="électrodynamiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	électrodynamiquement_____1	Default		%default
électrodynamiquement	100	advm	[pred="électrodynamiquement_____2",clivee=+,cat=adv]	électrodynamiquement_____2	Default		%default
électrolytiquement	100	advm	[pred="électrolytiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	électrolytiquement_____1	Default		%default
électrolytiquement	100	advm	[pred="électrolytiquement_____2",clivee=+,cat=adv]	électrolytiquement_____2	Default		%default
électromagnétiquement	100	advm	[pred="électromagnétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	électromagnétiquement_____1	Default		%default
électromagnétiquement	100	advm	[pred="électromagnétiquement_____2",clivee=+,cat=adv]	électromagnétiquement_____2	Default		%default
électromécaniquement	100	advm	[pred="électromécaniquement_____1",clivee=+,cat=adv]	électromécaniquement_____1	Default		%default
électroniquement	100	advm	[pred="électroniquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	électroniquement_____1	Default		%default
électroniquement	100	advm	[pred="électroniquement_____2",cat=adv]	électroniquement_____2	Default		%default
électronucléairement	100	advm	[pred="électronucléairement_____1",clivee=+,cat=adv]	électronucléairement_____1	Default		%default
électropneumatiquement	100	advm	[pred="électropneumatiquement_____1",clivee=+,cat=adv]	électropneumatiquement_____1	Default		%default
électrothérapiquement	100	advm	[pred="électrothérapiquement_____1",clivee=+,cat=adv]	électrothérapiquement_____1	Default		%default
élitistement	100	advm	[pred="élitistement_____1",clivee=+,detach_neg = +,cat=adv]	élitistement_____1	Default		%default
élogieusement	100	advm	[pred="élogieusement_____1",clivee=+,detach_neg = +,cat=adv]	élogieusement_____1	Default		%default
éloquemment	100	advm	[pred="éloquemment_____1",clivee=+,detach_neg = +,cat=adv]	éloquemment_____1	Default		%default
élégamment	100	advm	[pred="élégamment_____1",clivee=+,detach_neg = +,cat=adv]	élégamment_____1	Default		%default
élégiaquement	100	advm	[pred="élégiaquement_____1",clivee=+,cat=adv]	élégiaquement_____1	Default		%default
élémentairement	100	advm	[pred="élémentairement_____1",clivee=+,cat=adv]	élémentairement_____1	Default		%default
éminemment	100	advm	[pred="éminemment_____1",advi=combien,cat=adv]	éminemment_____1	Default		%default
émotionnellement	100	advm	[pred="émotionnellement_____1",clivee=+,cat=adv]	émotionnellement_____1	Default		%default
énergiquement	100	advm	[pred="énergiquement_____1",clivee=+,detach_neg = +,cat=adv]	énergiquement_____1	Default		%default
énergétiquement	100	advm	[pred="énergétiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	énergétiquement_____1	Default		%default
énigmatiquement	100	advm	[pred="énigmatiquement_____1",clivee=+,detach_neg = +,cat=adv]	énigmatiquement_____1	Default		%default
énormément	100	advm	[pred="énormément_____1",advi=combien,cat=adv]	énormément_____1	Default		%default
épais	100	adv	[pred="épais_____1",cat=adv]	épais_____1	Default		%default
épaissement	100	advm	[pred="épaissement_____1",clivee=+,cat=adv]	épaissement_____1	Default		%default
éparsement	100	advm	[pred="éparsement_____1",clivee=+,cat=adv]	éparsement_____1	Default		%default
épatamment	100	advm	[pred="épatamment_____1",advi=combien,clivee=+,cat=adv]	épatamment_____1	Default		%default
éperdument	100	advm	[pred="éperdument_____1",advi=combien,clivee=+,cat=adv]	éperdument_____1	Default		%default
éphémèrement	100	advm	[pred="éphémèrement_____1",clivee=+,cat=adv]	éphémèrement_____1	Default		%default
épicuriennement	100	advm	[pred="épicuriennement_____1",clivee=+,detach_neg = +,cat=adv]	épicuriennement_____1	Default		%default
épidermiquement	100	advm	[pred="épidermiquement_____1",clivee=+,cat=adv]	épidermiquement_____1	Default		%default
épidémiquement	100	advm	[pred="épidémiquement_____1",clivee=+,cat=adv]	épidémiquement_____1	Default		%default
épigrammatiquement	100	advm	[pred="épigrammatiquement_____1",clivee=+,cat=adv]	épigrammatiquement_____1	Default		%default
épigraphiquement	100	advm	[pred="épigraphiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	épigraphiquement_____1	Default		%default
épigraphiquement	100	advm	[pred="épigraphiquement_____2",clivee=+,cat=adv]	épigraphiquement_____2	Default		%default
épileptiquement	100	advm	[pred="épileptiquement_____1",clivee=+,detach_neg = +,cat=adv]	épileptiquement_____1	Default		%default
épiquement	100	advm	[pred="épiquement_____1",clivee=+,cat=adv]	épiquement_____1	Default		%default
épiscopalement	100	advm	[pred="épiscopalement_____1",clivee=+,cat=adv]	épiscopalement_____1	Default		%default
épisodiquement	100	advm	[pred="épisodiquement_____1",advi=à_quelle_fréquence,clivee=+,detach = +,detach_neg = +,cat=adv]	épisodiquement_____1	Default		%default
épistolairement	100	advm	[pred="épistolairement_____1",clivee=+,cat=adv]	épistolairement_____1	Default		%default
épistémolgiquement	100	advm	[pred="épistémolgiquement_____1",clivee=+,cat=adv]	épistémolgiquement_____1	Default		%default
épistémologiquement	100	advm	[pred="épistémologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	épistémologiquement_____1	Default		%default
épouvantablement	100	advm	[pred="épouvantablement_____1",advi=combien,clivee=+,cat=adv]	épouvantablement_____1	Default		%default
équitablement	100	advm	[pred="équitablement_____1",clivee=+,detach_neg = +,cat=adv]	équitablement_____1	Default		%default
équivalemment	100	advm	[pred="équivalemment_____1",clivee=+,cat=adv]	équivalemment_____1	Default		%default
équivoquement	100	advm	[pred="équivoquement_____1",clivee=+,detach_neg = +,cat=adv]	équivoquement_____1	Default		%default
érotiquement	100	advm	[pred="érotiquement_____1",clivee=+,detach_neg = +,cat=adv]	érotiquement_____1	Default		%default
éruditement	100	advm	[pred="éruditement_____1",clivee=+,detach_neg = +,cat=adv]	éruditement_____1	Default		%default
éruptivement	100	advm	[pred="éruptivement_____1",clivee=+,cat=adv]	éruptivement_____1	Default		%default
érémitiquement	100	advm	[pred="érémitiquement_____1",clivee=+,cat=adv]	érémitiquement_____1	Default		%default
ésotériquement	100	advm	[pred="ésotériquement_____1",clivee=+,detach_neg = +,cat=adv]	ésotériquement_____1	Default		%default
étatiquement	100	advm	[pred="étatiquement_____1",clivee=+,cat=adv]	étatiquement_____1	Default		%default
éternellement	100	advm	[pred="éternellement_____1",advi=en_combien_de_temps,clivee=+,detach_neg = +,cat=adv]	éternellement_____1	Default		%default
éthiquement	100	advm	[pred="éthiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	éthiquement_____1	Default		%default
éthologiquement	100	advm	[pred="éthologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	éthologiquement_____1	Default		%default
éthologiquement	100	advm	[pred="éthologiquement_____2",clivee=+,cat=adv]	éthologiquement_____2	Default		%default
étonnamment	100	advm	[pred="étonnamment_____1",advi=combien,clivee=+,cat=adv]	étonnamment_____1	Default		%default
étonnamment	100	advp	[pred="étonnamment_____2",detach = +,detach_neg = +,cat=adv]	étonnamment_____2	Default		%default
étourdiment	100	advm	[pred="étourdiment_____1",clivee=+,detach_neg = +,cat=adv]	étourdiment_____1	Default		%default
étrangement	100	advm	[pred="étrangement_____1",clivee=+,detach_neg = +,cat=adv]	étrangement_____1	Default		%default
étrangement	100	advp	[pred="étrangement_____2",detach = +,detach_neg = +,cat=adv]	étrangement_____2	Default		%default
étroitement	100	advm	[pred="étroitement_____1",clivee=+,cat=adv]	étroitement_____1	Default		%default
étroitement	100	advm	[pred="étroitement_____2",clivee=+,cat=adv]	étroitement_____2	Default		%default
étymologiquement	100	advm	[pred="étymologiquement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	étymologiquement_____1	Default		%default
étymologiquement	100	advm	[pred="étymologiquement_____2",clivee=+,cat=adv]	étymologiquement_____2	Default		%default
évangéliquement	100	advm	[pred="évangéliquement_____1",clivee=+,cat=adv]	évangéliquement_____1	Default		%default
évasivement	100	advm	[pred="évasivement_____1",clivee=+,detach_neg = +,cat=adv]	évasivement_____1	Default		%default
éventuellement	100	advp	[pred="éventuellement_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	éventuellement_____1	Default		%default
évidemment	100	advp	[pred="évidemment_____1",detach = +,detach_neg = +,modnc = +,cat=adv]	évidemment_____1	Default		%default
évolutivement	100	advm	[pred="évolutivement_____1",adv_parlant = +,clivee=+,detach = +,detach_neg = +,cat=adv]	évolutivement_____1	Default		%default
événementiellement	100	advm	[pred="événementiellement_____1",clivee=+,cat=adv]	événementiellement_____1	Default		%default
ô	100	adv	[pred="ô_____1",cat=adv]	ô_____1	Default		%default
