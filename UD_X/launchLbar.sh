#!/bin/bash

set -e
set -u
set -o pipefail

#listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl no pl pt ro ru sl sme sv tr uk ur vi zh"
listLang="nl no pl pt ro ru sl sv tr uk ur vi zh"

# # Lbar
# for curLang in $listLang
# do
#     oargen -n Lbar_${curLang} -d outputCluster -t 72 -H see4c1 lifnode1 asfalda1 --run ./train.sh -t Lbar -l ${curLang}
#     sleep 180
# done  


# Lbar+W80
for curLang in $listLang
do
    oargen -n Lbar_${curLang}_W80 -d outputCluster -t 72 -H see4c1 lifnode1 asfalda1 --run ./train.sh -t Lbar -l ${curLang} -w 80
    sleep 180
done  
