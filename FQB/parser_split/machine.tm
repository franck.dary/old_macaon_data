Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
signature signature.cla
parser_unlabeled parser_unlabeled.cla
parser_labeled_left parser_labeled_left.cla
parser_labeled_right parser_labeled_right.cla
%STATES
sgn1 signature
parser_unlabeled parser_unlabeled
parser_labeled_left parser_labeled_left
parser_labeled_right parser_labeled_right
%TRANSITIONS
sgn1 parser_unlabeled 0 *
parser_unlabeled parser_labeled_left 0 LEFT
parser_labeled_left parser_unlabeled 0 *
parser_unlabeled parser_labeled_right 0 RIGHT
parser_labeled_right sgn1 +1 *
parser_unlabeled parser_unlabeled 0 REDUCE
parser_unlabeled parser_unlabeled 0 EOS
parser_unlabeled sgn1 +1 SHIFT
