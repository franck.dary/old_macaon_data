#!/usr/bin/python
# -*- coding: utf-8 -*-

# python ../../tools/stats_conllu_copora.py dev.eqToken > dev.csv

from __future__ import print_function          
import fileinput
import sys
import re
import os
import random

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def fillSequences(filepathConllu):
    sequences = dict()
    languages = dict()
    
    with open(filepathConllu, "r", encoding="utf8") as conllu:
        curLine = conllu.readline()[:-1]
        while(not fileCompletelyRead(curLine)):
            curSequence = ""
            while(isInASequence(curLine) and not fileCompletelyRead(curLine)):
                if(lineIsAComment(curLine)):
                    curLine = conllu.readline()[:-1]
                    continue
                curLine = curLine.split("\t")
                curLang = curLine[10]
                idCurTreebank = curLang
                if("_" in curLang):
                    curTreebank = curLang.split("_")[1]
                    curLang = curLang.split("_")[0]
                else:
                    curTreebank = "default"
                if(curLang not in languages):
                    languages[curLang] = dict()
                    languages[curLang][curTreebank] = []
                    sequences[idCurTreebank] = [] 
                if(curTreebank not in languages[curLang]):
                    languages[curLang][curTreebank] = []
                    sequences[idCurTreebank] = []
                curSequence += "\t".join(curLine)+"\n"
                curLine = conllu.readline()[:-1]
                   
            sequences[idCurTreebank].append(curSequence)
            curLine = conllu.readline()[:-1]
        
    return sequences, languages

def nbTokenInSequence(sequence):
    nbTokens = 0
    sequence = sequence.split("\n")
    for line in sequence:
        if("-" in line.split("\t")[0] or line == "\n" or line == ""):
            continue
        nbTokens += 1
    return nbTokens
    
def printStats(languages, Total):
    totalToPrint = False
    print("Languages\tTreebank\tnbTokens\tnbSeq\tlong.moy.seq")
    for curLang in sorted(languages):
        stringToPrint = curLang + "\t"
        for curTreebank in languages[curLang]:
            stringToPrint += curTreebank + "\t"
            for info in languages[curLang][curTreebank]:
                stringToPrint += str(info) + "\t"
            if(curTreebank == "Total"):
                totalToPrint = True
                totalStringToPrint = stringToPrint
            else:
                print(stringToPrint)
            stringToPrint = "\t"
        if(totalToPrint):
            print(totalStringToPrint)
            totalToPrint = False
            
    stringtoPrint = ""
    for info in Total:
        stringToPrint += str(info)+"\t"
    print("Total\t:", stringToPrint)
            
    
def main():
    
    if(len(sys.argv) < 2):
        print("Error, give a conllu file (format with the code treebank in the 11th column)")
        exit(1)
        
    filepathConllu = sys.argv[1]
     
    sequences, languages = fillSequences(filepathConllu)
    
    totalNbTokens = 0
    totalNbSequences = 0
    
    for curLang in sorted(languages):
        totalNbTokensLang = 0
        totalNbSequencesLang = 0
        for curTreebank in languages[curLang]:
            if(curTreebank == "default"):
                idCurTreebank = curLang
            else:
                idCurTreebank = curLang + "_" + curTreebank
            nbTokens = 0
            nbSequences = len(sequences[idCurTreebank])
            totalNbSequencesLang += nbSequences
            totalNbSequences += nbSequences
            for curSequence in sequences[idCurTreebank]:
                nbTokens += nbTokenInSequence(curSequence)
            totalNbTokensLang += nbTokens
            totalNbTokens += nbTokens
            languages[curLang][curTreebank] = [nbTokens, nbSequences, nbTokens*1.0/nbSequences]
        if(len(languages[curLang]) > 1):
           languages[curLang]["Total"] = [totalNbTokensLang, totalNbSequencesLang, totalNbTokensLang*1.0/totalNbSequencesLang]
                
    Total =  [totalNbTokens, totalNbSequences, totalNbTokens*1.0/totalNbSequences]            
    printStats(languages, Total)

main()
        
