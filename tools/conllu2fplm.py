#! /usr/bin/python3

import sys

def printUsageAndExit() :
  print("USAGE : %s file.conllu mcd"%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def sameLineWithoutLemma(l1, l2) :
  l1s = l1.split('\t')
  l2s = l2.split('\t')
  return (l1s[:-3],l1s[-1]) == (l2s[:-3],l2s[-1])

if __name__ == "__main__" :

  sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

  if len(sys.argv) != 3 :
    printUsageAndExit()

  conllMCD = readMCD(sys.argv[2])
  conllMCDr = {v: k for k, v in conllMCD.items()} 

  entriesCount = {}
  entriesList = []

  for line in open(sys.argv[1], "r", encoding="utf8") :
    if len(line.strip()) < 3 :
      continue
    if line.strip()[0] == '#' :
      continue

    columns = line.strip().split('\t')
    if len(columns[int(conllMCDr["ID"])].split('-')) > 1 :
      continue

    entry = ""
    for col in ["FORM", "POS", "LEMMA", "MORPHO"] :
      entry = entry + columns[int(conllMCDr[col])] + '\t'
    entry = entry[:-1]

    if entry not in entriesCount :
      entriesCount[entry] = 1
    else :
      entriesCount[entry] = 1+entriesCount[entry]

  for entry in entriesCount :
    entriesList.append(entry)

  entriesList.sort()
  i = 0
  while i < len(entriesList) :
    maxCount = 0
    maxIndex = 0
    j = i
    while j < len(entriesList) and sameLineWithoutLemma(entriesList[i], entriesList[j]) :
      if entriesCount[entriesList[j]] > maxCount :
        maxCount = entriesCount[entriesList[j]]
        maxIndex = j
      j = j+1
    print("%s"%(entriesList[maxIndex]))
    i = j

