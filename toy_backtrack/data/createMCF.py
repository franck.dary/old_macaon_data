#! /usr/bin/python3

import sys
import random

def printUsageAndExit() :
  print("Usage : %s mcd nbLines seed"%sys.argv[0], file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def main() :
  if len(sys.argv) != 4 :
    printUsageAndExit()

  random.seed(int(sys.argv[3]))

  mcfMCD = readMCD(sys.argv[1])
  mcfMCDr = {v: k for k, v in mcfMCD.items()}

  data = []

  nbLines = int(sys.argv[2])
  for i in range(nbLines) :
    word = random.randint(0,5)
    pos = word + random.randint(-2,2)
    morpho = (pos+word) % 6
    eos = "_"
    if len(data) % 50 == 0 and len(data) > 0 :
      eos = "1"
    data += [[word,pos,morpho,eos]]

  for i in range(len(data)) :
    if random.random() < 0.1 :
      continue

    if data[i][0] <= 1 and i > 0 and data[i-1][3] == "_" :
      data[i][1] = data[i-1][1]

    if data[i][1] >= 3 and i > 0 and data[i-1][3] == "_" :
      data[i][1] = data[i-1][1]

    data[i][2] = (data[i][0] + data[i][1]) % 6

  for i in range(len(data)) :
    if i > 0 and data[i-1][2] == 2 and data[i-1][3] == "_" :
      data[i][2] = 0
      continue
    if i > 0 and data[i-1][2] == 3 and data[i-1][3] == "_":
      data[i][2] = 0
      continue
    if i > 0 and data[i-1][2] < 0 and data[i-1][3] == "_" :
      data[i][2] = 0
      continue

    if i < len(data)-1 and data[i][0] == data[i+1][0] and data[i][3] == "_" :
      data[i][2] = 2
      if i < len(data)-2 and data[i][0] == data[i+2][0] and data[i][3]+data[i+1][3] == "__" :
        data[i][2] = 3
    elif i < len(data)-1 and data[i][1] == data[i+1][1] and data[i][3] == "_" :
      data[i][2] = -2
      if i < len(data)-2 and data[i][1] == data[i+2][1] and data[i][3]+data[i+1][3] == "__" :
        data[i][2] = -3


  for i in range(len(data)) :
    print(data[i][0],end="\t")
    print(data[i][1],end="\t")
    print(data[i][2],end="\t")
    print(data[i][3],end="\n")

if __name__ == "__main__" :
  main()

