#!/bin/bash

set -e
set -u
set -o pipefail

listLang="ar bg ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja ko lv nl no pl pt ro ru sl sv tr uk ur vi zh"

# listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl no pl pt ro ru sl sme sv tr uk ur vi zh"

cd data/
 
sed -i s/^FEAT_ID/#FEAT_ID/ Makefile # ignore ID col 
sed -i -E "s/^COMPLETE_WALS.+/COMPLETE_WALS=\$(WALS_DIR)\/Wdf.csv/" Makefile                

for curLang in $listLang
do
    cat ../../data/multilingual_manon/langToIgnore_all.txt | sed 's/'$curLang'//' > ../../data/multilingual_manon/langToIgnore_test.txt
                                               
    sed -i -E "s/feats_WALS_.+$/feats_WALS_Wd/" Makefile
    make clean && make test.mcf
    mv test.mcf MCFfiles/test/test_L_${curLang}_Wd.mcf

    sed -i -E "s/feats_WALS_.+$/feats_WALS_Wdf/" Makefile 
    make clean && make test.mcf
    mv test.mcf MCFfiles/test/test_L_${curLang}_Wdf.mcf
done  
cd ../
