#! /usr/bin/python3

import sys
import os
import re
import random
from subprocess import Popen
import time
import datetime

def printUsageAndExit() :
  print("Usage : ",sys.argv[0]," templateFolder")
  exit(1)

def getRelevantFilenames() :
  if len(sys.argv) != 2 :
    printUsageAndExit()

  folder = sys.argv[1]

  if not os.path.isdir(folder) :
    print(folder," doesn't exist")
    printUsageAndExit()

  files = os.listdir(folder)

  fms = [f for f in files if re.match(r"(.)*\.fm", f)]
  clas = [f for f in files if re.match(r"(.)*\.cla", f)]
  dicts = [f for f in files if re.match(r"(.)*\.dicts", f)][0]

  return folder, fms, clas, dicts

def generateTopology() :
  activations = ["SIGMOID", "TANH", "RELU", "ELU", "LINEAR", "SPARSEMAX", "CUBE", "SOFTMAX"]
  minNeurons = 10
  maxNeurons = 1000
  maxLayers = 5

  topology = ""
  layers = random.randrange(1, maxLayers+1)
  for i in range(layers) :
    topology += "("+str(random.randrange(minNeurons,maxNeurons))+","+random.choice(activations)+","+str(random.randrange(0, 101)/100)+")"
  return topology

def rewriteClaFile(claFile) :
  f = open(claFile, "r")
  totalFile = ""

  while True :
    line = f.readline();

    if len(line) == 0 :
      break

    if re.match(r"Topology(.)*", line) :
      line = "Topology : " + generateTopology() + "\n"

    totalFile += line

  f.close()

  f = open(claFile, "w")
  f.write(totalFile)
  f.close()

def rewriteDictsFile(dictsFile) :
  firstColSize = 30
  secondColSize = 5
  minNeurons = 1
  maxNeurons = 300

  f = open(dictsFile, "r")
  totalFile = ""

  while True :
    line = f.readline();

    if len(line) == 0 :
      break

    if not re.match(r"#(.)*", line) :
      neurons = str(random.randrange(minNeurons, maxNeurons+1))
      lineSplit = line.split()
      line = lineSplit[0] + (firstColSize-len(lineSplit[0]))*" " + neurons + (secondColSize-len(neurons))*" " + lineSplit[2] + "\n"

    totalFile += line

  f.close()

  f = open(dictsFile, "w")
  f.write(totalFile)
  f.close()

def done(p):
  return p.poll() is not None

def success(p):
  return p.returncode == 0

def launchNewExperiment(folder, fms, clas, dicts, nbIter) :
  time.sleep(1)

  for cla in clas :
    rewriteClaFile(folder+"/"+cla)
  rewriteDictsFile(folder+"/"+dicts)

  date = str(datetime.datetime.now().time())[:8]
  print(date," : Launched experiment ",expCount)

  return Popen(["./train.sh", folder, folder+"_"+str(expCount), "-n", str(nbIter)])

nbConcurentExperiments = 20
nbIter = 20

folder, fms, clas, dicts = getRelevantFilenames()

experiments = []
expCount = 0
while len(experiments) < nbConcurentExperiments :
  experiments.append(launchNewExperiment(folder, fms, clas, dicts, nbIter))
  expCount += 1

while True :  
  time.sleep(5)
  for exp in experiments :
    if done(exp) :
      exp = launchNewExperiment(folder, fms, clas, dicts, nbIter)
      expCount += 1

