#! /usr/bin/python3

import glob
import sys

if __name__ == "__main__" :

  for pathToFile in glob.iglob("" + '*stdout') :
    model = pathToFile.split("_UD_")[0]
    corpus = pathToFile.split("_UD_")[1].split('.')[0]
    for line in open(pathToFile, "r") :
      splited = line.split(' ')
      if len(splited) != 5 :
        print("File \'%s\' bad format."%pathToFile, file=sys.stderr)
        break
      splited = splited[3].split('/')
      if len(splited) < 2 :
        print("File \'%s\' bad format."%pathToFile, file=sys.stderr)
        break
      normal = False
      for part in splited :
        if corpus in part :
          normal = True
          break
      if not normal :
        print("File \'%s\' wrong file for eval."%pathToFile)
      break

