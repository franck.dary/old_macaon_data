#!/usr/bin/python
# -*- coding: utf-8 -*-

#./concatUDlang.py fichiers_config/concatUD_codesLangConcatLang 100 > tmp

from __future__ import print_function          
import fileinput
import sys
import re
import os

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def main():
    
    if(len(sys.argv) != 2):
        print("Error, give a conllu file in which you want to replace punct POS with the lemma")
        exit(1)

    with open(sys.argv[1], "r") as conllu:
        for curLine in conllu:
            if(lineIsAComment(curLine) or not isInASequence(curLine)):
                print(curLine.rstrip())
                continue
            curLine = curLine.rstrip() .split("\t")
            
            if(curLine[3] == "PUNCT"):
                curLine[3] = "PUNCT_"+curLine[2] #on met le lemme à l'emplacement du POS
        
            curLine = "\t".join(curLine)
        
            print(curLine)    
        
        


main()
        
