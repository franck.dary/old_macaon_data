-ce	100	cln	[pred="ce_____1",case=nom]	ce_____1	Default		%default
-ce	100	ce	[pred="ce_____2"]	ce_____2	Default		%default
-elle	100	cln	[pred="cln_____1",case=nom,@3fs]	cln_____1	Default	3fs	%default
-elles	100	cln	[pred="cln_____1",case=nom,@3fp]	cln_____1	Default	3fp	%default
-en	100	clg	[pred="clg_____1",case=gen,clgen=+]	clg_____1	Default		%default
-en	100	cll	[pred="cll_____1",case=loc,clloc=+]	cll_____1	Default		%default
-il	100	cln	[pred="cln_____1",case=nom,@3ms]	cln_____1	Default	3ms	%default
-il	100	ilimp	[imp=+,@3ms]	ilimp_____1	Default	3ms	%default
-ils	100	cln	[pred="cln_____1",case=nom,@3mp]	cln_____1	Default	3mp	%default
-je	100	cln	[pred="cln_____1",case=nom,@1s]	cln_____1	Default	1s	%default
-la	100	cla	[pred="cla_____1",case=acc,@3fs]	cla_____1	La	3fs	%default
-le	100	cla	[pred="cla_____1",case=acc,@3ms]	cla_____1	Le	3ms	%default
-les	100	cla	[pred="cla_____1",case=acc,@3p]	cla_____1	Les	3p	%default
-leur	100	cld	[pred="cld_____1",case=dat,@3p]	cld_____1	Default	3p	%default
-lui	100	cld	[pred="cld_____1",case=dat,@3s]	cld_____1	Default	3s	%default
-m'	100	cld	[pred="cld_____1",case=dat,@1s]	cld_____1	Default	1s	%default
-moi	100	cla	[pred="cla_____1",case=acc,@1s]	cla_____1	Default	1s	%default
-moi	100	cld	[pred="cld_____1",case=dat,@1s]	cld_____1	Default	1s	%default
-nous	100	cla	[pred="cla_____1",case=acc,@1p]	cla_____1	Default	1p	%default
-nous	100	cld	[pred="cld_____1",case=dat,@1p]	cld_____1	Default	1p	%default
-nous	100	cln	[pred="cln_____1",case=nom,@1p]	cln_____1	Default	1p	%default
-on	100	cln	[pred="cln_____1",case=nom,@3s]	cln_____1	Default	3s	%default
-t'	100	cld	[pred="cld_____1",case=dat,@2s]	cld_____1	Default	2s	%default
-t-elle	100	cln	[pred="cln_____1",case=nom,@3fs]	cln_____1	Default	3fs	%default
-t-elles	100	cln	[pred="cln_____1",case=nom,@3fp]	cln_____1	Default	3fp	%default
-t-en	100	cll	[pred="cll_____1",case=loc,clloc=+]	cll_____1	Default		%default
-t-il	100	cln	[pred="cln_____1",case=nom,@3ms]	cln_____1	Default	3ms	%default
-t-il	100	ilimp	[imp=+,@3ms]	ilimp_____1	Default	3ms	%default
-t-ils	100	cln	[pred="cln_____1",case=nom,@3mp]	cln_____1	Default	3mp	%default
-t-on	100	cln	[pred="cln_____1",case=nom,@3s]	cln_____1	Default	3s	%default
-t-y	100	cld	[pred="cld_____1",case=dat]	cld_____1	Default		%default
-toi	100	cla	[pred="cla_____1",case=acc,@2s]	cla_____1	Default	2s	%default
-toi	100	cld	[pred="cld_____1",case=dat,@2s]	cld_____1	Default	2s	%default
-tu	100	cln	[pred="cln_____1",case=nom,@2s]	cln_____1	Default	2s	%default
-vous	100	cla	[pred="cla_____1",case=acc,@2p]	cla_____1	Default	2p	%default
-vous	100	cld	[pred="cld_____1",case=dat,@2p]	cld_____1	Default	2p	%default
-vous	100	cln	[pred="cln_____1",case=nom,@2p]	cln_____1	Default	2p	%default
-vs	100	cla	[pred="cla_____1",case=acc,@2p]	cla_____1	Default	2p	%default
-vs	100	cld	[pred="cld_____1",case=dat,@2p]	cld_____1	Default	2p	%default
-vs	100	cln	[pred="cln_____1",case=nom,@2p]	cln_____1	Default	2p	%default
-y	100	cld	[pred="cld_____1",case=dat]	cld_____1	Default		%default
-y	100	cll	[pred="cll_____1",case=loc,clloc=+]	cll_____1	Default		%default
aucun	100	pro	[pred="aucun_____1<Objde:(de-sn)>",define=-,@ms]	aucun_____1	Default	ms	%default
aucune	100	pro	[pred="aucun_____1<Objde:(de-sn)>",define=-,@fs]	aucun_____1	Default	fs	%default
autre	100	pro	[pred="autre_____1",define=-,@s]	autre_____1	Default	s	%default
autre chose	100	pro	[pred="autre chose_____1",@3m]	autre chose_____1	Default	3m	%default
autres	100	pro	[pred="autre_____1",define=-,@p]	autre_____1	Default	p	%default
autrui	100	pro	[pred="autrui_____1",define=-,@s]	autrui_____1	Default	s	%default
c'	100	cln	[pred="ce_____1",case=nom,@s]	ce_____1	Default	s	%default
c'	100	ce	[pred="ce_____2",@s]	ce_____2	Default	s	%default
ce	100	cln	[pred="ce_____1",case=nom]	ce_____1	Default		%default
ce	100	ce	[pred="ce_____2"]	ce_____2	Default		%default
ceci	100	pro	[pred="ceci_____1",@s]	ceci_____1	Default	s	%default
cela	100	pro	[pred="cela_____1",@s]	cela_____1	Default	s	%default
cela	100	caimp	[imp=+,@3ms]	çaimp_____1	Default	3ms	%default
celle	100	pro	[pred="celui_____1",@fs]	celui_____1	Default	fs	%default
celle-ci	100	pro	[pred="celui-ci_____1",@fs]	celui-ci_____1	Default	fs	%default
celle-là	100	pro	[pred="celui-là_____1",@fs]	celui-là_____1	Default	fs	%default
celles	100	pro	[pred="celui_____1",@fp]	celui_____1	Default	fp	%default
celles-ci	100	pro	[pred="celui-ci_____1",@fp]	celui-ci_____1	Default	fp	%default
celles-là	100	pro	[pred="celui-là_____1",@fp]	celui-là_____1	Default	fp	%default
celui	100	pro	[pred="celui_____1",@ms]	celui_____1	Default	ms	%default
celui-ci	100	pro	[pred="celui-ci_____1",@ms]	celui-ci_____1	Default	ms	%default
celui-là	100	pro	[pred="celui-là_____1",@ms]	celui-là_____1	Default	ms	%default
certaines	100	pro	[pred="certains_____1<Objde:(de-sn)>",define=-,@fp]	certains_____1	Default	fp	%default
certains	100	pro	[pred="certains_____1<Objde:(de-sn)>",define=-,@mp]	certains_____1	Default	mp	%default
ceux	100	pro	[pred="celui_____1",@mp]	celui_____1	Default	mp	%default
ceux-ci	100	pro	[pred="celui-ci_____1",@mp]	celui-ci_____1	Default	mp	%default
ceux-là	100	pro	[pred="celui-là_____1",@mp]	celui-là_____1	Default	mp	%default
ch'	100	cln	[pred="cln_____1",case=nom,@1s]	cln_____1	Default	1s	%default
chacun	100	pro	[pred="chacun_____1<Objde:(de-sn)>",define=-,@m]	chacun_____1	Default	m	%default
chacune	100	pro	[pred="chacun_____1<Objde:(de-sn)>",define=-,@f]	chacun_____1	Default	f	%default
combien	100	pri	[pred="combien?_____1"]	combien?_____1	Default		%default
comment	100	pri	[pred="comment?_____1"]	comment?_____1	Default		%default
dont	100	prel	[pred="dont_____1",@pro_gen]	dont_____1	Default		%default
elle	100	cln	[pred="cln_____1",case=nom,@3fs]	cln_____1	Default	3fs	%default
elle	100	pro	[pred="lui_____1",@3fs]	lui_____1	Default	3fs	%default
elle-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
elles	100	cln	[pred="cln_____1",case=nom,@3fp]	cln_____1	Default	3fp	%default
elles	100	pro	[pred="lui_____1",@3fp]	lui_____1	Default	3fp	%default
elles-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
elles-mêmes	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
en	100	clg	[pred="clg_____1",case=gen,clgen=+]	clg_____1	Default		%default
en	100	cll	[pred="cll_____1",case=loc,clloc=+]	cll_____1	Default		%default
est -ce que	100	pri	[pred="est-ce que?_____1"]	est-ce que?_____1	Default		%default
eux	100	pro	[pred="lui_____1",@3mp]	lui_____1	Default	3mp	%default
eux-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
eux-mêmes	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
icelle	100	pro	[pred="icelui_____1",@fs]	icelui_____1	Default	fs	%default
icelles	100	pro	[pred="icelui_____1",@fp]	icelui_____1	Default	fp	%default
icelui	100	pro	[pred="icelui_____1",@ms]	icelui_____1	Default	ms	%default
iceux	100	pro	[pred="icelui_____1",@mp]	icelui_____1	Default	mp	%default
il	100	cln	[pred="cln_____1",case=nom,@3ms]	cln_____1	Default	3ms	%default
il	100	ilimp	[imp=+,@3ms]	ilimp_____1	Default	3ms	%default
ils	100	cln	[pred="cln_____1",case=nom,@3mp]	cln_____1	Default	3mp	%default
j'	100	cln	[pred="cln_____1",case=nom,@1s]	cln_____1	Default	1s	%default
je	100	cln	[pred="cln_____1",case=nom,@1s]	cln_____1	Default	1s	%default
l'	100	cla	[pred="cla_____1",case=acc,@3fs]	cla_____1	La	3fs	%default
l'	100	cla	[pred="cla_____1",case=acc,@3ms]	cla_____1	Le	3ms	%default
l'autre	100	pro	[pred="l'autre_____1<Objde:(de-sn)>",define=-,@s]	l'autre_____1	Default	s	%default
l'on	100	cln	[pred="cln_____1",case=nom,@3s]	cln_____1	Default	3s	%default
l'un	100	pro	[pred="l'un_____1<Objde:(de-sn)>",define=-,@ms]	l'un_____1	Default	ms	%default
l'un et l'autre	100	pro	[pred="l'un et l'autre_____1",@3mp]	l'un et l'autre_____1	Default	3mp	%default
l'un ou l'autre	100	pro	[pred="l'un ou l'autre_____1",@3mp]	l'un ou l'autre_____1	Default	3mp	%default
l'une	100	pro	[pred="l'un_____1<Objde:(de-sn)>",define=-,@fs]	l'un_____1	Default	fs	%default
l'une et l'autre	100	pro	[pred="l'un et l'autre_____1",@3fp]	l'un et l'autre_____1	Default	3fp	%default
l'une ou l'autre	100	pro	[pred="l'un ou l'autre_____1",@3fp]	l'un ou l'autre_____1	Default	3fp	%default
la	100	cla	[pred="cla_____1",case=acc,@3fs]	cla_____1	La	3fs	%default
la leur	100	pro	[pred="le sien_____1",@fs_P3p]	le sien_____1	Default	fs_P3p	%default
la mienne	100	pro	[pred="le sien_____1",@fs_P1s]	le sien_____1	Default	fs_P1s	%default
la nôtre	100	pro	[pred="le sien_____1",@fs_P1p]	le sien_____1	Default	fs_P1p	%default
la plupart	100	pro	[pred="la plupart_____1",@3]	la plupart_____1	Default	3	%default
la sienne	100	pro	[pred="le sien_____1",@fs_P3s]	le sien_____1	Default	fs_P3s	%default
la tienne	100	pro	[pred="le sien_____1",@fs_P2s]	le sien_____1	Default	fs_P2s	%default
la vôtre	100	pro	[pred="le sien_____1",@fs_P2p]	le sien_____1	Default	fs_P2p	%default
laquelle	100	prel	[pred="lequel_____1",@pro_nom,@fs]	lequel_____1	Default	fs	%default
laquelle	100	pri	[pred="lequel_____2",@pro_nom,@fs]	lequel_____2	Default	fs	%default
laquelle	100	pri	[pred="lequel_____3",@pro_acc,@fs]	lequel_____3	Default	fs	%default
le	100	cla	[pred="cla_____1",case=acc,@3ms]	cla_____1	Le	3ms	%default
le leur	100	pro	[pred="le sien_____1",@ms_P3p]	le sien_____1	Default	ms_P3p	%default
le mien	100	pro	[pred="le sien_____1",@ms_P1s]	le sien_____1	Default	ms_P1s	%default
le nôtre	100	pro	[pred="le sien_____1",@ms_P1p]	le sien_____1	Default	ms_P1p	%default
le sien	100	pro	[pred="le sien_____1",@ms_P3s]	le sien_____1	Default	ms_P3s	%default
le tien	100	pro	[pred="le sien_____1",@ms_P2s]	le sien_____1	Default	ms_P2s	%default
le vôtre	100	pro	[pred="le sien_____1",@ms_P2p]	le sien_____1	Default	ms_P2p	%default
lequel	100	prel	[pred="lequel_____1",@pro_nom,@ms]	lequel_____1	Default	ms	%default
lequel	100	pri	[pred="lequel_____2",@pro_nom,@ms]	lequel_____2	Default	ms	%default
lequel	100	pri	[pred="lequel_____3",@pro_acc,@ms]	lequel_____3	Default	ms	%default
les	100	cla	[pred="cla_____1",case=acc,@3p]	cla_____1	Les	3p	%default
les autres	100	pro	[pred="l'autre_____1<Objde:(de-sn)>",define=-,@p]	l'autre_____1	Default	p	%default
les leurs	100	pro	[pred="le sien_____1",@fp_P3p]	le sien_____1	Default	fp_P3p	%default
les leurs	100	pro	[pred="le sien_____1",@mp_P3p]	le sien_____1	Default	mp_P3p	%default
les miennes	100	pro	[pred="le sien_____1",@fp_P1s]	le sien_____1	Default	fp_P1s	%default
les miens	100	pro	[pred="le sien_____1",@mp_P1s]	le sien_____1	Default	mp_P1s	%default
les nôtres	100	pro	[pred="le sien_____1",@fp_P1p]	le sien_____1	Default	fp_P1p	%default
les nôtres	100	pro	[pred="le sien_____1",@mp_P1p]	le sien_____1	Default	mp_P1p	%default
les siennes	100	pro	[pred="le sien_____1",@fp_P3s]	le sien_____1	Default	fp_P3s	%default
les siens	100	pro	[pred="le sien_____1",@mp_P3s]	le sien_____1	Default	mp_P3s	%default
les tiennes	100	pro	[pred="le sien_____1",@fp_P2s]	le sien_____1	Default	fp_P2s	%default
les tiens	100	pro	[pred="le sien_____1",@mp_P2s]	le sien_____1	Default	mp_P2s	%default
les unes	100	pro	[pred="l'un_____1<Objde:(de-sn)>",define=-,@fp]	l'un_____1	Default	fp	%default
les uns	100	pro	[pred="l'un_____1<Objde:(de-sn)>",define=-,@mp]	l'un_____1	Default	mp	%default
les vôtres	100	pro	[pred="le sien_____1",@fp_P2p]	le sien_____1	Default	fp_P2p	%default
les vôtres	100	pro	[pred="le sien_____1",@mp_P2p]	le sien_____1	Default	mp_P2p	%default
lesquelles	100	prel	[pred="lequel_____1",@pro_nom,@fp]	lequel_____1	Default	fp	%default
lesquelles	100	pri	[pred="lequel_____2",@pro_nom,@fp]	lequel_____2	Default	fp	%default
lesquelles	100	pri	[pred="lequel_____3",@pro_acc,@fp]	lequel_____3	Default	fp	%default
lesquels	100	prel	[pred="lequel_____1",@pro_nom,@mp]	lequel_____1	Default	mp	%default
lesquels	100	pri	[pred="lequel_____2",@pro_nom,@mp]	lequel_____2	Default	mp	%default
lesquels	100	pri	[pred="lequel_____3",@pro_acc,@mp]	lequel_____3	Default	mp	%default
leur	100	cld	[pred="cld_____1",case=dat,@3p]	cld_____1	Default	3p	%default
lui	100	cld	[pred="cld_____1",case=dat,@3s]	cld_____1	Default	3s	%default
lui	100	pro	[pred="lui_____1",@3ms]	lui_____1	Default	3ms	%default
lui-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
m'	100	cla	[pred="cla_____1",case=acc,@1s]	cla_____1	Default	1s	%default
m'	100	cld	[pred="cld_____1",case=dat,@1s]	cld_____1	Default	1s	%default
m'	100	clr	[refl=+,@1s]	clr_____1	Default	1s	%default
me	100	cla	[pred="cla_____1",case=acc,@1s]	cla_____1	Default	1s	%default
me	100	cld	[pred="cld_____1",case=dat,@1s]	cld_____1	Default	1s	%default
me	100	clr	[refl=+,@1s]	clr_____1	Default	1s	%default
moi	100	cla	[pred="cla_____1",case=acc,@1s]	cla_____1	Default	1s	%default
moi	100	cld	[pred="cld_____1",case=dat,@1s]	cld_____1	Default	1s	%default
moi	100	pro	[pred="lui_____1",@1s]	lui_____1	Default	1s	%default
moi-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
même	100	pro	[pred="même_____1",define=-,@s]	même_____1	Default	s	%default
mêmes	100	pro	[pred="même_____1",define=-,@p]	même_____1	Default	p	%default
n'importe où	100	pro	[pred="n'importe où_____1"]	n'importe où_____1	Default		%default
ni l'un ni l'autre	100	pro	[pred="ni l'un ni l'autre_____1",@3mp]	ni l'un ni l'autre_____1	Default	3mp	%default
ni l'une ni l'autre	100	pro	[pred="ni l'un ni l'autre_____1",@3fp]	ni l'un ni l'autre_____1	Default	3fp	%default
nous	100	cla	[pred="cla_____1",case=acc,@1p]	cla_____1	Default	1p	%default
nous	100	cld	[pred="cld_____1",case=dat,@1p]	cld_____1	Default	1p	%default
nous	100	cln	[pred="cln_____1",case=nom,@1p]	cln_____1	Default	1p	%default
nous	100	clr	[refl=+,@1p]	clr_____1	Default	1p	%default
nous	100	pro	[pred="lui_____1",@1p]	lui_____1	Default	1p	%default
nous-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
nous-mêmes	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
nul	100	pro	[pred="nul_____1<Objde:(de-sn)>",define=-,@ms]	nul_____1	Default	ms	%default
nul	100	pro	[pred="nul_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@ms]	nul_____2	Default	ms	%default
nulle	100	pro	[pred="nul_____1<Objde:(de-sn)>",define=-,@fs]	nul_____1	Default	fs	%default
nulle	100	pro	[pred="nul_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@fs]	nul_____2	Default	fs	%default
on	100	cln	[pred="cln_____1",case=nom,@3s]	cln_____1	Default	3s	%default
où	100	pri	[pred="où?_____1",@pro_loc]	où?_____1	Default		%default
où	100	prel	[pred="où_____1",@pro_loc]	où_____1	Default		%default
pas grand-chose	100	pro	[pred="pas grand-chose_____1<Objde:(de-sn)>",define=-,@s]	pas grand-chose_____1	Default	s	%default
pas grand-chose	100	pro	[pred="pas grand-chose_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@s]	pas grand-chose_____2	Default	s	%default
personne	100	pro	[pred="personne_____1<Objde:(de-sn)>",define=-,@s]	personne_____1	Default	s	%default
personne	100	pro	[pred="personne_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@s]	personne_____2	Default	s	%default
peu de chose	100	pro	[pred="peu de chose_____1",@3]	peu de chose_____1	Default	3	%default
pkoi	100	pri	[pred="pourquoi?_____1"]	pourquoi?_____1	Default		%default
plus d'un	100	pro	[pred="plus d'un_____1<Objde:(de-sn)>",define=-,@m]	plus d'un_____1	Default	m	%default
plus d'un	100	pro	[pred="plus d'un_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@m]	plus d'un_____2	Default	m	%default
plus d'une	100	pro	[pred="plus d'un_____1<Objde:(de-sn)>",define=-,@f]	plus d'un_____1	Default	f	%default
plus d'une	100	pro	[pred="plus d'un_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@f]	plus d'un_____2	Default	f	%default
plusieurs	100	pro	[pred="plusieurs_____1<Objde:(de-sn)>",define=-]	plusieurs_____1	Default		%default
plusieurs	100	pro	[pred="plusieurs_____1<Objde:(de-sn)>",define=-,@p]	plusieurs_____1	Default	p	%default
plusieurs	100	pro	[pred="plusieurs_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-]	plusieurs_____2	Default		%default
plusieurs	100	pro	[pred="plusieurs_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@p]	plusieurs_____2	Default	p	%default
pourquoi	100	pri	[pred="pourquoi?_____1"]	pourquoi?_____1	Default		%default
qqc	100	pro	[pred="quelque chose_____1<Objde:(de-sn)>",define=-,@ms]	quelque chose_____1	Default	ms	%default
qqc	100	pro	[pred="quelque chose_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@ms]	quelque chose_____2	Default	ms	%default
qqch	100	pro	[pred="quelque chose_____1<Objde:(de-sn)>",define=-,@ms]	quelque chose_____1	Default	ms	%default
qqch	100	pro	[pred="quelque chose_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@ms]	quelque chose_____2	Default	ms	%default
qqchose	100	pro	[pred="quelque chose_____1<Objde:(de-sn)>",define=-,@ms]	quelque chose_____1	Default	ms	%default
qqchose	100	pro	[pred="quelque chose_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@ms]	quelque chose_____2	Default	ms	%default
qqn	100	pro	[pred="quelqu'un_____1<Objde:(de-sn)>",define=-,@s]	quelqu'un_____1	Default	s	%default
qqn	100	pro	[pred="quelqu'un_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@s]	quelqu'un_____2	Default	s	%default
qu'	100	pri	[pred="que?_____1",@pro_acc]	que?_____1	Default		%default
qu'	100	prel	[pred="que_____1",@pro_acc]	que_____1	Default		%default
qu' est -ce que	100	pri	[pred="qu'est-ce que?_____1",@pro_acc]	qu'est-ce que?_____1	Default		%default
quand	100	pri	[pred="quand?_____1"]	quand?_____1	Default		%default
que	100	pri	[pred="que?_____1",@pro_acc]	que?_____1	Default		%default
que	100	prel	[pred="que_____1",@pro_acc]	que_____1	Default		%default
quelqu'un	100	pro	[pred="quelqu'un_____1<Objde:(de-sn)>",define=-,@s]	quelqu'un_____1	Default	s	%default
quelqu'un	100	pro	[pred="quelqu'un_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@s]	quelqu'un_____2	Default	s	%default
quelqu'une	100	pro	[pred="quelqu'un_____1<Objde:(de-sn)>",define=-,@fs]	quelqu'un_____1	Default	fs	%default
quelqu'une	100	pro	[pred="quelqu'un_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@fs]	quelqu'un_____2	Default	fs	%default
quelque chose	100	pro	[pred="quelque chose_____1<Objde:(de-sn)>",define=-,@ms]	quelque chose_____1	Default	ms	%default
quelque chose	100	pro	[pred="quelque chose_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@ms]	quelque chose_____2	Default	ms	%default
quelques-unes	100	pro	[pred="quelques-uns_____1<Objde:(de-sn)>",define=-,@fp]	quelques-uns_____1	Default	fp	%default
quelques-uns	100	pro	[pred="quelques-uns_____1<Objde:(de-sn)>",define=-,@fp]	quelques-uns_____1	Default	fp	%default
qui	100	pri	[pred="quiAcc?_____1",@pro_acc]	quiAcc?_____1	Default		%default
qui	100	pri	[pred="quiNom?_____1",@pro_nom]	quiNom?_____1	Default		%default
qui	100	prel	[pred="qui_____1",@pro_nom]	qui_____1	Default		%default
qui de droit	100	pro	[pred="qui de droit_____1",@3s]	qui de droit_____1	Default	3s	%default
qui est -ce qui	100	pri	[pred="qui est-ce qui?_____1",@pro_nom]	qui est-ce qui?_____1	Default		%default
qui que ce soit	100	pro	[pred="qui que ce soit_____1",@3s]	qui que ce soit_____1	Default	3s	%default
quiconque	100	pro	[pred="quiconque_____1<Objde:(de-sn)>",define=-,@s]	quiconque_____1	Default	s	%default
quiconque	100	pro	[pred="quiconque_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@s]	quiconque_____2	Default	s	%default
quoi	100	pri	[pred="quoi?_____1"]	quoi?_____1	Default		%default
quoi	100	prel	[pred="quoi_____1"]	quoi_____1	Default		%default
rien	100	pro	[pred="rien_____1<Objde:(de-sn)>",define=-,@s]	rien_____1	Default	s	%default
rien	100	pro	[pred="rien_____2<Att:(de-sa),Obl:(parmi-sn)>",define=-,@s]	rien_____2	Default	s	%default
s'	100	clar	[pred="clar_____1",case=acc,@3p]	clar_____1	Default	3p	%default
s'	100	clar	[pred="clar_____1",case=acc,@3s]	clar_____1	Default	3s	%default
s'	100	cldr	[pred="cldr_____1",case=dat,@3p]	cldr_____1	Default	3p	%default
s'	100	cldr	[pred="cldr_____1",case=dat,@3s]	cldr_____1	Default	3s	%default
s'	100	clr	[refl=+,@3p]	clr_____1	Default	3p	%default
s'	100	clr	[refl=+,@3s]	clr_____1	Default	3s	%default
se	100	clar	[pred="clar_____1",case=acc,@3p]	clar_____1	Default	3p	%default
se	100	clar	[pred="clar_____1",case=acc,@3s]	clar_____1	Default	3s	%default
se	100	cldr	[pred="cldr_____1",case=dat,@3p]	cldr_____1	Default	3p	%default
se	100	cldr	[pred="cldr_____1",case=dat,@3s]	cldr_____1	Default	3s	%default
se	100	clr	[refl=+,@3p]	clr_____1	Default	3p	%default
se	100	clr	[refl=+,@3s]	clr_____1	Default	3s	%default
soi	100	pro	[pred="lui_____1",@3]	lui_____1	Default	3	%default
soi-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
t'	100	cla	[pred="cla_____1",case=acc,@2s]	cla_____1	Default	2s	%default
t'	100	cld	[pred="cld_____1",case=dat,@2s]	cld_____1	Default	2s	%default
t'	100	cln	[pred="cln_____1",case=nom,@2s]	cln_____1	Default	2s	%default
t'	100	clr	[refl=+,@2s]	clr_____1	Default	2s	%default
te	100	cla	[pred="cla_____1",case=acc,@2s]	cla_____1	Default	2s	%default
te	100	cld	[pred="cld_____1",case=dat,@2s]	cld_____1	Default	2s	%default
te	100	clr	[refl=+,@2s]	clr_____1	Default	2s	%default
tel	100	pro	[pred="tel_____1",define=-,@ms]	tel_____1	Default	ms	%default
tel et tel	100	pro	[pred="tel et tel_____1",@3ms]	tel et tel_____1	Default	3ms	%default
tel ou tel	100	pro	[pred="tel ou tel_____1",@3ms]	tel ou tel_____1	Default	3ms	%default
telle	100	pro	[pred="tel_____1",define=-,@fs]	tel_____1	Default	fs	%default
telle et telle	100	pro	[pred="tel et tel_____1",@3fs]	tel et tel_____1	Default	3fs	%default
telle ou telle	100	pro	[pred="tel ou tel_____1",@3fs]	tel ou tel_____1	Default	3fs	%default
telles	100	pro	[pred="tel_____1",define=-,@fp]	tel_____1	Default	fp	%default
telles et telles	100	pro	[pred="tel et tel_____1",@3fp]	tel et tel_____1	Default	3fp	%default
telles ou telles	100	pro	[pred="tel ou tel_____1",@3fp]	tel ou tel_____1	Default	3fp	%default
tels	100	pro	[pred="tel_____1",define=-,@mp]	tel_____1	Default	mp	%default
tels et tels	100	pro	[pred="tel et tel_____1",@3mp]	tel et tel_____1	Default	3mp	%default
tels ou tels	100	pro	[pred="tel ou tel_____1",@3mp]	tel ou tel_____1	Default	3mp	%default
toi	100	cla	[pred="cla_____1",case=acc,@2s]	cla_____1	Default	2s	%default
toi	100	cld	[pred="cld_____1",case=dat,@2s]	cld_____1	Default	2s	%default
toi	100	pro	[pred="lui_____1",@2s]	lui_____1	Default	2s	%default
toi-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
tous	100	pro	[pred="tout_____1",define=-,@mp]	tout_____1	Default	mp	%default
tout	100	pro	[pred="tout_____1",define=-,@ms]	tout_____1	Default	ms	%default
tout le monde	100	pro	[pred="tout le monde_____1",@3ms]	tout le monde_____1	Default	3ms	%default
toute	100	pro	[pred="tout_____1",define=-,@fs]	tout_____1	Default	fs	%default
toutes	100	pro	[pred="tout_____1",define=-,@fp]	tout_____1	Default	fp	%default
tu	100	cln	[pred="cln_____1",case=nom,@2s]	cln_____1	Default	2s	%default
un	100	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@ms]	un_____1	Default	ms	%default
une	100	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@fs]	un_____1	Default	fs	%default
unes	100	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@fp]	un_____1	Default	fp	%default
uns	100	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@mp]	un_____1	Default	mp	%default
vous	100	cla	[pred="cla_____1",case=acc,@2p]	cla_____1	Default	2p	%default
vous	100	cld	[pred="cld_____1",case=dat,@2p]	cld_____1	Default	2p	%default
vous	100	cln	[pred="cln_____1",case=nom,@2p]	cln_____1	Default	2p	%default
vous	100	clr	[refl=+,@2p]	clr_____1	Default	2p	%default
vous	100	pro	[pred="lui_____1",@2p]	lui_____1	Default	2p	%default
vous-même	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
vous-mêmes	100	pro	[pred="lui-même_____1"]	lui-même_____1	Default		%default
vs	100	cla	[pred="cla_____1",case=acc,@2p]	cla_____1	Default	2p	%default
vs	100	cld	[pred="cld_____1",case=dat,@2p]	cld_____1	Default	2p	%default
vs	100	cln	[pred="cln_____1",case=nom,@2p]	cln_____1	Default	2p	%default
vs	100	clr	[refl=+,@2p]	clr_____1	Default	2p	%default
y	100	cld	[pred="cld_____1",case=dat]	cld_____1	Default		%default
y	100	cll	[pred="cll_____1",case=loc,clloc=+]	cll_____1	Default		%default
ça	100	pro	[pred="cela_____1",@ms]	cela_____1	Default	ms	%default
ça	100	caimp	[imp=+,@3ms]	çaimp_____1	Default	3ms	%default
