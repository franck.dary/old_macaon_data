Dnum an	100	cf	[pred="donner Dnum an_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner Dnum an_____1	Default		%default
Dnum jour	100	cf	[pred="donner Dnum jour_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner Dnum jour_____1	Default		%default
Dnum mois	100	cf	[pred="donner Dnum mois_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner Dnum mois_____1	Default		%default
Dnum semaine	100	cf	[pred="donner Dnum semaine_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner Dnum semaine_____1	Default		%default
abri	100	cf	[pred="chercher abri_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=chercher]	chercher abri_____1	Default		%default
abri	100	cf	[pred="demander abri_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander abri_____1	Default		%default
abri	100	cf	[pred="donner abri_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner abri_____1	Default		%default
abri	100	cf	[pred="trouver abri_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver abri_____1	Default		%default
acte d'allégeance	100	cf	[pred="faire acte d'allégeance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire acte d'allégeance_____1	Default		%default
adieu	100	cf	[pred="dire adieu_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire adieu_____1	Default		%default
adieu	100	cf	[pred="dire adieu_____2<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire adieu_____2	Default		%default
affaire	100	cf	[pred="causer affaire_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=causer]	causer affaire_____1	Default		%default
affaire	100	cf	[pred="faire affaire_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire affaire_____1	Default		%default
affaire	100	cf	[pred="parler affaire_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=parler]	parler affaire_____1	Default		%default
affirmativement	100	cf	[pred="répondre affirmativement_____1<Suj:cln|sn,Objà:à-sn>",synt_head=répondre]	répondre affirmativement_____1	Default		%default
affront	100	cf	[pred="faire affront_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire affront_____1	Default		%default
aide	100	cf	[pred="demander aide_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander aide_____1	Default		%default
alliance	100	cf	[pred="faire alliance_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire alliance_____1	Default		%default
allégeance	100	cf	[pred="faire allégeance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire allégeance_____1	Default		%default
amende honorable	100	cf	[pred="faire amende honorable_____1<Suj:cln|sn,Obl:auprès_de-sn>",synt_head=faire]	faire amende honorable_____1	Default		%default
amitié	100	cf	[pred="jurer amitié_____1<Suj:cln|sn,Objà:à-sn>",synt_head=jurer]	jurer amitié_____1	Default		%default
amitié	100	cf	[pred="lier amitié_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=lier]	lier amitié_____1	Default		%default
appel	100	cf	[pred="faire appel_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire appel_____1	Default		%default
appel	100	cf	[pred="interjeter appel_____1<Suj:cln|sn,Obl:auprès_de-sn>",synt_head=interjeter]	interjeter appel_____1	Default		%default
appui	100	cf	[pred="prendre appui_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=prendre]	prendre appui_____1	Default		%default
appui	100	cf	[pred="trouver appui_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver appui_____1	Default		%default
asile	100	cf	[pred="chercher asile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=chercher]	chercher asile_____1	Default		%default
asile	100	cf	[pred="demander asile_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander asile_____1	Default		%default
asile	100	cf	[pred="donner asile_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner asile_____1	Default		%default
asile	100	cf	[pred="prendre asile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre asile_____1	Default		%default
asile	100	cf	[pred="trouver asile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver asile_____1	Default		%default
assaut de amabilité	100	cf	[pred="faire assaut de amabilité_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire assaut de amabilité_____1	Default		%default
assaut de politesse	100	cf	[pred="faire assaut de politesse_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire assaut de politesse_____1	Default		%default
assise	100	cf	[pred="prendre assise_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=prendre]	prendre assise_____1	Default		%default
assistance	100	cf	[pred="demander assistance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander assistance_____1	Default		%default
assistance	100	cf	[pred="porter assistance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter assistance_____1	Default		%default
assistance	100	cf	[pred="prêter assistance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=prêter]	prêter assistance_____1	Default		%default
assistance	100	cf	[pred="trouver assistance_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver assistance_____1	Default		%default
atteinte	100	cf	[pred="porter atteinte_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter atteinte_____1	Default		%default
audience	100	cf	[pred="demander audience_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander audience_____1	Default		%default
audience	100	cf	[pred="donner audience_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner audience_____1	Default		%default
autorité	100	cf	[pred="faire autorité_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire autorité_____1	Default		%default
avis	100	cf	[pred="prendre avis_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre avis_____1	Default		%default
barrage	100	cf	[pred="faire barrage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire barrage_____1	Default		%default
beaucoup	100	cf	[pred="compter beaucoup_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=compter]	compter beaucoup_____1	Default		%default
beaucoup	100	cf	[pred="en savoir beaucoup_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir beaucoup_____1	Default		%default
beaucoup	100	cf	[pred="espérer beaucoup_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=espérer]	espérer beaucoup_____1	Default		%default
beaucoup	100	cf	[pred="mériter beaucoup_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mériter]	mériter beaucoup_____1	Default		%default
beaucoup	100	cf	[pred="pouvoir beaucoup_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=pouvoir]	pouvoir beaucoup_____1	Default		%default
beaucoup	100	cf	[pred="signifier beaucoup_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=signifier]	signifier beaucoup_____1	Default		%default
beaucoup de honneur	100	cf	[pred="faire beaucoup de honneur_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire beaucoup de honneur_____1	Default		%default
belle	100	cf	[pred="la bailler belle_____1<Suj:cln|sn,Objà:à-sn>",synt_head=bailler]	la bailler belle_____1	Default		%default
bien	100	cf	[pred="le rendre bien_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	le rendre bien_____1	Default		%default
bien	100	cf	[pred="tomber bien_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=tomber]	tomber bien_____1	Default		%default
bien des choses	100	cf	[pred="souhaiter bien des choses_____1<Suj:cln|sn,Objà:à-sn>",synt_head=souhaiter]	souhaiter bien des choses_____1	Default		%default
bien du plaisir	100	cf	[pred="souhaiter bien du plaisir_____1<Suj:cln|sn,Objà:à-sn>",synt_head=souhaiter]	souhaiter bien du plaisir_____1	Default		%default
bloc	100	cf	[pred="faire bloc_____1<Suj:cln|sn,Obl:autour_de-sn>",synt_head=faire]	faire bloc_____1	Default		%default
bobo	100	cf	[pred="faire bobo_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire bobo_____1	Default		%default
bon	100	cf	[pred="tenir bon_____1<Suj:cln|sn,Obl:contre-sn>",synt_head=tenir]	tenir bon_____1	Default		%default
bon accueil	100	cf	[pred="faire bon accueil_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire bon accueil_____1	Default		%default
bon effet	100	cf	[pred="faire bon effet_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire bon effet_____1	Default		%default
bon ménage	100	cf	[pred="faire bon ménage_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire bon ménage_____1	Default		%default
bon usage	100	cf	[pred="faire bon usage_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire bon usage_____1	Default		%default
bonheur	100	cf	[pred="porter bonheur_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter bonheur_____1	Default		%default
bonjour	100	cf	[pred="dire bonjour_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire bonjour_____1	Default		%default
bonne chance	100	cf	[pred="souhaiter bonne chance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=souhaiter]	souhaiter bonne chance_____1	Default		%default
bonne contenance	100	cf	[pred="faire bonne contenance_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=faire]	faire bonne contenance_____1	Default		%default
bonne garde	100	cf	[pred="faire bonne garde_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire bonne garde_____1	Default		%default
bonne impression	100	cf	[pred="faire bonne impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire bonne impression_____1	Default		%default
bonne mesure	100	cf	[pred="faire bonne mesure_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire bonne mesure_____1	Default		%default
bonne mine	100	cf	[pred="faire bonne mine_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire bonne mine_____1	Default		%default
bonne nuit	100	cf	[pred="dire bonne nuit_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire bonne nuit_____1	Default		%default
bonne soirée	100	cf	[pred="dire bonne soirée_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire bonne soirée_____1	Default		%default
bonsoir	100	cf	[pred="dire bonsoir_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire bonsoir_____1	Default		%default
boutique	100	cf	[pred="causer boutique_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=causer]	causer boutique_____1	Default		%default
boutique	100	cf	[pred="ouvrir boutique_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=ouvrir]	ouvrir boutique_____1	Default		%default
boutique	100	cf	[pred="ouvrir boutique_____2<Suj:cln|sn,Loc:loc-sn|y>",synt_head=ouvrir]	ouvrir boutique_____2	Default		%default
boutique	100	cf	[pred="parler boutique_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=parler]	parler boutique_____1	Default		%default
boutique	100	cf	[pred="tenir boutique_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=tenir]	tenir boutique_____1	Default		%default
bézef	100	cf	[pred="n' en apprendre bézef_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=apprendre]	n' en apprendre bézef_____1	Default		%default
bézef	100	cf	[pred="n' en savoir bézef_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=savoir]	n' en savoir bézef_____1	Default		%default
bézef	100	cf	[pred="ne donner bézef_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=donner]	ne donner bézef_____1	Default		%default
camarade	100	cf	[pred="faire camarade_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire camarade_____1	Default		%default
campagne	100	cf	[pred="faire campagne_____1<Suj:cln|sn,Obl:contre-sn>",synt_head=faire]	faire campagne_____1	Default		%default
campagne	100	cf	[pred="faire campagne_____2<Suj:cln|sn,Obl:pour-sn>",synt_head=faire]	faire campagne_____2	Default		%default
campagne	100	cf	[pred="mener campagne_____1<Suj:cln|sn,Obl:contre-sn>",synt_head=mener]	mener campagne_____1	Default		%default
campagne	100	cf	[pred="mener campagne_____2<Suj:cln|sn,Obl:pour-sn>",synt_head=mener]	mener campagne_____2	Default		%default
carrière	100	cf	[pred="faire carrière_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire carrière_____1	Default		%default
casse-cou	100	cf	[pred="crier casse-cou_____1<Suj:cln|sn,Objà:à-sn>",synt_head=crier]	crier casse-cou_____1	Default		%default
ce que Il-0 veut	100	cf	[pred="dire ce que Il-0 veut_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=dire]	dire ce que Il-0 veut_____1	Default		%default
ce que Il-0 veut	100	cf	[pred="faire ce que Il-0 veut_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire ce que Il-0 veut_____1	Default		%default
ce que N gagner	100	cf	[pred="perdre ce que N gagner_____1<Suj:cln|sn,Obl:en-sn>",synt_head=perdre]	perdre ce que N gagner_____1	Default		%default
ce que N perdre	100	cf	[pred="gagner ce que N perdre_____1<Suj:cln|sn,Obl:en-sn>",synt_head=gagner]	gagner ce que N perdre_____1	Default		%default
chance	100	cf	[pred="porter chance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter chance_____1	Default		%default
chaud	100	cf	[pred="coûter chaud_____1<Suj:cln|sn,Objà:à-sn>",synt_head=coûter]	coûter chaud_____1	Default		%default
chaud	100	cf	[pred="tenir chaud_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir chaud_____1	Default		%default
cher	100	cf	[pred="coûter cher_____1<Suj:cln|sn,Objà:à-sn>",synt_head=coûter]	coûter cher_____1	Default		%default
cher	100	cf	[pred="ne donner cher_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=donner]	ne donner cher_____1	Default		%default
cher	100	cf	[pred="revenir cher_____1<Suj:cln|sn,Objà:à-sn>",synt_head=revenir]	revenir cher_____1	Default		%default
chicane	100	cf	[pred="chercher chicane_____1<Suj:cln|sn,Objà:à-sn>",synt_head=chercher]	chercher chicane_____1	Default		%default
chiffons	100	cf	[pred="causer chiffons_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=causer]	causer chiffons_____1	Default		%default
chiffons	100	cf	[pred="parler chiffons_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=parler]	parler chiffons_____1	Default		%default
chorus	100	cf	[pred="faire chorus_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire chorus_____1	Default		%default
chut	100	cf	[pred="faire chut_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire chut_____1	Default		%default
clair	100	cf	[pred="voir clair_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=voir]	voir clair_____1	Default		%default
classe	100	cf	[pred="faire classe_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire classe_____1	Default		%default
commande de N-hum	100	cf	[pred="passer commande de N-hum_____1<Suj:cln|sn,Objà:à-sn>",synt_head=passer]	passer commande de N-hum_____1	Default		%default
commerce	100	cf	[pred="faire commerce_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire commerce_____1	Default		%default
compagnie	100	cf	[pred="fausser compagnie_____1<Suj:cln|sn,Objà:à-sn>",synt_head=fausser]	fausser compagnie_____1	Default		%default
compagnie	100	cf	[pred="tenir compagnie_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir compagnie_____1	Default		%default
compensation	100	cf	[pred="obtenir compensation_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=obtenir]	obtenir compensation_____1	Default		%default
concurrence	100	cf	[pred="faire concurrence_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire concurrence_____1	Default		%default
confiance	100	cf	[pred="inspirer confiance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=inspirer]	inspirer confiance_____1	Default		%default
confiance	100	cf	[pred="perdre confiance_____1<Suj:cln|sn,Obl:en-sn>",synt_head=perdre]	perdre confiance_____1	Default		%default
confiance	100	cf	[pred="porter confiance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter confiance_____1	Default		%default
confiance	100	cf	[pred="rendre confiance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre confiance_____1	Default		%default
confiance	100	cf	[pred="reprendre confiance_____1<Suj:cln|sn,Obl:en-sn>",synt_head=reprendre]	reprendre confiance_____1	Default		%default
confirmation	100	cf	[pred="demander confirmation_____1<Suj:cln|sn,Obl:de_la_part_de-sn>",synt_head=demander]	demander confirmation_____1	Default		%default
confirmation	100	cf	[pred="mériter confirmation_____1<Suj:cln|sn,Obl:de_la_part_de-sn>",synt_head=mériter]	mériter confirmation_____1	Default		%default
confirmation	100	cf	[pred="nécessiter confirmation_____1<Suj:cln|sn,Obl:de_la_part_de-sn>",synt_head=nécessiter]	nécessiter confirmation_____1	Default		%default
congé	100	cf	[pred="donner congé_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner congé_____1	Default		%default
congé	100	cf	[pred="prendre congé_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre congé_____1	Default		%default
connaissance	100	cf	[pred="faire connaissance_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire connaissance_____1	Default		%default
connaissance	100	cf	[pred="lier connaissance_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=lier]	lier connaissance_____1	Default		%default
connaissance	100	cf	[pred="nouer connaissance_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=nouer]	nouer connaissance_____1	Default		%default
connaissance	100	cf	[pred="renouveler connaissance_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=renouveler]	renouveler connaissance_____1	Default		%default
conseil	100	cf	[pred="demander conseil_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander conseil_____1	Default		%default
conseil	100	cf	[pred="tenir conseil_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=tenir]	tenir conseil_____1	Default		%default
considération	100	cf	[pred="demander considération_____1<Suj:cln|sn,Obl:de_la_part_de-sn>",synt_head=demander]	demander considération_____1	Default		%default
constituer partie civile	100	cf	[pred="se constituer partie civile_____1<Suj:cln|sn,Obl:contre-sn>",@pron,@être,synt_head=se]	se constituer partie civile_____1	Default		%default
contact	100	cf	[pred="prendre contact_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	prendre contact_____1	Default		%default
contenance	100	cf	[pred="perdre contenance_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=perdre]	perdre contenance_____1	Default		%default
conversation	100	cf	[pred="lier conversation_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=lier]	lier conversation_____1	Default		%default
copie blanche	100	cf	[pred="rendre copie blanche_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=rendre]	rendre copie blanche_____1	Default		%default
corps	100	cf	[pred="donner corps_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner corps_____1	Default		%default
corps	100	cf	[pred="faire corps_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire corps_____1	Default		%default
coucou	100	cf	[pred="faire coucou_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire coucou_____1	Default		%default
courage	100	cf	[pred="donner courage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner courage_____1	Default		%default
courage	100	cf	[pred="redonner courage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=redonner]	redonner courage_____1	Default		%default
cours	100	cf	[pred="donner cours_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner cours_____1	Default		%default
court	100	cf	[pred="demeurer court_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=demeurer]	demeurer court_____1	Default		%default
court	100	cf	[pred="rester court_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=rester]	rester court_____1	Default		%default
couvert	100	cf	[pred="demeurer couvert_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=demeurer]	demeurer couvert_____1	Default		%default
couvert	100	cf	[pred="rester couvert_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=rester]	rester couvert_____1	Default		%default
créance	100	cf	[pred="trouver créance_____1<Suj:cln|sn,Obl:auprès_de-sn>",synt_head=trouver]	trouver créance_____1	Default		%default
crédit	100	cf	[pred="faire crédit_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire crédit_____1	Default		%default
date	100	cf	[pred="prendre date_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	prendre date_____1	Default		%default
de belles	100	cf	[pred="en apprendre de belles_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=apprendre]	en apprendre de belles_____1	Default		%default
de belles	100	cf	[pred="en conter de belles_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=conter]	en conter de belles_____1	Default		%default
de belles	100	cf	[pred="en savoir de belles_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir de belles_____1	Default		%default
de cadeau	100	cf	[pred="ne faire de cadeau_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=faire]	ne faire de cadeau_____1	Default		%default
de difficulté	100	cf	[pred="ne faire de difficulté_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=faire]	ne faire de difficulté_____1	Default		%default
de doute	100	cf	[pred="ne faire de doute_____1<Suj:cln|sn,Obl:pour-sn>",clneg =c +,synt_head=faire]	ne faire de doute_____1	Default		%default
de drôles	100	cf	[pred="en savoir de drôles_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir de drôles_____1	Default		%default
de drôles	100	cf	[pred="en sortir de drôles_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=sortir]	en sortir de drôles_____1	Default		%default
de l'oeil	100	cf	[pred="faire de l'oeil_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire de l'oeil_____1	Default		%default
de la voix	100	cf	[pred="donner de la voix_____1<Suj:cln|sn,Obl:contre-sn>",synt_head=donner]	donner de la voix_____1	Default		%default
de problème	100	cf	[pred="ne faire de problème_____1<Suj:cln|sn,Obl:pour-sn>",clneg =c +,synt_head=faire]	ne faire de problème_____1	Default		%default
de quartier	100	cf	[pred="ne faire de quartier_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=faire]	ne faire de quartier_____1	Default		%default
de quel bois Il-0 se chauffe	100	cf	[pred="montrer de quel bois Il-0 se chauffe_____1<Suj:cln|sn,Objà:à-sn>",synt_head=montrer]	montrer de quel bois Il-0 se chauffe_____1	Default		%default
dedans	100	cf	[pred="rentrer dedans_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rentrer]	rentrer dedans_____1	Default		%default
des frais	100	cf	[pred="faire des frais_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=faire]	faire des frais_____1	Default		%default
des nouvelles de N	100	cf	[pred="dire des nouvelles de N_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire des nouvelles de N_____1	Default		%default
deux mots	100	cf	[pred="dire deux mots_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire deux mots_____1	Default		%default
diamétralement	100	cf	[pred="s' opposer diamétralement_____1<Suj:cln|sn,Objà:à-sn>",synt_head=opposer]	s' opposer diamétralement_____1	Default		%default
domicile	100	cf	[pred="fixer domicile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=fixer]	fixer domicile_____1	Default		%default
domicile	100	cf	[pred="prendre domicile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre domicile_____1	Default		%default
domicile	100	cf	[pred="élire domicile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=élire]	élire domicile_____1	Default		%default
domicile	100	cf	[pred="établir domicile_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=établir]	établir domicile_____1	Default		%default
dommage	100	cf	[pred="porter dommage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter dommage_____1	Default		%default
double jeu	100	cf	[pred="jouer double jeu_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=jouer]	jouer double jeu_____1	Default		%default
droit	100	cf	[pred="faire droit_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire droit_____1	Default		%default
drôle	100	cf	[pred="faire drôle_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire drôle_____1	Default		%default
drôles	100	cf	[pred="en apprendre de drôles_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=apprendre]	en apprendre de drôles_____1	Default		%default
du Monsieur le Comte	100	cf	[pred="donner du Monsieur le Comte_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner du Monsieur le Comte_____1	Default		%default
du genou	100	cf	[pred="faire du genou_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire du genou_____1	Default		%default
du pied	100	cf	[pred="faire du pied_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire du pied_____1	Default		%default
du plaisir	100	cf	[pred="souhaiter du plaisir_____1<Suj:cln|sn,Objà:à-sn>",synt_head=souhaiter]	souhaiter du plaisir_____1	Default		%default
du plat de la langue	100	cf	[pred="donner du plat de la langue_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner du plat de la langue_____1	Default		%default
du plat de la langue	100	cf	[pred="faire du plat de la langue_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire du plat de la langue_____1	Default		%default
du sel	100	cf	[pred="mettre du sel_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=mettre]	mettre du sel_____1	Default		%default
du vilain	100	cf	[pred="faire du vilain_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire du vilain_____1	Default		%default
effet	100	cf	[pred="prendre effet_____1<Suj:cln|sn,Objà:à-sn>",synt_head=prendre]	prendre effet_____1	Default		%default
escale	100	cf	[pred="faire escale_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire escale_____1	Default		%default
espoir	100	cf	[pred="rendre espoir_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre espoir_____1	Default		%default
excellent effet	100	cf	[pred="faire excellent effet_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire excellent effet_____1	Default		%default
excellent ménage	100	cf	[pred="faire excellent ménage_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire excellent ménage_____1	Default		%default
excellent usage	100	cf	[pred="faire excellent usage_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire excellent usage_____1	Default		%default
excellente impression	100	cf	[pred="faire excellente impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire excellente impression_____1	Default		%default
exception	100	cf	[pred="faire exception_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire exception_____1	Default		%default
exemple	100	cf	[pred="prendre exemple_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=prendre]	prendre exemple_____1	Default		%default
face	100	cf	[pred="faire face_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire face_____1	Default		%default
face	100	cf	[pred="faire face_____2<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire face_____2	Default		%default
faible	100	cf	[pred="tomber faible_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=tomber]	tomber faible_____1	Default		%default
faire petit	100	cf	[pred="se faire petit_____1<Suj:cln|sn,Obl:devant-sn>",@pron,@être,synt_head=se]	se faire petit_____1	Default		%default
faute	100	cf	[pred="faire faute_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire faute_____1	Default		%default
faux bond	100	cf	[pred="faire faux bond_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire faux bond_____1	Default		%default
faveur	100	cf	[pred="trouver faveur_____1<Suj:cln|sn,Obl:auprès_de-sn>",synt_head=trouver]	trouver faveur_____1	Default		%default
feu	100	cf	[pred="faire feu_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire feu_____1	Default		%default
feu	100	cf	[pred="prendre feu_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=prendre]	prendre feu_____1	Default		%default
feuille blanche	100	cf	[pred="rendre feuille blanche_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=rendre]	rendre feuille blanche_____1	Default		%default
fidélité	100	cf	[pred="jurer fidélité_____1<Suj:cln|sn,Objà:à-sn>",synt_head=jurer]	jurer fidélité_____1	Default		%default
fleurette	100	cf	[pred="conter fleurette_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conter]	conter fleurette_____1	Default		%default
foi	100	cf	[pred="accorder foi_____1<Suj:cln|sn,Objà:à-sn>",synt_head=accorder]	accorder foi_____1	Default		%default
force de rames	100	cf	[pred="faire force de rames_____1<Suj:cln|sn,Obl:vers-sn>",synt_head=faire]	faire force de rames_____1	Default		%default
force de vapeur	100	cf	[pred="faire force de vapeur_____1<Suj:cln|sn,Obl:vers-sn>",synt_head=faire]	faire force de vapeur_____1	Default		%default
force de voiles	100	cf	[pred="faire force de voiles_____1<Suj:cln|sn,Obl:vers-sn>",synt_head=faire]	faire force de voiles_____1	Default		%default
fortune	100	cf	[pred="chercher fortune_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=chercher]	chercher fortune_____1	Default		%default
fortune	100	cf	[pred="faire fortune_____1<Suj:cln|sn,Obl:auprès_de-sn>",synt_head=faire]	faire fortune_____1	Default		%default
fortune	100	cf	[pred="tenter fortune_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=tenter]	tenter fortune_____1	Default		%default
fortune	100	cf	[pred="trouver fortune_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver fortune_____1	Default		%default
foule	100	cf	[pred="faire foule_____1<Suj:cln|sn,Obl:autour_de-sn>",synt_head=faire]	faire foule_____1	Default		%default
frais	100	cf	[pred="tenir frais_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir frais_____1	Default		%default
franc jeu	100	cf	[pred="jouer franc jeu_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=jouer]	jouer franc jeu_____1	Default		%default
franc jeu	100	cf	[pred="y aller franc jeu_____1<Suj:cln|sn,Obl:avec-sn>",@pseudo-y,synt_head=aller]	y aller franc jeu_____1	Default		%default
froid	100	cf	[pred="battre froid_____1<Suj:cln|sn,Objà:à-sn>",synt_head=battre]	battre froid_____1	Default		%default
froid	100	cf	[pred="tenir froid_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir froid_____1	Default		%default
front	100	cf	[pred="faire front_____1<Suj:cln|sn,Obl:contre-sn>",synt_head=faire]	faire front_____1	Default		%default
fête	100	cf	[pred="faire fête_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire fête_____1	Default		%default
garnison	100	cf	[pred="tenir garnison_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=tenir]	tenir garnison_____1	Default		%default
gloire	100	cf	[pred="rendre gloire_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre gloire_____1	Default		%default
grand bruit	100	cf	[pred="faire grand bruit_____1<Suj:cln|sn,Obl:autour_de-sn>",synt_head=faire]	faire grand bruit_____1	Default		%default
grand bruit	100	cf	[pred="mener grand bruit_____1<Suj:cln|sn,Obl:autour_de-sn>",synt_head=mener]	mener grand bruit_____1	Default		%default
grand tapage	100	cf	[pred="mener grand tapage_____1<Suj:cln|sn,Obl:autour_de-sn>",synt_head=mener]	mener grand tapage_____1	Default		%default
grand train	100	cf	[pred="mener grand train_____1<Suj:cln|sn,Obl:autour_de-sn>",synt_head=mener]	mener grand train_____1	Default		%default
grande impression	100	cf	[pred="faire grande impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire grande impression_____1	Default		%default
grandi	100	cf	[pred="sortir grandi_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=sortir]	sortir grandi_____1	Default		%default
gras	100	cf	[pred="n' en apprendre gras_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=apprendre]	n' en apprendre gras_____1	Default		%default
gras	100	cf	[pred="n' en savoir gras_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=savoir]	n' en savoir gras_____1	Default		%default
gros	100	cf	[pred="miser gros_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=miser]	miser gros_____1	Default		%default
gros jeu	100	cf	[pred="jouer gros jeu_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=jouer]	jouer gros jeu_____1	Default		%default
grosse impression	100	cf	[pred="faire grosse impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire grosse impression_____1	Default		%default
grrr	100	cf	[pred="faire grrr_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire grrr_____1	Default		%default
grâce	100	cf	[pred="faire grâce_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire grâce_____1	Default		%default
grâce	100	cf	[pred="trouver grâce_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=trouver]	trouver grâce_____1	Default		%default
guère	100	cf	[pred="ne compter guère_____1<Suj:cln|sn,Obl:pour-sn>",clneg =c +,synt_head=compter]	ne compter guère_____1	Default		%default
haro	100	cf	[pred="crier haro_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=crier]	crier haro_____1	Default		%default
hep	100	cf	[pred="faire hep_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire hep_____1	Default		%default
ho	100	cf	[pred="faire ho_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire ho_____1	Default		%default
hommage	100	cf	[pred="rendre hommage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre hommage_____1	Default		%default
honneur	100	cf	[pred="faire honneur_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire honneur_____1	Default		%default
honte	100	cf	[pred="faire honte_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire honte_____1	Default		%default
hé	100	cf	[pred="faire hé_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire hé_____1	Default		%default
impression	100	cf	[pred="faire impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire impression_____1	Default		%default
injure	100	cf	[pred="faire injure_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire injure_____1	Default		%default
insulte	100	cf	[pred="faire insulte_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire insulte_____1	Default		%default
intrusion	100	cf	[pred="faire intrusion_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire intrusion_____1	Default		%default
intérêt	100	cf	[pred="porter intérêt_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter intérêt_____1	Default		%default
invasion	100	cf	[pred="faire invasion_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire invasion_____1	Default		%default
irruption	100	cf	[pred="faire irruption_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire irruption_____1	Default		%default
jeu égal	100	cf	[pred="faire jeu égal_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire jeu égal_____1	Default		%default
joujou	100	cf	[pred="faire joujou_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire joujou_____1	Default		%default
jour	100	cf	[pred="prendre jour_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	prendre jour_____1	Default		%default
justice	100	cf	[pred="demander justice_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander justice_____1	Default		%default
justice	100	cf	[pred="obtenir justice_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=obtenir]	obtenir justice_____1	Default		%default
justice	100	cf	[pred="rendre justice_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre justice_____1	Default		%default
justice	100	cf	[pred="réclamer justice_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réclamer]	réclamer justice_____1	Default		%default
justice	100	cf	[pred="réclamer justice_____2<Suj:cln|sn,Obl:de_la_part_de-sn>",synt_head=réclamer]	réclamer justice_____2	Default		%default
langue	100	cf	[pred="prendre langue_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	prendre langue_____1	Default		%default
lecture du texte	100	cf	[pred="exiger lecture du texte_____1<Suj:cln|sn,Objà:à-sn>",synt_head=exiger]	exiger lecture du texte_____1	Default		%default
lerche	100	cf	[pred="n' en savoir lerche_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=savoir]	n' en savoir lerche_____1	Default		%default
les mains	100	cf	[pred="se laver les mains_____1<Suj:cln|sn,Objde:de-sn|en>",@pron,@être,synt_head=laver]	se laver les mains_____1	Default		%default
levier	100	cf	[pred="faire levier_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire levier_____1	Default		%default
lieu	100	cf	[pred="donner lieu_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner lieu_____1	Default		%default
livraison	100	cf	[pred="prendre livraison_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre livraison_____1	Default		%default
livraison	100	cf	[pred="recevoir livraison_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=recevoir]	recevoir livraison_____1	Default		%default
loin	100	cf	[pred="aller loin_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=aller]	aller loin_____1	Default		%default
loin	100	cf	[pred="n' aller loin_____1<Suj:cln|sn,Obl:avec-sn>",clneg =c +,synt_head=aller]	n' aller loin_____1	Default		%default
long	100	cf	[pred="en apprendre long_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=apprendre]	en apprendre long_____1	Default		%default
long	100	cf	[pred="en savoir long_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir long_____1	Default		%default
lourd	100	cf	[pred="n' en apprendre lourd_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=apprendre]	n' en apprendre lourd_____1	Default		%default
lourd	100	cf	[pred="n' en savoir lourd_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,clneg =c +,synt_head=savoir]	n' en savoir lourd_____1	Default		%default
lourd	100	cf	[pred="ne donner lourd_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=donner]	ne donner lourd_____1	Default		%default
lourd	100	cf	[pred="ne peser lourd_____1<Suj:cln|sn,Obl:contre-sn>",clneg =c +,synt_head=peser]	ne peser lourd_____1	Default		%default
main basse	100	cf	[pred="faire main basse_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire main basse_____1	Default		%default
main forte	100	cf	[pred="prêter main forte_____1<Suj:cln|sn,Objà:à-sn>",synt_head=prêter]	prêter main forte_____1	Default		%default
mal	100	cf	[pred="faire mal_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire mal_____1	Default		%default
mal	100	cf	[pred="répondre mal_____1<Suj:cln|sn,Objà:à-sn>",synt_head=répondre]	répondre mal_____1	Default		%default
mal	100	cf	[pred="s'y prendre mal_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	s'y prendre mal_____1	Default		%default
mal	100	cf	[pred="tomber mal_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=tomber]	tomber mal_____1	Default		%default
malheur	100	cf	[pred="porter malheur_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter malheur_____1	Default		%default
malice	100	cf	[pred="chercher malice_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=chercher]	chercher malice_____1	Default		%default
malice	100	cf	[pred="trouver malice_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver malice_____1	Default		%default
malice	100	cf	[pred="voir malice_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=voir]	voir malice_____1	Default		%default
marche	100	cf	[pred="faire marche_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire marche_____1	Default		%default
maritalement	100	cf	[pred="vivre maritalement_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=vivre]	vivre maritalement_____1	Default		%default
matière	100	cf	[pred="donner matière_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner matière_____1	Default		%default
mauvais accueil	100	cf	[pred="faire mauvais accueil_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire mauvais accueil_____1	Default		%default
mauvais effet	100	cf	[pred="faire mauvais effet_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire mauvais effet_____1	Default		%default
mauvais ménage	100	cf	[pred="faire mauvais ménage_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire mauvais ménage_____1	Default		%default
mauvais usage	100	cf	[pred="faire mauvais usage_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire mauvais usage_____1	Default		%default
mauvaise impression	100	cf	[pred="faire mauvaise impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire mauvaise impression_____1	Default		%default
mauvaise mine	100	cf	[pred="faire mauvaise mine_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire mauvaise mine_____1	Default		%default
maître	100	cf	[pred="passer maître_____1<Suj:cln|sn,Obl:dans-sn>",synt_head=passer]	passer maître_____1	Default		%default
merci	100	cf	[pred="demander merci_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander merci_____1	Default		%default
merveille	100	cf	[pred="faire merveille_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire merveille_____1	Default		%default
merveille de N	100	cf	[pred="promettre merveille de N_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre merveille de N_____1	Default		%default
mettre bien	100	cf	[pred="se mettre bien_____1<Suj:cln|sn,Obl:avec-sn>",@pron,@être,synt_head=se]	se mettre bien_____1	Default		%default
mettre mal	100	cf	[pred="se mettre mal_____1<Suj:cln|sn,Obl:avec-sn>",@pron,@être,synt_head=se]	se mettre mal_____1	Default		%default
misère	100	cf	[pred="pleurer misère_____1<Suj:cln|sn,Obl:auprès_de-sn>",synt_head=pleurer]	pleurer misère_____1	Default		%default
modèle	100	cf	[pred="prendre modèle_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=prendre]	prendre modèle_____1	Default		%default
mumuse	100	cf	[pred="faire mumuse_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire mumuse_____1	Default		%default
naissance	100	cf	[pred="donner naissance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner naissance_____1	Default		%default
naissance	100	cf	[pred="prendre naissance_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre naissance_____1	Default		%default
naissance	100	cf	[pred="trouver naissance_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver naissance_____1	Default		%default
noise	100	cf	[pred="chercher noise_____1<Suj:cln|sn,Objà:à-sn>",synt_head=chercher]	chercher noise_____1	Default		%default
non	100	cf	[pred="répondre non_____1<Suj:cln|sn,Objà:à-sn>",synt_head=répondre]	répondre non_____1	Default		%default
notification	100	cf	[pred="recevoir notification_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=recevoir]	recevoir notification_____1	Default		%default
négativement	100	cf	[pred="répondre négativement_____1<Suj:cln|sn,Objà:à-sn>",synt_head=répondre]	répondre négativement_____1	Default		%default
obéissance	100	cf	[pred="jurer obéissance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=jurer]	jurer obéissance_____1	Default		%default
obéissance	100	cf	[pred="refuser obéissance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=refuser]	refuser obéissance_____1	Default		%default
obéissance	100	cf	[pred="vouer obéissance_____1<Suj:cln|sn,Objà:à-sn>",synt_head=vouer]	vouer obéissance_____1	Default		%default
ohé	100	cf	[pred="faire ohé_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire ohé_____1	Default		%default
ombrage	100	cf	[pred="porter ombrage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter ombrage_____1	Default		%default
ouhouh	100	cf	[pred="faire ouhouh_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire ouhouh_____1	Default		%default
oui	100	cf	[pred="répondre oui_____1<Suj:cln|sn,Objà:à-sn>",synt_head=répondre]	répondre oui_____1	Default		%default
outrage	100	cf	[pred="faire outrage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire outrage_____1	Default		%default
où ?	100	cf	[pred="en être où ?_____1<Suj:cln|sn,Objde:de-sn>",@pseudo-en,synt_head=être]	en être où ?_____1	Default		%default
pan	100	cf	[pred="faire pan_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire pan_____1	Default		%default
parole	100	cf	[pred="tenir parole_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir parole_____1	Default		%default
part	100	cf	[pred="prendre part_____1<Suj:cln|sn,Objà:à-sn>",synt_head=prendre]	prendre part_____1	Default		%default
parti	100	cf	[pred="prendre parti_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=prendre]	prendre parti_____1	Default		%default
partie	100	cf	[pred="lier partie_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=lier]	lier partie_____1	Default		%default
pas grand chose	100	cf	[pred="ne valoir pas grand chose_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=valoir]	ne valoir pas grand chose_____1	Default		%default
pas grand chose de bon	100	cf	[pred="ne dire pas grand chose de bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire pas grand chose de bon_____1	Default		%default
pas grand chose de bon	100	cf	[pred="ne présager pas grand chose de bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=présager]	ne présager pas grand chose de bon_____1	Default		%default
pas grand chose qui vaille	100	cf	[pred="ne dire pas grand chose qui vaille_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire pas grand chose qui vaille_____1	Default		%default
pas peur	100	cf	[pred="ne faire pas peur_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=faire]	ne faire pas peur_____1	Default		%default
pas plus	100	cf	[pred="n' en demander pas plus_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,clneg =c +,synt_head=demander]	n' en demander pas plus_____1	Default		%default
pas si leur grand mère fait du vélo	100	cf	[pred="ne demander pas si sa grand mère fait du vélo_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=demander]	ne demander pas si sa grand mère fait du vélo_____1	Default		%default
pas si ma grand mère fait du vélo	100	cf	[pred="ne demander pas si sa grand mère fait du vélo_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=demander]	ne demander pas si sa grand mère fait du vélo_____1	Default		%default
pas si notre grand mère fait du vélo	100	cf	[pred="ne demander pas si sa grand mère fait du vélo_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=demander]	ne demander pas si sa grand mère fait du vélo_____1	Default		%default
pas si sa grand mère fait du vélo	100	cf	[pred="ne demander pas si sa grand mère fait du vélo_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=demander]	ne demander pas si sa grand mère fait du vélo_____1	Default		%default
pas si ta grand mère fait du vélo	100	cf	[pred="ne demander pas si sa grand mère fait du vélo_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=demander]	ne demander pas si sa grand mère fait du vélo_____1	Default		%default
pas si votre grand mère fait du vélo	100	cf	[pred="ne demander pas si sa grand mère fait du vélo_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=demander]	ne demander pas si sa grand mère fait du vélo_____1	Default		%default
pas tant	100	cf	[pred="n' en demander pas tant_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,clneg =c +,synt_head=demander]	n' en demander pas tant_____1	Default		%default
pas tripette	100	cf	[pred="ne valoir pas tripette_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=valoir]	ne valoir pas tripette_____1	Default		%default
pas trop	100	cf	[pred="n' en demander pas trop_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,clneg =c +,synt_head=demander]	n' en demander pas trop_____1	Default		%default
pas un chat	100	cf	[pred="ne rencontrer pas un chat_____1<Suj:cln|sn,Loc:loc-sn|y>",clneg =c +,synt_head=rencontrer]	ne rencontrer pas un chat_____1	Default		%default
pas un pouce de terrain	100	cf	[pred="ne céder pas un pouce de terrain_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=céder]	ne céder pas un pouce de terrain_____1	Default		%default
pas une larme	100	cf	[pred="n' arracher pas une larme_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=arracher]	n' arracher pas une larme_____1	Default		%default
pas âme qui vive	100	cf	[pred="ne rencontrer pas âme qui vive_____1<Suj:cln|sn,Loc:loc-sn|y>",clneg =c +,synt_head=rencontrer]	ne rencontrer pas âme qui vive_____1	Default		%default
pas âme qui vive	100	cf	[pred="ne trouver pas âme qui vive_____1<Suj:cln|sn,Loc:loc-sn|y>",clneg =c +,synt_head=trouver]	ne trouver pas âme qui vive_____1	Default		%default
pas âme qui vive	100	cf	[pred="ne voir pas âme qui vive_____1<Suj:cln|sn,Loc:loc-sn|y>",clneg =c +,synt_head=voir]	ne voir pas âme qui vive_____1	Default		%default
passage	100	cf	[pred="donner passage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner passage_____1	Default		%default
passage	100	cf	[pred="livrer passage_____1<Suj:cln|sn,Objà:à-sn>",synt_head=livrer]	livrer passage_____1	Default		%default
patience	100	cf	[pred="perdre patience_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=perdre]	perdre patience_____1	Default		%default
patte blanche	100	cf	[pred="montrer patte blanche_____1<Suj:cln|sn,Objà:à-sn>",synt_head=montrer]	montrer patte blanche_____1	Default		%default
pavillon	100	cf	[pred="baisser pavillon_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=baisser]	baisser pavillon_____1	Default		%default
pension	100	cf	[pred="prendre pension_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre pension_____1	Default		%default
peu	100	cf	[pred="compter peu_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=compter]	compter peu_____1	Default		%default
peu	100	cf	[pred="signifier peu_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=signifier]	signifier peu_____1	Default		%default
pied	100	cf	[pred="perdre pied_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=perdre]	perdre pied_____1	Default		%default
pied	100	cf	[pred="prendre pied_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre pied_____1	Default		%default
pitié	100	cf	[pred="faire pitié_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire pitié_____1	Default		%default
pièce	100	cf	[pred="faire pièce_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire pièce_____1	Default		%default
piètre effet	100	cf	[pred="faire piètre effet_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire piètre effet_____1	Default		%default
piètre impression	100	cf	[pred="faire piètre impression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire piètre impression_____1	Default		%default
piètre usage	100	cf	[pred="faire piètre usage_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire piètre usage_____1	Default		%default
place	100	cf	[pred="faire place_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire place_____1	Default		%default
place	100	cf	[pred="prendre place_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre place_____1	Default		%default
place	100	cf	[pred="prendre place_____2<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre place_____2	Default		%default
place	100	cf	[pred="trouver place_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver place_____1	Default		%default
plainte	100	cf	[pred="porter plainte_____1<Suj:cln|sn,Obl:(contre-sn)>",synt_head=porter]	porter plainte_____1	Default		%default
plaisir	100	cf	[pred="faire plaisir_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire plaisir_____1	Default		%default
pleins feux	100	cf	[pred="mettre pleins feux_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=mettre]	mettre pleins feux_____1	Default		%default
plus ample connaissance	100	cf	[pred="faire plus ample connaissance_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire plus ample connaissance_____1	Default		%default
plus avant	100	cf	[pred="aller plus avant_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=aller]	aller plus avant_____1	Default		%default
plus loin	100	cf	[pred="aller plus loin_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=aller]	aller plus loin_____1	Default		%default
porter acheteur	100	cf	[pred="se porter acheteur_____1<Suj:cln|sn,Objde:de-sn|en>",@pron,@être,synt_head=se]	se porter acheteur_____1	Default		%default
porter acquéreur	100	cf	[pred="se porter acquéreur_____1<Suj:cln|sn,Objde:de-sn|en>",@pron,@être,synt_head=se]	se porter acquéreur_____1	Default		%default
porter candidat	100	cf	[pred="se porter candidat_____1<Suj:cln|sn,Objà:à-sn>",@pron,@être,synt_head=se]	se porter candidat_____1	Default		%default
porter caution	100	cf	[pred="se porter caution_____1<Suj:cln|sn,Obl:pour-sn>",@pron,@être,synt_head=se]	se porter caution_____1	Default		%default
porter partie civile	100	cf	[pred="se porter partie civile_____1<Suj:cln|sn,Obl:contre-sn>",@pron,@être,synt_head=se]	se porter partie civile_____1	Default		%default
position	100	cf	[pred="prendre position_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre position_____1	Default		%default
position	100	cf	[pred="prendre position_____2<Suj:cln|sn,Obl:sur-sn>",synt_head=prendre]	prendre position_____2	Default		%default
possession	100	cf	[pred="prendre possession_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre possession_____1	Default		%default
pression	100	cf	[pred="faire pression_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire pression_____1	Default		%default
prise	100	cf	[pred="trouver prise_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=trouver]	trouver prise_____1	Default		%default
problème	100	cf	[pred="faire problème_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire problème_____1	Default		%default
problème	100	cf	[pred="poser problème_____1<Suj:cln|sn,Objà:à-sn>",synt_head=poser]	poser problème_____1	Default		%default
promesse	100	cf	[pred="tenir promesse_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir promesse_____1	Default		%default
protection	100	cf	[pred="donner protection_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner protection_____1	Default		%default
préjudice	100	cf	[pred="porter préjudice_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter préjudice_____1	Default		%default
psitt	100	cf	[pred="faire psitt_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire psitt_____1	Default		%default
pstt	100	cf	[pred="faire pstt_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire pstt_____1	Default		%default
que non	100	cf	[pred="dire que non_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire que non_____1	Default		%default
que oui	100	cf	[pred="dire que oui_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire que oui_____1	Default		%default
que si	100	cf	[pred="dire que si_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire que si_____1	Default		%default
que un	100	cf	[pred="ne faire que un_____1<Suj:cln|sn,Obl:avec-sn>",clneg =c +,synt_head=faire]	ne faire que un_____1	Default		%default
que une bouchée	100	cf	[pred="ne faire que une bouchée_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=faire]	ne faire que une bouchée_____1	Default		%default
quel âge ?	100	cf	[pred="donner quel âge ?_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner quel âge ?_____1	Default		%default
quelque chose	100	cf	[pred="casser quelque chose_____1<Suj:cln|sn,Objà:à-sn>",synt_head=casser]	casser quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="demander quelque chose_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="dire quelque chose_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="fabriquer quelque chose_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fabriquer]	fabriquer quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="faire quelque chose_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="faire quelque chose_____2<Suj:cln|sn,Obl:pour-sn>",synt_head=faire]	faire quelque chose_____2	Default		%default
quelque chose	100	cf	[pred="foutre quelque chose_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="pouvoir quelque chose_____1<Suj:cln|sn,Objà:à-sn>",synt_head=pouvoir]	pouvoir quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="pouvoir quelque chose_____2<Suj:cln|sn,Obl:contre-sn>",synt_head=pouvoir]	pouvoir quelque chose_____2	Default		%default
quelque chose	100	cf	[pred="pouvoir quelque chose_____3<Suj:cln|sn,Obl:pour-sn>",synt_head=pouvoir]	pouvoir quelque chose_____3	Default		%default
quelque chose	100	cf	[pred="pouvoir quelque chose_____4<Suj:cln|sn,Obl:sur-sn>",synt_head=pouvoir]	pouvoir quelque chose_____4	Default		%default
quelque chose	100	cf	[pred="prendre quelque chose_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	prendre quelque chose_____1	Default		%default
quelque chose	100	cf	[pred="tirer quelque chose_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tirer]	tirer quelque chose_____1	Default		%default
quelque chose ?	100	cf	[pred="faire quelque chose ?_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire quelque chose ?_____1	Default		%default
quelque chose ?	100	cf	[pred="faire quelque chose ?_____2<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire quelque chose ?_____2	Default		%default
quelque chose ?	100	cf	[pred="fiche quelque chose ?_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche quelque chose ?_____1	Default		%default
quelque part ?	100	cf	[pred="en être quelque part ?_____1<Suj:cln|sn,Objde:de-sn>",@pseudo-en,synt_head=être]	en être quelque part ?_____1	Default		%default
querelle	100	cf	[pred="chercher querelle_____1<Suj:cln|sn,Objà:à-sn>",synt_head=chercher]	chercher querelle_____1	Default		%default
quitus	100	cf	[pred="donner quitus_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner quitus_____1	Default		%default
quoi ?	100	cf	[pred="chanter quoi ?_____1<Suj:cln|sn,Objà:à-sn>",synt_head=chanter]	chanter quoi ?_____1	Default		%default
quoi ?	100	cf	[pred="ferait quoi ?_____1<Suj:cln|sn,Obl:sans-sn>",synt_head=ferait]	ferait quoi ?_____1	Default		%default
racine	100	cf	[pred="prendre racine_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre racine_____1	Default		%default
raison	100	cf	[pred="donner raison_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner raison_____1	Default		%default
rancune	100	cf	[pred="porter rancune_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter rancune_____1	Default		%default
rang	100	cf	[pred="prendre rang_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre rang_____1	Default		%default
refuge	100	cf	[pred="chercher refuge_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=chercher]	chercher refuge_____1	Default		%default
refuge	100	cf	[pred="demander refuge_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander refuge_____1	Default		%default
refuge	100	cf	[pred="donner refuge_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner refuge_____1	Default		%default
refuge	100	cf	[pred="trouver refuge_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver refuge_____1	Default		%default
registre	100	cf	[pred="tenir registre_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tenir]	tenir registre_____1	Default		%default
remède	100	cf	[pred="porter remède_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter remède_____1	Default		%default
rendez-vous	100	cf	[pred="fixer rendez-vous_____1<Suj:cln|sn,Objà:à-sn>",synt_head=fixer]	fixer rendez-vous_____1	Default		%default
rendez-vous	100	cf	[pred="prendre rendez-vous_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=prendre]	prendre rendez-vous_____1	Default		%default
retour	100	cf	[pred="faire retour_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire retour_____1	Default		%default
retraite	100	cf	[pred="faire retraite_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire retraite_____1	Default		%default
rien	100	cf	[pred="n' espérer rien_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=espérer]	n' espérer rien_____1	Default		%default
rien	100	cf	[pred="ne refuser rien_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=refuser]	ne refuser rien_____1	Default		%default
rien	100	cf	[pred="ne savoir rien_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=savoir]	ne savoir rien_____1	Default		%default
rien	100	cf	[pred="ne valoir rien_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=valoir]	ne valoir rien_____1	Default		%default
rien de bien bon	100	cf	[pred="ne dire rien de bien bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire rien de bien bon_____1	Default		%default
rien de bien bon	100	cf	[pred="ne promettre rien de bien bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=promettre]	ne promettre rien de bien bon_____1	Default		%default
rien de bien bon	100	cf	[pred="ne présager rien de bien bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=présager]	ne présager rien de bien bon_____1	Default		%default
rien de bon	100	cf	[pred="ne dire rien de bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire rien de bon_____1	Default		%default
rien de bon	100	cf	[pred="ne promettre rien de bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=promettre]	ne promettre rien de bon_____1	Default		%default
rien de bon	100	cf	[pred="ne présager rien de bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=présager]	ne présager rien de bon_____1	Default		%default
rien de très bon	100	cf	[pred="ne dire rien de très bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire rien de très bon_____1	Default		%default
rien de très bon	100	cf	[pred="ne promettre rien de très bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=promettre]	ne promettre rien de très bon_____1	Default		%default
rien de très bon	100	cf	[pred="ne présager rien de très bon_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=présager]	ne présager rien de très bon_____1	Default		%default
rien qui vaille	100	cf	[pred="ne dire rien qui vaille_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire rien qui vaille_____1	Default		%default
rien qui vaille	100	cf	[pred="ne présager rien qui vaille_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=présager]	ne présager rien qui vaille_____1	Default		%default
risette	100	cf	[pred="faire risette_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire risette_____1	Default		%default
route	100	cf	[pred="faire route_____1<Suj:cln|sn,Obl:vers-sn>",synt_head=faire]	faire route_____1	Default		%default
réflexion	100	cf	[pred="demander réflexion_____1<Suj:cln|sn,Obl:de_la_part_de-sn>",synt_head=demander]	demander réflexion_____1	Default		%default
réparation	100	cf	[pred="obtenir réparation_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=obtenir]	obtenir réparation_____1	Default		%default
satisfaction	100	cf	[pred="donner satisfaction_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner satisfaction_____1	Default		%default
satisfaction	100	cf	[pred="obtenir satisfaction_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=obtenir]	obtenir satisfaction_____1	Default		%default
satisfaction	100	cf	[pred="recevoir satisfaction_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=recevoir]	recevoir satisfaction_____1	Default		%default
sec	100	cf	[pred="demeurer sec_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=demeurer]	demeurer sec_____1	Default		%default
sec	100	cf	[pred="rester sec_____1<Suj:cln|sn,Obl:devant-sn>",synt_head=rester]	rester sec_____1	Default		%default
secours	100	cf	[pred="demander secours_____1<Suj:cln|sn,Objà:à-sn>",synt_head=demander]	demander secours_____1	Default		%default
secours	100	cf	[pred="porter secours_____1<Suj:cln|sn,Objà:à-sn>",synt_head=porter]	porter secours_____1	Default		%default
serment de N-hum	100	cf	[pred="prêter serment de N-hum_____1<Suj:cln|sn,Objà:à-sn>",synt_head=prêter]	prêter serment de N-hum_____1	Default		%default
serré	100	cf	[pred="jouer serré_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=jouer]	jouer serré_____1	Default		%default
service	100	cf	[pred="rendre service_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre service_____1	Default		%default
signe	100	cf	[pred="faire signe_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire signe_____1	Default		%default
signe de vie	100	cf	[pred="donner signe de vie_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner signe de vie_____1	Default		%default
signe de vie	100	cf	[pred="ne donner signe de vie_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=donner]	ne donner signe de vie_____1	Default		%default
silence	100	cf	[pred="faire silence_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire silence_____1	Default		%default
silence	100	cf	[pred="imposer silence_____1<Suj:cln|sn,Objà:à-sn>",synt_head=imposer]	imposer silence_____1	Default		%default
souche	100	cf	[pred="faire souche_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire souche_____1	Default		%default
souche	100	cf	[pred="prendre souche_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=prendre]	prendre souche_____1	Default		%default
suite	100	cf	[pred="donner suite_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner suite_____1	Default		%default
suite	100	cf	[pred="faire suite_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire suite_____1	Default		%default
sujet	100	cf	[pred="donner sujet_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner sujet_____1	Default		%default
sus	100	cf	[pred="courir sus_____1<Suj:cln|sn,Objà:à-sn>",synt_head=courir]	courir sus_____1	Default		%default
tache	100	cf	[pred="faire tache_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire tache_____1	Default		%default
tant	100	cf	[pred="coûter tant_____1<Suj:cln|sn,Objà:à-sn>",synt_head=coûter]	coûter tant_____1	Default		%default
tort	100	cf	[pred="donner tort_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner tort_____1	Default		%default
tort	100	cf	[pred="faire tort_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire tort_____1	Default		%default
tout	100	cf	[pred="dire tout_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire tout_____1	Default		%default
tout	100	cf	[pred="passer tout_____1<Suj:cln|sn,Objà:à-sn>",synt_head=passer]	passer tout_____1	Default		%default
tout	100	cf	[pred="savoir tout_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=savoir]	savoir tout_____1	Default		%default
tout	100	cf	[pred="voir tout_____1<Suj:cln|sn,Objà:à-sn>",synt_head=voir]	voir tout_____1	Default		%default
tout seul	100	cf	[pred="venir tout seul_____1<Suj:cln|sn,Objà:à-sn>",synt_head=venir]	venir tout seul_____1	Default		%default
toute responsabilité	100	cf	[pred="décliner toute responsabilité_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=décliner]	décliner toute responsabilité_____1	Default		%default
toutes réserves	100	cf	[pred="faire toutes réserves_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire toutes réserves_____1	Default		%default
tribut	100	cf	[pred="payer tribut_____1<Suj:cln|sn,Objà:à-sn>",synt_head=payer]	payer tribut_____1	Default		%default
trop	100	cf	[pred="en demander trop_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=demander]	en demander trop_____1	Default		%default
trop	100	cf	[pred="en savoir trop_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir trop_____1	Default		%default
trop avant	100	cf	[pred="aller trop avant_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=aller]	aller trop avant_____1	Default		%default
trop de honneur	100	cf	[pred="faire trop de honneur_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire trop de honneur_____1	Default		%default
trouver court	100	cf	[pred="se trouver court_____1<Suj:cln|sn,Obl:devant-sn>",@pron,@être,synt_head=se]	se trouver court_____1	Default		%default
tu	100	cf	[pred="dire tu_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire tu_____1	Default		%default
tête	100	cf	[pred="faire tête_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire tête_____1	Default		%default
tête	100	cf	[pred="tenir tête_____1<Suj:cln|sn,Objà:à-sn>",synt_head=tenir]	tenir tête_____1	Default		%default
un bout	100	cf	[pred="en apprendre un bout_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=apprendre]	en apprendre un bout_____1	Default		%default
un bout	100	cf	[pred="en connaître un bout_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=connaître]	en connaître un bout_____1	Default		%default
un bout	100	cf	[pred="en savoir un bout_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir un bout_____1	Default		%default
un chat	100	cf	[pred="ne trouver un chat_____1<Suj:cln|sn,Loc:loc-sn|y>",clneg =c +,synt_head=trouver]	ne trouver un chat_____1	Default		%default
un chien de leur chienne	100	cf	[pred="conserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conserver]	conserver un chien de sa chienne_____1	Default		%default
un chien de leur chienne	100	cf	[pred="devoir un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir un chien de sa chienne_____1	Default		%default
un chien de leur chienne	100	cf	[pred="garder un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=garder]	garder un chien de sa chienne_____1	Default		%default
un chien de leur chienne	100	cf	[pred="promettre un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre un chien de sa chienne_____1	Default		%default
un chien de leur chienne	100	cf	[pred="réserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réserver]	réserver un chien de sa chienne_____1	Default		%default
un chien de ma chienne	100	cf	[pred="conserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conserver]	conserver un chien de sa chienne_____1	Default		%default
un chien de ma chienne	100	cf	[pred="devoir un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir un chien de sa chienne_____1	Default		%default
un chien de ma chienne	100	cf	[pred="garder un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=garder]	garder un chien de sa chienne_____1	Default		%default
un chien de ma chienne	100	cf	[pred="promettre un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre un chien de sa chienne_____1	Default		%default
un chien de ma chienne	100	cf	[pred="réserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réserver]	réserver un chien de sa chienne_____1	Default		%default
un chien de notre chienne	100	cf	[pred="conserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conserver]	conserver un chien de sa chienne_____1	Default		%default
un chien de notre chienne	100	cf	[pred="devoir un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir un chien de sa chienne_____1	Default		%default
un chien de notre chienne	100	cf	[pred="garder un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=garder]	garder un chien de sa chienne_____1	Default		%default
un chien de notre chienne	100	cf	[pred="promettre un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre un chien de sa chienne_____1	Default		%default
un chien de notre chienne	100	cf	[pred="réserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réserver]	réserver un chien de sa chienne_____1	Default		%default
un chien de sa chienne	100	cf	[pred="conserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conserver]	conserver un chien de sa chienne_____1	Default		%default
un chien de sa chienne	100	cf	[pred="devoir un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir un chien de sa chienne_____1	Default		%default
un chien de sa chienne	100	cf	[pred="garder un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=garder]	garder un chien de sa chienne_____1	Default		%default
un chien de sa chienne	100	cf	[pred="promettre un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre un chien de sa chienne_____1	Default		%default
un chien de sa chienne	100	cf	[pred="réserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réserver]	réserver un chien de sa chienne_____1	Default		%default
un chien de ta chienne	100	cf	[pred="conserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conserver]	conserver un chien de sa chienne_____1	Default		%default
un chien de ta chienne	100	cf	[pred="devoir un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir un chien de sa chienne_____1	Default		%default
un chien de ta chienne	100	cf	[pred="garder un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=garder]	garder un chien de sa chienne_____1	Default		%default
un chien de ta chienne	100	cf	[pred="promettre un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre un chien de sa chienne_____1	Default		%default
un chien de ta chienne	100	cf	[pred="réserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réserver]	réserver un chien de sa chienne_____1	Default		%default
un chien de votre chienne	100	cf	[pred="conserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=conserver]	conserver un chien de sa chienne_____1	Default		%default
un chien de votre chienne	100	cf	[pred="devoir un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir un chien de sa chienne_____1	Default		%default
un chien de votre chienne	100	cf	[pred="garder un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=garder]	garder un chien de sa chienne_____1	Default		%default
un chien de votre chienne	100	cf	[pred="promettre un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=promettre]	promettre un chien de sa chienne_____1	Default		%default
un chien de votre chienne	100	cf	[pred="réserver un chien de sa chienne_____1<Suj:cln|sn,Objà:à-sn>",synt_head=réserver]	réserver un chien de sa chienne_____1	Default		%default
un coin	100	cf	[pred="en boucher un coin_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=boucher]	en boucher un coin_____1	Default		%default
un coup	100	cf	[pred="en fiche un coup_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=fiche]	en fiche un coup_____1	Default		%default
un coup	100	cf	[pred="en flanquer un coup_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=flanquer]	en flanquer un coup_____1	Default		%default
un coup	100	cf	[pred="en foutre un coup_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=foutre]	en foutre un coup_____1	Default		%default
un coup	100	cf	[pred="frapper un coup_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=frapper]	frapper un coup_____1	Default		%default
un iota	100	cf	[pred="ne changer un iota_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=changer]	ne changer un iota_____1	Default		%default
un rayon	100	cf	[pred="en apprendre un rayon_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=apprendre]	en apprendre un rayon_____1	Default		%default
un rayon	100	cf	[pred="en connaître un rayon_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=connaître]	en connaître un rayon_____1	Default		%default
un rayon	100	cf	[pred="en savoir un rayon_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=savoir]	en savoir un rayon_____1	Default		%default
un saut	100	cf	[pred="faire un saut_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire un saut_____1	Default		%default
un traître mot d'anglais	100	cf	[pred="ne dire un traître mot d'anglais_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire un traître mot d'anglais_____1	Default		%default
un traître mot de N-hum	100	cf	[pred="ne dire un traître mot de N-hum_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=dire]	ne dire un traître mot de N-hum_____1	Default		%default
une belle jambe	100	cf	[pred="faire une belle jambe_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire une belle jambe_____1	Default		%default
une bien bonne	100	cf	[pred="en entendre une bien bonne_____1<Suj:cln|sn,Obl:sur-sn>",@pseudo-en,synt_head=entendre]	en entendre une bien bonne_____1	Default		%default
une bien bonne	100	cf	[pred="en raconter une bien bonne_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=raconter]	en raconter une bien bonne_____1	Default		%default
une bien bonne	100	cf	[pred="en sortir une bien bonne_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=sortir]	en sortir une bien bonne_____1	Default		%default
une croix	100	cf	[pred="faire une croix_____1<Suj:cln|sn,Obl:sur-sn>",synt_head=faire]	faire une croix_____1	Default		%default
une fière chandelle	100	cf	[pred="devoir une fière chandelle_____1<Suj:cln|sn,Objà:à-sn>",synt_head=devoir]	devoir une fière chandelle_____1	Default		%default
une surface	100	cf	[pred="en boucher une surface_____1<Suj:cln|sn,Objà:à-sn>",@pseudo-en,synt_head=boucher]	en boucher une surface_____1	Default		%default
une virgule	100	cf	[pred="ne changer une virgule_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=changer]	ne changer une virgule_____1	Default		%default
une virgule	100	cf	[pred="ne modifier une virgule_____1<Suj:cln|sn,Objà:à-sn>",clneg =c +,synt_head=modifier]	ne modifier une virgule_____1	Default		%default
vengeance	100	cf	[pred="tirer vengeance_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tirer]	tirer vengeance_____1	Default		%default
vie	100	cf	[pred="donner vie_____1<Suj:cln|sn,Objà:à-sn>",synt_head=donner]	donner vie_____1	Default		%default
vie	100	cf	[pred="redonner vie_____1<Suj:cln|sn,Objà:à-sn>",synt_head=redonner]	redonner vie_____1	Default		%default
vie	100	cf	[pred="rendre vie_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre vie_____1	Default		%default
violence	100	cf	[pred="faire violence_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire violence_____1	Default		%default
visite	100	cf	[pred="rendre visite_____1<Suj:cln|sn,Objà:à-sn>",synt_head=rendre]	rendre visite_____1	Default		%default
vous	100	cf	[pred="dire vous_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire vous_____1	Default		%default
vrai	100	cf	[pred="dire vrai_____1<Suj:cln|sn,Objà:à-sn>",synt_head=dire]	dire vrai_____1	Default		%default
ça	100	cf	[pred="faire ça_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire ça_____1	Default		%default
ça	100	cf	[pred="faire ça_____2<Suj:cln|sn,Loc:loc-sn|y>",synt_head=faire]	faire ça_____2	Default		%default
ça	100	cf	[pred="revaloir ça_____1<Suj:cln|sn,Objà:à-sn>",synt_head=revaloir]	revaloir ça_____1	Default		%default
échec	100	cf	[pred="faire échec_____1<Suj:cln|sn,Objà:à-sn>",synt_head=faire]	faire échec_____1	Default		%default
écho	100	cf	[pred="trouver écho_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=trouver]	trouver écho_____1	Default		%default
école	100	cf	[pred="tenir école_____1<Suj:cln|sn,Loc:loc-sn|y>",synt_head=tenir]	tenir école_____1	Default		%default
énormément	100	cf	[pred="compter énormément_____1<Suj:cln|sn,Obl:pour-sn>",synt_head=compter]	compter énormément_____1	Default		%default
équipe	100	cf	[pred="faire équipe_____1<Suj:cln|sn,Obl:avec-sn>",synt_head=faire]	faire équipe_____1	Default		%default
étalage	100	cf	[pred="faire étalage_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire étalage_____1	Default		%default
