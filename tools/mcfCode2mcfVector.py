#!/usr/bin/python
# -*- coding: utf-8 -*-

#./concatUDlang.py fichiers_config/concatUD_codesLangConcatLang 100 > tmp

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import pandas
import extractLangWals as Lang
import extractFeatWals as Feat

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def findNearestLang(vectOfLang, lang): #find the language with the most identical features, and whiwh does not contain -1
    maxIdenticalFeat = 0
    nearestLang = lang
    for potentialNearestLang in vectOfLang:
        curIdenticalFeat = 0
        if((potentialNearestLang == lang) or ('-1' in vectOfLang[potentialNearestLang])):
            continue
        #sys.stderr.write("%s\n" %(vectOfLang[lang]))
        for i in range(len(vectOfLang[lang])):
            if(vectOfLang[lang][i] == vectOfLang[potentialNearestLang][i]):
                curIdenticalFeat += 1
        if(curIdenticalFeat > maxIdenticalFeat):
            maxIdenticalFeat = curIdenticalFeat
            nearestLang = potentialNearestLang   
    return nearestLang
    
def createVectOfLang(wals, langFile, featFile):
    codesLang = Lang.extractLanguages(langFile)
    df = pandas.read_csv(wals)
    idColumns = df.columns.values
    colToKeep = Feat.fct_indexColumnsToKeep(idColumns, featFile)
    colToKeep = colToKeep[1:]
    #sys.stderr.write("%d\t%d\t%s\n" %(cpt, maxSentences, cpt <= maxSentences)
    vectOfLang = dict()
    WALS = open(wals, "r")
    reader = csv.DictReader(WALS)
    for row in reader:
        curLang = row['wals_code']
        if(curLang not in codesLang):
            continue
        newVect = []
        nbFeat = 0
        for feat in colToKeep:
            curFeatValue = row[feat].split(" ")[0]
            if(curFeatValue == ''):
                curFeatValue = '-1'
            newVect.append(curFeatValue)
            nbFeat += 1
        vectOfLang[curLang] = newVect

    for lang in vectOfLang:
        if('-1' in vectOfLang[lang]):
            nearestLang = findNearestLang(vectOfLang, lang)
            #sys.stderr.write("%s/%s\n" %(lang, nearestLang))            
            #sys.stderr.write("%s\n%s\n" %(vectOfLang[lang], vectOfLang[nearestLang]))
            for i in range(len(vectOfLang[lang])):
                if(vectOfLang[lang][i] == '-1'):
                    vectOfLang[lang][i] = vectOfLang[nearestLang][i]
            
        
    newVect = ["-1"]*nbFeat
    vectOfLang["???"] = newVect
    return vectOfLang
    
def corr_UDX_WALS(filepathcorrespondanceUDX_WALS):
    dict_corr_UDX_WALS = dict()
    with open(filepathcorrespondanceUDX_WALS, "r") as corr:
        curLine = corr.readline()
        while(not fileCompletelyRead(curLine)):
            if(lineIsAComment(curLine)):
                curLine = corr.readline()
                continue
            
            curLine = curLine.split("\t")
            UDXcode = curLine[0]
            WALScode = curLine[1]
            dict_corr_UDX_WALS[UDXcode] = WALScode
            curLine = corr.readline()
    return dict_corr_UDX_WALS
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 6):
        sys.stderr("Error, pouit")
        exit(1)
    mcfFile = sys.argv[1]
    wals = sys.argv[2]
    langFile = sys.argv[3]
    featFile = sys.argv[4]
    filepathcorrespondanceUDX_WALS = sys.argv[5]
    dict_corr_UDX_WALS = corr_UDX_WALS(filepathcorrespondanceUDX_WALS)
    vectOfLang = createVectOfLang(wals, langFile, featFile)
    
    with open(mcfFile, "r") as mcf:
        curLine = mcf.readline()[:-1]
        while(not fileCompletelyRead(curLine)):
            if(lineIsAComment(curLine)):
                print(curLine[:-1])
                curLine = mcf.readline()[:-1]
                
            curLine = curLine.split("\t")
            curLangCode = curLine[0].split("_")
            if(len(curLangCode) > 1):
                curLangCode = '_'.join(curLangCode[:-1])
            else:
                curLangCode = curLangCode[0]
            curLangCodeWALS = curLangCode
            if(dict_corr_UDX_WALS.has_key(curLangCode)):
                curLangCode = dict_corr_UDX_WALS[curLangCode]
                if(vectOfLang.has_key(curLangCode)):
                    newLine = str("\t".join(vectOfLang[curLangCode]))+"\t"
            else:
                    newLine = str("\t".join(vectOfLang["???"]))+"\t"
            for curCol in curLine[1:]:
                newLine += curCol+"\t"
            print(newLine[:-1]+"\t"+curLangCodeWALS)
            curLine = mcf.readline()[:-1]
