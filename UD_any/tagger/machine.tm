Name : Tagger Machine
Dicts : tagger.dicts
%CLASSIFIERS
strategy strategy.cla
tagger tagger.cla
%STATES
strategy strategy
tagger tagger
%TRANSITIONS
strategy tagger MOVE tagger
tagger strategy *
