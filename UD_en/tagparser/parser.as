Default : SHIFT
REDUCE
EOS
LEFT _
LEFT acl
LEFT acl:relcl
LEFT advcl
LEFT advmod
LEFT amod
LEFT appos
LEFT aux
LEFT aux:pass
LEFT case
LEFT cc
LEFT ccomp
LEFT cc:preconj
LEFT compound
LEFT compound:prt
LEFT conj
LEFT cop
LEFT csubj
LEFT csubj:pass
LEFT dep
LEFT det
LEFT det:predet
LEFT discourse
LEFT dislocated
LEFT expl
LEFT fixed
LEFT flat
LEFT flat:foreign
LEFT goeswith
LEFT iobj
LEFT list
LEFT mark
LEFT nmod
LEFT nmod:npmod
LEFT nmod:poss
LEFT nmod:tmod
LEFT nsubj
LEFT nsubj:pass
LEFT nummod
LEFT obj
LEFT obl
LEFT obl:npmod
LEFT obl:tmod
LEFT orphan
LEFT parataxis
LEFT punct
LEFT reparandum
LEFT vocative
LEFT xcomp
RIGHT _
RIGHT acl
RIGHT acl:relcl
RIGHT advcl
RIGHT advmod
RIGHT amod
RIGHT appos
RIGHT aux
RIGHT aux:pass
RIGHT case
RIGHT cc
RIGHT ccomp
RIGHT cc:preconj
RIGHT compound
RIGHT compound:prt
RIGHT conj
RIGHT cop
RIGHT csubj
RIGHT csubj:pass
RIGHT dep
RIGHT det
RIGHT det:predet
RIGHT discourse
RIGHT dislocated
RIGHT expl
RIGHT fixed
RIGHT flat
RIGHT flat:foreign
RIGHT goeswith
RIGHT iobj
RIGHT list
RIGHT mark
RIGHT nmod
RIGHT nmod:npmod
RIGHT nmod:poss
RIGHT nmod:tmod
RIGHT nsubj
RIGHT nsubj:pass
RIGHT nummod
RIGHT obj
RIGHT obl
RIGHT obl:npmod
RIGHT obl:tmod
RIGHT orphan
RIGHT parataxis
RIGHT punct
RIGHT reparandum
RIGHT vocative
RIGHT xcomp
