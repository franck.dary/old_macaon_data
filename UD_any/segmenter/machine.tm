Name : Sentence Segmenter Machine
Dicts : segmenter.dicts
%CLASSIFIERS
strategy strategy.cla
segmenter segmenter.cla
%STATES
strategy strategy
segmenter segmenter
%TRANSITIONS
strategy segmenter *
segmenter strategy *
