#!/bin/bash

set -e
set -u
set -o pipefail

listPercent="01 03 05 10 20 50 100"


sed -i -E "s/^TEST=.+/TEST=..\/..\/data\/UD_French-FTB-full-2.3\/fr_ftb-ud-test.conllu/" data/Makefile
sed -i -E "s/^DEV=.+/DEV=..\/..\/data\/UD_French-FTB-full-2.3\/fr_ftb-ud-dev.conllu/" data/Makefile


# for curPercent in $listPercent
# do
#     sed -i -E "s/^TRAIN=.+/TRAIN=..\/..\/data\/UD_French-FTB-full-2.3\/ftb.train."$curPercent"percent.conllu/" data/Makefile
#     NAMEMODEL=FTB_${curPercent}_lex100
#     cd data
#     make clean & make without_w
#     sed -i -E "s/Parser_form.+$/Parser_form    100      Embeddings ..\/..\/..\/data\/embeddings\/monolingual\/dicts\/fr.dict/" ../parser/parser.dicts
#     echo "b.0.FORM" >> ../parser/parser.fm
    
#     mv train.mcf MCFfiles/train/train_$NAMEMODEL.mcf
#     mv dev.mcf MCFfiles/dev/dev_$NAMEMODEL.mcf
#     mv test.mcf MCFfiles/test/test_$NAMEMODEL.mcf
#     mv wpmlgfs.mcd MCDfiles/$NAMEMODEL.mcd

#     cd ../parser

#     mv train.bd dir_bd/train_$NAMEMODEL.bd
#     mv test.bd dir_bd/test_$NAMEMODEL.bd
#     mv parser.dicts dicts/$NAMEMODEL.dicts

#     mv parser.fm fm/$NAMEMODEL.fm

#     oargen -H see4c1 -n ${NAMEMODEL} -d ../outputCluster -t 96 -b --run macaon_train --tm machine.tm --bd dir_bd/train_$NAMEMODEL.bd --mcd ../../data/MCDfiles/$NAMEMODEL.mcd -T ../../data/MCFfiles/train/train_$NAMEMODEL.mcf --dev ../../data/MCFfiles/dev/dev_$NAMEMODEL.mcf --dicts dicts/$NAMEMODEL.dicts --featureModels Parser=fm/$NAMEMODEL.fm --expName parser_$NAMEMODEL --lang UD_X -n 10 --templateName parser
    
#     sleep 10

#     cd ../
#     NAMEMODEL=FTB_${curPercent}
#     cd data
#     make clean & make without_w
#     mv train.mcf MCFfiles/train/train_$NAMEMODEL.mcf
#     mv dev.mcf MCFfiles/dev/dev_$NAMEMODEL.mcf
#     mv test.mcf MCFfiles/test/test_$NAMEMODEL.mcf
#     mv wpmlgfs.mcd MCDfiles/$NAMEMODEL.mcd

#     cd ../parser

#     mv train.bd dir_bd/train_$NAMEMODEL.bd
#     mv test.bd dir_bd/test_$NAMEMODEL.bd
#     mv parser.dicts dicts/$NAMEMODEL.dicts

#     mv parser.fm fm/$NAMEMODEL.fm

#     oargen -H see4c1 -n ${NAMEMODEL} -d ../outputCluster -t 96 -b --run macaon_train --tm machine.tm --bd dir_bd/train_$NAMEMODEL.bd --mcd ../../data/MCDfiles/$NAMEMODEL.mcd -T ../../data/MCFfiles/train/train_$NAMEMODEL.mcf --dev ../../data/MCFfiles/dev/dev_$NAMEMODEL.mcf --dicts dicts/$NAMEMODEL.dicts --featureModels Parser=fm/$NAMEMODEL.fm --expName parser_$NAMEMODEL --lang UD_X -n 10 --templateName parser
#     sleep 10


#     cd ../
# done
curPercent="100"

sed -i -E "s/^TRAIN=.+/TRAIN=..\/..\/data\/UD_French-FTB-full-2.3\/ftb.train."$curPercent"percent.conllu/" data/Makefile
NAMEMODEL=FTB_${curPercent}_newParam
cd data
make clean & make without_w
sed -i -E "s/Parser_form.+$/Parser_form    100      Embeddings ..\/..\/..\/data\/embeddings\/monolingual\/dicts\/fr.dict/" ../parser/parser.dicts

mv train.mcf MCFfiles/train/train_$NAMEMODEL.mcf
mv dev.mcf MCFfiles/dev/dev_$NAMEMODEL.mcf
mv test.mcf MCFfiles/test/test_$NAMEMODEL.mcf
mv wpmlgfs.mcd MCDfiles/$NAMEMODEL.mcd

cd ../parser

mv train.bd dir_bd/train_$NAMEMODEL.bd
mv test.bd dir_bd/test_$NAMEMODEL.bd
mv parser.dicts dicts/$NAMEMODEL.dicts

mv parser.fm fm/$NAMEMODEL.fm

oargen -H see4c1 -n ${NAMEMODEL} -d ../outputCluster -t 96 -b --run macaon_train --tm machine.tm --bd dir_bd/train_$NAMEMODEL.bd --mcd ../../data/MCDfiles/$NAMEMODEL.mcd -T ../../data/MCFfiles/train/train_$NAMEMODEL.mcf --dev ../../data/MCFfiles/dev/dev_$NAMEMODEL.mcf --dicts dicts/$NAMEMODEL.dicts --featureModels Parser=fm/$NAMEMODEL.fm --expName parser_$NAMEMODEL --lang UD_X -n 10 --templateName parser

cd ../

sed -i -E "s/^TRAIN=.+/TRAIN=\$(CONLL_DIR)\/train.eqTokens.conllu/" data/Makefile
sed -i -E "s/^TEST=.+/TEST=\$(CONLL_DIR)\/test.conllu/" data/Makefile
sed -i -E "s/^DEV=.+/DEV=\$(CONLL_DIR)\/dev.eqTokens.conllu/" data/Makefile
