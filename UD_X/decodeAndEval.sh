#!/bin/bash


# listLang="pt ro ru sl sme sv tr uk ur vi zh"
# listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl no pl pt ro ru sl sme sv tr uk ur vi zh"

# listPercent="01 03 05 10 20 50 100"

# listlang="ar bg bxr ca cu cs da de el es et eu fa fi fr ga gl got he hi hr hu id it ja kk kmr ko lv nl no nob nno pl pt ro ru sk sl sme sv tr ug uk ur vi zh"

listLang="ar bg bxr ca cs da de el en es et eu fa fi fr ga he hi hr hu id it ja kmr ko lv nl nno nob pl pt ro ru sl sme sv tr uk ur vi zh"




# L_lex300
rm -f results_L_lex300 
touch results_L_lex300

for curLang in $listLang
do
    echo $curLang
    # bin/maca_tm_parser_L_${curLang}_lex300 data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/L_${curLang}_lex300.mcd > predictions/pred_L_${curLang}_lex300.mcf
    cd eval
    echo $curLang >> ../results_L_lex300
    ./launchEval.sh ../predictions/pred_L_${curLang}_lex300.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/L_${curLang}_lex300.mcd >> ../results_L_lex300
    echo -e "\n\n" >> ../results_L_lex300
    cd ../
done






# # listFam="Germanic Slavic Romance"
# # Family
# # rm -f results_Family
# # touch results_Family
# listFam="Romance"

# for curFam in $listFam
# do
#     echo $curFam
#     echo $curFam >> results_Family
	
#     for curLang in $listLang
#     do
# 	echo $curLang
# 	echo $curLang >> results_Family
	
# 	bin/maca_tm_parser_Family_${curFam} data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/Family_${curFam}.mcd > predictions/pred_Family_${curFam}_${curLang}.mcf
# 	cd eval
# 	./launchEval.sh ../predictions/pred_Family_${curFam}_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/Family_$curFam.mcd >> ../results_Family
# 	echo -e "\n\n" >> ../results_Family
# 	cd ../
#     done
# done




# # Lbar
# rm -f results_Lbar
# touch results_Lbar

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_Lbar_${curLang} data/MCFfiles/test/test_Lbar_${curLang}.mcf data/MCDfiles/Lbar_${curLang}.mcd > predictions/pred_Lbar_${curLang}.mcf
#     cd eval
#     echo $curLang >> ../results_Lbar
#     ./launchEval.sh ../predictions/pred_Lbar_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/Lbar_${curLang}.mcd >> ../results_Lbar
#     echo -e "\n\n" >> ../results_Lbar
#     cd ../
# done



# # # FTB
# # rm -f results_FTB
# # touch results_FTB

# for curPercent in $listPercent
# do
#     echo -e "\n\nDelex"${curPercent}
#     bin/maca_tm_parser_FTB_${curPercent} data/MCFfiles/test/test_FTB_${curPercent}.mcf data/MCDfiles/FTB_${curPercent}.mcd > predictions/pred_FTB_${curPercent}.mcf

#     echo -e "\n\nLex"${curPercent}

#     bin/maca_tm_parser_FTB_${curPercent}_lex100 data/MCFfiles/test/test_FTB_${curPercent}_lex100.mcf data/MCDfiles/FTB_${curPercent}_lex100.mcd > predictions/pred_FTB_${curPercent}_lex100.mcf

#     cd eval

#     echo -e "\n\nDelex"${curPercent}"\n\n" >> ../results_FTB
#     ./launchEval.sh ../predictions/pred_FTB_${curPercent}.mcf testFilesGoldConllu/test_FTB.conllu ../data/MCDfiles/FTB_${curPercent}.mcd >> ../results_FTB
#     echo -e "\n\nLex"${curPercent}"\n\n" >> ../results_FTB
#     ./launchEval.sh ../predictions/pred_FTB_${curPercent}_lex100.mcf testFilesGoldConllu/test_FTB.conllu ../data/MCDfiles/FTB_${curPercent}_lex100.mcd >> ../results_FTB
#     cd ../
# done


# # Wd
# rm -f results_SIGMA_Wd
# touch results_SIGMA_Wd

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_Wd data/MCFfiles/test/test_L_${curLang}_Wd.mcf data/MCDfiles/SIGMA_Wd.mcd > predictions/pred_SIGMA_Wd_${curLang}.mcf

#     cd eval
#     echo $curLang >> ../results_SIGMA_Wd
#     ./launchEval.sh ../predictions/pred_SIGMA_Wd_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_Wd.mcd >> ../results_SIGMA_Wd
#     echo -e "\n\n" >> ../results_SIGMA_Wd
#     cd ../
# done





# # ID
# rm -f results_SIGMA_ID 
# touch results_SIGMA_ID

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_ID data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/SIGMA_ID.mcd > predictions/pred_SIGMA_ID_${curLang}.mcf
#     cd eval
#     echo $curLang >> ../results_SIGMA_ID
#     ./launchEval.sh ../predictions/pred_SIGMA_ID_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_ID.mcd >> ../results_SIGMA_ID
#     echo -e "\n\n" >> ../results_SIGMA_ID
#     cd ../
# done





# # SIGMA L && L_lex
# rm -f results_L_lex100 # results_L
# touch results_L_lex100  # results_L

# for curLang in $listLang
# do
#     echo $curLang
#     # bin/maca_tm_parser_L_${curLang} data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/L_${curLang}.mcd > predictions/pred_L_${curLang}.mcf
#     bin/maca_tm_parser_L_${curLang}_lex100 data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/L_${curLang}_lex100.mcd > predictions/pred_L_${curLang}_lex100.mcf
#     cd eval
#     # echo $curLang >> ../results_L
#     echo $curLang >> ../results_L_lex100
#    #  ./launchEval.sh ../predictions/pred_L_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/L_${curLang}.mcd >> ../results_L
#     # echo -e "\n\n" >> ../results_L
#     ./launchEval.sh ../predictions/pred_L_${curLang}_lex100.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/L_${curLang}_lex100.mcd >> ../results_L_lex100
#     echo -e "\n\n" >> ../results_L_lex100
#     cd ../
# done




# # SIGMA, ID, W80, Wd, Wdf
# rm -f results_SIGMA results_SIGMA_ID results_SIGMA_W80  results_SIGMA_Wd results_SIGMA_Wdf
# touch results_SIGMA results_SIGMA_ID results_SIGMA_W80  results_SIGMA_Wd results_SIGMA_Wdf

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/SIGMA.mcd > predictions/pred_SIGMA_${curLang}.mcf

#     bin/maca_tm_parser_SIGMA_ID data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/SIGMA_ID.mcd > predictions/pred_SIGMA_ID_${curLang}.mcf

#     bin/maca_tm_parser_SIGMA_W80 data/MCFfiles/test/test_L_${curLang}_W80.mcf data/MCDfiles/SIGMA_W80.mcd > predictions/pred_SIGMA_W80_${curLang}.mcf

#     bin/maca_tm_parser_SIGMA_Wd data/MCFfiles/test/test_L_${curLang}_Wd.mcf data/MCDfiles/SIGMA_Wd.mcd > predictions/pred_SIGMA_Wd_${curLang}.mcf

#     bin/maca_tm_parser_SIGMA_Wdf data/MCFfiles/test/test_L_${curLang}_Wdf.mcf data/MCDfiles/SIGMA_Wdf.mcd > predictions/pred_SIGMA_Wdf_${curLang}.mcf

#     cd eval
#     echo $curLang >> ../results_SIGMA
#     echo $curLang >> ../results_SIGMA_ID
#     echo $curLang >> ../results_SIGMA_W80
#     echo $curLang >> ../results_SIGMA_Wd
#     echo $curLang >> ../results_SIGMA_Wdf
#     ./launchEval.sh ../predictions/pred_SIGMA_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA.mcd >> ../results_SIGMA
#     ./launchEval.sh ../predictions/pred_SIGMA_ID_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_ID.mcd >> ../results_SIGMA_ID
#     ./launchEval.sh ../predictions/pred_SIGMA_W80_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_W80.mcd >> ../results_SIGMA_W80
#     ./launchEval.sh ../predictions/pred_SIGMA_Wd_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_Wd.mcd >> ../results_SIGMA_Wd
#     ./launchEval.sh ../predictions/pred_SIGMA_Wdf_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_Wdf.mcd >> ../results_SIGMA_Wdf
#     echo -e "\n\n" >> ../results_SIGMA
#     echo -e "\n\n" >> ../results_SIGMA_ID
#     echo -e "\n\n" >> ../results_SIGMA_W80
#     echo -e "\n\n" >> ../results_SIGMA_Wd
#     echo -e "\n\n" >> ../results_SIGMA_Wdf
#     cd ../
# done

















# # SIGMA ID embed37
# # rm -f results_SIGMA_ID_embed37
# # touch results_SIGMA_ID_embed37

# for curLang in $listLang
# do
#     echo $curLang
#     # bin/maca_tm_parser_SIGMA_ID_embed37 data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/SIGMA_ID.mcd > predictions/pred_SIGMA_ID_L_${curLang}_embed37.mcf
#     cd eval
#     echo $curLang >> ../results_SIGMA_ID_embed37
#     ./launchEval.sh ../predictions/pred_SIGMA_ID_L_${curLang}_embed37.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_ID.mcd >> ../results_SIGMA_ID_embed37
#     echo -e "\n\n" >> ../results_SIGMA_ID_embed37
#     cd ../
# done







# # SIGMA Wn WoneHot
# rm -f results_SIGMA_Wn_WoneHot
# touch results_SIGMA_Wn_WoneHot

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_Wn_WoneHot data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/SIGMA_Wn.mcd > predictions/pred_SIGMA_Wn_L_${curLang}_WoneHot.mcf
#     cd eval
#     echo $curLang >> ../results_SIGMA_Wn_WoneHot
#     ./launchEval.sh ../predictions/pred_SIGMA_Wn_L_${curLang}_WoneHot.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_Wn.mcd >> ../results_SIGMA_Wn_WoneHot
#     echo -e "\n\n" >> ../results_SIGMA_Wn_WoneHot
#     cd ../
# done




# # SIGMA W80 WoneHot
# #rm -f results_SIGMA_W80_WoneHot
# #touch results_SIGMA_W80_WoneHot

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_W80_WoneHot data/MCFfiles/test/test_L_${curLang}_W80.mcf data/MCDfiles/SIGMA_W80.mcd > predictions/pred_SIGMA_W80_L_${curLang}_WoneHot.mcf
#     cd eval
#     echo $curLang >> ../results_SIGMA_W80_WoneHot
#     ./launchEval.sh ../predictions/pred_SIGMA_W80_L_${curLang}_WoneHot.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_W80.mcd  >> ../results_SIGMA_W80_WoneHot
#     echo -e "\n\n" >> ../results_SIGMA_W80_WoneHot
#     cd ../
# done




# # Lbar
# rm -f results_tests_Lbar
# touch results_tests_Lbar

# for curLang in $listLang
# do
#     echo $curLang
#     time bin/maca_tm_parser_Lbar_${curLang} data/MCFfiles/test/test_L_${curLang}.mcf data/MCDfiles/Lbar_${curLang}.mcd > predictions/pred_Lbar_${curLang}.mcf 
#     cd eval
#     echo $curLang >> ../results_tests_Lbar
#     ./launchEval.sh ../predictions/pred_Lbar_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/Lbar_${curLang}.mcd >> ../results_tests_Lbar
#     echo -e "\n\n" >> ../results_tests_Lbar
#     cd ../
# done


# # Lbar+W80
# rm -f results_tests_Lbar_W80
# touch results_tests_Lbar_W80

# for curLang in $listLang
# do
#     echo $curLang
#     time bin/maca_tm_parser_Lbar_${curLang}_W80 data/MCFfiles/test/test_L_${curLang}_W80.mcf data/MCDfiles/Lbar_${curLang}_W80.mcd > predictions/pred_Lbar_${curLang}_W80.mcf 
#     cd eval
#     echo $curLang >> ../results_tests_Lbar_W80
#     ./launchEval.sh ../predictions/pred_Lbar_${curLang}_W80.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/Lbar_${curLang}_W80.mcd >> ../results_tests_Lbar_W80
#     echo -e "\n\n" >> ../results_tests_Lbar_W80
#     cd ../
# done



# # SIGMA Wd
# rm -f results_SIGMA_Wd
# touch results_SIGMA_Wd

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_Wd data/MCFfiles/test/test_L_${curLang}_Wd.mcf data/MCDfiles/SIGMA_Wd.mcd > predictions/pred_SIGMA_Wd_L_${curLang}.mcf
#     cd eval
#     echo $curLang >> ../results_SIGMA_Wd
#     ./launchEval.sh ../predictions/pred_SIGMA_Wd_L_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_Wd.mcd >> ../results_SIGMA_Wd
#     echo -e "\n\n" >> ../results_SIGMA_Wd
#     cd ../
# done



# # SIGMA Wdf
# rm -f results_SIGMA_Wdf
# touch results_SIGMA_Wdf

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_Wdf data/MCFfiles/test/test_L_${curLang}_Wdf.mcf data/MCDfiles/SIGMA_Wdf.mcd > predictions/pred_SIGMA_Wdf_L_${curLang}.mcf
#     cd eval
#     echo $curLang >> ../results_SIGMA_Wdf
#     ./launchEval.sh ../predictions/pred_SIGMA_Wdf_L_${curLang}.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_Wdf.mcd >> ../results_SIGMA_Wdf
#     echo -e "\n\n" >> ../results_SIGMA_Wdf
#     cd ../
# done






# # L
# rm -f results_tests_L_10iters
# touch results_tests_L_10iters

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_L_${curLang}_10iters eval/MCFfiles/test_L_${curLang}.mcf data/MCDfiles/L_${curLang}_10iters.mcd > predictions/pred_L_${curLang}_10iters.mcf 
#     cd eval
#     echo $curLang >> ../results_tests_L_10iters
#     ./launchEval.sh ../predictions/pred_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/L_${curLang}_10iters.mcd >> ../results_tests_L_10iters
#     echo -e "\n\n" >> ../results_tests_10iters
#     cd ../
# done


# # SIGMA
# # rm -f results_SIGMA
# # touch results_SIGMA

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_10iters data/MCFfiles/test/test_L_${curLang}_10iters.mcf data/MCDfiles/SIGMA.mcd --showFeatureRepresentation 2 2> predictions/activations/test/SIGMA/pred_SIGMA_L_${curLang}_10iters.vect > tmp #pred_SIGMA_L_${curLang}_10iters.mcf
#     # cd eval
#     # echo $curLang >> ../results_SIGMA
#     # ./launchEval.sh ../predictions/pred_SIGMA_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA.mcd >> ../results_SIGMA
#     # echo -e "\n\n" >> ../results_SIGMA
#     # cd ../
# done



# # SIGMA Wn
# # rm -f results_SIGMA_Wn
# # touch results_SIGMA_Wn

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_W_10iters data/MCFfiles/test/test_L_${curLang}_10iters.mcf data/MCDfiles/SIGMA_W.mcd --showFeatureRepresentation 2 > tmp 2> predictions/activations/test/SIGMA_Wn/pred_SIGMA_W_L_${curLang}_10iters.vect
#     # cd eval
#     # echo $curLang >> ../results_SIGMA_Wn
#     # ./launchEval.sh ../predictions/pred_SIGMA_W_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_W.mcd >> ../results_SIGMA_Wn
#     # echo -e "\n\n" >> ../results_SIGMA_Wn
#     # cd ../
# done



# # SIGMA W80
# # rm -f results_SIGMA_W80
# # touch results_SIGMA_W80
# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_W80_10iters data/MCFfiles/test/test_L_${curLang}_W80.mcf data/MCDfiles/SIGMA_W80.mcd --showFeatureRepresentation 2 2> predictions/activations/test/SIGMA_W80/pred_SIGMA_W80_L_${curLang}_10iters.vect > tmp
#     # cd eval
#     # echo $curLang >> ../results_SIGMA_W80
#     # ./launchEval.sh ../predictions/pred_SIGMA_W80_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_W80.mcd >> ../results_SIGMA_W80
#     # echo -e "\n\n" >> ../results_SIGMA_Wn
#     # cd ../
# done



# # SIGMA ID
# # rm -f results_SIGMA_ID
# # touch results_SIGMA_ID

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_ID_10iters data/MCFfiles/test/test_L_${curLang}_ID.mcf data/MCDfiles/SIGMA_ID.mcd --showFeatureRepresentation 2 2> predictions/activations/test/SIGMA_ID/pred_SIGMA_ID_L_${curLang}_10iters.vect > tmp
#     # cd eval
#     # echo $curLang >> ../results_SIGMA_ID
#     # ./launchEval.sh ../predictions/pred_SIGMA_ID_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_ID.mcd >> ../results_SIGMA_ID
#     # echo -e "\n\n" >> ../results_SIGMA_ID
#     # cd ../
# done

# #SIGMA Wn ID

# # rm -f results_SIGMA_Wn_ID
# # touch results_SIGMA_Wn_ID

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_W_ID_10iters data/MCFfiles/test/test_L_${curLang}_ID.mcf data/MCDfiles/SIGMA_W_ID.mcd --showFeatureRepresentation 2 2> predictions/activations/test/SIGMA_Wn_ID/pred_SIGMA_Wn_ID_L_${curLang}_10iters.vect > tmp
#     # cd eval
#     # echo $curLang >> ../results_SIGMA_Wn_ID
#     # ./launchEval.sh ../predictions/pred_SIGMA_Wn_ID_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_ID.mcd >> ../results_SIGMA_Wn_ID
#     # echo -e "\n\n" >> ../results_SIGMA_Wn_ID
#     # cd ../
# done

# #SIGMA W80 ID

# # rm -f results_SIGMA_W80_ID
# # touch results_SIGMA_W80_ID

# for curLang in $listLang
# do
#     echo $curLang
#     bin/maca_tm_parser_SIGMA_W80_ID_10iters data/MCFfiles/test/test_L_${curLang}_W80_ID.mcf data/MCDfiles/SIGMA_W80_ID.mcd --showFeatureRepresentation 2 2> predictions/activations/test/SIGMA_W80_ID/pred_SIGMA_W80_ID_L_${curLang}_10iters.vect > tmp
#     # cd eval
#     # echo $curLang >> ../results_SIGMA_W80_ID
#     # ./launchEval.sh ../predictions/pred_SIGMA_W80_ID_L_${curLang}_10iters.mcf testFilesGoldConllu/test_L_${curLang}.conllu ../data/MCDfiles/SIGMA_W80_ID.mcd >> ../results_SIGMA_W80_ID
#     # echo -e "\n\n" >> ../results_SIGMA_W80_ID
#     # cd ../
# done
