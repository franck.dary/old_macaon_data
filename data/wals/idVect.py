#!/usr/bin/python
# -*- coding: utf-8 -*-



from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import collections
import random

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def distanceBetween2Vect(vect1, vect2):
    if(len(vect1) != len(vect2)):
        sys.stderr.write("Error length vectors\n")
    cptDiff = 0
    for i in range(len(vect1)):
        if(vect1[i] != vect2[i]):
            cptDiff+=1
    return cptDiff
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 2):
        sys.stderr("Error, give the matrix of vectors from the wals")
        exit(1)
    filePathMatrixWals = sys.argv[1]
    
    vectOfLang = dict()
    
    with open(filePathMatrixWals, "r") as matrix:
        curLine = matrix.readline().strip()
        curLine = matrix.readline().strip()
        while(not fileCompletelyRead(curLine)):
            curLang = curLine.split(",")[0]
            curVect = curLine.split(",")
            curVect.pop(0)
            vectOfLang[curLang]=curVect
            curLine = matrix.readline().strip()
    vectOfLang = collections.OrderedDict(sorted(vectOfLang.items()))
    
    idVects = dict() 
    
    for lang1 in vectOfLang:
        vect1 = vectOfLang[lang1]
        for lang2 in vectOfLang:
            if(lang1 == lang2):
                continue
            vect2 = vectOfLang[lang2]
            dist = distanceBetween2Vect(vect1, vect2)
            if(dist == 0):
                if(str(vect1) in idVects):
                    print("---", lang1, lang2)
                    idVects[str(vect1)].add(lang1)
                    idVects[str(vect1)].add(lang2)
                else:
                    idVects[str(vect1)] = set([lang1, lang2])
    print(idVects)
