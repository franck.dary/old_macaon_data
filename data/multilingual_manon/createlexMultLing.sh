cut -f 11 train.eqTokens.conllu | cut -f 1 -d '_' > tmp_lang
cut -f 2 train.eqTokens.conllu > tmp_word
cut -f 1 train.eqTokens.conllu > tmp_before
cut -f 3,4,5,6,7,8,9,10,11 train.eqTokens.conllu > tmp_after
paste -d '_' tmp_lang tmp_word | awk '{print tolower($0)}' > tmp_newWord
paste tmp_before tmp_newWord tmp_after > train.eqTokens.lexMultLing.conllu 
sed -i "s/\t_\t//" train.eqTokens.lexMultLing.conllu 

cut -f 11 dev.eqTokens.conllu | cut -f 1 -d '_' > tmp_lang
cut -f 2 dev.eqTokens.conllu > tmp_word
cut -f 1 dev.eqTokens.conllu > tmp_before
cut -f 3,4,5,6,7,8,9,10,11 dev.eqTokens.conllu > tmp_after
paste -d '_' tmp_lang tmp_word | awk '{print tolower($0)}' > tmp_newWord
paste tmp_before tmp_newWord tmp_after > dev.eqTokens.lexMultLing.conllu 
sed -i "s/\t_\t//" dev.eqTokens.lexMultLing.conllu 
