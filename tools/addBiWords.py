#! /usr/bin/python

import sys

def printUsageAndExit() :
  print("Usage : %s file.conllu reference.conllu"%sys.argv[0])
  exit(1)

def main() :
  if len(sys.argv) != 3 :
    printUsageAndExit()

  previousId = -1

  baseFile = open(sys.argv[1], "r")
  refFile = open(sys.argv[2], "r")

  while (True) :
    while (True) :
      refLine = refFile.readline()
      if refLine == "" :
        break
      if refLine.split('\t')[0][0].isdigit() :
        break

    while (True) :
      baseLine = baseFile.readline()
      if baseLine == "" :
        break
      if baseLine.split('\t')[0][0].isdigit() :
        break

    if refLine == "" or baseLine == "" :
      break

    idRef = refLine.split('\t')[0]
    idBase = baseLine.split('\t')[0]

    if int(idBase) < int(previousId) :
      print("")

    if len(idRef.split('-')) > 1 :
      print(refLine,end="")
      print(baseLine,end="")
      while (True) :
        refLine = refFile.readline()
        if refLine == "" :
          break
        if refLine.split('\t')[0][0].isdigit() :
          break
    else :
      print(baseLine,end="")

    previousId = int(idBase)

  print("")

  baseFile.close()
  refFile.close()

if __name__ == "__main__" :
  main()
