#!/usr/bin/python3
# -*- coding: utf-8 -*-

import fileinput
import sys
import re
import csv
import os
import string
import pandas as pd
import collections

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def createLabelsList(filepathListLabel):
    listLabel = []
    with open(filepathListLabel, "r", encoding="utf8") as listLab:
        curLine = listLab.readline()
        while(not fileCompletelyRead(curLine)):
            if(lineIsAComment(curLine)):
                curLine = listLab.readline()
                continue
            listLabel.append(curLine.strip())
            curLine = listLab.readline()
    return listLabel
    
def updateDictVectorLang(dictVectorLang):
    for k in dictVectorLang:
        if("freq" in str(k)):
            dictVectorLang[k].append(0)
        else:
            dictVectorLang[k].append([])
    
    
if __name__ == '__main__':
    
    if(len(sys.argv) < 3):
        sys.stderr.write("Error, give the list of existing labels, then the list containing the list of dependence (one line : ID GOV LABEL). Each language separrate by ------pathLanguage. You can also give the option --freq if you want Wdf\n")
        exit(1)

    filepathListLabel = sys.argv[1]
    filepathListDep = sys.argv[2]
    
    labelsList = createLabelsList(filepathListLabel)
    
    boolFreq = False
    
    if(len(sys.argv) == 4):
        boolFreq = True
        
    listLang = [] # [ar,bg,ca,...]
    nbDepEachLang = [] # nb dependences in each lang (index same lsit lang) . If 20 dep in ar, 23 in bg and 6 in ca : [20,23,6,...]
    # listColumnVector = [] #['L_acl', 'R_acl', 'L_acl_freq',...]
    dictVectorLang = collections.OrderedDict() # dictVectorLang[L_acl][index_ar] = [2,3,2,5,1,2,...] where each number is the distance between gov and current fir each L_acl relation. For frequence : dictVectorLang[L_acl_freq][index_ar] = 0.01% len(dictVectorLang[L_acl][index_ar])/nbDepEachLang[index_ar]
    
    for curLabel in labelsList:
        keyLeft = "L_"+curLabel
        keyRight = "R_"+curLabel
        dictVectorLang[keyLeft] = []
        dictVectorLang[keyRight] = []
        
        if(boolFreq):
            keyLeftFreq = "L_"+curLabel+"_freq"
            keyRightFreq = "R_"+curLabel+"_freq"
            dictVectorLang[keyLeftFreq] = []
            dictVectorLang[keyRightFreq] = []
    
    with open(filepathListDep, "r", encoding="utf8") as listDep:
        curLine = listDep.readline()
        if(curLine[:6] != "------"):
            sys.stderr.write("Error %s\n" %curLine[:6])
            exit(1)
        while(not fileCompletelyRead(curLine)):
            if(lineIsAComment(curLine) or curLine == "\n"):
                curLine = listDep.readline()
                continue
            if(curLine[:6] == "------"):
                curLang = curLine[7:].split("/")[-1].split(".")[0].split("-")[0].split("_")[0]
                if(curLang not in listLang):
                    listLang.append(curLang)
                    nbDepEachLang.append(0)
                    updateDictVectorLang(dictVectorLang)
                indexCurLang = listLang.index(curLang)
                curLine = listDep.readline()
                continue
            curLine = curLine.strip().split("\t")
            curID = curLine[0]
            if("-" in curID or "." in curID):
                curLine = listDep.readline()
                continue
            curID = int(curID)
            curGov = int(curLine[1])
            curLabel = curLine[2].split(":")[0]
            if(curID < curGov):
                curKey = "L_"+curLabel
                dictVectorLang[curKey][indexCurLang].append(curGov-curID)
            elif(curID > curGov):
                curKey = "R_"+curLabel
                dictVectorLang[curKey][indexCurLang].append(curID-curGov)
            else:
                sys.stderr.write("Error ID and GOV identical\n")
                exit(1)
            nbDepEachLang[indexCurLang]+=1
            if(boolFreq):
                curKeyFreq = curKey + "_freq"
                dictVectorLang[curKeyFreq][indexCurLang] = len(dictVectorLang[curKey][indexCurLang])/nbDepEachLang[indexCurLang]
                
            
            curLine = listDep.readline()
            
            
            
    for curKey in dictVectorLang:
        if(not "freq" in str(curKey)):
            for indexCurLang in range(len(listLang)):
                if(len(dictVectorLang[curKey][indexCurLang]) != 0):
                    dictVectorLang[curKey][indexCurLang] = sum(dictVectorLang[curKey][indexCurLang])*1.0/len(dictVectorLang[curKey][indexCurLang])
                else:
                    dictVectorLang[curKey][indexCurLang] = 0
        else:
            dictVectorLang[curKey][indexCurLang] *= 100
        
    df = pd.DataFrame(index=listLang, data=dictVectorLang)
    df.to_csv(sys.stdout)
