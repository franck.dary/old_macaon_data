#!/usr/bin/env python2
# create a crf-compatible pos/word lexicon from a corpus file in the CoNLL 2009 format
import sys
from collections import defaultdict

wordtag_counter = defaultdict(lambda: defaultdict(int))
# count the wordtag pairs in the dictionary
for line in sys.stdin:
    line = line.strip()
    if line:
        # keep only word and tag
        # cut -f2,5
        cont = line.split('\t')
        cont = [cont[1], cont[4]]
        # sed 's/ /_/g'
        cont = map(lambda x: x.replace(' ', '_'), cont)
        # perl -ne '{($k,$v)=split;$x{$k}{$v}++}END{for $w(keys %x){if($w eq ""){next;}printf("%s",$w);for $t(sort (keys %{$x{$w}})){printf("\t%s",$t)}print "\n";}} | LANG=C sort
        word = cont[0]
        tag = cont[1]
        wordtag_counter[word][tag] += 1
# output the word tags lexicon
for w in sorted(wordtag_counter):
    ts = sorted(wordtag_counter[w])
    print "{w}\t{ts}".format(w=w, ts='\t'.join(ts))
