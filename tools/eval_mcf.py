#! /usr/bin/python3

import sys

COLSIZE1 = 20
COLSIZE = 10

class ColumnResults :
  def  __init__(self, index, name, vocabs) :
    self.index = index
    self.name = name
    self.good = 0
    self.total = 0
    self.longestVocab = 0
    self.perVocabGood = dict(vocabs[index])
    self.perVocabTotal = dict(vocabs[index])
    self.perVocabTotalPred = dict(vocabs[index])
    for token in vocabs[index] :
      self.perVocabGood[token] = 0
      self.perVocabTotal[token] = 0
      self.perVocabTotalPred[token] = 0
      self.longestVocab = max(self.longestVocab, len(token))

  def computeResults(self) :
    self.acc = 100.0*self.good / self.total
    self.perVocabAcc = dict(self.perVocabTotal)
    self.perVocabRec = dict(self.perVocabTotal)
    test = 0
    for token in self.perVocabTotal :
      if self.perVocabTotal[token] == 0 :
        self.perVocabRec[token] = 100.0
      else :
        self.perVocabRec[token] = 100.0*self.perVocabGood[token] / self.perVocabTotal[token]
      if self.perVocabTotalPred[token] == 0 :
        self.perVocabAcc[token] = 100.0
      else :
        self.perVocabAcc[token] = 100.0*self.perVocabGood[token] / self.perVocabTotalPred[token]

def printUsageAndExit() :
  print("Usage : ",sys.argv[0]," file.mcd gold.mcf predictions.mcf [OPTIONS]",file=sys.stderr)
  print("[OPTIONS] : ",file=sys.stderr)
  print("\t-h, --help\n\t\tPrint this help",file=sys.stderr)
  print("\t--printHeader\n\t\tPrint each column name",file=sys.stderr)
  print("\t--keepPunct columnName\n\t\tDo not ignore punctuation for the column 'columnName'",file=sys.stderr)
  print("\t--keepPunctAll\n\t\tDo not ignore punctuation",file=sys.stderr)
  print("\t--ignore column\n\t\tIgnore column",file=sys.stderr)
  print("\t--details column\n\t\tPrint per label details for column",file=sys.stderr)
  print("\t--toolname name\n\t\tSpecify the name of the tool being tested",file=sys.stderr)
  print("\t--relative column1 column2\n\t\tFor each line, consider that 'column1' was correctly predicted only if 'column2' was also correctly predicted",file=sys.stderr)
  exit(1)

def readMCD(mcdFilename) :
  columnNames = {}
  columnIndexes = {}
  ignorePunct = {}
  details = {}
  ignore = {}
  for line in open(mcdFilename, "r", encoding="utf-8") :
    if len(line) == 0 :
      continue
    if line[-1] == '\n' :
      line = line[:-1]
    split = line.split()

    if len(split) < 2 or split[0][0] == '#' :
      continue

    if len(split) > 2 :
      print("Ill formated mcd line'",line,"'",file=sys.stderr)
      print("Aborting.",file=sys.stderr)
      exit(1)

    columnNames[int(split[0])] = split[1]
    columnIndexes[split[1]] = int(split[0])
    ignorePunct[split[1]] = True
    details[split[1]] = False
    ignore[split[1]] = False

  return columnNames, columnIndexes, ignorePunct, details, ignore

def readMCF(mcfFilename) :
  columns = []
  for line in open(mcfFilename, "r", encoding="utf-8") :
    if len(line) == 0 :
      continue
    if line[-1] == '\n' :
      line = line[:-1]
    split = line.split("\t")
    columns.append(split)

  return columns

def checkFormats(gold, pred) :
  if len(gold) == 0 or len(pred) == 0 :
    print("Invalid mcf format : empty file. Aborting",file=sys.stderr)
    exit(1)
  if len(gold) != len(pred) :
    print("Invalid mcf format : not the same number of lines pred=%d gold=%d. Aborting"%(len(pred),len(gold)),file=sys.stderr)
    exit(1)
  size1 = len(gold[0])
  size2 = len(pred[0])
  for i in range(len(gold)) :
    if len(gold[i]) != size1 or len(pred[i]) != size2 :
      print("Invalid mcf format : not the same number of columns on line",i,". Aborting.",file=sys.stderr)
      exit(1)

def getVocabs(gold, pred) :
  vocabs = [{} for _ in gold[0]]

  for line in gold :
    for i in range(len(line)) :
      vocabs[i][line[i]] = vocabs[i].get(line[i], 0) + 1

  for line in pred :
    for i in range(len(line)) :
      vocabs[i][line[i]] = vocabs[i].get(line[i], 0) + 1

  return vocabs

def getResultsPerColumn(columnNames, gold, pred, vocabPerColumn, ignorePunct, relativeTo) :
  nbCols = min(len(gold[0]), len(pred[0]))
  columns = []
  for i in range(nbCols) :
    columns.append(ColumnResults(i, columnNames[i], vocabPerColumn))

  for i in range(len(gold)) :
    isPunct = False
    if lineIsPunct(gold[i]) :
      isPunct = True
    colWasGood = [False for _ in range(nbCols)]
    for j in range(nbCols) :
      if isPunct and ignorePunct[columnNames[j]] :
        continue
      tokenGold = gold[i][j]
      tokenPred = pred[i][j]

      columns[j].perVocabTotalPred[tokenPred] += 1
      columns[j].perVocabTotal[tokenGold] += 1
      columns[j].total += 1
      if tokenGold.lower() == tokenPred.lower() :
        columns[j].perVocabGood[tokenGold] += 1
        if j not in relativeTo or colWasGood[relativeTo[j]] :
          columns[j].good += 1
          colWasGood[j] = True

  for col in columns :
    col.computeResults()

  return columns

def printHeader(columnNames, relativeTo, ignore) :
  keys = list(columnNames.keys())
  keys.sort()
  print("tool"+" "*(COLSIZE1-len("tool")),end=" ")
  for key in keys :
    name = columnNames[key]
    if ignore[name] :
      continue
    if key in relativeTo :
      name += "|" + columnNames[relativeTo[key]]
    print(name+" "*(COLSIZE-len(name)),end=" ")
  print("")

def cutMCD(columnNames, columnIndexes, pred) :
  keys = list(columnNames.keys())
  for key in keys :
    if key >= len(pred[0]) :
      columnNames.pop(key)

  keys = list(columnIndexes.keys())
  for key in keys :
    if columnIndexes[key] not in columnNames :
      columnIndexes.pop(key)

allPunct = ["``",",",":",".","''","LRB","RRB","PCT","PONCT","ponctw","poncts","PUNCT"]
def lineIsPunct(line) :
  for token in line :
    if token in allPunct :
      return True

  return False

################################################################################

for arg in sys.argv :
  if arg == "-h" or arg == "--help" :
    printUsageAndExit()

if len(sys.argv) < 4 :
  printUsageAndExit()

mcdFilename = sys.argv[1]
goldFilename = sys.argv[2]
predFilename = sys.argv[3]
mustPrintHeader = False

columnNames, columnIndexes, ignorePunct, details, ignore = readMCD(mcdFilename)

gold = readMCF(goldFilename)
pred = readMCF(predFilename)
checkFormats(gold, pred)

i = 4
relativeTo = {}
toolName = "__tool__"
while i < len(sys.argv) :
  arg = sys.argv[i]
  goodArg = False
  if arg == "--printHeader" :
    goodArg = True
    mustPrintHeader = True
  if arg == "--keepPunctAll" :
    goodArg = True
    for key in ignorePunct :
      ignorePunct[key] = False
  if arg == "--keepPunct" :
    goodArg = True
    if i+1 >= len(sys.argv) :
      print("Missing argument value '%s'"%arg,file=sys.stderr)
      printUsageAndExit()
    colName = sys.argv[i+1]
    if colName in ignorePunct :
      ignorePunct[colName] = False
    else :
      print("Invalid column name '%s' for argument '%s'"%(colName, arg),file=sys.stderr)
      printUsageAndExit()
    i += 1
  if arg == "--details" :
    goodArg = True
    if i+1 >= len(sys.argv) :
      print("Missing argument value '%s'"%arg,file=sys.stderr)
      printUsageAndExit()
    colName = sys.argv[i+1]
    if colName in details :
      details[colName] = True
    else :
      print("Invalid column name '%s' for argument '%s'"%(colName, arg),file=sys.stderr)
      printUsageAndExit()
    i += 1
  if arg == "--ignore" :
    goodArg = True
    if i+1 >= len(sys.argv) :
      print("Missing argument value '%s'"%arg,file=sys.stderr)
      printUsageAndExit()
    colName = sys.argv[i+1]
    if colName in ignore :
      ignore[colName] = True
    else :
      print("Invalid column name '%s' for argument '%s'"%(colName, arg),file=sys.stderr)
      printUsageAndExit()
    i += 1
  if arg == "--toolname" :
    goodArg = True
    if i+1 >= len(sys.argv) :
      print("Missing argument value '%s'"%arg,file=sys.stderr)
      printUsageAndExit()
    toolName = sys.argv[i+1]
    i += 1

  if arg == "--relative" :
    goodArg = True
    if i+2 >= len(sys.argv) :
      print("Missing argument values '%s'"%arg,file=sys.stderr)
      printUsageAndExit()
    firstColName = sys.argv[i+1]
    secondColName = sys.argv[i+2]
    firstColIndex = columnIndexes.get(firstColName,-1)
    secondColIndex = columnIndexes.get(secondColName, -1)
    relativeTo[firstColIndex] = secondColIndex
    i += 2
  if not goodArg :
    print("Invalid argument '%s'"%arg,file=sys.stderr)
    printUsageAndExit()
  i += 1

if mustPrintHeader :
  printHeader(columnNames,relativeTo,ignore)

cutMCD(columnNames, columnIndexes, pred)

vocabPerColumn = getVocabs(gold, pred)

results = getResultsPerColumn(columnNames, gold, pred, vocabPerColumn, ignorePunct, relativeTo)

print(toolName+" "*(COLSIZE1-len(toolName)),end=" ")
for column in results :
  if ignore[column.name] :
    continue
  tmp = "%.2f%%" % column.acc
  print(tmp+" "*(COLSIZE-len(tmp)),end=" ")
print("")

for column in results :
  if details[column.name] :
    print("/\\ "+column.name+" %.2f%% Accuracy :" % column.acc)
    toPrint = []
    for token in column.perVocabAcc :
      acc = column.perVocabAcc[token]
      rec = column.perVocabRec[token]
      total = column.perVocabTotal[token]
      if acc + rec == 0 :
        fMeasure = "%6.2f" % 0.0
      else :
        fMeasure = "%6.2f" % (2 * (acc*rec) / (acc+rec))
      toPrint.append([token+" "*(1+column.longestVocab-len(token)),"%6.2f" % acc,"%6.2f" % rec, fMeasure, total])

    toPrint.sort(key=lambda x: x[3],reverse=True)

    for line in toPrint :
      print(line[0],end=" ")
      print(line[1]+"% Acc",end=" ")
      print(line[2]+"% Rec",end=" ")
      print(line[3]+"% F1",end=" ")
      print("# "+str(line[4]))

