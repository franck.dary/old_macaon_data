WRITE b.0 MORPHO _
WRITE b.0 MORPHO Definite=Def|Gender=Fem|Number=Plur|Person=3|PronType=Art
WRITE b.0 MORPHO Definite=Def|Gender=Fem|Number=Plur|PronType=Art
WRITE b.0 MORPHO Definite=Def|Gender=Fem|Number=Sing|Person=3|PronType=Art
WRITE b.0 MORPHO Definite=Def|Gender=Fem|Number=Sing|PronType=Art
WRITE b.0 MORPHO Definite=Def|Gender=Masc|Number=Plur|PronType=Art
WRITE b.0 MORPHO Definite=Def|Gender=Masc|Number=Sing|PronType=Art
WRITE b.0 MORPHO Definite=Def|Number=Plur|PronType=Art
WRITE b.0 MORPHO Definite=Def|Number=Sing|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Gender=Fem|Number=Plur|Person=3|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Gender=Fem|Number=Plur|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Gender=Fem|Number=Sing|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Gender=Masc|Number=Plur|Person=3|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Gender=Masc|Number=Plur|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Gender=Masc|Number=Sing|PronType=Art
WRITE b.0 MORPHO Definite=Ind|Number=Plur|PronType=Art
WRITE b.0 MORPHO Definite=Ind|PronType=Art
WRITE b.0 MORPHO Gender=Fem
WRITE b.0 MORPHO Gender=Fem|Number=Plur
WRITE b.0 MORPHO Gender=Fem|Number=Plur|NumType=Card
WRITE b.0 MORPHO Gender=Fem|Number=Plur|NumType=Card|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Plur|NumType=Ord
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=1
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=1|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=2
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=2|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=2|Reflex=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3|PronType=Dem
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3|PronType=Int
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3|PronType=Prs
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Person=3|Reflex=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Plur|PronType=Dem
WRITE b.0 MORPHO Gender=Fem|Number=Plur|PronType=Int
WRITE b.0 MORPHO Gender=Fem|Number=Plur|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part
WRITE b.0 MORPHO Gender=Fem|Number=Plur|Tense=Past|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Gender=Fem|Number=Sing
WRITE b.0 MORPHO Gender=Fem|Number=Sing|NumType=Card
WRITE b.0 MORPHO Gender=Fem|Number=Sing|NumType=Card|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Sing|NumType=Card|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Sing|NumType=Ord
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=1
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=1|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=1|PronType=Prs
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=1|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=1|Reflex=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=2
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=2|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=2|PronType=Prs
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|Poss=Yes|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|PronType=Dem
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|PronType=Int
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|PronType=Prs
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Person=3|Reflex=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Poss=Yes
WRITE b.0 MORPHO Gender=Fem|Number=Sing|PronType=Dem
WRITE b.0 MORPHO Gender=Fem|Number=Sing|PronType=Int
WRITE b.0 MORPHO Gender=Fem|Number=Sing|PronType=Rel
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part
WRITE b.0 MORPHO Gender=Fem|Number=Sing|Tense=Past|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Gender=Masc
WRITE b.0 MORPHO Gender=Masc|Number=Plur
WRITE b.0 MORPHO Gender=Masc|Number=Plur|NumType=Card
WRITE b.0 MORPHO Gender=Masc|Number=Plur|NumType=Card|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Plur|NumType=Card|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Plur|NumType=Ord
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=1
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=1|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=1|PronType=Prs
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=1|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=1|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=2
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=2|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=2|PronType=Prs
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=2|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|Poss=Yes|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|PronType=Dem
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|PronType=Int
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|PronType=Prs
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|PronType=Rel|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Person=3|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Plur|PronType=Dem
WRITE b.0 MORPHO Gender=Masc|Number=Plur|PronType=Int
WRITE b.0 MORPHO Gender=Masc|Number=Plur|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part
WRITE b.0 MORPHO Gender=Masc|Number=Plur|Tense=Past|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Gender=Masc|Number=Sing
WRITE b.0 MORPHO Gender=Masc|Number=Sing|NumType=Card
WRITE b.0 MORPHO Gender=Masc|Number=Sing|NumType=Card|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Sing|NumType=Card|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Sing|NumType=Ord
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=1
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=1|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=1|PronType=Int
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=1|PronType=Prs
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=1|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=1|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=2
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=2|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=2|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|Poss=Yes|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|PronType=Dem
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|PronType=Int
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|PronType=Prs
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|PronType=Rel|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Person=3|Reflex=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Poss=Yes
WRITE b.0 MORPHO Gender=Masc|Number=Sing|PronType=Dem
WRITE b.0 MORPHO Gender=Masc|Number=Sing|PronType=Int
WRITE b.0 MORPHO Gender=Masc|Number=Sing|PronType=Prs
WRITE b.0 MORPHO Gender=Masc|Number=Sing|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Tense=Past|VerbForm=Fin|Voice=Pass
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part
WRITE b.0 MORPHO Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Gender=Masc|NumType=Card
WRITE b.0 MORPHO Gender=Masc|PronType=Rel
WRITE b.0 MORPHO Gender=Masc|Tense=Past|VerbForm=Part
WRITE b.0 MORPHO Gender=Masc|Tense=Past|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Mood=Cnd|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Cnd|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Cnd|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Cnd|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Cnd|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Cnd|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Pass
WRITE b.0 MORPHO Mood=Imp|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Imp|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Imp|Number=Plur|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Imp|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=1|Tense=Fut|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=1|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=2|Tense=Fut|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=2|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=3|Tense=Fut|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=3|Tense=Past|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin|Voice=Pass
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=1|Tense=Fut|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=1|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=1|Tense=Past|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=2|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Fut|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Fut|VerbForm=Fin|Voice=Pass
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin|Voice=Pass
WRITE b.0 MORPHO Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Mood=Ind|VerbForm=Fin
WRITE b.0 MORPHO Mood=Ind|VerbForm=Fin|Voice=Pass
WRITE b.0 MORPHO Mood=Sub|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Sub|Number=Plur|Person=2|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Sub|Number=Plur|Person=3|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Sub|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Sub|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Mood=Sub|Number=Sing|Person=3|Tense=Imp|VerbForm=Fin
WRITE b.0 MORPHO Mood=Sub|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin
WRITE b.0 MORPHO Number=Plur
WRITE b.0 MORPHO Number=Plur|Person=1|PronType=Prs
WRITE b.0 MORPHO Number=Plur|Person=2
WRITE b.0 MORPHO Number=Plur|Person=2|PronType=Prs
WRITE b.0 MORPHO Number=Plur|Person=3|PronType=Prs
WRITE b.0 MORPHO Number=Plur|Poss=Yes
WRITE b.0 MORPHO Number=Plur|PronType=Dem
WRITE b.0 MORPHO Number=Plur|PronType=Rel
WRITE b.0 MORPHO Number=Sing
WRITE b.0 MORPHO Number=Sing|NumType=Ord
WRITE b.0 MORPHO Number=Sing|Person=1|PronType=Prs
WRITE b.0 MORPHO Number=Sing|Person=3
WRITE b.0 MORPHO Number=Sing|Person=3|PronType=Prs
WRITE b.0 MORPHO Number=Sing|Person=3|Tense=Pres|VerbForm=Inf
WRITE b.0 MORPHO Number=Sing|Poss=Yes
WRITE b.0 MORPHO Number=Sing|PronType=Dem
WRITE b.0 MORPHO Number=Sing|PronType=Rel
WRITE b.0 MORPHO NumType=Card
WRITE b.0 MORPHO NumType=Card|PronType=Rel
WRITE b.0 MORPHO NumType=Ord
WRITE b.0 MORPHO Person=3
WRITE b.0 MORPHO Person=3|Reflex=Yes
WRITE b.0 MORPHO Polarity=Neg
WRITE b.0 MORPHO PronType=Int
WRITE b.0 MORPHO PronType=Rel
WRITE b.0 MORPHO Tense=Past|VerbForm=Part
WRITE b.0 MORPHO Tense=Past|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO Tense=Pres|VerbForm=Part
WRITE b.0 MORPHO Tense=Pres|VerbForm=Part|Voice=Pass
WRITE b.0 MORPHO VerbForm=Fin
WRITE b.0 MORPHO VerbForm=Inf
WRITE b.0 MORPHO VerbForm=Inf|Voice=Pass
WRITE b.0 MORPHO VerbForm=Part
