#! /usr/bin/python3

import sys
import random

def printUsageAndExit() :
  print("Usage : %s file.conllu"%sys.argv[0], file=sys.stderr)
  exit(1)

def choseDelimiter() :
  return random.sample(["\n", " ", " ", " ", " ", " "], k=1)[0]

if __name__ == "__main__" :

  sys.stdout = open(1, 'w', encoding='utf-8', closefd=False)

  if len(sys.argv) != 2 :
    printUsageAndExit()

  for line in open(sys.argv[1], encoding='utf-8') :
    if len(line.strip()) < 2 :
      continue

    if line.startswith("# text") :
      print(line.strip()[9:], end=choseDelimiter())

