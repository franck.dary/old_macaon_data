#!/usr/bin/perl

#################################################
#categories enlex
#################################################
#
#      1 
#      3 a
# 165514 A
#    126 adjPref
#     10 adjSuff
#      1 adv
#     89 advPref
#     93 C
#    103 D
#      3 det
#      7 epsilon
#      1 etr
#    434 I
#      3 meta
#     16 n
# 273843 N
#      1 nc
#     15 np
#    117 P
#      9 parentf
#      9 parento
#     13 poncts
#     16 ponctw
#      3 pro
#  45690 R
#    162 S
#      1 sbound
#  89938 V

#################################################
#categories PTB
#################################################
# CC
# CD
# DT
# EX
# FW
# IN
# JJ
# JJR
# JJS
# LS
# MD
# NN
# NNS
# NNP
# NNPS
# PDT
# POS
# PRP
# PRP$
# RB
# RBR
# RBS
# RP
# SYM
# TO
# UH
# VB
# VBD
# VBG
# VBN
# VBP
# VBZ
# WDT
# WP
# WP$
# WRB
# #
# $
# ''
# ``
# (
# )
# ,
# .
# :

$h_pos{"a"} = "";
$h_pos{"A"} = "JJ";
$h_pos{"adjPref"} = "";
$h_pos{"adjSuff"} = "";
$h_pos{"adv"} = "";
$h_pos{"advPref"} = "";
$h_pos{"C"} = "";
$h_pos{"D"} = "DT";
$h_pos{"det"} = "";
$h_pos{"epsilon"} = "";
$h_pos{"etr"} = "";
$h_pos{"I"} = "";
$h_pos{"meta"} = "";
$h_pos{"n"} = "";
$h_pos{"N"} = "";
$h_pos{"nc"} = "";
$h_pos{"np"} = "";
$h_pos{"P"} = "IN";
$h_pos{"parentf"} = "";
$h_pos{"parento"} = "";
$h_pos{"poncts"} = "";
$h_pos{"ponctw"} = "";
$h_pos{"pro"} = "";
$h_pos{"R"} = "";
$h_pos{"S"} = "";
$h_pos{"sbound"} = "";
$h_pos{"V"} = "";


#$h_pos{"meta"} = "XX";


while(<>){
    /([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/;
    my $forme = $1;
    my $cat = $2;
    my $cat_ptb = $h_pos{$cat};
    my $lemme = $3;
    my $morpho = $4;

    print "morpho = $morpho\n";

    if($cat eq "A"){
	if($morpho =~ /comp/){
	    $cat_ptb = "JJR";
	} 
	if($morpho =~ /sup/){
	    $cat_ptb = "JJS";
	} 
    }


    if($cat eq "v"){
	if($morpho =~ /^K/){
	    $cat_ptb = "vppart";
	} 
	elsif($morpho =~ /^W/){
	    $cat_ptb = "vinf";
	} 
	elsif($morpho =~ /^G/){
	    $cat_ptb = "vprespart";
	} 
    }


    if(($cat_ptb) && ($cat_ptb ne "XX")){
	print"$forme\t$cat_ptb\t$lemme\t$morpho";
    }
}
