Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
signature signature.cla
parser_gov parser_gov.cla
parser_label parser_label.cla
%STATES
sgn1 signature
parser_gov parser_gov
parser_label parser_label
%TRANSITIONS
sgn1 parser_gov 0 *
parser_gov parser_label 0 *
parser_label sgn1 +1 *
