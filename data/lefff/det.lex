assez d'	100	det	[define=-,det=+,@e]	assez de_____1	Default	e	%default
assez de	100	det	[define=-,det=+]	assez de_____1	Default		%default
assez de la	100	det	[define=+,det=+,@fs]	assez du_____1	Default	fs	%default
assez des	100	det	[define=+,det=+,@p]	assez du_____1	Default	p	%default
assez du	100	det	[define=+,det=+,@ms]	assez du_____1	Default	ms	%default
aucun	100	det	[define=-,det=+,@ms]	aucun_____1	Default	ms	%default
aucune	100	det	[define=-,det=+,@fs]	aucun_____1	Default	fs	%default
autant d'	100	det	[define=-,det=+,@e]	autant de_____1	Default	e	%default
autant de	100	det	[define=-,det=+]	autant de_____1	Default		%default
autant de la	100	det	[define=+,det=+,@fs]	autant du_____1	Default	fs	%default
autant des	100	det	[define=+,det=+,@p]	autant du_____1	Default	p	%default
autant du	100	det	[define=+,det=+,@ms]	autant du_____1	Default	ms	%default
bcp de	100	det	[define=-,det=+]	beaucoup de_____1	Default		%default
beaucoup d'	100	det	[define=-,det=+,@e]	beaucoup de_____1	Default	e	%default
beaucoup de	100	det	[define=-,det=+]	beaucoup de_____1	Default		%default
beaucoup de la	100	det	[define=+,det=+,@fs]	beaucoup du_____1	Default	fs	%default
beaucoup des	100	det	[define=+,det=+,@p]	beaucoup du_____1	Default	p	%default
beaucoup du	100	det	[define=+,det=+,@ms]	beaucoup du_____1	Default	ms	%default
bien de la	100	det	[define=-,det=+,@fs]	bien du_____1	Default	fs	%default
bien des	100	det	[define=-,det=+,@p]	bien du_____1	Default	p	%default
bien du	100	det	[define=-,det=+,@ms]	bien du_____1	Default	ms	%default
c't'	100	det	[demonstrative=+,det=+]	ce_____1	Default		%default
ce	100	det	[demonstrative=+,det=+,@ms]	ce_____1	Default	ms	%default
ce putain d'	100	det	[demonstrative=+,det=+,@ms]	ce putain de_____1	Default	ms	%default
ce putain de	100	det	[demonstrative=+,det=+,@ms]	ce putain de_____1	Default	ms	%default
certain	100	det	[define=-,det=+,@ms]	certain_____1	Default	ms	%default
certaine	100	det	[define=-,det=+,@fs]	certain_____1	Default	fs	%default
certaines	100	det	[define=-,det=+,@fp]	certain_____1	Default	fp	%default
certains	100	det	[define=-,det=+,@mp]	certain_____1	Default	mp	%default
ces	100	det	[demonstrative=+,det=+,@p]	ce_____1	Default	p	%default
ces putain d'	100	det	[demonstrative=+,det=+,@p]	ce putain de_____1	Default	p	%default
ces putain de	100	det	[demonstrative=+,det=+,@p]	ce putain de_____1	Default	p	%default
cet	100	det	[demonstrative=+,det=+,@ms]	ce_____1	Default	ms	%default
cette	100	det	[demonstrative=+,det=+,@fs]	ce_____1	Default	fs	%default
cette putain d'	100	det	[demonstrative=+,det=+,@fs]	ce putain de_____1	Default	fs	%default
cette putain de	100	det	[demonstrative=+,det=+,@fs]	ce putain de_____1	Default	fs	%default
chaque	100	det	[define=-,det=+,@s]	chaque_____1	Default	s	%default
combien d'	100	det	[define=-,det=+,@e]	combien de_____1	Default	e	%default
combien de	100	det	[define=-,det=+]	combien de_____1	Default		%default
combien de la	100	det	[define=+,det=+,@fs]	combien du_____1	Default	fs	%default
combien des	100	det	[define=+,det=+,@p]	combien du_____1	Default	p	%default
combien du	100	det	[define=+,det=+,@ms]	combien du_____1	Default	ms	%default
ct'	100	det	[demonstrative=+,det=+]	ce_____1	Default		%default
d'	100	det	[define=-,det=+,@p]	un_____1	Default	p	%default
de	100	det	[define=-,det=+,@p]	un_____1	Default	p	%default
de l'	100	det	[define=-,det=+,@s]	du_____1	Default	s	%default
de la	100	det	[define=-,det=+,@fs]	du_____1	Default	fs	%default
de telles	100	det	[define=-,det=+,@fp]	un tel_____1	Default	fp	%default
de tels	100	det	[define=-,det=+,@mp]	un tel_____1	Default	mp	%default
des	100	det	[define=-,det=+,@p]	un_____1	Default	p	%default
des drôle d'	100	det	[define=-,det=+,@p]	un drôle de_____1	Default	p	%default
des drôle de	100	det	[define=-,det=+,@p]	un drôle de_____1	Default	p	%default
des telles	100	det	[define=-,det=+,@fp]	un tel_____1	Default	fp	%default
des tels	100	det	[define=-,det=+,@mp]	un tel_____1	Default	mp	%default
dieu sait quel	100	det	[define=-,det=+,@ms]	dieu sait quel_____1	Default	ms	%default
dieu sait quelle	100	det	[define=-,det=+,@fs]	dieu sait quel_____1	Default	fs	%default
dieu sait quelles	100	det	[define=-,det=+,@fp]	dieu sait quel_____1	Default	fp	%default
dieu sait quels	100	det	[define=-,det=+,@mp]	dieu sait quel_____1	Default	mp	%default
du	100	det	[define=-,det=+,@ms]	du_____1	Default	ms	%default
force	100	det	[define=-,det=+]	force_____1	Default		%default
l'	100	det	[define=+,det=+,@s]	le_____1	Default	s	%default
la	100	det	[define=+,det=+,@fs]	le_____1	Default	fs	%default
la plupart de la	100	det	[define=+,det=+,@fs]	la plupart du_____1	Default	fs	%default
la plupart des	100	det	[define=+,det=+,@p]	la plupart du_____1	Default	p	%default
la plupart du	100	det	[define=+,det=+,@ms]	la plupart du_____1	Default	ms	%default
ladite	100	det	[define=+,det=+,@fs]	ledit_____1	Default	fs	%default
le	100	det	[define=+,det=+,@ms]	le_____1	Default	ms	%default
le moins d'	100	det	[define=+,det=+,@e]	le moins de_____1	Default	e	%default
le moins de	100	det	[define=+,det=+]	le moins de_____1	Default		%default
le plus d'	100	det	[define=+,det=+,@e]	le plus de_____1	Default	e	%default
le plus de	100	det	[define=+,det=+]	le plus de_____1	Default		%default
le__det	100	det	[define=+,det=+,@ms]	le_____1	Default	ms	%default
ledit	100	det	[define=+,det=+,@ms]	ledit_____1	Default	ms	%default
les	100	det	[define=+,det=+,@p]	le_____1	Default	p	%default
les__det	100	det	[define=+,det=+,@p]	le_____1	Default	p	%default
lesdites	100	det	[define=+,det=+,@fp]	ledit_____1	Default	fp	%default
lesdits	100	det	[define=+,det=+,@mp]	ledit_____1	Default	mp	%default
leur	100	det	[@poss,det=+,@s_P3p]	son_____1	Default	s_P3p	%default
leurs	100	det	[@poss,det=+,@p_P3p]	son_____1	Default	p_P3p	%default
ma	100	det	[@poss,det=+,@fs_P1s]	son_____1	Default	fs_P1s	%default
maint	100	det	[define=-,det=+,@ms]	maint_____1	Default	ms	%default
mainte	100	det	[define=-,det=+,@fs]	maint_____1	Default	fs	%default
maintes	100	det	[define=-,det=+,@fp]	maint_____1	Default	fp	%default
maints	100	det	[define=-,det=+,@mp]	maint_____1	Default	mp	%default
mes	100	det	[@poss,det=+,@p_P1s]	son_____1	Default	p_P1s	%default
moins d'	100	det	[define=-,det=+,@e]	moins de_____1	Default	e	%default
moins de	100	det	[define=-,det=+]	moins de_____1	Default		%default
moins de la	100	det	[define=+,det=+,@fs]	moins du_____1	Default	fs	%default
moins des	100	det	[define=+,det=+,@p]	moins du_____1	Default	p	%default
moins du	100	det	[define=+,det=+,@ms]	moins du_____1	Default	ms	%default
mon	100	det	[@poss,det=+,@s_P1s]	son_____1	Default	s_P1s	%default
n'importe quel	100	det	[define=-,det=+,@ms]	n'importe quel_____1	Default	ms	%default
n'importe quelle	100	det	[define=-,det=+,@fs]	n'importe quel_____1	Default	fs	%default
n'importe quelles	100	det	[define=-,det=+,@fp]	n'importe quel_____1	Default	fp	%default
n'importe quels	100	det	[define=-,det=+,@mp]	n'importe quel_____1	Default	mp	%default
nos	100	det	[@poss,det=+,@p_P1p]	son_____1	Default	p_P1p	%default
notre	100	det	[@poss,det=+,@s_P1p]	son_____1	Default	s_P1p	%default
nul	100	det	[define=-,det=+,@ms]	nul_____1	Default	ms	%default
nulle	100	det	[define=-,det=+,@fs]	nul_____1	Default	fs	%default
nulles	100	det	[define=-,det=+,@fp]	nul_____1	Default	fp	%default
nuls	100	det	[define=-,det=+,@mp]	nul_____1	Default	mp	%default
pareil	100	det	[define=+,det=+,@ms]	pareil_____1	Default	ms	%default
pareille	100	det	[define=+,det=+,@fs]	pareil_____1	Default	fs	%default
pareilles	100	det	[define=+,det=+,@fp]	pareil_____1	Default	fp	%default
pareils	100	det	[define=+,det=+,@mp]	pareil_____1	Default	mp	%default
pas d'	100	det	[define=-,det=+,@e]	pas de_____1	Default	e	%default
pas de	100	det	[define=-,det=+]	pas de_____1	Default		%default
pas mal d'	100	det	[define=-,det=+,@e]	pas mal de_____1	Default	e	%default
pas mal de	100	det	[define=-,det=+]	pas mal de_____1	Default		%default
pas mal de la	100	det	[define=+,det=+,@fs]	pas mal du_____1	Default	fs	%default
pas mal des	100	det	[define=+,det=+,@p]	pas mal du_____1	Default	p	%default
pas mal du	100	det	[define=+,det=+,@ms]	pas mal du_____1	Default	ms	%default
plein d'	100	det	[define=-,det=+,@e]	plein de_____1	Default	e	%default
plein de	100	det	[define=-,det=+]	plein de_____1	Default		%default
plus d'	100	det	[define=-,det=+,@e]	plus de_____1	Default	e	%default
plus de	100	det	[define=-,det=+]	plus de_____1	Default		%default
plus de la	100	det	[define=+,det=+,@fs]	plus du_____1	Default	fs	%default
plus des	100	det	[define=+,det=+,@p]	plus du_____1	Default	p	%default
plus du	100	det	[define=+,det=+,@ms]	plus du_____1	Default	ms	%default
plusieurs	100	det	[define=-,det=+,@p]	plusieurs_____1	Default	p	%default
qq	100	det	[define=-,det=+]	quelque_____1	Default		%default
qqes	100	det	[define=-,det=+,@p]	quelque_____1	Default	p	%default
qqs	100	det	[define=-,det=+,@p]	quelque_____1	Default	p	%default
quel	100	det	[det=+,qu=+,@ms]	quel_____1	Default	ms	%default
quelle	100	det	[det=+,qu=+,@fs]	quel_____1	Default	fs	%default
quelles	100	det	[det=+,qu=+,@fp]	quel_____1	Default	fp	%default
quelqu'	100	det	[define=-,det=+,@s]	quelque_____1	Default	s	%default
quelque	100	det	[define=-,det=+,@s]	quelque_____1	Default	s	%default
quelques	100	det	[define=-,det=+,@p]	quelque_____1	Default	p	%default
quels	100	det	[det=+,qu=+,@mp]	quel_____1	Default	mp	%default
sa	100	det	[@poss,det=+,@fs_P3s]	son_____1	Default	fs_P3s	%default
ses	100	det	[@poss,det=+,@p_P3s]	son_____1	Default	p_P3s	%default
son	100	det	[@poss,det=+,@s_P3s]	son_____1	Default	s_P3s	%default
ta	100	det	[@poss,det=+,@fs_P2s]	son_____1	Default	fs_P2s	%default
tant d'	100	det	[define=-,det=+,@e]	tant de_____1	Default	e	%default
tant de	100	det	[define=-,det=+]	tant de_____1	Default		%default
tant de la	100	det	[define=+,det=+,@fs]	tant du_____1	Default	fs	%default
tant des	100	det	[define=+,det=+,@p]	tant du_____1	Default	p	%default
tant du	100	det	[define=+,det=+,@ms]	tant du_____1	Default	ms	%default
tel	100	det	[define=+,det=+,@ms]	tel_____1	Default	ms	%default
tel et tel	100	det	[define=-,det=+,@ms]	tel et tel_____1	Default	ms	%default
tel ou tel	100	det	[define=-,det=+,@ms]	tel ou tel_____1	Default	ms	%default
telle	100	det	[define=+,det=+,@fs]	tel_____1	Default	fs	%default
telle et telle	100	det	[define=-,det=+,@fs]	tel et tel_____1	Default	fs	%default
telle ou telle	100	det	[define=-,det=+,@fs]	tel ou tel_____1	Default	fs	%default
telles	100	det	[define=+,det=+,@fp]	tel_____1	Default	fp	%default
telles et telles	100	det	[define=-,det=+,@fp]	tel et tel_____1	Default	fp	%default
telles ou telles	100	det	[define=-,det=+,@fp]	tel ou tel_____1	Default	fp	%default
tels	100	det	[define=+,det=+,@mp]	tel_____1	Default	mp	%default
tels et tels	100	det	[define=-,det=+,@mp]	tel et tel_____1	Default	mp	%default
tels ou tels	100	det	[define=-,det=+,@mp]	tel ou tel_____1	Default	mp	%default
tes	100	det	[@poss,det=+,@p_P2s]	son_____1	Default	p_P2s	%default
ton	100	det	[@poss,det=+,@s_P2s]	son_____1	Default	s_P2s	%default
tous	100	det	[define=-,det=+,@mp]	tout_____1	Default	mp	%default
tout	100	det	[define=-,det=+,@ms]	tout_____1	Default	ms	%default
toute	100	det	[define=-,det=+,@fs]	tout_____1	Default	fs	%default
toutes	100	det	[define=-,det=+,@fp]	tout_____1	Default	fp	%default
un	100	det	[define=-,det=+,@ms]	un_____1	Default	ms	%default
un drôle d'	100	det	[define=-,det=+,@ms]	un drôle de_____1	Default	ms	%default
un drôle de	100	det	[define=-,det=+,@ms]	un drôle de_____1	Default	ms	%default
un peu d'	100	det	[define=-,det=+,@e]	un peu de_____1	Default	e	%default
un peu de	100	det	[define=-,det=+]	un peu de_____1	Default		%default
un peu de la	100	det	[define=+,det=+,@fs]	un peu du_____1	Default	fs	%default
un peu des	100	det	[define=+,det=+,@p]	un peu du_____1	Default	p	%default
un peu du	100	det	[define=+,det=+,@ms]	un peu du_____1	Default	ms	%default
un tel	100	det	[define=-,det=+,@ms]	un tel_____1	Default	ms	%default
une	100	det	[define=-,det=+,@fs]	un_____1	Default	fs	%default
une drôle d'	100	det	[define=-,det=+,@fs]	un drôle de_____1	Default	fs	%default
une drôle de	100	det	[define=-,det=+,@fs]	un drôle de_____1	Default	fs	%default
une telle	100	det	[define=-,det=+,@fs]	un tel_____1	Default	fs	%default
vos	100	det	[@poss,det=+,@p_P2p]	son_____1	Default	p_P2p	%default
vot'	100	det	[@poss,det=+,@s_P2p]	son_____1	Default	s_P2p	%default
votre	100	det	[@poss,det=+,@s_P2p]	son_____1	Default	s_P2p	%default
