#! /usr/bin/python3

import sys

###############################################################################
def isSeparator(c) :
  return c == ' ' or c == '\n' or c == '\t' or c == '\r'

###############################################################################
def isPonct(c) :
  return c == '.' or c == '?' or c == '!' or c == '(' or c == ')' or c == '[' or c == ']' or c == '{' or c == '}' or c == ',' or c == ';' or c == ':' or c == '\"'

###############################################################################
def isEndOfWord(c) :
  return c == "\'"

###############################################################################
def endOfSentence(c) :
  return c == '.' or c == '?' or c == '!' 

###############################################################################
def isAllPonct(token) :
  for c in token :
    if not isPonct(c) :
      return False

  return True

###############################################################################
def tokenize(lines) :
  tokenized = []

  for line in lines :
    currentToken = ""
    for c in line :
      if isSeparator(c) :
        if len(currentToken) == 0 :
          continue
        tokenized += [currentToken]
        currentToken = ""
      elif isEndOfWord(c) :
        if len(currentToken) == 0 :
          continue
        currentToken += c
        tokenized += [currentToken]
        currentToken = ""
      elif isPonct(c) :
        if isAllPonct(currentToken) :
          currentToken += c
        else :
          tokenized += [currentToken]
          currentToken = "" + c
      else :
        if len(currentToken) > 0 and isAllPonct(currentToken) :
          tokenized += [currentToken]
          currentToken = "" + c
        else :
          currentToken += c

    if not len(currentToken) == 0 :
      tokenized += [currentToken]

  return tokenized

###############################################################################
def main() :
  for line in sys.stdin :
    for word in tokenize([line]) :
      print(word)

if __name__ == "__main__" :
  main()
