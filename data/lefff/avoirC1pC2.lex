_NUM N-hum pour le prix d'un	100	cf	[pred="avoir _NUM N-hum pour le prix d'un_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM N-hum pour le prix d'un_____1	Default		%default
_NUM N-hum pour le prix d'un seul	100	cf	[pred="avoir _NUM N-hum pour le prix d'un seul_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM N-hum pour le prix d'un seul_____1	Default		%default
_NUM N-hum pour le prix de _NUM	100	cf	[pred="avoir _NUM N-hum pour le prix de _NUM_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM N-hum pour le prix de _NUM_____1	Default		%default
_NUM Nmesure de circonférence	100	cf	[pred="avoir _NUM Nmesure de circonférence_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de circonférence_____1	Default		%default
_NUM Nmesure de diamètre	100	cf	[pred="avoir _NUM Nmesure de diamètre_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de diamètre_____1	Default		%default
_NUM Nmesure de haut	100	cf	[pred="avoir _NUM Nmesure de haut_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de haut_____1	Default		%default
_NUM Nmesure de hauteur	100	cf	[pred="avoir _NUM Nmesure de hauteur_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de hauteur_____1	Default		%default
_NUM Nmesure de large	100	cf	[pred="avoir _NUM Nmesure de large_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de large_____1	Default		%default
_NUM Nmesure de largeur	100	cf	[pred="avoir _NUM Nmesure de largeur_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de largeur_____1	Default		%default
_NUM Nmesure de long	100	cf	[pred="avoir _NUM Nmesure de long_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de long_____1	Default		%default
_NUM Nmesure de longueur	100	cf	[pred="avoir _NUM Nmesure de longueur_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de longueur_____1	Default		%default
_NUM Nmesure de surface	100	cf	[pred="avoir _NUM Nmesure de surface_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de surface_____1	Default		%default
_NUM Nmesure de tour	100	cf	[pred="avoir _NUM Nmesure de tour_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de tour_____1	Default		%default
_NUM Nmesure de tour de hanches	100	cf	[pred="avoir _NUM Nmesure de tour de hanches_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de tour de hanches_____1	Default		%default
_NUM Nmesure de tour de poitrine	100	cf	[pred="avoir _NUM Nmesure de tour de poitrine_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de tour de poitrine_____1	Default		%default
_NUM Nmesure de tour de taille	100	cf	[pred="avoir _NUM Nmesure de tour de taille_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de tour de taille_____1	Default		%default
_NUM Nmesure de trop	100	cf	[pred="avoir _NUM Nmesure de trop_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de trop_____1	Default		%default
_NUM Nmesure de volume	100	cf	[pred="avoir _NUM Nmesure de volume_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure de volume_____1	Default		%default
_NUM Nmesure en trop	100	cf	[pred="avoir _NUM Nmesure en trop_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM Nmesure en trop_____1	Default		%default
_NUM ans et toutes leurs dents	100	cf	[pred="avoir _NUM ans et toutes ses dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM ans et toutes ses dents_____1	Default		%default
_NUM ans et toutes mes dents	100	cf	[pred="avoir _NUM ans et toutes ses dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM ans et toutes ses dents_____1	Default		%default
_NUM ans et toutes nos dents	100	cf	[pred="avoir _NUM ans et toutes ses dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM ans et toutes ses dents_____1	Default		%default
_NUM ans et toutes ses dents	100	cf	[pred="avoir _NUM ans et toutes ses dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM ans et toutes ses dents_____1	Default		%default
_NUM ans et toutes tes dents	100	cf	[pred="avoir _NUM ans et toutes ses dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM ans et toutes ses dents_____1	Default		%default
_NUM ans et toutes vos dents	100	cf	[pred="avoir _NUM ans et toutes ses dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM ans et toutes ses dents_____1	Default		%default
_NUM enfant à charge	100	cf	[pred="avoir _NUM enfant à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM enfant à charge_____1	Default		%default
_NUM fille et _NUM garçon	100	cf	[pred="avoir _NUM fille et _NUM garçon_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM fille et _NUM garçon_____1	Default		%default
_NUM garçon et _NUM fille	100	cf	[pred="avoir _NUM garçon et _NUM fille_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM garçon et _NUM fille_____1	Default		%default
_NUM gramme d'alcool dans le sang	100	cf	[pred="avoir _NUM gramme d'alcool dans le sang_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM gramme d'alcool dans le sang_____1	Default		%default
_NUM heure à leur montre	100	cf	[pred="avoir _NUM heure à sa montre_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM heure à sa montre_____1	Default		%default
_NUM heure à ma montre	100	cf	[pred="avoir _NUM heure à sa montre_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM heure à sa montre_____1	Default		%default
_NUM heure à notre montre	100	cf	[pred="avoir _NUM heure à sa montre_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM heure à sa montre_____1	Default		%default
_NUM heure à sa montre	100	cf	[pred="avoir _NUM heure à sa montre_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM heure à sa montre_____1	Default		%default
_NUM heure à ta montre	100	cf	[pred="avoir _NUM heure à sa montre_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM heure à sa montre_____1	Default		%default
_NUM heure à votre montre	100	cf	[pred="avoir _NUM heure à sa montre_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM heure à sa montre_____1	Default		%default
_NUM kilo de trop	100	cf	[pred="avoir _NUM kilo de trop_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM kilo de trop_____1	Default		%default
_NUM kilo en trop	100	cf	[pred="avoir _NUM kilo en trop_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM kilo en trop_____1	Default		%default
_NUM kilomètres dans les jambes	100	cf	[pred="avoir _NUM kilomètres dans les jambes_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM kilomètres dans les jambes_____1	Default		%default
_NUM kilomètres dans les pattes	100	cf	[pred="avoir _NUM kilomètres dans les pattes_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM kilomètres dans les pattes_____1	Default		%default
_NUM lits pour le prix de deux	100	cf	[pred="avoir _NUM lits pour le prix de deux_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM lits pour le prix de deux_____1	Default		%default
_NUM mètre carré de surface	100	cf	[pred="avoir _NUM mètre carré de surface_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre carré de surface_____1	Default		%default
_NUM mètre cube de volume	100	cf	[pred="avoir _NUM mètre cube de volume_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre cube de volume_____1	Default		%default
_NUM mètre de circonférence	100	cf	[pred="avoir _NUM mètre de circonférence_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de circonférence_____1	Default		%default
_NUM mètre de diamètre	100	cf	[pred="avoir _NUM mètre de diamètre_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de diamètre_____1	Default		%default
_NUM mètre de haut	100	cf	[pred="avoir _NUM mètre de haut_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de haut_____1	Default		%default
_NUM mètre de hauteur	100	cf	[pred="avoir _NUM mètre de hauteur_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de hauteur_____1	Default		%default
_NUM mètre de large	100	cf	[pred="avoir _NUM mètre de large_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de large_____1	Default		%default
_NUM mètre de largeur	100	cf	[pred="avoir _NUM mètre de largeur_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de largeur_____1	Default		%default
_NUM mètre de long	100	cf	[pred="avoir _NUM mètre de long_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de long_____1	Default		%default
_NUM mètre de longueur	100	cf	[pred="avoir _NUM mètre de longueur_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de longueur_____1	Default		%default
_NUM mètre de tour	100	cf	[pred="avoir _NUM mètre de tour_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir _NUM mètre de tour_____1	Default		%default
_NUM personne à charge	100	cf	[pred="avoir _NUM personne à charge_____1<Suj:cln|sn>",lightverb=avoir]	avoir _NUM personne à charge_____1	Default		%default
affaire aux tribunaux	100	cf	[pred="avoir affaire aux tribunaux_____1<Suj:cln|sn>",lightverb=avoir]	avoir affaire aux tribunaux_____1	Default		%default
affaire à forte partie	100	cf	[pred="avoir affaire à forte partie_____1<Suj:cln|sn>",lightverb=avoir]	avoir affaire à forte partie_____1	Default		%default
affaire à la justice	100	cf	[pred="avoir affaire à la justice_____1<Suj:cln|sn>",lightverb=avoir]	avoir affaire à la justice_____1	Default		%default
affaire à la police	100	cf	[pred="avoir affaire à la police_____1<Suj:cln|sn>",lightverb=avoir]	avoir affaire à la police_____1	Default		%default
autant à leur service	100	cf	[pred="en avoir autant à son service_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir autant à son service_____1	Default		%default
autant à mon service	100	cf	[pred="en avoir autant à son service_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir autant à son service_____1	Default		%default
autant à notre service	100	cf	[pred="en avoir autant à son service_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir autant à son service_____1	Default		%default
autant à son service	100	cf	[pred="en avoir autant à son service_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir autant à son service_____1	Default		%default
autant à ton service	100	cf	[pred="en avoir autant à son service_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir autant à son service_____1	Default		%default
autant à votre service	100	cf	[pred="en avoir autant à son service_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir autant à son service_____1	Default		%default
autre chose en tête	100	cf	[pred="avoir autre chose en tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir autre chose en tête_____1	Default		%default
beaucoup à faire	100	cf	[pred="avoir beaucoup à faire_____1<Suj:cln|sn>",lightverb=avoir]	avoir beaucoup à faire_____1	Default		%default
bobo à l'oreille	100	cf	[pred="avoir bobo à l'oreille_____1<Suj:cln|sn>",lightverb=avoir]	avoir bobo à l'oreille_____1	Default		%default
bon pied bon oeil	100	cf	[pred="avoir bon pied bon oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir bon pied bon oeil_____1	Default		%default
chaud au coeur	100	cf	[pred="avoir chaud au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir chaud au coeur_____1	Default		%default
chaud aux fesses	100	cf	[pred="avoir chaud aux fesses_____1<Suj:cln|sn>",lightverb=avoir]	avoir chaud aux fesses_____1	Default		%default
chaud aux oreilles	100	cf	[pred="avoir chaud aux oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir chaud aux oreilles_____1	Default		%default
cinq sur cinq	100	cf	[pred="avoir cinq sur cinq_____1<Suj:cln|sn>",lightverb=avoir]	avoir cinq sur cinq_____1	Default		%default
confiance dans l'avenir	100	cf	[pred="avoir confiance dans l'avenir_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans l'avenir_____1	Default		%default
confiance dans le progrès	100	cf	[pred="avoir confiance dans le progrès_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans le progrès_____1	Default		%default
confiance dans leur bonne étoile	100	cf	[pred="avoir confiance dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans sa bonne étoile_____1	Default		%default
confiance dans leur étoile	100	cf	[pred="avoir confiance dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans son étoile_____1	Default		%default
confiance dans ma bonne étoile	100	cf	[pred="avoir confiance dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans sa bonne étoile_____1	Default		%default
confiance dans mon étoile	100	cf	[pred="avoir confiance dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans son étoile_____1	Default		%default
confiance dans notre bonne étoile	100	cf	[pred="avoir confiance dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans sa bonne étoile_____1	Default		%default
confiance dans notre étoile	100	cf	[pred="avoir confiance dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans son étoile_____1	Default		%default
confiance dans sa bonne étoile	100	cf	[pred="avoir confiance dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans sa bonne étoile_____1	Default		%default
confiance dans son étoile	100	cf	[pred="avoir confiance dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans son étoile_____1	Default		%default
confiance dans ta bonne étoile	100	cf	[pred="avoir confiance dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans sa bonne étoile_____1	Default		%default
confiance dans ton étoile	100	cf	[pred="avoir confiance dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans son étoile_____1	Default		%default
confiance dans votre bonne étoile	100	cf	[pred="avoir confiance dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans sa bonne étoile_____1	Default		%default
confiance dans votre étoile	100	cf	[pred="avoir confiance dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance dans son étoile_____1	Default		%default
confiance en elle	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en elles	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en eux	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en l'avenir	100	cf	[pred="avoir confiance en l'avenir_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en l'avenir_____1	Default		%default
confiance en leur bonne étoile	100	cf	[pred="avoir confiance en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en sa bonne étoile_____1	Default		%default
confiance en leur étoile	100	cf	[pred="avoir confiance en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en son étoile_____1	Default		%default
confiance en lui	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en ma bonne étoile	100	cf	[pred="avoir confiance en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en sa bonne étoile_____1	Default		%default
confiance en moi	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en mon étoile	100	cf	[pred="avoir confiance en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en son étoile_____1	Default		%default
confiance en notre bonne étoile	100	cf	[pred="avoir confiance en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en sa bonne étoile_____1	Default		%default
confiance en notre étoile	100	cf	[pred="avoir confiance en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en son étoile_____1	Default		%default
confiance en nous	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en sa bonne étoile	100	cf	[pred="avoir confiance en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en sa bonne étoile_____1	Default		%default
confiance en soi	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en son étoile	100	cf	[pred="avoir confiance en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en son étoile_____1	Default		%default
confiance en ta bonne étoile	100	cf	[pred="avoir confiance en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en sa bonne étoile_____1	Default		%default
confiance en toi	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
confiance en ton étoile	100	cf	[pred="avoir confiance en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en son étoile_____1	Default		%default
confiance en votre bonne étoile	100	cf	[pred="avoir confiance en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en sa bonne étoile_____1	Default		%default
confiance en votre étoile	100	cf	[pred="avoir confiance en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en son étoile_____1	Default		%default
confiance en vous	100	cf	[pred="avoir confiance en lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir confiance en lui_____1	Default		%default
d'autres chats à fouetter	100	cf	[pred="avoir d'autres chats à fouetter_____1<Suj:cln|sn>",lightverb=avoir]	avoir d'autres chats à fouetter_____1	Default		%default
de beaux jours devant elle	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant elles	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant eux	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant lui	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant moi	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant nous	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant soi	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant toi	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de beaux jours devant vous	100	cf	[pred="avoir de beaux jours devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de beaux jours devant lui_____1	Default		%default
de belles années devant elle	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant elles	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant eux	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant lui	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant moi	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant nous	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant soi	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant toi	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de belles années devant vous	100	cf	[pred="avoir de belles années devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de belles années devant lui_____1	Default		%default
de l'argent de côté	100	cf	[pred="avoir de l'argent de côté_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent de côté_____1	Default		%default
de l'argent devant elle	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant elles	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant eux	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant lui	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant moi	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant nous	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant soi	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant toi	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent devant vous	100	cf	[pred="avoir de l'argent devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent devant lui_____1	Default		%default
de l'argent en caisse	100	cf	[pred="avoir de l'argent en caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent en caisse_____1	Default		%default
de l'argent plein les poches	100	cf	[pred="avoir de l'argent plein les poches_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent plein les poches_____1	Default		%default
de l'argent sur elle	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur elles	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur eux	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur lui	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur moi	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur nous	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur soi	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur toi	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'argent sur vous	100	cf	[pred="avoir de l'argent sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'argent sur lui_____1	Default		%default
de l'avance à l'allumage	100	cf	[pred="avoir de l'avance à l'allumage_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'avance à l'allumage_____1	Default		%default
de l'eau jusqu'à la ceinture	100	cf	[pred="avoir de l'eau jusqu'à la ceinture_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'eau jusqu'à la ceinture_____1	Default		%default
de l'eau jusqu'à la taille	100	cf	[pred="avoir de l'eau jusqu'à la taille_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'eau jusqu'à la taille_____1	Default		%default
de l'eau jusqu'à les oreilles	100	cf	[pred="avoir de l'eau jusqu'à les oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'eau jusqu'à les oreilles_____1	Default		%default
de l'eau jusqu'à micuisse	100	cf	[pred="avoir de l'eau jusqu'à micuisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'eau jusqu'à micuisse_____1	Default		%default
de l'esprit jusqu'à le bout des ongles	100	cf	[pred="avoir de l'esprit jusqu'à le bout des ongles_____1<Suj:cln|sn>",lightverb=avoir]	avoir de l'esprit jusqu'à le bout des ongles_____1	Default		%default
de la barbe au menton	100	cf	[pred="avoir de la barbe au menton_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la barbe au menton_____1	Default		%default
de la chance au jeu	100	cf	[pred="avoir de la chance au jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la chance au jeu_____1	Default		%default
de la chance en amour	100	cf	[pred="avoir de la chance en amour_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la chance en amour_____1	Default		%default
de la colère dans le regard	100	cf	[pred="avoir de la colère dans le regard_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la colère dans le regard_____1	Default		%default
de la colère dans les yeux	100	cf	[pred="avoir de la colère dans les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la colère dans les yeux_____1	Default		%default
de la force dans les bras	100	cf	[pred="avoir de la force dans les bras_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la force dans les bras_____1	Default		%default
de la force dans les doigts	100	cf	[pred="avoir de la force dans les doigts_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la force dans les doigts_____1	Default		%default
de la force dans les mains	100	cf	[pred="avoir de la force dans les mains_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la force dans les mains_____1	Default		%default
de la force dans les poignets	100	cf	[pred="avoir de la force dans les poignets_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la force dans les poignets_____1	Default		%default
de la haine dans le regard	100	cf	[pred="avoir de la haine dans le regard_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la haine dans le regard_____1	Default		%default
de la haine dans les yeux	100	cf	[pred="avoir de la haine dans les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la haine dans les yeux_____1	Default		%default
de la merde dans les oreilles	100	cf	[pred="avoir de la merde dans les oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la merde dans les oreilles_____1	Default		%default
de la merde dans les yeux	100	cf	[pred="avoir de la merde dans les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la merde dans les yeux_____1	Default		%default
de la peine à joindre les deux bouts	100	cf	[pred="avoir de la peine à joindre les deux bouts_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la peine à joindre les deux bouts_____1	Default		%default
de la rage dans le regard	100	cf	[pred="avoir de la rage dans le regard_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la rage dans le regard_____1	Default		%default
de la rage dans les yeux	100	cf	[pred="avoir de la rage dans les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la rage dans les yeux_____1	Default		%default
de la suite dans les idées	100	cf	[pred="avoir de la suite dans les idées_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la suite dans les idées_____1	Default		%default
de la veine au jeu	100	cf	[pred="avoir de la veine au jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la veine au jeu_____1	Default		%default
de la énergie à revendre	100	cf	[pred="avoir de la énergie à revendre_____1<Suj:cln|sn>",lightverb=avoir]	avoir de la énergie à revendre_____1	Default		%default
de le fric devant elle	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant elles	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant eux	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant lui	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant moi	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant nous	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant soi	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant toi	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric devant vous	100	cf	[pred="avoir de le fric devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric devant lui_____1	Default		%default
de le fric sur elle	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur elles	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur eux	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur lui	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur moi	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur nous	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur soi	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur toi	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de le fric sur vous	100	cf	[pred="avoir de le fric sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir de le fric sur lui_____1	Default		%default
de secret pour personne	100	cf	[pred="n' avoir de secret pour personne_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir de secret pour personne_____1	Default		%default
des ampoules au pied	100	cf	[pred="avoir des ampoules au pied_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des ampoules au pied_____1	Default		%default
des ampoules au talon	100	cf	[pred="avoir des ampoules au talon_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des ampoules au talon_____1	Default		%default
des ampoules à la main	100	cf	[pred="avoir des ampoules à la main_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des ampoules à la main_____1	Default		%default
des années de N-hum derrière elle	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière elles	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière eux	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière lui	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière moi	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière nous	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière soi	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière toi	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum derrière vous	100	cf	[pred="avoir des années de N-hum derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum derrière lui_____1	Default		%default
des années de N-hum devant elle	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant elles	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant eux	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant lui	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant moi	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant nous	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant soi	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant toi	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de N-hum devant vous	100	cf	[pred="avoir des années de N-hum devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de N-hum devant lui_____1	Default		%default
des années de labeur derrière elle	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière elles	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière eux	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière lui	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière moi	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière nous	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière soi	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière toi	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de labeur derrière vous	100	cf	[pred="avoir des années de labeur derrière lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de labeur derrière lui_____1	Default		%default
des années de travail devant elle	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant elles	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant eux	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant lui	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant moi	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant nous	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant soi	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant toi	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des années de travail devant vous	100	cf	[pred="avoir des années de travail devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir des années de travail devant lui_____1	Default		%default
des atouts dans sa manche	100	cf	[pred="avoir des atouts dans sa manche_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des atouts dans sa manche_____1	Default		%default
des atouts dans son jeu	100	cf	[pred="avoir des atouts dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des atouts dans son jeu_____1	Default		%default
des bourdonnements dans les oreilles	100	cf	[pred="avoir des bourdonnements dans les oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir des bourdonnements dans les oreilles_____1	Default		%default
des bâtons dans les roues	100	cf	[pred="avoir des bâtons dans les roues_____1<Suj:cln|sn>",lightverb=avoir]	avoir des bâtons dans les roues_____1	Default		%default
des cartes dans leur jeu	100	cf	[pred="avoir des cartes dans son jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir des cartes dans son jeu_____1	Default		%default
des cartes dans mon jeu	100	cf	[pred="avoir des cartes dans son jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir des cartes dans son jeu_____1	Default		%default
des cartes dans notre jeu	100	cf	[pred="avoir des cartes dans son jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir des cartes dans son jeu_____1	Default		%default
des cartes dans son jeu	100	cf	[pred="avoir des cartes dans son jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir des cartes dans son jeu_____1	Default		%default
des cartes dans ton jeu	100	cf	[pred="avoir des cartes dans son jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir des cartes dans son jeu_____1	Default		%default
des cartes dans votre jeu	100	cf	[pred="avoir des cartes dans son jeu_____1<Suj:cln|sn>",lightverb=avoir]	avoir des cartes dans son jeu_____1	Default		%default
des couilles au cul	100	cf	[pred="avoir des couilles au cul_____1<Suj:cln|sn>",lightverb=avoir]	avoir des couilles au cul_____1	Default		%default
des difficultés à joindre les deux bouts	100	cf	[pred="avoir des difficultés à joindre les deux bouts_____1<Suj:cln|sn>",lightverb=avoir]	avoir des difficultés à joindre les deux bouts_____1	Default		%default
des démêlés avec la justice	100	cf	[pred="avoir des démêlés avec la justice_____1<Suj:cln|sn>",lightverb=avoir]	avoir des démêlés avec la justice_____1	Default		%default
des fourmis dans les jambes	100	cf	[pred="avoir des fourmis dans les jambes_____1<Suj:cln|sn>",lightverb=avoir]	avoir des fourmis dans les jambes_____1	Default		%default
des grenouilles dans le ventre	100	cf	[pred="avoir des grenouilles dans le ventre_____1<Suj:cln|sn>",lightverb=avoir]	avoir des grenouilles dans le ventre_____1	Default		%default
des hauts et des bas	100	cf	[pred="avoir des hauts et des bas_____1<Suj:cln|sn>",lightverb=avoir]	avoir des hauts et des bas_____1	Default		%default
des intelligences dans la place	100	cf	[pred="avoir des intelligences dans la place_____1<Suj:cln|sn>",lightverb=avoir]	avoir des intelligences dans la place_____1	Default		%default
des larmes dans la voix	100	cf	[pred="avoir des larmes dans la voix_____1<Suj:cln|sn>",lightverb=avoir]	avoir des larmes dans la voix_____1	Default		%default
des larmes dans les yeux	100	cf	[pred="avoir des larmes dans les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir des larmes dans les yeux_____1	Default		%default
des mille et des cents	100	cf	[pred="avoir des mille et des cents_____1<Suj:cln|sn>",lightverb=avoir]	avoir des mille et des cents_____1	Default		%default
des noms à particule	100	cf	[pred="avoir des noms à particule_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des noms à particule_____1	Default		%default
des noms à rallonge	100	cf	[pred="avoir des noms à rallonge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des noms à rallonge_____1	Default		%default
des participations croisées dans une entreprise	100	cf	[pred="avoir des participations croisées dans une entreprise_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir des participations croisées dans une entreprise_____1	Default		%default
des poches sous les yeux	100	cf	[pred="avoir des poches sous les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir des poches sous les yeux_____1	Default		%default
des ronds de côté	100	cf	[pred="avoir des ronds de côté_____1<Suj:cln|sn>",lightverb=avoir]	avoir des ronds de côté_____1	Default		%default
des sanglots dans la voix	100	cf	[pred="avoir des sanglots dans la voix_____1<Suj:cln|sn>",lightverb=avoir]	avoir des sanglots dans la voix_____1	Default		%default
des trémolos dans la voix	100	cf	[pred="avoir des trémolos dans la voix_____1<Suj:cln|sn>",lightverb=avoir]	avoir des trémolos dans la voix_____1	Default		%default
des valises sous les yeux	100	cf	[pred="avoir des valises sous les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir des valises sous les yeux_____1	Default		%default
des yeux pour voir	100	cf	[pred="avoir des yeux pour voir_____1<Suj:cln|sn>",lightverb=avoir]	avoir des yeux pour voir_____1	Default		%default
deux comme papa	100	cf	[pred="en avoir deux comme papa_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir deux comme papa_____1	Default		%default
deux fers au feu	100	cf	[pred="avoir deux fers au feu_____1<Suj:cln|sn>",lightverb=avoir]	avoir deux fers au feu_____1	Default		%default
dix sur dix	100	cf	[pred="avoir dix sur dix_____1<Suj:cln|sn>",lightverb=avoir]	avoir dix sur dix_____1	Default		%default
douze balles dans la peau	100	cf	[pred="avoir douze balles dans la peau_____1<Suj:cln|sn>",lightverb=avoir]	avoir douze balles dans la peau_____1	Default		%default
droit au respect de leur vie privée	100	cf	[pred="avoir droit au respect de sa vie privée_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit au respect de sa vie privée_____1	Default		%default
droit au respect de ma vie privée	100	cf	[pred="avoir droit au respect de sa vie privée_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit au respect de sa vie privée_____1	Default		%default
droit au respect de notre vie privée	100	cf	[pred="avoir droit au respect de sa vie privée_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit au respect de sa vie privée_____1	Default		%default
droit au respect de sa vie privée	100	cf	[pred="avoir droit au respect de sa vie privée_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit au respect de sa vie privée_____1	Default		%default
droit au respect de ta vie privée	100	cf	[pred="avoir droit au respect de sa vie privée_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit au respect de sa vie privée_____1	Default		%default
droit au respect de votre vie privée	100	cf	[pred="avoir droit au respect de sa vie privée_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit au respect de sa vie privée_____1	Default		%default
droit aux honneurs de la presse	100	cf	[pred="avoir droit aux honneurs de la presse_____1<Suj:cln|sn>",lightverb=avoir]	avoir droit aux honneurs de la presse_____1	Default		%default
du baume au coeur	100	cf	[pred="avoir du baume au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir du baume au coeur_____1	Default		%default
du beurre dans les épinards	100	cf	[pred="avoir du beurre dans les épinards_____1<Suj:cln|sn>",lightverb=avoir]	avoir du beurre dans les épinards_____1	Default		%default
du bien au soleil	100	cf	[pred="avoir du bien au soleil_____1<Suj:cln|sn>",lightverb=avoir]	avoir du bien au soleil_____1	Default		%default
du boulot par dessus la tête	100	cf	[pred="avoir du boulot par dessus la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir du boulot par dessus la tête_____1	Default		%default
du coeur au ouvrage	100	cf	[pred="avoir du coeur au ouvrage_____1<Suj:cln|sn>",lightverb=avoir]	avoir du coeur au ouvrage_____1	Default		%default
du coeur au ventre	100	cf	[pred="avoir du coeur au ventre_____1<Suj:cln|sn>",lightverb=avoir]	avoir du coeur au ventre_____1	Default		%default
du coton dans les oreilles	100	cf	[pred="avoir du coton dans les oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir du coton dans les oreilles_____1	Default		%default
du feu dans les veines	100	cf	[pred="avoir du feu dans les veines_____1<Suj:cln|sn>",lightverb=avoir]	avoir du feu dans les veines_____1	Default		%default
du foin dans leurs bottes	100	cf	[pred="avoir du foin dans ses bottes_____1<Suj:cln|sn>",lightverb=avoir]	avoir du foin dans ses bottes_____1	Default		%default
du foin dans mes bottes	100	cf	[pred="avoir du foin dans ses bottes_____1<Suj:cln|sn>",lightverb=avoir]	avoir du foin dans ses bottes_____1	Default		%default
du foin dans nos bottes	100	cf	[pred="avoir du foin dans ses bottes_____1<Suj:cln|sn>",lightverb=avoir]	avoir du foin dans ses bottes_____1	Default		%default
du foin dans ses bottes	100	cf	[pred="avoir du foin dans ses bottes_____1<Suj:cln|sn>",lightverb=avoir]	avoir du foin dans ses bottes_____1	Default		%default
du foin dans tes bottes	100	cf	[pred="avoir du foin dans ses bottes_____1<Suj:cln|sn>",lightverb=avoir]	avoir du foin dans ses bottes_____1	Default		%default
du foin dans vos bottes	100	cf	[pred="avoir du foin dans ses bottes_____1<Suj:cln|sn>",lightverb=avoir]	avoir du foin dans ses bottes_____1	Default		%default
du fric de côté	100	cf	[pred="avoir du fric de côté_____1<Suj:cln|sn>",lightverb=avoir]	avoir du fric de côté_____1	Default		%default
du goût pour la peinture	100	cf	[pred="avoir du goût pour la peinture_____1<Suj:cln|sn>",lightverb=avoir]	avoir du goût pour la peinture_____1	Default		%default
du lait dans les oreilles	100	cf	[pred="avoir du lait dans les oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir du lait dans les oreilles_____1	Default		%default
du mal à joindre les deux bouts	100	cf	[pred="avoir du mal à joindre les deux bouts_____1<Suj:cln|sn>",lightverb=avoir]	avoir du mal à joindre les deux bouts_____1	Default		%default
du mal à passer	100	cf	[pred="avoir du mal à passer_____1<Suj:cln|sn>",lightverb=avoir]	avoir du mal à passer_____1	Default		%default
du mal à se mobiliser	100	cf	[pred="avoir du mal à se mobiliser_____1<Suj:cln|sn>",lightverb=avoir]	avoir du mal à se mobiliser_____1	Default		%default
du monde à déjeuner	100	cf	[pred="avoir du monde à déjeuner_____1<Suj:cln|sn>",lightverb=avoir]	avoir du monde à déjeuner_____1	Default		%default
du monde à dîner	100	cf	[pred="avoir du monde à dîner_____1<Suj:cln|sn>",lightverb=avoir]	avoir du monde à dîner_____1	Default		%default
du monde à goûter	100	cf	[pred="avoir du monde à goûter_____1<Suj:cln|sn>",lightverb=avoir]	avoir du monde à goûter_____1	Default		%default
du monde à manger	100	cf	[pred="avoir du monde à manger_____1<Suj:cln|sn>",lightverb=avoir]	avoir du monde à manger_____1	Default		%default
du monde à souper	100	cf	[pred="avoir du monde à souper_____1<Suj:cln|sn>",lightverb=avoir]	avoir du monde à souper_____1	Default		%default
du pain sur la planche	100	cf	[pred="avoir du pain sur la planche_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pain sur la planche_____1	Default		%default
du plomb dans l'aile	100	cf	[pred="avoir du plomb dans l'aile_____1<Suj:cln|sn>",lightverb=avoir]	avoir du plomb dans l'aile_____1	Default		%default
du plomb dans la cervelle	100	cf	[pred="avoir du plomb dans la cervelle_____1<Suj:cln|sn>",lightverb=avoir]	avoir du plomb dans la cervelle_____1	Default		%default
du plomb dans la tête	100	cf	[pred="avoir du plomb dans la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir du plomb dans la tête_____1	Default		%default
du poil au menton	100	cf	[pred="avoir du poil au menton_____1<Suj:cln|sn>",lightverb=avoir]	avoir du poil au menton_____1	Default		%default
du pèze de côté	100	cf	[pred="avoir du pèze de côté_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze de côté_____1	Default		%default
du pèze devant elle	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant elles	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant eux	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant lui	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant moi	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant nous	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant soi	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant toi	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze devant vous	100	cf	[pred="avoir du pèze devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze devant lui_____1	Default		%default
du pèze sur elle	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur elles	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur eux	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur lui	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur moi	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur nous	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur soi	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur toi	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du pèze sur vous	100	cf	[pred="avoir du pèze sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du pèze sur lui_____1	Default		%default
du retard à l'allumage	100	cf	[pred="avoir du retard à l'allumage_____1<Suj:cln|sn>",lightverb=avoir]	avoir du retard à l'allumage_____1	Default		%default
du sang bleu dans les veines	100	cf	[pred="avoir du sang bleu dans les veines_____1<Suj:cln|sn>",lightverb=avoir]	avoir du sang bleu dans les veines_____1	Default		%default
du sang de navet dans les veines	100	cf	[pred="avoir du sang de navet dans les veines_____1<Suj:cln|sn>",lightverb=avoir]	avoir du sang de navet dans les veines_____1	Default		%default
du sang sur les mains	100	cf	[pred="avoir du sang sur les mains_____1<Suj:cln|sn>",lightverb=avoir]	avoir du sang sur les mains_____1	Default		%default
du temps devant elle	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant elles	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant eux	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant lui	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant moi	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant nous	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant soi	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant toi	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps devant vous	100	cf	[pred="avoir du temps devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps devant lui_____1	Default		%default
du temps pour leurs amis	100	cf	[pred="avoir du temps pour ses amis_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps pour ses amis_____1	Default		%default
du temps pour mes amis	100	cf	[pred="avoir du temps pour ses amis_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps pour ses amis_____1	Default		%default
du temps pour nos amis	100	cf	[pred="avoir du temps pour ses amis_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps pour ses amis_____1	Default		%default
du temps pour ses amis	100	cf	[pred="avoir du temps pour ses amis_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps pour ses amis_____1	Default		%default
du temps pour tes amis	100	cf	[pred="avoir du temps pour ses amis_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps pour ses amis_____1	Default		%default
du temps pour vos amis	100	cf	[pred="avoir du temps pour ses amis_____1<Suj:cln|sn>",lightverb=avoir]	avoir du temps pour ses amis_____1	Default		%default
du travail par dessus la tête	100	cf	[pred="avoir du travail par dessus la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir du travail par dessus la tête_____1	Default		%default
du vague à la âme	100	cf	[pred="avoir du vague à la âme_____1<Suj:cln|sn>",lightverb=avoir]	avoir du vague à la âme_____1	Default		%default
du vent dans les voiles	100	cf	[pred="avoir du vent dans les voiles_____1<Suj:cln|sn>",lightverb=avoir]	avoir du vent dans les voiles_____1	Default		%default
du vif-argent dans les veines	100	cf	[pred="avoir du vif-argent dans les veines_____1<Suj:cln|sn>",lightverb=avoir]	avoir du vif-argent dans les veines_____1	Default		%default
eu un sale coup	100	cf	[pred="avoir eu un sale coup_____1<Suj:cln|sn>",lightverb=avoir]	avoir eu un sale coup_____1	Default		%default
eu une belle fin	100	cf	[pred="avoir eu une belle fin_____1<Suj:cln|sn>",lightverb=avoir]	avoir eu une belle fin_____1	Default		%default
eu une belle mort	100	cf	[pred="avoir eu une belle mort_____1<Suj:cln|sn>",lightverb=avoir]	avoir eu une belle mort_____1	Default		%default
eu une triste fin	100	cf	[pred="avoir eu une triste fin_____1<Suj:cln|sn>",lightverb=avoir]	avoir eu une triste fin_____1	Default		%default
fait leur temps	100	cf	[pred="avoir fait son temps_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait son temps_____1	Default		%default
fait mon temps	100	cf	[pred="avoir fait son temps_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait son temps_____1	Default		%default
fait notre temps	100	cf	[pred="avoir fait son temps_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait son temps_____1	Default		%default
fait quoi	100	cf	[pred="avoir fait quoi_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait quoi_____1	Default		%default
fait son temps	100	cf	[pred="avoir fait son temps_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait son temps_____1	Default		%default
fait ton temps	100	cf	[pred="avoir fait son temps_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait son temps_____1	Default		%default
fait votre temps	100	cf	[pred="avoir fait son temps_____1<Suj:cln|sn>",lightverb=avoir]	avoir fait son temps_____1	Default		%default
foi dans l'avenir	100	cf	[pred="avoir foi dans l'avenir_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans l'avenir_____1	Default		%default
foi dans le progrès	100	cf	[pred="avoir foi dans le progrès_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans le progrès_____1	Default		%default
foi dans leur bonne étoile	100	cf	[pred="avoir foi dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans sa bonne étoile_____1	Default		%default
foi dans leur étoile	100	cf	[pred="avoir foi dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans son étoile_____1	Default		%default
foi dans ma bonne étoile	100	cf	[pred="avoir foi dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans sa bonne étoile_____1	Default		%default
foi dans mon étoile	100	cf	[pred="avoir foi dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans son étoile_____1	Default		%default
foi dans notre bonne étoile	100	cf	[pred="avoir foi dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans sa bonne étoile_____1	Default		%default
foi dans notre étoile	100	cf	[pred="avoir foi dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans son étoile_____1	Default		%default
foi dans sa bonne étoile	100	cf	[pred="avoir foi dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans sa bonne étoile_____1	Default		%default
foi dans son étoile	100	cf	[pred="avoir foi dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans son étoile_____1	Default		%default
foi dans ta bonne étoile	100	cf	[pred="avoir foi dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans sa bonne étoile_____1	Default		%default
foi dans ton étoile	100	cf	[pred="avoir foi dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans son étoile_____1	Default		%default
foi dans votre bonne étoile	100	cf	[pred="avoir foi dans sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans sa bonne étoile_____1	Default		%default
foi dans votre étoile	100	cf	[pred="avoir foi dans son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi dans son étoile_____1	Default		%default
foi en DIEU	100	cf	[pred="avoir foi en DIEU_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en DIEU_____1	Default		%default
foi en l'avenir	100	cf	[pred="avoir foi en l'avenir_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en l'avenir_____1	Default		%default
foi en leur bonne étoile	100	cf	[pred="avoir foi en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en sa bonne étoile_____1	Default		%default
foi en leur étoile	100	cf	[pred="avoir foi en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en son étoile_____1	Default		%default
foi en ma bonne étoile	100	cf	[pred="avoir foi en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en sa bonne étoile_____1	Default		%default
foi en mon étoile	100	cf	[pred="avoir foi en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en son étoile_____1	Default		%default
foi en notre bonne étoile	100	cf	[pred="avoir foi en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en sa bonne étoile_____1	Default		%default
foi en notre étoile	100	cf	[pred="avoir foi en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en son étoile_____1	Default		%default
foi en sa bonne étoile	100	cf	[pred="avoir foi en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en sa bonne étoile_____1	Default		%default
foi en son étoile	100	cf	[pred="avoir foi en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en son étoile_____1	Default		%default
foi en ta bonne étoile	100	cf	[pred="avoir foi en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en sa bonne étoile_____1	Default		%default
foi en ton étoile	100	cf	[pred="avoir foi en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en son étoile_____1	Default		%default
foi en votre bonne étoile	100	cf	[pred="avoir foi en sa bonne étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en sa bonne étoile_____1	Default		%default
foi en votre étoile	100	cf	[pred="avoir foi en son étoile_____1<Suj:cln|sn>",lightverb=avoir]	avoir foi en son étoile_____1	Default		%default
froid aux mains	100	cf	[pred="avoir froid aux mains_____1<Suj:cln|sn>",lightverb=avoir]	avoir froid aux mains_____1	Default		%default
froid aux oreilles	100	cf	[pred="avoir froid aux oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir froid aux oreilles_____1	Default		%default
froid aux pieds	100	cf	[pred="avoir froid aux pieds_____1<Suj:cln|sn>",lightverb=avoir]	avoir froid aux pieds_____1	Default		%default
gros sur la conscience	100	cf	[pred="en avoir gros sur la conscience_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir gros sur la conscience_____1	Default		%default
gros sur la patate	100	cf	[pred="en avoir gros sur la patate_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir gros sur la patate_____1	Default		%default
gros sur le coeur	100	cf	[pred="en avoir gros sur le coeur_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir gros sur le coeur_____1	Default		%default
jamais ouvert un livre	100	cf	[pred="n' avoir jamais ouvert un livre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir jamais ouvert un livre_____1	Default		%default
jamais rien vu	100	cf	[pred="n' avoir jamais rien vu_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir jamais rien vu_____1	Default		%default
l'air mais pas la chanson	100	cf	[pred="en avoir l'air mais pas la chanson_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir l'air mais pas la chanson_____1	Default		%default
l'appartement à payer	100	cf	[pred="avoir l'appartement à payer_____1<Suj:cln|sn>",lightverb=avoir]	avoir l'appartement à payer_____1	Default		%default
l'eau à la bouche	100	cf	[pred="avoir l'eau à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir l'eau à la bouche_____1	Default		%default
l'oeil à tout	100	cf	[pred="avoir l'oeil à tout_____1<Suj:cln|sn>",lightverb=avoir]	avoir l'oeil à tout_____1	Default		%default
la bague au doigt	100	cf	[pred="avoir la bague au doigt_____1<Suj:cln|sn>",lightverb=avoir]	avoir la bague au doigt_____1	Default		%default
la bouche en coeur	100	cf	[pred="avoir la bouche en coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir la bouche en coeur_____1	Default		%default
la bride sur le cou	100	cf	[pred="avoir la bride sur le cou_____1<Suj:cln|sn>",lightverb=avoir]	avoir la bride sur le cou_____1	Default		%default
la corde au cou	100	cf	[pred="avoir la corde au cou_____1<Suj:cln|sn>",lightverb=avoir]	avoir la corde au cou_____1	Default		%default
la fleur au fusil	100	cf	[pred="avoir la fleur au fusil_____1<Suj:cln|sn>",lightverb=avoir]	avoir la fleur au fusil_____1	Default		%default
la goutte au nez	100	cf	[pred="avoir la goutte au nez_____1<Suj:cln|sn>",lightverb=avoir]	avoir la goutte au nez_____1	Default		%default
la insulte à la bouche	100	cf	[pred="avoir la insulte à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir la insulte à la bouche_____1	Default		%default
la joie au coeur	100	cf	[pred="avoir la joie au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir la joie au coeur_____1	Default		%default
la larme au oeil	100	cf	[pred="avoir la larme au oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir la larme au oeil_____1	Default		%default
la loi pour elle	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour elles	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour eux	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour lui	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour moi	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour nous	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour soi	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour toi	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la loi pour vous	100	cf	[pred="avoir la loi pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la loi pour lui_____1	Default		%default
la main au bonnet	100	cf	[pred="avoir la main au bonnet_____1<Suj:cln|sn>",lightverb=avoir]	avoir la main au bonnet_____1	Default		%default
la main sur le coeur	100	cf	[pred="avoir la main sur le coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir la main sur le coeur_____1	Default		%default
la main à la pâte	100	cf	[pred="avoir la main à la pâte_____1<Suj:cln|sn>",lightverb=avoir]	avoir la main à la pâte_____1	Default		%default
la maison à payer	100	cf	[pred="avoir la maison à payer_____1<Suj:cln|sn>",lightverb=avoir]	avoir la maison à payer_____1	Default		%default
la marche à suivre	100	cf	[pred="avoir la marche à suivre_____1<Suj:cln|sn>",lightverb=avoir]	avoir la marche à suivre_____1	Default		%default
la mort dans la âme	100	cf	[pred="avoir la mort dans la âme_____1<Suj:cln|sn>",lightverb=avoir]	avoir la mort dans la âme_____1	Default		%default
la peur au ventre	100	cf	[pred="avoir la peur au ventre_____1<Suj:cln|sn>",lightverb=avoir]	avoir la peur au ventre_____1	Default		%default
la pipe à la bouche	100	cf	[pred="avoir la pipe à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir la pipe à la bouche_____1	Default		%default
la puce à l'oreille	100	cf	[pred="avoir la puce à l'oreille_____1<Suj:cln|sn>",lightverb=avoir]	avoir la puce à l'oreille_____1	Default		%default
la queue entre les jambes	100	cf	[pred="avoir la queue entre les jambes_____1<Suj:cln|sn>",lightverb=avoir]	avoir la queue entre les jambes_____1	Default		%default
la rage au coeur	100	cf	[pred="avoir la rage au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir la rage au coeur_____1	Default		%default
la rage au ventre	100	cf	[pred="avoir la rage au ventre_____1<Suj:cln|sn>",lightverb=avoir]	avoir la rage au ventre_____1	Default		%default
la situation en main	100	cf	[pred="avoir la situation en main_____1<Suj:cln|sn>",lightverb=avoir]	avoir la situation en main_____1	Default		%default
la situation en mains	100	cf	[pred="avoir la situation en mains_____1<Suj:cln|sn>",lightverb=avoir]	avoir la situation en mains_____1	Default		%default
la tête comme une passoire	100	cf	[pred="avoir la tête comme une passoire_____1<Suj:cln|sn>",lightverb=avoir]	avoir la tête comme une passoire_____1	Default		%default
la tête en bas	100	cf	[pred="avoir la tête en bas_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir la tête en bas_____1	Default		%default
la tête en l'air	100	cf	[pred="avoir la tête en l'air_____1<Suj:cln|sn>",lightverb=avoir]	avoir la tête en l'air_____1	Default		%default
la tête hors de l'eau	100	cf	[pred="avoir la tête hors de l'eau_____1<Suj:cln|sn>",lightverb=avoir]	avoir la tête hors de l'eau_____1	Default		%default
la voiture à payer	100	cf	[pred="avoir la voiture à payer_____1<Suj:cln|sn>",lightverb=avoir]	avoir la voiture à payer_____1	Default		%default
la âme en peine	100	cf	[pred="avoir la âme en peine_____1<Suj:cln|sn>",lightverb=avoir]	avoir la âme en peine_____1	Default		%default
la éternité devant elle	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant elles	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant eux	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant lui	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant moi	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant nous	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant soi	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant toi	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité devant vous	100	cf	[pred="avoir la éternité devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité devant lui_____1	Default		%default
la éternité à elle	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à elles	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à eux	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à lui	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à moi	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à nous	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à soi	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à toi	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
la éternité à vous	100	cf	[pred="avoir la éternité à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir la éternité à lui_____1	Default		%default
le bec dans l'eau	100	cf	[pred="avoir le bec dans l'eau_____1<Suj:cln|sn>",lightverb=avoir]	avoir le bec dans l'eau_____1	Default		%default
le beurre et l'argent du beurre	100	cf	[pred="avoir le beurre et l'argent du beurre_____1<Suj:cln|sn>",lightverb=avoir]	avoir le beurre et l'argent du beurre_____1	Default		%default
le bérêt sur l'oreille	100	cf	[pred="avoir le bérêt sur l'oreille_____1<Suj:cln|sn>",lightverb=avoir]	avoir le bérêt sur l'oreille_____1	Default		%default
le chapeau sur l'oreille	100	cf	[pred="avoir le chapeau sur l'oreille_____1<Suj:cln|sn>",lightverb=avoir]	avoir le chapeau sur l'oreille_____1	Default		%default
le coeur au ouvrage	100	cf	[pred="avoir le coeur au ouvrage_____1<Suj:cln|sn>",lightverb=avoir]	avoir le coeur au ouvrage_____1	Default		%default
le coeur sur la main	100	cf	[pred="avoir le coeur sur la main_____1<Suj:cln|sn>",lightverb=avoir]	avoir le coeur sur la main_____1	Default		%default
le coeur à rien	100	cf	[pred="n' avoir le coeur à rien_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir le coeur à rien_____1	Default		%default
le compas dans l'oeil	100	cf	[pred="avoir le compas dans l'oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir le compas dans l'oeil_____1	Default		%default
le cul entre deux chaises	100	cf	[pred="avoir le cul entre deux chaises_____1<Suj:cln|sn>",lightverb=avoir]	avoir le cul entre deux chaises_____1	Default		%default
le diable au corps	100	cf	[pred="avoir le diable au corps_____1<Suj:cln|sn>",lightverb=avoir]	avoir le diable au corps_____1	Default		%default
le diable dans la peau	100	cf	[pred="avoir le diable dans la peau_____1<Suj:cln|sn>",lightverb=avoir]	avoir le diable dans la peau_____1	Default		%default
le doigt sur la détente	100	cf	[pred="avoir le doigt sur la détente_____1<Suj:cln|sn>",lightverb=avoir]	avoir le doigt sur la détente_____1	Default		%default
le dos au mur	100	cf	[pred="avoir le dos au mur_____1<Suj:cln|sn>",lightverb=avoir]	avoir le dos au mur_____1	Default		%default
le droit pour elle	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour elles	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour eux	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour lui	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour moi	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour nous	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour soi	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour toi	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le droit pour vous	100	cf	[pred="avoir le droit pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir le droit pour lui_____1	Default		%default
le feu au cul	100	cf	[pred="avoir le feu au cul_____1<Suj:cln|sn>",lightverb=avoir]	avoir le feu au cul_____1	Default		%default
le feu au derrière	100	cf	[pred="avoir le feu au derrière_____1<Suj:cln|sn>",lightverb=avoir]	avoir le feu au derrière_____1	Default		%default
le feu aux fesses	100	cf	[pred="avoir le feu aux fesses_____1<Suj:cln|sn>",lightverb=avoir]	avoir le feu aux fesses_____1	Default		%default
le marché en mains	100	cf	[pred="avoir le marché en mains_____1<Suj:cln|sn>",lightverb=avoir]	avoir le marché en mains_____1	Default		%default
le mot aux lèvres	100	cf	[pred="avoir le mot aux lèvres_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le mot aux lèvres_____1	Default		%default
le mot pour rire	100	cf	[pred="avoir le mot pour rire_____1<Suj:cln|sn>",lightverb=avoir]	avoir le mot pour rire_____1	Default		%default
le nez en l'air	100	cf	[pred="avoir le nez en l'air_____1<Suj:cln|sn>",lightverb=avoir]	avoir le nez en l'air_____1	Default		%default
le pied au étrier	100	cf	[pred="avoir le pied au étrier_____1<Suj:cln|sn>",lightverb=avoir]	avoir le pied au étrier_____1	Default		%default
le pistolet au poing	100	cf	[pred="avoir le pistolet au poing_____1<Suj:cln|sn>",lightverb=avoir]	avoir le pistolet au poing_____1	Default		%default
le revolver au poing	100	cf	[pred="avoir le revolver au poing_____1<Suj:cln|sn>",lightverb=avoir]	avoir le revolver au poing_____1	Default		%default
le règlement pour elle	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour elles	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour eux	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour lui	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour moi	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour nous	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour soi	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour toi	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le règlement pour vous	100	cf	[pred="avoir le règlement pour lui_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir le règlement pour lui_____1	Default		%default
le sourire au coin des lèvres	100	cf	[pred="avoir le sourire au coin des lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir le sourire au coin des lèvres_____1	Default		%default
le vent en poupe	100	cf	[pred="avoir le vent en poupe_____1<Suj:cln|sn>",lightverb=avoir]	avoir le vent en poupe_____1	Default		%default
les bras le long du corps	100	cf	[pred="avoir les bras le long du corps_____1<Suj:cln|sn>",lightverb=avoir]	avoir les bras le long du corps_____1	Default		%default
les bras à la retourne	100	cf	[pred="avoir les bras à la retourne_____1<Suj:cln|sn>",lightverb=avoir]	avoir les bras à la retourne_____1	Default		%default
les cheveux au vent	100	cf	[pred="avoir les cheveux au vent_____1<Suj:cln|sn>",lightverb=avoir]	avoir les cheveux au vent_____1	Default		%default
les cheveux en coup de vent	100	cf	[pred="avoir les cheveux en coup de vent_____1<Suj:cln|sn>",lightverb=avoir]	avoir les cheveux en coup de vent_____1	Default		%default
les chevilles qui enflent	100	cf	[pred="avoir les chevilles qui enflent_____1<Suj:cln|sn>",lightverb=avoir]	avoir les chevilles qui enflent_____1	Default		%default
les coudes sur la table	100	cf	[pred="avoir les coudes sur la table_____1<Suj:cln|sn>",lightverb=avoir]	avoir les coudes sur la table_____1	Default		%default
les côtes en long	100	cf	[pred="avoir les côtes en long_____1<Suj:cln|sn>",lightverb=avoir]	avoir les côtes en long_____1	Default		%default
les deux pieds dans le même sabot	100	cf	[pred="avoir les deux pieds dans le même sabot_____1<Suj:cln|sn>",lightverb=avoir]	avoir les deux pieds dans le même sabot_____1	Default		%default
les deux pieds dans le même soulier	100	cf	[pred="avoir les deux pieds dans le même soulier_____1<Suj:cln|sn>",lightverb=avoir]	avoir les deux pieds dans le même soulier_____1	Default		%default
les fers aux pieds	100	cf	[pred="avoir les fers aux pieds_____1<Suj:cln|sn>",lightverb=avoir]	avoir les fers aux pieds_____1	Default		%default
les fesses au chaud	100	cf	[pred="avoir les fesses au chaud_____1<Suj:cln|sn>",lightverb=avoir]	avoir les fesses au chaud_____1	Default		%default
les larmes aux yeux	100	cf	[pred="avoir les larmes aux yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir les larmes aux yeux_____1	Default		%default
les mains dans la graisse	100	cf	[pred="avoir les mains dans la graisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir les mains dans la graisse_____1	Default		%default
les mains dans la merde	100	cf	[pred="avoir les mains dans la merde_____1<Suj:cln|sn>",lightverb=avoir]	avoir les mains dans la merde_____1	Default		%default
les mains dans le cambouis	100	cf	[pred="avoir les mains dans le cambouis_____1<Suj:cln|sn>",lightverb=avoir]	avoir les mains dans le cambouis_____1	Default		%default
les mains derrière le dos	100	cf	[pred="avoir les mains derrière le dos_____1<Suj:cln|sn>",lightverb=avoir]	avoir les mains derrière le dos_____1	Default		%default
les menottes aux poignets	100	cf	[pred="avoir les menottes aux poignets_____1<Suj:cln|sn>",lightverb=avoir]	avoir les menottes aux poignets_____1	Default		%default
les pieds sur terre	100	cf	[pred="avoir les pieds sur terre_____1<Suj:cln|sn>",lightverb=avoir]	avoir les pieds sur terre_____1	Default		%default
les questions mais pas les réponses	100	cf	[pred="avoir les questions mais pas les réponses_____1<Suj:cln|sn>",lightverb=avoir]	avoir les questions mais pas les réponses_____1	Default		%default
les seins à l'air	100	cf	[pred="avoir les seins à l'air_____1<Suj:cln|sn>",lightverb=avoir]	avoir les seins à l'air_____1	Default		%default
les tripes au soleil	100	cf	[pred="avoir les tripes au soleil_____1<Suj:cln|sn>",lightverb=avoir]	avoir les tripes au soleil_____1	Default		%default
les tripes à l'air	100	cf	[pred="avoir les tripes à l'air_____1<Suj:cln|sn>",lightverb=avoir]	avoir les tripes à l'air_____1	Default		%default
leur langue dans leur poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
leur langue dans ma poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
leur langue dans notre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
leur langue dans sa poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
leur langue dans ta poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
leur langue dans votre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
leur mot à dire	100	cf	[pred="avoir son mot à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir son mot à dire_____1	Default		%default
leur vieille mère à charge	100	cf	[pred="avoir sa vieille mère à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir sa vieille mère à charge_____1	Default		%default
leur vieux père à charge	100	cf	[pred="avoir son vieux père à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir son vieux père à charge_____1	Default		%default
leurs vieux parent à charge	100	cf	[pred="avoir ses vieux parent à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir ses vieux parent à charge_____1	Default		%default
lourd sur la conscience	100	cf	[pred="en avoir lourd sur la conscience_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir lourd sur la conscience_____1	Default		%default
lourd sur la patate	100	cf	[pred="en avoir lourd sur la patate_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir lourd sur la patate_____1	Default		%default
lourd sur le coeur	100	cf	[pred="en avoir lourd sur le coeur_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir lourd sur le coeur_____1	Default		%default
ma langue dans leur poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ma langue dans ma poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ma langue dans notre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ma langue dans sa poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ma langue dans ta poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ma langue dans votre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ma vieille mère à charge	100	cf	[pred="avoir sa vieille mère à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir sa vieille mère à charge_____1	Default		%default
mal au coeur	100	cf	[pred="avoir mal au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal au coeur_____1	Default		%default
mal au estomac	100	cf	[pred="avoir mal au estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal au estomac_____1	Default		%default
mal au foie	100	cf	[pred="avoir mal au foie_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal au foie_____1	Default		%default
mal aux cheveux	100	cf	[pred="avoir mal aux cheveux_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal aux cheveux_____1	Default		%default
mal aux dents	100	cf	[pred="avoir mal aux dents_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal aux dents_____1	Default		%default
mal aux oreilles	100	cf	[pred="avoir mal aux oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal aux oreilles_____1	Default		%default
mal aux pieds	100	cf	[pred="avoir mal aux pieds_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal aux pieds_____1	Default		%default
mal aux reins	100	cf	[pred="avoir mal aux reins_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal aux reins_____1	Default		%default
mal aux yeux	100	cf	[pred="avoir mal aux yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal aux yeux_____1	Default		%default
mal dans les oreilles	100	cf	[pred="avoir mal dans les oreilles_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal dans les oreilles_____1	Default		%default
mal à la tête	100	cf	[pred="avoir mal à la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir mal à la tête_____1	Default		%default
martel en tête	100	cf	[pred="avoir martel en tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir martel en tête_____1	Default		%default
mes vieux parent à charge	100	cf	[pred="avoir ses vieux parent à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir ses vieux parent à charge_____1	Default		%default
mon mot à dire	100	cf	[pred="avoir son mot à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir son mot à dire_____1	Default		%default
mon vieux père à charge	100	cf	[pred="avoir son vieux père à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir son vieux père à charge_____1	Default		%default
ni Dieu ni maître	100	cf	[pred="n' avoir ni Dieu ni maître_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni Dieu ni maître_____1	Default		%default
ni feu ni lieu	100	cf	[pred="n' avoir ni feu ni lieu_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni feu ni lieu_____1	Default		%default
ni fin ni trève	100	cf	[pred="n' avoir ni fin ni trève_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni fin ni trève_____1	Default		%default
ni foi ni loi	100	cf	[pred="n' avoir ni foi ni loi_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni foi ni loi_____1	Default		%default
ni queue ni tête	100	cf	[pred="n' avoir ni queue ni tête_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni queue ni tête_____1	Default		%default
ni rime ni raison	100	cf	[pred="n' avoir ni rime ni raison_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni rime ni raison_____1	Default		%default
ni sou ni maille	100	cf	[pred="n' avoir ni sou ni maille_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir ni sou ni maille_____1	Default		%default
nos vieux parent à charge	100	cf	[pred="avoir ses vieux parent à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir ses vieux parent à charge_____1	Default		%default
notre langue dans leur poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
notre langue dans ma poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
notre langue dans notre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
notre langue dans sa poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
notre langue dans ta poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
notre langue dans votre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
notre mot à dire	100	cf	[pred="avoir son mot à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir son mot à dire_____1	Default		%default
notre vieille mère à charge	100	cf	[pred="avoir sa vieille mère à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir sa vieille mère à charge_____1	Default		%default
notre vieux père à charge	100	cf	[pred="avoir son vieux père à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir son vieux père à charge_____1	Default		%default
nulle part où aller	100	cf	[pred="n' avoir nulle part où aller_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir nulle part où aller_____1	Default		%default
pas de pitié pour les canards boiteux	100	cf	[pred="n' avoir pas de pitié pour les canards boiteux_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas de pitié pour les canards boiteux_____1	Default		%default
pas de souci à se faire	100	cf	[pred="n' avoir pas de souci à se faire_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir|mettre]	n' avoir pas de souci à se faire_____1	Default		%default
pas de temps à perdre	100	cf	[pred="n' avoir pas de temps à perdre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas de temps à perdre_____1	Default		%default
pas droit à l'erreur	100	cf	[pred="n' avoir pas droit à l'erreur_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas droit à l'erreur_____1	Default		%default
pas eu de enfance	100	cf	[pred="n' avoir pas eu de enfance_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas eu de enfance_____1	Default		%default
pas froid aux yeux	100	cf	[pred="n' avoir pas froid aux yeux_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas froid aux yeux_____1	Default		%default
pas grand chose à faire	100	cf	[pred="n' avoir pas grand chose à faire_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas grand chose à faire_____1	Default		%default
pas grand chose à perdre	100	cf	[pred="n' avoir pas grand chose à perdre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas grand chose à perdre_____1	Default		%default
pas le droit à l'erreur	100	cf	[pred="n' avoir pas le droit à l'erreur_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas le droit à l'erreur_____1	Default		%default
pas les yeux dans leur poche	100	cf	[pred="n' avoir pas les yeux dans sa poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas les yeux dans sa poche_____1	Default		%default
pas les yeux dans ma poche	100	cf	[pred="n' avoir pas les yeux dans sa poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas les yeux dans sa poche_____1	Default		%default
pas les yeux dans notre poche	100	cf	[pred="n' avoir pas les yeux dans sa poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas les yeux dans sa poche_____1	Default		%default
pas les yeux dans sa poche	100	cf	[pred="n' avoir pas les yeux dans sa poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas les yeux dans sa poche_____1	Default		%default
pas les yeux dans ta poche	100	cf	[pred="n' avoir pas les yeux dans sa poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas les yeux dans sa poche_____1	Default		%default
pas les yeux dans votre poche	100	cf	[pred="n' avoir pas les yeux dans sa poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas les yeux dans sa poche_____1	Default		%default
pas loin à aller	100	cf	[pred="n' avoir pas loin à aller_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas loin à aller_____1	Default		%default
pas perdu leur journée	100	cf	[pred="n' avoir pas perdu sa journée_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu sa journée_____1	Default		%default
pas perdu leur temps	100	cf	[pred="n' avoir pas perdu son temps_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu son temps_____1	Default		%default
pas perdu ma journée	100	cf	[pred="n' avoir pas perdu sa journée_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu sa journée_____1	Default		%default
pas perdu mon temps	100	cf	[pred="n' avoir pas perdu son temps_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu son temps_____1	Default		%default
pas perdu notre journée	100	cf	[pred="n' avoir pas perdu sa journée_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu sa journée_____1	Default		%default
pas perdu notre temps	100	cf	[pred="n' avoir pas perdu son temps_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu son temps_____1	Default		%default
pas perdu sa journée	100	cf	[pred="n' avoir pas perdu sa journée_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu sa journée_____1	Default		%default
pas perdu son temps	100	cf	[pred="n' avoir pas perdu son temps_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu son temps_____1	Default		%default
pas perdu ta journée	100	cf	[pred="n' avoir pas perdu sa journée_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu sa journée_____1	Default		%default
pas perdu ton temps	100	cf	[pred="n' avoir pas perdu son temps_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu son temps_____1	Default		%default
pas perdu votre journée	100	cf	[pred="n' avoir pas perdu sa journée_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu sa journée_____1	Default		%default
pas perdu votre temps	100	cf	[pred="n' avoir pas perdu son temps_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas perdu son temps_____1	Default		%default
pas un centime de côté	100	cf	[pred="n' avoir pas un centime de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime de côté_____1	Default		%default
pas un centime devant elle	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant elles	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant eux	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant lui	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant moi	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant nous	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant soi	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant toi	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime devant vous	100	cf	[pred="n' avoir pas un centime devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime devant lui_____1	Default		%default
pas un centime en poche	100	cf	[pred="n' avoir pas un centime en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime en poche_____1	Default		%default
pas un centime sur elle	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur elles	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur eux	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur lui	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur moi	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur nous	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur soi	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur toi	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un centime sur vous	100	cf	[pred="n' avoir pas un centime sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un centime sur lui_____1	Default		%default
pas un cheveu sur le caillou	100	cf	[pred="n' avoir pas un cheveu sur le caillou_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un cheveu sur le caillou_____1	Default		%default
pas un instant à elle	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à elles	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à eux	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à lui	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à moi	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à nous	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à soi	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à toi	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un instant à vous	100	cf	[pred="n' avoir pas un instant à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un instant à lui_____1	Default		%default
pas un kopeck devant elle	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant elles	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant eux	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant lui	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant moi	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant nous	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant soi	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant toi	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck devant vous	100	cf	[pred="n' avoir pas un kopeck devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck devant lui_____1	Default		%default
pas un kopeck sur elle	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur elles	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur eux	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur lui	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur moi	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur nous	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur soi	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur toi	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck sur vous	100	cf	[pred="n' avoir pas un kopeck sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck sur lui_____1	Default		%default
pas un kopeck à elle	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à elles	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à eux	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à lui	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à moi	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à nous	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à soi	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à toi	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un kopeck à vous	100	cf	[pred="n' avoir pas un kopeck à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un kopeck à lui_____1	Default		%default
pas un liard de côté	100	cf	[pred="n' avoir pas un liard de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir|mettre]	n' avoir pas un liard de côté_____1	Default		%default
pas un liard devant elle	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant elles	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant eux	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant lui	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant moi	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant nous	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant soi	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant toi	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard devant vous	100	cf	[pred="n' avoir pas un liard devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard devant lui_____1	Default		%default
pas un liard en poche	100	cf	[pred="n' avoir pas un liard en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir|mettre]	n' avoir pas un liard en poche_____1	Default		%default
pas un liard sur elle	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur elles	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur eux	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur lui	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur moi	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur nous	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur soi	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur toi	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard sur vous	100	cf	[pred="n' avoir pas un liard sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard sur lui_____1	Default		%default
pas un liard à elle	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à elles	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à eux	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à lui	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à moi	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à nous	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à soi	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à toi	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un liard à vous	100	cf	[pred="n' avoir pas un liard à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un liard à lui_____1	Default		%default
pas un maravédis de côté	100	cf	[pred="n' avoir pas un maravédis de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis de côté_____1	Default		%default
pas un maravédis devant elle	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant elles	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant eux	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant lui	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant moi	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant nous	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant soi	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant toi	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis devant vous	100	cf	[pred="n' avoir pas un maravédis devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis devant lui_____1	Default		%default
pas un maravédis en poche	100	cf	[pred="n' avoir pas un maravédis en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis en poche_____1	Default		%default
pas un maravédis sur elle	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur elles	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur eux	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur lui	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur moi	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur nous	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur soi	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur toi	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis sur vous	100	cf	[pred="n' avoir pas un maravédis sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis sur lui_____1	Default		%default
pas un maravédis à elle	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à elles	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à eux	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à lui	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à moi	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à nous	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à soi	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à toi	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un maravédis à vous	100	cf	[pred="n' avoir pas un maravédis à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un maravédis à lui_____1	Default		%default
pas un moment à elle	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à elles	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à eux	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à lui	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à moi	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à nous	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à soi	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à toi	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un moment à vous	100	cf	[pred="n' avoir pas un moment à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un moment à lui_____1	Default		%default
pas un poil de sec	100	cf	[pred="n' avoir pas un poil de sec_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un poil de sec_____1	Default		%default
pas un poil sur le caillou	100	cf	[pred="n' avoir pas un poil sur le caillou_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un poil sur le caillou_____1	Default		%default
pas un radis de côté	100	cf	[pred="n' avoir pas un radis de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un radis de côté_____1	Default		%default
pas un radis devant elle	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant elles	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant eux	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant lui	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant moi	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant nous	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant soi	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant toi	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis devant vous	100	cf	[pred="avoir pas un radis devant lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis devant lui_____1	Default		%default
pas un radis en poche	100	cf	[pred="n' avoir pas un radis en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un radis en poche_____1	Default		%default
pas un radis sur elle	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur elles	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur eux	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur lui	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur moi	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur nous	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur soi	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur toi	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis sur vous	100	cf	[pred="avoir pas un radis sur lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis sur lui_____1	Default		%default
pas un radis à elle	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à elles	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à eux	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à lui	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à moi	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à nous	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à soi	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à toi	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un radis à vous	100	cf	[pred="avoir pas un radis à lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir pas un radis à lui_____1	Default		%default
pas un rond de côté	100	cf	[pred="n' avoir pas un rond de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond de côté_____1	Default		%default
pas un rond devant elle	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant elles	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant eux	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant lui	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant moi	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant nous	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant soi	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant toi	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond devant vous	100	cf	[pred="n' avoir pas un rond devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond devant lui_____1	Default		%default
pas un rond en poche	100	cf	[pred="n' avoir pas un rond en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond en poche_____1	Default		%default
pas un rond sur elle	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur elles	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur eux	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur lui	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur moi	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur nous	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur soi	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur toi	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rond sur vous	100	cf	[pred="n' avoir pas un rond sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rond sur lui_____1	Default		%default
pas un rotin de côté	100	cf	[pred="n' avoir pas un rotin de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin de côté_____1	Default		%default
pas un rotin devant elle	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant elles	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant eux	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant lui	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant moi	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant nous	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant soi	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant toi	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin devant vous	100	cf	[pred="n' avoir pas un rotin devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin devant lui_____1	Default		%default
pas un rotin en poche	100	cf	[pred="n' avoir pas un rotin en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin en poche_____1	Default		%default
pas un rotin sur elle	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur elles	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur eux	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur lui	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur moi	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur nous	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur soi	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur toi	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin sur vous	100	cf	[pred="n' avoir pas un rotin sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin sur lui_____1	Default		%default
pas un rotin à elle	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à elles	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à eux	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à lui	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à moi	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à nous	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à soi	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à toi	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un rotin à vous	100	cf	[pred="n' avoir pas un rotin à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un rotin à lui_____1	Default		%default
pas un sou de côté	100	cf	[pred="n' avoir pas un sou de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou de côté_____1	Default		%default
pas un sou devant elle	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant elles	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant eux	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant lui	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant moi	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant nous	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant soi	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant toi	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou devant vous	100	cf	[pred="n' avoir pas un sou devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou devant lui_____1	Default		%default
pas un sou en poche	100	cf	[pred="n' avoir pas un sou en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou en poche_____1	Default		%default
pas un sou sur elle	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur elles	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur eux	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur lui	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur moi	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur nous	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur soi	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur toi	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou sur vous	100	cf	[pred="n' avoir pas un sou sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou sur lui_____1	Default		%default
pas un sou à elle	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à elles	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à eux	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à lui	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à moi	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à nous	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à soi	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à toi	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas un sou à vous	100	cf	[pred="n' avoir pas un sou à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas un sou à lui_____1	Default		%default
pas une gueule à sucer de la glace	100	cf	[pred="n' avoir pas une gueule à sucer de la glace_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une gueule à sucer de la glace_____1	Default		%default
pas une gueule à sucer des glaçons	100	cf	[pred="n' avoir pas une gueule à sucer des glaçons_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une gueule à sucer des glaçons_____1	Default		%default
pas une minute à elle	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à elles	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à eux	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à lui	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à moi	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à nous	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à perdre	100	cf	[pred="n' avoir pas une minute à perdre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à perdre_____1	Default		%default
pas une minute à soi	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à toi	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une minute à vous	100	cf	[pred="n' avoir pas une minute à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une minute à lui_____1	Default		%default
pas une seconde à elle	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à elles	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à eux	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à lui	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à moi	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à nous	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à perdre	100	cf	[pred="n' avoir pas une seconde à perdre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à perdre_____1	Default		%default
pas une seconde à soi	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à toi	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une seconde à vous	100	cf	[pred="n' avoir pas une seconde à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une seconde à lui_____1	Default		%default
pas une thune de côté	100	cf	[pred="n' avoir pas une thune de côté_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune de côté_____1	Default		%default
pas une thune devant elle	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant elles	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant eux	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant lui	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant moi	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant nous	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant soi	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant toi	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune devant vous	100	cf	[pred="n' avoir pas une thune devant lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune devant lui_____1	Default		%default
pas une thune en poche	100	cf	[pred="n' avoir pas une thune en poche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune en poche_____1	Default		%default
pas une thune sur elle	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur elles	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur eux	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur lui	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur moi	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur nous	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur soi	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur toi	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune sur vous	100	cf	[pred="n' avoir pas une thune sur lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune sur lui_____1	Default		%default
pas une thune à elle	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à elles	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à eux	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à lui	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à moi	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à nous	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à soi	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à toi	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une thune à vous	100	cf	[pred="n' avoir pas une thune à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une thune à lui_____1	Default		%default
pas une tronche à sucer de la glace	100	cf	[pred="n' avoir pas une tronche à sucer de la glace_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une tronche à sucer de la glace_____1	Default		%default
pas une tronche à sucer des glaçons	100	cf	[pred="n' avoir pas une tronche à sucer des glaçons_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une tronche à sucer des glaçons_____1	Default		%default
pas une tête à sucer de la glace	100	cf	[pred="n' avoir pas une tête à sucer de la glace_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une tête à sucer de la glace_____1	Default		%default
pas une tête à sucer des glaçons	100	cf	[pred="n' avoir pas une tête à sucer des glaçons_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir pas une tête à sucer des glaçons_____1	Default		%default
peu à faire	100	cf	[pred="avoir peu à faire_____1<Suj:cln|sn>",lightverb=avoir]	avoir peu à faire_____1	Default		%default
peur de leur ombre	100	cf	[pred="avoir peur de son ombre_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur de son ombre_____1	Default		%default
peur de mon ombre	100	cf	[pred="avoir peur de son ombre_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur de son ombre_____1	Default		%default
peur de notre ombre	100	cf	[pred="avoir peur de son ombre_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur de son ombre_____1	Default		%default
peur de son ombre	100	cf	[pred="avoir peur de son ombre_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur de son ombre_____1	Default		%default
peur de ton ombre	100	cf	[pred="avoir peur de son ombre_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur de son ombre_____1	Default		%default
peur de votre ombre	100	cf	[pred="avoir peur de son ombre_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur de son ombre_____1	Default		%default
peur des mots	100	cf	[pred="avoir peur des mots_____1<Suj:cln|sn>",lightverb=avoir]	avoir peur des mots_____1	Default		%default
pignon sur rue	100	cf	[pred="avoir pignon sur rue_____1<Suj:cln|sn>",lightverb=avoir]	avoir pignon sur rue_____1	Default		%default
plus d'un tour dans leur sac	100	cf	[pred="avoir plus d'un tour dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'un tour dans son sac_____1	Default		%default
plus d'un tour dans mon sac	100	cf	[pred="avoir plus d'un tour dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'un tour dans son sac_____1	Default		%default
plus d'un tour dans notre sac	100	cf	[pred="avoir plus d'un tour dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'un tour dans son sac_____1	Default		%default
plus d'un tour dans son sac	100	cf	[pred="avoir plus d'un tour dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'un tour dans son sac_____1	Default		%default
plus d'un tour dans ton sac	100	cf	[pred="avoir plus d'un tour dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'un tour dans son sac_____1	Default		%default
plus d'un tour dans votre sac	100	cf	[pred="avoir plus d'un tour dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'un tour dans son sac_____1	Default		%default
plus d'une ruse dans leur sac	100	cf	[pred="avoir plus d'une ruse dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'une ruse dans son sac_____1	Default		%default
plus d'une ruse dans mon sac	100	cf	[pred="avoir plus d'une ruse dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'une ruse dans son sac_____1	Default		%default
plus d'une ruse dans notre sac	100	cf	[pred="avoir plus d'une ruse dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'une ruse dans son sac_____1	Default		%default
plus d'une ruse dans son sac	100	cf	[pred="avoir plus d'une ruse dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'une ruse dans son sac_____1	Default		%default
plus d'une ruse dans ton sac	100	cf	[pred="avoir plus d'une ruse dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'une ruse dans son sac_____1	Default		%default
plus d'une ruse dans votre sac	100	cf	[pred="avoir plus d'une ruse dans son sac_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus d'une ruse dans son sac_____1	Default		%default
plus de peur que de mal	100	cf	[pred="avoir plus de peur que de mal_____1<Suj:cln|sn>",lightverb=avoir]	avoir plus de peur que de mal_____1	Default		%default
plus leur tête à elle	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à elles	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à eux	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à lui	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à moi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à nous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à soi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à toi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus leur tête à vous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à elle	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à elles	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à eux	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à lui	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à moi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à nous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à soi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à toi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ma tête à vous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à elle	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à elles	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à eux	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à lui	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à moi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à nous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à soi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à toi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus notre tête à vous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus que les yeux pour pleurer	100	cf	[pred="n' avoir plus que les yeux pour pleurer_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus que les yeux pour pleurer_____1	Default		%default
plus rien à désirer	100	cf	[pred="n' avoir plus rien à désirer_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus rien à désirer_____1	Default		%default
plus rien à perdre	100	cf	[pred="n' avoir plus rien à perdre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus rien à perdre_____1	Default		%default
plus sa tête à elle	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à elles	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à eux	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à lui	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à moi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à nous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à soi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à toi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus sa tête à vous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à elle	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à elles	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à eux	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à lui	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à moi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à nous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à soi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à toi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus ta tête à vous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à elle	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à elles	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à eux	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à lui	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à moi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à nous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à soi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à toi	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plus votre tête à vous	100	cf	[pred="n' avoir plus sa tête à lui_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir plus sa tête à lui_____1	Default		%default
plusieurs cordes à leur arc	100	cf	[pred="avoir plusieurs cordes à son arc_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs cordes à son arc_____1	Default		%default
plusieurs cordes à mon arc	100	cf	[pred="avoir plusieurs cordes à son arc_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs cordes à son arc_____1	Default		%default
plusieurs cordes à notre arc	100	cf	[pred="avoir plusieurs cordes à son arc_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs cordes à son arc_____1	Default		%default
plusieurs cordes à son arc	100	cf	[pred="avoir plusieurs cordes à son arc_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs cordes à son arc_____1	Default		%default
plusieurs cordes à ton arc	100	cf	[pred="avoir plusieurs cordes à son arc_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs cordes à son arc_____1	Default		%default
plusieurs cordes à votre arc	100	cf	[pred="avoir plusieurs cordes à son arc_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs cordes à son arc_____1	Default		%default
plusieurs fers au feu	100	cf	[pred="avoir plusieurs fers au feu_____1<Suj:cln|sn>",lightverb=avoir]	avoir plusieurs fers au feu_____1	Default		%default
que la peau et les os	100	cf	[pred="n' avoir que la peau et les os_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir que la peau et les os_____1	Default		%default
que un mot à la bouche	100	cf	[pred="n' avoir que un mot à la bouche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir|mettre]	n' avoir que un mot à la bouche_____1	Default		%default
que ça à faire	100	cf	[pred="n' avoir que ça à faire_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir que ça à faire_____1	Default		%default
que ça à la bouche	100	cf	[pred="n' avoir que ça à la bouche_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir que ça à la bouche_____1	Default		%default
quelque chose contre la toux	100	cf	[pred="avoir quelque chose contre la toux_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose contre la toux_____1	Default		%default
quelque chose dans l'estomac	100	cf	[pred="avoir quelque chose dans l'estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans l'estomac_____1	Default		%default
quelque chose dans la cervelle	100	cf	[pred="avoir quelque chose dans la cervelle_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans la cervelle_____1	Default		%default
quelque chose dans la culotte	100	cf	[pred="avoir quelque chose dans la culotte_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans la culotte_____1	Default		%default
quelque chose dans la tête	100	cf	[pred="avoir quelque chose dans la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans la tête_____1	Default		%default
quelque chose dans le buffet	100	cf	[pred="avoir quelque chose dans le buffet_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le buffet_____1	Default		%default
quelque chose dans le caleçon	100	cf	[pred="avoir quelque chose dans le caleçon_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le caleçon_____1	Default		%default
quelque chose dans le cerveau	100	cf	[pred="avoir quelque chose dans le cerveau_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le cerveau_____1	Default		%default
quelque chose dans le ciboulot	100	cf	[pred="avoir quelque chose dans le ciboulot_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le ciboulot_____1	Default		%default
quelque chose dans le citron	100	cf	[pred="avoir quelque chose dans le citron_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le citron_____1	Default		%default
quelque chose dans le coffre	100	cf	[pred="avoir quelque chose dans le coffre_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le coffre_____1	Default		%default
quelque chose dans le crâne	100	cf	[pred="avoir quelque chose dans le crâne_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le crâne_____1	Default		%default
quelque chose dans le ventre	100	cf	[pred="avoir quelque chose dans le ventre_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose dans le ventre_____1	Default		%default
quelque chose pour la toux	100	cf	[pred="avoir quelque chose pour la toux_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose pour la toux_____1	Default		%default
quelque chose sur le coeur	100	cf	[pred="avoir quelque chose sur le coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose sur le coeur_____1	Default		%default
quelque chose à ajouter	100	cf	[pred="avoir quelque chose à ajouter_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à ajouter_____1	Default		%default
quelque chose à dire	100	cf	[pred="avoir quelque chose à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à dire_____1	Default		%default
quelque chose à déclarer	100	cf	[pred="avoir quelque chose à déclarer_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à déclarer_____1	Default		%default
quelque chose à faire	100	cf	[pred="avoir quelque chose à faire_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à faire_____1	Default		%default
quelque chose à mettre	100	cf	[pred="avoir quelque chose à mettre_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à mettre_____1	Default		%default
quelque chose à se mettre	100	cf	[pred="avoir quelque chose à se mettre_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à se mettre_____1	Default		%default
quelque chose à se mettre sous la dent	100	cf	[pred="avoir quelque chose à se mettre sous la dent_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à se mettre sous la dent_____1	Default		%default
quelque chose à se reprocher	100	cf	[pred="avoir quelque chose à se reprocher_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelque chose à se reprocher_____1	Default		%default
quelques kilos en moins	100	cf	[pred="avoir quelques kilos en moins_____1<Suj:cln|sn>",lightverb=avoir]	avoir quelques kilos en moins_____1	Default		%default
raison cent fois	100	cf	[pred="avoir raison cent fois_____1<Suj:cln|sn>",lightverb=avoir]	avoir raison cent fois_____1	Default		%default
raison mille fois	100	cf	[pred="avoir raison mille fois_____1<Suj:cln|sn>",lightverb=avoir]	avoir raison mille fois_____1	Default		%default
rien dans la cervelle	100	cf	[pred="n' avoir rien dans la cervelle_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans la cervelle_____1	Default		%default
rien dans la tête	100	cf	[pred="n' avoir rien dans la tête_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans la tête_____1	Default		%default
rien dans le cerveau	100	cf	[pred="n' avoir rien dans le cerveau_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans le cerveau_____1	Default		%default
rien dans le ciboulot	100	cf	[pred="n' avoir rien dans le ciboulot_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans le ciboulot_____1	Default		%default
rien dans le citron	100	cf	[pred="n' avoir rien dans le citron_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans le citron_____1	Default		%default
rien dans le coco	100	cf	[pred="n' avoir rien dans le coco_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans le coco_____1	Default		%default
rien dans le crâne	100	cf	[pred="n' avoir rien dans le crâne_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans le crâne_____1	Default		%default
rien dans le ventre	100	cf	[pred="n' avoir rien dans le ventre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien dans le ventre_____1	Default		%default
rien de bien malin	100	cf	[pred="n' avoir rien de bien malin_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien de bien malin_____1	Default		%default
rien de extraordinaire	100	cf	[pred="n' avoir rien de extraordinaire_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien de extraordinaire_____1	Default		%default
rien de mystérieux	100	cf	[pred="n' avoir rien de mystérieux_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien de mystérieux_____1	Default		%default
rien de sorcier	100	cf	[pred="n' avoir rien de sorcier_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien de sorcier_____1	Default		%default
rien pour rien	100	cf	[pred="n' avoir rien pour rien_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien pour rien_____1	Default		%default
rien à cacher	100	cf	[pred="n' avoir rien à cacher_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien à cacher_____1	Default		%default
rien à faire	100	cf	[pred="n' avoir rien à faire_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien à faire_____1	Default		%default
rien à mettre	100	cf	[pred="n' avoir rien à mettre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien à mettre_____1	Default		%default
rien à se mettre	100	cf	[pred="n' avoir rien à se mettre_____1<Suj:cln|sn>",clneg =c +,lightverb=avoir]	n' avoir rien à se mettre_____1	Default		%default
réponse à tout	100	cf	[pred="avoir réponse à tout_____1<Suj:cln|sn>",lightverb=avoir]	avoir réponse à tout_____1	Default		%default
sa langue dans leur poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
sa langue dans ma poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
sa langue dans notre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
sa langue dans sa poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
sa langue dans ta poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
sa langue dans votre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
sa vieille mère à charge	100	cf	[pred="avoir sa vieille mère à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir sa vieille mère à charge_____1	Default		%default
ses vieux parent à charge	100	cf	[pred="avoir ses vieux parent à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir ses vieux parent à charge_____1	Default		%default
six bières pour le prix d'une	100	cf	[pred="avoir six bières pour le prix d'une_____1<Suj:cln|sn>",lightverb=avoir]	avoir six bières pour le prix d'une_____1	Default		%default
six mois à vivre	100	cf	[pred="avoir six mois à vivre_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir six mois à vivre_____1	Default		%default
son mot à dire	100	cf	[pred="avoir son mot à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir son mot à dire_____1	Default		%default
son vieux père à charge	100	cf	[pred="avoir son vieux père à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir son vieux père à charge_____1	Default		%default
suffisamment à faire	100	cf	[pred="avoir suffisamment à faire_____1<Suj:cln|sn>",lightverb=avoir]	avoir suffisamment à faire_____1	Default		%default
ta langue dans leur poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ta langue dans ma poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ta langue dans notre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ta langue dans sa poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ta langue dans ta poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ta langue dans votre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
ta vieille mère à charge	100	cf	[pred="avoir sa vieille mère à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir sa vieille mère à charge_____1	Default		%default
tes vieux parent à charge	100	cf	[pred="avoir ses vieux parent à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir ses vieux parent à charge_____1	Default		%default
ton mot à dire	100	cf	[pred="avoir son mot à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir son mot à dire_____1	Default		%default
ton vieux père à charge	100	cf	[pred="avoir son vieux père à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir son vieux père à charge_____1	Default		%default
tous les atouts en main	100	cf	[pred="avoir tous les atouts en main_____1<Suj:cln|sn>",lightverb=avoir]	avoir tous les atouts en main_____1	Default		%default
tout pour elle	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour elles	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour eux	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour lui	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour moi	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour nous	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour plaire	100	cf	[pred="avoir tout pour plaire_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour plaire_____1	Default		%default
tout pour soi	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour toi	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
tout pour vous	100	cf	[pred="avoir tout pour lui_____1<Suj:cln|sn>",lightverb=avoir]	avoir tout pour lui_____1	Default		%default
trop à faire	100	cf	[pred="avoir trop à faire_____1<Suj:cln|sn>",lightverb=avoir]	avoir trop à faire_____1	Default		%default
un _NUM enfant en route	100	cf	[pred="avoir un _NUM enfant en route_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un _NUM enfant en route_____1	Default		%default
un an de plus que la année dernière	100	cf	[pred="avoir un an de plus que la année dernière_____1<Suj:cln|sn>",lightverb=avoir]	avoir un an de plus que la année dernière_____1	Default		%default
un atout dans leur jeu	100	cf	[pred="avoir un atout dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans son jeu_____1	Default		%default
un atout dans mon jeu	100	cf	[pred="avoir un atout dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans son jeu_____1	Default		%default
un atout dans notre jeu	100	cf	[pred="avoir un atout dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans son jeu_____1	Default		%default
un atout dans sa manche	100	cf	[pred="avoir un atout dans sa manche_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans sa manche_____1	Default		%default
un atout dans son jeu	100	cf	[pred="avoir un atout dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans son jeu_____1	Default		%default
un atout dans ton jeu	100	cf	[pred="avoir un atout dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans son jeu_____1	Default		%default
un atout dans votre jeu	100	cf	[pred="avoir un atout dans son jeu_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un atout dans son jeu_____1	Default		%default
un bandeau sur les yeux	100	cf	[pred="avoir un bandeau sur les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir un bandeau sur les yeux_____1	Default		%default
un bobo à l'oreille	100	cf	[pred="avoir un bobo à l'oreille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un bobo à l'oreille_____1	Default		%default
un boeuf sur la langue	100	cf	[pred="avoir un boeuf sur la langue_____1<Suj:cln|sn>",lightverb=avoir]	avoir un boeuf sur la langue_____1	Default		%default
un boulet au pied	100	cf	[pred="avoir un boulet au pied_____1<Suj:cln|sn>",lightverb=avoir]	avoir un boulet au pied_____1	Default		%default
un but dans la vie	100	cf	[pred="avoir un but dans la vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un but dans la vie_____1	Default		%default
un casque en plomb	100	cf	[pred="avoir un casque en plomb_____1<Suj:cln|sn>",lightverb=avoir]	avoir un casque en plomb_____1	Default		%default
un certain rapport à l'argent	100	cf	[pred="avoir un certain rapport à l'argent_____1<Suj:cln|sn>",lightverb=avoir]	avoir un certain rapport à l'argent_____1	Default		%default
un cerveau comme une passoire	100	cf	[pred="avoir un cerveau comme une passoire_____1<Suj:cln|sn>",lightverb=avoir]	avoir un cerveau comme une passoire_____1	Default		%default
un chat dans la gorge	100	cf	[pred="avoir un chat dans la gorge_____1<Suj:cln|sn>",lightverb=avoir]	avoir un chat dans la gorge_____1	Default		%default
un cheveu sur la langue	100	cf	[pred="avoir un cheveu sur la langue_____1<Suj:cln|sn>",lightverb=avoir]	avoir un cheveu sur la langue_____1	Default		%default
un cigare à la bouche	100	cf	[pred="avoir un cigare à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir un cigare à la bouche_____1	Default		%default
un clope aux lèvres	100	cf	[pred="avoir un clope aux lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir un clope aux lèvres_____1	Default		%default
un clope à la bouche	100	cf	[pred="avoir un clope à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir un clope à la bouche_____1	Default		%default
un coeur d'or derrière une façade arrogante	100	cf	[pred="avoir un coeur d'or derrière une façade arrogante_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coeur d'or derrière une façade arrogante_____1	Default		%default
un coeur d'or sous des dehors hautains	100	cf	[pred="avoir un coeur d'or sous des dehors hautains_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coeur d'or sous des dehors hautains_____1	Default		%default
un contrat en poche	100	cf	[pred="avoir un contrat en poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir un contrat en poche_____1	Default		%default
un cor au pied	100	cf	[pred="avoir un cor au pied_____1<Suj:cln|sn>",lightverb=avoir]	avoir un cor au pied_____1	Default		%default
un coup au coeur	100	cf	[pred="avoir un coup au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coup au coeur_____1	Default		%default
un coup dans l'aile	100	cf	[pred="avoir un coup dans l'aile_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coup dans l'aile_____1	Default		%default
un coup dans le nez	100	cf	[pred="avoir un coup dans le nez_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coup dans le nez_____1	Default		%default
un coup dans les carreaux	100	cf	[pred="avoir un coup dans les carreaux_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coup dans les carreaux_____1	Default		%default
un coup sur la tête	100	cf	[pred="avoir un coup sur la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir un coup sur la tête_____1	Default		%default
un creux au estomac	100	cf	[pred="avoir un creux au estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir un creux au estomac_____1	Default		%default
un créneau dans leur emploi du temps	100	cf	[pred="avoir un créneau dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un créneau dans son emploi du temps_____1	Default		%default
un créneau dans mon emploi du temps	100	cf	[pred="avoir un créneau dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un créneau dans son emploi du temps_____1	Default		%default
un créneau dans notre emploi du temps	100	cf	[pred="avoir un créneau dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un créneau dans son emploi du temps_____1	Default		%default
un créneau dans son emploi du temps	100	cf	[pred="avoir un créneau dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un créneau dans son emploi du temps_____1	Default		%default
un créneau dans ton emploi du temps	100	cf	[pred="avoir un créneau dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un créneau dans son emploi du temps_____1	Default		%default
un créneau dans votre emploi du temps	100	cf	[pred="avoir un créneau dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un créneau dans son emploi du temps_____1	Default		%default
un découvert en banque	100	cf	[pred="avoir un découvert en banque_____1<Suj:cln|sn>",lightverb=avoir]	avoir un découvert en banque_____1	Default		%default
un découvert à la banque	100	cf	[pred="avoir un découvert à la banque_____1<Suj:cln|sn>",lightverb=avoir]	avoir un découvert à la banque_____1	Default		%default
un décès dans leur famille	100	cf	[pred="avoir un décès dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un décès dans sa famille_____1	Default		%default
un décès dans ma famille	100	cf	[pred="avoir un décès dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un décès dans sa famille_____1	Default		%default
un décès dans notre famille	100	cf	[pred="avoir un décès dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un décès dans sa famille_____1	Default		%default
un décès dans sa famille	100	cf	[pred="avoir un décès dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un décès dans sa famille_____1	Default		%default
un décès dans ta famille	100	cf	[pred="avoir un décès dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un décès dans sa famille_____1	Default		%default
un décès dans votre famille	100	cf	[pred="avoir un décès dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un décès dans sa famille_____1	Default		%default
un fil à la patte	100	cf	[pred="avoir un fil à la patte_____1<Suj:cln|sn>",lightverb=avoir]	avoir un fil à la patte_____1	Default		%default
un genou en terre	100	cf	[pred="avoir un genou en terre_____1<Suj:cln|sn>",lightverb=avoir]	avoir un genou en terre_____1	Default		%default
un goût amer dans la bouche	100	cf	[pred="avoir un goût amer dans la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir un goût amer dans la bouche_____1	Default		%default
un homme dans leur vie	100	cf	[pred="avoir un homme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un homme dans sa vie_____1	Default		%default
un homme dans ma vie	100	cf	[pred="avoir un homme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un homme dans sa vie_____1	Default		%default
un homme dans notre vie	100	cf	[pred="avoir un homme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un homme dans sa vie_____1	Default		%default
un homme dans sa vie	100	cf	[pred="avoir un homme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un homme dans sa vie_____1	Default		%default
un homme dans ta vie	100	cf	[pred="avoir un homme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un homme dans sa vie_____1	Default		%default
un homme dans votre vie	100	cf	[pred="avoir un homme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un homme dans sa vie_____1	Default		%default
un mari à charge	100	cf	[pred="avoir un mari à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un mari à charge_____1	Default		%default
un mort dans leur famille	100	cf	[pred="avoir un mort dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mort dans sa famille_____1	Default		%default
un mort dans ma famille	100	cf	[pred="avoir un mort dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mort dans sa famille_____1	Default		%default
un mort dans notre famille	100	cf	[pred="avoir un mort dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mort dans sa famille_____1	Default		%default
un mort dans sa famille	100	cf	[pred="avoir un mort dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mort dans sa famille_____1	Default		%default
un mort dans ta famille	100	cf	[pred="avoir un mort dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mort dans sa famille_____1	Default		%default
un mort dans votre famille	100	cf	[pred="avoir un mort dans sa famille_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mort dans sa famille_____1	Default		%default
un mégot aux lèvres	100	cf	[pred="avoir un mégot aux lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mégot aux lèvres_____1	Default		%default
un mégot à la bouche	100	cf	[pred="avoir un mégot à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir un mégot à la bouche_____1	Default		%default
un noeud dans la gorge	100	cf	[pred="avoir un noeud dans la gorge_____1<Suj:cln|sn>",lightverb=avoir]	avoir un noeud dans la gorge_____1	Default		%default
un nom à particule	100	cf	[pred="avoir un nom à particule_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un nom à particule_____1	Default		%default
un nom à rallonge	100	cf	[pred="avoir un nom à rallonge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un nom à rallonge_____1	Default		%default
un oeil qui joue au billard et l'autre qui marque les points	100	cf	[pred="avoir un oeil qui joue au billard et l'autre qui marque les points_____1<Suj:cln|sn>",lightverb=avoir]	avoir un oeil qui joue au billard et l'autre qui marque les points_____1	Default		%default
un os à ronger	100	cf	[pred="avoir un os à ronger_____1<Suj:cln|sn>",lightverb=avoir]	avoir un os à ronger_____1	Default		%default
un passage à vide	100	cf	[pred="avoir un passage à vide_____1<Suj:cln|sn>",lightverb=avoir]	avoir un passage à vide_____1	Default		%default
un pavé sur l'estomac	100	cf	[pred="avoir un pavé sur l'estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir un pavé sur l'estomac_____1	Default		%default
un pied dans la fosse	100	cf	[pred="avoir un pied dans la fosse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un pied dans la fosse_____1	Default		%default
un pied dans la place	100	cf	[pred="avoir un pied dans la place_____1<Suj:cln|sn>",lightverb=avoir]	avoir un pied dans la place_____1	Default		%default
un pied dans la tombe	100	cf	[pred="avoir un pied dans la tombe_____1<Suj:cln|sn>",lightverb=avoir]	avoir un pied dans la tombe_____1	Default		%default
un pincement au coeur	100	cf	[pred="avoir un pincement au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un pincement au coeur_____1	Default		%default
un poids de moins sur la conscience	100	cf	[pred="avoir un poids de moins sur la conscience_____1<Suj:cln|sn>",lightverb=avoir]	avoir un poids de moins sur la conscience_____1	Default		%default
un poids de moins sur le coeur	100	cf	[pred="avoir un poids de moins sur le coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un poids de moins sur le coeur_____1	Default		%default
un poids sur l'estomac	100	cf	[pred="avoir un poids sur l'estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir un poids sur l'estomac_____1	Default		%default
un poil dans la main	100	cf	[pred="avoir un poil dans la main_____1<Suj:cln|sn>",lightverb=avoir]	avoir un poil dans la main_____1	Default		%default
un point dans le dos	100	cf	[pred="avoir un point dans le dos_____1<Suj:cln|sn>",lightverb=avoir]	avoir un point dans le dos_____1	Default		%default
un polichinelle dans le tiroir	100	cf	[pred="avoir un polichinelle dans le tiroir_____1<Suj:cln|sn>",lightverb=avoir]	avoir un polichinelle dans le tiroir_____1	Default		%default
un regard en coin	100	cf	[pred="avoir un regard en coin_____1<Suj:cln|sn>",lightverb=avoir]	avoir un regard en coin_____1	Default		%default
un regard en coulisse	100	cf	[pred="avoir un regard en coulisse_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un regard en coulisse_____1	Default		%default
un souci de moins à se faire	100	cf	[pred="avoir un souci de moins à se faire_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un souci de moins à se faire_____1	Default		%default
un souci en tête	100	cf	[pred="avoir un souci en tête_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un souci en tête_____1	Default		%default
un souffle au coeur	100	cf	[pred="avoir un souffle au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un souffle au coeur_____1	Default		%default
un sourire aux lèvres	100	cf	[pred="avoir un sourire aux lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir un sourire aux lèvres_____1	Default		%default
un sourire en coin	100	cf	[pred="avoir un sourire en coin_____1<Suj:cln|sn>",lightverb=avoir]	avoir un sourire en coin_____1	Default		%default
un sourire idiot aux lèvres	100	cf	[pred="avoir un sourire idiot aux lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir un sourire idiot aux lèvres_____1	Default		%default
un sérieux coup dans l'aile	100	cf	[pred="avoir un sérieux coup dans l'aile_____1<Suj:cln|sn>",lightverb=avoir]	avoir un sérieux coup dans l'aile_____1	Default		%default
un sérieux coup dans le nez	100	cf	[pred="avoir un sérieux coup dans le nez_____1<Suj:cln|sn>",lightverb=avoir]	avoir un sérieux coup dans le nez_____1	Default		%default
un tigre dans leur moteur	100	cf	[pred="avoir un tigre dans son moteur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tigre dans son moteur_____1	Default		%default
un tigre dans mon moteur	100	cf	[pred="avoir un tigre dans son moteur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tigre dans son moteur_____1	Default		%default
un tigre dans notre moteur	100	cf	[pred="avoir un tigre dans son moteur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tigre dans son moteur_____1	Default		%default
un tigre dans son moteur	100	cf	[pred="avoir un tigre dans son moteur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tigre dans son moteur_____1	Default		%default
un tigre dans ton moteur	100	cf	[pred="avoir un tigre dans son moteur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tigre dans son moteur_____1	Default		%default
un tigre dans votre moteur	100	cf	[pred="avoir un tigre dans son moteur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tigre dans son moteur_____1	Default		%default
un tournant dans leur vie	100	cf	[pred="avoir un tournant dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tournant dans sa vie_____1	Default		%default
un tournant dans ma vie	100	cf	[pred="avoir un tournant dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tournant dans sa vie_____1	Default		%default
un tournant dans notre vie	100	cf	[pred="avoir un tournant dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tournant dans sa vie_____1	Default		%default
un tournant dans sa vie	100	cf	[pred="avoir un tournant dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tournant dans sa vie_____1	Default		%default
un tournant dans ta vie	100	cf	[pred="avoir un tournant dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tournant dans sa vie_____1	Default		%default
un tournant dans votre vie	100	cf	[pred="avoir un tournant dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir un tournant dans sa vie_____1	Default		%default
un train de retard	100	cf	[pred="avoir un train de retard_____1<Suj:cln|sn>",lightverb=avoir]	avoir un train de retard_____1	Default		%default
un transport au cerveau	100	cf	[pred="avoir un transport au cerveau_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un transport au cerveau_____1	Default		%default
un trou dans le talon	100	cf	[pred="avoir un trou dans le talon_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou dans le talon_____1	Default		%default
un trou dans leur emploi du temps	100	cf	[pred="avoir un trou dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un trou dans son emploi du temps_____1	Default		%default
un trou dans mon emploi du temps	100	cf	[pred="avoir un trou dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un trou dans son emploi du temps_____1	Default		%default
un trou dans notre emploi du temps	100	cf	[pred="avoir un trou dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un trou dans son emploi du temps_____1	Default		%default
un trou dans son emploi du temps	100	cf	[pred="avoir un trou dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un trou dans son emploi du temps_____1	Default		%default
un trou dans ton emploi du temps	100	cf	[pred="avoir un trou dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un trou dans son emploi du temps_____1	Default		%default
un trou dans votre emploi du temps	100	cf	[pred="avoir un trou dans son emploi du temps_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un trou dans son emploi du temps_____1	Default		%default
un trou de _MONTANT dans leur caisse	100	cf	[pred="avoir un trou de _MONTANT dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa caisse_____1	Default		%default
un trou de _MONTANT dans leur comptabilité	100	cf	[pred="avoir un trou de _MONTANT dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa comptabilité_____1	Default		%default
un trou de _MONTANT dans leurs comptes	100	cf	[pred="avoir un trou de _MONTANT dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans ses comptes_____1	Default		%default
un trou de _MONTANT dans ma caisse	100	cf	[pred="avoir un trou de _MONTANT dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa caisse_____1	Default		%default
un trou de _MONTANT dans ma comptabilité	100	cf	[pred="avoir un trou de _MONTANT dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa comptabilité_____1	Default		%default
un trou de _MONTANT dans mes comptes	100	cf	[pred="avoir un trou de _MONTANT dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans ses comptes_____1	Default		%default
un trou de _MONTANT dans nos comptes	100	cf	[pred="avoir un trou de _MONTANT dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans ses comptes_____1	Default		%default
un trou de _MONTANT dans notre caisse	100	cf	[pred="avoir un trou de _MONTANT dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa caisse_____1	Default		%default
un trou de _MONTANT dans notre comptabilité	100	cf	[pred="avoir un trou de _MONTANT dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa comptabilité_____1	Default		%default
un trou de _MONTANT dans sa caisse	100	cf	[pred="avoir un trou de _MONTANT dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa caisse_____1	Default		%default
un trou de _MONTANT dans sa comptabilité	100	cf	[pred="avoir un trou de _MONTANT dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa comptabilité_____1	Default		%default
un trou de _MONTANT dans ses comptes	100	cf	[pred="avoir un trou de _MONTANT dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans ses comptes_____1	Default		%default
un trou de _MONTANT dans ta caisse	100	cf	[pred="avoir un trou de _MONTANT dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa caisse_____1	Default		%default
un trou de _MONTANT dans ta comptabilité	100	cf	[pred="avoir un trou de _MONTANT dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa comptabilité_____1	Default		%default
un trou de _MONTANT dans tes comptes	100	cf	[pred="avoir un trou de _MONTANT dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans ses comptes_____1	Default		%default
un trou de _MONTANT dans vos comptes	100	cf	[pred="avoir un trou de _MONTANT dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans ses comptes_____1	Default		%default
un trou de _MONTANT dans votre caisse	100	cf	[pred="avoir un trou de _MONTANT dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa caisse_____1	Default		%default
un trou de _MONTANT dans votre comptabilité	100	cf	[pred="avoir un trou de _MONTANT dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de _MONTANT dans sa comptabilité_____1	Default		%default
un trou de yens dans leur caisse	100	cf	[pred="avoir un trou de yens dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa caisse_____1	Default		%default
un trou de yens dans leur comptabilité	100	cf	[pred="avoir un trou de yens dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa comptabilité_____1	Default		%default
un trou de yens dans leurs comptes	100	cf	[pred="avoir un trou de yens dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans ses comptes_____1	Default		%default
un trou de yens dans ma caisse	100	cf	[pred="avoir un trou de yens dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa caisse_____1	Default		%default
un trou de yens dans ma comptabilité	100	cf	[pred="avoir un trou de yens dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa comptabilité_____1	Default		%default
un trou de yens dans mes comptes	100	cf	[pred="avoir un trou de yens dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans ses comptes_____1	Default		%default
un trou de yens dans nos comptes	100	cf	[pred="avoir un trou de yens dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans ses comptes_____1	Default		%default
un trou de yens dans notre caisse	100	cf	[pred="avoir un trou de yens dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa caisse_____1	Default		%default
un trou de yens dans notre comptabilité	100	cf	[pred="avoir un trou de yens dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa comptabilité_____1	Default		%default
un trou de yens dans sa caisse	100	cf	[pred="avoir un trou de yens dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa caisse_____1	Default		%default
un trou de yens dans sa comptabilité	100	cf	[pred="avoir un trou de yens dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa comptabilité_____1	Default		%default
un trou de yens dans ses comptes	100	cf	[pred="avoir un trou de yens dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans ses comptes_____1	Default		%default
un trou de yens dans ta caisse	100	cf	[pred="avoir un trou de yens dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa caisse_____1	Default		%default
un trou de yens dans ta comptabilité	100	cf	[pred="avoir un trou de yens dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa comptabilité_____1	Default		%default
un trou de yens dans tes comptes	100	cf	[pred="avoir un trou de yens dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans ses comptes_____1	Default		%default
un trou de yens dans vos comptes	100	cf	[pred="avoir un trou de yens dans ses comptes_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans ses comptes_____1	Default		%default
un trou de yens dans votre caisse	100	cf	[pred="avoir un trou de yens dans sa caisse_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa caisse_____1	Default		%default
un trou de yens dans votre comptabilité	100	cf	[pred="avoir un trou de yens dans sa comptabilité_____1<Suj:cln|sn>",lightverb=avoir]	avoir un trou de yens dans sa comptabilité_____1	Default		%default
un verre dans le nez	100	cf	[pred="avoir un verre dans le nez_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir un verre dans le nez_____1	Default		%default
un vide au coeur	100	cf	[pred="avoir un vide au coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir un vide au coeur_____1	Default		%default
un voile devant les yeux	100	cf	[pred="avoir un voile devant les yeux_____1<Suj:cln|sn>",lightverb=avoir]	avoir un voile devant les yeux_____1	Default		%default
une ampoule au pied	100	cf	[pred="avoir une ampoule au pied_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une ampoule au pied_____1	Default		%default
une ampoule au talon	100	cf	[pred="avoir une ampoule au talon_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une ampoule au talon_____1	Default		%default
une ampoule à la main	100	cf	[pred="avoir une ampoule à la main_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une ampoule à la main_____1	Default		%default
une araignée dans le plafond	100	cf	[pred="avoir une araignée dans le plafond_____1<Suj:cln|sn>",lightverb=avoir]	avoir une araignée dans le plafond_____1	Default		%default
une barre au creux de l'estomac	100	cf	[pred="avoir une barre au creux de l'estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir une barre au creux de l'estomac_____1	Default		%default
une barre dans la tête	100	cf	[pred="avoir une barre dans la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir une barre dans la tête_____1	Default		%default
une boule au creux de l'estomac	100	cf	[pred="avoir une boule au creux de l'estomac_____1<Suj:cln|sn>",lightverb=avoir]	avoir une boule au creux de l'estomac_____1	Default		%default
une boule dans la gorge	100	cf	[pred="avoir une boule dans la gorge_____1<Suj:cln|sn>",lightverb=avoir]	avoir une boule dans la gorge_____1	Default		%default
une carte à jouer	100	cf	[pred="avoir une carte à jouer_____1<Suj:cln|sn>",lightverb=avoir]	avoir une carte à jouer_____1	Default		%default
une case en moins	100	cf	[pred="avoir une case en moins_____1<Suj:cln|sn>",lightverb=avoir]	avoir une case en moins_____1	Default		%default
une cervelle comme une passoire	100	cf	[pred="avoir une cervelle comme une passoire_____1<Suj:cln|sn>",lightverb=avoir]	avoir une cervelle comme une passoire_____1	Default		%default
une cigarette aux lèvres	100	cf	[pred="avoir une cigarette aux lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir une cigarette aux lèvres_____1	Default		%default
une cigarette à la bouche	100	cf	[pred="avoir une cigarette à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir une cigarette à la bouche_____1	Default		%default
une clope aux lèvres	100	cf	[pred="avoir une clope aux lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir une clope aux lèvres_____1	Default		%default
une clope à la bouche	100	cf	[pred="avoir une clope à la bouche_____1<Suj:cln|sn>",lightverb=avoir]	avoir une clope à la bouche_____1	Default		%default
une cochonnerie dans l'oeil	100	cf	[pred="avoir une cochonnerie dans l'oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir une cochonnerie dans l'oeil_____1	Default		%default
une coquetterie dans l'oeil	100	cf	[pred="avoir une coquetterie dans l'oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir une coquetterie dans l'oeil_____1	Default		%default
une décoration à leur boutonnière	100	cf	[pred="avoir une décoration à sa boutonnière_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une décoration à sa boutonnière_____1	Default		%default
une décoration à ma boutonnière	100	cf	[pred="avoir une décoration à sa boutonnière_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une décoration à sa boutonnière_____1	Default		%default
une décoration à notre boutonnière	100	cf	[pred="avoir une décoration à sa boutonnière_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une décoration à sa boutonnière_____1	Default		%default
une décoration à sa boutonnière	100	cf	[pred="avoir une décoration à sa boutonnière_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une décoration à sa boutonnière_____1	Default		%default
une décoration à ta boutonnière	100	cf	[pred="avoir une décoration à sa boutonnière_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une décoration à sa boutonnière_____1	Default		%default
une décoration à votre boutonnière	100	cf	[pred="avoir une décoration à sa boutonnière_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une décoration à sa boutonnière_____1	Default		%default
une famille à charge	100	cf	[pred="avoir une famille à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une famille à charge_____1	Default		%default
une femme dans chaque port	100	cf	[pred="avoir une femme dans chaque port_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans chaque port_____1	Default		%default
une femme dans leur vie	100	cf	[pred="avoir une femme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans sa vie_____1	Default		%default
une femme dans ma vie	100	cf	[pred="avoir une femme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans sa vie_____1	Default		%default
une femme dans notre vie	100	cf	[pred="avoir une femme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans sa vie_____1	Default		%default
une femme dans sa vie	100	cf	[pred="avoir une femme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans sa vie_____1	Default		%default
une femme dans ta vie	100	cf	[pred="avoir une femme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans sa vie_____1	Default		%default
une femme dans votre vie	100	cf	[pred="avoir une femme dans sa vie_____1<Suj:cln|sn>",lightverb=avoir]	avoir une femme dans sa vie_____1	Default		%default
une femme à charge	100	cf	[pred="avoir une femme à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une femme à charge_____1	Default		%default
une flamme dans le regard	100	cf	[pred="avoir une flamme dans le regard_____1<Suj:cln|sn>",lightverb=avoir]	avoir une flamme dans le regard_____1	Default		%default
une fleur à la boutonnière	100	cf	[pred="avoir une fleur à la boutonnière_____1<Suj:cln|sn>",lightverb=avoir]	avoir une fleur à la boutonnière_____1	Default		%default
une guerre de retard	100	cf	[pred="avoir une guerre de retard_____1<Suj:cln|sn>",lightverb=avoir]	avoir une guerre de retard_____1	Default		%default
une idée derrière la tête	100	cf	[pred="avoir une idée derrière la tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir une idée derrière la tête_____1	Default		%default
une idée en tête	100	cf	[pred="avoir une idée en tête_____1<Suj:cln|sn>",lightverb=avoir]	avoir une idée en tête_____1	Default		%default
une idée sur la question	100	cf	[pred="avoir une idée sur la question_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une idée sur la question_____1	Default		%default
une main de fer dans un gant de velours	100	cf	[pred="avoir une main de fer dans un gant de velours_____1<Suj:cln|sn>",lightverb=avoir]	avoir une main de fer dans un gant de velours_____1	Default		%default
une moquette sur la langue	100	cf	[pred="avoir une moquette sur la langue_____1<Suj:cln|sn>",lightverb=avoir]	avoir une moquette sur la langue_____1	Default		%default
une médaille d'argent aux J.O.	100	cf	[pred="avoir une médaille d'argent aux J.O._____1<Suj:cln|sn>",lightverb=avoir]	avoir une médaille d'argent aux J.O._____1	Default		%default
une médaille d'or aux J.O.	100	cf	[pred="avoir une médaille d'or aux J.O._____1<Suj:cln|sn>",lightverb=avoir]	avoir une médaille d'or aux J.O._____1	Default		%default
une médaille de bronze aux J.O.	100	cf	[pred="avoir une médaille de bronze aux J.O._____1<Suj:cln|sn>",lightverb=avoir]	avoir une médaille de bronze aux J.O._____1	Default		%default
une participation croisée dans une entreprise	100	cf	[pred="avoir une participation croisée dans une entreprise_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une participation croisée dans une entreprise_____1	Default		%default
une pierre dans leur jardin	100	cf	[pred="avoir une pierre dans son jardin_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre dans son jardin_____1	Default		%default
une pierre dans mon jardin	100	cf	[pred="avoir une pierre dans son jardin_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre dans son jardin_____1	Default		%default
une pierre dans notre jardin	100	cf	[pred="avoir une pierre dans son jardin_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre dans son jardin_____1	Default		%default
une pierre dans son jardin	100	cf	[pred="avoir une pierre dans son jardin_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre dans son jardin_____1	Default		%default
une pierre dans ton jardin	100	cf	[pred="avoir une pierre dans son jardin_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre dans son jardin_____1	Default		%default
une pierre dans votre jardin	100	cf	[pred="avoir une pierre dans son jardin_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre dans son jardin_____1	Default		%default
une pierre à la place du coeur	100	cf	[pred="avoir une pierre à la place du coeur_____1<Suj:cln|sn>",lightverb=avoir]	avoir une pierre à la place du coeur_____1	Default		%default
une place au soleil	100	cf	[pred="avoir une place au soleil_____1<Suj:cln|sn>",lightverb=avoir]	avoir une place au soleil_____1	Default		%default
une poire pour la soif	100	cf	[pred="avoir une poire pour la soif_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir une poire pour la soif_____1	Default		%default
une poussière dans l'oeil	100	cf	[pred="avoir une poussière dans l'oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir une poussière dans l'oeil_____1	Default		%default
une saleté dans l'oeil	100	cf	[pred="avoir une saleté dans l'oeil_____1<Suj:cln|sn>",lightverb=avoir]	avoir une saleté dans l'oeil_____1	Default		%default
une tête à caler les roues de corbillard	100	cf	[pred="avoir une tête à caler les roues de corbillard_____1<Suj:cln|sn>",lightverb=avoir]	avoir une tête à caler les roues de corbillard_____1	Default		%default
une tête à claques	100	cf	[pred="avoir une tête à claques_____1<Suj:cln|sn>",lightverb=avoir]	avoir une tête à claques_____1	Default		%default
une tête à gifles	100	cf	[pred="avoir une tête à gifles_____1<Suj:cln|sn>",lightverb=avoir]	avoir une tête à gifles_____1	Default		%default
une éponge dans le gosier	100	cf	[pred="avoir une éponge dans le gosier_____1<Suj:cln|sn>",lightverb=avoir]	avoir une éponge dans le gosier_____1	Default		%default
une épée dans les reins	100	cf	[pred="avoir une épée dans les reins_____1<Suj:cln|sn>",lightverb=avoir]	avoir une épée dans les reins_____1	Default		%default
vingt sur vingt	100	cf	[pred="avoir vingt sur vingt_____1<Suj:cln|sn>",lightverb=avoir]	avoir vingt sur vingt_____1	Default		%default
voix au chapitre	100	cf	[pred="avoir voix au chapitre_____1<Suj:cln|sn>",lightverb=avoir]	avoir voix au chapitre_____1	Default		%default
vos vieux parent à charge	100	cf	[pred="avoir ses vieux parent à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir ses vieux parent à charge_____1	Default		%default
votre langue dans leur poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
votre langue dans ma poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
votre langue dans notre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
votre langue dans sa poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
votre langue dans ta poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
votre langue dans votre poche	100	cf	[pred="avoir sa langue dans sa poche_____1<Suj:cln|sn>",lightverb=avoir]	avoir sa langue dans sa poche_____1	Default		%default
votre mot à dire	100	cf	[pred="avoir son mot à dire_____1<Suj:cln|sn>",lightverb=avoir]	avoir son mot à dire_____1	Default		%default
votre vieille mère à charge	100	cf	[pred="avoir sa vieille mère à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir sa vieille mère à charge_____1	Default		%default
votre vieux père à charge	100	cf	[pred="avoir son vieux père à charge_____1<Suj:cln|sn>",lightverb=avoir|mettre]	avoir son vieux père à charge_____1	Default		%default
vu assez	100	cf	[pred="en avoir vu assez_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir vu assez_____1	Default		%default
vu le loup	100	cf	[pred="avoir vu le loup_____1<Suj:cln|sn>",lightverb=avoir]	avoir vu le loup_____1	Default		%default
vu trop	100	cf	[pred="en avoir vu trop_____1<Suj:cln|sn>",@pseudo-en,lightverb=avoir]	en avoir vu trop_____1	Default		%default
