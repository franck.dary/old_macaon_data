#!/usr/bin/python
# -*- coding: utf-8 -*-

# ./MID.py ../../../data/wals/matrix_valueReplace_deleteLangUseless listFamily.txt

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import collections
import random
import numpy as np
from scipy import stats

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 3):
        sys.stderr("Error, give 2 files with the list of values to compare (in column, one per line, same length)")
        exit(1)
    filePath1 = sys.argv[1]
    filePath2 = sys.argv[2]
    
    vect1 = []
    vect2 = []
    
    
    with open(filePath1, "r") as file1:
        curLine = file1.readline().strip()
        while(not fileCompletelyRead(curLine)):
            vect1.append(float(curLine))
            curLine = file1.readline().strip()
    with open(filePath2, "r") as file2:
        curLine = file2.readline().strip()
        while(not fileCompletelyRead(curLine)):
            vect2.append(float(curLine))
            curLine = file2.readline().strip()    
    if(len(vect1) != len(vect2)):
        sys.stderr("Error length (number of values in files must be the same)")
        exit(1)
        
    vect1 = np.array(vect1)
    vect2 = np.array(vect2)
    
    print("Pearson :",stats.pearsonr(vect1,vect2))
    print(stats.spearmanr(vect1,vect2)) 
    print(stats.kendalltau(vect1,vect2))    

