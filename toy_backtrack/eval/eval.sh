#! /bin/bash

LANG=toy_backtrack
MCF=../data/test.mcf
MCD=../data/wpmlgfs.mcd
ARGS="--ignore FORM --ignore POS"

exec ../../scripts/eval.py $LANG $MCF $MCD $* $ARGS
