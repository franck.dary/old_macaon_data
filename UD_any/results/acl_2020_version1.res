Corpus        Metric      F1.score   Model                         
--------------------------------------------------------------------------------
Chinese-GSD   LAS         37.71    tokeparser                    
Chinese-GSD   LAS         38.35    tokeparser_incremental        
Chinese-GSD   LAS         61.51    tagparser                     
Chinese-GSD   LAS         62.81    tagparser_sequential          
Chinese-GSD   LAS         64.19    tagparser_sequential_strong   

Chinese-GSD   Lemmas      100.00   tagparser                     
Chinese-GSD   Lemmas      100.00   tagparser_sequential          
Chinese-GSD   Lemmas      100.00   tagparser_sequential_strong   
Chinese-GSD   Lemmas      87.42    tokeparser                    
Chinese-GSD   Lemmas      87.56    tokeparser_incremental        

Chinese-GSD   Sentences   97.19    tagparser_sequential_strong   
Chinese-GSD   Sentences   97.48    tagparser                     
Chinese-GSD   Sentences   98.80    tokeparser                    
Chinese-GSD   Sentences   99.10    tagparser_sequential          
Chinese-GSD   Sentences   99.10    tokeparser_incremental        

Chinese-GSD   Tokens      100.00   tagparser                     
Chinese-GSD   Tokens      100.00   tagparser_sequential          
Chinese-GSD   Tokens      100.00   tagparser_sequential_strong   
Chinese-GSD   Tokens      87.42    tokeparser                    
Chinese-GSD   Tokens      87.56    tokeparser_incremental        

Chinese-GSD   UAS         42.28    tokeparser                    
Chinese-GSD   UAS         43.10    tokeparser_incremental        
Chinese-GSD   UAS         67.24    tagparser                     
Chinese-GSD   UAS         68.78    tagparser_sequential          
Chinese-GSD   UAS         69.72    tagparser_sequential_strong   

Chinese-GSD   UFeats      85.94    tokeparser_incremental        
Chinese-GSD   UFeats      85.98    tokeparser                    
Chinese-GSD   UFeats      98.66    tagparser                     
Chinese-GSD   UFeats      98.66    tagparser_sequential          
Chinese-GSD   UFeats      98.68    tagparser_sequential_strong   

Chinese-GSD   UPOS        80.32    tokeparser                    
Chinese-GSD   UPOS        80.55    tokeparser_incremental        
Chinese-GSD   UPOS        92.38    tagparser_sequential          
Chinese-GSD   UPOS        92.50    tagparser_sequential_strong   
Chinese-GSD   UPOS        92.52    tagparser                     
--------------------------------------------------------------------------------
English-EWT   LAS         73.53    tagparser                     
English-EWT   LAS         74.44    tagparser_sequential_strong   
English-EWT   LAS         75.11    tagparser_sequential          

English-EWT   Lemmas      95.28    tagparser                     
English-EWT   Lemmas      95.39    tagparser_sequential          
English-EWT   Lemmas      95.44    tagparser_sequential_strong   

English-EWT   Sentences   70.80    tagparser_sequential_strong   
English-EWT   Sentences   72.43    tagparser_sequential          
English-EWT   Sentences   73.60    tagparser                     

English-EWT   Tokens      100.00   tagparser                     
English-EWT   Tokens      100.00   tagparser_sequential          
English-EWT   Tokens      100.00   tagparser_sequential_strong   

English-EWT   UAS         78.17    tagparser                     
English-EWT   UAS         79.08    tagparser_sequential_strong   
English-EWT   UAS         79.38    tagparser_sequential          

English-EWT   UFeats      94.92    tagparser_sequential          
English-EWT   UFeats      94.94    tagparser                     
English-EWT   UFeats      95.09    tagparser_sequential_strong   

English-EWT   UPOS        93.66    tagparser_sequential          
English-EWT   UPOS        93.71    tagparser_sequential_strong   
English-EWT   UPOS        93.79    tagparser                     
--------------------------------------------------------------------------------
French-GSD    LAS         68.89    tokeparser_incremental        
French-GSD    LAS         69.43    tokeparser                    
French-GSD    LAS         82.90    tagparser                     
French-GSD    LAS         84.04    tagparser_sequential          
French-GSD    LAS         84.66    tagparser_sequential_strong   

French-GSD    Lemmas      92.00    tokeparser                    
French-GSD    Lemmas      92.08    tokeparser_incremental        
French-GSD    Lemmas      96.82    tagparser_sequential          
French-GSD    Lemmas      96.82    tagparser_sequential_strong   
French-GSD    Lemmas      96.90    tagparser                     

French-GSD    Sentences   92.62    tagparser_sequential_strong   
French-GSD    Sentences   93.01    tagparser                     
French-GSD    Sentences   93.16    tagparser_sequential          
French-GSD    Sentences   93.24    tokeparser_incremental        
French-GSD    Sentences   93.37    tokeparser                    

French-GSD    Tokens      100.00   tagparser                     
French-GSD    Tokens      100.00   tagparser_sequential          
French-GSD    Tokens      100.00   tagparser_sequential_strong   
French-GSD    Tokens      99.51    tokeparser                    
French-GSD    Tokens      99.53    tokeparser_incremental        

French-GSD    UAS         74.57    tokeparser_incremental        
French-GSD    UAS         74.96    tokeparser                    
French-GSD    UAS         86.61    tagparser                     
French-GSD    UAS         87.55    tagparser_sequential          
French-GSD    UAS         87.92    tagparser_sequential_strong   

French-GSD    UFeats      88.47    tokeparser_incremental        
French-GSD    UFeats      88.52    tokeparser                    
French-GSD    UFeats      95.91    tagparser                     
French-GSD    UFeats      96.00    tagparser_sequential_strong   
French-GSD    UFeats      96.06    tagparser_sequential          

French-GSD    UPOS        90.59    tokeparser_incremental        
French-GSD    UPOS        90.64    tokeparser                    
French-GSD    UPOS        96.72    tagparser_sequential_strong   
French-GSD    UPOS        96.82    tagparser                     
French-GSD    UPOS        96.86    tagparser_sequential          
--------------------------------------------------------------------------------
Hebrew-HTB    LAS         74.76    tagparser                     
Hebrew-HTB    LAS         75.39    tagparser_sequential          
Hebrew-HTB    LAS         76.85    tagparser_sequential_strong   

Hebrew-HTB    Lemmas      86.41    tagparser                     
Hebrew-HTB    Lemmas      86.46    tagparser_sequential          
Hebrew-HTB    Lemmas      86.59    tagparser_sequential_strong   

Hebrew-HTB    Sentences   100.00   tagparser                     
Hebrew-HTB    Sentences   100.00   tagparser_sequential          
Hebrew-HTB    Sentences   99.69    tagparser_sequential_strong   

Hebrew-HTB    Tokens      100.00   tagparser                     
Hebrew-HTB    Tokens      100.00   tagparser_sequential          
Hebrew-HTB    Tokens      100.00   tagparser_sequential_strong   

Hebrew-HTB    UAS         80.04    tagparser                     
Hebrew-HTB    UAS         80.19    tagparser_sequential          
Hebrew-HTB    UAS         81.49    tagparser_sequential_strong   

Hebrew-HTB    UFeats      92.90    tagparser                     
Hebrew-HTB    UFeats      93.19    tagparser_sequential_strong   
Hebrew-HTB    UFeats      93.41    tagparser_sequential          

Hebrew-HTB    UPOS        94.26    tagparser                     
Hebrew-HTB    UPOS        94.55    tagparser_sequential          
Hebrew-HTB    UPOS        94.62    tagparser_sequential_strong   
