#! /usr/bin/python3

import sys
from collections import Counter

# Parameters :
nbOfErrorsType = 20 #Consider only the most frequent error types
nbOfExamplesEach = 20 #Number of examples by types of error to display
window = 20 #size of the context window when printing an example
columnsToConsider = [] #MCF columns to work with (must have 2 elements)
extraColumns = [] #MCF columns to print (can be empty)
totallyIgnorePunctuation = False
longWordsCutoffPrefixSize = 4
longWordsCutoffString = ".."
longWordsCutoff = len(longWordsCutoffString) + 2*longWordsCutoffPrefixSize

def printColumns(prefix, cols, suffix, colSizes = []) :
  if len(colSizes) == 0 :
    colSizes = [10] * len(cols)

  print(prefix, end = '')

  for index in range(len(cols)) :
    col = cols[index]
    size = colSizes[index]
    print(col, end = '')
    if index == len(cols)-1 :
      break
    for i in range(size - len(col)) :
      print(' ', end = '')

  print(suffix, end = '')

class Error :
  def __init__(self, sentence, index, isCorr) :
    self.index = index
    self.sentence = sentence
    self.id = "0"
    self.isCorr = isCorr
    self.reference = sentence[index][1]
    self.prediction = sentence[index][2]

  def __str__(self) :
    return self.prediction + "\t->\t" + self.reference

  def __repr__(self) :
    return str(self)

  def __eq__(self, other) :
    sameClass = isinstance(other, self.__class__)
    sameRef = self.reference == other.reference
    samePred = self.prediction == other.prediction
    return sameClass and sameRef and samePred

  def __hash__(self) :
    return hash(self.reference) ^ hash(self.prediction)

  def setId(self, id) :
    self.id = str(id)

  def printAsSentence(self) :
    colsWord = ["ID:"+self.id,"|"]
    colsGold = ["GOLD","|"]
    extraColsGold = [[" ", " "] for _ in range(len(extraColumns))]
    colsPred = ["PRED","|"]
    extraColsPred = [[" ", " "] for _ in range(len(extraColumns))]
    colsCorr = ["CORR","|"]
    extraColsCorr = [[" ", " "] for _ in range(len(extraColumns))]

    for index in range(len(self.sentence)) :
      elem = self.sentence[index]
      pref = ""
      suf = ""
      if index == self.index :
        pref = "("
        suf = ")"
      if len(elem[0]) <= longWordsCutoff or index == self.index :
        colsWord += [elem[0]]
      else :
        word = elem[0][0:longWordsCutoffPrefixSize] + longWordsCutoffString + elem[0][-longWordsCutoffPrefixSize:]
        colsWord += [word]
      colsGold += [pref+elem[1]+suf]
      colsPred += [pref+elem[2]+suf]
      if self.isCorr :
        colsCorr += [pref+elem[3]+suf]
      for k in range(len(extraColumns)) :
        if self.isCorr :
          extraColsGold[k] += [pref+elem[4+3*k]+suf]
          extraColsPred[k] += [pref+elem[4+3*k+1]+suf]
          extraColsCorr[k] += [pref+elem[4+3*k+2]+suf]
        else :
          extraColsGold[k] += [pref+elem[3+2*k]+suf]
          extraColsPred[k] += [pref+elem[3+2*k+1]+suf]

    colSizes = []
    for i in range(len(colsWord)) :
      maxLenExtra = 0
      for k in range(len(extraColumns)) :
        maxLenExtra = max(maxLenExtra, len(extraColsGold[k][i]))
        maxLenExtra = max(maxLenExtra, len(extraColsPred[k][i]))
        if self.isCorr :
          maxLenExtra = max(maxLenExtra, len(extraColsCorr[k][i]))

      if self.isCorr :
        colSizes += [1+max(len(colsWord[i]), len(colsGold[i]), len(colsPred[i]), len(colsCorr[i]), maxLenExtra)]
      else :
        colSizes += [1+max(len(colsWord[i]), len(colsGold[i]), len(colsPred[i]), maxLenExtra)]

    printColumns("", colsWord, "\n", colSizes)
    printColumns("", colsGold, "\n", colSizes)
    for k in range(len(extraColumns)) :
      printColumns("", extraColsGold[k], "\n", colSizes)
    printColumns("", colsPred, "\n", colSizes)
    for k in range(len(extraColumns)) :
      printColumns("", extraColsPred[k], "\n", colSizes)
    if self.isCorr :
      printColumns("", colsCorr, "\n", colSizes)
      for k in range(len(extraColumns)) :
        printColumns("", extraColsCorr[k], "\n", colSizes)

    print('')

def getLineAsList(inputFile) :
  line = inputFile.readline()

  if len(line) == 0 :
    return line

  line = line.replace("\n", "")
  line = line.split("\t")
  result = []
  for col in columnsToConsider :
    result += [line[col]]

  for col in extraColumns :
    result += [line[col]]

  return result

def readFiles(refFn, pred1Fn, pred2Fn) :
  fRef = open(refFn, "r")
  fPred1 = open(pred1Fn, "r")
  fPred2 = open(pred2Fn, "r")

  contentRef = []
  contentPred1 = []
  contentPred2 = []

  while True :
    line = getLineAsList(fRef)

    if len(line) == 0 :
      break

    if totallyIgnorePunctuation :
      if POSIsPunctuation(line[1]) :
        line = getLineAsList(fPred1)
        line = getLineAsList(fPred2)
        continue

    contentRef.append(line)

    line = getLineAsList(fPred1)
    contentPred1.append(line)

    line = getLineAsList(fPred2)
    contentPred2.append(line)

  fRef.close()
  fPred1.close()
  fPred2.close()
  return contentRef, contentPred1, contentPred2

def POSIsPunctuation(POS) :
  return POS == "PCT" or POS == "PONCT" or POS == "ponctw" or POS == "poncts"

def printUsageAndExit() :
  requiredArgs = "refFilename.txt prediction1.txt prediction2.txt --columns x,y,z..."
  print("USAGE : python", sys.argv[0], requiredArgs)
  exit(1)

def printErrorsCount(prefix, examples, examplesList) :
  print("")
  print(prefix, len(examples), "total")
  cols = ["NB", "PRED", "->", "GOLD", "ID"]
  colSizes = [len(s) for s in cols]
  for elem in examplesList :
    colSizes[0] = max(colSizes[0], len(str(elem[1]))+1)
    colSizes[1] = max(colSizes[1], len(elem[0].prediction)+1)
    colSizes[2] = max(colSizes[2], len("->")+1)
    colSizes[3] = max(colSizes[3], len(elem[0].reference)+1)
    colSizes[4] = max(colSizes[4], len(str(elem[0].id))+1)

  printColumns("\t", cols, "\n", colSizes)
  for elem in examplesList :
    cols = [str(elem[1]), elem[0].prediction, "->", elem[0].reference, elem[0].id]
    printColumns("\t", cols, "\n", colSizes)

def evaluateAndPrintErrors(ref, pred1, pred2) :
  errors1 = []
  errors2 = []
  errorsIntroduced = []
  errorsCorrected = []
  viewWindow = window

  for i in range(len(ref)) :
    if ref[i][0] != pred1[i][0] or ref[i][0] != pred2[i][0] : 
      print("The 3 files don't match :", ref[i][0], pred1[i][0], pred2[i][0])
      printUsageAndExit()

    if ref[i][1].lower() != pred1[i][1].lower() :
      sentence = []
      errorIndex = 0
      for j in range(-viewWindow, viewWindow+1, 1) :
        index = i+j
        if j == 0 :
          errorIndex = len(sentence)
        if index >= 0 and index < len(ref) :
          toAppend = [ref[index][0], ref[index][1], pred1[index][1]]
          for k in range(len(extraColumns)) :
            toAppend += [ref[index][k+2], pred1[index][k+2]]
          sentence.append(toAppend)
      errors1.append(Error(sentence, errorIndex, False))

    if ref[i][1].lower() != pred2[i][1].lower() :
      sentence = []
      errorIndex = 0
      for j in range(-viewWindow, viewWindow+1, 1) :
        index = i+j
        if j == 0 :
          errorIndex = len(sentence)
        if index >= 0 and index < len(ref) :
          toAppend = [ref[index][0], ref[index][1], pred2[index][1]]
          for k in range(len(extraColumns)) :
            toAppend += [ref[index][k+2], pred2[index][k+2]]
          sentence.append(toAppend)
      errors2.append(Error(sentence, errorIndex, False))

    if ref[i][1].lower() == pred1[i][1].lower() and ref[i][1].lower() != pred2[i][1].lower() :
      sentence = []
      errorIndex = 0
      for j in range(-viewWindow, viewWindow+1, 1) :
        index = i+j
        if j == 0 :
          errorIndex = len(sentence)
        if index >= 0 and index < len(ref) :
          toAppend = [ref[index][0], ref[index][1], pred2[index][1]]
          for k in range(len(extraColumns)) :
            toAppend += [ref[index][k+2], pred2[index][k+2]]
          sentence.append(toAppend)
      errorsIntroduced.append(Error(sentence, errorIndex, False))

    if ref[i][1].lower() == pred2[i][1].lower() and ref[i][1].lower() != pred1[i][1].lower() :
      sentence = []
      errorIndex = 0
      for j in range(-viewWindow, viewWindow+1, 1) :
        index = i+j
        if j == 0 :
          errorIndex = len(sentence)
        if index >= 0 and index < len(ref) :
          toAppend = [ref[index][0], ref[index][1], pred1[index][1], pred2[index][1]]
          for k in range(len(extraColumns)) :
            toAppend += [ref[index][k+2], pred1[index][k+2], pred2[index][k+2]]
          sentence.append(toAppend)
      errorsCorrected.append(Error(sentence, errorIndex, True))

  errors1Count = Counter(errors1)
  errors2Count = Counter(errors2)
  errorsIntroducedCount = Counter(errorsIntroduced)
  errorsCorrectedCount = Counter(errorsCorrected)

  errors1CountList = []
  errors2CountList = []
  errorsIntroducedCountList = []
  errorsCorrectedCountList = []

  cutoff = nbOfErrorsType

  for error, nbOcc in errors1Count.items() :
    errors1CountList.append([error, nbOcc])

  for error, nbOcc in errors2Count.items() :
    errors2CountList.append([error, nbOcc])

  for error, nbOcc in errorsIntroducedCount.items() :
    errorsIntroducedCountList.append([error, nbOcc])

  for error, nbOcc in errorsCorrectedCount.items() :
    errorsCorrectedCountList.append([error, nbOcc])

  errors1CountList.sort(key = lambda x: x[1], reverse = True)
  errors2CountList.sort(key = lambda x: x[1], reverse = True)
  errorsIntroducedCountList.sort(key = lambda x: x[1], reverse = True)
  errorsCorrectedCountList.sort(key = lambda x: x[1], reverse = True)

  errors1CountList = errors1CountList[:cutoff]
  errors2CountList = errors2CountList[:cutoff]
  errorsIntroducedCountList = errorsIntroducedCountList[:cutoff]
  errorsCorrectedCountList = errorsCorrectedCountList[:cutoff]

  examplesById = []

  for errorType in errors1CountList :
    examplesById.append([])
    errorType[0].setId(len(examplesById))
    for error in errors1 :
      if error == errorType[0] :
        error.setId(errorType[0].id)
        examplesById[-1].append(error)

  for errorType in errors2CountList :
    examplesById.append([])
    errorType[0].setId(len(examplesById))
    for error in errors2 :
      if error == errorType[0] :
        error.setId(errorType[0].id)
        examplesById[-1].append(error)

  for errorType in errorsIntroducedCountList :
    examplesById.append([])
    errorType[0].setId(len(examplesById))
    for error in errorsIntroduced :
      if error == errorType[0] :
        error.setId(errorType[0].id)
        examplesById[-1].append(error)

  for errorType in errorsCorrectedCountList :
    examplesById.append([])
    errorType[0].setId(len(examplesById))
    for error in errorsCorrected :
      if error == errorType[0] :
        error.setId(errorType[0].id)
        examplesById[-1].append(error)

  score1 = "{0:.2f}%".format(100.0*(len(ref)-len(errors1))/len(ref))
  score2 = "{0:.2f}%".format(100.0*(len(ref)-len(errors2))/len(ref))
  cols = [sys.argv[2], "SCORE :", score1]
  printColumns("", cols, "\n", [2+max(len(sys.argv[2]), len(sys.argv[3])), 10, 10])
  cols = [sys.argv[3], "SCORE :", score2]
  printColumns("", cols, "\n", [2+max(len(sys.argv[2]), len(sys.argv[3])), 10, 10])

  printErrorsCount("Errors made by " + sys.argv[2], errors1, errors1CountList)

  printErrorsCount("Errors made by " + sys.argv[3], errors2, errors2CountList)

  printErrorsCount("Errors introduced by " + sys.argv[3], errorsIntroduced, errorsIntroducedCountList)

  printErrorsCount("Errors corrected by " + sys.argv[3], errorsCorrected, errorsCorrectedCountList)

  nbExamplesEach = nbOfExamplesEach

  print("")
  print("")
  print("Examples of sentences by ID of error :")
  print("")
  for examples in examplesById :
    for i in range(min(len(examples), nbExamplesEach)) :
      examples[i].printAsSentence()

def main() :
  if len(sys.argv) != 6 :
    printUsageAndExit()
  if sys.argv[4] != "--columns" :
    printUsageAndExit()

  columns = sys.argv[5].split(',')
  if len(columns) < 2 :
    printUsageAndExit()

  columnsToConsider.append(int(columns[0]))
  columnsToConsider.append(int(columns[1]))
  for col in columns[2:] :
    extraColumns.append(int(col))

  ref, pred1, pred2 = readFiles(sys.argv[1], sys.argv[2], sys.argv[3])

  if len(ref) != len(pred1) or len(ref) != len(pred2) :
    print("The 3 files don't have the same number of lines")
    printUsageAndExit()

  evaluateAndPrintErrors(ref, pred1, pred2)

if __name__ == "__main__" :
  main()
