@	120	prep	[pred="à_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = loc|à]	à_____1	Default		%default
afin	120	prep	[pred="afin_____1<Objde:de-sn|de-sinf>"]	afin_____1	Default		%default
afin d'	120	prep	[pred="afin de_____1<Obj:sadv|sinf>",@e]	afin de_____1	Default	e	%default
afin d'__prep	120	prep	[pred="afin de_____1<Obj:sadv|sinf>",@e]	afin de_____1	Default	e	%default
afin de	120	prep	[pred="afin de_____1<Obj:sadv|sinf>"]	afin de_____1	Default		%default
afin de__prep	120	prep	[pred="afin de_____1<Obj:sadv|sinf>"]	afin de_____1	Default		%default
apr.	120	prep	[pred="après_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|après]	après_____1	Default		%default
après	120	prep	[pred="après_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|après]	après_____1	Default		%default
au beau milieu d'	120	prep	[pred="au beau milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au beau milieu de_____1	Default	e	%default
au beau milieu d'__prep	120	prep	[pred="au beau milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au beau milieu de_____1	Default	e	%default
au beau milieu de	120	prep	[pred="au beau milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au beau milieu de_____1	Default		%default
au beau milieu de__prep	120	prep	[pred="au beau milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au beau milieu de_____1	Default		%default
au bord d'	120	prep	[pred="au bord de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au bord de_____1	Default	e	%default
au bord d'__prep	120	prep	[pred="au bord de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au bord de_____1	Default	e	%default
au bord de	120	prep	[pred="au bord de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au bord de_____1	Default		%default
au bord de__prep	120	prep	[pred="au bord de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au bord de_____1	Default		%default
au bout d'	120	prep	[pred="au bout de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au bout de_____1	Default	e	%default
au bout d'__prep	120	prep	[pred="au bout de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au bout de_____1	Default	e	%default
au bout de	120	prep	[pred="au bout de_____1<Obj:sa|sadv|sn>",pcas = loc]	au bout de_____1	Default		%default
au bout de__prep	120	prep	[pred="au bout de_____1<Obj:sa|sadv|sn>",pcas = loc]	au bout de_____1	Default		%default
au centre d'	120	prep	[pred="au centre de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au centre de_____1	Default	e	%default
au centre d'__prep	120	prep	[pred="au centre de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au centre de_____1	Default	e	%default
au centre de	120	prep	[pred="au centre de_____1<Obj:sa|sadv|sn>",pcas = loc]	au centre de_____1	Default		%default
au centre de__prep	120	prep	[pred="au centre de_____1<Obj:sa|sadv|sn>",pcas = loc]	au centre de_____1	Default		%default
au comble d'	120	prep	[pred="au comble de_____1<Obj:sa|sadv|sn>",@e]	au comble de_____1	Default	e	%default
au comble d'__prep	120	prep	[pred="au comble de_____1<Obj:sa|sadv|sn>",@e]	au comble de_____1	Default	e	%default
au comble de	120	prep	[pred="au comble de_____1<Obj:sa|sadv|sn>"]	au comble de_____1	Default		%default
au comble de__prep	120	prep	[pred="au comble de_____1<Obj:sa|sadv|sn>"]	au comble de_____1	Default		%default
au cours d'	120	prep	[pred="au cours de_____1<Obj:sa|sadv|sn>",@e]	au cours de_____1	Default	e	%default
au cours d'__prep	120	prep	[pred="au cours de_____1<Obj:sa|sadv|sn>",@e]	au cours de_____1	Default	e	%default
au cours de	120	prep	[pred="au cours de_____1<Obj:sa|sadv|sn>"]	au cours de_____1	Default		%default
au cours de__prep	120	prep	[pred="au cours de_____1<Obj:sa|sadv|sn>"]	au cours de_____1	Default		%default
au côté d'	120	prep	[pred="au côté de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au côté de_____1	Default	e	%default
au côté d'__prep	120	prep	[pred="au côté de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au côté de_____1	Default	e	%default
au côté de	120	prep	[pred="au côté de_____1<Obj:sa|sadv|sn>",pcas = loc]	au côté de_____1	Default		%default
au côté de__prep	120	prep	[pred="au côté de_____1<Obj:sa|sadv|sn>",pcas = loc]	au côté de_____1	Default		%default
au dehors d'	120	prep	[pred="au dehors de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au dehors de_____1	Default	e	%default
au dehors d'__prep	120	prep	[pred="au dehors de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au dehors de_____1	Default	e	%default
au dehors de	120	prep	[pred="au dehors de_____1<Obj:sa|sadv|sn>",pcas = loc]	au dehors de_____1	Default		%default
au dehors de__prep	120	prep	[pred="au dehors de_____1<Obj:sa|sadv|sn>",pcas = loc]	au dehors de_____1	Default		%default
au delà d'	120	prep	[pred="au delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au delà de_____1	Default	e	%default
au delà d'	120	prep	[pred="au-delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au-delà de_____1	Default	e	%default
au delà d'__prep	120	prep	[pred="au delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au delà de_____1	Default	e	%default
au delà de	120	prep	[pred="au delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au delà de_____1	Default		%default
au delà de	120	prep	[pred="au-delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au-delà de_____1	Default		%default
au delà de__prep	120	prep	[pred="au delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au delà de_____1	Default		%default
au dessous d'	120	prep	[pred="au dessous de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au dessous de_____1	Default	e	%default
au dessous d'__prep	120	prep	[pred="au dessous de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au dessous de_____1	Default	e	%default
au dessous de	120	prep	[pred="au dessous de_____1<Obj:sa|sadv|sn>",pcas = loc]	au dessous de_____1	Default		%default
au dessous de__prep	120	prep	[pred="au dessous de_____1<Obj:sa|sadv|sn>",pcas = loc]	au dessous de_____1	Default		%default
au devant d'	120	prep	[pred="au devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au devant de_____1	Default	e	%default
au devant d'__prep	120	prep	[pred="au devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au devant de_____1	Default	e	%default
au devant de	120	prep	[pred="au devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au devant de_____1	Default		%default
au devant de__prep	120	prep	[pred="au devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au devant de_____1	Default		%default
au fond d'	120	prep	[pred="au fond de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au fond de_____1	Default	e	%default
au fond d'__prep	120	prep	[pred="au fond de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au fond de_____1	Default	e	%default
au fond de	120	prep	[pred="au fond de_____1<Obj:sa|sadv|sn>",pcas = loc]	au fond de_____1	Default		%default
au fond de__prep	120	prep	[pred="au fond de_____1<Obj:sa|sadv|sn>",pcas = loc]	au fond de_____1	Default		%default
au fur et à mesure d'	120	prep	[pred="au fur et à mesure de_____1<Obj:sa|sadv|sn>",@e]	au fur et à mesure de_____1	Default	e	%default
au fur et à mesure d'__prep	120	prep	[pred="au fur et à mesure de_____1<Obj:sa|sadv|sn>",@e]	au fur et à mesure de_____1	Default	e	%default
au fur et à mesure de	120	prep	[pred="au fur et à mesure de_____1<Obj:sa|sadv|sn>"]	au fur et à mesure de_____1	Default		%default
au fur et à mesure de__prep	120	prep	[pred="au fur et à mesure de_____1<Obj:sa|sadv|sn>"]	au fur et à mesure de_____1	Default		%default
au grand dam d'	120	prep	[pred="au grand dam de_____1<Obj:sa|sadv|sn>",@e]	au grand dam de_____1	Default	e	%default
au grand dam d'__prep	120	prep	[pred="au grand dam de_____1<Obj:sa|sadv|sn>",@e]	au grand dam de_____1	Default	e	%default
au grand dam de	120	prep	[pred="au grand dam de_____1<Obj:sa|sadv|sn>"]	au grand dam de_____1	Default		%default
au grand dam de__prep	120	prep	[pred="au grand dam de_____1<Obj:sa|sadv|sn>"]	au grand dam de_____1	Default		%default
au gré d'	120	prep	[pred="au gré de_____1<Obj:sa|sadv|sn>",@e]	au gré de_____1	Default	e	%default
au gré d'__prep	120	prep	[pred="au gré de_____1<Obj:sa|sadv|sn>",@e]	au gré de_____1	Default	e	%default
au gré de	120	prep	[pred="au gré de_____1<Obj:sa|sadv|sn>"]	au gré de_____1	Default		%default
au gré de__prep	120	prep	[pred="au gré de_____1<Obj:sa|sadv|sn>"]	au gré de_____1	Default		%default
au large d'	120	prep	[pred="au large de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au large de_____1	Default	e	%default
au large d'__prep	120	prep	[pred="au large de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au large de_____1	Default	e	%default
au large de	120	prep	[pred="au large de_____1<Obj:sa|sadv|sn>",pcas = loc]	au large de_____1	Default		%default
au large de__prep	120	prep	[pred="au large de_____1<Obj:sa|sadv|sn>",pcas = loc]	au large de_____1	Default		%default
au lieu d'	120	prep	[pred="au lieu de_____1<Obj:sa|sadv|sinf|sn>",@e]	au lieu de_____1	Default	e	%default
au lieu d'__prep	120	prep	[pred="au lieu de_____1<Obj:sa|sadv|sinf|sn>",@e]	au lieu de_____1	Default	e	%default
au lieu de	120	prep	[pred="au lieu de_____1<Obj:sa|sadv|sinf|sn>"]	au lieu de_____1	Default		%default
au lieu de__prep	120	prep	[pred="au lieu de_____1<Obj:sa|sadv|sinf|sn>"]	au lieu de_____1	Default		%default
au long d'	120	prep	[pred="au long de_____1<Obj:sa|sadv|sn>",@e]	au long de_____1	Default	e	%default
au long d'__prep	120	prep	[pred="au long de_____1<Obj:sa|sadv|sn>",@e]	au long de_____1	Default	e	%default
au long de	120	prep	[pred="au long de_____1<Obj:sa|sadv|sn>"]	au long de_____1	Default		%default
au long de__prep	120	prep	[pred="au long de_____1<Obj:sa|sadv|sn>"]	au long de_____1	Default		%default
au milieu d'	120	prep	[pred="au milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au milieu de_____1	Default	e	%default
au milieu d'__prep	120	prep	[pred="au milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au milieu de_____1	Default	e	%default
au milieu de	120	prep	[pred="au milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au milieu de_____1	Default		%default
au milieu de__prep	120	prep	[pred="au milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au milieu de_____1	Default		%default
au moment d'	120	prep	[pred="au moment de_____1<Obj:sa|sadv|sinf|sn>",@e]	au moment de_____1	Default	e	%default
au moment d'__prep	120	prep	[pred="au moment de_____1<Obj:sa|sadv|sinf|sn>",@e]	au moment de_____1	Default	e	%default
au moment de	120	prep	[pred="au moment de_____1<Obj:sa|sadv|sinf|sn>"]	au moment de_____1	Default		%default
au moment de__prep	120	prep	[pred="au moment de_____1<Obj:sa|sadv|sinf|sn>"]	au moment de_____1	Default		%default
au moyen d'	120	prep	[pred="au moyen de_____1<Obj:sa|sadv|sn>",@e]	au moyen de_____1	Default	e	%default
au moyen d'__prep	120	prep	[pred="au moyen de_____1<Obj:sa|sadv|sn>",@e]	au moyen de_____1	Default	e	%default
au moyen de	120	prep	[pred="au moyen de_____1<Obj:sa|sadv|sn>"]	au moyen de_____1	Default		%default
au moyen de__prep	120	prep	[pred="au moyen de_____1<Obj:sa|sadv|sn>"]	au moyen de_____1	Default		%default
au nom d'	120	prep	[pred="au nom de_____1<Obj:sa|sadv|sn>",@e]	au nom de_____1	Default	e	%default
au nom d'__prep	120	prep	[pred="au nom de_____1<Obj:sa|sadv|sn>",@e]	au nom de_____1	Default	e	%default
au nom de	120	prep	[pred="au nom de_____1<Obj:sa|sadv|sn>"]	au nom de_____1	Default		%default
au nom de__prep	120	prep	[pred="au nom de_____1<Obj:sa|sadv|sn>"]	au nom de_____1	Default		%default
au point d'	120	prep	[pred="au point de_____1<Obj:sa|sadv|sinf|sn>",@e]	au point de_____1	Default	e	%default
au point d'__prep	120	prep	[pred="au point de_____1<Obj:sa|sadv|sinf|sn>",@e]	au point de_____1	Default	e	%default
au point de	120	prep	[pred="au point de_____1<Obj:sa|sadv|sinf|sn>"]	au point de_____1	Default		%default
au point de__prep	120	prep	[pred="au point de_____1<Obj:sa|sadv|sinf|sn>"]	au point de_____1	Default		%default
au prorata d'	120	prep	[pred="au prorata de_____1<Obj:sa|sadv|sn>",@e]	au prorata de_____1	Default	e	%default
au prorata d'__prep	120	prep	[pred="au prorata de_____1<Obj:sa|sadv|sn>",@e]	au prorata de_____1	Default	e	%default
au prorata de	120	prep	[pred="au prorata de_____1<Obj:sa|sadv|sn>"]	au prorata de_____1	Default		%default
au prorata de__prep	120	prep	[pred="au prorata de_____1<Obj:sa|sadv|sn>"]	au prorata de_____1	Default		%default
au ras d'	120	prep	[pred="au ras de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au ras de_____1	Default	e	%default
au ras d'__prep	120	prep	[pred="au ras de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au ras de_____1	Default	e	%default
au ras de	120	prep	[pred="au ras de_____1<Obj:sa|sadv|sn>",pcas = loc]	au ras de_____1	Default		%default
au ras de__prep	120	prep	[pred="au ras de_____1<Obj:sa|sadv|sn>",pcas = loc]	au ras de_____1	Default		%default
au sein d'	120	prep	[pred="au sein de_____1<Obj:sa|sadv|sn>",@e]	au sein de_____1	Default	e	%default
au sein d'__prep	120	prep	[pred="au sein de_____1<Obj:sa|sadv|sn>",@e]	au sein de_____1	Default	e	%default
au sein de	120	prep	[pred="au sein de_____1<Obj:sa|sadv|sn>"]	au sein de_____1	Default		%default
au sein de__prep	120	prep	[pred="au sein de_____1<Obj:sa|sadv|sn>"]	au sein de_____1	Default		%default
au sortir d'	120	prep	[pred="au sortir de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au sortir de_____1	Default	e	%default
au sortir d'__prep	120	prep	[pred="au sortir de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au sortir de_____1	Default	e	%default
au sortir de	120	prep	[pred="au sortir de_____1<Obj:sa|sadv|sn>",pcas = loc]	au sortir de_____1	Default		%default
au sortir de__prep	120	prep	[pred="au sortir de_____1<Obj:sa|sadv|sn>",pcas = loc]	au sortir de_____1	Default		%default
au sujet d'	120	prep	[pred="au sujet de_____1<Obj:sa|sadv|sinf|sn>",pcas= au_sujet_de,@e]	au sujet de_____1	Default	e	%default
au sujet d'__prep	120	prep	[pred="au sujet de_____1<Obj:sa|sadv|sinf|sn>",pcas= au_sujet_de,@e]	au sujet de_____1	Default	e	%default
au sujet de	120	prep	[pred="au sujet de_____1<Obj:sa|sadv|sinf|sn>",pcas= au_sujet_de]	au sujet de_____1	Default		%default
au sujet de__prep	120	prep	[pred="au sujet de_____1<Obj:sa|sadv|sinf|sn>",pcas= au_sujet_de]	au sujet de_____1	Default		%default
au terme d'	120	prep	[pred="au terme de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au terme de_____1	Default	e	%default
au terme d'__prep	120	prep	[pred="au terme de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au terme de_____1	Default	e	%default
au terme de	120	prep	[pred="au terme de_____1<Obj:sa|sadv|sn>",pcas = loc]	au terme de_____1	Default		%default
au terme de__prep	120	prep	[pred="au terme de_____1<Obj:sa|sadv|sn>",pcas = loc]	au terme de_____1	Default		%default
au travers d'	120	prep	[pred="au travers de_____1<Obj:sa|sadv|sn>",pcas = loc|au_travers_de,@e]	au travers de_____1	Default	e	%default
au travers d'__prep	120	prep	[pred="au travers de_____1<Obj:sa|sadv|sn>",pcas = loc|au_travers_de,@e]	au travers de_____1	Default	e	%default
au travers de	120	prep	[pred="au travers de_____1<Obj:sa|sadv|sn>",pcas = loc|au_travers_de]	au travers de_____1	Default		%default
au travers de__prep	120	prep	[pred="au travers de_____1<Obj:sa|sadv|sn>",pcas = loc|au_travers_de]	au travers de_____1	Default		%default
au tréfonds d'	120	prep	[pred="au tréfonds de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au tréfonds de_____1	Default	e	%default
au tréfonds d'__prep	120	prep	[pred="au tréfonds de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au tréfonds de_____1	Default	e	%default
au tréfonds de	120	prep	[pred="au tréfonds de_____1<Obj:sa|sadv|sn>",pcas = loc]	au tréfonds de_____1	Default		%default
au tréfonds de__prep	120	prep	[pred="au tréfonds de_____1<Obj:sa|sadv|sn>",pcas = loc]	au tréfonds de_____1	Default		%default
au vu d'	120	prep	[pred="au vu de_____1<Obj:sa|sadv|sn>",@e]	au vu de_____1	Default	e	%default
au vu d'__prep	120	prep	[pred="au vu de_____1<Obj:sa|sadv|sn>",@e]	au vu de_____1	Default	e	%default
au vu de	120	prep	[pred="au vu de_____1<Obj:sa|sadv|sn>"]	au vu de_____1	Default		%default
au vu de__prep	120	prep	[pred="au vu de_____1<Obj:sa|sadv|sn>"]	au vu de_____1	Default		%default
au vu et au su d'	120	prep	[pred="au vu et au su de_____1<Obj:sa|sadv|sn>",@e]	au vu et au su de_____1	Default	e	%default
au vu et au su d'__prep	120	prep	[pred="au vu et au su de_____1<Obj:sa|sadv|sn>",@e]	au vu et au su de_____1	Default	e	%default
au vu et au su de	120	prep	[pred="au vu et au su de_____1<Obj:sa|sadv|sn>"]	au vu et au su de_____1	Default		%default
au vu et au su de__prep	120	prep	[pred="au vu et au su de_____1<Obj:sa|sadv|sn>"]	au vu et au su de_____1	Default		%default
au-delà d'	120	prep	[pred="au-delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au-delà de_____1	Default	e	%default
au-delà de	120	prep	[pred="au-delà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au-delà de_____1	Default		%default
au-dessous d'	120	prep	[pred="au-dessous de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au-dessous de_____1	Default	e	%default
au-dessous d'__prep	120	prep	[pred="au-dessous de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	au-dessous de_____1	Default	e	%default
au-dessous de	120	prep	[pred="au-dessous de_____1<Obj:sa|sadv|sn>",pcas = loc]	au-dessous de_____1	Default		%default
au-dessous de__prep	120	prep	[pred="au-dessous de_____1<Obj:sa|sadv|sn>",pcas = loc]	au-dessous de_____1	Default		%default
au-dessus d'	120	prep	[pred="au-dessus de_____1<Obj:sa|sadv|sn>",pcas = loc|au_dessus_de,@e]	au-dessus de_____1	Default	e	%default
au-dessus d'__prep	120	prep	[pred="au-dessus de_____1<Obj:sa|sadv|sn>",pcas = loc|au_dessus_de,@e]	au-dessus de_____1	Default	e	%default
au-dessus de	120	prep	[pred="au-dessus de_____1<Obj:sa|sadv|sn>",pcas = loc|au_dessus_de]	au-dessus de_____1	Default		%default
au-dessus de__prep	120	prep	[pred="au-dessus de_____1<Obj:sa|sadv|sn>",pcas = loc|au_dessus_de]	au-dessus de_____1	Default		%default
au-devant d'	120	prep	[pred="au-devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au-devant de_____1	Default	e	%default
au-devant d'__prep	120	prep	[pred="au-devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	au-devant de_____1	Default	e	%default
au-devant de	120	prep	[pred="au-devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au-devant de_____1	Default		%default
au-devant de__prep	120	prep	[pred="au-devant de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	au-devant de_____1	Default		%default
auprès d'	120	prep	[pred="auprès de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	auprès de_____1	Default	e	%default
auprès d'__prep	120	prep	[pred="auprès de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	auprès de_____1	Default	e	%default
auprès de	120	prep	[pred="auprès de_____1<Obj:sa|sadv|sn>",pcas = loc]	auprès de_____1	Default		%default
auprès de__prep	120	prep	[pred="auprès de_____1<Obj:sa|sadv|sn>",pcas = loc]	auprès de_____1	Default		%default
autour d'	120	prep	[pred="autour de_____1<Obj:sa|sadv|sn>",pcas = loc|autour_de,@e]	autour de_____1	Default	e	%default
autour d'__prep	120	prep	[pred="autour de_____1<Obj:sa|sadv|sn>",pcas = loc|autour_de,@e]	autour de_____1	Default	e	%default
autour de	120	prep	[pred="autour de_____1<Obj:sa|sadv|sn>",pcas = loc|autour_de]	autour de_____1	Default		%default
autour de__prep	120	prep	[pred="autour de_____1<Obj:sa|sadv|sn>",pcas = loc|autour_de]	autour de_____1	Default		%default
aux bons soins d'	120	prep	[pred="aux bons soins de_____1<Obj:sa|sadv|sn>",@e]	aux bons soins de_____1	Default	e	%default
aux bons soins d'__prep	120	prep	[pred="aux bons soins de_____1<Obj:sa|sadv|sn>",@e]	aux bons soins de_____1	Default	e	%default
aux bons soins de	120	prep	[pred="aux bons soins de_____1<Obj:sa|sadv|sn>"]	aux bons soins de_____1	Default		%default
aux bons soins de__prep	120	prep	[pred="aux bons soins de_____1<Obj:sa|sadv|sn>"]	aux bons soins de_____1	Default		%default
aux confins d'	120	prep	[pred="aux confins de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	aux confins de_____1	Default	e	%default
aux confins d'__prep	120	prep	[pred="aux confins de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	aux confins de_____1	Default	e	%default
aux confins de	120	prep	[pred="aux confins de_____1<Obj:sa|sadv|sn>",pcas = loc]	aux confins de_____1	Default		%default
aux confins de__prep	120	prep	[pred="aux confins de_____1<Obj:sa|sadv|sn>",pcas = loc]	aux confins de_____1	Default		%default
aux cotés d'	120	prep	[pred="aux cotés de_____1<Obj:sn>",pcas = loc,@e]	aux cotés de_____1	Default	e	%default
aux cotés d'__prep	120	prep	[pred="aux cotés de_____1<Obj:sn>",pcas = loc,@e]	aux cotés de_____1	Default	e	%default
aux cotés de	120	prep	[pred="aux cotés de_____1<Obj:sn>",pcas = loc]	aux cotés de_____1	Default		%default
aux cotés de__prep	120	prep	[pred="aux cotés de_____1<Obj:sn>",pcas = loc]	aux cotés de_____1	Default		%default
aux dépens d'	120	prep	[pred="aux dépens de_____1<Obj:sa|sadv|sn>",@e]	aux dépens de_____1	Default	e	%default
aux dépens d'__prep	120	prep	[pred="aux dépens de_____1<Obj:sa|sadv|sn>",@e]	aux dépens de_____1	Default	e	%default
aux dépens de	120	prep	[pred="aux dépens de_____1<Obj:sa|sadv|sn>"]	aux dépens de_____1	Default		%default
aux dépens de__prep	120	prep	[pred="aux dépens de_____1<Obj:sa|sadv|sn>"]	aux dépens de_____1	Default		%default
av.	120	prep	[pred="avant_____1<Obj:sa|sadv|sn>",pcas = loc]	avant_____1	Default		%default
avant	120	prep	[pred="avant_____1<Obj:sa|sadv|sn>",pcas = loc]	avant_____1	Default		%default
avant d'	120	prep	[pred="avant de_____1<Obj:sadv|sinf>",@e]	avant de_____1	Default	e	%default
avant d'__prep	120	prep	[pred="avant de_____1<Obj:sadv|sinf>",@e]	avant de_____1	Default	e	%default
avant de	120	prep	[pred="avant de_____1<Obj:sadv|sinf>"]	avant de_____1	Default		%default
avant de__prep	120	prep	[pred="avant de_____1<Obj:sadv|sinf>"]	avant de_____1	Default		%default
avant même	120	prep	[pred="avant même_____1<Obj:sn>"]	avant même_____1	Default		%default
avant même d'	120	prep	[pred="avant même de_____1<Obj:sadv|sinf>",@e]	avant même de_____1	Default	e	%default
avant même d'__prep	120	prep	[pred="avant même de_____1<Obj:sadv|sinf>",@e]	avant même de_____1	Default	e	%default
avant même de	120	prep	[pred="avant même de_____1<Obj:sadv|sinf>"]	avant même de_____1	Default		%default
avant même de__prep	120	prep	[pred="avant même de_____1<Obj:sadv|sinf>"]	avant même de_____1	Default		%default
avant que d'	120	prep	[pred="avant que de_____1<Obj:sadv|sinf>",@e]	avant que de_____1	Default	e	%default
avant que d'__prep	120	prep	[pred="avant que de_____1<Obj:sadv|sinf>",@e]	avant que de_____1	Default	e	%default
avant que de	120	prep	[pred="avant que de_____1<Obj:sadv|sinf>"]	avant que de_____1	Default		%default
avant que de__prep	120	prep	[pred="avant que de_____1<Obj:sadv|sinf>"]	avant que de_____1	Default		%default
avec	120	prep	[pred="avec_____1<Obj:sa|sadv|sn>",pcas = avec]	avec_____1	Default		%default
avt	120	prep	[pred="avant_____1<Obj:sa|sadv|sn>",pcas = loc]	avant_____1	Default		%default
cause d'	120	prep	[pred="cause de_____1<Obj:sa|sadv|sn>",@e]	cause de_____1	Default	e	%default
cause d'__prep	120	prep	[pred="cause de_____1<Obj:sa|sadv|sn>",@e]	cause de_____1	Default	e	%default
cause de	120	prep	[pred="cause de_____1<Obj:sa|sadv|sn>"]	cause de_____1	Default		%default
cause de__prep	120	prep	[pred="cause de_____1<Obj:sa|sadv|sn>"]	cause de_____1	Default		%default
cf	120	prep	[pred="cf_____1<Obj:sa|sadv|sn>"]	cf_____1	Default		%default
cf.	120	prep	[pred="cf_____1<Obj:sa|sadv|sn>"]	cf_____1	Default		%default
chez	120	prep	[pred="chez_____1<Obj:sa|sadv|sn>",pcas = loc|chez]	chez_____1	Default		%default
ci-devant	120	prep	[pred="ci-devant_____1<Obj:sa|sadv|sn>"]	ci-devant_____1	Default		%default
comme	120	prep	[pred="comme_____1<Obj:sa|sadv|sn>",pcas = comme]	comme_____1	Default		%default
compte tenu d'	120	prep	[pred="compte tenu de_____1<Obj:sa|sadv|sn|scompl>",@e]	compte tenu de_____1	Default	e	%default
compte tenu d'__prep	120	prep	[pred="compte tenu de_____1<Obj:sa|sadv|sn|scompl>",@e]	compte tenu de_____1	Default	e	%default
compte tenu de	120	prep	[pred="compte tenu de_____1<Obj:sa|sadv|sn|scompl>"]	compte tenu de_____1	Default		%default
compte tenu de__prep	120	prep	[pred="compte tenu de_____1<Obj:sa|sadv|sn|scompl>"]	compte tenu de_____1	Default		%default
concernant	120	prep	[pred="concernant_____1<Obj:sa|sadv|sn>",chunk_type=NV]	concernant_____1	Default		%default
conf.	120	prep	[pred="cf_____1<Obj:sa|sadv|sn>"]	cf_____1	Default		%default
confer	120	prep	[pred="cf_____1<Obj:sa|sadv|sn>"]	cf_____1	Default		%default
conformément à	120	prep	[pred="conformément à_____1<Obj:sa|sadv|sn>"]	conformément à_____1	Default		%default
conformément à__prep	120	prep	[pred="conformément à_____1<Obj:sa|sadv|sn>"]	conformément à_____1	Default		%default
considérant	120	prep	[pred="considérant_____1<Obj:sn>"]	considérant_____1	Default		%default
considéré	120	prep	[pred="considéré_____1<Obj:sn>"]	considéré_____1	Default		%default
contre	120	prep	[pred="contre_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|contre]	contre_____1	Default		%default
d'	120	prep	[pred="de_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = de,@e]	de_____1	Default	e	%default
d'__prep	120	prep	[pred="de_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = de,@e]	de_____1	Default	e	%default
d'après	120	prep	[pred="d'après_____1<Obj:sa|sadv|sn>"]	d'après_____1	Default		%default
d'avec	120	prep	[pred="d'avec_____1<Obj:sa|sadv|sn>"]	d'avec_____1	Default		%default
d'avis d'	120	prep	[pred="d'avis de_____1<Obj:sadv|sinf>",@e]	d'avis de_____1	Default	e	%default
d'avis d'__prep	120	prep	[pred="d'avis de_____1<Obj:sadv|sinf>",@e]	d'avis de_____1	Default	e	%default
d'avis de	120	prep	[pred="d'avis de_____1<Obj:sadv|sinf>"]	d'avis de_____1	Default		%default
d'avis de__prep	120	prep	[pred="d'avis de_____1<Obj:sadv|sinf>"]	d'avis de_____1	Default		%default
d'entre	120	prep	[pred="d'entre_____1<Obj:sa|sadv|sn>"]	d'entre_____1	Default		%default
d'ici	120	prep	[pred="d'ici_____1<Obj:sa|sadv|sn>"]	d'ici_____1	Default		%default
d'ici à	120	prep	[pred="d'ici à_____1<Obj:sa|sadv|sinf|sn>"]	d'ici à_____1	Default		%default
d'ici à__prep	120	prep	[pred="d'ici à_____1<Obj:sa|sadv|sinf|sn>"]	d'ici à_____1	Default		%default
d'où	120	prep	[pred="d'où_____1<Obj:sn>"]	d'où_____1	Default		%default
dans	120	prep	[pred="dans_____1<Obj:sa|sadv|sn>",pcas = loc|dans]	dans_____1	Default		%default
dans l'espoir d'	120	prep	[pred="dans l'espoir de_____1<Obj:sadv|sinf>",@e]	dans l'espoir de_____1	Default	e	%default
dans l'espoir d'__prep	120	prep	[pred="dans l'espoir de_____1<Obj:sadv|sinf>",@e]	dans l'espoir de_____1	Default	e	%default
dans l'espoir de	120	prep	[pred="dans l'espoir de_____1<Obj:sadv|sinf>"]	dans l'espoir de_____1	Default		%default
dans l'espoir de__prep	120	prep	[pred="dans l'espoir de_____1<Obj:sadv|sinf>"]	dans l'espoir de_____1	Default		%default
dans l'intention d'	120	prep	[pred="dans l'intention de_____1<Obj:sadv|sinf>",@e]	dans l'intention de_____1	Default	e	%default
dans l'intention d'__prep	120	prep	[pred="dans l'intention de_____1<Obj:sadv|sinf>",@e]	dans l'intention de_____1	Default	e	%default
dans l'intention de	120	prep	[pred="dans l'intention de_____1<Obj:sadv|sinf>"]	dans l'intention de_____1	Default		%default
dans l'intention de__prep	120	prep	[pred="dans l'intention de_____1<Obj:sadv|sinf>"]	dans l'intention de_____1	Default		%default
dans le but d'	120	prep	[pred="dans le but de_____1<Obj:sinf>",@e]	dans le but de_____1	Default	e	%default
dans le but d'__prep	120	prep	[pred="dans le but de_____1<Obj:sinf>",@e]	dans le but de_____1	Default	e	%default
dans le but de	120	prep	[pred="dans le but de_____1<Obj:sinf>"]	dans le but de_____1	Default		%default
dans le but de__prep	120	prep	[pred="dans le but de_____1<Obj:sinf>"]	dans le but de_____1	Default		%default
de	120	prep	[pred="de_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = de]	de_____1	Default		%default
de chez	120	prep	[pred="de chez_____1<Obj:sa|sadv|sn>",pcas=de_chez]	de chez_____1	Default		%default
de derrière	120	prep	[pred="de derrière_____1<Obj:sa|sadv|sn>"]	de derrière_____1	Default		%default
de façon à	120	prep	[pred="de façon à_____1<Obj:sinf>"]	de façon à_____1	Default		%default
de l'ordre d'	120	prep	[pred="de l'ordre de_____1<Obj:sa|sadv|sn>",@e]	de l'ordre de_____1	Default	e	%default
de l'ordre d'__prep	120	prep	[pred="de l'ordre de_____1<Obj:sa|sadv|sn>",@e]	de l'ordre de_____1	Default	e	%default
de l'ordre de	120	prep	[pred="de l'ordre de_____1<Obj:sa|sadv|sn>"]	de l'ordre de_____1	Default		%default
de l'ordre de__prep	120	prep	[pred="de l'ordre de_____1<Obj:sa|sadv|sn>"]	de l'ordre de_____1	Default		%default
de manière à	120	prep	[pred="de manière à_____1<Obj:sadv|sinf>"]	de manière à_____1	Default		%default
de manière à__prep	120	prep	[pred="de manière à_____1<Obj:sadv|sinf>"]	de manière à_____1	Default		%default
de par	120	prep	[pred="de par_____1<Obj:sa|sadv|sn>"]	de par_____1	Default		%default
de part et d'autre d'	120	prep	[pred="de part et d'autre de_____1<Obj:sa|sadv|sn>",@e]	de part et d'autre de_____1	Default	e	%default
de part et d'autre d'__prep	120	prep	[pred="de part et d'autre de_____1<Obj:sa|sadv|sn>",@e]	de part et d'autre de_____1	Default	e	%default
de part et d'autre de	120	prep	[pred="de part et d'autre de_____1<Obj:sa|sadv|sn>"]	de part et d'autre de_____1	Default		%default
de part et d'autre de__prep	120	prep	[pred="de part et d'autre de_____1<Obj:sa|sadv|sn>"]	de part et d'autre de_____1	Default		%default
de près	120	prep	[pred="de près_____1<Obj:sa|sadv|sn>"]	de près_____1	Default		%default
de__prep	120	prep	[pred="de_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = de]	de_____1	Default		%default
depuis	120	prep	[pred="depuis_____1<Obj:sa|sadv|sn>",pcas=depuis]	depuis_____1	Default		%default
derrière	120	prep	[pred="derrière_____1<Obj:sa|sadv|sn>",pcas = loc]	derrière_____1	Default		%default
dessous	120	prep	[pred="dessous_____1<Obj:sa|sadv|sn>",pcas = loc]	dessous_____1	Default		%default
dessus	120	prep	[pred="dessus_____1<Obj:sa|sadv|sn>",pcas = loc]	dessus_____1	Default		%default
devant	120	prep	[pred="devant_____1<Obj:sa|sadv|sn>",pcas = loc|devant]	devant_____1	Default		%default
devers	120	prep	[pred="devers_____1<Obj:sa|sadv|sn>",pcas = loc]	devers_____1	Default		%default
dixit	120	prep	[pred="dixit_____1<Obj:sa|sadv|sn>"]	dixit_____1	Default		%default
du côté d'	120	prep	[pred="du côté de_____1<Obj:sa|sadv|sn>",pcas = loc|du_côté_de,@e]	du côté de_____1	Default	e	%default
du côté d'__prep	120	prep	[pred="du côté de_____1<Obj:sa|sadv|sn>",pcas = loc|du_côté_de,@e]	du côté de_____1	Default	e	%default
du côté de	120	prep	[pred="du côté de_____1<Obj:sa|sadv|sn>",pcas = loc|du_côté_de]	du côté de_____1	Default		%default
du côté de__prep	120	prep	[pred="du côté de_____1<Obj:sa|sadv|sn>",pcas = loc|du_côté_de]	du côté de_____1	Default		%default
du fait d'	120	prep	[pred="du fait de_____1<Obj:sn>",@e]	du fait de_____1	Default	e	%default
du fait d'__prep	120	prep	[pred="du fait de_____1<Obj:sn>",@e]	du fait de_____1	Default	e	%default
du fait de	120	prep	[pred="du fait de_____1<Obj:sn>"]	du fait de_____1	Default		%default
du fait de__prep	120	prep	[pred="du fait de_____1<Obj:sn>"]	du fait de_____1	Default		%default
durant	120	prep	[pred="durant_____1<Obj:sa|sadv|sn>"]	durant_____1	Default		%default
dès	120	prep	[pred="dès_____1<Obj:sa|sadv|sn>"]	dès_____1	Default		%default
dès avant	120	prep	[pred="dès avant_____1<Obj:sa|sadv|sn>"]	dès avant_____1	Default		%default
en	120	prep	[pred="en_____1<Obj:sa|sadv|sn>",pcas = loc|en]	en_____1	Default		%default
en absence d'	120	prep	[pred="en absence de_____1<Obj:sa|sadv|sn>",@e]	en absence de_____1	Default	e	%default
en absence d'__prep	120	prep	[pred="en absence de_____1<Obj:sa|sadv|sn>",@e]	en absence de_____1	Default	e	%default
en absence de	120	prep	[pred="en absence de_____1<Obj:sa|sadv|sn>"]	en absence de_____1	Default		%default
en absence de__prep	120	prep	[pred="en absence de_____1<Obj:sa|sadv|sn>"]	en absence de_____1	Default		%default
en accord avec	120	prep	[pred="en accord avec_____1<Obj:sa|sadv|sn>"]	en accord avec_____1	Default		%default
en admettant	120	prep	[pred="en admettant_____1<Obj:sn>"]	en admettant_____1	Default		%default
en amont d'	120	prep	[pred="en amont de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en amont de_____1	Default	e	%default
en amont d'__prep	120	prep	[pred="en amont de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en amont de_____1	Default	e	%default
en amont de	120	prep	[pred="en amont de_____1<Obj:sa|sadv|sn>",pcas = loc]	en amont de_____1	Default		%default
en amont de__prep	120	prep	[pred="en amont de_____1<Obj:sa|sadv|sn>",pcas = loc]	en amont de_____1	Default		%default
en arrière d'	120	prep	[pred="en arrière de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en arrière de_____1	Default	e	%default
en arrière d'__prep	120	prep	[pred="en arrière de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en arrière de_____1	Default	e	%default
en arrière de	120	prep	[pred="en arrière de_____1<Obj:sa|sadv|sn>",pcas = loc]	en arrière de_____1	Default		%default
en arrière de__prep	120	prep	[pred="en arrière de_____1<Obj:sa|sadv|sn>",pcas = loc]	en arrière de_____1	Default		%default
en aval d'	120	prep	[pred="en aval de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en aval de_____1	Default	e	%default
en aval d'__prep	120	prep	[pred="en aval de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en aval de_____1	Default	e	%default
en aval de	120	prep	[pred="en aval de_____1<Obj:sa|sadv|sn>",pcas = loc]	en aval de_____1	Default		%default
en aval de__prep	120	prep	[pred="en aval de_____1<Obj:sa|sadv|sn>",pcas = loc]	en aval de_____1	Default		%default
en avant d'	120	prep	[pred="en avant de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en avant de_____1	Default	e	%default
en avant d'__prep	120	prep	[pred="en avant de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en avant de_____1	Default	e	%default
en avant de	120	prep	[pred="en avant de_____1<Obj:sa|sadv|sn>",pcas = loc]	en avant de_____1	Default		%default
en avant de__prep	120	prep	[pred="en avant de_____1<Obj:sa|sadv|sn>",pcas = loc]	en avant de_____1	Default		%default
en cas d'	120	prep	[pred="en cas de_____1<Obj:sa|sadv|sn>",@e]	en cas de_____1	Default	e	%default
en cas d'__prep	120	prep	[pred="en cas de_____1<Obj:sa|sadv|sn>",@e]	en cas de_____1	Default	e	%default
en cas de	120	prep	[pred="en cas de_____1<Obj:sa|sadv|sn>"]	en cas de_____1	Default		%default
en cas de__prep	120	prep	[pred="en cas de_____1<Obj:sa|sadv|sn>"]	en cas de_____1	Default		%default
en contrebas d'	120	prep	[pred="en contrebas de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en contrebas de_____1	Default	e	%default
en contrebas d'__prep	120	prep	[pred="en contrebas de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en contrebas de_____1	Default	e	%default
en contrebas de	120	prep	[pred="en contrebas de_____1<Obj:sa|sadv|sn>",pcas = loc]	en contrebas de_____1	Default		%default
en contrebas de__prep	120	prep	[pred="en contrebas de_____1<Obj:sa|sadv|sn>",pcas = loc]	en contrebas de_____1	Default		%default
en dedans d'	120	prep	[pred="en dedans de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en dedans de_____1	Default	e	%default
en dedans d'__prep	120	prep	[pred="en dedans de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en dedans de_____1	Default	e	%default
en dedans de	120	prep	[pred="en dedans de_____1<Obj:sa|sadv|sn>",pcas = loc]	en dedans de_____1	Default		%default
en dedans de__prep	120	prep	[pred="en dedans de_____1<Obj:sa|sadv|sn>",pcas = loc]	en dedans de_____1	Default		%default
en dehors d'	120	prep	[pred="en dehors de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en dehors de_____1	Default	e	%default
en dehors d'__prep	120	prep	[pred="en dehors de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en dehors de_____1	Default	e	%default
en dehors de	120	prep	[pred="en dehors de_____1<Obj:sa|sadv|sn>",pcas = loc]	en dehors de_____1	Default		%default
en dehors de__prep	120	prep	[pred="en dehors de_____1<Obj:sa|sadv|sn>",pcas = loc]	en dehors de_____1	Default		%default
en delà d'	120	prep	[pred="en delà de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en delà de_____1	Default	e	%default
en delà d'__prep	120	prep	[pred="en delà de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en delà de_____1	Default	e	%default
en delà de	120	prep	[pred="en delà de_____1<Obj:sa|sadv|sn>",pcas = loc]	en delà de_____1	Default		%default
en delà de__prep	120	prep	[pred="en delà de_____1<Obj:sa|sadv|sn>",pcas = loc]	en delà de_____1	Default		%default
en dessous d'	120	prep	[pred="en dessous de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en dessous de_____1	Default	e	%default
en dessous d'__prep	120	prep	[pred="en dessous de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en dessous de_____1	Default	e	%default
en dessous de	120	prep	[pred="en dessous de_____1<Obj:sa|sadv|sn>",pcas = loc]	en dessous de_____1	Default		%default
en dessous de__prep	120	prep	[pred="en dessous de_____1<Obj:sa|sadv|sn>",pcas = loc]	en dessous de_____1	Default		%default
en deçà d'	120	prep	[pred="en-deçà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en-deçà de_____1	Default	e	%default
en deçà de	120	prep	[pred="en-deçà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en-deçà de_____1	Default		%default
en début d'	120	prep	[pred="en début de_____1<Obj:sa|sadv|sn>",@e]	en début de_____1	Default	e	%default
en début d'__prep	120	prep	[pred="en début de_____1<Obj:sa|sadv|sn>",@e]	en début de_____1	Default	e	%default
en début de	120	prep	[pred="en début de_____1<Obj:sa|sadv|sn>"]	en début de_____1	Default		%default
en début de__prep	120	prep	[pred="en début de_____1<Obj:sa|sadv|sn>"]	en début de_____1	Default		%default
en dépit d'	120	prep	[pred="en dépit de_____1<Obj:sa|sadv|sinf|sn>",@e]	en dépit de_____1	Default	e	%default
en dépit d'__prep	120	prep	[pred="en dépit de_____1<Obj:sa|sadv|sinf|sn>",@e]	en dépit de_____1	Default	e	%default
en dépit de	120	prep	[pred="en dépit de_____1<Obj:sa|sadv|sinf|sn>"]	en dépit de_____1	Default		%default
en dépit de__prep	120	prep	[pred="en dépit de_____1<Obj:sa|sadv|sinf|sn>"]	en dépit de_____1	Default		%default
en dépit du fait d'	120	prep	[pred="en dépit du fait de_____1<Obj:sn>",@e]	en dépit du fait de_____1	Default	e	%default
en dépit du fait d'__prep	120	prep	[pred="en dépit du fait de_____1<Obj:sn>",@e]	en dépit du fait de_____1	Default	e	%default
en dépit du fait de	120	prep	[pred="en dépit du fait de_____1<Obj:sn>"]	en dépit du fait de_____1	Default		%default
en dépit du fait de__prep	120	prep	[pred="en dépit du fait de_____1<Obj:sn>"]	en dépit du fait de_____1	Default		%default
en face d'	120	prep	[pred="en face de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en face de_____1	Default	e	%default
en face d'__prep	120	prep	[pred="en face de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en face de_____1	Default	e	%default
en face de	120	prep	[pred="en face de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en face de_____1	Default		%default
en face de__prep	120	prep	[pred="en face de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en face de_____1	Default		%default
en fait d'	120	prep	[pred="en fait de_____1<Obj:sa|sadv|sinf|sn>",@e]	en fait de_____1	Default	e	%default
en fait d'__prep	120	prep	[pred="en fait de_____1<Obj:sa|sadv|sinf|sn>",@e]	en fait de_____1	Default	e	%default
en fait de	120	prep	[pred="en fait de_____1<Obj:sa|sadv|sinf|sn>"]	en fait de_____1	Default		%default
en fait de__prep	120	prep	[pred="en fait de_____1<Obj:sa|sadv|sinf|sn>"]	en fait de_____1	Default		%default
en faveur d'	120	prep	[pred="en faveur de_____1<Obj:sa|sadv|sinf|sn>",pcas= en_faveur_de,@e]	en faveur de_____1	Default	e	%default
en faveur d'__prep	120	prep	[pred="en faveur de_____1<Obj:sa|sadv|sinf|sn>",pcas= en_faveur_de,@e]	en faveur de_____1	Default	e	%default
en faveur de	120	prep	[pred="en faveur de_____1<Obj:sa|sadv|sinf|sn>",pcas= en_faveur_de]	en faveur de_____1	Default		%default
en faveur de__prep	120	prep	[pred="en faveur de_____1<Obj:sa|sadv|sinf|sn>",pcas= en_faveur_de]	en faveur de_____1	Default		%default
en fin d'	120	prep	[pred="en fin de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en fin de_____1	Default	e	%default
en fin d'__prep	120	prep	[pred="en fin de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en fin de_____1	Default	e	%default
en fin de	120	prep	[pred="en fin de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en fin de_____1	Default		%default
en fin de__prep	120	prep	[pred="en fin de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en fin de_____1	Default		%default
en guise d'	120	prep	[pred="en guise de_____1<Obj:sa|sadv|sn>",@e]	en guise de_____1	Default	e	%default
en guise d'__prep	120	prep	[pred="en guise de_____1<Obj:sa|sadv|sn>",@e]	en guise de_____1	Default	e	%default
en guise de	120	prep	[pred="en guise de_____1<Obj:sa|sadv|sn>"]	en guise de_____1	Default		%default
en guise de__prep	120	prep	[pred="en guise de_____1<Obj:sa|sadv|sn>"]	en guise de_____1	Default		%default
en haut d'	120	prep	[pred="en haut de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en haut de_____1	Default	e	%default
en haut d'__prep	120	prep	[pred="en haut de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en haut de_____1	Default	e	%default
en haut de	120	prep	[pred="en haut de_____1<Obj:sa|sadv|sn>",pcas = loc]	en haut de_____1	Default		%default
en haut de__prep	120	prep	[pred="en haut de_____1<Obj:sa|sadv|sn>",pcas = loc]	en haut de_____1	Default		%default
en hommage à	120	prep	[pred="en hommage à_____1<Obj:sa|sadv|sn>"]	en hommage à_____1	Default		%default
en hommage à__prep	120	prep	[pred="en hommage à_____1<Obj:sa|sadv|sn>"]	en hommage à_____1	Default		%default
en instance d'	120	prep	[pred="en instance de_____1<Obj:sa|sadv|sn>",@e]	en instance de_____1	Default	e	%default
en instance d'__prep	120	prep	[pred="en instance de_____1<Obj:sa|sadv|sn>",@e]	en instance de_____1	Default	e	%default
en instance de	120	prep	[pred="en instance de_____1<Obj:sa|sadv|sn>"]	en instance de_____1	Default		%default
en instance de__prep	120	prep	[pred="en instance de_____1<Obj:sa|sadv|sn>"]	en instance de_____1	Default		%default
en manière d'	120	prep	[pred="en manière de_____1<Obj:sa|sadv|sn>",@e]	en manière de_____1	Default	e	%default
en manière d'__prep	120	prep	[pred="en manière de_____1<Obj:sa|sadv|sn>",@e]	en manière de_____1	Default	e	%default
en manière de	120	prep	[pred="en manière de_____1<Obj:sa|sadv|sn>"]	en manière de_____1	Default		%default
en manière de__prep	120	prep	[pred="en manière de_____1<Obj:sa|sadv|sn>"]	en manière de_____1	Default		%default
en manque d'	120	prep	[pred="en manque de_____1<Obj:sa|sadv|sn>",@e]	en manque de_____1	Default	e	%default
en manque d'__prep	120	prep	[pred="en manque de_____1<Obj:sa|sadv|sn>",@e]	en manque de_____1	Default	e	%default
en manque de	120	prep	[pred="en manque de_____1<Obj:sa|sadv|sn>"]	en manque de_____1	Default		%default
en manque de__prep	120	prep	[pred="en manque de_____1<Obj:sa|sadv|sn>"]	en manque de_____1	Default		%default
en matière d'	120	prep	[pred="en matière de_____1<Obj:sa|sadv|sn>",@e]	en matière de_____1	Default	e	%default
en matière d'__prep	120	prep	[pred="en matière de_____1<Obj:sa|sadv|sn>",@e]	en matière de_____1	Default	e	%default
en matière de	120	prep	[pred="en matière de_____1<Obj:sa|sadv|sn>"]	en matière de_____1	Default		%default
en matière de__prep	120	prep	[pred="en matière de_____1<Obj:sa|sadv|sn>"]	en matière de_____1	Default		%default
en milieu d'	120	prep	[pred="en milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en milieu de_____1	Default	e	%default
en milieu d'__prep	120	prep	[pred="en milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en milieu de_____1	Default	e	%default
en milieu de	120	prep	[pred="en milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en milieu de_____1	Default		%default
en milieu de__prep	120	prep	[pred="en milieu de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en milieu de_____1	Default		%default
en outre d'	120	prep	[pred="en outre de_____1<Obj:sa|sadv|sn>",@e]	en outre de_____1	Default	e	%default
en outre d'__prep	120	prep	[pred="en outre de_____1<Obj:sa|sadv|sn>",@e]	en outre de_____1	Default	e	%default
en outre de	120	prep	[pred="en outre de_____1<Obj:sa|sadv|sn>"]	en outre de_____1	Default		%default
en outre de__prep	120	prep	[pred="en outre de_____1<Obj:sa|sadv|sn>"]	en outre de_____1	Default		%default
en perte d'	120	prep	[pred="en perte de_____1<Obj:sa|sadv|sn>",@e]	en perte de_____1	Default	e	%default
en perte d'__prep	120	prep	[pred="en perte de_____1<Obj:sa|sadv|sn>",@e]	en perte de_____1	Default	e	%default
en perte de	120	prep	[pred="en perte de_____1<Obj:sa|sadv|sn>"]	en perte de_____1	Default		%default
en perte de__prep	120	prep	[pred="en perte de_____1<Obj:sa|sadv|sn>"]	en perte de_____1	Default		%default
en plein	120	prep	[pred="en plein_____1<Obj:sa|sadv|sn>",pcas = loc]	en plein_____1	Default		%default
en plein centre d'	120	prep	[pred="en plein centre de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en plein centre de_____1	Default	e	%default
en plein centre d'__prep	120	prep	[pred="en plein centre de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en plein centre de_____1	Default	e	%default
en plein centre de	120	prep	[pred="en plein centre de_____1<Obj:sa|sadv|sn>",pcas = loc]	en plein centre de_____1	Default		%default
en plein centre de__prep	120	prep	[pred="en plein centre de_____1<Obj:sa|sadv|sn>",pcas = loc]	en plein centre de_____1	Default		%default
en plein dans	120	prep	[pred="en plein dans_____1<Obj:sa|sadv|sn>",pcas = loc]	en plein dans_____1	Default		%default
en plein sur	120	prep	[pred="en plein sur_____1<Obj:sa|sadv|sn>",pcas = loc]	en plein sur_____1	Default		%default
en plus d'	120	prep	[pred="en plus de_____1<Obj:sa|sadv|sinf|sn>",@e]	en plus de_____1	Default	e	%default
en plus d'__prep	120	prep	[pred="en plus de_____1<Obj:sa|sadv|sinf|sn>",@e]	en plus de_____1	Default	e	%default
en plus de	120	prep	[pred="en plus de_____1<Obj:sa|sadv|sinf|sn>"]	en plus de_____1	Default		%default
en plus de__prep	120	prep	[pred="en plus de_____1<Obj:sa|sadv|sinf|sn>"]	en plus de_____1	Default		%default
en proie à	120	prep	[pred="en proie à_____1<Obj:sa|sadv|sn>"]	en proie à_____1	Default		%default
en proie à__prep	120	prep	[pred="en proie à_____1<Obj:sa|sadv|sn>"]	en proie à_____1	Default		%default
en quête d'	120	prep	[pred="en quête de_____1<Obj:sa|sadv|sn>",@e]	en quête de_____1	Default	e	%default
en quête d'__prep	120	prep	[pred="en quête de_____1<Obj:sa|sadv|sn>",@e]	en quête de_____1	Default	e	%default
en quête de	120	prep	[pred="en quête de_____1<Obj:sa|sadv|sn>"]	en quête de_____1	Default		%default
en quête de__prep	120	prep	[pred="en quête de_____1<Obj:sa|sadv|sn>"]	en quête de_____1	Default		%default
en raison d'	120	prep	[pred="en raison de_____1<Obj:sa|sadv|sn>",@e]	en raison de_____1	Default	e	%default
en raison d'__prep	120	prep	[pred="en raison de_____1<Obj:sa|sadv|sn>",@e]	en raison de_____1	Default	e	%default
en raison de	120	prep	[pred="en raison de_____1<Obj:sa|sadv|sn>"]	en raison de_____1	Default		%default
en raison de__prep	120	prep	[pred="en raison de_____1<Obj:sa|sadv|sn>"]	en raison de_____1	Default		%default
en regard d'	120	prep	[pred="en regard de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en regard de_____1	Default	e	%default
en regard d'__prep	120	prep	[pred="en regard de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	en regard de_____1	Default	e	%default
en regard de	120	prep	[pred="en regard de_____1<Obj:sa|sadv|sn>",pcas = loc]	en regard de_____1	Default		%default
en regard de__prep	120	prep	[pred="en regard de_____1<Obj:sa|sadv|sn>",pcas = loc]	en regard de_____1	Default		%default
en relation avec	120	prep	[pred="en relation avec_____1<Obj:sa|sadv|sn>"]	en relation avec_____1	Default		%default
en règlement d'	120	prep	[pred="en règlement de_____1<Obj:sa|sadv|sn>",@e]	en règlement de_____1	Default	e	%default
en règlement d'__prep	120	prep	[pred="en règlement de_____1<Obj:sa|sadv|sn>",@e]	en règlement de_____1	Default	e	%default
en règlement de	120	prep	[pred="en règlement de_____1<Obj:sa|sadv|sn>"]	en règlement de_____1	Default		%default
en règlement de__prep	120	prep	[pred="en règlement de_____1<Obj:sa|sadv|sn>"]	en règlement de_____1	Default		%default
en signe d'	120	prep	[pred="en signe de_____1<Obj:sa|sadv|sn>",@e]	en signe de_____1	Default	e	%default
en signe d'__prep	120	prep	[pred="en signe de_____1<Obj:sa|sadv|sn>",@e]	en signe de_____1	Default	e	%default
en signe de	120	prep	[pred="en signe de_____1<Obj:sa|sadv|sn>"]	en signe de_____1	Default		%default
en signe de__prep	120	prep	[pred="en signe de_____1<Obj:sa|sadv|sn>"]	en signe de_____1	Default		%default
en supposant	120	prep	[pred="en supposant_____1<Obj:sn>"]	en supposant_____1	Default		%default
en tant qu'	120	prep	[pred="en tant que_____1<Obj:sa|sadv|sn>",pcas = comme,@e]	en tant que_____1	Default	e	%default
en tant qu'__prep	120	prep	[pred="en tant que_____1<Obj:sa|sadv|sn>",pcas = comme,@e]	en tant que_____1	Default	e	%default
en tant que	120	prep	[pred="en tant que_____1<Obj:sa|sadv|sn>",pcas = comme]	en tant que_____1	Default		%default
en tant que__prep	120	prep	[pred="en tant que_____1<Obj:sa|sadv|sn>",pcas = comme]	en tant que_____1	Default		%default
en terme d'	120	prep	[pred="en terme de_____1<Obj:sa|sadv|sn>",@e]	en terme de_____1	Default	e	%default
en terme d'__prep	120	prep	[pred="en terme de_____1<Obj:sa|sadv|sn>",@e]	en terme de_____1	Default	e	%default
en terme de	120	prep	[pred="en terme de_____1<Obj:sa|sadv|sn>"]	en terme de_____1	Default		%default
en terme de__prep	120	prep	[pred="en terme de_____1<Obj:sa|sadv|sn>"]	en terme de_____1	Default		%default
en termes d'	120	prep	[pred="en termes de_____1<Obj:sa|sadv|sn>",@e]	en termes de_____1	Default	e	%default
en termes d'__prep	120	prep	[pred="en termes de_____1<Obj:sa|sadv|sn>",@e]	en termes de_____1	Default	e	%default
en termes de	120	prep	[pred="en termes de_____1<Obj:sa|sadv|sn>"]	en termes de_____1	Default		%default
en termes de__prep	120	prep	[pred="en termes de_____1<Obj:sa|sadv|sn>"]	en termes de_____1	Default		%default
en train d'	120	prep	[pred="en train de_____1<Obj:sadv|sinf>",@e]	en train de_____1	Default	e	%default
en train d'__prep	120	prep	[pred="en train de_____1<Obj:sadv|sinf>",@e]	en train de_____1	Default	e	%default
en train de	120	prep	[pred="en train de_____1<Obj:sadv|sinf>"]	en train de_____1	Default		%default
en train de__prep	120	prep	[pred="en train de_____1<Obj:sadv|sinf>"]	en train de_____1	Default		%default
en vertu d'	120	prep	[pred="en vertu de_____1<Obj:sa|sadv|sn>",@e]	en vertu de_____1	Default	e	%default
en vertu d'__prep	120	prep	[pred="en vertu de_____1<Obj:sa|sadv|sn>",@e]	en vertu de_____1	Default	e	%default
en vertu de	120	prep	[pred="en vertu de_____1<Obj:sa|sadv|sn>"]	en vertu de_____1	Default		%default
en vertu de__prep	120	prep	[pred="en vertu de_____1<Obj:sa|sadv|sn>"]	en vertu de_____1	Default		%default
en voie d'	120	prep	[pred="en voie de_____1<Obj:sa|sadv|sinf|sn>",@e]	en voie de_____1	Default	e	%default
en voie d'__prep	120	prep	[pred="en voie de_____1<Obj:sa|sadv|sinf|sn>",@e]	en voie de_____1	Default	e	%default
en voie de	120	prep	[pred="en voie de_____1<Obj:sa|sadv|sinf|sn>"]	en voie de_____1	Default		%default
en voie de__prep	120	prep	[pred="en voie de_____1<Obj:sa|sadv|sinf|sn>"]	en voie de_____1	Default		%default
en vue d'	120	prep	[pred="en vue de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en vue de_____1	Default	e	%default
en vue de	120	prep	[pred="en vue de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en vue de_____1	Default		%default
en âge d'	120	prep	[pred="en âge de_____1<Obj:sa|sadv|sinf|sn>",@e]	en âge de_____1	Default	e	%default
en âge d'__prep	120	prep	[pred="en âge de_____1<Obj:sa|sadv|sinf|sn>",@e]	en âge de_____1	Default	e	%default
en âge de	120	prep	[pred="en âge de_____1<Obj:sa|sadv|sinf|sn>"]	en âge de_____1	Default		%default
en âge de__prep	120	prep	[pred="en âge de_____1<Obj:sa|sadv|sinf|sn>"]	en âge de_____1	Default		%default
en-deçà d'	120	prep	[pred="en-deçà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	en-deçà de_____1	Default	e	%default
en-deçà de	120	prep	[pred="en-deçà de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	en-deçà de_____1	Default		%default
en__prep	120	prep	[pred="en_____1<Obj:sa|sadv|sn>",pcas = loc|en]	en_____1	Default		%default
endéans	120	prep	[pred="endéans_____1<Obj:sa|sadv|sn>",pcas = loc]	endéans_____1	Default		%default
entre	120	prep	[pred="entre_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|entre]	entre_____1	Default		%default
envers	120	prep	[pred="envers_____1<Obj:sa|sadv|sn>",pcas= envers]	envers_____1	Default		%default
eu égard à	120	prep	[pred="eu égard à_____1<Obj:sa|sadv|sn>"]	eu égard à_____1	Default		%default
eu égard à__prep	120	prep	[pred="eu égard à_____1<Obj:sa|sadv|sn>"]	eu égard à_____1	Default		%default
excepté	120	prep	[pred="excepté_____1<Obj:sa|sadv|sinf|sn>"]	excepté_____1	Default		%default
face à	120	prep	[pred="face à_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|face_à]	face à_____1	Default		%default
face à__prep	120	prep	[pred="face à_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|face_à]	face à_____1	Default		%default
faute d'	120	prep	[pred="faute de_____1<Obj:sa|sadv|sinf|sn>",@e]	faute de_____1	Default	e	%default
faute d'__prep	120	prep	[pred="faute de_____1<Obj:sa|sadv|sinf|sn>",@e]	faute de_____1	Default	e	%default
faute de	120	prep	[pred="faute de_____1<Obj:sa|sadv|sinf|sn>"]	faute de_____1	Default		%default
faute de__prep	120	prep	[pred="faute de_____1<Obj:sa|sadv|sinf|sn>"]	faute de_____1	Default		%default
grâce à	120	prep	[pred="grâce à_____1<Obj:sa|sadv|sn>",mod_kind = causal]	grâce à_____1	Default		%default
grâce à__prep	120	prep	[pred="grâce à_____1<Obj:sa|sadv|sn>",mod_kind = causal]	grâce à_____1	Default		%default
histoire d'	120	prep	[pred="histoire de_____1<Obj:sn|sinf>",@e]	histoire de_____1	Default	e	%default
histoire d'__prep	120	prep	[pred="histoire de_____1<Obj:sn|sinf>",@e]	histoire de_____1	Default	e	%default
histoire de	120	prep	[pred="histoire de_____1<Obj:sn|sinf>"]	histoire de_____1	Default		%default
histoire de__prep	120	prep	[pred="histoire de_____1<Obj:sn|sinf>"]	histoire de_____1	Default		%default
hormis	120	prep	[pred="hormis_____1<Obj:sa|sadv|sinf|sn>"]	hormis_____1	Default		%default
hors	120	prep	[pred="hors_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	hors_____1	Default		%default
hors d'	120	prep	[pred="hors de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	hors de_____1	Default	e	%default
hors d'__prep	120	prep	[pred="hors de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	hors de_____1	Default	e	%default
hors de	120	prep	[pred="hors de_____1<Obj:sa|sadv|sn>",pcas = loc]	hors de_____1	Default		%default
hors de__prep	120	prep	[pred="hors de_____1<Obj:sa|sadv|sn>",pcas = loc]	hors de_____1	Default		%default
il y a	120	prep	[pred="il y a_____1<Obj:sa|sadv|sn>"]	il y a_____1	Default		%default
jusqu'	100	adv	[pred="jusque_____1",adv_kind=modprep,cat=adv,@e]	jusque_____1	Default	e	%default
jusqu'	120	prep	[pred="jusque_____2<Obj:sa|sadv|sn>",pcas=jusque,@e]	jusque_____2	Default	e	%default
jusqu'__prep	100	adv	[pred="jusque_____1",adv_kind=modprep,cat=adv,@e]	jusque_____1	Default	e	%default
jusqu'__prep	120	prep	[pred="jusque_____2<Obj:sa|sadv|sn>",pcas=jusque,@e]	jusque_____2	Default	e	%default
jusqu'en	120	prep	[pred="jusqu'en_____1<Obj:sa|sadv|sn>",pcas = loc]	jusqu'en_____1	Default		%default
jusqu'où	120	prep	[pred="jusqu'où_____1<Obj:sadv|sinf>"]	jusqu'où_____1	Default		%default
jusqu'à	120	prep	[pred="jusqu'à_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	jusqu'à_____1	Default		%default
jusque	100	adv	[pred="jusque_____1",adv_kind=modprep,cat=adv]	jusque_____1	Default		%default
jusque	120	prep	[pred="jusque_____2<Obj:sa|sadv|sn>",pcas=jusque]	jusque_____2	Default		%default
jusque__prep	100	adv	[pred="jusque_____1",adv_kind=modprep,cat=adv]	jusque_____1	Default		%default
jusque__prep	120	prep	[pred="jusque_____2<Obj:sa|sadv|sn>",pcas=jusque]	jusque_____2	Default		%default
le long d'	120	prep	[pred="le long de_____1<Obj:sa|sadv|sn>",pcas = loc|le_long_de,@e]	le long de_____1	Default	e	%default
le long d'__prep	120	prep	[pred="le long de_____1<Obj:sa|sadv|sn>",pcas = loc|le_long_de,@e]	le long de_____1	Default	e	%default
le long de	120	prep	[pred="le long de_____1<Obj:sa|sadv|sn>",pcas = loc|le_long_de]	le long de_____1	Default		%default
le long de__prep	120	prep	[pred="le long de_____1<Obj:sa|sadv|sn>",pcas = loc|le_long_de]	le long de_____1	Default		%default
le temps d'	120	prep	[pred="le temps de_____1<Obj:sn|sinf>",@e]	le temps de_____1	Default	e	%default
le temps d'__prep	120	prep	[pred="le temps de_____1<Obj:sn|sinf>",@e]	le temps de_____1	Default	e	%default
le temps de	120	prep	[pred="le temps de_____1<Obj:sn|sinf>"]	le temps de_____1	Default		%default
le temps de__prep	120	prep	[pred="le temps de_____1<Obj:sn|sinf>"]	le temps de_____1	Default		%default
lez	120	prep	[pred="lès_____1<Obj:sa|sadv|sn>",pcas = loc]	lès_____1	Default		%default
lors d'	120	prep	[pred="lors de_____1<Obj:sa|sadv|sn>",@e]	lors de_____1	Default	e	%default
lors d'__prep	120	prep	[pred="lors de_____1<Obj:sa|sadv|sn>",@e]	lors de_____1	Default	e	%default
lors de	120	prep	[pred="lors de_____1<Obj:sa|sadv|sn>"]	lors de_____1	Default		%default
lors de__prep	120	prep	[pred="lors de_____1<Obj:sa|sadv|sn>"]	lors de_____1	Default		%default
lès	120	prep	[pred="lès_____1<Obj:sa|sadv|sn>",pcas = loc]	lès_____1	Default		%default
malgré	120	prep	[pred="malgré_____1<Obj:sa|sadv|sn>"]	malgré_____1	Default		%default
mis à part	120	prep	[pred="mis à part_____1<Obj:sn>"]	mis à part_____1	Default		%default
moyennant	120	prep	[pred="moyennant_____1<Obj:sa|sadv|sn>"]	moyennant_____1	Default		%default
n'en déplaise à	120	prep	[pred="n'en déplaise à_____1<Obj:sn>"]	n'en déplaise à_____1	Default		%default
non sans	120	prep	[pred="non sans_____1<Obj:sa|sadv|sinf|sn>"]	non sans_____1	Default		%default
nonobstant	120	prep	[pred="nonobstant_____1<Obj:sa|sadv|sinf|sn>"]	nonobstant_____1	Default		%default
outre	120	prep	[pred="outre_____1<Obj:sa|sadv|sinf|sn>"]	outre_____1	Default		%default
par	120	prep	[pred="par_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|par]	par_____1	Default		%default
par delà	120	prep	[pred="par delà_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	par delà_____1	Default		%default
par delà	120	prep	[pred="par-delà_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	par-delà_____1	Default		%default
par devers	120	prep	[pred="par devers_____1<Obj:sa|sadv|sn>"]	par devers_____1	Default		%default
par l'entremise d'	120	prep	[pred="par l'entremise de_____1<Obj:sa|sadv|sn>",@e]	par l'entremise de_____1	Default	e	%default
par l'entremise d'__prep	120	prep	[pred="par l'entremise de_____1<Obj:sa|sadv|sn>",@e]	par l'entremise de_____1	Default	e	%default
par l'entremise de	120	prep	[pred="par l'entremise de_____1<Obj:sa|sadv|sn>"]	par l'entremise de_____1	Default		%default
par l'entremise de__prep	120	prep	[pred="par l'entremise de_____1<Obj:sa|sadv|sn>"]	par l'entremise de_____1	Default		%default
par le fait d'	120	prep	[pred="par le fait de_____1<Obj:sn>",@e]	par le fait de_____1	Default	e	%default
par le fait d'__prep	120	prep	[pred="par le fait de_____1<Obj:sn>",@e]	par le fait de_____1	Default	e	%default
par le fait de	120	prep	[pred="par le fait de_____1<Obj:sn>"]	par le fait de_____1	Default		%default
par le fait de__prep	120	prep	[pred="par le fait de_____1<Obj:sn>"]	par le fait de_____1	Default		%default
par rapport à	120	prep	[pred="par rapport à_____1<Obj:sa|sadv|sinf|sn>"]	par rapport à_____1	Default		%default
par rapport à__prep	120	prep	[pred="par rapport à_____1<Obj:sa|sadv|sinf|sn>"]	par rapport à_____1	Default		%default
par égard pour	120	prep	[pred="par égard pour_____1<Obj:sa|sadv|sn>"]	par égard pour_____1	Default		%default
par égard à	120	prep	[pred="par égard à_____1<Obj:sa|sadv|sn>"]	par égard à_____1	Default		%default
par égard à__prep	120	prep	[pred="par égard à_____1<Obj:sa|sadv|sn>"]	par égard à_____1	Default		%default
par-delà	120	prep	[pred="par-delà_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	par-delà_____1	Default		%default
par-dessous	120	prep	[pred="par-dessous_____1<Obj:sa|sadv|sn>",pcas = loc]	par-dessous_____1	Default		%default
par-dessus	120	prep	[pred="par-dessus_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	par-dessus_____1	Default		%default
parmi	120	prep	[pred="parmi_____1<Obj:sa|sadv|sn>",pcas = loc|parmi]	parmi_____1	Default		%default
passé	120	prep	[pred="passé_____1<Obj:sa|sadv|sn>"]	passé_____1	Default		%default
pdt	120	prep	[pred="pendant_____1<Obj:sa|sadv|sn>"]	pendant_____1	Default		%default
pendant	120	prep	[pred="pendant_____1<Obj:sa|sadv|sn>"]	pendant_____1	Default		%default
peu d'	120	prep	[pred="peu de_____1<Obj:sa|sadv|sn>",@e]	peu de_____1	Default	e	%default
peu d'__prep	120	prep	[pred="peu de_____1<Obj:sa|sadv|sn>",@e]	peu de_____1	Default	e	%default
peu de	120	prep	[pred="peu de_____1<Obj:sa|sadv|sn>"]	peu de_____1	Default		%default
peu de__prep	120	prep	[pred="peu de_____1<Obj:sa|sadv|sn>"]	peu de_____1	Default		%default
plein d'	120	prep	[pred="plein de_____1<Obj:sa|sadv|sn>",@e]	plein de_____1	Default	e	%default
plein d'__prep	120	prep	[pred="plein de_____1<Obj:sa|sadv|sn>",@e]	plein de_____1	Default	e	%default
plein de	120	prep	[pred="plein de_____1<Obj:sa|sadv|sn>"]	plein de_____1	Default		%default
plein de__prep	120	prep	[pred="plein de_____1<Obj:sa|sadv|sn>"]	plein de_____1	Default		%default
plutôt que	120	prep	[pred="plutôt que_____1<Obj:de-sinf|sinf>"]	plutôt que_____1	Default		%default
plutôt que d'	120	prep	[pred="plutôt que de_____1<Obj:sadv|sinf>",@e]	plutôt que de_____1	Default	e	%default
plutôt que d'__prep	120	prep	[pred="plutôt que de_____1<Obj:sadv|sinf>",@e]	plutôt que de_____1	Default	e	%default
plutôt que de	120	prep	[pred="plutôt que de_____1<Obj:sadv|sinf>"]	plutôt que de_____1	Default		%default
plutôt que de__prep	120	prep	[pred="plutôt que de_____1<Obj:sadv|sinf>"]	plutôt que de_____1	Default		%default
pour	120	prep	[pred="pour_____1<Obj:sa|sadv|sinf|sn>",pcas = pour]	pour_____1	Default		%default
pour cause d'	120	prep	[pred="pour cause de_____1<Obj:sa|sadv|sn>",@e]	pour cause de_____1	Default	e	%default
pour cause d'__prep	120	prep	[pred="pour cause de_____1<Obj:sa|sadv|sn>",@e]	pour cause de_____1	Default	e	%default
pour cause de	120	prep	[pred="pour cause de_____1<Obj:sa|sadv|sn>"]	pour cause de_____1	Default		%default
pour cause de__prep	120	prep	[pred="pour cause de_____1<Obj:sa|sadv|sn>"]	pour cause de_____1	Default		%default
pour comble d'	120	prep	[pred="pour comble de_____1<Obj:sa|sadv|sn>",@e]	pour comble de_____1	Default	e	%default
pour comble d'__prep	120	prep	[pred="pour comble de_____1<Obj:sa|sadv|sn>",@e]	pour comble de_____1	Default	e	%default
pour comble de	120	prep	[pred="pour comble de_____1<Obj:sa|sadv|sn>"]	pour comble de_____1	Default		%default
pour comble de__prep	120	prep	[pred="pour comble de_____1<Obj:sa|sadv|sn>"]	pour comble de_____1	Default		%default
pourvu d'	120	prep	[pred="pourvu de_____1<Obj:sinf>",@e]	pourvu de_____1	Default	e	%default
pourvu d'__prep	120	prep	[pred="pourvu de_____1<Obj:sinf>",@e]	pourvu de_____1	Default	e	%default
pourvu de	120	prep	[pred="pourvu de_____1<Obj:sinf>"]	pourvu de_____1	Default		%default
pourvu de__prep	120	prep	[pred="pourvu de_____1<Obj:sinf>"]	pourvu de_____1	Default		%default
pr	120	prep	[pred="pour_____1<Obj:sa|sadv|sinf|sn>",pcas = pour]	pour_____1	Default		%default
preuve d'	120	prep	[pred="preuve de_____1<Obj:sn>",@e]	preuve de_____1	Default	e	%default
preuve d'__prep	120	prep	[pred="preuve de_____1<Obj:sn>",@e]	preuve de_____1	Default	e	%default
preuve de	120	prep	[pred="preuve de_____1<Obj:sn>"]	preuve de_____1	Default		%default
preuve de__prep	120	prep	[pred="preuve de_____1<Obj:sn>"]	preuve de_____1	Default		%default
près d'	120	prep	[pred="près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	près de_____1	Default	e	%default
près d'__prep	120	prep	[pred="près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	près de_____1	Default	e	%default
près de	120	prep	[pred="près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	près de_____1	Default		%default
près de__prep	120	prep	[pred="près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	près de_____1	Default		%default
quant à	120	prep	[pred="quant à_____1<Obj:sa|sadv|sinf|sn>"]	quant à_____1	Default		%default
quant à__prep	120	prep	[pred="quant à_____1<Obj:sa|sadv|sinf|sn>"]	quant à_____1	Default		%default
quitte à	120	prep	[pred="quitte à_____1<Obj:sinf>"]	quitte à_____1	Default		%default
s'agissant	120	prep	[pred="s'agissant_____1<Objde:de-sn|de-sinf>"]	s'agissant_____1	Default		%default
sachant	120	prep	[pred="sachant_____1<Obj:sn>"]	sachant_____1	Default		%default
sans	120	prep	[pred="sans_____1<Obj:sa|sadv|sinf|sn>",pcas= sans]	sans_____1	Default		%default
sans compter	120	prep	[pred="sans compter_____1<Obj:sn>"]	sans compter_____1	Default		%default
sans oublier	120	prep	[pred="sans oublier_____1<Obj:sn>"]	sans oublier_____1	Default		%default
sauf	120	prep	[pred="sauf_____1<Obj:sa|sadv|sn>"]	sauf_____1	Default		%default
sauf à	120	prep	[pred="sauf à_____1<Obj:sadv|sinf>"]	sauf à_____1	Default		%default
sauf à__prep	120	prep	[pred="sauf à_____1<Obj:sadv|sinf>"]	sauf à_____1	Default		%default
selon	120	prep	[pred="selon_____1<Obj:sa|sadv|sn>",pcas= selon]	selon_____1	Default		%default
si ce n'est	120	prep	[pred="si ce n'est_____1<Obj:sn>"]	si ce n'est_____1	Default		%default
si près d'	120	prep	[pred="si près de_____1<Obj:sa|sadv|sinf|sn>",@e]	si près de_____1	Default	e	%default
si près d'__prep	120	prep	[pred="si près de_____1<Obj:sa|sadv|sinf|sn>",@e]	si près de_____1	Default	e	%default
si près de	120	prep	[pred="si près de_____1<Obj:sa|sadv|sinf|sn>"]	si près de_____1	Default		%default
si près de__prep	120	prep	[pred="si près de_____1<Obj:sa|sadv|sinf|sn>"]	si près de_____1	Default		%default
sitôt	120	prep	[pred="sitôt_____1<Obj:sn>"]	sitôt_____1	Default		%default
sous	120	prep	[pred="sous_____1<Obj:sa|sadv|sn>",pcas = loc|sous]	sous_____1	Default		%default
sous l'effet d'	120	prep	[pred="sous l'effet de_____1<Obj:sa|sadv|sn>",@e]	sous l'effet de_____1	Default	e	%default
sous l'effet d'__prep	120	prep	[pred="sous l'effet de_____1<Obj:sa|sadv|sn>",@e]	sous l'effet de_____1	Default	e	%default
sous l'effet de	120	prep	[pred="sous l'effet de_____1<Obj:sa|sadv|sn>"]	sous l'effet de_____1	Default		%default
sous l'effet de__prep	120	prep	[pred="sous l'effet de_____1<Obj:sa|sadv|sn>"]	sous l'effet de_____1	Default		%default
sous la forme d'	120	prep	[pred="sous la forme de_____1<Obj:sn>",pcas= sous_la_forme_de,@e]	sous la forme de_____1	Default	e	%default
sous la forme d'__prep	120	prep	[pred="sous la forme de_____1<Obj:sn>",pcas= sous_la_forme_de,@e]	sous la forme de_____1	Default	e	%default
sous la forme de	120	prep	[pred="sous la forme de_____1<Obj:sn>",pcas= sous_la_forme_de]	sous la forme de_____1	Default		%default
sous la forme de__prep	120	prep	[pred="sous la forme de_____1<Obj:sn>",pcas= sous_la_forme_de]	sous la forme de_____1	Default		%default
sous le coup d'	120	prep	[pred="sous le coup de_____1<Obj:sa|sadv|sn>",@e]	sous le coup de_____1	Default	e	%default
sous le coup d'__prep	120	prep	[pred="sous le coup de_____1<Obj:sa|sadv|sn>",@e]	sous le coup de_____1	Default	e	%default
sous le coup de	120	prep	[pred="sous le coup de_____1<Obj:sa|sadv|sn>"]	sous le coup de_____1	Default		%default
sous le coup de__prep	120	prep	[pred="sous le coup de_____1<Obj:sa|sadv|sn>"]	sous le coup de_____1	Default		%default
suite à	120	prep	[pred="suite à_____1<Obj:sa|sadv|sn>"]	suite à_____1	Default		%default
suite à__prep	120	prep	[pred="suite à_____1<Obj:sa|sadv|sn>"]	suite à_____1	Default		%default
suivant	120	prep	[pred="suivant_____1<Obj:sa|sadv|sn>",pcas= suivant]	suivant_____1	Default		%default
sur	120	prep	[pred="sur_____1<Obj:sa|sadv|sn>",pcas = loc|sur]	sur_____1	Default		%default
tout au bout d'	120	prep	[pred="tout au bout de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	tout au bout de_____1	Default	e	%default
tout au bout d'__prep	120	prep	[pred="tout au bout de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	tout au bout de_____1	Default	e	%default
tout au bout de	120	prep	[pred="tout au bout de_____1<Obj:sa|sadv|sn>",pcas = loc]	tout au bout de_____1	Default		%default
tout au bout de__prep	120	prep	[pred="tout au bout de_____1<Obj:sa|sadv|sn>",pcas = loc]	tout au bout de_____1	Default		%default
tout au long d'	120	prep	[pred="tout au long de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	tout au long de_____1	Default	e	%default
tout au long d'__prep	120	prep	[pred="tout au long de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	tout au long de_____1	Default	e	%default
tout au long de	120	prep	[pred="tout au long de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	tout au long de_____1	Default		%default
tout au long de__prep	120	prep	[pred="tout au long de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	tout au long de_____1	Default		%default
tout en	120	prep	[pred="tout en_____1<Obj:sa|sadv|sn>",pcas = en]	tout en_____1	Default		%default
tout en haut d'	120	prep	[pred="tout en haut de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	tout en haut de_____1	Default	e	%default
tout en haut d'__prep	120	prep	[pred="tout en haut de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	tout en haut de_____1	Default	e	%default
tout en haut de	120	prep	[pred="tout en haut de_____1<Obj:sa|sadv|sn>",pcas = loc]	tout en haut de_____1	Default		%default
tout en haut de__prep	120	prep	[pred="tout en haut de_____1<Obj:sa|sadv|sn>",pcas = loc]	tout en haut de_____1	Default		%default
tout plein d'	120	prep	[pred="tout plein de_____1<Obj:sa|sadv|sn>",@e]	tout plein de_____1	Default	e	%default
tout plein d'__prep	120	prep	[pred="tout plein de_____1<Obj:sa|sadv|sn>",@e]	tout plein de_____1	Default	e	%default
tout plein de	120	prep	[pred="tout plein de_____1<Obj:sa|sadv|sn>"]	tout plein de_____1	Default		%default
tout plein de__prep	120	prep	[pred="tout plein de_____1<Obj:sa|sadv|sn>"]	tout plein de_____1	Default		%default
tout près d'	120	prep	[pred="tout près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	tout près de_____1	Default	e	%default
tout près d'__prep	120	prep	[pred="tout près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	tout près de_____1	Default	e	%default
tout près de	120	prep	[pred="tout près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	tout près de_____1	Default		%default
tout près de__prep	120	prep	[pred="tout près de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	tout près de_____1	Default		%default
une fois	120	prep	[pred="une fois_____1<Obj:sn>"]	une fois_____1	Default		%default
vers	120	prep	[pred="vers_____1<Obj:sa|sadv|sinf|sn>",pcas = loc|vers]	vers_____1	Default		%default
versus	120	prep	[pred="vs_____1<Obj:sa|sadv|sinf|sn>"]	vs_____1	Default		%default
via	120	prep	[pred="via_____1<Obj:sa|sadv|sn>"]	via_____1	Default		%default
vis-à-vis	120	prep	[pred="vis-à-vis_____1<Obj:sa|sadv|sn>",pcas = loc|vis-à-vis]	vis-à-vis_____1	Default		%default
vis-à-vis d'	120	prep	[pred="vis-à-vis de_____1<Obj:sa|sadv|sn>",pcas = loc|vis_à_vis_de,@e]	vis-à-vis de_____1	Default	e	%default
vis-à-vis d'__prep	120	prep	[pred="vis-à-vis de_____1<Obj:sa|sadv|sn>",pcas = loc|vis_à_vis_de,@e]	vis-à-vis de_____1	Default	e	%default
vis-à-vis de	120	prep	[pred="vis-à-vis de_____1<Obj:sa|sadv|sn>",pcas = loc|vis_à_vis_de]	vis-à-vis de_____1	Default		%default
vis-à-vis de__prep	120	prep	[pred="vis-à-vis de_____1<Obj:sa|sadv|sn>",pcas = loc|vis_à_vis_de]	vis-à-vis de_____1	Default		%default
voici	120	prep	[pred="voici_____1<Obj:sa|sadv|sn>"]	voici_____1	Default		%default
voilà	120	prep	[pred="voilà_____1<Obj:sa|sadv|sn>"]	voilà_____1	Default		%default
vs	120	prep	[pred="vs_____1<Obj:sa|sadv|sinf|sn>"]	vs_____1	Default		%default
vs.	120	prep	[pred="vs_____1<Obj:sa|sadv|sinf|sn>"]	vs_____1	Default		%default
vu	120	prep	[pred="vu_____1<Obj:sn>"]	vu_____1	Default		%default
y compris	120	prep	[pred="y compris_____1<Obj:sa|sadv|sinf|sn>"]	y compris_____1	Default		%default
à	120	prep	[pred="à_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = loc|à]	à_____1	Default		%default
à cause d'	120	prep	[pred="à cause de_____1<Obj:sa|sadv|sn>",mod_kind = causal,@e]	à cause de_____1	Default	e	%default
à cause d'__prep	120	prep	[pred="à cause de_____1<Obj:sa|sadv|sn>",mod_kind = causal,@e]	à cause de_____1	Default	e	%default
à cause de	120	prep	[pred="à cause de_____1<Obj:sa|sadv|sn>",mod_kind = causal]	à cause de_____1	Default		%default
à cause de__prep	120	prep	[pred="à cause de_____1<Obj:sa|sadv|sn>",mod_kind = causal]	à cause de_____1	Default		%default
à condition d'	120	prep	[pred="à condition de_____1<Obj:sadv|sinf>",@e]	à condition de_____1	Default	e	%default
à condition d'__prep	120	prep	[pred="à condition de_____1<Obj:sadv|sinf>",@e]	à condition de_____1	Default	e	%default
à condition de	120	prep	[pred="à condition de_____1<Obj:sadv|sinf>"]	à condition de_____1	Default		%default
à condition de__prep	120	prep	[pred="à condition de_____1<Obj:sadv|sinf>"]	à condition de_____1	Default		%default
à côté d'	120	prep	[pred="à côté de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	à côté de_____1	Default	e	%default
à côté d'__prep	120	prep	[pred="à côté de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	à côté de_____1	Default	e	%default
à côté de	120	prep	[pred="à côté de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	à côté de_____1	Default		%default
à côté de__prep	120	prep	[pred="à côté de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	à côté de_____1	Default		%default
à défaut d'	120	prep	[pred="à défaut de_____1<Obj:sa|sadv|sinf|sn>",@e]	à défaut de_____1	Default	e	%default
à défaut d'__prep	120	prep	[pred="à défaut de_____1<Obj:sa|sadv|sinf|sn>",@e]	à défaut de_____1	Default	e	%default
à défaut de	120	prep	[pred="à défaut de_____1<Obj:sa|sadv|sinf|sn>"]	à défaut de_____1	Default		%default
à défaut de__prep	120	prep	[pred="à défaut de_____1<Obj:sa|sadv|sinf|sn>"]	à défaut de_____1	Default		%default
à force d'	120	prep	[pred="à force de_____1<Obj:sa|sadv|sinf|sn>",mod_kind = causal,@e]	à force de_____1	Default	e	%default
à force d'__prep	120	prep	[pred="à force de_____1<Obj:sa|sadv|sinf|sn>",mod_kind = causal,@e]	à force de_____1	Default	e	%default
à force de	120	prep	[pred="à force de_____1<Obj:sa|sadv|sinf|sn>",mod_kind = causal]	à force de_____1	Default		%default
à force de__prep	120	prep	[pred="à force de_____1<Obj:sa|sadv|sinf|sn>",mod_kind = causal]	à force de_____1	Default		%default
à hauteur d'	120	prep	[pred="à hauteur de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	à hauteur de_____1	Default	e	%default
à hauteur d'__prep	120	prep	[pred="à hauteur de_____1<Obj:sa|sadv|sn>",pcas = loc,@e]	à hauteur de_____1	Default	e	%default
à hauteur de	120	prep	[pred="à hauteur de_____1<Obj:sa|sadv|sn>",pcas = loc]	à hauteur de_____1	Default		%default
à hauteur de__prep	120	prep	[pred="à hauteur de_____1<Obj:sa|sadv|sn>",pcas = loc]	à hauteur de_____1	Default		%default
à l'aune d'	120	prep	[pred="à l'aune de_____1<Obj:sa|sadv|sn>",@e]	à l'aune de_____1	Default	e	%default
à l'aune d'__prep	120	prep	[pred="à l'aune de_____1<Obj:sa|sadv|sn>",@e]	à l'aune de_____1	Default	e	%default
à l'aune de	120	prep	[pred="à l'aune de_____1<Obj:sa|sadv|sn>"]	à l'aune de_____1	Default		%default
à l'aune de__prep	120	prep	[pred="à l'aune de_____1<Obj:sa|sadv|sn>"]	à l'aune de_____1	Default		%default
à l'encontre d'	120	prep	[pred="à l'encontre de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	à l'encontre de_____1	Default	e	%default
à l'encontre d'__prep	120	prep	[pred="à l'encontre de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc,@e]	à l'encontre de_____1	Default	e	%default
à l'encontre de	120	prep	[pred="à l'encontre de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	à l'encontre de_____1	Default		%default
à l'encontre de__prep	120	prep	[pred="à l'encontre de_____1<Obj:sa|sadv|sinf|sn>",pcas = loc]	à l'encontre de_____1	Default		%default
à l'instar d'	120	prep	[pred="à l'instar de_____1<Obj:sa|sadv|sn>",@e]	à l'instar de_____1	Default	e	%default
à l'instar d'__prep	120	prep	[pred="à l'instar de_____1<Obj:sa|sadv|sn>",@e]	à l'instar de_____1	Default	e	%default
à l'instar de	120	prep	[pred="à l'instar de_____1<Obj:sa|sadv|sn>"]	à l'instar de_____1	Default		%default
à l'instar de__prep	120	prep	[pred="à l'instar de_____1<Obj:sa|sadv|sn>"]	à l'instar de_____1	Default		%default
à l'insu d'	120	prep	[pred="à l'insu de_____1<Obj:sa|sadv|sn>",@e]	à l'insu de_____1	Default	e	%default
à l'insu d'__prep	120	prep	[pred="à l'insu de_____1<Obj:sa|sadv|sn>",@e]	à l'insu de_____1	Default	e	%default
à l'insu de	120	prep	[pred="à l'insu de_____1<Obj:sa|sadv|sn>"]	à l'insu de_____1	Default		%default
à l'insu de__prep	120	prep	[pred="à l'insu de_____1<Obj:sa|sadv|sn>"]	à l'insu de_____1	Default		%default
à l'égard d'	120	prep	[pred="à l'égard de_____1<Obj:sa|sadv|sn>",@e]	à l'égard de_____1	Default	e	%default
à l'égard d'__prep	120	prep	[pred="à l'égard de_____1<Obj:sa|sadv|sn>",@e]	à l'égard de_____1	Default	e	%default
à l'égard de	120	prep	[pred="à l'égard de_____1<Obj:sa|sadv|sn>"]	à l'égard de_____1	Default		%default
à l'égard de__prep	120	prep	[pred="à l'égard de_____1<Obj:sa|sadv|sn>"]	à l'égard de_____1	Default		%default
à mesure d'	120	prep	[pred="à mesure de_____1<Obj:sn>",@e]	à mesure de_____1	Default	e	%default
à mesure d'__prep	120	prep	[pred="à mesure de_____1<Obj:sn>",@e]	à mesure de_____1	Default	e	%default
à mesure de	120	prep	[pred="à mesure de_____1<Obj:sn>"]	à mesure de_____1	Default		%default
à mesure de__prep	120	prep	[pred="à mesure de_____1<Obj:sn>"]	à mesure de_____1	Default		%default
à moins d'	120	prep	[pred="à moins de_____1<Obj:sa|sadv|sinf|sn>",@e]	à moins de_____1	Default	e	%default
à moins d'__prep	120	prep	[pred="à moins de_____1<Obj:sa|sadv|sinf|sn>",@e]	à moins de_____1	Default	e	%default
à moins de	120	prep	[pred="à moins de_____1<Obj:sa|sadv|sinf|sn>"]	à moins de_____1	Default		%default
à moins de__prep	120	prep	[pred="à moins de_____1<Obj:sa|sadv|sinf|sn>"]	à moins de_____1	Default		%default
à même	120	prep	[pred="à même_____1<Obj:sa|sadv|sn>",pcas = loc]	à même_____1	Default		%default
à partir d'	120	prep	[pred="à partir de_____1<Obj:sa|sadv|sn>",pcas=à_partir_de,@e]	à partir de_____1	Default	e	%default
à partir d'__prep	120	prep	[pred="à partir de_____1<Obj:sa|sadv|sn>",pcas=à_partir_de,@e]	à partir de_____1	Default	e	%default
à partir de	120	prep	[pred="à partir de_____1<Obj:sa|sadv|sn>",pcas=à_partir_de]	à partir de_____1	Default		%default
à partir de__prep	120	prep	[pred="à partir de_____1<Obj:sa|sadv|sn>",pcas=à_partir_de]	à partir de_____1	Default		%default
à proportion d'	120	prep	[pred="à proportion de_____1<Obj:sa|sadv|sn>",@e]	à proportion de_____1	Default	e	%default
à proportion d'__prep	120	prep	[pred="à proportion de_____1<Obj:sa|sadv|sn>",@e]	à proportion de_____1	Default	e	%default
à proportion de	120	prep	[pred="à proportion de_____1<Obj:sa|sadv|sn>"]	à proportion de_____1	Default		%default
à proportion de__prep	120	prep	[pred="à proportion de_____1<Obj:sa|sadv|sn>"]	à proportion de_____1	Default		%default
à propos d'	120	prep	[pred="à propos de_____1<Obj:sa|sadv|sinf|sn>",@e]	à propos de_____1	Default	e	%default
à propos d'__prep	120	prep	[pred="à propos de_____1<Obj:sa|sadv|sinf|sn>",@e]	à propos de_____1	Default	e	%default
à propos de	120	prep	[pred="à propos de_____1<Obj:sa|sadv|sinf|sn>"]	à propos de_____1	Default		%default
à propos de__prep	120	prep	[pred="à propos de_____1<Obj:sa|sadv|sinf|sn>"]	à propos de_____1	Default		%default
à raison d'	120	prep	[pred="à raison de_____1<Obj:sa|sadv|sn>",@e]	à raison de_____1	Default	e	%default
à raison d'__prep	120	prep	[pred="à raison de_____1<Obj:sa|sadv|sn>",@e]	à raison de_____1	Default	e	%default
à raison de	120	prep	[pred="à raison de_____1<Obj:sa|sadv|sn>"]	à raison de_____1	Default		%default
à raison de__prep	120	prep	[pred="à raison de_____1<Obj:sa|sadv|sn>"]	à raison de_____1	Default		%default
à seule fin d'	120	prep	[pred="à seule fin de_____1<Obj:sinf>",@e]	à seule fin de_____1	Default	e	%default
à seule fin d'__prep	120	prep	[pred="à seule fin de_____1<Obj:sinf>",@e]	à seule fin de_____1	Default	e	%default
à seule fin de	120	prep	[pred="à seule fin de_____1<Obj:sinf>"]	à seule fin de_____1	Default		%default
à seule fin de__prep	120	prep	[pred="à seule fin de_____1<Obj:sinf>"]	à seule fin de_____1	Default		%default
à supposer	120	prep	[pred="à supposer_____1<Obj:sn>"]	à supposer_____1	Default		%default
à travers	120	prep	[pred="à travers_____1<Obj:sa|sadv|sn>",pcas = loc|à_travers]	à travers_____1	Default		%default
à__prep	120	prep	[pred="à_____1<Obj:sa|sadv|scompl|sinf|sn>",pcas = loc|à]	à_____1	Default		%default
étant donné	120	prep	[pred="étant donné_____1<Obj:sn>"]	étant donné_____1	Default		%default
