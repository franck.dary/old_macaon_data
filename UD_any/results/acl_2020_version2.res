Corpus              Metric      F1.score        Model                       
--------------------------------------------------------------------------------
English-EWT         LAS         66.11[±0.33]%   tokeparser_incremental_b6   
English-EWT         LAS         66.44[±0.47]%   tokeparser_incremental_b5   
English-EWT         LAS         66.93[±0.27]%   tokeparser_incremental_b0   
English-EWT         LAS         66.94[±0.28]%   tokeparser_incremental_b3   
English-EWT         LAS         66.95[±0.33]%   tokeparser_incremental_b2   
English-EWT         LAS         67.02[±0.23]%   tokeparser_incremental_b4   
English-EWT         LAS         67.19[±0.26]%   tokeparser_incremental_b1   

English-EWT         Lemmas      93.29[±0.10]%   tokeparser_incremental_b0   
English-EWT         Lemmas      93.61[±0.10]%   tokeparser_incremental_b6   
English-EWT         Lemmas      93.66[±0.12]%   tokeparser_incremental_b2   
English-EWT         Lemmas      93.67[±0.20]%   tokeparser_incremental_b3   
English-EWT         Lemmas      93.72[±0.13]%   tokeparser_incremental_b4   
English-EWT         Lemmas      93.81[±0.16]%   tokeparser_incremental_b1   
English-EWT         Lemmas      93.81[±0.24]%   tokeparser_incremental_b5   

English-EWT         Sentences   66.08[±0.25]%   tokeparser_incremental_b5   
English-EWT         Sentences   66.31[±0.78]%   tokeparser_incremental_b2   
English-EWT         Sentences   66.63[±1.76]%   tokeparser_incremental_b3   
English-EWT         Sentences   66.70[±0.39]%   tokeparser_incremental_b6   
English-EWT         Sentences   67.00[±0.64]%   tokeparser_incremental_b0   
English-EWT         Sentences   67.00[±0.78]%   tokeparser_incremental_b1   
English-EWT         Sentences   67.23[±0.55]%   tokeparser_incremental_b4   

English-EWT         UAS         71.53[±0.27]%   tokeparser_incremental_b6   
English-EWT         UAS         71.68[±0.51]%   tokeparser_incremental_b5   
English-EWT         UAS         71.99[±0.18]%   tokeparser_incremental_b0   
English-EWT         UAS         72.06[±0.29]%   tokeparser_incremental_b2   
English-EWT         UAS         72.09[±0.21]%   tokeparser_incremental_b3   
English-EWT         UAS         72.29[±0.06]%   tokeparser_incremental_b4   
English-EWT         UAS         72.54[±0.37]%   tokeparser_incremental_b1   

English-EWT         UFeats      92.53[±0.26]%   tokeparser_incremental_b0   
English-EWT         UFeats      92.65[±0.21]%   tokeparser_incremental_b1   
English-EWT         UFeats      92.86[±0.11]%   tokeparser_incremental_b6   
English-EWT         UFeats      93.03[±0.12]%   tokeparser_incremental_b4   
English-EWT         UFeats      93.04[±0.10]%   tokeparser_incremental_b2   
English-EWT         UFeats      93.09[±0.29]%   tokeparser_incremental_b3   
English-EWT         UFeats      93.13[±0.23]%   tokeparser_incremental_b5   

English-EWT         UPOS        89.61[±0.06]%   tokeparser_incremental_b0   
English-EWT         UPOS        91.07[±0.17]%   tokeparser_incremental_b6   
English-EWT         UPOS        91.26[±0.24]%   tokeparser_incremental_b5   
English-EWT         UPOS        91.27[±0.15]%   tokeparser_incremental_b4   
English-EWT         UPOS        91.29[±0.25]%   tokeparser_incremental_b3   
English-EWT         UPOS        91.34[±0.17]%   tokeparser_incremental_b1   
English-EWT         UPOS        91.34[±0.21]%   tokeparser_incremental_b2   

English-EWT         Words       98.12[±0.08]%   tokeparser_incremental_b6   
English-EWT         Words       98.16[±0.09]%   tokeparser_incremental_b0   
English-EWT         Words       98.18[±0.10]%   tokeparser_incremental_b2   
English-EWT         Words       98.23[±0.03]%   tokeparser_incremental_b4   
English-EWT         Words       98.24[±0.29]%   tokeparser_incremental_b3   
English-EWT         Words       98.34[±0.22]%   tokeparser_incremental_b5   
English-EWT         Words       98.36[±0.10]%   tokeparser_incremental_b1   

--------------------------------------------------------------------------------
English-EWT         LAS         67.35[±0.27]%   tokeparser                  
English-EWT         LAS         67.68[±0.14]%   tokeparser_incremental      
English-EWT         LAS         73.74[±0.42]%   tokeparser_sequential       

English-EWT         Lemmas      93.63[±0.08]%   tokeparser                  
English-EWT         Lemmas      93.73[±0.01]%   tokeparser_incremental      
English-EWT         Lemmas      93.91[±0.09]%   tokeparser_sequential       

English-EWT         Sentences   66.66[±0.23]%   tokeparser_incremental      
English-EWT         Sentences   66.98[±0.24]%   tokeparser                  
English-EWT         Sentences   70.27[±0.63]%   tokeparser_sequential       

English-EWT         UAS         72.09[±0.19]%   tokeparser                  
English-EWT         UAS         72.50[±0.01]%   tokeparser_incremental      
English-EWT         UAS         77.86[±0.47]%   tokeparser_sequential       

English-EWT         UFeats      92.93[±0.24]%   tokeparser                  
English-EWT         UFeats      93.16[±0.05]%   tokeparser_incremental      
English-EWT         UFeats      93.42[±0.05]%   tokeparser_sequential       

English-EWT         UPOS        91.25[±0.15]%   tokeparser                  
English-EWT         UPOS        91.38[±0.08]%   tokeparser_incremental      
English-EWT         UPOS        92.21[±0.11]%   tokeparser_sequential       

English-EWT         Words       98.14[±0.08]%   tokeparser                  
English-EWT         Words       98.25[±0.01]%   tokeparser_incremental      
English-EWT         Words       98.30[±0.09]%   tokeparser_sequential       
--------------------------------------------------------------------------------
Finnish-TDT         LAS         66.74[±0.50]%   tokeparser                  
Finnish-TDT         LAS         67.35[±0.39]%   tokeparser_incremental      
Finnish-TDT         LAS         71.39[±0.32]%   tokeparser_sequential       

Finnish-TDT         Lemmas      84.02[±0.10]%   tokeparser                  
Finnish-TDT         Lemmas      84.04[±0.12]%   tokeparser_incremental      
Finnish-TDT         Lemmas      84.07[±0.13]%   tokeparser_sequential       

Finnish-TDT         Sentences   86.97[±0.69]%   tokeparser_incremental      
Finnish-TDT         Sentences   87.52[±0.66]%   tokeparser                  
Finnish-TDT         Sentences   87.73[±0.32]%   tokeparser_sequential       

Finnish-TDT         UAS         73.18[±0.31]%   tokeparser                  
Finnish-TDT         UAS         73.33[±0.49]%   tokeparser_incremental      
Finnish-TDT         UAS         76.46[±0.39]%   tokeparser_sequential       

Finnish-TDT         UFeats      90.31[±0.12]%   tokeparser_incremental      
Finnish-TDT         UFeats      90.46[±0.17]%   tokeparser                  
Finnish-TDT         UFeats      90.88[±0.16]%   tokeparser_sequential       

Finnish-TDT         UPOS        93.14[±0.16]%   tokeparser_incremental      
Finnish-TDT         UPOS        93.18[±0.07]%   tokeparser                  
Finnish-TDT         UPOS        93.39[±0.11]%   tokeparser_sequential       

Finnish-TDT         Words       99.56[±0.02]%   tokeparser                  
Finnish-TDT         Words       99.56[±0.03]%   tokeparser_sequential       
Finnish-TDT         Words       99.57[±0.05]%   tokeparser_incremental      
--------------------------------------------------------------------------------
French-Spoken       LAS         59.21[±0.31]%   tokeparser_incremental      
French-Spoken       LAS         59.22[±0.33]%   tokeparser                  
French-Spoken       LAS         61.78[±0.21]%   tokeparser_sequential       

French-Spoken       Lemmas      92.61[±0.29]%   tokeparser_incremental      
French-Spoken       Lemmas      92.82[±0.46]%   tokeparser_sequential       
French-Spoken       Lemmas      92.83[±0.03]%   tokeparser                  

French-Spoken       Sentences   9.78[±2.48]%    tokeparser_incremental      
French-Spoken       Sentences   11.16[±2.79]%   tokeparser                  
French-Spoken       Sentences   20.51[±0.48]%   tokeparser_sequential       

French-Spoken       UAS         64.48[±0.53]%   tokeparser                  
French-Spoken       UAS         64.71[±0.46]%   tokeparser_incremental      
French-Spoken       UAS         67.21[±0.15]%   tokeparser_sequential       

French-Spoken       UFeats      98.89[±0.35]%   tokeparser_incremental      
French-Spoken       UFeats      99.22[±0.49]%   tokeparser_sequential       
French-Spoken       UFeats      99.25[±0.12]%   tokeparser                  

French-Spoken       UPOS        91.31[±0.23]%   tokeparser_incremental      
French-Spoken       UPOS        91.56[±0.19]%   tokeparser                  
French-Spoken       UPOS        92.42[±0.55]%   tokeparser_sequential       

French-Spoken       Words       98.89[±0.35]%   tokeparser_incremental      
French-Spoken       Words       99.22[±0.49]%   tokeparser_sequential       
French-Spoken       Words       99.25[±0.12]%   tokeparser                  
--------------------------------------------------------------------------------
Norwegian-Bokmaal   LAS         73.89[±0.40]%   tokeparser_incremental      
Norwegian-Bokmaal   LAS         74.57[±0.86]%   tokeparser                  
Norwegian-Bokmaal   LAS         79.86[±0.23]%   tokeparser_sequential       

Norwegian-Bokmaal   Lemmas      96.28[±0.05]%   tokeparser_incremental      
Norwegian-Bokmaal   Lemmas      96.30[±0.12]%   tokeparser                  
Norwegian-Bokmaal   Lemmas      96.41[±0.08]%   tokeparser_sequential       

Norwegian-Bokmaal   Sentences   92.28[±0.90]%   tokeparser_sequential       
Norwegian-Bokmaal   Sentences   92.42[±0.54]%   tokeparser                  
Norwegian-Bokmaal   Sentences   92.77[±0.19]%   tokeparser_incremental      

Norwegian-Bokmaal   UAS         79.09[±0.23]%   tokeparser_incremental      
Norwegian-Bokmaal   UAS         79.40[±0.52]%   tokeparser                  
Norwegian-Bokmaal   UAS         83.71[±0.12]%   tokeparser_sequential       

Norwegian-Bokmaal   UFeats      93.08[±0.14]%   tokeparser_incremental      
Norwegian-Bokmaal   UFeats      93.25[±0.08]%   tokeparser                  
Norwegian-Bokmaal   UFeats      93.95[±0.07]%   tokeparser_sequential       

Norwegian-Bokmaal   UPOS        94.50[±0.11]%   tokeparser_incremental      
Norwegian-Bokmaal   UPOS        94.56[±0.15]%   tokeparser                  
Norwegian-Bokmaal   UPOS        95.50[±0.02]%   tokeparser_sequential       

Norwegian-Bokmaal   Words       99.72[±0.02]%   tokeparser_sequential       
Norwegian-Bokmaal   Words       99.73[±0.01]%   tokeparser                  
Norwegian-Bokmaal   Words       99.74[±0.01]%   tokeparser_incremental      
--------------------------------------------------------------------------------
Romanian-RRT        LAS         73.90[±0.21]%   tokeparser                  
Romanian-RRT        LAS         74.14[±0.03]%   tokeparser_incremental      
Romanian-RRT        LAS         77.30[±0.26]%   tokeparser_sequential       

Romanian-RRT        Lemmas      95.79[±0.13]%   tokeparser                  
Romanian-RRT        Lemmas      95.85[±0.02]%   tokeparser_incremental      
Romanian-RRT        Lemmas      96.21[±0.11]%   tokeparser_sequential       

Romanian-RRT        Sentences   94.65[±0.52]%   tokeparser                  
Romanian-RRT        Sentences   94.73[±0.42]%   tokeparser_incremental      
Romanian-RRT        Sentences   95.51[±0.85]%   tokeparser_sequential       

Romanian-RRT        UAS         81.03[±0.06]%   tokeparser_incremental      
Romanian-RRT        UAS         81.09[±0.18]%   tokeparser                  
Romanian-RRT        UAS         83.57[±0.25]%   tokeparser_sequential       

Romanian-RRT        UFeats      94.62[±0.06]%   tokeparser                  
Romanian-RRT        UFeats      94.69[±0.22]%   tokeparser_incremental      
Romanian-RRT        UFeats      95.21[±0.08]%   tokeparser_sequential       

Romanian-RRT        UPOS        95.47[±0.09]%   tokeparser                  
Romanian-RRT        UPOS        95.48[±0.19]%   tokeparser_incremental      
Romanian-RRT        UPOS        95.98[±0.13]%   tokeparser_sequential       

Romanian-RRT        Words       99.61[±0.04]%   tokeparser_incremental      
Romanian-RRT        Words       99.63[±0.01]%   tokeparser                  
Romanian-RRT        Words       99.66[±0.03]%   tokeparser_sequential       
--------------------------------------------------------------------------------
Russian-SynTagRus   LAS         77.52[±0.31]%   tokeparser                  
Russian-SynTagRus   LAS         78.32[±0.16]%   tokeparser_incremental      
Russian-SynTagRus   LAS         81.47[±0.23]%   tokeparser_sequential       

Russian-SynTagRus   Lemmas      95.57[±0.08]%   tokeparser_incremental      
Russian-SynTagRus   Lemmas      95.60[±0.07]%   tokeparser                  
Russian-SynTagRus   Lemmas      95.78[±0.06]%   tokeparser_sequential       

Russian-SynTagRus   Sentences   96.57[±0.03]%   tokeparser_incremental      
Russian-SynTagRus   Sentences   96.67[±0.18]%   tokeparser_sequential       
Russian-SynTagRus   Sentences   96.84[±0.06]%   tokeparser                  

Russian-SynTagRus   UAS         81.59[±0.25]%   tokeparser_incremental      
Russian-SynTagRus   UAS         82.03[±0.32]%   tokeparser                  
Russian-SynTagRus   UAS         85.13[±0.19]%   tokeparser_sequential       

Russian-SynTagRus   UFeats      92.55[±0.10]%   tokeparser_incremental      
Russian-SynTagRus   UFeats      92.62[±0.15]%   tokeparser                  
Russian-SynTagRus   UFeats      93.68[±0.05]%   tokeparser_sequential       

Russian-SynTagRus   UPOS        97.29[±0.03]%   tokeparser                  
Russian-SynTagRus   UPOS        97.33[±0.07]%   tokeparser_incremental      
Russian-SynTagRus   UPOS        97.57[±0.02]%   tokeparser_sequential       

Russian-SynTagRus   Words       99.53[±0.01]%   tokeparser                  
Russian-SynTagRus   Words       99.53[±0.04]%   tokeparser_sequential       
Russian-SynTagRus   Words       99.55[±0.05]%   tokeparser_incremental      
--------------------------------------------------------------------------------
Turkish-IMST        LAS         48.14[±0.60]%   tokeparser_incremental      
Turkish-IMST        LAS         48.15[±1.25]%   tokeparser                  
Turkish-IMST        LAS         49.08[±0.49]%   tokeparser_sequential       

Turkish-IMST        Lemmas      83.52[±0.15]%   tokeparser_sequential       
Turkish-IMST        Lemmas      83.56[±0.34]%   tokeparser                  
Turkish-IMST        Lemmas      83.67[±0.14]%   tokeparser_incremental      

Turkish-IMST        Sentences   96.16[±0.38]%   tokeparser_incremental      
Turkish-IMST        Sentences   96.21[±0.08]%   tokeparser                  
Turkish-IMST        Sentences   97.05[±0.03]%   tokeparser_sequential       

Turkish-IMST        UAS         55.64[±1.45]%   tokeparser                  
Turkish-IMST        UAS         55.69[±0.29]%   tokeparser_incremental      
Turkish-IMST        UAS         56.30[±0.31]%   tokeparser_sequential       

Turkish-IMST        UFeats      85.18[±0.12]%   tokeparser_incremental      
Turkish-IMST        UFeats      85.46[±0.21]%   tokeparser                  
Turkish-IMST        UFeats      85.85[±0.14]%   tokeparser_sequential       

Turkish-IMST        UPOS        89.23[±0.24]%   tokeparser                  
Turkish-IMST        UPOS        89.24[±0.22]%   tokeparser_incremental      
Turkish-IMST        UPOS        89.41[±0.11]%   tokeparser_sequential       

Turkish-IMST        Words       96.73[±0.24]%   tokeparser                  
Turkish-IMST        Words       96.76[±0.18]%   tokeparser_sequential       
Turkish-IMST        Words       96.87[±0.02]%   tokeparser_incremental      
