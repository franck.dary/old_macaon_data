Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
signature signature.cla
parser parser.cla
%STATES
sgn1 signature
parser parser
%TRANSITIONS
sgn1 parser 0 *
parser parser 0 LEFT
parser parser 0 REDUCE
parser parser 0 EOS
parser sgn1 +1 SHIFT
parser sgn1 +1 RIGHT
