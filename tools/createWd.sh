#!/bin/bash


if [ "$#" -lt 1 ]; then
		echo "Usage : $0 directory_path_UD_corpora"
		exit
fi

directory_path_UD_corpora=$1

files=$(ls ${directory_path_UD_corpora}UD_*/*train.conllu)

rm -f tmp
touch tmp

for file in $files
do
		echo "------"$file >> tmp
		cut -f 1,7,8 $file | grep -v -E "^#" >> tmp
done

python3 createWdFromListDep.py ../data/wals/listLabelsUD.txt tmp --freq
