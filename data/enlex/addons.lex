&	100	V	[pred="&_____1",cat=V]	&_____1	Default		%default	C1
'	100	V	[pred="'_____1",cat=?]	'_____1	Default		%default	inv
'd	100	V	[pred="'d_____1",cat=V]	'd_____1	Default		%default	inv
'll	100	V	[pred="'ll_____1",cat=V]	'll_____1	Default		%default	inv
'm	100	V	[pred="'m_____1",cat=?]	'm_____1	Default		%default	inv
're	100	V	[pred="'re_____1",cat=?]	're_____1	Default		%default	inv
's	100	V	[pred="'s_____1",cat=?]	's_____1	Default		%default	inv
've	100	V	[pred="'ve_____1",cat=?]	've_____1	Default		%default	inv
ASAP	100	N	[pred="ASAP_____1",cat=N]	ASAP_____1	Default		%default	R1b
Christmas Eve	100	V	[pred="Christmas Eve_____1",cat=V]	Christmas Eve_____1	Default		%default	I1
FYI	100	N	[pred="FYI_____1",cat=N]	FYI_____1	Default		%default	R1b
H1N1	100	N	[pred="H1N1_____1",cat=N]	H1N1_____1	Default		%default	N2
H1N1s	100	N	[pred="H1N1_____1",cat=N,@p]	H1N1_____1	Default	p	%default	N2
Merry Christmas	100	V	[pred="Merry Christmas_____1",cat=V]	Merry Christmas_____1	Default		%default	I1
O.K.	100	V	[pred="OK_____1",cat=V]	OK_____1	Default		%default	inv
OK	100	V	[pred="OK_____1",cat=V]	OK_____1	Default		%default	inv
actioning	100	N	[pred="actioning_____1",cat=N,@?]	actioning_____1	Default	?	%default	N1
around	100	N	[pred="around_____1",cat=N]	around_____1	Default		%default	N2
arounds	100	N	[pred="around_____1",cat=N,@p]	around_____1	Default	p	%default	N2
asap	100	N	[pred="asap_____1",cat=N]	asap_____1	Default		%default	R1b
bi-annual	100	N	[pred="bi-annual_____1",cat=N,@?]	bi-annual_____1	Default	?	%default	A1
blackline	100	V	[pred="blackline_____1",cat=V,@inf]	blackline_____1	Default	inf	%default	V2
blacklined	100	V	[pred="blackline_____1",cat=V,@past]	blackline_____1	Default	past	%default	V2
blacklined	100	V	[pred="blackline_____1",cat=V,@ppart]	blackline_____1	Default	ppart	%default	V2
blacklines	100	V	[pred="blackline_____1",cat=V,@pres3s]	blackline_____1	Default	pres3s	%default	V2
blacklining	100	V	[pred="blackline_____1",cat=V,@prespart]	blackline_____1	Default	prespart	%default	V2
blog	100	N	[pred="blog_____1",cat=N]	blog_____1	Default		%default	N2
blogs	100	N	[pred="blog_____1",cat=N,@p]	blog_____1	Default	p	%default	N2
cannot	100	R	[pred="cannot_____1",cat=R]	cannot_____1	Default		%default	inv
capex	100	N	[pred="capex_____1",cat=N]	capex_____1	Default		%default	N3
capexes	100	N	[pred="capex_____1",cat=N,@p]	capex_____1	Default	p	%default	N3
coup d'�tat	100	N	[pred="coup d'�tat_____1",cat=N,@?]	coup d'�tat_____1	Default	?	%default	N1
deliverable	100	N	[pred="deliverable_____1",cat=N]	deliverable_____1	Default		%default	N2
deliverables	100	N	[pred="deliverable_____1",cat=N,@p]	deliverable_____1	Default	p	%default	N2
demotivate	100	V	[pred="demotivate_____1",cat=V,@inf]	demotivate_____1	Default	inf	%default	V2
demotivated	100	V	[pred="demotivate_____1",cat=V,@past]	demotivate_____1	Default	past	%default	V2
demotivated	100	V	[pred="demotivate_____1",cat=V,@ppart]	demotivate_____1	Default	ppart	%default	V2
demotivates	100	V	[pred="demotivate_____1",cat=V,@pres3s]	demotivate_____1	Default	pres3s	%default	V2
demotivating	100	V	[pred="demotivate_____1",cat=V,@prespart]	demotivate_____1	Default	prespart	%default	V2
demotivation	100	N	[pred="demotivation_____1",cat=N]	demotivation_____1	Default		%default	N2
demotivations	100	N	[pred="demotivation_____1",cat=N,@p]	demotivation_____1	Default	p	%default	N2
desking	100	N	[pred="desking_____1",cat=N,@?]	desking_____1	Default	?	%default	N1
desktop	100	N	[pred="desktop_____1",cat=N]	desktop_____1	Default		%default	N2
desktops	100	N	[pred="desktop_____1",cat=N,@p]	desktop_____1	Default	p	%default	N2
directive-oriented	100	A	[pred="directive-oriented_____1",cat=A,@?]	directive-oriented_____1	Default	?	%default	A1
dissatisfaction	100	N	[pred="dissatisfaction_____1",cat=N]	dissatisfaction_____1	Default		%default	N2
dissatisfactions	100	N	[pred="dissatisfaction_____1",cat=N,@p]	dissatisfaction_____1	Default	p	%default	N2
downside	100	N	[pred="downside_____1",cat=N]	downside_____1	Default		%default	N2
downsides	100	N	[pred="downside_____1",cat=N,@p]	downside_____1	Default	p	%default	N2
downsize	100	V	[pred="downsize_____1",cat=V,@inf]	downsize_____1	Default	inf	%default	V2
downsized	100	V	[pred="downsize_____1",cat=V,@past]	downsize_____1	Default	past	%default	V2
downsized	100	V	[pred="downsize_____1",cat=V,@ppart]	downsize_____1	Default	ppart	%default	V2
downsizes	100	V	[pred="downsize_____1",cat=V,@pres3s]	downsize_____1	Default	pres3s	%default	V2
downsizing	100	V	[pred="downsize_____1",cat=V,@prespart]	downsize_____1	Default	prespart	%default	V2
e-business	100	N	[pred="e-business_____1",cat=N]	e-business_____1	Default		%default	N2
e-businesss	100	N	[pred="e-business_____1",cat=N,@p]	e-business_____1	Default	p	%default	N2
e-commerce	100	N	[pred="e-commerce_____1",cat=N]	e-commerce_____1	Default		%default	N6
e-learning	100	N	[pred="e-learning_____1",cat=N]	e-learning_____1	Default		%default	N6
euro	100	N	[pred="euro_____1",cat=N]	euro_____1	Default		%default	N2
euros	100	N	[pred="euro_____1",cat=N,@p]	euro_____1	Default	p	%default	N2
expat	100	N	[pred="expat_____1",cat=N]	expat_____1	Default		%default	N2
expats	100	N	[pred="expat_____1",cat=N,@p]	expat_____1	Default	p	%default	N2
fait accompli	100	N	[pred="fait accompli_____1",cat=N,@?]	fait accompli_____1	Default	?	%default	N1
flexi	100	N	[pred="flexi_____1",cat=N,@?]	flexi_____1	Default	?	%default	A1
functionalities	100	N	[pred="functionality_____1",cat=N,@p]	functionality_____1	Default	p	%default	N4
functionality	100	N	[pred="functionality_____1",cat=N]	functionality_____1	Default		%default	N4
fyi	100	N	[pred="fyi_____1",cat=N]	fyi_____1	Default		%default	R1b
hands-on	100	N	[pred="hands-on_____1",cat=N,@?]	hands-on_____1	Default	?	%default	A1
helpdesk	100	N	[pred="helpdesk_____1",cat=N]	helpdesk_____1	Default		%default	N2
helpdesks	100	N	[pred="helpdesk_____1",cat=N,@p]	helpdesk_____1	Default	p	%default	N2
hotline	100	N	[pred="hotline_____1",cat=N]	hotline_____1	Default		%default	N2
hotlines	100	N	[pred="hotline_____1",cat=N,@p]	hotline_____1	Default	p	%default	N2
incent	110	V	[pred="incent_____1",cat=V,@inf]	incent_____1	Default	inf	%default	V3
incented	110	V	[pred="incent_____1",cat=V,@past]	incent_____1	Default	past	%default	V3
incented	110	V	[pred="incent_____1",cat=V,@ppart]	incent_____1	Default	ppart	%default	V3
incenting	110	V	[pred="incent_____1",cat=V,@prespart]	incent_____1	Default	prespart	%default	V3
incents	110	V	[pred="incent_____1",cat=V,@pres3s]	incent_____1	Default	pres3s	%default	V3
intranet	100	N	[pred="intranet_____1",cat=N]	intranet_____1	Default		%default	N2
intranets	100	N	[pred="intranet_____1",cat=N,@p]	intranet_____1	Default	p	%default	N2
laptop	100	N	[pred="laptop_____1",cat=N]	laptop_____1	Default		%default	N2
laptops	100	N	[pred="laptop_____1",cat=N,@p]	laptop_____1	Default	p	%default	N2
login	100	N	[pred="login_____1",cat=N]	login_____1	Default		%default	N2
logins	100	N	[pred="login_____1",cat=N,@p]	login_____1	Default	p	%default	N2
logon	100	N	[pred="logon_____1",cat=N]	logon_____1	Default		%default	N2
logons	100	N	[pred="logon_____1",cat=N,@p]	logon_____1	Default	p	%default	N2
long-lasting	100	A	[pred="long-lasting_____1",cat=A,@?]	long-lasting_____1	Default	?	%default	A1
mega	100	N	[pred="mega_____1",cat=N,@?]	mega_____1	Default	?	%default	A1
mentoring	100	N	[pred="mentoring_____1",cat=N]	mentoring_____1	Default		%default	N2
mentorings	100	N	[pred="mentoring_____1",cat=N,@p]	mentoring_____1	Default	p	%default	N2
micromanagement	100	N	[pred="micromanagement_____1",cat=N,@?]	micromanagement_____1	Default	?	%default	N1
mid-level	100	N	[pred="mid-level_____1",cat=N,@?]	mid-level_____1	Default	?	%default	A1
mindset	100	N	[pred="mindset_____1",cat=N]	mindset_____1	Default		%default	N2
mindsets	100	N	[pred="mindset_____1",cat=N,@p]	mindset_____1	Default	p	%default	N2
miscommunication	100	N	[pred="miscommunication_____1",cat=N]	miscommunication_____1	Default		%default	N2
miscommunications	100	N	[pred="miscommunication_____1",cat=N,@p]	miscommunication_____1	Default	p	%default	N2
misinformation	100	N	[pred="misinformation_____1",cat=N]	misinformation_____1	Default		%default	N6
n't	100	R	[pred="n't_____1",cat=R]	n't_____1	Default		%default	inv
non	100	V	[pred="non_____1",cat=V]	non_____1	Default		%default	R1b
o.k.	100	V	[pred="OK_____1",cat=V]	OK_____1	Default		%default	inv
offline	100	A	[pred="offline_____1",cat=A,@?]	offline_____1	Default	?	%default	A1
ok	100	V	[pred="OK_____1",cat=V]	OK_____1	Default		%default	inv
on-time	100	A	[pred="on-time_____1",cat=A,@?]	on-time_____1	Default	?	%default	A1
online	100	A	[pred="online_____1",cat=A,@?]	online_____1	Default	?	%default	A1
onsite	100	A	[pred="onsite_____1",cat=A,@?]	onsite_____1	Default	?	%default	A1
paperless	100	N	[pred="paperless_____1",cat=N,@?]	paperless_____1	Default	?	%default	A1
pathing	100	N	[pred="pathing_____1",cat=N]	pathing_____1	Default		%default	N2
pathings	100	N	[pred="pathing_____1",cat=N,@p]	pathing_____1	Default	p	%default	N2
payout	100	N	[pred="payout_____1",cat=N]	payout_____1	Default		%default	N2
payouts	100	N	[pred="payout_____1",cat=N,@p]	payout_____1	Default	p	%default	N2
payrise	100	N	[pred="payrise_____1",cat=N]	payrise_____1	Default		%default	N2
payrises	100	N	[pred="payrise_____1",cat=N,@p]	payrise_____1	Default	p	%default	N2
payscale	100	N	[pred="payscale_____1",cat=N]	payscale_____1	Default		%default	N2
payscale	100	N	[pred="payscale_____2",cat=N]	payscale_____2	Default		%default	N2
payscales	100	N	[pred="payscale_____1",cat=N,@p]	payscale_____1	Default	p	%default	N2
payscales	100	N	[pred="payscale_____2",cat=N,@p]	payscale_____2	Default	p	%default	N2
pdp	100	N	[pred="pdp_____1",cat=N,@?]	pdp_____1	Default	?	%default	N1
powerpoint	100	N	[pred="powerpoint_____1",cat=N]	powerpoint_____1	Default		%default	N2
powerpoints	100	N	[pred="powerpoint_____1",cat=N,@p]	powerpoint_____1	Default	p	%default	N2
pro rata	100	N	[pred="pro rata_____1",cat=N]	pro rata_____1	Default		%default	N6
pro-active	100	N	[pred="pro-active_____1",cat=N,@?]	pro-active_____1	Default	?	%default	A1
proactive	100	A	[pred="proactive_____1",cat=A,@?]	proactive_____1	Default	?	%default	A1
proactivelier	100	N	[pred="proactively_____1",cat=N,@comp]	proactively_____1	Default	comp	%default	R2
proactiveliest	100	N	[pred="proactively_____1",cat=N,@sup]	proactively_____1	Default	sup	%default	R2
proactively	100	N	[pred="proactively_____1",cat=N]	proactively_____1	Default		%default	R2
proactivities	100	N	[pred="proactivity_____1",cat=N,@p]	proactivity_____1	Default	p	%default	N4
proactivity	100	N	[pred="proactivity_____1",cat=N]	proactivity_____1	Default		%default	N4
refurb	100	V	[pred="refurb_____1",cat=V,@inf]	refurb_____1	Default	inf	%default	V3
refurbed	100	V	[pred="refurb_____1",cat=V,@past]	refurb_____1	Default	past	%default	V3
refurbed	100	V	[pred="refurb_____1",cat=V,@ppart]	refurb_____1	Default	ppart	%default	V3
refurbing	100	V	[pred="refurb_____1",cat=V,@prespart]	refurb_____1	Default	prespart	%default	V3
refurbs	100	V	[pred="refurb_____1",cat=V,@pres3s]	refurb_____1	Default	pres3s	%default	V3
remotivate	100	V	[pred="remotivate_____1",cat=V,@inf]	remotivate_____1	Default	inf	%default	V2
remotivated	100	V	[pred="remotivate_____1",cat=V,@past]	remotivate_____1	Default	past	%default	V2
remotivated	100	V	[pred="remotivate_____1",cat=V,@ppart]	remotivate_____1	Default	ppart	%default	V2
remotivates	100	V	[pred="remotivate_____1",cat=V,@pres3s]	remotivate_____1	Default	pres3s	%default	V2
remotivating	100	V	[pred="remotivate_____1",cat=V,@prespart]	remotivate_____1	Default	prespart	%default	V2
resourcing	100	N	[pred="resourcing_____1",cat=N]	resourcing_____1	Default		%default	N6
revisit	100	V	[pred="revisit_____1",cat=V,@inf]	revisit_____1	Default	inf	%default	V3
revisited	100	V	[pred="revisit_____1",cat=V,@past]	revisit_____1	Default	past	%default	V3
revisited	100	V	[pred="revisit_____1",cat=V,@ppart]	revisit_____1	Default	ppart	%default	V3
revisiting	100	V	[pred="revisit_____1",cat=V,@prespart]	revisit_____1	Default	prespart	%default	V3
revisits	100	V	[pred="revisit_____1",cat=V,@pres3s]	revisit_____1	Default	pres3s	%default	V3
robotic	100	A	[pred="robotic_____1",cat=A]	robotic_____1	Default		%default	A2
roboticer	100	A	[pred="robotic_____1",cat=A,@comp]	robotic_____1	Default	comp	%default	A2
roboticest	100	A	[pred="robotic_____1",cat=A,@sup]	robotic_____1	Default	sup	%default	A2
rosey	100	A	[pred="rosey_____1",cat=A,@?]	rosey_____1	Default	?	%default	A1
scope	100	V	[pred="scope_____1",cat=V,@inf]	scope_____1	Default	inf	%default	V2
scoped	100	V	[pred="scope_____1",cat=V,@past]	scope_____1	Default	past	%default	V2
scoped	100	V	[pred="scope_____1",cat=V,@ppart]	scope_____1	Default	ppart	%default	V2
scopes	100	V	[pred="scope_____1",cat=V,@pres3s]	scope_____1	Default	pres3s	%default	V2
scoping	100	V	[pred="scope_____1",cat=V,@prespart]	scope_____1	Default	prespart	%default	V2
scoping	100	N	[pred="scoping_____1",cat=N]	scoping_____1	Default		%default	N6
sign-off	100	N	[pred="sign-off_____1",cat=N]	sign-off_____1	Default		%default	N2
sign-offs	100	N	[pred="sign-off_____1",cat=N,@p]	sign-off_____1	Default	p	%default	N2
skillset	100	N	[pred="skillset_____1",cat=N]	skillset_____1	Default		%default	N2
skillsets	100	N	[pred="skillset_____1",cat=N,@p]	skillset_____1	Default	p	%default	N2
slippage	100	N	[pred="slippage_____1",cat=N,@?]	slippage_____1	Default	?	%default	N1
sourcing	100	N	[pred="sourcing_____1",cat=N]	sourcing_____1	Default		%default	N6
state-of-the-art	100	A	[pred="state-of-the-art_____1",cat=A,@?]	state-of-the-art_____1	Default	?	%default	A1
timeline	100	N	[pred="timeline_____1",cat=N]	timeline_____1	Default		%default	N2
timelines	100	N	[pred="timeline_____1",cat=N,@p]	timeline_____1	Default	p	%default	N2
timetable	110	V	[pred="timetable_____1",cat=V,@inf]	timetable_____1	Default	inf	%default	V2
timetabled	110	V	[pred="timetable_____1",cat=V,@past]	timetable_____1	Default	past	%default	V2
timetabled	110	V	[pred="timetable_____1",cat=V,@ppart]	timetable_____1	Default	ppart	%default	V2
timetables	110	V	[pred="timetable_____1",cat=V,@pres3s]	timetable_____1	Default	pres3s	%default	V2
timetabling	110	V	[pred="timetable_____1",cat=V,@prespart]	timetable_____1	Default	prespart	%default	V2
top-down	100	A	[pred="top-down_____1",cat=A,@?]	top-down_____1	Default	?	%default	A1
training	100	N	[pred="training_____1",cat=N]	training_____1	Default		%default	N2
trainings	100	N	[pred="training_____1",cat=N,@p]	training_____1	Default	p	%default	N2
uncompetitive	100	A	[pred="uncompetitive_____1",cat=A,@?]	uncompetitive_____1	Default	?	%default	A1
understaffing	100	N	[pred="understaffing_____1",cat=N]	understaffing_____1	Default		%default	N2
understaffings	100	N	[pred="understaffing_____1",cat=N,@p]	understaffing_____1	Default	p	%default	N2
unsatisfaction	100	N	[pred="unsatisfaction_____1",cat=N]	unsatisfaction_____1	Default		%default	N2
unsatisfactions	100	N	[pred="unsatisfaction_____1",cat=N,@p]	unsatisfaction_____1	Default	p	%default	N2
unsatisfied	100	V	[pred="unsatisfy_____1",cat=V,@past]	unsatisfy_____1	Default	past	%default	V5
unsatisfied	100	V	[pred="unsatisfy_____1",cat=V,@ppart]	unsatisfy_____1	Default	ppart	%default	V5
unsatisfies	100	V	[pred="unsatisfy_____1",cat=V,@pres3s]	unsatisfy_____1	Default	pres3s	%default	V5
unsatisfy	100	V	[pred="unsatisfy_____1",cat=V,@inf]	unsatisfy_____1	Default	inf	%default	V5
unsatisfying	100	V	[pred="unsatisfy_____1",cat=V,@prespart]	unsatisfy_____1	Default	prespart	%default	V5
upfront	100	N	[pred="upfront_____1",cat=N]	upfront_____1	Default		%default	N2
upfronts	100	N	[pred="upfront_____1",cat=N,@p]	upfront_____1	Default	p	%default	N2
upper-management	100	N	[pred="upper-management_____1",cat=N]	upper-management_____1	Default		%default	N2
upper-managements	100	N	[pred="upper-management_____1",cat=N,@p]	upper-management_____1	Default	p	%default	N2
user-friendly	100	N	[pred="user-friendly_____1",cat=N,@?]	user-friendly_____1	Default	?	%default	A1
value-adding	100	A	[pred="value-adding_____1",cat=A,@?]	value-adding_____1	Default	?	%default	A1
voicemail	100	N	[pred="voicemail_____1",cat=N]	voicemail_____1	Default		%default	N2
voicemails	100	N	[pred="voicemail_____1",cat=N,@p]	voicemail_____1	Default	p	%default	N2
vs.	100	V	[pred="vs._____1",cat=?]	vs._____1	Default		%default	inv
webcam	100	N	[pred="webcam_____1",cat=N]	webcam_____1	Default		%default	N2
webcams	100	N	[pred="webcam_____1",cat=N,@p]	webcam_____1	Default	p	%default	N2
win-win	100	A	[pred="win-win_____1",cat=A,@?]	win-win_____1	Default	?	%default	A1
workflow	100	N	[pred="workflow_____1",cat=N]	workflow_____1	Default		%default	N2
workflows	100	N	[pred="workflow_____1",cat=N,@p]	workflow_____1	Default	p	%default	N2
world-class	100	A	[pred="world-class_____1",cat=A,@?]	world-class_____1	Default	?	%default	A1
