Name : Tagger Machine
Dicts : tagger.dicts
%CLASSIFIERS
strategy strategy.cla
tagger tagger.cla
tokenizer tokenizer.cla
signature signature.cla
%STATES
strategy strategy
tokenizer tokenizer
signature signature
tagger tagger
%TRANSITIONS
strategy signature MOVE signature
strategy tagger MOVE tagger
strategy tokenizer MOVE tokenizer
tagger strategy *
signature strategy *
tokenizer strategy *
