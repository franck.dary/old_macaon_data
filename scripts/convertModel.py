#! /usr/bin/python3

import sys

def printUsageAndExit() :
  print("USAGE :", sys.argv[0], "file.model")
  exit(1)

if not len(sys.argv) == 2 :
  printUsageAndExit()

lineIsParam = False
for line in open(sys.argv[1]) :
  if len(line) == 0 :
    continue
  if line[0] == '#' :
    splited = line.split(' ')
    if splited[1].split('_')[0] == "Layer" :
      print(splited[0],end=" ")
      print("MLP_",end="")
      print(splited[1],end=" ")
      print(splited[2],end=" ")
      print(splited[3],end=" ")
      print(splited[4],end="")
    else :
      print(line,end="")
    if line.split()[0] == "#Parameter#" :
      lineIsParam = True
  elif lineIsParam :
    print(line,end="")
    lineIsParam = False
  else :
    print("#TOPOLOGY#","#","{1,1} 0",line,end="")

