#! /usr/bin/python3

import sys;

def printUsageAndExit() :
  print("USAGE : file.mcf mcd", file=sys.stderr)
  exit(1)

def readMCD(mcd) :
  col2index = {}
  index2col = {}

  for line in open(mcd, "r", encoding="utf8") :
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : wrong MCD line \'%s\'"%line.strip(), file=sys.stderr)
      exit(1)
    col2index[splited[1].strip()] = int(splited[0].strip())
    index2col[int(splited[0].strip())] = splited[1].strip()

  return col2index, index2col

def posIsPonct(pos) :
  poncts = {"PONCT" : True, "PUNCT" : True, "ponct" : True, "punct" : True, "puncts" : True, "poncts" : True, "PUNCTS" : True, "PONCTS" : True}

  return pos in poncts

def treatFile(col2index, index2col, mcf) :
  lines = []
  for line in open(mcf, "r", encoding="utf8") :
    splited = line.strip().split("\t")
    if len(splited) != len(col2index) :
      print("ERROR : invalid MCF line \'%s\'"%line.strip(), file=sys.stderr)
      exit(1)
    lines.append(splited)

  for i in range(len(lines)) :
    if i < len(lines)-1 and lines[i+1][col2index["EOS"]] == "1" :
      lines[i][col2index["EOS"]] = "1"
      if i+2 < len(lines) :
        lines[i+2][col2index["FORM"]] = lines[i+2][col2index["FORM"]].lower()
        lines[i+2][col2index["LEMMA"]] = lines[i+2][col2index["LEMMA"]].lower()
    if not posIsPonct(lines[i][col2index["POS"]]) :
      for j in range(len(lines[i])) :
        if j == len(lines[i])-1 :
          print(lines[i][j])
        else :
          print(lines[i][j], end="\t")

def main() :
  if len(sys.argv) != 3 :
    printUsageAndExit()

  col2index, index2col = readMCD(sys.argv[2])
  treatFile(col2index, index2col, sys.argv[1])

main()
