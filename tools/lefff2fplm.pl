#!/usr/bin/perl

my $dir= shift;

#le programme lefff2fplm prend en entrée le lefff et produit un fichier de la forme
#forme_fléchie catégorie lemme morphologie
#
#le séparateur de champ est la tabulation
#
#le champ morphologie est de la forme tps#pers#gnr#nb#cas#type_det
#
#tps prend les valeurs
#
#P 	indicatif 	présent
#F 	indicatif 	futur
#I 	indicatif 	imparfait
#J 	indicatif 	passé-simple
#C 	conditionnel 	présent
#Y 	impératif 	présent
#S 	subjonctif 	présent
#T 	subjonctif 	imparfait
#K 	participe 	passé
#G 	participe 	présent
#W 	infinitif 	présent
#
#pers prend les valeurs
#
#1 première personne
#2 deuxième personne
#3 troisième personne
#
#gnr prend les valeurs
#
#m masculin
#f féminin
#
#nb prend les valeurs
#
#s singulier
#p pluriel
#
#cas prend les valeurs
#
#i objet indirect
#o objet direct
#u sujet
#
#type_det prend les valeurs
#
#d defini
#n indefini
#e demonstratif
#p possessif





#################################################
#categories P7
#################################################

#ADJ
##ADJWH
#ADV
#ADVWH
#CC
#CLO
#CLR
#CLS
#CS
#DET
#DETWH
#ET
#I
#NC
#NPP
#P
#P+D
#PONCT
#P+PRO
#PREF
#PRO
#PROREL
#PROWH
#V
#VIMP
#VINF
#VPP
#VPR
#VS

#################################################
#################################################

#valeurs du trait cas
#i objet indirect
#o objet direct
#u sujet

#valeurs du trait tps
#P 	indicatif 	présent
#F 	indicatif 	futur
#I 	indicatif 	imparfait
#J 	indicatif 	passé-simple
#C 	conditionnel 	présent
#Y 	impératif 	présent
#S 	subjonctif 	présent
#T 	subjonctif 	imparfait
#K 	participe 	passé
#G 	participe 	présent
#W 	infinitif 	présent

#valeurs du trait type_det
#d defini
#n indefini
#e demonstratif
#p possessif

#la flexion est la concatenation des traits
#tps pers gnr nb cas type_det


$h_pos{"adj"} = "ADJ";
$h_pos{"adjPref"} = "ADJ";
$h_pos{"adv"} = "ADV";
$h_pos{"advm"} = "ADV";
$h_pos{"advneg"} = "ADV";
$h_pos{"advp"} = "ADV";
$h_pos{"advPref"} = "ADV";
$h_pos{"auxAvoir"} = "V";
$h_pos{"auxEtre"} = "V";
$h_pos{"caimp"} = "XX";
$h_pos{"ce"} = "XX";
$h_pos{"cf"} = "XX";
$h_pos{"cfi"} = "XX";
$h_pos{"cla"} = "XX";
$h_pos{"clar"} = "XX";
$h_pos{"cld"} = "XX";
$h_pos{"cldr"} = "XX";
$h_pos{"clg"} = "XX";
$h_pos{"cll"} = "XX";
$h_pos{"cln"} = "CLS";
$h_pos{"clneg"} = "XX";
$h_pos{"clr"} = "CLR";
$h_pos{"coo"} = "CC";
$h_pos{"csu"} = "CS";
$h_pos{"det"} = "DET";
$h_pos{"etr"} = "ETR";
$h_pos{"ilimp"} = "XX";
$h_pos{"meta"} = "XX";
$h_pos{"nc"} = "NC";
$h_pos{"np"} = "NPP";
$h_pos{"parentf"} = "PONCT";
$h_pos{"parento"} = "PONCT";
$h_pos{"poncts"} = "PONCT";
$h_pos{"ponctw"} = "PONCT";
$h_pos{"prel"} = "PROREL";
$h_pos{"prep"} = "P";
$h_pos{"pres"} = "I";
$h_pos{"pri"} = "PROWH";
$h_pos{"pro"} = "PRO";
$h_pos{"que"} = "XX";
$h_pos{"que_restr"} = "XX";
$h_pos{"sbound"} = "XX";
$h_pos{"suffAdj"} = "XX";
$h_pos{"v"} = "V";

#alarmante	100	adj	[pred="alarmant_____1<Suj:(cln|sn)>",@pers,cat=adj,@fs]	alarmant_____1	Default	fs	%adj_personnel


my @file_list = `ls $dir/*.lex`;

my @file_list = (
    "$dir/adj.lex",
#    "$dir/avoirC1pC2.lex",
    "$dir/conj.lex",
    "$dir/det.lex",
    "$dir/interj.lex",
    "$dir/nom.lex",
    "$dir/nompred.lex",
    "$dir/pref-suff.lex",
    "$dir/pro.lex",
#    "$dir/vC1pC2.lex",
#    "$dir/v.lex",
#    "$dir/v-phd.lex",
    "$dir/adv.lex",
#    "$dir/avoirN1pC2.lex",
    "$dir/coord.lex",
#    "$dir/entnom.lex",
    "$dir/neg.lex",
    "$dir/nomp.lex",
    "$dir/ponct.lex",
    "$dir/prep.lex",
#    "$dir/uw.lex",
#    "$dir/vC1pN2.lex",
    "$dir/v_new.lex");

#my @file_list = (
#    "$dir/prep.lex",
#);


foreach $file (@file_list){
    chomp($file);
    print STDERR "processing file : $file\n";
    open(FILE,$file);
    while(<FILE>){
#	/([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/;
	/([^\t]*)\t([^\t]*)\t([^\t]*)\t\[([^\]]*)\]\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/;

	my $forme = $1;
	my $cat = $3;
	my $morpho = $4;
	my $lemme = $5;
	$lemme =~ s/_+[0-9]$//;

	my $tps = '';
	my $pers = '';
	my $gnr = '';
	my $nb = '';
	my $cas = '';
	my $type_det = '';
	
	
	#ajout d'espace après les apostrophes dans les formes
	$forme =~ s/'(.)/' \1/;

	
	my @tab_morpho = split /,/ , $morpho;
	$flexion = $tab_morpho[$#tab_morpho];
	if(!($flexion =~ /^@/)){
	    $flexion = "-";
	}
	else{
	    $flexion = substr $flexion, 1;
	}
	
	if($cat eq "prep"){
	    $flexion='';
	}
	
	if($flexion =~ /([PFIJCYSTKGW]*)([123]*)([fm]*)([sp]*)/){
	    $tps = $1;
	    $pers = $2;
	    $gnr = $3;
	    $nb = $4;
	    
#	    print"tps = $tps pers = $pers gnr = $gnr nb = $nb\n";
	    
	}


	
	foreach $trait(@tab_morpho){
	    if($trait =~ /pred=\"([^_]*)/){
		$lemme = $1;
	    }
	    elsif($trait =~ /case=(.*)/){
		$cas = $1;
	    }
	    elsif($trait =~ /define=\+/){
		$type_det = "d";
	    }
	    elsif($trait =~ /define=-/){
		$type_det = "i";
	    }
	    elsif($trait =~ /demonstrative=+/){
		$type_det = "e";
	    }
	    elsif($trait =~ /\@poss/){
		$type_det = "o";
	    }
	}
	
	if($cat eq "ponctw")
	{
#	    print STDERR "cat = $cat lemme = $lemme form = $forme\n";
	    $lemme = $forme; 
	}
	    
	if($cat eq "cln")
	{
	    if(($forme  =~  /il/) || ($forme  =~  /elle/)){
		$lemme = "il";
	    }
	    if(($forme  =~  /on/)){
		$lemme = "on";
	    }
	}
	    
	if($cat eq "clr")
	{
	    if(($forme  =~  /se/) || ($forme  =~  /s\'/)){
		$lemme = "se";
	    }
	    if($forme  =~  /il/){
		$lemme = "il";
	    }
	}
	    
	if($cat eq "cla")
	{
	    if(($forme  =~  /il/) || ($forme  =~  /elle/)){
		$lemme = "il";
	    }
	    if(($forme  =~  /le/) || ($forme  =~  /la/)  || ($forme  =~  /l\'/)){
		$lemme = "le";
	    }
	}
	    
	if($cat eq "prel"){
	    $flexion =~ /pro_(.*)/;
	    $cas = $1;
	    $flexion = "";
	}
	
	elsif($cat eq "pri"){
	    $flexion =~ /pro_(.*)/;
	    $cas = $1;
	    $flexion = "";
	}
	
	if(($cat eq "prorel") || ($cat eq "cla") || ($cat eq "cln") || ($cat eq "clo") || ($cat eq "clr") || ($cat eq "clneg") || ($cat eq "pro") || ($cat eq "pri")){
	    if($cas eq "gen") {$cas = '';}
	    elsif($cas eq "loc") {$cas = '';}
	    elsif($cas eq "dat") {$cas = 'i';}
	    elsif($cas eq "acc") {$cas = 'o';}
	    elsif($cas eq "nom") {$cas = 'u';}
	    $flexion = $cas . $flexion;
	    $cas = "";
	}
	
	
	if($cat eq "det"){
	    $flexion =~ s/\_P([123])(p|s)/$1/;
	    if($flexion =~ /e/){$flexion = "";}
	    if($flexion =~ /\-/){$flexion = "";}
	    $flexion = $type_det . $flexion;
	}
	
	$flexion = $tps . '#' . $pers . '#' . $gnr . '#' . $nb . '#' . $cas . '#' . $type_det;
	if($lemme ne ":"){
	    $lemme =~ s/://;
	}
	$lemme =~ s/s'//;
	$lemme =~ s/se //;
	$lemme =~ s/\(se\) //;
	$lemme =~ s/\(\)//;
	$lemme =~ s/_-_/-/;
	$lemme =~ s/l\'_/l\'/;
	if(!$lemme){
	    $lemme = "_";
	}

#	if($lemme){
	    print "$forme\t$cat\t$lemme\t$flexion\n";
#	}
    }
    close(FILE);
}

