Name : Parser Machine
Dicts : parser.dicts
%CLASSIFIERS
parser parser.cla
%STATES
parser1 parser
%TRANSITIONS
parser1 parser1 0 LEFT
parser1 parser1 0 REDUCE
parser1 parser1 0 EOS
parser1 parser1 +1 SHIFT
parser1 parser1 +1 RIGHT
