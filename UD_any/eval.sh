#! /bin/bash

source config

function has_space {
  [[ "$1" != "${1%[[:space:]]*}" ]] && return 0 || return 1
}

function print_usage_and_exit {
  >&2 echo "USAGE : language_keyword templateName expName [arguments]"
  exit 1
}

LANG=UD_any
LANGPATH=$MACAON_DIR/$LANG
MCD=$LANGPATH/data/conllu.mcd
KEYWORD=$1
TEMPLATENAME=$2
EXPNAME=$3

if [ -z "$KEYWORD" ];
then
  >&2 echo "ERROR : missing argument 1 (keyword)"
  print_usage_and_exit
fi

if [ -z "$TEMPLATENAME" ];
then
  >&2 echo "ERROR : missing argument 2 (templateName)"
  print_usage_and_exit
fi

if [ -z "$EXPNAME" ];
then
  >&2 echo "ERROR : missing argument 3 (expName)"
  print_usage_and_exit
fi

shift
shift
shift
RAWINPUT=""
params=( $* )
index=0

for var in "$@"
do
  if [ "$var" = "--rawInput" ]
  then
    RAWINPUT="$var"
    unset params[$((index))]
    set -- "${params[@]}"
    break
  fi
  index=$((index+1))
done

if [ "$KEYWORD" = "." ]
then
  KEYWORD=""
fi

TRAIN=$(find $UD_ROOT*$KEYWORD -type f -name '*train*.conllu')
TRAINRAW=$(find $UD_ROOT*$KEYWORD -type f -name '*train*.txt')
DEV=$(find $UD_ROOT*$KEYWORD -type f -name '*dev*.conllu')
TEST=$(find $UD_ROOT*$KEYWORD -type f -name '*test*.conllu')
TESTRAW=$(find $UD_ROOT*$KEYWORD -type f -name '*test*.txt')

if has_space "$TRAIN" || has_space "$DEV" || has_space "$TEST";
then
  >&2 echo "ERROR : more than 1 match with keyword" $KEYWORD
  >&2 echo "TRAIN : " $TRAIN
  >&2 echo "DEV : " $DEV
  >&2 echo "TEST : " $TEST
  print_usage_and_exit
fi

if test ! -f $TRAIN;
then
  >&2 echo "ERROR : no train file found with keyword" $KEYWORD
  >&2 echo "$TRAIN"
  print_usage_and_exit
fi

if [ ! -z "$RAWINPUT" ];
then
  if [ ! -s "$TRAINRAW" ];
  then
    >&2 echo "ERROR : file is empty " $TRAINRAW
    >&2 echo "TIP : makes sure file has raw text metadata " $TRAIN
    print_usage_and_exit
  fi
fi

TEMPLATEPATH=$LANGPATH/$TEMPLATENAME

if [ ! -d "$TEMPLATEPATH" ]; then
  >&2 echo "ERROR : directory $TEMPLATEPATH doesn't exist"
  print_usage_and_exit
fi

EVALCONLL="../scripts/conll18_ud_eval.py"
ADDMISSINGCOLUMNS="../tools/conlluAddMissingColumns.py"

EXPPATH=$LANGPATH/bin/$EXPNAME
TOOL=$LANGPATH/bin/maca_tm_$EXPNAME

if [ -z "$RAWINPUT" ];
then
  echo "Evaluation on file" $TEST ":"
  $TOOL $TEST $MCD --interactive 0 > $EXPPATH/tmpOutTest.txt
else
  echo "Evaluation on file" $TESTRAW ":"
  $TOOL $TESTRAW $MCD --interactive 0 --rawInput > $EXPPATH/tmpOutTest.txt
fi

$ADDMISSINGCOLUMNS $EXPPATH/tmpOutTest.txt $MCD > $EXPPATH/tmpOutTest.conllu
$EVALCONLL $TEST $EXPPATH/tmpOutTest.conllu -v

