#!/usr/bin/perl




#################################################
#categories P7
#################################################

#ADJ
#ADJWH
#ADV
#ADVWH
#CC
#CLO
#CLR
#CLS
#CS
#DET
#DETWH
#ET
#I
#NC
#NPP
#P
#P+D
#PONCT
#P+PRO
#PREF
#PRO
#PROREL
#PROWH
#V
#VIMP
#VINF
#VPP
#VPR
#VS

#vers p7
#$h_pos{"adj"} = "ADJ";
#$h_pos{"adjPref"} = "ADJ";
#$h_pos{"adv"} = "ADV";
#$h_pos{"advm"} = "ADV";
#$h_pos{"advneg"} = "ADV";
#$h_pos{"advp"} = "ADV";
#$h_pos{"advPref"} = "ADV";
#$h_pos{"auxAvoir"} = "V";
#$h_pos{"auxEtre"} = "V";
#$h_pos{"caimp"} = "XX";
#$h_pos{"ce"} = "XX";
#$h_pos{"cf"} = "XX";
#$h_pos{"cfi"} = "XX";
#$h_pos{"cla"} = "XX";
#$h_pos{"clar"} = "XX";
#$h_pos{"cld"} = "XX";
#$h_pos{"cldr"} = "XX";
#$h_pos{"clg"} = "XX";
#$h_pos{"cll"} = "XX";
#$h_pos{"cln"} = "CLS";
#$h_pos{"clneg"} = "XX";
#$h_pos{"clr"} = "CLR";
#$h_pos{"coo"} = "CC";
#$h_pos{"csu"} = "CS";
#$h_pos{"det"} = "DET";
#$h_pos{"etr"} = "ETR";
#$h_pos{"ilimp"} = "XX";
#$h_pos{"meta"} = "XX";
#$h_pos{"nc"} = "NC";
#$h_pos{"np"} = "NPP";
#$h_pos{"parentf"} = "PONCT";
#$h_pos{"parento"} = "PONCT";
#$h_pos{"poncts"} = "PONCT";
#$h_pos{"ponctw"} = "PONCT";
#$h_pos{"prel"} = "PROREL";
#$h_pos{"prep"} = "P";
#$h_pos{"pres"} = "I";
#$h_pos{"pri"} = "PROWH";
#$h_pos{"pro"} = "PRO";
#$h_pos{"que"} = "XX";
#$h_pos{"que_restr"} = "XX";
#$h_pos{"sbound"} = "XX";
#$h_pos{"suffAdj"} = "XX";
#$h_pos{"v"} = "V";


#nc np adj adv clneg advneg cln clr clo coo csu det pres prep prorel pri pro etr v vppart vprespart vinf pref poncts ponctw


$h_pos{"adj"} = "adj";
$h_pos{"adjPref"} = "adj";
$h_pos{"adv"} = "adv";
$h_pos{"advm"} = "adv";
$h_pos{"advneg"} = "advneg";
$h_pos{"advp"} = "adv";
$h_pos{"advPref"} = "adv";
$h_pos{"auxAvoir"} = "XX";
$h_pos{"auxEtre"} = "XX";
$h_pos{"caimp"} = "XX";
$h_pos{"ce"} = "XX";
$h_pos{"cf"} = "XX";
$h_pos{"cfi"} = "XX";
$h_pos{"cla"} = "clo";
$h_pos{"clar"} = "XX";
$h_pos{"cld"} = "XX";
$h_pos{"cldr"} = "XX";
$h_pos{"clg"} = "XX";
$h_pos{"cll"} = "XX";
$h_pos{"cln"} = "cln";
$h_pos{"clneg"} = "advneg";
$h_pos{"clr"} = "clr";
$h_pos{"coo"} = "coo";
$h_pos{"csu"} = "csu";
$h_pos{"det"} = "det";
$h_pos{"etr"} = "etr";
$h_pos{"ilimp"} = "XX";
$h_pos{"meta"} = "XX";
$h_pos{"nc"} = "nc";
$h_pos{"np"} = "np";
$h_pos{"parentf"} = "ponctw";
$h_pos{"parento"} = "poncts";
$h_pos{"poncts"} = "poncts";
$h_pos{"ponctw"} = "ponctw";
$h_pos{"prel"} = "prorel";
$h_pos{"prep"} = "prep";
$h_pos{"pres"} = "pres";
$h_pos{"pri"} = "pri";
$h_pos{"pro"} = "pro";
$h_pos{"que"} = "XX";
$h_pos{"que_restr"} = "XX";
$h_pos{"sbound"} = "XX";
$h_pos{"suffAdj"} = "XX";
$h_pos{"v"} = "v";


while(<>){
    /([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/;
    my $forme = $1;
    my $cat = $2;
    my $cat_p7 = $h_pos{$cat};
    my $lemme = $3;
    my $morpho = $4;

    if($cat eq "v"){
	if($morpho =~ /^K/){
	    $cat_p7 = "vppart";
	} 
	elsif($morpho =~ /^W/){
	    $cat_p7 = "vinf";
	} 
	elsif($morpho =~ /^G/){
	    $cat_p7 = "vprespart";
	} 
    }


    if(($cat_p7) && ($cat_p7 ne "XX")){
	print"$forme\t$cat_p7\t$lemme\t$morpho";
    }
}
