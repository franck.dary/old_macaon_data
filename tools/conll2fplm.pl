#!/usr/bin/perl

my $use_coarse_pos = 0;

$arg = shift;
while($arg){
    if($arg eq "-f"){
	$conll = shift;
    }
    elsif($arg eq "-c"){
	$use_coarse_pos = 1;
    }
    elsif($arg eq "-h"){
	print "usage conll2fplm OPTIONS\n";
	print "reads a conll07 file on stdin and produces an fplm file\n";
	print "OPTIONS :\n";
	print "\t-h write this message\n";
#	print "\t-f conll file to process\n";
	print "\t-c use coarse part of speech tags (otherwise fine part of speech tags will be used)\n";
	exit;
    }

    $arg = shift;
}

#print "use_coarse_pos = $use_coarse_pos conll = $conll\n";

open(my $CONLL, "<", $conll)
	or die "Can't open < $conll: $!";

while(<$CONLL>){
    if(!/^#/){
    ($index, $form, $lemma, $cpos, $pos, $morpho, $gov, $label) = split /\t/;
    if($use_coarse_pos){
	$fplm = $form . "\t" . $cpos . "\t" . $lemma . "\t" . $morpho;
    }
    else{
	$fplm = $form . "\t" . $pos . "\t" . $lemma . "\t" . $morpho;
    }
    $hash{$fplm} = 1;
    }
}

close($CONLL);
foreach $fplm (keys %hash){
    print "$fplm\n";
}
