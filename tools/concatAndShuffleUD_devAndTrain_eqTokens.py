#!/usr/bin/python
# -*- coding: utf-8 -*-

# python ../../tools/concatAndShuffleUD_devAndTrain_eqTokens.py concatUDLang_codesLangConcatLang_dev concatUDLang_codesLangConcatLang_train 20000 > train.eqToken 2> dev.eqToken

from __future__ import print_function          
import fileinput
import sys
import re
import os
import random

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def transformGenericPathToRealPath(genericPath, curLang, codeCorpus):
    return genericPath.replace("**", curLang).replace("*", codeCorpus)
    
def fillSequences(filepathCodeLang, sequences, langues):
    
    codesFile = open(filepathCodeLang, "r",encoding="utf8")
    
    genericPathOfCorpusToConcat = codesFile.readline().rstrip() 
    while(lineIsAComment(genericPathOfCorpusToConcat)):
        genericPathOfCorpusToConcat = codesFile.readline().rstrip() 
        
    curLine = codesFile.readline().rstrip()
    
    while(not fileCompletelyRead(curLine)):
        cpt = 0
        if(lineIsAComment(curLine)):
            curLine = codesFile.readline().rstrip()
            continue
        
        curLine = curLine.split("\t")
        curLang = curLine[0]
        codeCorpus = curLine[1]
        codeLangue = codeCorpus.split("_")[0]
        
        
        filepathCurCorpus = transformGenericPathToRealPath(genericPathOfCorpusToConcat, curLang, codeCorpus)
        if(not os.path.exists(filepathCurCorpus)):
            sys.stderr.write("Warning, this filepath doesn't exist (%s)\n" % filepathCurCorpus)
            curLine = codesFile.readline().rstrip()
            continue
            
        if(codeLangue not in langues):
            langues.append(codeLangue)
            sequences[codeLangue] = []
            
        curCorpus = open(filepathCurCorpus, "r", encoding="utf8")
        lineCurCorpus = curCorpus.readline()
        while(not fileCompletelyRead(lineCurCorpus)):
            curSequence = ""
            while(isInASequence(lineCurCorpus)):
                if(not lineIsAComment(lineCurCorpus)):
                    curSequence += lineCurCorpus.rstrip()+ "\t"+ codeCorpus +"\n"
                elif('# text = ' in lineCurCorpus):
                    curSequenceTxt = lineCurCorpus.rstrip()[9:]
                lineCurCorpus = curCorpus.readline()
            lineCurCorpus = curCorpus.readline()
            sequences[codeLangue].append([curSequence, curSequenceTxt])
            
        curLine = codesFile.readline().rstrip()
    return sequences, langues

def nbTokenInSequence(sequence):
    nbTokens = 0
    sequence = sequence.split("\n")
    for line in sequence:
        if("-" in line.split("\t")[0] or line == "\n" or line == ""):
            continue
        nbTokens += 1
    return nbTokens
    
def printTrain(nbTokens, langues, sequences):
    sequencesToPrint = []
    stringToPrintTxtFormat = ""
    for curLangue in langues:
        cpt = 0
        while(cpt < nbTokens):
            curSequence = random.randint(0, int(9.0*len(sequences[curLangue])/10.0))
            sequencesToPrint.append(sequences[curLangue][curSequence])
            cpt+=nbTokenInSequence(sequences[curLangue][curSequence][0])
    random.shuffle(sequencesToPrint)
    
    sys.stdout=open("train.eqTokens.conllu","w", encoding="utf8")
    for sequence in sequencesToPrint:
       print(sequence[0])
       stringToPrintTxtFormat += sequence[1] + " "
    sys.stdout.close()

    sys.stdout=open("train.eqTokens.txt","w", encoding="utf8")
    print(stringToPrintTxtFormat[:-1])
    sys.stdout.close()
    
def printDev(nbTokens, langues, sequences):
    sequencesToPrint = []
    stringToPrintTxtFormat = ""
    for curLangue in langues:
        cpt = 0
        while(cpt < nbTokens/10):
            curSequence = random.randint(int(len(sequences[curLangue])/10.0)+1, len(sequences[curLangue])-1)
            sequencesToPrint.append(sequences[curLangue][curSequence])
            cpt+=nbTokenInSequence(sequences[curLangue][curSequence][0])
    random.shuffle(sequencesToPrint)
    
    sys.stdout=open("dev.eqTokens.conllu","w", encoding="utf8")
    for sequence in sequencesToPrint:
        print(sequence[0])
        stringToPrintTxtFormat += sequence[1] + " "
    sys.stdout.close()

    sys.stdout=open("dev.eqTokens.txt","w", encoding="utf8")
    print(stringToPrintTxtFormat[:-1])
    sys.stdout.close()
        
def main():
    
    if(len(sys.argv) < 4):
        print("Error, give two files with the code of languages you want to concat and the number of tokens you want per languages in the training corpora (the dev will be 10 times inferior). In the file with code, the first line is reserved to give the path where are the corpus, with a * instead of the code of the corpus (example : ../data_ST2017/ud-treebanks/*-ud-train.conllu) and you can add at the end an optional option '-i' to ignore and don't add the code of the corpus at the end of the line. The output will be automatically store in train.eqTokens.conllu, train.eqTokens.txt, dev.eqTokens.conllu and dev.eqTokens.txt")
        exit(1)
        
    filepathCodeLang1 = sys.argv[1] #contient sur la première ligne le chemin vers les différents dossiers des UD, avec une * à la place du code des langues, et sur les lignes suivantes (un par ligne) le code des langues souhaitées
    filepathCodeLang2 = sys.argv[2] #contient sur la première ligne le chemin vers les différents dossiers des UD, avec une * à la place du code des langues, et sur les lignes suivantes (un par ligne) le code des langues souhaitées
    langues = []
    sequences = dict()
        
    nbTokens = int(sys.argv[3])
    
    sequences, langues = fillSequences(filepathCodeLang1, sequences, langues)

    #for langue in langues :
        #sys.stderr.write("%s\t%d\n" %( langue, len(sequences[langue])))
        
    sequences, langues = fillSequences(filepathCodeLang2, sequences, langues)

    #nbSequences = len(langues)*maxSentences

    for langue in langues :
        random.shuffle(sequences[langue])

    printTrain(nbTokens, langues, sequences)
    printDev(nbTokens, langues, sequences)

        


main()
        
