#! /usr/bin/python3

import subprocess
import sys
import os

def printUsageAndExit() :
  print("Usage : eval.sh expName1 expName2...")
  exit(1)

if len(sys.argv) < 5 :
  printUsageAndExit()

lang = sys.argv[1]
test = sys.argv[2]
testText = test[:-6]+"txt"
mcd = sys.argv[3]
experiences = []
debug = ""
evalArgs = " "
beamSize = ""
readSize = ""
rawInput = ""

binpath = os.environ["MACAON_DIR"] + "/" + lang + "/bin"
evalConll = "../../scripts/conll18_ud_eval.py"
addMissingColumns = "../../tools/conlluAddMissingColumns.py"
result_file = lang + ".res"
output = "output.txt"
err = "stderr.log"

toskip = False
for i in range(4, len(sys.argv)) :
  if toskip :
    toskip = False
    continue
  arg = sys.argv[i]
  if arg == "-d" or arg == "--debug" :
    debug = "-d"
  elif arg == "--rawInput" :
    rawInput = " --rawInput "
  elif arg == "--beamSize" :
    beamSize = " --beamSize " + sys.argv[i+1] + " "
    toskip = True
  elif arg == "--readSize" :
    readSize = " --readSize " + sys.argv[i+1] + " "
    toskip = True
  else :
    if arg.startswith("--") :
      for j in range(i, len(sys.argv)) :
        evalArgs += sys.argv[j] + " "
      break
    experiences += [arg.split('+')]

for experience in experiences :
  if experience[0][-1] == '*' :
    name = experience[0]
    experiences.remove(experience)
    i = 0
    while os.path.isdir(binpath + "/" + name[:-1] + str(i)) :
      experiences.append([name[:-1] + str(i)])
      i += 1

firstWrite = True

for experience in experiences :
  abridged_name = ""
  for tool in experience :
    abridged_name += tool[0] + "+"

  abridged_name = abridged_name[:len(abridged_name) - 1]

  if len(experience) == 1 :
    abridged_name = experience[0]

  print("Evaluation of",abridged_name,"...",end="")
  sys.stdout.flush()

  input_file = test
  if len(rawInput) > 0 :
    input_file = testText
  error_occured = False

  for exp in experience :
    process = subprocess.Popen(binpath + "/maca_tm_" + exp + " " + input_file + " " + mcd + " " + debug + beamSize + readSize + rawInput + "--interactive 0 > " + output + " 2> " + err, shell=True)
    process.wait()
    subprocess.Popen(addMissingColumns + " " + output + " " + mcd + " > tmp_input", shell=True).wait()
    input_file = "tmp_input"
    if process.returncode != 0 :
      print(" ERROR (see " + err + " for details)")
      error_occured = True
      break

  if not error_occured :
    redirect = " >> "
    header = ""
    if firstWrite :
      redirect = " > "
      header = " --printHeader "
    toolname = " --toolname " + abridged_name + " "
    process = subprocess.Popen(evalConll + " " + test + " " + input_file + " -v " + evalArgs + redirect + result_file + " 2>> " + err, shell=True)
    process.wait()
    if process.returncode != 0 :
      print(" ERROR (see " + err + " for details)")
      error_occured = True
      break
    print(" Done !")
    firstWrite = False

subprocess.Popen("rm " + output + "* tmp_input", shell=True).wait()
    
subprocess.Popen("cat " + result_file + " > " + output + " && mv " + output + " " + result_file, shell=True).wait()
