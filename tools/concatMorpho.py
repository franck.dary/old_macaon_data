#!/usr/bin/python
# -*- coding: utf-8 -*-

#

from __future__ import print_function          
import fileinput
import sys
import re
import os
import numpy
import re

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def main():
    
    if(len(sys.argv) != 4):
        print("Error, give the file to modified, its mcd, then give the new column for the concatenated morphology.")
        exit(1)
    
    newColMorpho = int(sys.argv[3])
    listColMorpho = []
    MCDlist = []
    with open(sys.argv[2], "r") as mcd:
        for curLine in mcd:
            if(lineIsAComment(curLine) or not isInASequence(curLine)):
                continue
            curMorpho = curLine.rstrip().split(" ")[1]
            if(re.match("M\d+",curMorpho)):            
                listColMorpho.append(int(curLine.rstrip().split(" ")[0]))
            else:
                MCDlist.append(curMorpho)
    MCDlist.insert(newColMorpho, "MORPHO")

    with open(sys.argv[1], "r") as conllu:
        for curLine in conllu:
            curLine = curLine.rstrip().split("\t")
            morphoOrig = [curLine[i] for i in listColMorpho]
            
            morphoOrig = list(filter(lambda a: a != "_", morphoOrig))
            
            newMorpho = '|'.join(morphoOrig)
            newLine = []
            for i in range(len(curLine)):
                if i not in listColMorpho:
                    newLine.append(curLine[i])
            #curLine = [curLine[i] for i not in listColMorpho] #map(lambda x: curLine.pop(x), sorted(listColMorpho, key = lambda x:-x))
            
            newLine.insert(newColMorpho, newMorpho)

            print("\t".join(newLine))
            
    for i in range(len(MCDlist)):
        sys.stderr.write("%s %s\n" %(i,MCDlist[i]))

main()
        
