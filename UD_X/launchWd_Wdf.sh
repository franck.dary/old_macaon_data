#!/bin/bash

set -e
set -u
set -o pipefail

# SIGMA+Wd
oargen -n SIGMA_Wd -d outputCluster -t 96 --run ./train.sh -w d
sleep 500 

# SIGMA+Wdf
oargen -n SIGMA_Wdf -d outputCluster -t 96 --run ./train.sh -w df

