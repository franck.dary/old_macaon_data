#! /usr/bin/python

import sys

threshold = 10

def printUsageAndExit() :
  print("USAGE :", sys.argv[0], "extractedFeatures file1.fm file2.fm...")
  exit(1)

def main() :
  if len(sys.argv) < 3 :
    printUsageAndExit()

  extractedFilename = sys.argv[1]
  fms = {}
  extracted = {}
  for i in range(2, len(sys.argv)) :
    filename = sys.argv[i].split('/')[-1]
    fms[filename] = []
    for line in open(sys.argv[i], "r") :
      if len(line) < 2 :
        continue
      if line[0] == '#' :
        continue
      fms[filename].append(line[:-1])

  for line in open(extractedFilename, "r") :
    if len(line) < 2 :
      continue

    splited = line.strip().split('\t')

    if len(splited) < 3 :
      print("ERROR : invalid line \'%s\'. Aborting."%(line), file=sys.stderr)
      exit(1)

    fm = splited[0]
    action = splited[1]
    features = splited[2:]

    if fm not in fms :
      print("ERROR : invalid feature model \'%s\', not found in arguments (\'%s\'). Aborting."%(fm, sys.argv[2:]), file=sys.stderr)
      exit(1)

    if len(fms[fm]) != len(features) :
      print("ERROR : invalid number of features, read %d expected %d.\n%s"%(len(features), len(fms[fm]),line), file=sys.stderr)
      print(fms[fm])
      exit(1)

    for i in range(len(features)) :
      specializedFeat = fm + ":" + fms[fm][i]

      if specializedFeat not in extracted :
        extracted[specializedFeat] = {}

      if features[i] not in extracted[specializedFeat] :
        extracted[specializedFeat][features[i]] = 0
      extracted[specializedFeat][features[i]] += 1

  for feature in extracted :
    total = 0
    for value in extracted[feature] :
      total += extracted[feature][value]

    print("%s (%d) :"%(feature, len(extracted[feature])))
    if len(extracted[feature]) > threshold :
      print("")
      continue

    for value in extracted[feature] :
      print("\t(%5.2f%%) %s"%(100.0*extracted[feature][value]/total, value))


main()
