Name : Tagger, Morpho, Lemmatizer, Parser with error correction
Dicts : tagparser.dicts
%CLASSIFIERS
tagger tagger.cla
morpho morpho.cla
signature signature.cla
lemma_lookup lemmatizer_lookup.cla
lemma_rules lemmatizer_rules.cla
parser parser.cla
error_tagger error_tagger.cla
error_morpho error_morpho.cla
error_parser error_parser.cla
%STATES
signature signature
tagger tagger
error_tagger error_tagger
morpho morpho
error_morpho error_morpho
lemma_lookup lemma_lookup
lemma_rules lemma_rules
parser parser
error_parser_stay error_parser
error_parser_move error_parser
%TRANSITIONS
signature tagger 0 *
tagger error_tagger 0 *
error_tagger tagger 0 BACK
error_tagger morpho 0 *
morpho error_morpho 0 *
error_morpho morpho 0 BACK
error_morpho lemma_lookup 0 *
lemma_lookup parser 0 *
lemma_lookup lemma_rules 0 NOTFOUND
lemma_rules parser 0 *
parser error_parser_stay 0 LEFT
parser error_parser_stay 0 EOS
parser error_parser_stay 0 REDUCE
parser error_parser_stay 0 ROOT
parser error_parser_move 0 SHIFT
parser error_parser_move 0 RIGHT
error_parser_stay parser 0 *
error_parser_move parser +1 BACK
error_parser_move signature +1 *
