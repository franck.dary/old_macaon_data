#!/usr/bin/env python2
# merge two wordtag lexica
# the second lexicon is used to complement the first ;
# each word in the first lexicon is associated to the
# union of the tags from the first and second lexica
import sys
from collections import defaultdict

wt_lexicon1 = sys.argv[1]
wt_lexicon2 = sys.argv[2]

wt_dict = {}
# create the dict for lexicon1
with open(wt_lexicon1, encoding="utf8") as f1:
    for line in f1:
        line = line.strip()
        cont = line.split('\t')
        w = cont[0]
        ts = set(cont[1:])
        wt_dict[w] = ts
# enrich with info from lexicon2
with open(wt_lexicon2, encoding="utf8") as f2:
    for line in f2:
        line = line.strip()
        cont = line.split('\t')
        w = cont[0]
        ts = set(cont[1:])
        if w in wt_dict:
            wt_dict[w].update(ts)
# output the result
for w,ts in sorted(wt_dict.items()):
    sts = sorted(ts)
    print "{w}\t{ts}".format(w=w, ts='\t'.join(sts))
