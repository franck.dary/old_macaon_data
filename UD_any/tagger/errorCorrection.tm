Name : Tagger with error correction
Dicts : tagger.dicts
%CLASSIFIERS
strategy strategy.cla
tagger tagger.cla
error_tagger error_tagger.cla
%STATES
strategy strategy
tagger tagger
error_tagger.cla
%TRANSITIONS
strategy tagger MOVE tagger
tagger error_tagger *
error_tagger tagger BACK
error_tagger strategy *
