Name : Morpho with error correction
Dicts : morpho.dicts
%CLASSIFIERS
strategy strategy.cla
morpho morpho.cla
error_morpho error_morpho.cla
%STATES
morpho morpho
strategy strategy
error_morpho error_morpho
%TRANSITIONS
morpho error_morpho *
error_morpho morpho BACK
error_morpho strategy *
strategy morpho *
