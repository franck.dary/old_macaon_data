#!/usr/bin/python3
# -*- coding: utf-8 -*-


import fileinput
import sys
import re
import csv
import os
import string

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
if __name__ == '__main__':
    
    if(len(sys.argv) < 3):
        sys.stderr.write("Error, give the file featWALS2keep and the basic mcd, and optionnaly the --IDcol=nbColId. nbColId could be =end if it is simply in the last column")
        exit(1)
    filepathFEATS = sys.argv[1]
    filepathBasicMCD = sys.argv[2]
    if(len(sys.argv) > 3):
        if(sys.argv[3].split("=")[0] == "--IDcol"):
            if(sys.argv[3].split("=")[1] == "end"):
                nbColID = 0
            else:
                nbColID = int(sys.argv[3].split("=")[1])-1
        else:
            sys.stderr.write("You gave the %s non recognize option" %(sys.argv[3].split("=")[0]))
            exit(1)
    else:
        nbColID = -1
        
    listMCD = []
    with open(filepathBasicMCD, "r", encoding="utf8") as mcd:
        curLine = mcd.readline().strip()
        while(not fileCompletelyRead(curLine)):
            if(lineIsAComment(curLine)):
                curLine = mcd.readline().strip()
                continue
            curLine = curLine.split(" ")
            listMCD.append(curLine[1])
            curLine = mcd.readline().strip()
    listFeats = []
    with open(filepathFEATS, "r", encoding="utf8") as FEATSfile:
        curLine = FEATSfile.readline().strip()
        while(not fileCompletelyRead(curLine)):
            if(not lineIsAComment(curLine)):
                listFeats.append(str(curLine))
            curLine = FEATSfile.readline().strip()
            
    listMCDoutput = listFeats + listMCD

    if(nbColID != -1 and nbColID > len(listMCDoutput)):
        sys.stderr.write("You gave a wrong column number for arg IDcol (%s, but the current number of column is %s)" %(nbColID,len(listMCDoutput)))
        exit(1)

    if(nbColID == 0):
        nbColID = len(listMCDoutput)
    
    if(nbColID != -1):
        listMCDoutput.insert(nbColID, "ID")
    for i in range(len(listMCDoutput)):
        print(i, listMCDoutput[i])
    print()

    
