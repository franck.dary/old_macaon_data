#! /bin/bash

TRAIN=../../data/train_tiny.conllu
DEV=../../data/dev_tiny.conllu

if [ "$2" == "-h" ]; then
  macaon_train "-h"
  exit
fi

if [ "$#" -lt 3 ]; then
 echo "Usage : train.sh templateName expName"
 exit
fi

LANG=$1
TEMPLATENAME=$2
EXPNAME=$3

# Here we collect all the extra arguments, to forward them to macaon_train
shift
shift
ARGS=""
for arg in "$@"
do
  ARGS="$ARGS $arg"
done

#if MACAON_DIR is not set, throw an error
if [ ! -e "$MACAON_DIR" ]; then
  echo "ERROR : MACAON_DIR is empty."
  exit
fi

LANGPATH=$MACAON_DIR/$LANG
TEMPLATEPATH=$LANGPATH/$TEMPLATENAME
EXPPATH=$LANGPATH/bin/$EXPNAME

mkdir -p $LANGPATH/bin

if [ ! -d "$TEMPLATEPATH" ]; then
 echo "ERROR : directory $TEMPLATEPATH doesn't exist"
 echo "Usage : train.sh templateName expName"
 exit
fi

# We start the training
macaon_train --tm machine.tm --bd train.bd --mcd ../../data/conllu.mcd -T $TRAIN --dev $DEV --expName $EXPNAME --lang $LANG $ARGS --templateName $TEMPLATENAME
