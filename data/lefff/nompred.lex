abstraction	100	cfi	[pred="abstraction_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	abstraction_____1	Default		%default
accès	100	cfi	[pred="accès_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	accès_____1	Default		%default
accès	100	cfi	[pred="accès_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	accès_____2	Default		%default
acte	100	cfi	[pred="acte_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	acte_____1	Default		%default
acte	100	cfi	[pred="acte_____2<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	acte_____2	Default		%default
affaire	100	cfi	[pred="affaire_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	affaire_____1	Default		%default
affaire	100	cfi	[pred="affaire_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	affaire_____2	Default		%default
alliance	100	cfi	[pred="alliance_____1<Suj:cln|scompl|sinf|sn,Obl:avec-sn>",lightverb=faire]	alliance_____1	Default		%default
allusion	100	cfi	[pred="allusion_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|y>",lightverb=faire]	allusion_____1	Default		%default
appel	100	cfi	[pred="appel_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|y>",lightverb=faire]	appel_____1	Default		%default
appui	100	cfi	[pred="appui_____1<Suj:cln|scompl|sinf|sn,Loc:sur-sn|y>",lightverb=prendre]	appui_____1	Default		%default
asile	100	cfi	[pred="asile_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	asile_____1	Default		%default
attention	100	cfi	[pred="attention_____1<Suj:cln|scompl|sinf|sn,Objà:scompl|à-scompl|à-sinf|à-sn>",lightverb=faire]	attention_____1	Default		%default
autorité	100	cfi	[pred="autorité_____1<Suj:cln|scompl|sinf|sn,Obl:sur-sn>",lightverb=avoir]	autorité_____1	Default		%default
autorité	100	cfi	[pred="autorité_____2<Suj:cln|scompl|sinf|sn>",lightverb=faire]	autorité_____2	Default		%default
avantage	100	cfi	[pred="avantage_____1<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	avantage_____1	Default		%default
avantage	100	cfi	[pred="avantage_____2<Suj:cln|scompl|sinf|sn,Objde:de-sn|de-scompl|de-sinf|en>",@CtrlSujObjde,lightverb=tirer]	avantage_____2	Default		%default
banqueroute	100	cfi	[pred="banqueroute_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	banqueroute_____1	Default		%default
besoin	100	cfi	[pred="besoin_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	besoin_____1	Default		%default
bloc	100	cfi	[pred="bloc_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	bloc_____1	Default		%default
boule de neige	100	cfi	[pred="boule de neige_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	boule de neige_____1	Default		%default
cadeau	100	cfi	[pred="cadeau_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en,Objà:à-sn|cld>",lightverb=faire]	cadeau_____1	Default		%default
campagne	100	cfi	[pred="campagne_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	campagne_____1	Default		%default
carrière	100	cfi	[pred="carrière_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	carrière_____1	Default		%default
carte blanche	100	cfi	[pred="carte blanche_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	carte blanche_____1	Default		%default
cas	100	cfi	[pred="cas_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	cas_____1	Default		%default
chaud	100	cfi	[pred="chaud_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	chaud_____1	Default		%default
choix	100	cfi	[pred="choix_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf|de-sn|en>",lightverb=faire]	choix_____1	Default		%default
compte	100	cfi	[pred="compte_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en|de-scompl>",lightverb=tenir]	compte_____1	Default		%default
compte	100	cfi	[pred="se rendre compte_____2<Suj:cln|sn,Objde:de-sn|en|de-scompl>",lightverb=se_rendre]	compte_____2	Default		%default
concurrence	100	cfi	[pred="concurrence_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld>",lightverb=faire]	concurrence_____1	Default		%default
confiance	100	cfi	[pred="confiance_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	confiance_____1	Default		%default
confiance	100	cfi	[pred="confiance_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	confiance_____2	Default		%default
confiance	100	cfi	[pred="confiance_____3<Suj:cln|scompl|sinf|sn,Obl:en-sn>",lightverb=prendre|avoir]	confiance_____3	Default		%default
congé	100	cfi	[pred="congé_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|de-sinf|en>",lightverb=prendre]	congé_____1	Default		%default
congé	100	cfi	[pred="congé_____2<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	congé_____2	Default		%default
congé	100	cfi	[pred="congé_____3<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	congé_____3	Default		%default
connaissance	100	cfi	[pred="connaissance_____1<Suj:cln|scompl|sinf|sn,Obl:avec-sn>",lightverb=faire]	connaissance_____1	Default		%default
connaissance	100	cfi	[pred="connaissance_____2<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	connaissance_____2	Default		%default
connaissance	100	cfi	[pred="connaissance_____3<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	connaissance_____3	Default		%default
conscience	100	cfi	[pred="conscience_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=prendre]	conscience_____1	Default		%default
conscience	100	cfi	[pred="conscience_____2<Suj:cln|scompl|sinf|sn,Objde:de-sinf|de-sn|en|de-scompl>",lightverb=avoir]	conscience_____2	Default		%default
conseil	100	cfi	[pred="conseil_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	conseil_____1	Default		%default
consensus	100	cfi	[pred="consensus_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	consensus_____1	Default		%default
contact	100	cfi	[pred="contact_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	contact_____1	Default		%default
corps	100	cfi	[pred="corps_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	corps_____1	Default		%default
corps	100	cfi	[pred="corps_____2<Suj:cln|scompl|sinf|sn,Obl:avec-sn>",lightverb=faire]	corps_____2	Default		%default
corps	100	cfi	[pred="corps_____3<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	corps_____3	Default		%default
courage	100	cfi	[pred="courage_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	courage_____1	Default		%default
cours	100	cfi	[pred="cours_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	cours_____1	Default		%default
cours	100	cfi	[pred="cours_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	cours_____2	Default		%default
coutume	100	cfi	[pred="coutume_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	coutume_____1	Default		%default
crédit	100	cfi	[pred="crédit_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld>",lightverb=faire]	crédit_____1	Default		%default
cure	100	cfi	[pred="cure_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	cure_____1	Default		%default
date	100	cfi	[pred="date_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre|faire]	date_____1	Default		%default
dessein	100	cfi	[pred="dessein_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	dessein_____1	Default		%default
diversion	100	cfi	[pred="diversion_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	diversion_____1	Default		%default
don	100	cfi	[pred="don_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	don_____1	Default		%default
droit	100	cfi	[pred="droit_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en,Objà:à-sn>",lightverb=faire]	droit_____1	Default		%default
droit	100	cfi	[pred="droit_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	droit_____2	Default		%default
droit	100	cfi	[pred="droit_____3<Suj:cln|scompl|sinf|sn,Objde:de-sinf,Objà:à-sn>",lightverb=donner]	droit_____3	Default		%default
défaut	100	cfi	[pred="défaut_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld>",lightverb=faire]	défaut_____1	Default		%default
effet	100	cfi	[pred="effet_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	effet_____1	Default		%default
effet	100	cfi	[pred="effet_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	effet_____2	Default		%default
en compte	100	cfi	[pred="en compte_____1<Suj:cln|scompl|sinf|sn,Obj:cla|sn|scompl>",lightverb=prendre]	en compte_____1	Default		%default
en main	100	cfi	[pred="en main_____1<Suj:cln|scompl|sinf|sn,Obj:cla|sn>",lightverb=prendre]	en main_____1	Default		%default
enquête	100	cfi	[pred="enquête_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	enquête_____1	Default		%default
envie	100	cfi	[pred="envie_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	envie_____1	Default		%default
envie	100	cfi	[pred="envie_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	envie_____2	Default		%default
envie	100	cfi	[pred="envie_____3<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	envie_____3	Default		%default
envie	100	cfi	[pred="envie_____4<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=donner]	envie_____4	Default		%default
erreur	100	cfi	[pred="erreur_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	erreur_____1	Default		%default
espoir	100	cfi	[pred="espoir_____1<Suj:cln|scompl|sinf|sn,Obl:en-sn>",lightverb=prendre]	espoir_____1	Default		%default
espoir	100	cfi	[pred="espoir_____2<Suj:cln|scompl|sinf|sn,Obj:scompl>",lightverb=avoir]	espoir_____2	Default		%default
exception	100	cfi	[pred="exception_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|y>",lightverb=faire]	exception_____1	Default		%default
exemple	100	cfi	[pred="exemple_____1<Suj:cln|scompl|sinf|sn,Loc:sur-sn|y>",lightverb=prendre]	exemple_____1	Default		%default
face	100	cfi	[pred="face_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld|y>",lightverb=faire]	face_____1	Default		%default
faillite	100	cfi	[pred="faillite_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	faillite_____1	Default		%default
faim	100	cfi	[pred="faim_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	faim_____1	Default		%default
fait et cause	100	cfi	[pred="fait et cause_____1<Suj:cln|scompl|sinf|sn,Obl:pour-sn|contre-sn>",lightverb=prendre]	fait et cause_____1	Default		%default
feu	100	cfi	[pred="feu_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre|faire]	feu_____1	Default		%default
figure	100	cfi	[pred="figure_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	figure_____1	Default		%default
fin	100	cfi	[pred="fin_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	fin_____1	Default		%default
foi	100	cfi	[pred="foi_____1<Suj:cln|scompl|sinf|sn,Obl:dans-sn|en-sn>",lightverb=avoir]	foi_____1	Default		%default
foi	100	cfi	[pred="foi_____2<Suj:cln|scompl|sinf|sn>",lightverb=faire]	foi_____2	Default		%default
forme	100	cfi	[pred="forme_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	forme_____1	Default		%default
fortune	100	cfi	[pred="fortune_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	fortune_____1	Default		%default
froid	100	cfi	[pred="froid_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	froid_____1	Default		%default
front	100	cfi	[pred="front_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	front_____1	Default		%default
fureur	100	cfi	[pred="fureur_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	fureur_____1	Default		%default
gain de cause	100	cfi	[pred="gain de cause_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	gain de cause_____1	Default		%default
gain de cause	100	cfi	[pred="gain de cause_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	gain de cause_____2	Default		%default
garde	100	cfi	[pred="garde_____1<Suj:cln|scompl|sinf|sn,Objà:scompl|à-scompl|à-sinf|à-sn>",lightverb=prendre]	garde_____1	Default		%default
gloire	100	cfi	[pred="gloire_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	gloire_____1	Default		%default
goût	100	cfi	[pred="goût_____1<Suj:cln|scompl|sinf|sn,Objà:scompl|à-scompl|à-sinf|à-sn>",lightverb=prendre]	goût_____1	Default		%default
grâce	100	cfi	[pred="grâce_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en,Objà:à-sn|cld>",lightverb=faire]	grâce_____1	Default		%default
grève	100	cfi	[pred="grève_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	grève_____1	Default		%default
haleine	100	cfi	[pred="haleine_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	haleine_____1	Default		%default
halte	100	cfi	[pred="halte_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	halte_____1	Default		%default
honneur	100	cfi	[pred="honneur_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld|y>",lightverb=faire]	honneur_____1	Default		%default
honte	100	cfi	[pred="honte_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	honte_____1	Default		%default
honte	100	cfi	[pred="honte_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	honte_____2	Default		%default
horreur	100	cfi	[pred="horreur_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	horreur_____1	Default		%default
horreur	100	cfi	[pred="horreur_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	horreur_____2	Default		%default
hâte	100	cfi	[pred="hâte_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	hâte_____1	Default		%default
idée	100	cfi	[pred="idée_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	idée_____1	Default		%default
illusion	100	cfi	[pred="illusion_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	illusion_____1	Default		%default
instruction	100	cfi	[pred="instruction_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf,Objà:à-sn>",lightverb=donner]	instruction_____1	Default		%default
intérêt	100	cfi	[pred="intérêt_____1<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	intérêt_____1	Default		%default
irruption	100	cfi	[pred="irruption_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	irruption_____1	Default		%default
jour	100	cfi	[pred="jour_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre|faire]	jour_____1	Default		%default
justice	100	cfi	[pred="justice_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	justice_____1	Default		%default
lecture	100	cfi	[pred="lecture_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en,Objà:à-sn>",lightverb=donner]	lecture_____1	Default		%default
lecture	100	cfi	[pred="lecture_____2<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	lecture_____2	Default		%default
lieu	100	cfi	[pred="lieu_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	lieu_____1	Default		%default
lieu	100	cfi	[pred="lieu_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	lieu_____2	Default		%default
lieu	100	cfi	[pred="lieu_____3<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=tenir]	lieu_____3	Default		%default
livraison	100	cfi	[pred="livraison_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	livraison_____1	Default		%default
loi	100	cfi	[pred="loi_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	loi_____1	Default		%default
maille à partir	100	cfi	[pred="maille à partir_____1<Suj:cln|scompl|sinf|sn,Obl:avec-sn>",lightverb=avoir]	maille à partir_____1	Default		%default
marre	100	cfi	[pred="marre_____1<Suj:cln|scompl|sinf|sn,Objde:(de-sn|de-scompl|de-sinf)>en",@CtrlSujObjde,lightverb=avoir]	marre_____1	Default		%default
matière	100	cfi	[pred="matière_____1<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	matière_____1	Default		%default
matière	100	cfi	[pred="matière_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	matière_____2	Default		%default
mention	100	cfi	[pred="mention_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	mention_____1	Default		%default
merveille	100	cfi	[pred="merveille_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	merveille_____1	Default		%default
mine	100	cfi	[pred="mine_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	mine_____1	Default		%default
mission	100	cfi	[pred="mission_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	mission_____1	Default		%default
montre	100	cfi	[pred="montre_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	montre_____1	Default		%default
mystère	100	cfi	[pred="mystère_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	mystère_____1	Default		%default
naissance	100	cfi	[pred="naissance_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	naissance_____1	Default		%default
naissance	100	cfi	[pred="naissance_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	naissance_____2	Default		%default
naufrage	100	cfi	[pred="naufrage_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	naufrage_____1	Default		%default
note	100	cfi	[pred="note_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	note_____1	Default		%default
obstacle	100	cfi	[pred="obstacle_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld|y>",lightverb=faire]	obstacle_____1	Default		%default
obstruction	100	cfi	[pred="obstruction_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld|y>",lightverb=faire]	obstruction_____1	Default		%default
occasion	100	cfi	[pred="occasion_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	occasion_____1	Default		%default
oeuvre	100	cfi	[pred="oeuvre_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	oeuvre_____1	Default		%default
office	100	cfi	[pred="office_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	office_____1	Default		%default
ordre	100	cfi	[pred="ordre_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf,Objà:à-sn>",lightverb=donner]	ordre_____1	Default		%default
ordre	100	cfi	[pred="ordre_____2<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	ordre_____2	Default		%default
part	100	cfi	[pred="part_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	part_____1	Default		%default
part	100	cfi	[pred="part_____2<Suj:cln|scompl|sinf|sn,Objà:scompl|à-scompl|à-sinf|à-sn>",lightverb=prendre]	part_____2	Default		%default
part	100	cfi	[pred="part_____3<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	part_____3	Default		%default
parti	100	cfi	[pred="parti_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	parti_____1	Default		%default
parti	100	cfi	[pred="parti_____2<Suj:cln|scompl|sinf|sn,Objde:de-sn|de-scompl|de-sinf|en>",@CtrlSujObjde,lightverb=tirer]	parti_____2	Default		%default
partie	100	cfi	[pred="partie_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	partie_____1	Default		%default
patience	100	cfi	[pred="patience_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	patience_____1	Default		%default
peine	100	cfi	[pred="peine_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	peine_____1	Default		%default
peine	100	cfi	[pred="peine_____2<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	peine_____2	Default		%default
peur	100	cfi	[pred="peur_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	peur_____1	Default		%default
peur	100	cfi	[pred="peur_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	peur_____2	Default		%default
peur	100	cfi	[pred="peur_____3<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	peur_____3	Default		%default
pied	100	cfi	[pred="pied_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	pied_____1	Default		%default
pignon sur rue	100	cfi	[pred="pignon sur rue_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	pignon sur rue_____1	Default		%default
pitié	100	cfi	[pred="pitié_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	pitié_____1	Default		%default
pitié	100	cfi	[pred="pitié_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	pitié_____2	Default		%default
place	100	cfi	[pred="place_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	place_____1	Default		%default
place	100	cfi	[pred="place_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	place_____2	Default		%default
plaisir	100	cfi	[pred="plaisir_____1<Suj:cln|scompl|sinf|sn,Objà:scompl|à-scompl|à-sinf|à-sn>",lightverb=prendre]	plaisir_____1	Default		%default
plaisir	100	cfi	[pred="plaisir_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	plaisir_____2	Default		%default
plaisir	100	cfi	[pred="plaisir_____3<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	plaisir_____3	Default		%default
position	100	cfi	[pred="position_____1<Suj:cln|scompl|sinf|sn,Obl:(sur-sn),Obl2:(pour-sn|contre-sn)>",lightverb=prendre]	position_____1	Default		%default
possession	100	cfi	[pred="possession_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=prendre]	possession_____1	Default		%default
possibilité	100	cfi	[pred="possibilité_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	possibilité_____1	Default		%default
preuve	100	cfi	[pred="preuve_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	preuve_____1	Default		%default
priorité	100	cfi	[pred="priorité_____1<Suj:cln|scompl|sinf|sn,Obl:sur-sn>",lightverb=avoir]	priorité_____1	Default		%default
priorité	100	cfi	[pred="priorité_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	priorité_____2	Default		%default
prise	100	cfi	[pred="prise_____1<Suj:cln|scompl|sinf|sn,Obl:sur-sn>",lightverb=avoir]	prise_____1	Default		%default
problème	100	cfi	[pred="problème_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	problème_____1	Default		%default
profession	100	cfi	[pred="profession_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	profession_____1	Default		%default
profit	100	cfi	[pred="profit_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|de-scompl|de-sinf|en>",@CtrlSujObjde,lightverb=tirer]	profit_____1	Default		%default
préséance	100	cfi	[pred="préséance_____1<Suj:cln|scompl|sinf|sn,Obl:sur-sn>",lightverb=avoir]	préséance_____1	Default		%default
racine	100	cfi	[pred="racine_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	racine_____1	Default		%default
rage	100	cfi	[pred="rage_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	rage_____1	Default		%default
raison	100	cfi	[pred="raison_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	raison_____1	Default		%default
raison	100	cfi	[pred="raison_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	raison_____2	Default		%default
rapport	100	cfi	[pred="rapport_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	rapport_____1	Default		%default
rapport	100	cfi	[pred="rapport_____2<Suj:cln|scompl|sinf|sn,Obl:avec-sn>",lightverb=avoir]	rapport_____2	Default		%default
recette	100	cfi	[pred="recette_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	recette_____1	Default		%default
recours	100	cfi	[pred="recours_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	recours_____1	Default		%default
route	100	cfi	[pred="route_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	route_____1	Default		%default
référence	100	cfi	[pred="référence_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|y>",lightverb=faire]	référence_____1	Default		%default
réponse	100	cfi	[pred="réponse_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	réponse_____1	Default		%default
satisfaction	100	cfi	[pred="satisfaction_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	satisfaction_____1	Default		%default
scandale	100	cfi	[pred="scandale_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	scandale_____1	Default		%default
semblant	100	cfi	[pred="semblant_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf|de-sn|en>",lightverb=faire]	semblant_____1	Default		%default
sensation	100	cfi	[pred="sensation_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	sensation_____1	Default		%default
serment	100	cfi	[pred="serment_____1<Suj:cln|scompl|sinf|sn,Objde:de-sinf|de-sn|en>",lightverb=faire]	serment_____1	Default		%default
signe	100	cfi	[pred="signe_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=donner]	signe_____1	Default		%default
signe	100	cfi	[pred="signe_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	signe_____2	Default		%default
silence	100	cfi	[pred="silence_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	silence_____1	Default		%default
soif	100	cfi	[pred="soif_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=avoir]	soif_____1	Default		%default
soin	100	cfi	[pred="soin_____1<Suj:cln|scompl|sinf|sn,Objde:de-scompl|de-sinf|de-sn|en|scompl>",lightverb=prendre]	soin_____1	Default		%default
soin	100	cfi	[pred="soin_____2<Suj:cln|scompl|sinf|sn,Objde:de-sinf>",lightverb=avoir]	soin_____2	Default		%default
suite	100	cfi	[pred="suite_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	suite_____1	Default		%default
suite	100	cfi	[pred="suite_____2<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	suite_____2	Default		%default
surface	100	cfi	[pred="surface_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	surface_____1	Default		%default
sécession	100	cfi	[pred="sécession_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	sécession_____1	Default		%default
table rase	100	cfi	[pred="table rase_____1<Suj:cln|scompl|sinf|sn,Objde:en|de-sn>",lightverb=faire]	table rase_____1	Default		%default
tendance	100	cfi	[pred="tendance_____1<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	tendance_____1	Default		%default
tort	100	cfi	[pred="tort_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=faire]	tort_____1	Default		%default
tort	100	cfi	[pred="tort_____2<Suj:cln|scompl|sinf|sn,Objde:(de-sinf)>",lightverb=avoir]	tort_____2	Default		%default
tort	100	cfi	[pred="tort_____3<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=donner]	tort_____3	Default		%default
trait	100	cfi	[pred="trait_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	trait_____1	Default		%default
valeur	100	cfi	[pred="valeur_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=avoir]	valeur_____1	Default		%default
vent	100	cfi	[pred="vent_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=avoir]	vent_____1	Default		%default
vie	100	cfi	[pred="vie_____1<Suj:cln|scompl|sinf|sn>",lightverb=prendre]	vie_____1	Default		%default
violence	100	cfi	[pred="violence_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld>",lightverb=faire]	violence_____1	Default		%default
vocation	100	cfi	[pred="vocation_____1<Suj:cln|scompl|sinf|sn,Objà:à-sinf|à-sn>",lightverb=avoir]	vocation_____1	Default		%default
voeu	100	cfi	[pred="voeu_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	voeu_____1	Default		%default
voile	100	cfi	[pred="voile_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	voile_____1	Default		%default
voix au chapitre	100	cfi	[pred="voix au chapitre_____1<Suj:cln|scompl|sinf|sn>",lightverb=avoir]	voix au chapitre_____1	Default		%default
volte-face	100	cfi	[pred="volte-face_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	volte-face_____1	Default		%default
échec	100	cfi	[pred="échec_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld|y>",lightverb=faire]	échec_____1	Default		%default
écho	100	cfi	[pred="écho_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn|cld|y>",lightverb=faire]	écho_____1	Default		%default
école	100	cfi	[pred="école_____1<Suj:cln|scompl|sinf|sn>",lightverb=faire]	école_____1	Default		%default
égard	100	cfi	[pred="égard_____1<Suj:cln|scompl|sinf|sn,Objà:à-sn>",lightverb=avoir]	égard_____1	Default		%default
étalage	100	cfi	[pred="étalage_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	étalage_____1	Default		%default
état	100	cfi	[pred="état_____1<Suj:cln|scompl|sinf|sn,Objde:de-sn|en>",lightverb=faire]	état_____1	Default		%default
