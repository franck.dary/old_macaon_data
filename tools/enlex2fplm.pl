#!/usr/bin/perl

my $dir= shift;

#  6965 ``
#  47696 ,
#   4766 :
#  38870 .
#   6784 ''
#   1343 (
#   1346 )
#   7298 $
#    142 #
#  22561 CC
#  37981 CD
#  84349 DT
#    853 EX
#      1 FW
#  11946 HYPH
#  98083 IN
#  51594 JJ
#   2142 JJR
#   1401 JJS
#     25 LS
#   9634 MD
#     12 NIL
# 144992 NN
#  89817 NNP
#   2084 NNPS
#  58470 NNS
#    293 PDT
#   8536 POS
#    316 PRF
#  17196 PRP
#   8228 PRP$
#  29098 RB
#   2180 RBR
#    452 RBS
#   2622 RP
#     54 SYM
#  21953 TO
#     97 UH
#  25816 VB
#  29662 VBD
#  15421 VBG
#  22132 VBN
#  12433 VBP
#  21373 VBZ
#   4555 WDT
#   2329 WP
#    159 WP$
#   2107 WRB
#
#       1.	CC	Coordinating conjunction
#	2.	CD	Cardinal number
#	3.	DT	Determiner
#	4.	EX	Existential there
#	5.	FW	Foreign word
#	6.	IN	Preposition or subordinating conjunction
#	7.	JJ	Adjective
#	8.	JJR	Adjective, comparative
#	9.	JJS	Adjective, superlative
#	10.	LS	List item marker
#	11.	MD	Modal
#	12.	NN	Noun, singular or mass
#	13.	NNS	Noun, plural
#	14.	NP	Proper noun, singular
#	15.	NPS	Proper noun, plural
#	16.	PDT	Predeterminer
#	17.	POS	Possessive ending
#	18.	PP	Personal pronoun
#	19.	PP$	Possessive pronoun
#	20.	RB	Adverb
#	21.	RBR	Adverb, comparative
#	22.	RBS	Adverb, superlative
#	23.	RP	Particle
#	24.	SYM	Symbol
#	25.	TO	to
#	26.	UH	Interjection
#	27.	VB	Verb, base form
#	28.	VBD	Verb, past tense
#	29.	VBG	Verb, gerund or present participle
#	30.	VBN	Verb, past participle
#	31.	VBP	Verb, non-3rd person singular present
#	32.	VBZ	Verb, 3rd person singular present
#	33.	WDT	Wh-determiner
#	34.	WP	Wh-pronoun
#	35.	WP$	Possessive wh-pronoun
#	36.	WRB	Wh-adverb



#################################################
#################################################

#valeurs du trait cas
#i objet indirect
#o objet direct
#u sujet

#valeurs du trait tps
#P 	indicatif 	présent
#F 	indicatif 	futur
#I 	indicatif 	imparfait
#J 	indicatif 	passé-simple
#C 	conditionnel 	présent
#Y 	impératif 	présent
#S 	subjonctif 	présent
#T 	subjonctif 	imparfait
#K 	participe 	passé
#G 	participe 	présent
#W 	infinitif 	présent

#valeurs du trait type_det
#d defini
#n indefini
#e demonstratif
#p possessif

#la flexion est la concatenation des traits
#tps pers gnr nb cas type_det


#alarmante	100	adj	[pred="alarmant_____1<Suj:(cln|sn)>",@pers,cat=adj,@fs]	alarmant_____1	Default	fs	%adj_personnel


$h_pos{"a"} = "";
$h_pos{"A"} = "JJ";
$h_pos{"adjPref"} = "JJ";
$h_pos{"adjSuff"} = "JJ";
$h_pos{"adv"} = "";
$h_pos{"advPref"} = "";
$h_pos{"C"} = "CC";
$h_pos{"D"} = "DT";
$h_pos{"det"} = "";
$h_pos{"epsilon"} = "";
$h_pos{"etr"} = "";
$h_pos{"I"} = "UH";
$h_pos{"meta"} = "";
$h_pos{"n"} = "";
$h_pos{"N"} = "NN";
$h_pos{"nc"} = "";
$h_pos{"np"} = "";
$h_pos{"P"} = "PRP";
$h_pos{"parentf"} = "";
$h_pos{"parento"} = "";
$h_pos{"poncts"} = "";
$h_pos{"ponctw"} = "";
$h_pos{"pro"} = "";
$h_pos{"R"} = "RB";
$h_pos{"S"} = "IN";
$h_pos{"sbound"} = "";
$h_pos{"V"} = "";




my @file_list = `ls $dir/*.lex`;
foreach $file (@file_list){
    chomp($file);
    print STDERR "processing file : $file\n";
    open(FILE,$file);
    while(<FILE>){
	/([^\t]*)\t([^\t]*)\t([^\t]*)\t\[([^\]]*)\]\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/;
	my $forme = $1;
	my $cat = $3;
	my $cat_ptb = $h_pos{$cat};
	my $morpho = $4;

	my @tab_morpho = split /,/ , $morpho;
	$flexion = $tab_morpho[$#tab_morpho];
	if(!($flexion =~ /^@/)){
	    $flexion = "-";
	}
	else{
	    $flexion = substr $flexion, 1;
	}
	
	# proper nouns
	if($cat eq "N"){
	    if($forme =~ /^[A-Z]/){
		$cat_ptb = "NNP";
	    }
	}	
	elsif($cat eq "S"){
	    # the preposition "to" has its own tag
	    if ($forme eq "to"){
		$cat_ptb = "TO";
	    }
	}
	elsif($cat eq "P"){
	    # wh- pronouns
	    if (($forme eq "who") ||
		($forme eq "what") ||
		($forme eq "whoever") ||
		($forme eq "whom")){
		$cat_ptb = "WP";
	    }
	}
	elsif($cat eq "R"){
	    # wh- adverbs
	    if (($forme eq "how") ||
		($forme eq "when") ||
		($forme eq "whenever") ||
		($forme eq "where") ||
		($forme eq "whereby") ||
		($forme eq "wherever") ||
		($forme eq "why")){
		$cat_ptb = "WRB";
	    }	
	}
	elsif($cat eq "D"){
	    # possessive
	    if(($forme eq "its")
	       || ($forme eq "my")
	       || ($forme eq "his")
	       || ($forme eq "her")
	       || ($forme eq "our")
	       || ($forme eq "your")
	       || ($forme eq "their")
		){
		$cat_ptb = "PRP\$";
	    }
	    elsif($forme eq "whose"){
		$cat_ptb = "WP\$";
	    }
	    elsif(($forme eq "what") ||
		  ($forme eq "which") ||
		  ($forme eq "whatever") ||
		  ($forme eq "whichever")){
		$cat_ptb = "WDT";
	    }
	}
	
	my $lemme = "-";
	foreach $trait(@tab_morpho){
	    if($trait =~ /pred=\"([^_]*)/){
		$lemme = $1;
	    }
	    #adjectifs
	    elsif($cat_ptb eq "JJ"){
		if($trait =~ /\@sup/){
		    $cat_ptb = "JJS";
		}
		elsif($trait =~ /\@comp/){
		    $cat_ptb = "JJR";
		}
	    }
	    #adverbes
	    elsif($cat_ptb eq "RB"){
		if($trait =~ /\@sup/){
		    $cat_ptb = "RBS";
		}
		elsif($trait =~ /\@comp/){
		    $cat_ptb = "RBR";
		}
	    }
	    #noms
	    elsif(($cat_ptb eq "NN") || ($cat_ptb eq "NNP")){
		if($trait =~ /\@p/){
		    $cat_ptb = $cat_ptb . "S";
		}
	    }
	    #verbes
	    elsif($cat eq "V"){
		if($trait =~ /\@inf/){
		    $cat_ptb = "VB";
		}
		elsif($trait =~ /\@past/){
		    $cat_ptb = "VBD";
		}
		elsif($trait =~ /\@ppart/){
		    $cat_ptb = "VBN";
		}		
		elsif($trait =~ /\@pres/){
		    if($trait =~ /\@pres3s/){
			$cat_ptb = "VBZ";
		    }
		    elsif($trait =~ /\@prespart/){
			$cat_ptb = "VBG";
		    }
		    else {
			$cat_ptb = "VBP";
		    }
		}
	    }
	}
	    
	$flexion = "#####";
	$lemme =~ s/://;
	
	if($cat_ptb){
	    print "$forme\t$cat_ptb\t$lemme\t$flexion\n";

	    if($cat_ptb eq "VB"){
		# add entries for present forms
		print "$forme\tVBP\t$lemme\t$flexion\n";
		# add entries for modals
		if(($forme eq "will") ||
		   ($forme eq "can") ||
		   ($forme eq "may") ||
		   ($forme eq "must") ||
		   ($forme eq "shall") ||
		   ($forme eq "dare") ||
		   ($forme eq "need") ||
		   ($forme eq "ought")){
		    print "$forme\tMD\t$lemme\t$flexion\n";
		}
	    }
	    elsif($cat_ptb eq "VBD"){
		# modals
		if(($forme eq "would") ||
		   ($forme eq "could") ||
		   ($forme eq "should") ||
		   ($forme eq "might")){
		    print "$forme\tMD\t$lemme\t$flexion\n";
		}
	    }
	    elsif($cat_ptb eq "IN"){
		# particles from prepositions
#		if(($forme eq "for") ||
#		   ($forme eq "on") ||
#		   ($forme eq "up") ||
#		   ($forme eq "about") ||
#		   ($forme eq "across") ||
#		   ($forme eq "along") ||
#		   ($forme eq "around") ||
#		   ($forme eq "before") ||
#		   ($forme eq "behind") ||
#		   ($forme eq "by") ||
#		   ($forme eq "down") ||
#		   ($forme eq "in") ||
#		   ($forme eq "of") ||
#		   ($forme eq "off") ||
#		   ($forme eq "out") ||
#		   ($forme eq "over") ||
#		   ($forme eq "through") ||
#		   ($forme eq "upon") ||
#		   ($forme eq "with")){
		print "$forme\tRP\t$lemme\t$flexion\n";
#		}
	    }
	    elsif($cat_ptb eq "RB"){
		# "however" can be a WRB
		if($forme eq "however"){
		    print "$forme\tWRB\t$lemme\t$flexion\n";		
		}
		# some adverbs can be predeterminers
		elsif(($forme eq "quite") || 
		      ($forme eq "nary")){
		    print "$forme\tPDT\t$lemme\t$flexion\n";
		}
		else {
		    # particles from adverbs
#		if(($forme eq "back") ||
#		   ($forme eq "ahead") ||
#		   ($forme eq "apart") ||
#		   ($forme eq "aside") ||
#		   ($forme eq "away") ||
#		   ($forme eq "even") ||
#		   ($forme eq "forward") ||
#		   ($forme eq "open") ||
#		   ($forme eq "together") ||
#		   ($forme eq "whole") ||
#		   ($forme eq "yet")){
		    print "$forme\tRP\t$lemme\t$flexion\n";
#		}
		}
	    }
	    elsif($cat_ptb eq "DT"){
		# "that" as WDT
		if($forme eq "that"){
		    print "$forme\tWDT\t$lemme\t$flexion\n";		
		}
		# some determiners can be predeterminers
		elsif(($forme eq "all") || 
		      ($forme eq "half") ||
		      ($forme eq "many") ||
		      ($forme eq "such")){
		    print "$forme\tPDT\t$lemme\t$flexion\n";		    
		}
	    }	       
	    # add all CC as IN (subordinating conjunctions are IN)
	    elsif($cat_ptb eq "CC"){
		print "$forme\tIN\t$lemme\t$flexion\n";
	    }
	    # add an entry for "or" as a conjunction
	    elsif($cat_ptb eq "NN"){
		if($forme eq "or"){
		    print "$forme\tCC\t$lemme\t$flexion\n";		    
		}
	    }
	}

    }
    close(FILE);
}

