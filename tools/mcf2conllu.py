#! /usr/bin/python3

import sys

def printUsageAndExit() :
  print("Usage : %s file.mcf mcf.mcd conllu.mcd"%sys.argv[0])
  exit(1)

def readMCD(mcdFilename) :
  mcd = {}
  for line in open(mcdFilename, "r", encoding="utf8") :
    clean = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = line.split(' ')
    if len(splited) != 2 :
      print("ERROR : invalid mcd line \'%s\'. Aborting"%line, file=sys.stderr)
      exit(1)
    mcd[splited[0].strip()] = splited[1].strip()

  return mcd

def main() :
  if len(sys.argv) != 4 :
    printUsageAndExit()

  conllMCD = readMCD(sys.argv[3])
  mcfMCD = readMCD(sys.argv[2])
  conllMCDr = {v: k for k, v in conllMCD.items()} 
  mcfMCDr = {v: k for k, v in mcfMCD.items()} 

  curID = 1

  for line in open(sys.argv[1], encoding="utf8") :
    striped = line.strip()
    if len(line) < 2 or line[0] == '#' :
      continue
    splited = striped.split('\t')

    toPrint = ""

    for ind in range(0, len(conllMCD)) :
      col = conllMCD[str(ind)]
      if col == "EMPTY" :
        toPrint += "_\t"
      elif col == "ID" and "ID" not in mcfMCDr :
        toPrint += str(curID) + "\t"
      elif col == "GOV" :
        if int(mcfMCDr["GOV"]) >= len(splited) :
          if "ID" in mcfMCDr :
            curID = int(splited[int(mcfMCDr["ID"])].split('-')[0])
          if (curID == 1) and not ("EOS" in mcfMCDr and int(mcfMCDr["EOS"]) < len(splited)) :
            toPrint += str(0)+'\t'
          else :
            toPrint += str(1)+'\t'
        else :
          relInd = int(splited[int(mcfMCDr["GOV"])])
          gov = 0
          if relInd != 0 :
            gov = relInd + curID
          toPrint += str(gov) + '\t'
      else :
        if col not in mcfMCDr :
          print("ERROR : %s not in mcf.mcd."%col)
          exit(1)
        if int(mcfMCDr[col]) >= len(splited) :
          toPrint += '_\t'
        else :
          toPrint += splited[int(mcfMCDr[col])] + '\t'

    print(toPrint[:-1])
    
    if "ID" not in mcfMCDr :
      curID += 1

    if "EOS" in mcfMCDr and int(mcfMCDr["EOS"]) < len(splited) :
      if splited[int(mcfMCDr["EOS"])] == "1" :
        print("")
        curID = 1

if __name__ == "__main__" :
  main()
  print("")

