#!/usr/bin/python
# -*- coding: utf-8 -*-

#./extractFeatWals.py fichiers_config/featWALS ../WALS/language.csv > WALSFeat.csv

from __future__ import print_function          
import fileinput
import sys
import re
import os
import csv
import pandas

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def extractFeat(filepathFeat):
    featFile = open(filepathFeat, "r")
    features = ['wals_code'] # we always want to keep the id of the language
    curFeat = featFile.readline().rstrip()
    while(not fileCompletelyRead(curFeat)):
        if(lineIsAComment(curFeat) or not isInASequence(curFeat)):
            curFeat = featFile.readline().rstrip()
            continue
        features.append(curFeat)
        curFeat = featFile.readline().rstrip()    
    
    return features
    
def fct_indexColumnsToKeep(idColumns, filepathFeat):
    features = extractFeat(filepathFeat)
    columnsToKeep = []
    newFirstLine = ""
    for i in range(len(idColumns)):
        curID = idColumns[i].split(' ')[0]
        if(curID in features):
            columnsToKeep.append(idColumns[i])
        
    return columnsToKeep
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 3):
        print("Error, give a file with the code of features you want to keep, and the file containing the WALS")
        exit(1)
        
    df = pandas.read_csv(sys.argv[2])
    idColumns = df.columns.values
    
    columnsToKeep = fct_indexColumnsToKeep(idColumns, sys.argv[1])
    new_df = df[columnsToKeep]
    
    new_df.to_csv(sys.stdout, index=False)
        
