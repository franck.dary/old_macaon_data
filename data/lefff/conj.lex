afin qu'	100	csu	[wh=+,@e]	afin que_____1	Default	e	%default
afin que	100	csu	[wh=+]	afin que_____1	Default		%default
ainsi	100	csu	[]	ainsi_____1	Default		%default
ainsi qu'	100	csu	[wh=+,@e]	ainsi que_____1	Default	e	%default
ainsi que	100	csu	[wh=+]	ainsi que_____1	Default		%default
alors même qu'	100	csu	[wh=+,@e]	alors même que_____1	Default	e	%default
alors même que	100	csu	[wh=+]	alors même que_____1	Default		%default
alors qu'	100	csu	[wh=+,@e]	alors que_____1	Default	e	%default
alors que	100	csu	[wh=+]	alors que_____1	Default		%default
après qu'	100	csu	[wh=+,@e]	après que_____1	Default	e	%default
après que	100	csu	[wh=+]	après que_____1	Default		%default
après quoi	100	csu	[]	après quoi_____1	Default		%default
attendu qu'	100	csu	[wh=+,@e]	attendu que_____1	Default	e	%default
attendu que	100	csu	[wh=+]	attendu que_____1	Default		%default
au cas où	100	csu	[]	au cas où_____1	Default		%default
au fur et à mesure qu'	100	csu	[wh=+,@e]	au fur et à mesure que_____1	Default	e	%default
au fur et à mesure que	100	csu	[wh=+]	au fur et à mesure que_____1	Default		%default
au moment où	100	csu	[]	au moment où_____1	Default		%default
au même titre qu'	100	csu	[wh=+,@e]	au même titre que_____1	Default	e	%default
au même titre que	100	csu	[wh=+]	au même titre que_____1	Default		%default
au point qu'	100	csu	[wh=+,@e]	au point que_____1	Default	e	%default
au point que	100	csu	[wh=+]	au point que_____1	Default		%default
aussi	100	csu	[]	aussi_____1	Default		%default
aussi bien qu'	100	csu	[wh=+,@e]	aussi bien que_____1	Default	e	%default
aussi bien que	100	csu	[wh=+]	aussi bien que_____1	Default		%default
aussi longtemps qu'	100	csu	[wh=+,@e]	aussi longtemps que_____1	Default	e	%default
aussi longtemps que	100	csu	[wh=+]	aussi longtemps que_____1	Default		%default
aussitôt qu'	100	csu	[wh=+,@e]	aussitôt que_____1	Default	e	%default
aussitôt que	100	csu	[wh=+]	aussitôt que_____1	Default		%default
autant dire qu'	100	csu	[wh=+,@e]	autant dire que_____1	Default	e	%default
autant dire que	100	csu	[wh=+]	autant dire que_____1	Default		%default
autant qu'	100	csu	[wh=+,@e]	autant que_____1	Default	e	%default
autant que	100	csu	[wh=+]	autant que_____1	Default		%default
avant même qu'	100	csu	[wh=+,@e]	avant même que_____1	Default	e	%default
avant même que	100	csu	[wh=+]	avant même que_____1	Default		%default
avant qu'	100	csu	[wh=+,@e]	avant que_____1	Default	e	%default
avant que	100	csu	[wh=+]	avant que_____1	Default		%default
because	100	csu	[]	because_____1	Default		%default
bicause	100	csu	[mod_kind = causal,wh=+]	parce que_____1	Default		%default
bien qu'	100	csu	[wh=+,@e]	bien que_____1	Default	e	%default
bien que	100	csu	[wh=+]	bien que_____1	Default		%default
c'est pourquoi	100	csu	[wh=+]	c'est pourquoi_____1	Default		%default
c'est à dire qu'	100	csu	[wh=+,@e]	c'est à dire que_____1	Default	e	%default
c'est à dire qu'	100	csu	[wh=+,@e]	c'est-à-dire que_____1	Default	e	%default
c'est à dire que	100	csu	[wh=+]	c'est à dire que_____1	Default		%default
c'est à dire que	100	csu	[wh=+]	c'est-à-dire que_____1	Default		%default
c'est-à-dire qu'	100	csu	[wh=+,@e]	c'est-à-dire que_____1	Default	e	%default
c'est-à-dire que	100	csu	[wh=+]	c'est-à-dire que_____1	Default		%default
ce qu'	100	csu	[wh=+,@e]	ce que_____1	Default	e	%default
ce que	100	csu	[wh=+]	ce que_____1	Default		%default
cependant qu'	100	csu	[wh=+,@e]	cependant que_____1	Default	e	%default
cependant que	100	csu	[wh=+]	cependant que_____1	Default		%default
cette fois qu'	100	csu	[wh=+,@e]	cette fois que_____1	Default	e	%default
cette fois que	100	csu	[wh=+]	cette fois que_____1	Default		%default
chaqu'fois qu'	100	csu	[wh=+,@e]	chaque fois que_____1	Default	e	%default
chaqu'fois que	100	csu	[wh=+]	chaque fois que_____1	Default		%default
chaque fois qu'	100	csu	[wh=+,@e]	chaque fois que_____1	Default	e	%default
chaque fois que	100	csu	[wh=+]	chaque fois que_____1	Default		%default
comme	100	csu	[]	comme_____1	Default		%default
comme quoi	100	csu	[]	comme quoi_____1	Default		%default
comme si	100	csu	[]	comme si_____1	Default		%default
comme ça	100	csu	[]	comme ça_____1	Default		%default
considérant qu'	100	csu	[wh=+,@e]	considérant que_____1	Default	e	%default
considérant que	100	csu	[wh=+]	considérant que_____1	Default		%default
considéré qu'	100	csu	[wh=+,@e]	considéré que_____1	Default	e	%default
considéré que	100	csu	[wh=+]	considéré que_____1	Default		%default
d'autant plus qu'	100	csu	[wh=+,@e]	d'autant plus que_____1	Default	e	%default
d'autant plus que	100	csu	[wh=+]	d'autant plus que_____1	Default		%default
d'autant qu'	100	csu	[wh=+,@e]	d'autant que_____1	Default	e	%default
d'autant que	100	csu	[wh=+]	d'autant que_____1	Default		%default
d'ici qu'	100	csu	[wh=+,@e]	d'ici que_____1	Default	e	%default
d'ici que	100	csu	[wh=+]	d'ici que_____1	Default		%default
d'où	100	csu	[]	d'où_____1	Default		%default
dans l' espoir qu'	100	csu	[wh=+,@e]	dans l' espoir que_____1	Default	e	%default
dans l' espoir que	100	csu	[wh=+]	dans l' espoir que_____1	Default		%default
dans l' idée qu'	100	csu	[wh=+,@e]	dans l' idée que_____1	Default	e	%default
dans l' idée que	100	csu	[wh=+]	dans l' idée que_____1	Default		%default
dans l'espoir qu'	100	csu	[wh=+,@e]	dans l'espoir que_____1	Default	e	%default
dans l'espoir que	100	csu	[wh=+]	dans l'espoir que_____1	Default		%default
dans l'hypothèse où	100	csu	[]	dans l'hypothèse où_____1	Default		%default
dans l'intention qu'	100	csu	[wh=+,@e]	dans l'intention que_____1	Default	e	%default
dans l'intention que	100	csu	[wh=+]	dans l'intention que_____1	Default		%default
dans la crainte qu'	100	csu	[wh=+,@e]	dans la crainte que_____1	Default	e	%default
dans la crainte que	100	csu	[wh=+]	dans la crainte que_____1	Default		%default
dans la mesure où	100	csu	[]	dans la mesure où_____1	Default		%default
dans le but qu'	100	csu	[wh=+,@e]	dans le but que_____1	Default	e	%default
dans le but que	100	csu	[wh=+]	dans le but que_____1	Default		%default
dans le cas où	100	csu	[]	dans le cas où_____1	Default		%default
dans le sens où	100	csu	[]	dans le sens où_____1	Default		%default
dans le sens qu'	100	csu	[wh=+,@e]	dans le sens que_____1	Default	e	%default
dans le sens que	100	csu	[wh=+]	dans le sens que_____1	Default		%default
de ce qu'	100	csu	[wh=+,@e]	de ce que_____1	Default	e	%default
de ce que	100	csu	[wh=+]	de ce que_____1	Default		%default
de crainte qu'	100	csu	[wh=+,@e]	de crainte que_____1	Default	e	%default
de crainte que	100	csu	[wh=+]	de crainte que_____1	Default		%default
de façon qu'	100	csu	[wh=+,@e]	de façon que_____1	Default	e	%default
de façon que	100	csu	[wh=+]	de façon que_____1	Default		%default
de façon à ce qu'	100	csu	[wh=+,@e]	de façon à ce que_____1	Default	e	%default
de façon à ce que	100	csu	[wh=+]	de façon à ce que_____1	Default		%default
de la même façon qu'	100	csu	[wh=+,@e]	de la même façon que_____1	Default	e	%default
de la même façon que	100	csu	[wh=+]	de la même façon que_____1	Default		%default
de la même manière qu'	100	csu	[wh=+,@e]	de la même manière que_____1	Default	e	%default
de la même manière que	100	csu	[wh=+]	de la même manière que_____1	Default		%default
de manière qu'	100	csu	[wh=+,@e]	de manière que_____1	Default	e	%default
de manière que	100	csu	[wh=+]	de manière que_____1	Default		%default
de manière à ce qu'	100	csu	[wh=+,@e]	de manière à ce que_____1	Default	e	%default
de manière à ce que	100	csu	[wh=+]	de manière à ce que_____1	Default		%default
de même qu'	100	csu	[wh=+,@e]	de même que_____1	Default	e	%default
de même que	100	csu	[wh=+]	de même que_____1	Default		%default
de parce qu'	100	csu	[wh=+,@e]	de parce que_____1	Default	e	%default
de parce que	100	csu	[wh=+]	de parce que_____1	Default		%default
de peur qu'	100	csu	[wh=+,@e]	de peur que_____1	Default	e	%default
de peur que	100	csu	[wh=+]	de peur que_____1	Default		%default
de sorte qu'	100	csu	[wh=+,@e]	de sorte que_____1	Default	e	%default
de sorte que	100	csu	[wh=+]	de sorte que_____1	Default		%default
de telle façon qu'	100	csu	[wh=+,@e]	de telle façon que_____1	Default	e	%default
de telle façon que	100	csu	[wh=+]	de telle façon que_____1	Default		%default
de telle manière qu'	100	csu	[wh=+,@e]	de telle manière que_____1	Default	e	%default
de telle manière que	100	csu	[wh=+]	de telle manière que_____1	Default		%default
de telle sorte qu'	100	csu	[wh=+,@e]	de telle sorte que_____1	Default	e	%default
de telle sorte que	100	csu	[wh=+]	de telle sorte que_____1	Default		%default
depuis qu'	100	csu	[wh=+,@e]	depuis que_____1	Default	e	%default
depuis que	100	csu	[wh=+]	depuis que_____1	Default		%default
des fois qu'	100	csu	[wh=+,@e]	des fois que_____1	Default	e	%default
des fois que	100	csu	[wh=+]	des fois que_____1	Default		%default
du fait qu'	100	csu	[wh=+,@e]	du fait que_____1	Default	e	%default
du fait que	100	csu	[wh=+]	du fait que_____1	Default		%default
du moment qu'	100	csu	[wh=+,@e]	du moment que_____1	Default	e	%default
du moment que	100	csu	[wh=+]	du moment que_____1	Default		%default
du temps où	100	csu	[]	du temps où_____1	Default		%default
dès lors qu'	100	csu	[wh=+,@e]	dès lors que_____1	Default	e	%default
dès lors que	100	csu	[wh=+]	dès lors que_____1	Default		%default
dès qu'	100	csu	[wh=+,@e]	dès que_____1	Default	e	%default
dès que	100	csu	[wh=+]	dès que_____1	Default		%default
déjà qu'	100	csu	[wh=+,@e]	déjà que_____1	Default	e	%default
déjà que	100	csu	[wh=+]	déjà que_____1	Default		%default
en admettant qu'	100	csu	[wh=+,@e]	en admettant que_____1	Default	e	%default
en admettant que	100	csu	[wh=+]	en admettant que_____1	Default		%default
en ce qu'	100	csu	[wh=+,@e]	en ce que_____1	Default	e	%default
en ce que	100	csu	[wh=+]	en ce que_____1	Default		%default
en ce sens qu'	100	csu	[wh=+,@e]	en ce sens que_____1	Default	e	%default
en ce sens que	100	csu	[wh=+]	en ce sens que_____1	Default		%default
en dépit du fait qu'	100	csu	[wh=+,@e]	en dépit du fait que_____1	Default	e	%default
en dépit du fait que	100	csu	[wh=+]	en dépit du fait que_____1	Default		%default
en dépit qu'	100	csu	[wh=+,@e]	en dépit que_____1	Default	e	%default
en dépit que	100	csu	[wh=+]	en dépit que_____1	Default		%default
en même temps qu'	100	csu	[wh=+,@e]	en même temps que_____1	Default	e	%default
en même temps que	100	csu	[wh=+]	en même temps que_____1	Default		%default
en sorte qu'	100	csu	[wh=+,@e]	en sorte que_____1	Default	e	%default
en sorte que	100	csu	[wh=+]	en sorte que_____1	Default		%default
en supposant qu'	100	csu	[wh=+,@e]	en supposant que_____1	Default	e	%default
en supposant que	100	csu	[wh=+]	en supposant que_____1	Default		%default
en tant qu'	100	csu	[wh=+,@e]	en tant que_____1	Default	e	%default
en tant que	100	csu	[wh=+]	en tant que_____1	Default		%default
encore qu'	100	csu	[wh=+,@e]	encore que_____1	Default	e	%default
encore que	100	csu	[wh=+]	encore que_____1	Default		%default
et même	100	csu	[]	et même_____1	Default		%default
excepté qu'	100	csu	[wh=+,@e]	excepté que_____1	Default	e	%default
excepté que	100	csu	[wh=+]	excepté que_____1	Default		%default
hormis le fait qu'	100	csu	[wh=+,@e]	hormis le fait que_____1	Default	e	%default
hormis le fait que	100	csu	[wh=+]	hormis le fait que_____1	Default		%default
hormis qu'	100	csu	[wh=+,@e]	hormis que_____1	Default	e	%default
hormis que	100	csu	[wh=+]	hormis que_____1	Default		%default
jusqu'à ce qu'	100	csu	[wh=+,@e]	jusqu'à ce que_____1	Default	e	%default
jusqu'à ce que	100	csu	[wh=+]	jusqu'à ce que_____1	Default		%default
le jour où	100	csu	[]	le jour où_____1	Default		%default
le moins qu'	100	csu	[wh=+,@e]	le moins que_____1	Default	e	%default
le moins que	100	csu	[wh=+]	le moins que_____1	Default		%default
le temps qu'	100	csu	[wh=+,@e]	le temps que_____1	Default	e	%default
le temps que	100	csu	[wh=+]	le temps que_____1	Default		%default
lorsqu'	100	csu	[wh=+,@e]	lorsque_____1	Default	e	%default
lorsque	100	csu	[wh=+]	lorsque_____1	Default		%default
maintenant qu'	100	csu	[wh=+,@e]	maintenant que_____1	Default	e	%default
maintenant que	100	csu	[wh=+]	maintenant que_____1	Default		%default
malgré le fait qu'	100	csu	[wh=+,@e]	malgré le fait que_____1	Default	e	%default
malgré le fait que	100	csu	[wh=+]	malgré le fait que_____1	Default		%default
malgré qu'	100	csu	[wh=+,@e]	malgré que_____1	Default	e	%default
malgré que	100	csu	[wh=+]	malgré que_____1	Default		%default
mis à part le fait qu'	100	csu	[wh=+,@e]	mis à part le fait que_____1	Default	e	%default
mis à part le fait que	100	csu	[wh=+]	mis à part le fait que_____1	Default		%default
mis à part qu'	100	csu	[wh=+,@e]	mis à part que_____1	Default	e	%default
mis à part que	100	csu	[wh=+]	mis à part que_____1	Default		%default
même si	100	csu	[]	même si_____1	Default		%default
même si__csu	100	csu	[]	même si_____1	Default		%default
non plus qu'	100	csu	[wh=+,@e]	non plus que_____1	Default	e	%default
non plus que	100	csu	[wh=+]	non plus que_____1	Default		%default
non qu'	100	csu	[wh=+,@e]	non que_____1	Default	e	%default
non que	100	csu	[wh=+]	non que_____1	Default		%default
nonobstant qu'	100	csu	[wh=+,@e]	nonobstant que_____1	Default	e	%default
nonobstant que	100	csu	[wh=+]	nonobstant que_____1	Default		%default
outre qu'	100	csu	[wh=+,@e]	outre que_____1	Default	e	%default
outre que	100	csu	[wh=+]	outre que_____1	Default		%default
pac'qu'	100	csu	[mod_kind = causal,wh=+,@e]	parce que_____1	Default	e	%default
pac'que	100	csu	[mod_kind = causal,wh=+]	parce que_____1	Default		%default
par le fait qu'	100	csu	[wh=+,@e]	par le fait que_____1	Default	e	%default
par le fait que	100	csu	[wh=+]	par le fait que_____1	Default		%default
parc'qu'	100	csu	[mod_kind = causal,wh=+,@e]	parce que_____1	Default	e	%default
parc'que	100	csu	[mod_kind = causal,wh=+]	parce que_____1	Default		%default
parce qu'	100	csu	[mod_kind = causal,wh=+,@e]	parce que_____1	Default	e	%default
parce que	100	csu	[mod_kind = causal,wh=+]	parce que_____1	Default		%default
partant	100	csu	[]	partant_____1	Default		%default
pask'	100	csu	[mod_kind = causal,wh=+,@e]	parce que_____1	Default	e	%default
paske	100	csu	[mod_kind = causal,wh=+]	parce que_____1	Default		%default
pcq	100	csu	[mod_kind = causal,wh=+]	parce que_____1	Default		%default
pdt qu'	100	csu	[wh=+,@e]	pendant que_____1	Default	e	%default
pdt que	100	csu	[wh=+]	pendant que_____1	Default		%default
pendant qu'	100	csu	[wh=+,@e]	pendant que_____1	Default	e	%default
pendant que	100	csu	[wh=+]	pendant que_____1	Default		%default
plus qu'	100	csu	[wh=+,@e]	plus que_____1	Default	e	%default
plus que	100	csu	[wh=+]	plus que_____1	Default		%default
plutôt qu'	100	csu	[wh=+,@e]	plutôt que_____1	Default	e	%default
plutôt que	100	csu	[wh=+]	plutôt que_____1	Default		%default
pour autant qu'	100	csu	[wh=+,@e]	pour autant que_____1	Default	e	%default
pour autant que	100	csu	[wh=+]	pour autant que_____1	Default		%default
pour cette raison qu'	100	csu	[wh=+,@e]	pour cette raison que_____1	Default	e	%default
pour cette raison que	100	csu	[wh=+]	pour cette raison que_____1	Default		%default
pour peu qu'	100	csu	[wh=+,@e]	pour peu que_____1	Default	e	%default
pour peu que	100	csu	[wh=+]	pour peu que_____1	Default		%default
pour qu'	100	csu	[wh=+,@e]	pour que_____1	Default	e	%default
pour que	100	csu	[wh=+]	pour que_____1	Default		%default
pour une fois qu'	100	csu	[wh=+,@e]	pour une fois que_____1	Default	e	%default
pour une fois que	100	csu	[wh=+]	pour une fois que_____1	Default		%default
pourvu qu'	100	csu	[wh=+,@e]	pourvu que_____1	Default	e	%default
pourvu que	100	csu	[wh=+]	pourvu que_____1	Default		%default
preuve qu'	100	csu	[wh=+,@e]	preuve que_____1	Default	e	%default
preuve que	100	csu	[wh=+]	preuve que_____1	Default		%default
puis	100	csu	[]	puis_____1	Default		%default
puisqu'	100	csu	[wh=+,@e]	puisque_____1	Default	e	%default
puisque	100	csu	[wh=+]	puisque_____1	Default		%default
qd	100	csu	[wh=+]	quand_____1	Default		%default
qu'	100	que	[wh=+,@e]	que_____1	Default	e	%default
quand	100	csu	[wh=+]	quand_____1	Default		%default
quand bien même	100	csu	[wh=+]	quand bien même_____1	Default		%default
quand bien même qu'	100	csu	[wh=+,@e]	quand bien même que_____1	Default	e	%default
quand bien même que	100	csu	[wh=+]	quand bien même que_____1	Default		%default
que	100	que	[wh=+]	que_____1	Default		%default
quitte à ce qu'	100	csu	[wh=+,@e]	quitte à ce que_____1	Default	e	%default
quitte à ce que	100	csu	[wh=+]	quitte à ce que_____1	Default		%default
quoi qu'	100	csu	[wh=+,@e]	quoi que_____1	Default	e	%default
quoi que	100	csu	[wh=+]	quoi que_____1	Default		%default
quoiqu'	100	csu	[wh=+,@e]	quoique_____1	Default	e	%default
quoique	100	csu	[wh=+]	quoique_____1	Default		%default
reste à savoir si	100	csu	[]	reste à savoir si_____1	Default		%default
sachant qu'	100	csu	[wh=+,@e]	sachant que_____1	Default	e	%default
sachant que	100	csu	[wh=+]	sachant que_____1	Default		%default
sans compter qu'	100	csu	[wh=+,@e]	sans compter que_____1	Default	e	%default
sans compter que	100	csu	[wh=+]	sans compter que_____1	Default		%default
sans oublier qu'	100	csu	[wh=+,@e]	sans oublier que_____1	Default	e	%default
sans oublier que	100	csu	[wh=+]	sans oublier que_____1	Default		%default
sans qu'	100	csu	[wh=+,@e]	sans que_____1	Default	e	%default
sans que	100	csu	[wh=+]	sans que_____1	Default		%default
sauf qu'	100	csu	[wh=+,@e]	sauf que_____1	Default	e	%default
sauf que	100	csu	[wh=+]	sauf que_____1	Default		%default
selon qu'	100	csu	[wh=+,@e]	selon que_____1	Default	e	%default
selon que	100	csu	[wh=+]	selon que_____1	Default		%default
si	100	csu	[]	si_____1	Default		%default
si bien qu'	100	csu	[wh=+,@e]	si bien que_____1	Default	e	%default
si bien que	100	csu	[wh=+]	si bien que_____1	Default		%default
si ce n' est qu'	100	csu	[wh=+,@e]	si ce n' est que_____1	Default	e	%default
si ce n' est que	100	csu	[wh=+]	si ce n' est que_____1	Default		%default
si ce n'est qu'	100	csu	[wh=+,@e]	si ce n'est que_____1	Default	e	%default
si ce n'est que	100	csu	[wh=+]	si ce n'est que_____1	Default		%default
si jamais	100	csu	[]	si_____1	Default		%default
si peu qu'	100	csu	[wh=+,@e]	si peu que_____1	Default	e	%default
si peu que	100	csu	[wh=+]	si peu que_____1	Default		%default
si tant est qu'	100	csu	[wh=+,@e]	si tant est que_____1	Default	e	%default
si tant est que	100	csu	[wh=+]	si tant est que_____1	Default		%default
si__csu	100	csu	[]	si_____1	Default		%default
sinon	100	csu	[]	sinon_____1	Default		%default
sinon qu'	100	csu	[wh=+,@e]	sinon que_____1	Default	e	%default
sinon que	100	csu	[wh=+]	sinon que_____1	Default		%default
sitôt qu'	100	csu	[wh=+,@e]	sitôt que_____1	Default	e	%default
sitôt que	100	csu	[wh=+]	sitôt que_____1	Default		%default
soit	100	csu	[]	soit_____1	Default		%default
sous prétexte qu'	100	csu	[wh=+,@e]	sous prétexte que_____1	Default	e	%default
sous prétexte que	100	csu	[wh=+]	sous prétexte que_____1	Default		%default
sous réserve qu'	100	csu	[wh=+,@e]	sous réserve que_____1	Default	e	%default
sous réserve que	100	csu	[wh=+]	sous réserve que_____1	Default		%default
suivant qu'	100	csu	[wh=+,@e]	suivant que_____1	Default	e	%default
suivant que	100	csu	[wh=+]	suivant que_____1	Default		%default
surtout qu'	100	csu	[wh=+,@e]	surtout que_____1	Default	e	%default
surtout que	100	csu	[wh=+]	surtout que_____1	Default		%default
tandis qu'	100	csu	[wh=+,@e]	tandis que_____1	Default	e	%default
tandis que	100	csu	[wh=+]	tandis que_____1	Default		%default
tant et si bien qu'	100	csu	[wh=+,@e]	tant et si bien que_____1	Default	e	%default
tant et si bien que	100	csu	[wh=+]	tant et si bien que_____1	Default		%default
tant qu'	100	csu	[wh=+,@e]	tant que_____1	Default	e	%default
tant que	100	csu	[wh=+]	tant que_____1	Default		%default
un jour qu'	100	csu	[wh=+,@e]	un jour que_____1	Default	e	%default
un jour que	100	csu	[wh=+]	un jour que_____1	Default		%default
une fois qu'	100	csu	[wh=+,@e]	une fois que_____1	Default	e	%default
une fois que	100	csu	[wh=+]	une fois que_____1	Default		%default
vu qu'	100	csu	[wh=+,@e]	vu que_____1	Default	e	%default
vu que	100	csu	[wh=+]	vu que_____1	Default		%default
à ce point qu'	100	csu	[wh=+,@e]	à ce point que_____1	Default	e	%default
à ce point que	100	csu	[wh=+]	à ce point que_____1	Default		%default
à ceci près qu'	100	csu	[wh=+,@e]	à ceci près que_____1	Default	e	%default
à ceci près que	100	csu	[wh=+]	à ceci près que_____1	Default		%default
à cela près qu'	100	csu	[wh=+,@e]	à cela près que_____1	Default	e	%default
à cela près que	100	csu	[wh=+]	à cela près que_____1	Default		%default
à chaque fois qu'	100	csu	[wh=+,@e]	à chaque fois que_____1	Default	e	%default
à chaque fois que	100	csu	[wh=+]	à chaque fois que_____1	Default		%default
à condition qu'	100	csu	[wh=+,@e]	à condition que_____1	Default	e	%default
à condition que	100	csu	[wh=+]	à condition que_____1	Default		%default
à l'heure où	100	csu	[]	à l'heure où_____1	Default		%default
à l'instant où	100	csu	[]	à l'instant où_____1	Default		%default
à l'époque où	100	csu	[]	à l'époque où_____1	Default		%default
à mesure qu'	100	csu	[wh=+,@e]	à mesure que_____1	Default	e	%default
à mesure que	100	csu	[wh=+]	à mesure que_____1	Default		%default
à moins qu'	100	csu	[wh=+,@e]	à moins que_____1	Default	e	%default
à moins que	100	csu	[wh=+]	à moins que_____1	Default		%default
à part qu'	100	csu	[wh=+,@e]	à part que_____1	Default	e	%default
à part que	100	csu	[wh=+]	à part que_____1	Default		%default
à partir du moment où	100	csu	[]	à partir du moment où_____1	Default		%default
à présent qu'	100	csu	[wh=+,@e]	à présent que_____1	Default	e	%default
à présent que	100	csu	[wh=+]	à présent que_____1	Default		%default
à seule fin qu'	100	csu	[wh=+,@e]	à seule fin que_____1	Default	e	%default
à seule fin que	100	csu	[wh=+]	à seule fin que_____1	Default		%default
à supposer qu'	100	csu	[wh=+,@e]	à supposer que_____1	Default	e	%default
à supposer que	100	csu	[wh=+]	à supposer que_____1	Default		%default
à tel point qu'	100	csu	[wh=+,@e]	à tel point que_____1	Default	e	%default
à tel point que	100	csu	[wh=+]	à tel point que_____1	Default		%default
à telle enseigne qu'	100	csu	[wh=+,@e]	à telle enseigne que_____1	Default	e	%default
à telle enseigne que	100	csu	[wh=+]	à telle enseigne que_____1	Default		%default
à ça près qu'	100	csu	[wh=+,@e]	à ça près que_____1	Default	e	%default
à ça près que	100	csu	[wh=+]	à ça près que_____1	Default		%default
étant donné qu'	100	csu	[wh=+,@e]	étant donné que_____1	Default	e	%default
étant donné que	100	csu	[wh=+]	étant donné que_____1	Default		%default
