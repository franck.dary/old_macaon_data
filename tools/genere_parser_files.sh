#! /bin/bash

function in_array {
  ARRAY=$2
  for e in ${ARRAY[*]}
  do
    if [[ "$e" == "$1" ]]
    then
      return 0
    fi
  done
  return 1
}


if [ "$#" -lt 4 ]; then
		echo "Usage : $0 mcd.mcd parser_dir tools_dir data_dir file.mcf"
		exit
fi

MCD=$1
PARSER_DIR=$2
TOOLS_DIR=$3
DATA_DIR=$4
MCFFILE=$4
ignore_W=false

dicts=${PARSER_DIR}/parser.dicts
fm=${PARSER_DIR}/parser.fm
testBD=${PARSER_DIR}/test.bd
trainBD=${PARSER_DIR}/train.bd
as=${PARSER_DIR}/parser.as

shift
shift
shift

ARGS=""
for arg in "$@"
do
		if [ "$arg" == "-w" ]
		then
				ignore_W=true
		else
				ARGS="$ARGS $arg"
		fi
done


####

rm -f $as

echo -e "Default : SHIFT\nREDUCE\nEOS" > $as

LABEL="$(grep LABEL $MCD | cut -f1 -d ' ')"

if [ -z "$LABEL" ]; then
		echo "ERROR : MCD file does not contain a LABEL column"
		exit
fi

LABEL=$((LABEL+1))

cut -f $LABEL $MCFFILE > tmp

for arg in $ARGS
do
		cut -f $LABEL $arg >> tmp
done

python3 ${TOOLS_DIR}/sortUniq.py tmp > tmp2

sed -e 's/^/RIGHT /' tmp2 | grep -v root >> $as
sed -e 's/^/LEFT /' tmp2 | grep -v root >> $as

rm tmp tmp2


##### Others

LIST_FEATS_TOTAL="$(cut -f 2 -d ' ' "$MCD" | sed '/^$/N;/^\n$/D' | sed '1{/^$/d}')"

LIST_FEATS_VAR="$(cut -f 2 -d ' ' "$MCD" | grep -v -E 'FORM|POS|LEMMA|GOV|LABEL|EOS' | sed '/^$/N;/^\n$/D' | sed '1{/^$/d}')"
rm -f tmp
for feat in ${LIST_FEATS_VAR}
do
		echo $feat >> tmp
done
LIST_MORPHO=$(grep -E 'M[0-9]+' tmp)
LIST_WALS=$(grep -v -E 'M[0-9]+|ID' tmp)

rm -f $dicts $testBD $trainBD $fm

echo -e "#Name\tDimension\tMode\t#\n###################################\nParser_actions\t10\tEmbeddings\nParser_bool\t03\tEmbeddings\nParser_int\t10\tEmbeddings\nParser_pos\t15\tEmbeddings\nParser_form\t05\tEmbeddings\nParser_labels\t15\tEmbeddings" > $dicts

for feat in ${LIST_FEATS_VAR}
do
    if [[ $feat == L_* ]]
    then
	echo -e "Parser_"$feat"\t01\tEmbeddings" >> $dicts
    elif [[ $feat == R_* ]]
    then
	echo -e "Parser_"$feat"\t01\tEmbeddings" >> $dicts
    elif [[ $feat == ID ]]
    then
	echo -e "Parser_"$feat"\t40\tOneHot" >> $dicts
    else	
	echo -e "Parser_"$feat"\t03\tEmbeddings" >> $dicts
    fi
done

echo -e "s.0.POS\ns.1.POS\ns.2.POS\nb.0.POS\nb.1.POS\nb.2.POS\nb.3.POS\nb.-1.POS\nb.-2.POS\nb.0.ldep.POS\ns.0.ldep.LABEL\ns.0.rdep.LABEL\ns.1.ldep.LABEL\ns.1.rdep.LABEL\nb.0.ldep.LABEL\nb.0.rdep.LABEL\nb.0.GOV\ns.0.GOV\ns.0.DIST.b.0\ntc.1\ntc.2\ntc.3" > $fm

for morpho in ${LIST_MORPHO}
do
		echo -e "b.0."$morpho"\ns.0."$morpho >> $fm
done



if ! ${ignore_W} ; then
    for featW in ${LIST_WALS}
    do
	echo "b.0."$featW >> $fm
    done
fi

ID="ID"

if in_array "ID" "${LIST_FEATS_VAR}" ; then 
    echo "b.0.ID" >> $fm
fi

echo -e "#Name\tref/hyp dict\tPolicy\tMust print?#\n############################################" > $testBD

echo -e "#Name\tref/hyp dict\tPolicy\tMust print?#\n############################################" > $trainBD

for feat in ${LIST_FEATS_TOTAL}
do
	if in_array "$feat" "FORM POS LEMMA GOV LABEL EOS"
	then
			if [[ "$feat" == "FORM" ]]
			then
					echo -e "FORM\tref\tform\tFinal\t1" >> $testBD
					echo -e "FORM\tref\tform\tFromZero\t1" >> $trainBD
			elif [[ "$feat" == "POS" ]]
			then
					echo -e "POS\tref\tpos\tFinal\t1" >> $testBD
					echo -e "POS\tref\tpos\tFromZero\t1" >> $trainBD
			elif [[ "$feat" == "LEMMA" ]]
			then
					echo -e "LEMMA\tref\tform\tFinal\t1" >> $testBD
					echo -e "LEMMA\tref\tform\tFromZero\t1" >> $trainBD
			elif [[ "$feat" == "GOV" ]]
			then
					echo -e "GOV\thyp\tint\tFinal\t1" >> $testBD
					echo -e "GOV\thyp\tint\tFromZero\t1" >> $trainBD
			elif [[ "$feat" == "LABEL" ]]
			then
					echo -e "LABEL\thyp\tlabels\tFinal\t1" >> $testBD
					echo -e "LABEL\thyp\tlabels\tFromZero\t1" >> $trainBD
			elif [[ "$feat" == "EOS" ]]
			then
					echo -e "EOS\thyp\tint\tFinal\t1" >> $testBD
					echo -e "EOS\thyp\tint\tFromZero\t1" >> $trainBD
			fi
	else
			echo -e $feat"\tref\t"$feat"\tFinal\t1" >> $testBD
			echo -e $feat"\tref\t"$feat"\tFromZero\t1" >> $trainBD 
	fi
done


rm tmp
