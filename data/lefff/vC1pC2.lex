Poss0 poids dans la balance	100	cf	[pred="jeter Poss0 poids dans la balance_____1<Suj:cln|sn>",synt_head=jeter]	jeter Poss0 poids dans la balance_____1	Default		%default
Poss0 poing dans la figure	100	cf	[pred="expédier Poss0 poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=expédier]	expédier Poss0 poing dans la figure_____1	Default		%default
Poss0 vie chèrement	100	cf	[pred="vendre Poss0 vie chèrement_____1<Suj:cln|sn>",synt_head=vendre]	vendre Poss0 vie chèrement_____1	Default		%default
Pâques avant Rameaux	100	cf	[pred="mettre Pâques avant Rameaux_____1<Suj:cln|sn>",synt_head=mettre]	mettre Pâques avant Rameaux_____1	Default		%default
_MESURE	100	cf	[pred="faire _MESURE_____1<Suj:cln|sn>",synt_head=faire]	faire _MESURE_____1	Default		%default
_MESURE Nmesure	100	cf	[pred="faire _MESURE Nmesure_____1<Suj:cln|sn>",synt_head=faire]	faire _MESURE Nmesure_____1	Default		%default
_MESURE de circonférence	100	cf	[pred="faire _MESURE de circonférence_____1<Suj:cln|sn>",synt_head=faire]	faire _MESURE de circonférence_____1	Default		%default
_MESURE de tour	100	cf	[pred="faire _MESURE de tour_____1<Suj:cln|sn>",synt_head=faire]	faire _MESURE de tour_____1	Default		%default
_NUM de longueur de manche	100	cf	[pred="faire _NUM de longueur de manche_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire _NUM de longueur de manche_____1	Default		%default
_NUM de tour de Npc	100	cf	[pred="faire _NUM de tour de Npc_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire _NUM de tour de Npc_____1	Default		%default
_NUM de tour de cou	100	cf	[pred="faire _NUM de tour de cou_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire _NUM de tour de cou_____1	Default		%default
_NUM de tour de poitrine	100	cf	[pred="faire _NUM de tour de poitrine_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire _NUM de tour de poitrine_____1	Default		%default
_NUM francs _NUM sous	100	cf	[pred="coûter _NUM francs _NUM sous_____1<Suj:cln|sn>",synt_head=coûter]	coûter _NUM francs _NUM sous_____1	Default		%default
_NUM heures au téléphone	100	cf	[pred="être _NUM heures au téléphone_____1<Suj:cln|sn>",synt_head=être]	être _NUM heures au téléphone_____1	Default		%default
_NUM mètres	100	cf	[pred="faire _NUM mètres_____1<Suj:cln|sn>",synt_head=faire]	faire _NUM mètres_____1	Default		%default
_NUM mètres de circonférence	100	cf	[pred="faire _NUM mètres de circonférence_____1<Suj:cln|sn>",synt_head=faire]	faire _NUM mètres de circonférence_____1	Default		%default
_NUM mètres de tour	100	cf	[pred="faire _NUM mètres de tour_____1<Suj:cln|sn>",synt_head=faire]	faire _NUM mètres de tour_____1	Default		%default
_NUM tours	100	cf	[pred="faire _NUM tours_____1<Suj:cln|sn>",synt_head=faire]	faire _NUM tours_____1	Default		%default
argent de tout	100	cf	[pred="faire argent de tout_____1<Suj:cln|sn>",synt_head=faire]	faire argent de tout_____1	Default		%default
armes de tout	100	cf	[pred="faire armes de tout_____1<Suj:cln|sn>",synt_head=faire]	faire armes de tout_____1	Default		%default
au lit	100	cf	[pred="faire au lit_____1<Suj:cln|sn>",synt_head=faire]	faire au lit_____1	Default		%default
bande à part	100	cf	[pred="faire bande à part_____1<Suj:cln|sn>",synt_head=faire]	faire bande à part_____1	Default		%default
bas les masques	100	cf	[pred="mettre bas les masques_____1<Suj:cln|sn>",synt_head=mettre]	mettre bas les masques_____1	Default		%default
bien	100	cf	[pred="faire bien_____1<Suj:cln|sn>",synt_head=faire]	faire bien_____1	Default		%default
bien Poss0 âge	100	cf	[pred="faire bien Poss0 âge_____1<Suj:cln|sn>",synt_head=faire]	faire bien Poss0 âge_____1	Default		%default
bien dans le décor	100	cf	[pred="faire bien dans le décor_____1<Suj:cln|sn>",synt_head=faire]	faire bien dans le décor_____1	Default		%default
bien dans le paysage	100	cf	[pred="faire bien dans le paysage_____1<Suj:cln|sn>",synt_head=faire]	faire bien dans le paysage_____1	Default		%default
bien dans le tableau	100	cf	[pred="faire bien dans le tableau_____1<Suj:cln|sn>",synt_head=faire]	faire bien dans le tableau_____1	Default		%default
bien de la suite	100	cf	[pred="augurer bien de la suite_____1<Suj:cln|sn>",synt_head=augurer]	augurer bien de la suite_____1	Default		%default
bien leur nom	100	cf	[pred="porter bien son nom_____1<Suj:cln|sn>",synt_head=porter]	porter bien son nom_____1	Default		%default
bien leur rôle	100	cf	[pred="posséder bien son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son rôle_____1	Default		%default
bien leur sujet	100	cf	[pred="posséder bien son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son sujet_____1	Default		%default
bien leur âge	100	cf	[pred="porter bien son âge_____1<Suj:cln|sn>",synt_head=porter]	porter bien son âge_____1	Default		%default
bien leurs _NUM ans	100	cf	[pred="faire bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire bien ses _NUM ans_____1	Default		%default
bien leurs _NUM ans	100	cf	[pred="porter bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=porter]	porter bien ses _NUM ans_____1	Default		%default
bien mes _NUM ans	100	cf	[pred="faire bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire bien ses _NUM ans_____1	Default		%default
bien mes _NUM ans	100	cf	[pred="porter bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=porter]	porter bien ses _NUM ans_____1	Default		%default
bien mon nom	100	cf	[pred="porter bien son nom_____1<Suj:cln|sn>",synt_head=porter]	porter bien son nom_____1	Default		%default
bien mon rôle	100	cf	[pred="posséder bien son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son rôle_____1	Default		%default
bien mon sujet	100	cf	[pred="posséder bien son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son sujet_____1	Default		%default
bien mon âge	100	cf	[pred="porter bien son âge_____1<Suj:cln|sn>",synt_head=porter]	porter bien son âge_____1	Default		%default
bien nos _NUM ans	100	cf	[pred="faire bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire bien ses _NUM ans_____1	Default		%default
bien nos _NUM ans	100	cf	[pred="porter bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=porter]	porter bien ses _NUM ans_____1	Default		%default
bien notre nom	100	cf	[pred="porter bien son nom_____1<Suj:cln|sn>",synt_head=porter]	porter bien son nom_____1	Default		%default
bien notre rôle	100	cf	[pred="posséder bien son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son rôle_____1	Default		%default
bien notre sujet	100	cf	[pred="posséder bien son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son sujet_____1	Default		%default
bien notre âge	100	cf	[pred="porter bien son âge_____1<Suj:cln|sn>",synt_head=porter]	porter bien son âge_____1	Default		%default
bien ses _NUM ans	100	cf	[pred="faire bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire bien ses _NUM ans_____1	Default		%default
bien ses _NUM ans	100	cf	[pred="porter bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=porter]	porter bien ses _NUM ans_____1	Default		%default
bien son nom	100	cf	[pred="porter bien son nom_____1<Suj:cln|sn>",synt_head=porter]	porter bien son nom_____1	Default		%default
bien son rôle	100	cf	[pred="posséder bien son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son rôle_____1	Default		%default
bien son sujet	100	cf	[pred="posséder bien son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son sujet_____1	Default		%default
bien son âge	100	cf	[pred="porter bien son âge_____1<Suj:cln|sn>",synt_head=porter]	porter bien son âge_____1	Default		%default
bien tes _NUM ans	100	cf	[pred="faire bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire bien ses _NUM ans_____1	Default		%default
bien tes _NUM ans	100	cf	[pred="porter bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=porter]	porter bien ses _NUM ans_____1	Default		%default
bien ton nom	100	cf	[pred="porter bien son nom_____1<Suj:cln|sn>",synt_head=porter]	porter bien son nom_____1	Default		%default
bien ton rôle	100	cf	[pred="posséder bien son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son rôle_____1	Default		%default
bien ton sujet	100	cf	[pred="posséder bien son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son sujet_____1	Default		%default
bien ton âge	100	cf	[pred="porter bien son âge_____1<Suj:cln|sn>",synt_head=porter]	porter bien son âge_____1	Default		%default
bien vos _NUM ans	100	cf	[pred="faire bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire bien ses _NUM ans_____1	Default		%default
bien vos _NUM ans	100	cf	[pred="porter bien ses _NUM ans_____1<Suj:cln|sn>",synt_head=porter]	porter bien ses _NUM ans_____1	Default		%default
bien votre nom	100	cf	[pred="porter bien son nom_____1<Suj:cln|sn>",synt_head=porter]	porter bien son nom_____1	Default		%default
bien votre rôle	100	cf	[pred="posséder bien son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son rôle_____1	Default		%default
bien votre sujet	100	cf	[pred="posséder bien son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder bien son sujet_____1	Default		%default
bien votre âge	100	cf	[pred="porter bien son âge_____1<Suj:cln|sn>",synt_head=porter]	porter bien son âge_____1	Default		%default
bien à table	100	cf	[pred="se tenir bien à table_____1<Suj:cln|sn>",@pron,synt_head=tenir]	se tenir bien à table_____1	Default		%default
bon chic bon genre	100	cf	[pred="faire bon chic bon genre_____1<Suj:cln|sn>",synt_head=faire]	faire bon chic bon genre_____1	Default		%default
bon chic bon genre	100	cf	[pred="être bon chic bon genre_____1<Suj:cln|sn>",synt_head=être]	être bon chic bon genre_____1	Default		%default
bon coeur contre mauvaise fortune	100	cf	[pred="faire bon coeur contre mauvaise fortune_____1<Suj:cln|sn>",synt_head=faire]	faire bon coeur contre mauvaise fortune_____1	Default		%default
bon la rampe	100	cf	[pred="tenir bon la rampe_____1<Suj:cln|sn>",synt_head=tenir]	tenir bon la rampe_____1	Default		%default
bouche cousue	100	cf	[pred="garder bouche cousue_____1<Suj:cln|sn>",synt_head=garder]	garder bouche cousue_____1	Default		%default
bourse à part	100	cf	[pred="faire bourse à part_____1<Suj:cln|sn>",synt_head=faire]	faire bourse à part_____1	Default		%default
buisson creux	100	cf	[pred="trouver buisson creux_____1<Suj:cln|sn>",synt_head=trouver]	trouver buisson creux_____1	Default		%default
caca	100	cf	[pred="faire caca_____1<Suj:cln|sn>",synt_head=faire]	faire caca_____1	Default		%default
caca dans leur culotte	100	cf	[pred="faire caca dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire caca dans sa culotte_____1	Default		%default
caca dans ma culotte	100	cf	[pred="faire caca dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire caca dans sa culotte_____1	Default		%default
caca dans notre culotte	100	cf	[pred="faire caca dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire caca dans sa culotte_____1	Default		%default
caca dans sa culotte	100	cf	[pred="faire caca dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire caca dans sa culotte_____1	Default		%default
caca dans ta culotte	100	cf	[pred="faire caca dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire caca dans sa culotte_____1	Default		%default
caca dans votre culotte	100	cf	[pred="faire caca dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire caca dans sa culotte_____1	Default		%default
cartes sur table	100	cf	[pred="jouer cartes sur table_____1<Suj:cln|sn>",synt_head=jouer]	jouer cartes sur table_____1	Default		%default
cartes sur tables	100	cf	[pred="mettre cartes sur tables_____1<Suj:cln|sn>",synt_head=mettre]	mettre cartes sur tables_____1	Default		%default
ce monde	100	cf	[pred="laisser ce monde_____1<Suj:cln|sn>",synt_head=laisser]	laisser ce monde_____1	Default		%default
ce monde	100	cf	[pred="quitter ce monde_____1<Suj:cln|sn>",synt_head=quitter]	quitter ce monde_____1	Default		%default
ce monde pour un autre monde	100	cf	[pred="laisser ce monde pour un autre monde_____1<Suj:cln|sn>",synt_head=laisser]	laisser ce monde pour un autre monde_____1	Default		%default
ce monde pour un autre monde	100	cf	[pred="quitter ce monde pour un autre monde_____1<Suj:cln|sn>",synt_head=quitter]	quitter ce monde pour un autre monde_____1	Default		%default
ce monde pour un monde meilleur	100	cf	[pred="laisser ce monde pour un monde meilleur_____1<Suj:cln|sn>",synt_head=laisser]	laisser ce monde pour un monde meilleur_____1	Default		%default
ce monde pour un monde meilleur	100	cf	[pred="quitter ce monde pour un monde meilleur_____1<Suj:cln|sn>",synt_head=quitter]	quitter ce monde pour un monde meilleur_____1	Default		%default
cela pour dit	100	cf	[pred="se tenir cela pour dit_____1<Suj:cln|sn>",@pron,synt_head=tenir]	se tenir cela pour dit_____1	Default		%default
cela pour rire	100	cf	[pred="dire cela pour rire_____1<Suj:cln|sn>",synt_head=dire]	dire cela pour rire_____1	Default		%default
chaud au coeur	100	cf	[pred="faire chaud au coeur_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire chaud au coeur_____1	Default		%default
chaussure à Poss0 pied	100	cf	[pred="trouver chaussure à Poss0 pied_____1<Suj:cln|sn>",synt_head=trouver]	trouver chaussure à Poss0 pied_____1	Default		%default
chèrement leur peau	100	cf	[pred="défendre chèrement sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre chèrement sa peau_____1	Default		%default
chèrement ma peau	100	cf	[pred="défendre chèrement sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre chèrement sa peau_____1	Default		%default
chèrement notre peau	100	cf	[pred="défendre chèrement sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre chèrement sa peau_____1	Default		%default
chèrement sa peau	100	cf	[pred="défendre chèrement sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre chèrement sa peau_____1	Default		%default
chèrement ta peau	100	cf	[pred="défendre chèrement sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre chèrement sa peau_____1	Default		%default
chèrement votre peau	100	cf	[pred="défendre chèrement sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre chèrement sa peau_____1	Default		%default
clair	100	cf	[pred="voir clair_____1<Suj:cln|sn>",synt_head=voir]	voir clair_____1	Default		%default
clair dans le jeu	100	cf	[pred="voir clair dans le jeu_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=voir]	voir clair dans le jeu_____1	Default		%default
cocu bon genre	100	cf	[pred="faire cocu bon genre_____1<Suj:cln|sn>",synt_head=faire]	faire cocu bon genre_____1	Default		%default
contre mauvaise fortune bon coeur	100	cf	[pred="faire contre mauvaise fortune bon coeur_____1<Suj:cln|sn>",synt_head=faire]	faire contre mauvaise fortune bon coeur_____1	Default		%default
contresens	100	cf	[pred="faire contresens_____1<Suj:cln|sn>",synt_head=faire]	faire contresens_____1	Default		%default
contresens après contresens	100	cf	[pred="faire contresens après contresens_____1<Suj:cln|sn>",synt_head=faire]	faire contresens après contresens_____1	Default		%default
contresens sur contresens	100	cf	[pred="faire contresens sur contresens_____1<Suj:cln|sn>",synt_head=faire]	faire contresens sur contresens_____1	Default		%default
coup double	100	cf	[pred="faire coup double_____1<Suj:cln|sn>",synt_head=faire]	faire coup double_____1	Default		%default
coup pour coup	100	cf	[pred="rendre coup pour coup_____1<Suj:cln|sn>",synt_head=rendre]	rendre coup pour coup_____1	Default		%default
cul sec	100	cf	[pred="faire cul sec_____1<Suj:cln|sn>",synt_head=faire]	faire cul sec_____1	Default		%default
dans le jeu	100	cf	[pred="voir dans le jeu_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=voir]	voir dans le jeu_____1	Default		%default
dans le panneau	100	cf	[pred="donner dans le panneau_____1<Suj:cln|sn>",synt_head=donner]	donner dans le panneau_____1	Default		%default
dans les gencives	100	cf	[pred="en prendre dans les gencives_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre dans les gencives_____1	Default		%default
dans leur culotte	100	cf	[pred="faire dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire dans sa culotte_____1	Default		%default
dans ma culotte	100	cf	[pred="faire dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire dans sa culotte_____1	Default		%default
dans notre culotte	100	cf	[pred="faire dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire dans sa culotte_____1	Default		%default
dans sa culotte	100	cf	[pred="faire dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire dans sa culotte_____1	Default		%default
dans ta culotte	100	cf	[pred="faire dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire dans sa culotte_____1	Default		%default
dans un hospice	100	cf	[pred="finir dans un hospice_____1<Suj:cln|sn>",synt_head=finir]	finir dans un hospice_____1	Default		%default
dans un hospice	100	cf	[pred="terminer dans un hospice_____1<Suj:cln|sn>",synt_head=terminer]	terminer dans un hospice_____1	Default		%default
dans votre culotte	100	cf	[pred="faire dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire dans sa culotte_____1	Default		%default
date	100	cf	[pred="faire date_____1<Suj:cln|sn>",synt_head=faire]	faire date_____1	Default		%default
date dans les annales	100	cf	[pred="faire date dans les annales_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire date dans les annales_____1	Default		%default
de l'huile dans les rouages	100	cf	[pred="mettre de l'huile dans les rouages_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre de l'huile dans les rouages_____1	Default		%default
de la eau dans le vin	100	cf	[pred="mettre de la eau dans le vin_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre de la eau dans le vin_____1	Default		%default
de la poudre aux yeux	100	cf	[pred="jeter de la poudre aux yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=jeter]	jeter de la poudre aux yeux_____1	Default		%default
de leurs forces	100	cf	[pred="présumer de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer de ses forces_____1	Default		%default
de mal	100	cf	[pred="ne faire de mal_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire de mal_____1	Default		%default
de mal à une mouche	100	cf	[pred="ne faire de mal à une mouche_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire de mal à une mouche_____1	Default		%default
de mes forces	100	cf	[pred="présumer de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer de ses forces_____1	Default		%default
de nos forces	100	cf	[pred="présumer de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer de ses forces_____1	Default		%default
de ordres de personne	100	cf	[pred="ne recevoir de ordres de personne_____1<Suj:cln|sn>",clneg =c +,synt_head=recevoir]	ne recevoir de ordres de personne_____1	Default		%default
de rire	100	cf	[pred="se tordre de rire_____1<Suj:cln|sn>",@pron,synt_head=tordre]	se tordre de rire_____1	Default		%default
de ses forces	100	cf	[pred="présumer de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer de ses forces_____1	Default		%default
de tes forces	100	cf	[pred="présumer de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer de ses forces_____1	Default		%default
de vos forces	100	cf	[pred="présumer de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer de ses forces_____1	Default		%default
de ça	100	cf	[pred="ne vouloir de ça_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça_____1	Default		%default
de ça chez elle	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez elles	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez eux	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez lui	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez moi	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez nous	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez soi	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez toi	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
de ça chez vous	100	cf	[pred="ne vouloir de ça chez lui_____1<Suj:cln|sn>",clneg =c +,synt_head=vouloir]	ne vouloir de ça chez lui_____1	Default		%default
des bouts d'allumettes sous les yeux	100	cf	[pred="mettre des bouts d'allumettes sous les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre des bouts d'allumettes sous les yeux_____1	Default		%default
des bâtons dans les roues	100	cf	[pred="mettre des bâtons dans les roues_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre des bâtons dans les roues_____1	Default		%default
des coups	100	cf	[pred="ramasser des coups_____1<Suj:cln|sn>",synt_head=ramasser]	ramasser des coups_____1	Default		%default
des coups	100	cf	[pred="se prendre des coups_____1<Suj:cln|sn>",@pron,synt_head=prendre]	se prendre des coups_____1	Default		%default
des coups dans la gueule	100	cf	[pred="ramasser des coups dans la gueule_____1<Suj:cln|sn>",synt_head=ramasser]	ramasser des coups dans la gueule_____1	Default		%default
des coups dans la gueule	100	cf	[pred="se prendre des coups dans la gueule_____1<Suj:cln|sn>",@pron,synt_head=prendre]	se prendre des coups dans la gueule_____1	Default		%default
des dettes derrière elle	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière elles	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière eux	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière lui	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière moi	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière nous	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière soi	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière toi	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des dettes derrière vous	100	cf	[pred="laisser des dettes derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des dettes derrière lui_____1	Default		%default
des enfants derrière elle	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière elles	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière eux	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière lui	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière moi	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière nous	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière soi	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière toi	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des enfants derrière vous	100	cf	[pred="laisser des enfants derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des enfants derrière lui_____1	Default		%default
des mille et des cents	100	cf	[pred="coûter des mille et des cents_____1<Suj:cln|sn>",synt_head=coûter]	coûter des mille et des cents_____1	Default		%default
des mille et des cents	100	cf	[pred="gagner des mille et des cents_____1<Suj:cln|sn>",synt_head=gagner]	gagner des mille et des cents_____1	Default		%default
des mille et des cents	100	cf	[pred="valoir des mille et des cents_____1<Suj:cln|sn>",synt_head=valoir]	valoir des mille et des cents_____1	Default		%default
des noeuds	100	cf	[pred="se faire des noeuds_____1<Suj:cln|sn>",@pron,synt_head=faire]	se faire des noeuds_____1	Default		%default
des noeuds dans les jambes	100	cf	[pred="se faire des noeuds dans les jambes_____1<Suj:cln|sn>",@pron,synt_head=faire]	se faire des noeuds dans les jambes_____1	Default		%default
des paroles en le air	100	cf	[pred="jeter des paroles en le air_____1<Suj:cln|sn>",synt_head=jeter]	jeter des paroles en le air_____1	Default		%default
des paroles en le air	100	cf	[pred="lancer des paroles en le air_____1<Suj:cln|sn>",synt_head=lancer]	lancer des paroles en le air_____1	Default		%default
des pets d'un âne mort	100	cf	[pred="tirer des pets d'un âne mort_____1<Suj:cln|sn>",synt_head=tirer]	tirer des pets d'un âne mort_____1	Default		%default
des plans	100	cf	[pred="tirer des plans_____1<Suj:cln|sn>",synt_head=tirer]	tirer des plans_____1	Default		%default
des plans sur la comète	100	cf	[pred="tirer des plans sur la comète_____1<Suj:cln|sn>",synt_head=tirer]	tirer des plans sur la comète_____1	Default		%default
des pleins et des déliés	100	cf	[pred="faire des pleins et des déliés_____1<Suj:cln|sn>",synt_head=faire]	faire des pleins et des déliés_____1	Default		%default
des poux dans la tête	100	cf	[pred="chercher des poux dans la tête_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=chercher]	chercher des poux dans la tête_____1	Default		%default
des progrès au envers	100	cf	[pred="faire des progrès au envers_____1<Suj:cln|sn>",synt_head=faire]	faire des progrès au envers_____1	Default		%default
des regrets autour de elle	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de elles	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de eux	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de lui	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de moi	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de nous	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de soi	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de toi	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets autour de vous	100	cf	[pred="laisser des regrets autour de lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets autour de lui_____1	Default		%default
des regrets derrière elle	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière elles	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière eux	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière lui	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière moi	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière nous	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière soi	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière toi	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des regrets derrière vous	100	cf	[pred="laisser des regrets derrière lui_____1<Suj:cln|sn>",synt_head=laisser]	laisser des regrets derrière lui_____1	Default		%default
des ronds dans la eau	100	cf	[pred="faire des ronds dans la eau_____1<Suj:cln|sn>",synt_head=faire]	faire des ronds dans la eau_____1	Default		%default
des vertes et des pas mûres	100	cf	[pred="en subir des vertes et des pas mûres_____1<Suj:cln|sn>",@pseudo-en,synt_head=subir]	en subir des vertes et des pas mûres_____1	Default		%default
des vertes et des pas mûres	100	cf	[pred="en voir des vertes et des pas mûres_____1<Suj:cln|sn>",@pseudo-en,synt_head=voir]	en voir des vertes et des pas mûres_____1	Default		%default
des vessies pour des lanternes	100	cf	[pred="prendre des vessies pour des lanternes_____1<Suj:cln|sn>",synt_head=prendre]	prendre des vessies pour des lanternes_____1	Default		%default
des yeux comme des soucoupes	100	cf	[pred="faire des yeux comme des soucoupes_____1<Suj:cln|sn>",synt_head=faire]	faire des yeux comme des soucoupes_____1	Default		%default
des yeux comme des soucoupes	100	cf	[pred="ouvrir des yeux comme des soucoupes_____1<Suj:cln|sn>",synt_head=ouvrir]	ouvrir des yeux comme des soucoupes_____1	Default		%default
deux coups de une pierre	100	cf	[pred="faire deux coups de une pierre_____1<Suj:cln|sn>",synt_head=faire]	faire deux coups de une pierre_____1	Default		%default
deux à deux	100	cf	[pred="différer deux à deux_____1<Suj:cln|sn>",synt_head=différer]	différer deux à deux_____1	Default		%default
douze balles dans la peau	100	cf	[pred="recevoir douze balles dans la peau_____1<Suj:cln|sn>",synt_head=recevoir]	recevoir douze balles dans la peau_____1	Default		%default
droit au but	100	cf	[pred="aller droit au but_____1<Suj:cln|sn>",synt_head=aller]	aller droit au but_____1	Default		%default
droit au fait	100	cf	[pred="aller droit au fait_____1<Suj:cln|sn>",synt_head=aller]	aller droit au fait_____1	Default		%default
droit devant elle	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant elles	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant eux	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant lui	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant moi	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant nous	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant soi	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant toi	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
droit devant vous	100	cf	[pred="foncer droit devant lui_____1<Suj:cln|sn>",synt_head=foncer]	foncer droit devant lui_____1	Default		%default
du _NUM	100	cf	[pred="faire du _NUM_____1<Suj:cln|sn>",synt_head=faire]	faire du _NUM_____1	Default		%default
du _NUM d'épaules	100	cf	[pred="faire du _NUM d'épaules_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire du _NUM d'épaules_____1	Default		%default
du _NUM de largeur	100	cf	[pred="faire du _NUM de largeur_____1<Suj:cln|sn>",synt_head=faire]	faire du _NUM de largeur_____1	Default		%default
du _NUM de long	100	cf	[pred="faire du _NUM de long_____1<Suj:cln|sn>",synt_head=faire]	faire du _NUM de long_____1	Default		%default
du _NUM de longueur	100	cf	[pred="faire du _NUM de longueur_____1<Suj:cln|sn>",synt_head=faire]	faire du _NUM de longueur_____1	Default		%default
du _NUM de longueur de manche	100	cf	[pred="faire du _NUM de longueur de manche_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire du _NUM de longueur de manche_____1	Default		%default
du _NUM de tour	100	cf	[pred="faire du _NUM de tour_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire du _NUM de tour_____1	Default		%default
du _NUM de tour de Npc	100	cf	[pred="faire du _NUM de tour de Npc_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire du _NUM de tour de Npc_____1	Default		%default
du _NUM de tour de cou	100	cf	[pred="faire du _NUM de tour de cou_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire du _NUM de tour de cou_____1	Default		%default
du _NUM de tour de poitrine	100	cf	[pred="faire du _NUM de tour de poitrine_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire du _NUM de tour de poitrine_____1	Default		%default
du _NUM à la heure	100	cf	[pred="faire du _NUM à la heure_____1<Suj:cln|sn>",synt_head=faire]	faire du _NUM à la heure_____1	Default		%default
du argent	100	cf	[pred="jeter du argent_____1<Suj:cln|sn>",synt_head=jeter]	jeter du argent_____1	Default		%default
du argent par les fenêtres	100	cf	[pred="jeter du argent par les fenêtres_____1<Suj:cln|sn>",synt_head=jeter]	jeter du argent par les fenêtres_____1	Default		%default
du baume au coeur	100	cf	[pred="mettre du baume au coeur_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre du baume au coeur_____1	Default		%default
du beurre dans les épinards	100	cf	[pred="mettre du beurre dans les épinards_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre du beurre dans les épinards_____1	Default		%default
du bruit	100	cf	[pred="faire du bruit_____1<Suj:cln|sn>",synt_head=faire]	faire du bruit_____1	Default		%default
du bruit dans Landernau	100	cf	[pred="faire du bruit dans Landernau_____1<Suj:cln|sn>",synt_head=faire]	faire du bruit dans Landernau_____1	Default		%default
du neuf avec du vieux	100	cf	[pred="faire du neuf avec du vieux_____1<Suj:cln|sn>",synt_head=faire]	faire du neuf avec du vieux_____1	Default		%default
du noir	100	cf	[pred="se mettre du noir_____1<Suj:cln|sn>",@pron,synt_head=mettre]	se mettre du noir_____1	Default		%default
du noir aux yeux	100	cf	[pred="se mettre du noir aux yeux_____1<Suj:cln|sn>",@pron,synt_head=mettre]	se mettre du noir aux yeux_____1	Default		%default
du pareil au même	100	cf	[pred="être du pareil au même_____1<Suj:cln|sn>",synt_head=être]	être du pareil au même_____1	Default		%default
du poil	100	cf	[pred="reprendre du poil_____1<Suj:cln|sn>",synt_head=reprendre]	reprendre du poil_____1	Default		%default
du poil de la bête	100	cf	[pred="perdre du poil de la bête_____1<Suj:cln|sn>",synt_head=perdre]	perdre du poil de la bête_____1	Default		%default
du poil de la bête	100	cf	[pred="reprendre du poil de la bête_____1<Suj:cln|sn>",synt_head=reprendre]	reprendre du poil de la bête_____1	Default		%default
du rouge	100	cf	[pred="se mettre du rouge_____1<Suj:cln|sn>",@pron,synt_head=mettre]	se mettre du rouge_____1	Default		%default
du rouge aux lèvres	100	cf	[pred="se mettre du rouge aux lèvres_____1<Suj:cln|sn>",@pron,synt_head=mettre]	se mettre du rouge aux lèvres_____1	Default		%default
du sel sur la queue	100	cf	[pred="mettre du sel sur la queue_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre du sel sur la queue_____1	Default		%default
du sucre sur le dos	100	cf	[pred="casser du sucre sur le dos_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=casser]	casser du sucre sur le dos_____1	Default		%default
eau	100	cf	[pred="faire eau_____1<Suj:cln|sn>",synt_head=faire]	faire eau_____1	Default		%default
eau de partout	100	cf	[pred="faire eau de partout_____1<Suj:cln|sn>",synt_head=faire]	faire eau de partout_____1	Default		%default
eau de toutes parts	100	cf	[pred="faire eau de toutes parts_____1<Suj:cln|sn>",synt_head=faire]	faire eau de toutes parts_____1	Default		%default
effet _DATE	100	cf	[pred="prendre effet _DATE_____1<Suj:cln|sn>",synt_head=prendre]	prendre effet _DATE_____1	Default		%default
en l'honneur	100	cf	[pred="boire en l'honneur_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=boire]	boire en l'honneur_____1	Default		%default
en prison	100	cf	[pred="finir en prison_____1<Suj:cln|sn>",synt_head=finir]	finir en prison_____1	Default		%default
en prison	100	cf	[pred="terminer en prison_____1<Suj:cln|sn>",synt_head=terminer]	terminer en prison_____1	Default		%default
en touche	100	cf	[pred="dégager en touche_____1<Suj:cln|sn>",synt_head=dégager]	dégager en touche_____1	Default		%default
encore rien vu	100	cf	[pred="n' avoir encore rien vu_____1<Suj:cln|sn>",clneg =c +,synt_head=avoir]	n' avoir encore rien vu_____1	Default		%default
entendu ça déjà	100	cf	[pred="avoir entendu ça déjà_____1<Suj:cln|sn>",synt_head=avoir]	avoir entendu ça déjà_____1	Default		%default
entre deux maux	100	cf	[pred="choisir entre deux maux_____1<Suj:cln|sn>",synt_head=choisir]	choisir entre deux maux_____1	Default		%default
erreur	100	cf	[pred="faire erreur_____1<Suj:cln|sn>",synt_head=faire]	faire erreur_____1	Default		%default
erreur après erreur	100	cf	[pred="faire erreur après erreur_____1<Suj:cln|sn>",synt_head=faire]	faire erreur après erreur_____1	Default		%default
erreur sur erreur	100	cf	[pred="faire erreur sur erreur_____1<Suj:cln|sn>",synt_head=faire]	faire erreur sur erreur_____1	Default		%default
exception à la règle	100	cf	[pred="faire exception à la règle_____1<Suj:cln|sn>",synt_head=faire]	faire exception à la règle_____1	Default		%default
face	100	cf	[pred="faire face_____1<Suj:cln|sn>",synt_head=faire]	faire face_____1	Default		%default
face à leurs engagements	100	cf	[pred="faire face à ses engagements_____1<Suj:cln|sn>",synt_head=faire]	faire face à ses engagements_____1	Default		%default
face à mes engagements	100	cf	[pred="faire face à ses engagements_____1<Suj:cln|sn>",synt_head=faire]	faire face à ses engagements_____1	Default		%default
face à nos engagements	100	cf	[pred="faire face à ses engagements_____1<Suj:cln|sn>",synt_head=faire]	faire face à ses engagements_____1	Default		%default
face à ses engagements	100	cf	[pred="faire face à ses engagements_____1<Suj:cln|sn>",synt_head=faire]	faire face à ses engagements_____1	Default		%default
face à tes engagements	100	cf	[pred="faire face à ses engagements_____1<Suj:cln|sn>",synt_head=faire]	faire face à ses engagements_____1	Default		%default
face à toute éventualité	100	cf	[pred="faire face à toute éventualité_____1<Suj:cln|sn>",synt_head=faire]	faire face à toute éventualité_____1	Default		%default
face à vos engagements	100	cf	[pred="faire face à ses engagements_____1<Suj:cln|sn>",synt_head=faire]	faire face à ses engagements_____1	Default		%default
fait du cheval sur un tonneau	100	cf	[pred="avoir fait du cheval sur un tonneau_____1<Suj:cln|sn>",synt_head=avoir]	avoir fait du cheval sur un tonneau_____1	Default		%default
famine sur un tas de blé	100	cf	[pred="crier famine sur un tas de blé_____1<Suj:cln|sn>",synt_head=crier]	crier famine sur un tas de blé_____1	Default		%default
faute après faute	100	cf	[pred="faire faute après faute_____1<Suj:cln|sn>",synt_head=faire]	faire faute après faute_____1	Default		%default
faute sur faute	100	cf	[pred="faire faute sur faute_____1<Suj:cln|sn>",synt_head=faire]	faire faute sur faute_____1	Default		%default
favorablement de la suite	100	cf	[pred="augurer favorablement de la suite_____1<Suj:cln|sn>",synt_head=augurer]	augurer favorablement de la suite_____1	Default		%default
feu de les quatre fers	100	cf	[pred="faire feu de les quatre fers_____1<Suj:cln|sn>",synt_head=faire]	faire feu de les quatre fers_____1	Default		%default
feu de tout bois	100	cf	[pred="faire feu de tout bois_____1<Suj:cln|sn>",synt_head=faire]	faire feu de tout bois_____1	Default		%default
feu et flamme	100	cf	[pred="jeter feu et flamme_____1<Suj:cln|sn>",synt_head=jeter]	jeter feu et flamme_____1	Default		%default
fin à leur existence	100	cf	[pred="mettre fin à son existence_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à son existence_____1	Default		%default
fin à leur vie	100	cf	[pred="mettre fin à sa vie_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à sa vie_____1	Default		%default
fin à leurs jours	100	cf	[pred="mettre fin à ses jours_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à ses jours_____1	Default		%default
fin à ma vie	100	cf	[pred="mettre fin à sa vie_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à sa vie_____1	Default		%default
fin à mes jours	100	cf	[pred="mettre fin à ses jours_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à ses jours_____1	Default		%default
fin à mon existence	100	cf	[pred="mettre fin à son existence_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à son existence_____1	Default		%default
fin à nos jours	100	cf	[pred="mettre fin à ses jours_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à ses jours_____1	Default		%default
fin à notre existence	100	cf	[pred="mettre fin à son existence_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à son existence_____1	Default		%default
fin à notre vie	100	cf	[pred="mettre fin à sa vie_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à sa vie_____1	Default		%default
fin à sa vie	100	cf	[pred="mettre fin à sa vie_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à sa vie_____1	Default		%default
fin à ses jours	100	cf	[pred="mettre fin à ses jours_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à ses jours_____1	Default		%default
fin à son existence	100	cf	[pred="mettre fin à son existence_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à son existence_____1	Default		%default
fin à ta vie	100	cf	[pred="mettre fin à sa vie_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à sa vie_____1	Default		%default
fin à tes jours	100	cf	[pred="mettre fin à ses jours_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à ses jours_____1	Default		%default
fin à ton existence	100	cf	[pred="mettre fin à son existence_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à son existence_____1	Default		%default
fin à vos jours	100	cf	[pred="mettre fin à ses jours_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à ses jours_____1	Default		%default
fin à votre existence	100	cf	[pred="mettre fin à son existence_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à son existence_____1	Default		%default
fin à votre vie	100	cf	[pred="mettre fin à sa vie_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre fin à sa vie_____1	Default		%default
flamberge au vent	100	cf	[pred="mettre flamberge au vent_____1<Suj:cln|sn>",synt_head=mettre]	mettre flamberge au vent_____1	Default		%default
flèche de tout bois	100	cf	[pred="faire flèche de tout bois_____1<Suj:cln|sn>",synt_head=faire]	faire flèche de tout bois_____1	Default		%default
français comme une vache espagnole	100	cf	[pred="parler français comme une vache espagnole_____1<Suj:cln|sn>",synt_head=parler]	parler français comme une vache espagnole_____1	Default		%default
froid dans le dos	100	cf	[pred="faire froid dans le dos_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire froid dans le dos_____1	Default		%default
gaffe	100	cf	[pred="faire gaffe_____1<Suj:cln|sn>",synt_head=faire]	faire gaffe_____1	Default		%default
gaffe après gaffe	100	cf	[pred="faire gaffe après gaffe_____1<Suj:cln|sn>",synt_head=faire]	faire gaffe après gaffe_____1	Default		%default
gaffe sur gaffe	100	cf	[pred="faire gaffe sur gaffe_____1<Suj:cln|sn>",synt_head=faire]	faire gaffe sur gaffe_____1	Default		%default
goût à la vie	100	cf	[pred="reprendre goût à la vie_____1<Suj:cln|sn>",synt_head=reprendre]	reprendre goût à la vie_____1	Default		%default
grand-mère dans les orties	100	cf	[pred="pousser grand-mère dans les orties_____1<Suj:cln|sn>",synt_head=pousser]	pousser grand-mère dans les orties_____1	Default		%default
grâce aux yeux	100	cf	[pred="trouver grâce aux yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=trouver]	trouver grâce aux yeux_____1	Default		%default
haro sur le baudet	100	cf	[pred="crier haro sur le baudet_____1<Suj:cln|sn>",synt_head=crier]	crier haro sur le baudet_____1	Default		%default
haut	100	cf	[pred="parler haut_____1<Suj:cln|sn>",synt_head=parler]	parler haut_____1	Default		%default
haut et fort	100	cf	[pred="parler haut et fort_____1<Suj:cln|sn>",synt_head=parler]	parler haut et fort_____1	Default		%default
ho hisse	100	cf	[pred="faire ho hisse_____1<Suj:cln|sn>",synt_head=faire]	faire ho hisse_____1	Default		%default
honneur au repas	100	cf	[pred="faire honneur au repas_____1<Suj:cln|sn>",synt_head=faire]	faire honneur au repas_____1	Default		%default
inaperçu	100	cf	[pred="passer inaperçu_____1<Suj:cln|sn>",synt_head=passer]	passer inaperçu_____1	Default		%default
inaperçu aux yeux	100	cf	[pred="passer inaperçu aux yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=passer]	passer inaperçu aux yeux_____1	Default		%default
injure pour injure	100	cf	[pred="rendre injure pour injure_____1<Suj:cln|sn>",synt_head=rendre]	rendre injure pour injure_____1	Default		%default
jamais ouvert un livre	100	cf	[pred="n' avoir jamais ouvert un livre _____1<Suj:cln|sn>",clneg =c +,synt_head=avoir]	n' avoir jamais ouvert un livre _____1	Default		%default
jamais à pareille fête	100	cf	[pred="n' aller jamais à pareille fête_____1<Suj:cln|sn>",clneg =c +,synt_head=aller]	n' aller jamais à pareille fête_____1	Default		%default
jour	100	cf	[pred="se faire jour_____1<Suj:cln|sn>",@pron,synt_head=faire]	se faire jour_____1	Default		%default
jour dans le esprit	100	cf	[pred="se faire jour dans le esprit_____1<Suj:cln|sn,Objde:de-sn|en>",@pron,synt_head=faire]	se faire jour dans le esprit_____1	Default		%default
la Marseillaise en Breton	100	cf	[pred="chanter la Marseillaise en Breton_____1<Suj:cln|sn>",synt_head=chanter]	chanter la Marseillaise en Breton_____1	Default		%default
la arme à gauche	100	cf	[pred="passer la arme à gauche_____1<Suj:cln|sn>",synt_head=passer]	passer la arme à gauche_____1	Default		%default
la bague au doigt	100	cf	[pred="passer la bague au doigt_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=passer]	passer la bague au doigt_____1	Default		%default
la balle	100	cf	[pred="prendre la balle_____1<Suj:cln|sn>",synt_head=prendre]	prendre la balle_____1	Default		%default
la balle au bond	100	cf	[pred="attraper la balle au bond_____1<Suj:cln|sn>",synt_head=attraper]	attraper la balle au bond_____1	Default		%default
la balle au bond	100	cf	[pred="prendre la balle au bond_____1<Suj:cln|sn>",synt_head=prendre]	prendre la balle au bond_____1	Default		%default
la balle au bond	100	cf	[pred="rattraper la balle au bond_____1<Suj:cln|sn>",synt_head=rattraper]	rattraper la balle au bond_____1	Default		%default
la balle au bond	100	cf	[pred="saisir la balle au bond_____1<Suj:cln|sn>",synt_head=saisir]	saisir la balle au bond_____1	Default		%default
la balle en touche	100	cf	[pred="dégager la balle en touche_____1<Suj:cln|sn>",synt_head=dégager]	dégager la balle en touche_____1	Default		%default
la blonde et la brune	100	cf	[pred="courtiser la blonde et la brune_____1<Suj:cln|sn>",synt_head=courtiser]	courtiser la blonde et la brune_____1	Default		%default
la bouche pleine	100	cf	[pred="parler la bouche pleine_____1<Suj:cln|sn>",synt_head=parler]	parler la bouche pleine_____1	Default		%default
la chandelle par les deux bouts	100	cf	[pred="brûler la chandelle par les deux bouts_____1<Suj:cln|sn>",synt_head=brûler]	brûler la chandelle par les deux bouts_____1	Default		%default
la chasse aux mouches	100	cf	[pred="faire la chasse aux mouches_____1<Suj:cln|sn>",synt_head=faire]	faire la chasse aux mouches_____1	Default		%default
la clé sous la porte	100	cf	[pred="mettre la clé sous la porte_____1<Suj:cln|sn>",synt_head=mettre]	mettre la clé sous la porte_____1	Default		%default
la clé sous le paillasson	100	cf	[pred="mettre la clé sous le paillasson_____1<Suj:cln|sn>",synt_head=mettre]	mettre la clé sous le paillasson_____1	Default		%default
la corde au cou	100	cf	[pred="passer la corde au cou_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=passer]	passer la corde au cou_____1	Default		%default
la corde au cou	100	cf	[pred="se mettre la corde au cou_____1<Suj:cln|sn,Objde:de-sn|en>",@pron,synt_head=mettre]	se mettre la corde au cou_____1	Default		%default
la couverture à elle	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à elles	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à eux	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à lui	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à moi	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à nous	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à soi	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à toi	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la couverture à vous	100	cf	[pred="tirer la couverture à lui_____1<Suj:cln|sn>",synt_head=tirer]	tirer la couverture à lui_____1	Default		%default
la crosse en le air	100	cf	[pred="mettre la crosse en le air_____1<Suj:cln|sn>",synt_head=mettre]	mettre la crosse en le air_____1	Default		%default
la face contre terre	100	cf	[pred="tomber la face contre terre_____1<Suj:cln|sn>",synt_head=tomber]	tomber la face contre terre_____1	Default		%default
la feuille au envers	100	cf	[pred="regarder la feuille au envers_____1<Suj:cln|sn>",synt_head=regarder]	regarder la feuille au envers_____1	Default		%default
la feuille à l'envers	100	cf	[pred="voir la feuille à l'envers_____1<Suj:cln|sn>",synt_head=voir]	voir la feuille à l'envers_____1	Default		%default
la grève sur le tas	100	cf	[pred="faire la grève sur le tas_____1<Suj:cln|sn>",synt_head=faire]	faire la grève sur le tas_____1	Default		%default
la gueule ouverte	100	cf	[pred="crever la gueule ouverte_____1<Suj:cln|sn>",synt_head=crever]	crever la gueule ouverte_____1	Default		%default
la gueule par terre	100	cf	[pred="se fiche la gueule par terre_____1<Suj:cln|sn>",@pron,synt_head=fiche]	se fiche la gueule par terre_____1	Default		%default
la gueule par terre	100	cf	[pred="se flanquer la gueule par terre_____1<Suj:cln|sn>",@pron,synt_head=flanquer]	se flanquer la gueule par terre_____1	Default		%default
la gueule par terre	100	cf	[pred="se foutre la gueule par terre_____1<Suj:cln|sn>",@pron,synt_head=foutre]	se foutre la gueule par terre_____1	Default		%default
la insulte à la injure	100	cf	[pred="joindre la insulte à la injure_____1<Suj:cln|sn>",synt_head=joindre]	joindre la insulte à la injure_____1	Default		%default
la laine sur le dos	100	cf	[pred="tondre la laine sur le dos_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tondre]	tondre la laine sur le dos_____1	Default		%default
la lune avec Poss0 dents	100	cf	[pred="attraper la lune avec Poss0 dents_____1<Suj:cln|sn>",synt_head=attraper]	attraper la lune avec Poss0 dents_____1	Default		%default
la lune avec leurs dents	100	cf	[pred="prendre la lune avec ses dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre la lune avec ses dents_____1	Default		%default
la lune avec mes dents	100	cf	[pred="prendre la lune avec ses dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre la lune avec ses dents_____1	Default		%default
la lune avec nos dents	100	cf	[pred="prendre la lune avec ses dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre la lune avec ses dents_____1	Default		%default
la lune avec ses dents	100	cf	[pred="prendre la lune avec ses dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre la lune avec ses dents_____1	Default		%default
la lune avec tes dents	100	cf	[pred="prendre la lune avec ses dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre la lune avec ses dents_____1	Default		%default
la lune avec vos dents	100	cf	[pred="prendre la lune avec ses dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre la lune avec ses dents_____1	Default		%default
la main au collet	100	cf	[pred="mettre la main au collet_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre la main au collet_____1	Default		%default
la main au ouvrage	100	cf	[pred="mettre la main au ouvrage_____1<Suj:cln|sn>",synt_head=mettre]	mettre la main au ouvrage_____1	Default		%default
la main au panier	100	cf	[pred="mettre la main au panier_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre la main au panier_____1	Default		%default
la main dans le dos	100	cf	[pred="passer la main dans le dos_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=passer]	passer la main dans le dos_____1	Default		%default
la main sur le coeur	100	cf	[pred="mettre la main sur le coeur_____1<Suj:cln|sn>",synt_head=mettre]	mettre la main sur le coeur_____1	Default		%default
la main sur le paletot	100	cf	[pred="mettre la main sur le paletot_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre la main sur le paletot_____1	Default		%default
la main à la bouche	100	cf	[pred="mettre la main à la bouche_____1<Suj:cln|sn>",synt_head=mettre]	mettre la main à la bouche_____1	Default		%default
la main à la poche	100	cf	[pred="mettre la main à la poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre la main à la poche_____1	Default		%default
la main à la pâte	100	cf	[pred="mettre la main à la pâte_____1<Suj:cln|sn>",synt_head=mettre]	mettre la main à la pâte_____1	Default		%default
la main à leur chapeau	100	cf	[pred="porter la main à son chapeau_____1<Suj:cln|sn>",synt_head=porter]	porter la main à son chapeau_____1	Default		%default
la main à mon chapeau	100	cf	[pred="porter la main à son chapeau_____1<Suj:cln|sn>",synt_head=porter]	porter la main à son chapeau_____1	Default		%default
la main à notre chapeau	100	cf	[pred="porter la main à son chapeau_____1<Suj:cln|sn>",synt_head=porter]	porter la main à son chapeau_____1	Default		%default
la main à son chapeau	100	cf	[pred="porter la main à son chapeau_____1<Suj:cln|sn>",synt_head=porter]	porter la main à son chapeau_____1	Default		%default
la main à ton chapeau	100	cf	[pred="porter la main à son chapeau_____1<Suj:cln|sn>",synt_head=porter]	porter la main à son chapeau_____1	Default		%default
la main à votre chapeau	100	cf	[pred="porter la main à son chapeau_____1<Suj:cln|sn>",synt_head=porter]	porter la main à son chapeau_____1	Default		%default
la mer et les poissons	100	cf	[pred="avaler la mer et les poissons_____1<Suj:cln|sn>",synt_head=avaler]	avaler la mer et les poissons_____1	Default		%default
la mer et les poissons	100	cf	[pred="boire la mer et les poissons_____1<Suj:cln|sn>",synt_head=boire]	boire la mer et les poissons_____1	Default		%default
la moelle de les os	100	cf	[pred="tirer la moelle de les os_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tirer]	tirer la moelle de les os_____1	Default		%default
la mort de près	100	cf	[pred="voir la mort de près_____1<Suj:cln|sn>",synt_head=voir]	voir la mort de près_____1	Default		%default
la mort en face	100	cf	[pred="contempler la mort en face_____1<Suj:cln|sn>",synt_head=contempler]	contempler la mort en face_____1	Default		%default
la mort en face	100	cf	[pred="regarder la mort en face_____1<Suj:cln|sn>",synt_head=regarder]	regarder la mort en face_____1	Default		%default
la mort en face	100	cf	[pred="voir la mort en face_____1<Suj:cln|sn>",synt_head=voir]	voir la mort en face_____1	Default		%default
la occasion	100	cf	[pred="saisir la occasion_____1<Suj:cln|sn>",synt_head=saisir]	saisir la occasion_____1	Default		%default
la occasion aux cheveux	100	cf	[pred="prendre la occasion aux cheveux_____1<Suj:cln|sn>",synt_head=prendre]	prendre la occasion aux cheveux_____1	Default		%default
la occasion aux cheveux	100	cf	[pred="saisir la occasion aux cheveux_____1<Suj:cln|sn>",synt_head=saisir]	saisir la occasion aux cheveux_____1	Default		%default
la paix dans la âme	100	cf	[pred="ramener la paix dans la âme_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=ramener]	ramener la paix dans la âme_____1	Default		%default
la peau des fesses	100	cf	[pred="coûter la peau des fesses_____1<Suj:cln|sn>",synt_head=coûter]	coûter la peau des fesses_____1	Default		%default
la peau du cul	100	cf	[pred="coûter la peau du cul_____1<Suj:cln|sn>",synt_head=coûter]	coûter la peau du cul_____1	Default		%default
la place aux jeunes	100	cf	[pred="laisser la place aux jeunes_____1<Suj:cln|sn>",synt_head=laisser]	laisser la place aux jeunes_____1	Default		%default
la première levée	100	cf	[pred="être la première levée_____1<Suj:cln|sn>",synt_head=être]	être la première levée_____1	Default		%default
la première levée et la dernière couchée	100	cf	[pred="être la première levée et la dernière couchée_____1<Suj:cln|sn>",synt_head=être]	être la première levée et la dernière couchée_____1	Default		%default
la proie pour l'ombre	100	cf	[pred="abandonner la proie pour l'ombre_____1<Suj:cln|sn>",synt_head=abandonner]	abandonner la proie pour l'ombre_____1	Default		%default
la proie pour la ombre	100	cf	[pred="laisser la proie pour la ombre_____1<Suj:cln|sn>",synt_head=laisser]	laisser la proie pour la ombre_____1	Default		%default
la proie pour la ombre	100	cf	[pred="lâcher la proie pour la ombre_____1<Suj:cln|sn>",synt_head=lâcher]	lâcher la proie pour la ombre_____1	Default		%default
la question	100	cf	[pred="toucher la question_____1<Suj:cln|sn>",synt_head=toucher]	toucher la question_____1	Default		%default
la question du doigt	100	cf	[pred="toucher la question du doigt_____1<Suj:cln|sn>",synt_head=toucher]	toucher la question du doigt_____1	Default		%default
la robe	100	cf	[pred="quitter la robe_____1<Suj:cln|sn>",synt_head=quitter]	quitter la robe_____1	Default		%default
la robe pour la épée	100	cf	[pred="quitter la robe pour la épée_____1<Suj:cln|sn>",synt_head=quitter]	quitter la robe pour la épée_____1	Default		%default
la soupe sur la tête	100	cf	[pred="manger la soupe sur la tête_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=manger]	manger la soupe sur la tête_____1	Default		%default
la tête	100	cf	[pred="renverser la tête_____1<Suj:cln|sn>",synt_head=renverser]	renverser la tête_____1	Default		%default
la tête contre les murs	100	cf	[pred="se cogner la tête contre les murs_____1<Suj:cln|sn>",@pron,synt_head=cogner]	se cogner la tête contre les murs_____1	Default		%default
la tête contre les murs	100	cf	[pred="se taper la tête contre les murs_____1<Suj:cln|sn>",@pron,synt_head=taper]	se taper la tête contre les murs_____1	Default		%default
la tête en arrière	100	cf	[pred="rejeter la tête en arrière_____1<Suj:cln|sn>",synt_head=rejeter]	rejeter la tête en arrière_____1	Default		%default
la tête en arrière	100	cf	[pred="renverser la tête en arrière_____1<Suj:cln|sn>",synt_head=renverser]	renverser la tête en arrière_____1	Default		%default
la tête froide	100	cf	[pred="conserver la tête froide_____1<Suj:cln|sn>",synt_head=conserver]	conserver la tête froide_____1	Default		%default
la tête froide	100	cf	[pred="garder la tête froide_____1<Suj:cln|sn>",synt_head=garder]	garder la tête froide_____1	Default		%default
la tête à deux mains	100	cf	[pred="se prendre la tête à deux mains_____1<Suj:cln|sn>",@pron,synt_head=prendre]	se prendre la tête à deux mains_____1	Default		%default
la veuve et l'orphelin	100	cf	[pred="défendre la veuve et l'orphelin_____1<Suj:cln|sn>",synt_head=défendre]	défendre la veuve et l'orphelin_____1	Default		%default
la vie	100	cf	[pred="laisser la vie_____1<Suj:cln|sn>",synt_head=laisser]	laisser la vie_____1	Default		%default
la vie en rose	100	cf	[pred="voir la vie en rose_____1<Suj:cln|sn>",synt_head=voir]	voir la vie en rose_____1	Default		%default
la vie à grandes guides	100	cf	[pred="mener la vie à grandes guides_____1<Suj:cln|sn>",synt_head=mener]	mener la vie à grandes guides_____1	Default		%default
la vérité en face	100	cf	[pred="regarder la vérité en face_____1<Suj:cln|sn>",synt_head=regarder]	regarder la vérité en face_____1	Default		%default
le argent où il est	100	cf	[pred="prendre le argent où il est_____1<Suj:cln|sn>",synt_head=prendre]	prendre le argent où il est_____1	Default		%default
le ballon en touche	100	cf	[pred="dégager le ballon en touche_____1<Suj:cln|sn>",synt_head=dégager]	dégager le ballon en touche_____1	Default		%default
le beau temps	100	cf	[pred="amener le beau temps_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps_____1	Default		%default
le beau temps avec elle	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec elles	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec eux	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec lui	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec moi	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec nous	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec soi	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec toi	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beau temps avec vous	100	cf	[pred="amener le beau temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le beau temps avec lui_____1	Default		%default
le beurre et le argent du beurre	100	cf	[pred="vouloir le beurre et le argent du beurre_____1<Suj:cln|sn>",synt_head=vouloir]	vouloir le beurre et le argent du beurre_____1	Default		%default
le bien pour le mal	100	cf	[pred="rendre le bien pour le mal_____1<Suj:cln|sn>",synt_head=rendre]	rendre le bien pour le mal_____1	Default		%default
le bouchon trop loin	100	cf	[pred="pousser le bouchon trop loin_____1<Suj:cln|sn>",synt_head=pousser]	pousser le bouchon trop loin_____1	Default		%default
le buste en arrière	100	cf	[pred="rejeter le buste en arrière_____1<Suj:cln|sn>",synt_head=rejeter]	rejeter le buste en arrière_____1	Default		%default
le calme dans l'esprit	100	cf	[pred="ramener le calme dans l'esprit_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=ramener]	ramener le calme dans l'esprit_____1	Default		%default
le caviar à la louche	100	cf	[pred="manger le caviar à la louche_____1<Suj:cln|sn>",synt_head=manger]	manger le caviar à la louche_____1	Default		%default
le chaud et le froid	100	cf	[pred="souffler le chaud et le froid_____1<Suj:cln|sn>",synt_head=souffler]	souffler le chaud et le froid_____1	Default		%default
le chemin seul	100	cf	[pred="faire le chemin seul_____1<Suj:cln|sn>",synt_head=faire]	faire le chemin seul_____1	Default		%default
le cou à une bouteille	100	cf	[pred="tordre le cou à une bouteille_____1<Suj:cln|sn>",synt_head=tordre]	tordre le cou à une bouteille_____1	Default		%default
le couteau dans la plaie	100	cf	[pred="remuer le couteau dans la plaie_____1<Suj:cln|sn>",synt_head=remuer]	remuer le couteau dans la plaie_____1	Default		%default
le couteau dans la plaie	100	cf	[pred="retourner le couteau dans la plaie_____1<Suj:cln|sn>",synt_head=retourner]	retourner le couteau dans la plaie_____1	Default		%default
le cul par terre	100	cf	[pred="se taper le cul par terre_____1<Suj:cln|sn>",@pron,synt_head=taper]	se taper le cul par terre_____1	Default		%default
le derrière par terre	100	cf	[pred="se taper le derrière par terre_____1<Suj:cln|sn>",@pron,synt_head=taper]	se taper le derrière par terre_____1	Default		%default
le diable et ses cornes	100	cf	[pred="mangerait le diable et ses cornes_____1<Suj:cln|sn>",synt_head=mangerait]	mangerait le diable et ses cornes_____1	Default		%default
le diable par la queue	100	cf	[pred="tirer le diable par la queue_____1<Suj:cln|sn>",synt_head=tirer]	tirer le diable par la queue_____1	Default		%default
le diable à quatre	100	cf	[pred="faire le diable à quatre_____1<Suj:cln|sn>",synt_head=faire]	faire le diable à quatre_____1	Default		%default
le doigt dans le oeil	100	cf	[pred="se fiche le doigt dans le oeil_____1<Suj:cln|sn>",@pron,synt_head=fiche]	se fiche le doigt dans le oeil_____1	Default		%default
le doigt dans le oeil	100	cf	[pred="se fourrer le doigt dans le oeil_____1<Suj:cln|sn>",@pron,synt_head=fourrer]	se fourrer le doigt dans le oeil_____1	Default		%default
le doigt dans le oeil	100	cf	[pred="se foutre le doigt dans le oeil_____1<Suj:cln|sn>",@pron,synt_head=foutre]	se foutre le doigt dans le oeil_____1	Default		%default
le doigt dans le oeil	100	cf	[pred="se mettre le doigt dans le oeil_____1<Suj:cln|sn>",@pron,synt_head=mettre]	se mettre le doigt dans le oeil_____1	Default		%default
le doigt sur la bouche	100	cf	[pred="mettre le doigt sur la bouche_____1<Suj:cln|sn>",synt_head=mettre]	mettre le doigt sur la bouche_____1	Default		%default
le doigt sur la plaie	100	cf	[pred="mettre le doigt sur la plaie_____1<Suj:cln|sn>",synt_head=mettre]	mettre le doigt sur la plaie_____1	Default		%default
le flanc	100	cf	[pred="prêter le flanc_____1<Suj:cln|sn>",synt_head=prêter]	prêter le flanc_____1	Default		%default
le flanc à la critique	100	cf	[pred="prêter le flanc à la critique_____1<Suj:cln|sn>",synt_head=prêter]	prêter le flanc à la critique_____1	Default		%default
le fond	100	cf	[pred="connaître le fond_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=connaître]	connaître le fond_____1	Default		%default
le fond et le tréfond	100	cf	[pred="connaître le fond et le tréfond_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=connaître]	connaître le fond et le tréfond_____1	Default		%default
le français comme un basque espagnol	100	cf	[pred="parler le français comme un basque espagnol_____1<Suj:cln|sn>",synt_head=parler]	parler le français comme un basque espagnol_____1	Default		%default
le geste à la parole	100	cf	[pred="joindre le geste à la parole_____1<Suj:cln|sn>",synt_head=joindre]	joindre le geste à la parole_____1	Default		%default
le loup dans la bergerie	100	cf	[pred="enfermer le loup dans la bergerie_____1<Suj:cln|sn>",synt_head=enfermer]	enfermer le loup dans la bergerie_____1	Default		%default
le mal par le mal	100	cf	[pred="combattre le mal par le mal_____1<Suj:cln|sn>",synt_head=combattre]	combattre le mal par le mal_____1	Default		%default
le mal partout	100	cf	[pred="voir le mal partout_____1<Suj:cln|sn>",synt_head=voir]	voir le mal partout_____1	Default		%default
le manche après la cognée	100	cf	[pred="jeter le manche après la cognée_____1<Suj:cln|sn>",synt_head=jeter]	jeter le manche après la cognée_____1	Default		%default
le masque bas	100	cf	[pred="mettre le masque bas_____1<Suj:cln|sn>",synt_head=mettre]	mettre le masque bas_____1	Default		%default
le mauvais temps	100	cf	[pred="amener le mauvais temps_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps_____1	Default		%default
le mauvais temps avec elle	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec elles	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec eux	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec lui	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec moi	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec nous	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec soi	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec toi	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le mauvais temps avec vous	100	cf	[pred="amener le mauvais temps avec lui_____1<Suj:cln|sn>",synt_head=amener]	amener le mauvais temps avec lui_____1	Default		%default
le moindre entre deux maux	100	cf	[pred="choisir le moindre entre deux maux_____1<Suj:cln|sn>",synt_head=choisir]	choisir le moindre entre deux maux_____1	Default		%default
le mors aux dents	100	cf	[pred="prendre le mors aux dents_____1<Suj:cln|sn>",synt_head=prendre]	prendre le mors aux dents_____1	Default		%default
le mort	100	cf	[pred="faire le mort_____1<Suj:cln|sn>",synt_head=faire]	faire le mort_____1	Default		%default
le mort au bridge	100	cf	[pred="faire le mort au bridge_____1<Suj:cln|sn>",synt_head=faire]	faire le mort au bridge_____1	Default		%default
le nez dehors	100	cf	[pred="mettre le nez dehors_____1<Suj:cln|sn>",synt_head=mettre]	mettre le nez dehors_____1	Default		%default
le nez à la fenêtre	100	cf	[pred="mettre le nez à la fenêtre_____1<Suj:cln|sn>",synt_head=mettre]	mettre le nez à la fenêtre_____1	Default		%default
le oeil	100	cf	[pred="ne fermer le oeil_____1<Suj:cln|sn>",clneg =c +,synt_head=fermer]	ne fermer le oeil_____1	Default		%default
le oeil	100	cf	[pred="ouvrir le oeil_____1<Suj:cln|sn>",synt_head=ouvrir]	ouvrir le oeil_____1	Default		%default
le oeil de la nuit	100	cf	[pred="ne fermer le oeil de la nuit_____1<Suj:cln|sn>",clneg =c +,synt_head=fermer]	ne fermer le oeil de la nuit_____1	Default		%default
le oeil et le bon	100	cf	[pred="ouvrir le oeil et le bon_____1<Suj:cln|sn>",synt_head=ouvrir]	ouvrir le oeil et le bon_____1	Default		%default
le pain de la bouche	100	cf	[pred="enlever le pain de la bouche_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=enlever]	enlever le pain de la bouche_____1	Default		%default
le pain de la bouche	100	cf	[pred="prendre le pain de la bouche_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre le pain de la bouche_____1	Default		%default
le pain de la bouche	100	cf	[pred="retirer le pain de la bouche_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=retirer]	retirer le pain de la bouche_____1	Default		%default
le pied	100	cf	[pred="lever le pied_____1<Suj:cln|sn>",synt_head=lever]	lever le pied_____1	Default		%default
le pied dans ce que tu penses	100	cf	[pred="mettre le pied dans ce que tu penses_____1<Suj:cln|sn>",synt_head=mettre]	mettre le pied dans ce que tu penses_____1	Default		%default
le pied dans ce que vous pensez	100	cf	[pred="mettre le pied dans ce que vous pensez_____1<Suj:cln|sn>",synt_head=mettre]	mettre le pied dans ce que vous pensez_____1	Default		%default
le pied du accélérateur	100	cf	[pred="lever le pied du accélérateur_____1<Suj:cln|sn>",synt_head=lever]	lever le pied du accélérateur_____1	Default		%default
le pied sur un sol	100	cf	[pred="mettre le pied sur un sol_____1<Suj:cln|sn>",synt_head=mettre]	mettre le pied sur un sol_____1	Default		%default
le pied sur un sol	100	cf	[pred="placer le pied sur un sol_____1<Suj:cln|sn>",synt_head=placer]	placer le pied sur un sol_____1	Default		%default
le pied sur une terre	100	cf	[pred="mettre le pied sur une terre_____1<Suj:cln|sn>",synt_head=mettre]	mettre le pied sur une terre_____1	Default		%default
le pied sur une terre	100	cf	[pred="placer le pied sur une terre_____1<Suj:cln|sn>",synt_head=placer]	placer le pied sur une terre_____1	Default		%default
le poisson d'eau	100	cf	[pred="changer le poisson d'eau_____1<Suj:cln|sn>",synt_head=changer]	changer le poisson d'eau_____1	Default		%default
le premier levé	100	cf	[pred="être le premier levé_____1<Suj:cln|sn>",synt_head=être]	être le premier levé_____1	Default		%default
le premier levé et le dernier couché	100	cf	[pred="être le premier levé et le dernier couché_____1<Suj:cln|sn>",synt_head=être]	être le premier levé et le dernier couché_____1	Default		%default
le sol	100	cf	[pred="toucher le sol_____1<Suj:cln|sn>",synt_head=toucher]	toucher le sol_____1	Default		%default
le sol de les deux épaules	100	cf	[pred="toucher le sol de les deux épaules_____1<Suj:cln|sn>",synt_head=toucher]	toucher le sol de les deux épaules_____1	Default		%default
le soleil à travers une passoire	100	cf	[pred="regarder le soleil à travers une passoire_____1<Suj:cln|sn>",synt_head=regarder]	regarder le soleil à travers une passoire_____1	Default		%default
le taureau par les cornes	100	cf	[pred="attraper le taureau par les cornes_____1<Suj:cln|sn>",synt_head=attraper]	attraper le taureau par les cornes_____1	Default		%default
le taureau par les cornes	100	cf	[pred="prendre le taureau par les cornes_____1<Suj:cln|sn>",synt_head=prendre]	prendre le taureau par les cornes_____1	Default		%default
le temps long	100	cf	[pred="trouver le temps long_____1<Suj:cln|sn>",synt_head=trouver]	trouver le temps long_____1	Default		%default
le tout pour le tout	100	cf	[pred="jouer le tout pour le tout_____1<Suj:cln|sn>",synt_head=jouer]	jouer le tout pour le tout_____1	Default		%default
le tout pour le tout	100	cf	[pred="risquer le tout pour le tout_____1<Suj:cln|sn>",synt_head=risquer]	risquer le tout pour le tout_____1	Default		%default
le train en marche	100	cf	[pred="prendre le train en marche_____1<Suj:cln|sn>",synt_head=prendre]	prendre le train en marche_____1	Default		%default
le trouble	100	cf	[pred="jeter le trouble_____1<Suj:cln|sn>",synt_head=jeter]	jeter le trouble_____1	Default		%default
le trouble dans le esprit	100	cf	[pred="jeter le trouble dans le esprit_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=jeter]	jeter le trouble dans le esprit_____1	Default		%default
le utile au agréable	100	cf	[pred="joindre le utile au agréable_____1<Suj:cln|sn>",synt_head=joindre]	joindre le utile au agréable_____1	Default		%default
le ventre	100	cf	[pred="se tenir le ventre_____1<Suj:cln|sn>",@pron,synt_head=tenir]	se tenir le ventre_____1	Default		%default
le ventre de douleur	100	cf	[pred="se tenir le ventre de douleur_____1<Suj:cln|sn>",@pron,synt_head=tenir]	se tenir le ventre de douleur_____1	Default		%default
le vrai du faux	100	cf	[pred="discerner le vrai du faux_____1<Suj:cln|sn>",synt_head=discerner]	discerner le vrai du faux_____1	Default		%default
le vrai du faux	100	cf	[pred="distinguer le vrai du faux_____1<Suj:cln|sn>",synt_head=distinguer]	distinguer le vrai du faux_____1	Default		%default
les armes bas	100	cf	[pred="mettre les armes bas_____1<Suj:cln|sn>",synt_head=mettre]	mettre les armes bas_____1	Default		%default
les bouchées doubles	100	cf	[pred="mettre les bouchées doubles_____1<Suj:cln|sn>",synt_head=mettre]	mettre les bouchées doubles_____1	Default		%default
les bras	100	cf	[pred="lever les bras_____1<Suj:cln|sn>",synt_head=lever]	lever les bras_____1	Default		%default
les bras au ciel	100	cf	[pred="jeter les bras au ciel_____1<Suj:cln|sn>",synt_head=jeter]	jeter les bras au ciel_____1	Default		%default
les bras au ciel	100	cf	[pred="lever les bras au ciel_____1<Suj:cln|sn>",synt_head=lever]	lever les bras au ciel_____1	Default		%default
les bras autour du cou	100	cf	[pred="jeter les bras autour du cou_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=jeter]	jeter les bras autour du cou_____1	Default		%default
les bras en le air	100	cf	[pred="jeter les bras en le air_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=jeter]	jeter les bras en le air_____1	Default		%default
les bras vers le ciel	100	cf	[pred="lever les bras vers le ciel_____1<Suj:cln|sn>",synt_head=lever]	lever les bras vers le ciel_____1	Default		%default
les cheveux en quatre	100	cf	[pred="couper les cheveux en quatre_____1<Suj:cln|sn>",synt_head=couper]	couper les cheveux en quatre_____1	Default		%default
les choses en grand	100	cf	[pred="faire les choses en grand_____1<Suj:cln|sn>",synt_head=faire]	faire les choses en grand_____1	Default		%default
les choses largement	100	cf	[pred="faire les choses largement_____1<Suj:cln|sn>",synt_head=faire]	faire les choses largement_____1	Default		%default
les choses à moitié	100	cf	[pred="ne faire les choses à moitié_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire les choses à moitié_____1	Default		%default
les coeurs	100	cf	[pred="sonder les coeurs_____1<Suj:cln|sn>",synt_head=sonder]	sonder les coeurs_____1	Default		%default
les coeurs et les reins	100	cf	[pred="sonder les coeurs et les reins_____1<Suj:cln|sn>",synt_head=sonder]	sonder les coeurs et les reins_____1	Default		%default
les cornes à leur mari	100	cf	[pred="donner les cornes à son mari_____1<Suj:cln|sn>",synt_head=donner]	donner les cornes à son mari_____1	Default		%default
les cornes à mon mari	100	cf	[pred="donner les cornes à son mari_____1<Suj:cln|sn>",synt_head=donner]	donner les cornes à son mari_____1	Default		%default
les cornes à notre mari	100	cf	[pred="donner les cornes à son mari_____1<Suj:cln|sn>",synt_head=donner]	donner les cornes à son mari_____1	Default		%default
les cornes à son mari	100	cf	[pred="donner les cornes à son mari_____1<Suj:cln|sn>",synt_head=donner]	donner les cornes à son mari_____1	Default		%default
les cornes à ton mari	100	cf	[pred="donner les cornes à son mari_____1<Suj:cln|sn>",synt_head=donner]	donner les cornes à son mari_____1	Default		%default
les cornes à votre mari	100	cf	[pred="donner les cornes à son mari_____1<Suj:cln|sn>",synt_head=donner]	donner les cornes à son mari_____1	Default		%default
les côtes	100	cf	[pred="se tenir les côtes_____1<Suj:cln|sn>",@pron,synt_head=tenir]	se tenir les côtes_____1	Default		%default
les côtes	100	cf	[pred="se tordre les côtes_____1<Suj:cln|sn>",@pron,synt_head=tordre]	se tordre les côtes_____1	Default		%default
les côtes de rire	100	cf	[pred="se tenir les côtes de rire_____1<Suj:cln|sn>",@pron,synt_head=tenir]	se tenir les côtes de rire_____1	Default		%default
les côtes de rire	100	cf	[pred="se tordre les côtes de rire_____1<Suj:cln|sn>",@pron,synt_head=tordre]	se tordre les côtes de rire_____1	Default		%default
les enfants du bon Dieu pour des canards sauvages	100	cf	[pred="prendre les enfants du bon Dieu pour des canards sauvages_____1<Suj:cln|sn>",synt_head=prendre]	prendre les enfants du bon Dieu pour des canards sauvages_____1	Default		%default
les mains en le air	100	cf	[pred="mettre les mains en le air_____1<Suj:cln|sn>",synt_head=mettre]	mettre les mains en le air_____1	Default		%default
les mouches à quinze pas	100	cf	[pred="tuer les mouches à quinze pas_____1<Suj:cln|sn>",synt_head=tuer]	tuer les mouches à quinze pas_____1	Default		%default
les pieds au mur	100	cf	[pred="faire les pieds au mur_____1<Suj:cln|sn>",synt_head=faire]	faire les pieds au mur_____1	Default		%default
les pieds dans le plat	100	cf	[pred="mettre les pieds dans le plat_____1<Suj:cln|sn>",synt_head=mettre]	mettre les pieds dans le plat_____1	Default		%default
les pieds devant	100	cf	[pred="partir les pieds devant_____1<Suj:cln|sn>",synt_head=partir]	partir les pieds devant_____1	Default		%default
les pissenlits par la racine	100	cf	[pred="manger les pissenlits par la racine_____1<Suj:cln|sn>",synt_head=manger]	manger les pissenlits par la racine_____1	Default		%default
les poings sur les hanches	100	cf	[pred="mettre les poings sur les hanches_____1<Suj:cln|sn>",synt_head=mettre]	mettre les poings sur les hanches_____1	Default		%default
les reins et les coeurs	100	cf	[pred="sonder les reins et les coeurs_____1<Suj:cln|sn>",synt_head=sonder]	sonder les reins et les coeurs_____1	Default		%default
les spaghettis par la racine	100	cf	[pred="manger les spaghettis par la racine_____1<Suj:cln|sn>",synt_head=manger]	manger les spaghettis par la racine_____1	Default		%default
les torchons avec les serviettes	100	cf	[pred="mélanger les torchons avec les serviettes_____1<Suj:cln|sn>",synt_head=mélanger]	mélanger les torchons avec les serviettes_____1	Default		%default
les torchons et les serviettes	100	cf	[pred="mélanger les torchons et les serviettes_____1<Suj:cln|sn>",synt_head=mélanger]	mélanger les torchons et les serviettes_____1	Default		%default
les veines avec les dents	100	cf	[pred="s' arracher les veines avec les dents_____1<Suj:cln|sn>",@pron,synt_head=arracher]	s' arracher les veines avec les dents_____1	Default		%default
les vers du nez	100	cf	[pred="tirer les vers du nez_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tirer]	tirer les vers du nez_____1	Default		%default
les yeux	100	cf	[pred="fermer les yeux_____1<Suj:cln|sn>",synt_head=fermer]	fermer les yeux_____1	Default		%default
les yeux définitivement	100	cf	[pred="fermer les yeux définitivement_____1<Suj:cln|sn>",synt_head=fermer]	fermer les yeux définitivement_____1	Default		%default
leur amour-propre au vestiaire	100	cf	[pred="laisser son amour-propre au vestiaire_____1<Suj:cln|sn>",synt_head=laisser]	laisser son amour-propre au vestiaire_____1	Default		%default
leur bonnet par dessus les moulins	100	cf	[pred="jeter son bonnet par dessus les moulins_____1<Suj:cln|sn>",synt_head=jeter]	jeter son bonnet par dessus les moulins_____1	Default		%default
leur ceinture	100	cf	[pred="serrer sa ceinture_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture_____1	Default		%default
leur ceinture de un cran	100	cf	[pred="serrer sa ceinture de un cran_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture de un cran_____1	Default		%default
leur chemin dans la vie	100	cf	[pred="faire son chemin dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire son chemin dans la vie_____1	Default		%default
leur cheval	100	cf	[pred="enlever son cheval_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval_____1	Default		%default
leur cheval au galop	100	cf	[pred="enlever son cheval au galop_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval au galop_____1	Default		%default
leur courage à deux mains	100	cf	[pred="prendre son courage à deux mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre son courage à deux mains_____1	Default		%default
leur dette	100	cf	[pred="payer sa dette_____1<Suj:cln|sn>",synt_head=payer]	payer sa dette_____1	Default		%default
leur fond de culotte sur les bancs	100	cf	[pred="user son fond de culotte sur les bancs_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=user]	user son fond de culotte sur les bancs_____1	Default		%default
leur froc aux orties	100	cf	[pred="jeter son froc aux orties_____1<Suj:cln|sn>",synt_head=jeter]	jeter son froc aux orties_____1	Default		%default
leur fusil de épaule	100	cf	[pred="changer son fusil de épaule_____1<Suj:cln|sn>",synt_head=changer]	changer son fusil de épaule_____1	Default		%default
leur langue au chat	100	cf	[pred="donner sa langue au chat_____1<Suj:cln|sn>",synt_head=donner]	donner sa langue au chat_____1	Default		%default
leur lessive à la main	100	cf	[pred="faire sa lessive à la main_____1<Suj:cln|sn>",synt_head=faire]	faire sa lessive à la main_____1	Default		%default
leur linge sale en famille	100	cf	[pred="laver son linge sale en famille_____1<Suj:cln|sn>",synt_head=laver]	laver son linge sale en famille_____1	Default		%default
leur mains dans le sang	100	cf	[pred="tremper sa mains dans le sang_____1<Suj:cln|sn>",synt_head=tremper]	tremper sa mains dans le sang_____1	Default		%default
leur mal en patience	100	cf	[pred="prendre son mal en patience_____1<Suj:cln|sn>",synt_head=prendre]	prendre son mal en patience_____1	Default		%default
leur orgueil dans leur poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
leur orgueil dans ma poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
leur orgueil dans notre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
leur orgueil dans sa poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
leur orgueil dans ta poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
leur orgueil dans votre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
leur part aux chiens	100	cf	[pred="ne donner sa part aux chiens_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner sa part aux chiens_____1	Default		%default
leur peau	100	cf	[pred="défendre sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau_____1	Default		%default
leur peau chèrement	100	cf	[pred="défendre sa peau chèrement_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau chèrement_____1	Default		%default
leur pied au cul	100	cf	[pred="foutre son pied au cul_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son pied au cul_____1	Default		%default
leur poing dans la figure	100	cf	[pred="balancer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la figure_____1	Default		%default
leur poing dans la figure	100	cf	[pred="coller son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=coller]	coller son poing dans la figure_____1	Default		%default
leur poing dans la figure	100	cf	[pred="envoyer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=envoyer]	envoyer son poing dans la figure_____1	Default		%default
leur poing dans la figure	100	cf	[pred="fiche son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche son poing dans la figure_____1	Default		%default
leur poing dans la figure	100	cf	[pred="flanquer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=flanquer]	flanquer son poing dans la figure_____1	Default		%default
leur poing dans la figure	100	cf	[pred="foutre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son poing dans la figure_____1	Default		%default
leur poing dans la figure	100	cf	[pred="mettre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son poing dans la figure_____1	Default		%default
leur poing dans la gueule	100	cf	[pred="balancer son poing dans la gueule_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la gueule_____1	Default		%default
leur poudre aux moineaux	100	cf	[pred="tirer sa poudre aux moineaux_____1<Suj:cln|sn>",synt_head=tirer]	tirer sa poudre aux moineaux_____1	Default		%default
leur queue sur la table	100	cf	[pred="mettre sa queue sur la table_____1<Suj:cln|sn>",synt_head=mettre]	mettre sa queue sur la table_____1	Default		%default
leur regard dans les yeux	100	cf	[pred="planter son regard dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son regard dans les yeux_____1	Default		%default
leur rôle	100	cf	[pred="posséder son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder son rôle_____1	Default		%default
leur salive	100	cf	[pred="user sa salive_____1<Suj:cln|sn>",synt_head=user]	user sa salive_____1	Default		%default
leur salive pour rien	100	cf	[pred="user sa salive pour rien_____1<Suj:cln|sn>",synt_head=user]	user sa salive pour rien_____1	Default		%default
leur salut dans la fuite	100	cf	[pred="chercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=chercher]	chercher son salut dans la fuite_____1	Default		%default
leur salut dans la fuite	100	cf	[pred="rechercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=rechercher]	rechercher son salut dans la fuite_____1	Default		%default
leur secret dans la tombe	100	cf	[pred="emporter son secret dans la tombe_____1<Suj:cln|sn>",synt_head=emporter]	emporter son secret dans la tombe_____1	Default		%default
leur sujet	100	cf	[pred="posséder son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder son sujet_____1	Default		%default
leur tombe	100	cf	[pred="creuser sa tombe_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe_____1	Default		%default
leur tombe avec leurs dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
leur tombe avec mes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
leur tombe avec nos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
leur tombe avec ses dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
leur tombe avec tes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
leur tombe avec vos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
leur tête entre leurs mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
leur tête entre mes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
leur tête entre nos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
leur tête entre ses mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
leur tête entre tes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
leur tête entre vos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
leur yeux dans les yeux	100	cf	[pred="planter son yeux dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son yeux dans les yeux_____1	Default		%default
leur âge	100	cf	[pred="faire son âge_____1<Suj:cln|sn>",synt_head=faire]	faire son âge_____1	Default		%default
leur âme	100	cf	[pred="vendre son âme_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme_____1	Default		%default
leur âme au diable	100	cf	[pred="donner son âme au diable_____1<Suj:cln|sn>",synt_head=donner]	donner son âme au diable_____1	Default		%default
leur âme au diable	100	cf	[pred="vendre son âme au diable_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme au diable_____1	Default		%default
leur épingle du jeu	100	cf	[pred="retirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=retirer]	retirer son épingle du jeu_____1	Default		%default
leur épingle du jeu	100	cf	[pred="tirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=tirer]	tirer son épingle du jeu_____1	Default		%default
leur épée dans les reins	100	cf	[pred="mettre son épée dans les reins_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son épée dans les reins_____1	Default		%default
leurs _NUM ans	100	cf	[pred="faire ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire ses _NUM ans_____1	Default		%default
leurs distances au égard	100	cf	[pred="prendre ses distances au égard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre ses distances au égard_____1	Default		%default
leurs désirs pour des réalités	100	cf	[pred="prendre ses désirs pour des réalités_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses désirs pour des réalités_____1	Default		%default
leurs jambes à leur cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
leurs jambes à mon cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
leurs jambes à notre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
leurs jambes à son cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
leurs jambes à ton cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
leurs jambes à votre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
leurs mains dans la merde	100	cf	[pred="mettre ses mains dans la merde_____1<Suj:cln|sn>",synt_head=mettre]	mettre ses mains dans la merde_____1	Default		%default
lourd	100	cf	[pred="peser lourd_____1<Suj:cln|sn>",synt_head=peser]	peser lourd_____1	Default		%default
lourd dans la balance	100	cf	[pred="peser lourd dans la balance_____1<Suj:cln|sn>",synt_head=peser]	peser lourd dans la balance_____1	Default		%default
ma ceinture	100	cf	[pred="serrer sa ceinture_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture_____1	Default		%default
ma ceinture de un cran	100	cf	[pred="serrer sa ceinture de un cran_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture de un cran_____1	Default		%default
ma dette	100	cf	[pred="payer sa dette_____1<Suj:cln|sn>",synt_head=payer]	payer sa dette_____1	Default		%default
ma langue au chat	100	cf	[pred="donner sa langue au chat_____1<Suj:cln|sn>",synt_head=donner]	donner sa langue au chat_____1	Default		%default
ma lessive à la main	100	cf	[pred="faire sa lessive à la main_____1<Suj:cln|sn>",synt_head=faire]	faire sa lessive à la main_____1	Default		%default
ma mains dans le sang	100	cf	[pred="tremper sa mains dans le sang_____1<Suj:cln|sn>",synt_head=tremper]	tremper sa mains dans le sang_____1	Default		%default
ma part aux chiens	100	cf	[pred="ne donner sa part aux chiens_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner sa part aux chiens_____1	Default		%default
ma peau	100	cf	[pred="défendre sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau_____1	Default		%default
ma peau chèrement	100	cf	[pred="défendre sa peau chèrement_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau chèrement_____1	Default		%default
ma poudre aux moineaux	100	cf	[pred="tirer sa poudre aux moineaux_____1<Suj:cln|sn>",synt_head=tirer]	tirer sa poudre aux moineaux_____1	Default		%default
ma queue sur la table	100	cf	[pred="mettre sa queue sur la table_____1<Suj:cln|sn>",synt_head=mettre]	mettre sa queue sur la table_____1	Default		%default
ma salive	100	cf	[pred="user sa salive_____1<Suj:cln|sn>",synt_head=user]	user sa salive_____1	Default		%default
ma salive pour rien	100	cf	[pred="user sa salive pour rien_____1<Suj:cln|sn>",synt_head=user]	user sa salive pour rien_____1	Default		%default
ma tombe	100	cf	[pred="creuser sa tombe_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe_____1	Default		%default
ma tombe avec leurs dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ma tombe avec mes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ma tombe avec nos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ma tombe avec ses dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ma tombe avec tes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ma tombe avec vos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ma tête entre leurs mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ma tête entre mes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ma tête entre nos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ma tête entre ses mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ma tête entre tes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ma tête entre vos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
machine arrière	100	cf	[pred="faire machine arrière_____1<Suj:cln|sn>",synt_head=faire]	faire machine arrière_____1	Default		%default
mal de la suite	100	cf	[pred="augurer mal de la suite_____1<Suj:cln|sn>",synt_head=augurer]	augurer mal de la suite_____1	Default		%default
maladresse	100	cf	[pred="faire maladresse_____1<Suj:cln|sn>",synt_head=faire]	faire maladresse_____1	Default		%default
maladresse après maladresse	100	cf	[pred="faire maladresse après maladresse_____1<Suj:cln|sn>",synt_head=faire]	faire maladresse après maladresse_____1	Default		%default
maladresse sur maladresse	100	cf	[pred="faire maladresse sur maladresse_____1<Suj:cln|sn>",synt_head=faire]	faire maladresse sur maladresse_____1	Default		%default
marche arrière	100	cf	[pred="faire marche arrière_____1<Suj:cln|sn>",synt_head=faire]	faire marche arrière_____1	Default		%default
martel en tête	100	cf	[pred="mettre martel en tête_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre martel en tête_____1	Default		%default
martel en tête	100	cf	[pred="se mettre martel en tête_____1<Suj:cln|sn,Objde:de-sn|en>",@pron,synt_head=mettre]	se mettre martel en tête_____1	Default		%default
maître dans le art	100	cf	[pred="passer maître dans le art_____1<Suj:cln|sn,Objde:de-sinf>",synt_head=passer]	passer maître dans le art_____1	Default		%default
mes _NUM ans	100	cf	[pred="faire ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire ses _NUM ans_____1	Default		%default
mes distances au égard	100	cf	[pred="prendre ses distances au égard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre ses distances au égard_____1	Default		%default
mes désirs pour des réalités	100	cf	[pred="prendre ses désirs pour des réalités_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses désirs pour des réalités_____1	Default		%default
mes jambes à leur cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
mes jambes à mon cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
mes jambes à notre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
mes jambes à son cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
mes jambes à ton cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
mes jambes à votre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
mes mains dans la merde	100	cf	[pred="mettre ses mains dans la merde_____1<Suj:cln|sn>",synt_head=mettre]	mettre ses mains dans la merde_____1	Default		%default
midi à leur porte	100	cf	[pred="voir midi à sa porte_____1<Suj:cln|sn>",synt_head=voir]	voir midi à sa porte_____1	Default		%default
midi à ma porte	100	cf	[pred="voir midi à sa porte_____1<Suj:cln|sn>",synt_head=voir]	voir midi à sa porte_____1	Default		%default
midi à notre porte	100	cf	[pred="voir midi à sa porte_____1<Suj:cln|sn>",synt_head=voir]	voir midi à sa porte_____1	Default		%default
midi à quatorze heures	100	cf	[pred="chercher midi à quatorze heures_____1<Suj:cln|sn>",synt_head=chercher]	chercher midi à quatorze heures_____1	Default		%default
midi à sa porte	100	cf	[pred="voir midi à sa porte_____1<Suj:cln|sn>",synt_head=voir]	voir midi à sa porte_____1	Default		%default
midi à ta porte	100	cf	[pred="voir midi à sa porte_____1<Suj:cln|sn>",synt_head=voir]	voir midi à sa porte_____1	Default		%default
midi à votre porte	100	cf	[pred="voir midi à sa porte_____1<Suj:cln|sn>",synt_head=voir]	voir midi à sa porte_____1	Default		%default
mieux cela	100	cf	[pred="aimer mieux cela_____1<Suj:cln|sn>",synt_head=aimer]	aimer mieux cela_____1	Default		%default
mieux ça	100	cf	[pred="aimer mieux ça_____1<Suj:cln|sn>",synt_head=aimer]	aimer mieux ça_____1	Default		%default
mine de rien	100	cf	[pred="ne faire mine de rien_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire mine de rien_____1	Default		%default
moche	100	cf	[pred="faire moche_____1<Suj:cln|sn>",synt_head=faire]	faire moche_____1	Default		%default
moche dans le paysage	100	cf	[pred="faire moche dans le paysage_____1<Suj:cln|sn>",synt_head=faire]	faire moche dans le paysage_____1	Default		%default
moche dans le tableau	100	cf	[pred="faire moche dans le tableau_____1<Suj:cln|sn>",synt_head=faire]	faire moche dans le tableau_____1	Default		%default
moins que les impôts	100	cf	[pred="durer moins que les impôts_____1<Suj:cln|sn>",synt_head=durer]	durer moins que les impôts_____1	Default		%default
moins que rien	100	cf	[pred="coûter moins que rien_____1<Suj:cln|sn>",synt_head=coûter]	coûter moins que rien_____1	Default		%default
moins que rien	100	cf	[pred="valoir moins que rien_____1<Suj:cln|sn>",synt_head=valoir]	valoir moins que rien_____1	Default		%default
mon amour-propre au vestiaire	100	cf	[pred="laisser son amour-propre au vestiaire_____1<Suj:cln|sn>",synt_head=laisser]	laisser son amour-propre au vestiaire_____1	Default		%default
mon bonnet par dessus les moulins	100	cf	[pred="jeter son bonnet par dessus les moulins_____1<Suj:cln|sn>",synt_head=jeter]	jeter son bonnet par dessus les moulins_____1	Default		%default
mon chemin dans la vie	100	cf	[pred="faire son chemin dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire son chemin dans la vie_____1	Default		%default
mon cheval	100	cf	[pred="enlever son cheval_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval_____1	Default		%default
mon cheval au galop	100	cf	[pred="enlever son cheval au galop_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval au galop_____1	Default		%default
mon courage à deux mains	100	cf	[pred="prendre son courage à deux mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre son courage à deux mains_____1	Default		%default
mon fond de culotte sur les bancs	100	cf	[pred="user son fond de culotte sur les bancs_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=user]	user son fond de culotte sur les bancs_____1	Default		%default
mon froc aux orties	100	cf	[pred="jeter son froc aux orties_____1<Suj:cln|sn>",synt_head=jeter]	jeter son froc aux orties_____1	Default		%default
mon fusil de épaule	100	cf	[pred="changer son fusil de épaule_____1<Suj:cln|sn>",synt_head=changer]	changer son fusil de épaule_____1	Default		%default
mon linge sale en famille	100	cf	[pred="laver son linge sale en famille_____1<Suj:cln|sn>",synt_head=laver]	laver son linge sale en famille_____1	Default		%default
mon mal en patience	100	cf	[pred="prendre son mal en patience_____1<Suj:cln|sn>",synt_head=prendre]	prendre son mal en patience_____1	Default		%default
mon orgueil dans leur poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
mon orgueil dans ma poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
mon orgueil dans notre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
mon orgueil dans sa poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
mon orgueil dans ta poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
mon orgueil dans votre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
mon pied au cul	100	cf	[pred="foutre son pied au cul_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son pied au cul_____1	Default		%default
mon poing dans la figure	100	cf	[pred="balancer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la figure_____1	Default		%default
mon poing dans la figure	100	cf	[pred="coller son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=coller]	coller son poing dans la figure_____1	Default		%default
mon poing dans la figure	100	cf	[pred="envoyer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=envoyer]	envoyer son poing dans la figure_____1	Default		%default
mon poing dans la figure	100	cf	[pred="fiche son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche son poing dans la figure_____1	Default		%default
mon poing dans la figure	100	cf	[pred="flanquer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=flanquer]	flanquer son poing dans la figure_____1	Default		%default
mon poing dans la figure	100	cf	[pred="foutre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son poing dans la figure_____1	Default		%default
mon poing dans la figure	100	cf	[pred="mettre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son poing dans la figure_____1	Default		%default
mon poing dans la gueule	100	cf	[pred="balancer son poing dans la gueule_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la gueule_____1	Default		%default
mon regard dans les yeux	100	cf	[pred="planter son regard dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son regard dans les yeux_____1	Default		%default
mon rôle	100	cf	[pred="posséder son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder son rôle_____1	Default		%default
mon salut dans la fuite	100	cf	[pred="chercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=chercher]	chercher son salut dans la fuite_____1	Default		%default
mon salut dans la fuite	100	cf	[pred="rechercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=rechercher]	rechercher son salut dans la fuite_____1	Default		%default
mon secret dans la tombe	100	cf	[pred="emporter son secret dans la tombe_____1<Suj:cln|sn>",synt_head=emporter]	emporter son secret dans la tombe_____1	Default		%default
mon sujet	100	cf	[pred="posséder son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder son sujet_____1	Default		%default
mon yeux dans les yeux	100	cf	[pred="planter son yeux dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son yeux dans les yeux_____1	Default		%default
mon âge	100	cf	[pred="faire son âge_____1<Suj:cln|sn>",synt_head=faire]	faire son âge_____1	Default		%default
mon âme	100	cf	[pred="vendre son âme_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme_____1	Default		%default
mon âme au diable	100	cf	[pred="donner son âme au diable_____1<Suj:cln|sn>",synt_head=donner]	donner son âme au diable_____1	Default		%default
mon âme au diable	100	cf	[pred="vendre son âme au diable_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme au diable_____1	Default		%default
mon épingle du jeu	100	cf	[pred="retirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=retirer]	retirer son épingle du jeu_____1	Default		%default
mon épingle du jeu	100	cf	[pred="tirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=tirer]	tirer son épingle du jeu_____1	Default		%default
mon épée dans les reins	100	cf	[pred="mettre son épée dans les reins_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son épée dans les reins_____1	Default		%default
mouche	100	cf	[pred="faire mouche_____1<Suj:cln|sn>",synt_head=faire]	faire mouche_____1	Default		%default
mouche à tout coup	100	cf	[pred="faire mouche à tout coup_____1<Suj:cln|sn>",synt_head=faire]	faire mouche à tout coup_____1	Default		%default
naufrage au port	100	cf	[pred="faire naufrage au port_____1<Suj:cln|sn>",synt_head=faire]	faire naufrage au port_____1	Default		%default
ni dieu ni diable	100	cf	[pred="ne connaître ni dieu ni diable_____1<Suj:cln|sn>",clneg =c +,synt_head=connaître]	ne connaître ni dieu ni diable_____1	Default		%default
ni dieu ni diable	100	cf	[pred="ne craindre ni dieu ni diable_____1<Suj:cln|sn>",clneg =c +,synt_head=craindre]	ne craindre ni dieu ni diable_____1	Default		%default
ni oui ni non	100	cf	[pred="ne dire ni oui ni non_____1<Suj:cln|sn>",clneg =c +,synt_head=dire]	ne dire ni oui ni non_____1	Default		%default
ni rime ni raison	100	cf	[pred="n' entendre ni rime ni raison_____1<Suj:cln|sn>",clneg =c +,synt_head=entendre]	n' entendre ni rime ni raison_____1	Default		%default
ni une ni deux	100	cf	[pred="ne faire ni une ni deux_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire ni une ni deux_____1	Default		%default
nos _NUM ans	100	cf	[pred="faire ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire ses _NUM ans_____1	Default		%default
nos distances au égard	100	cf	[pred="prendre ses distances au égard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre ses distances au égard_____1	Default		%default
nos désirs pour des réalités	100	cf	[pred="prendre ses désirs pour des réalités_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses désirs pour des réalités_____1	Default		%default
nos jambes à leur cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
nos jambes à mon cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
nos jambes à notre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
nos jambes à son cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
nos jambes à ton cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
nos jambes à votre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
nos mains dans la merde	100	cf	[pred="mettre ses mains dans la merde_____1<Suj:cln|sn>",synt_head=mettre]	mettre ses mains dans la merde_____1	Default		%default
notre amour-propre au vestiaire	100	cf	[pred="laisser son amour-propre au vestiaire_____1<Suj:cln|sn>",synt_head=laisser]	laisser son amour-propre au vestiaire_____1	Default		%default
notre bonnet par dessus les moulins	100	cf	[pred="jeter son bonnet par dessus les moulins_____1<Suj:cln|sn>",synt_head=jeter]	jeter son bonnet par dessus les moulins_____1	Default		%default
notre ceinture	100	cf	[pred="serrer sa ceinture_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture_____1	Default		%default
notre ceinture de un cran	100	cf	[pred="serrer sa ceinture de un cran_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture de un cran_____1	Default		%default
notre chemin dans la vie	100	cf	[pred="faire son chemin dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire son chemin dans la vie_____1	Default		%default
notre cheval	100	cf	[pred="enlever son cheval_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval_____1	Default		%default
notre cheval au galop	100	cf	[pred="enlever son cheval au galop_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval au galop_____1	Default		%default
notre courage à deux mains	100	cf	[pred="prendre son courage à deux mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre son courage à deux mains_____1	Default		%default
notre dette	100	cf	[pred="payer sa dette_____1<Suj:cln|sn>",synt_head=payer]	payer sa dette_____1	Default		%default
notre fond de culotte sur les bancs	100	cf	[pred="user son fond de culotte sur les bancs_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=user]	user son fond de culotte sur les bancs_____1	Default		%default
notre froc aux orties	100	cf	[pred="jeter son froc aux orties_____1<Suj:cln|sn>",synt_head=jeter]	jeter son froc aux orties_____1	Default		%default
notre fusil de épaule	100	cf	[pred="changer son fusil de épaule_____1<Suj:cln|sn>",synt_head=changer]	changer son fusil de épaule_____1	Default		%default
notre langue au chat	100	cf	[pred="donner sa langue au chat_____1<Suj:cln|sn>",synt_head=donner]	donner sa langue au chat_____1	Default		%default
notre lessive à la main	100	cf	[pred="faire sa lessive à la main_____1<Suj:cln|sn>",synt_head=faire]	faire sa lessive à la main_____1	Default		%default
notre linge sale en famille	100	cf	[pred="laver son linge sale en famille_____1<Suj:cln|sn>",synt_head=laver]	laver son linge sale en famille_____1	Default		%default
notre mains dans le sang	100	cf	[pred="tremper sa mains dans le sang_____1<Suj:cln|sn>",synt_head=tremper]	tremper sa mains dans le sang_____1	Default		%default
notre mal en patience	100	cf	[pred="prendre son mal en patience_____1<Suj:cln|sn>",synt_head=prendre]	prendre son mal en patience_____1	Default		%default
notre orgueil dans leur poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
notre orgueil dans ma poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
notre orgueil dans notre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
notre orgueil dans sa poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
notre orgueil dans ta poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
notre orgueil dans votre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
notre part aux chiens	100	cf	[pred="ne donner sa part aux chiens_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner sa part aux chiens_____1	Default		%default
notre peau	100	cf	[pred="défendre sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau_____1	Default		%default
notre peau chèrement	100	cf	[pred="défendre sa peau chèrement_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau chèrement_____1	Default		%default
notre pied au cul	100	cf	[pred="foutre son pied au cul_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son pied au cul_____1	Default		%default
notre poing dans la figure	100	cf	[pred="balancer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la figure_____1	Default		%default
notre poing dans la figure	100	cf	[pred="coller son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=coller]	coller son poing dans la figure_____1	Default		%default
notre poing dans la figure	100	cf	[pred="envoyer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=envoyer]	envoyer son poing dans la figure_____1	Default		%default
notre poing dans la figure	100	cf	[pred="fiche son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche son poing dans la figure_____1	Default		%default
notre poing dans la figure	100	cf	[pred="flanquer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=flanquer]	flanquer son poing dans la figure_____1	Default		%default
notre poing dans la figure	100	cf	[pred="foutre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son poing dans la figure_____1	Default		%default
notre poing dans la figure	100	cf	[pred="mettre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son poing dans la figure_____1	Default		%default
notre poing dans la gueule	100	cf	[pred="balancer son poing dans la gueule_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la gueule_____1	Default		%default
notre poudre aux moineaux	100	cf	[pred="tirer sa poudre aux moineaux_____1<Suj:cln|sn>",synt_head=tirer]	tirer sa poudre aux moineaux_____1	Default		%default
notre queue sur la table	100	cf	[pred="mettre sa queue sur la table_____1<Suj:cln|sn>",synt_head=mettre]	mettre sa queue sur la table_____1	Default		%default
notre regard dans les yeux	100	cf	[pred="planter son regard dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son regard dans les yeux_____1	Default		%default
notre rôle	100	cf	[pred="posséder son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder son rôle_____1	Default		%default
notre salive	100	cf	[pred="user sa salive_____1<Suj:cln|sn>",synt_head=user]	user sa salive_____1	Default		%default
notre salive pour rien	100	cf	[pred="user sa salive pour rien_____1<Suj:cln|sn>",synt_head=user]	user sa salive pour rien_____1	Default		%default
notre salut dans la fuite	100	cf	[pred="chercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=chercher]	chercher son salut dans la fuite_____1	Default		%default
notre salut dans la fuite	100	cf	[pred="rechercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=rechercher]	rechercher son salut dans la fuite_____1	Default		%default
notre secret dans la tombe	100	cf	[pred="emporter son secret dans la tombe_____1<Suj:cln|sn>",synt_head=emporter]	emporter son secret dans la tombe_____1	Default		%default
notre sujet	100	cf	[pred="posséder son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder son sujet_____1	Default		%default
notre tombe	100	cf	[pred="creuser sa tombe_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe_____1	Default		%default
notre tombe avec leurs dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
notre tombe avec mes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
notre tombe avec nos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
notre tombe avec ses dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
notre tombe avec tes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
notre tombe avec vos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
notre tête entre leurs mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
notre tête entre mes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
notre tête entre nos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
notre tête entre ses mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
notre tête entre tes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
notre tête entre vos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
notre yeux dans les yeux	100	cf	[pred="planter son yeux dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son yeux dans les yeux_____1	Default		%default
notre âge	100	cf	[pred="faire son âge_____1<Suj:cln|sn>",synt_head=faire]	faire son âge_____1	Default		%default
notre âme	100	cf	[pred="vendre son âme_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme_____1	Default		%default
notre âme au diable	100	cf	[pred="donner son âme au diable_____1<Suj:cln|sn>",synt_head=donner]	donner son âme au diable_____1	Default		%default
notre âme au diable	100	cf	[pred="vendre son âme au diable_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme au diable_____1	Default		%default
notre épingle du jeu	100	cf	[pred="retirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=retirer]	retirer son épingle du jeu_____1	Default		%default
notre épingle du jeu	100	cf	[pred="tirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=tirer]	tirer son épingle du jeu_____1	Default		%default
notre épée dans les reins	100	cf	[pred="mettre son épée dans les reins_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son épée dans les reins_____1	Default		%default
oeuvre de leurs _NUM doigts	100	cf	[pred="faire oeuvre de ses _NUM doigts_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre de ses _NUM doigts_____1	Default		%default
oeuvre de mes _NUM doigts	100	cf	[pred="faire oeuvre de ses _NUM doigts_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre de ses _NUM doigts_____1	Default		%default
oeuvre de nos _NUM doigts	100	cf	[pred="faire oeuvre de ses _NUM doigts_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre de ses _NUM doigts_____1	Default		%default
oeuvre de ses _NUM doigts	100	cf	[pred="faire oeuvre de ses _NUM doigts_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre de ses _NUM doigts_____1	Default		%default
oeuvre de tes _NUM doigts	100	cf	[pred="faire oeuvre de ses _NUM doigts_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre de ses _NUM doigts_____1	Default		%default
oeuvre de vos _NUM doigts	100	cf	[pred="faire oeuvre de ses _NUM doigts_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre de ses _NUM doigts_____1	Default		%default
oeuvre utile	100	cf	[pred="faire oeuvre utile_____1<Suj:cln|sn>",synt_head=faire]	faire oeuvre utile_____1	Default		%default
parfaitement leur rôle	100	cf	[pred="posséder parfaitement son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son rôle_____1	Default		%default
parfaitement leur sujet	100	cf	[pred="posséder parfaitement son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son sujet_____1	Default		%default
parfaitement mon rôle	100	cf	[pred="posséder parfaitement son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son rôle_____1	Default		%default
parfaitement mon sujet	100	cf	[pred="posséder parfaitement son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son sujet_____1	Default		%default
parfaitement notre rôle	100	cf	[pred="posséder parfaitement son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son rôle_____1	Default		%default
parfaitement notre sujet	100	cf	[pred="posséder parfaitement son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son sujet_____1	Default		%default
parfaitement son rôle	100	cf	[pred="posséder parfaitement son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son rôle_____1	Default		%default
parfaitement son sujet	100	cf	[pred="posséder parfaitement son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son sujet_____1	Default		%default
parfaitement ton rôle	100	cf	[pred="posséder parfaitement son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son rôle_____1	Default		%default
parfaitement ton sujet	100	cf	[pred="posséder parfaitement son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son sujet_____1	Default		%default
parfaitement votre rôle	100	cf	[pred="posséder parfaitement son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son rôle_____1	Default		%default
parfaitement votre sujet	100	cf	[pred="posséder parfaitement son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder parfaitement son sujet_____1	Default		%default
pas des coups de pied dans les chevilles	100	cf	[pred="ne se donner pas des coups de pied dans les chevilles_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne se donner pas des coups de pied dans les chevilles_____1	Default		%default
pas lourd	100	cf	[pred="ne peser pas lourd_____1<Suj:cln|sn>",clneg =c +,synt_head=peser]	ne peser pas lourd_____1	Default		%default
pas lourd en face	100	cf	[pred="ne peser pas lourd en face_____1<Suj:cln|sn,Objde:de-sn|en>",clneg =c +,synt_head=peser]	ne peser pas lourd en face_____1	Default		%default
pas pierre sur pierre	100	cf	[pred="ne laisser pas pierre sur pierre_____1<Suj:cln|sn>",clneg =c +,synt_head=laisser]	ne laisser pas pierre sur pierre_____1	Default		%default
pas trois pattes à un canard	100	cf	[pred="ne casser pas trois pattes à un canard_____1<Suj:cln|sn>",clneg =c +,synt_head=casser]	ne casser pas trois pattes à un canard_____1	Default		%default
pas un sou	100	cf	[pred="ne coûter pas un sou_____1<Suj:cln|sn>",clneg =c +,synt_head=coûter]	ne coûter pas un sou_____1	Default		%default
pas un sou	100	cf	[pred="ne valoir pas un sou_____1<Suj:cln|sn>",clneg =c +,synt_head=valoir]	ne valoir pas un sou_____1	Default		%default
pas un sou pas un centime	100	cf	[pred="ne coûter pas un sou pas un centime_____1<Suj:cln|sn>",clneg =c +,synt_head=coûter]	ne coûter pas un sou pas un centime_____1	Default		%default
pas un sou pas un centime	100	cf	[pred="ne valoir pas un sou pas un centime_____1<Suj:cln|sn>",clneg =c +,synt_head=valoir]	ne valoir pas un sou pas un centime_____1	Default		%default
peau neuve	100	cf	[pred="faire peau neuve_____1<Suj:cln|sn>",synt_head=faire]	faire peau neuve_____1	Default		%default
peine à regarder	100	cf	[pred="faire peine à regarder_____1<Suj:cln|sn>",synt_head=faire]	faire peine à regarder_____1	Default		%default
peine à voir	100	cf	[pred="faire peine à voir_____1<Suj:cln|sn>",synt_head=faire]	faire peine à voir_____1	Default		%default
pied à terre	100	cf	[pred="mettre pied à terre_____1<Suj:cln|sn>",synt_head=mettre]	mettre pied à terre_____1	Default		%default
pipi	100	cf	[pred="faire pipi_____1<Suj:cln|sn>",synt_head=faire]	faire pipi_____1	Default		%default
pipi au lit	100	cf	[pred="faire pipi au lit_____1<Suj:cln|sn>",synt_head=faire]	faire pipi au lit_____1	Default		%default
pipi dans leur culotte	100	cf	[pred="faire pipi dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire pipi dans sa culotte_____1	Default		%default
pipi dans ma culotte	100	cf	[pred="faire pipi dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire pipi dans sa culotte_____1	Default		%default
pipi dans notre culotte	100	cf	[pred="faire pipi dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire pipi dans sa culotte_____1	Default		%default
pipi dans sa culotte	100	cf	[pred="faire pipi dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire pipi dans sa culotte_____1	Default		%default
pipi dans ta culotte	100	cf	[pred="faire pipi dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire pipi dans sa culotte_____1	Default		%default
pipi dans votre culotte	100	cf	[pred="faire pipi dans sa culotte_____1<Suj:cln|sn>",synt_head=faire]	faire pipi dans sa culotte_____1	Default		%default
place nette	100	cf	[pred="faire place nette_____1<Suj:cln|sn>",synt_head=faire]	faire place nette_____1	Default		%default
place nette	100	cf	[pred="laisser place nette_____1<Suj:cln|sn>",synt_head=laisser]	laisser place nette_____1	Default		%default
plaies et bosses	100	cf	[pred="rêver plaies et bosses_____1<Suj:cln|sn>",synt_head=rêver]	rêver plaies et bosses_____1	Default		%default
plaisir	100	cf	[pred="faire plaisir_____1<Suj:cln|sn>",synt_head=faire]	faire plaisir_____1	Default		%default
plaisir à entendre	100	cf	[pred="faire plaisir à entendre_____1<Suj:cln|sn>",synt_head=faire]	faire plaisir à entendre_____1	Default		%default
plaisir à regarder	100	cf	[pred="faire plaisir à regarder_____1<Suj:cln|sn>",synt_head=faire]	faire plaisir à regarder_____1	Default		%default
plaisir à voir	100	cf	[pred="faire plaisir à voir_____1<Suj:cln|sn>",synt_head=faire]	faire plaisir à voir_____1	Default		%default
plus de bruit que de mal	100	cf	[pred="faire plus de bruit que de mal_____1<Suj:cln|sn>",synt_head=faire]	faire plus de bruit que de mal_____1	Default		%default
plus de mal que de bien	100	cf	[pred="faire plus de mal que de bien_____1<Suj:cln|sn>",synt_head=faire]	faire plus de mal que de bien_____1	Default		%default
plus de peur que de mal	100	cf	[pred="faire plus de peur que de mal_____1<Suj:cln|sn>",synt_head=faire]	faire plus de peur que de mal_____1	Default		%default
plus fort que elle	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que elles	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que eux	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que lui	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que moi	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que nous	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que soi	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que toi	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus fort que vous	100	cf	[pred="trouver plus fort que lui_____1<Suj:cln|sn>",synt_head=trouver]	trouver plus fort que lui_____1	Default		%default
plus grand que leur âge	100	cf	[pred="faire plus grand que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus grand que son âge_____1	Default		%default
plus grand que mon âge	100	cf	[pred="faire plus grand que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus grand que son âge_____1	Default		%default
plus grand que notre âge	100	cf	[pred="faire plus grand que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus grand que son âge_____1	Default		%default
plus grand que son âge	100	cf	[pred="faire plus grand que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus grand que son âge_____1	Default		%default
plus grand que ton âge	100	cf	[pred="faire plus grand que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus grand que son âge_____1	Default		%default
plus grand que votre âge	100	cf	[pred="faire plus grand que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus grand que son âge_____1	Default		%default
plus haut que le trou	100	cf	[pred="péter plus haut que le trou_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que le trou_____1	Default		%default
plus haut que leur cul	100	cf	[pred="péter plus haut que son cul_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que son cul_____1	Default		%default
plus haut que mon cul	100	cf	[pred="péter plus haut que son cul_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que son cul_____1	Default		%default
plus haut que notre cul	100	cf	[pred="péter plus haut que son cul_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que son cul_____1	Default		%default
plus haut que son cul	100	cf	[pred="péter plus haut que son cul_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que son cul_____1	Default		%default
plus haut que ton cul	100	cf	[pred="péter plus haut que son cul_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que son cul_____1	Default		%default
plus haut que votre cul	100	cf	[pred="péter plus haut que son cul_____1<Suj:cln|sn>",synt_head=péter]	péter plus haut que son cul_____1	Default		%default
plus jeune que leur âge	100	cf	[pred="faire plus jeune que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus jeune que son âge_____1	Default		%default
plus jeune que mon âge	100	cf	[pred="faire plus jeune que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus jeune que son âge_____1	Default		%default
plus jeune que notre âge	100	cf	[pred="faire plus jeune que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus jeune que son âge_____1	Default		%default
plus jeune que son âge	100	cf	[pred="faire plus jeune que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus jeune que son âge_____1	Default		%default
plus jeune que ton âge	100	cf	[pred="faire plus jeune que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus jeune que son âge_____1	Default		%default
plus jeune que votre âge	100	cf	[pred="faire plus jeune que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus jeune que son âge_____1	Default		%default
plus loin que le bout de leur nez	100	cf	[pred="ne voir plus loin que le bout de son nez_____1<Suj:cln|sn>",clneg =c +,synt_head=voir]	ne voir plus loin que le bout de son nez_____1	Default		%default
plus loin que le bout de mon nez	100	cf	[pred="ne voir plus loin que le bout de son nez_____1<Suj:cln|sn>",clneg =c +,synt_head=voir]	ne voir plus loin que le bout de son nez_____1	Default		%default
plus loin que le bout de notre nez	100	cf	[pred="ne voir plus loin que le bout de son nez_____1<Suj:cln|sn>",clneg =c +,synt_head=voir]	ne voir plus loin que le bout de son nez_____1	Default		%default
plus loin que le bout de son nez	100	cf	[pred="ne voir plus loin que le bout de son nez_____1<Suj:cln|sn>",clneg =c +,synt_head=voir]	ne voir plus loin que le bout de son nez_____1	Default		%default
plus loin que le bout de ton nez	100	cf	[pred="ne voir plus loin que le bout de son nez_____1<Suj:cln|sn>",clneg =c +,synt_head=voir]	ne voir plus loin que le bout de son nez_____1	Default		%default
plus loin que le bout de votre nez	100	cf	[pred="ne voir plus loin que le bout de son nez_____1<Suj:cln|sn>",clneg =c +,synt_head=voir]	ne voir plus loin que le bout de son nez_____1	Default		%default
plus petit que leur âge	100	cf	[pred="faire plus petit que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus petit que son âge_____1	Default		%default
plus petit que mon âge	100	cf	[pred="faire plus petit que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus petit que son âge_____1	Default		%default
plus petit que notre âge	100	cf	[pred="faire plus petit que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus petit que son âge_____1	Default		%default
plus petit que son âge	100	cf	[pred="faire plus petit que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus petit que son âge_____1	Default		%default
plus petit que ton âge	100	cf	[pred="faire plus petit que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus petit que son âge_____1	Default		%default
plus petit que votre âge	100	cf	[pred="faire plus petit que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus petit que son âge_____1	Default		%default
plus rien de la vie	100	cf	[pred="n' attendre plus rien de la vie_____1<Suj:cln|sn>",clneg =c +,synt_head=attendre]	n' attendre plus rien de la vie_____1	Default		%default
plus vieux que leur âge	100	cf	[pred="faire plus vieux que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus vieux que son âge_____1	Default		%default
plus vieux que mon âge	100	cf	[pred="faire plus vieux que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus vieux que son âge_____1	Default		%default
plus vieux que notre âge	100	cf	[pred="faire plus vieux que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus vieux que son âge_____1	Default		%default
plus vieux que son âge	100	cf	[pred="faire plus vieux que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus vieux que son âge_____1	Default		%default
plus vieux que ton âge	100	cf	[pred="faire plus vieux que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus vieux que son âge_____1	Default		%default
plus vieux que votre âge	100	cf	[pred="faire plus vieux que son âge_____1<Suj:cln|sn>",synt_head=faire]	faire plus vieux que son âge_____1	Default		%default
plus vite que la musique	100	cf	[pred="aller plus vite que la musique_____1<Suj:cln|sn>",synt_head=aller]	aller plus vite que la musique_____1	Default		%default
plus vite que les violons	100	cf	[pred="aller plus vite que les violons_____1<Suj:cln|sn>",synt_head=aller]	aller plus vite que les violons_____1	Default		%default
porte close	100	cf	[pred="trouver porte close_____1<Suj:cln|sn>",synt_head=trouver]	trouver porte close_____1	Default		%default
pour leur grade	100	cf	[pred="en prendre pour son grade_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre pour son grade_____1	Default		%default
pour mon grade	100	cf	[pred="en prendre pour son grade_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre pour son grade_____1	Default		%default
pour notre grade	100	cf	[pred="en prendre pour son grade_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre pour son grade_____1	Default		%default
pour son grade	100	cf	[pred="en prendre pour son grade_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre pour son grade_____1	Default		%default
pour ton grade	100	cf	[pred="en prendre pour son grade_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre pour son grade_____1	Default		%default
pour votre grade	100	cf	[pred="en prendre pour son grade_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre pour son grade_____1	Default		%default
profit de tout	100	cf	[pred="faire profit de tout_____1<Suj:cln|sn>",synt_head=faire]	faire profit de tout_____1	Default		%default
présent	100	cf	[pred="répondre présent_____1<Suj:cln|sn>",synt_head=répondre]	répondre présent_____1	Default		%default
présent au appel	100	cf	[pred="répondre présent au appel_____1<Suj:cln|sn>",synt_head=répondre]	répondre présent au appel_____1	Default		%default
père et mère	100	cf	[pred="assassiner père et mère_____1<Suj:cln|sn>",synt_head=assassiner]	assassiner père et mère_____1	Default		%default
père et mère	100	cf	[pred="tuer père et mère_____1<Suj:cln|sn>",synt_head=tuer]	tuer père et mère_____1	Default		%default
quelque chose	100	cf	[pred="faire quelque chose_____1<Suj:cln|sn>",synt_head=faire]	faire quelque chose_____1	Default		%default
quelque chose _DATE ?	100	cf	[pred="faire quelque chose _DATE ?_____1<Suj:cln|sn>",synt_head=faire]	faire quelque chose _DATE ?_____1	Default		%default
quelque chose ce soir ?	100	cf	[pred="faire quelque chose ce soir ?_____1<Suj:cln|sn>",synt_head=faire]	faire quelque chose ce soir ?_____1	Default		%default
quelque chose dans la vie	100	cf	[pred="faire quelque chose dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire quelque chose dans la vie_____1	Default		%default
quelque chose demain ?	100	cf	[pred="faire quelque chose demain ?_____1<Suj:cln|sn>",synt_head=faire]	faire quelque chose demain ?_____1	Default		%default
quelque chose hier ?	100	cf	[pred="faire quelque chose hier ?_____1<Suj:cln|sn>",synt_head=faire]	faire quelque chose hier ?_____1	Default		%default
quelque chose à cela	100	cf	[pred="pousser quelque chose à cela_____1<Suj:cln|sn>",synt_head=pousser]	pousser quelque chose à cela_____1	Default		%default
quoi au juste ?	100	cf	[pred="dire quoi au juste ?_____1<Suj:cln|sn>",synt_head=dire]	dire quoi au juste ?_____1	Default		%default
quoi dans cette galère ?	100	cf	[pred="faire quoi dans cette galère ?_____1<Suj:cln|sn>",synt_head=faire]	faire quoi dans cette galère ?_____1	Default		%default
quoi de beau ?	100	cf	[pred="faire quoi de beau ?_____1<Suj:cln|sn>",synt_head=faire]	faire quoi de beau ?_____1	Default		%default
quoi là ?	100	cf	[pred="faire quoi là ?_____1<Suj:cln|sn>",synt_head=faire]	faire quoi là ?_____1	Default		%default
rien	100	cf	[pred="ne comprendre rien_____1<Suj:cln|sn>",clneg =c +,synt_head=comprendre]	ne comprendre rien_____1	Default		%default
rien	100	cf	[pred="ne demander rien_____1<Suj:cln|sn>",clneg =c +,synt_head=demander]	ne demander rien_____1	Default		%default
rien de leurs _NUM doigts	100	cf	[pred="ne faire rien de ses _NUM doigts_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien de ses _NUM doigts_____1	Default		%default
rien de mes _NUM doigts	100	cf	[pred="ne faire rien de ses _NUM doigts_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien de ses _NUM doigts_____1	Default		%default
rien de nos _NUM doigts	100	cf	[pred="ne faire rien de ses _NUM doigts_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien de ses _NUM doigts_____1	Default		%default
rien de ses _NUM doigts	100	cf	[pred="ne faire rien de ses _NUM doigts_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien de ses _NUM doigts_____1	Default		%default
rien de tes _NUM doigts	100	cf	[pred="ne faire rien de ses _NUM doigts_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien de ses _NUM doigts_____1	Default		%default
rien de vos _NUM doigts	100	cf	[pred="ne faire rien de ses _NUM doigts_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien de ses _NUM doigts_____1	Default		%default
rien pour attendre	100	cf	[pred="ne perdre rien pour attendre_____1<Suj:cln|sn>",clneg =c +,synt_head=perdre]	ne perdre rien pour attendre_____1	Default		%default
rien pour rien	100	cf	[pred="ne donner rien pour rien_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner rien pour rien_____1	Default		%default
rien à la affaire	100	cf	[pred="ne faire rien à la affaire_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire rien à la affaire_____1	Default		%default
rien à personne	100	cf	[pred="ne demander rien à personne_____1<Suj:cln|sn>",clneg =c +,synt_head=demander]	ne demander rien à personne_____1	Default		%default
rien à personne	100	cf	[pred="ne devoir rien à personne_____1<Suj:cln|sn>",clneg =c +,synt_head=devoir]	ne devoir rien à personne_____1	Default		%default
rien à rien	100	cf	[pred="ne comprendre rien à rien_____1<Suj:cln|sn>",clneg =c +,synt_head=comprendre]	ne comprendre rien à rien_____1	Default		%default
rien à rien	100	cf	[pred="ne connaître rien à rien_____1<Suj:cln|sn>",clneg =c +,synt_head=connaître]	ne connaître rien à rien_____1	Default		%default
sa ceinture	100	cf	[pred="serrer sa ceinture_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture_____1	Default		%default
sa ceinture de un cran	100	cf	[pred="serrer sa ceinture de un cran_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture de un cran_____1	Default		%default
sa dette	100	cf	[pred="payer sa dette_____1<Suj:cln|sn>",synt_head=payer]	payer sa dette_____1	Default		%default
sa langue au chat	100	cf	[pred="donner sa langue au chat_____1<Suj:cln|sn>",synt_head=donner]	donner sa langue au chat_____1	Default		%default
sa lessive à la main	100	cf	[pred="faire sa lessive à la main_____1<Suj:cln|sn>",synt_head=faire]	faire sa lessive à la main_____1	Default		%default
sa mains dans le sang	100	cf	[pred="tremper sa mains dans le sang_____1<Suj:cln|sn>",synt_head=tremper]	tremper sa mains dans le sang_____1	Default		%default
sa part aux chiens	100	cf	[pred="ne donner sa part aux chiens_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner sa part aux chiens_____1	Default		%default
sa peau	100	cf	[pred="défendre sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau_____1	Default		%default
sa peau chèrement	100	cf	[pred="défendre sa peau chèrement_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau chèrement_____1	Default		%default
sa poudre aux moineaux	100	cf	[pred="tirer sa poudre aux moineaux_____1<Suj:cln|sn>",synt_head=tirer]	tirer sa poudre aux moineaux_____1	Default		%default
sa queue sur la table	100	cf	[pred="mettre sa queue sur la table_____1<Suj:cln|sn>",synt_head=mettre]	mettre sa queue sur la table_____1	Default		%default
sa salive	100	cf	[pred="user sa salive_____1<Suj:cln|sn>",synt_head=user]	user sa salive_____1	Default		%default
sa salive pour rien	100	cf	[pred="user sa salive pour rien_____1<Suj:cln|sn>",synt_head=user]	user sa salive pour rien_____1	Default		%default
sa tombe	100	cf	[pred="creuser sa tombe_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe_____1	Default		%default
sa tombe avec leurs dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
sa tombe avec mes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
sa tombe avec nos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
sa tombe avec ses dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
sa tombe avec tes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
sa tombe avec vos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
sa tête entre leurs mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
sa tête entre mes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
sa tête entre nos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
sa tête entre ses mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
sa tête entre tes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
sa tête entre vos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
sabre au clair	100	cf	[pred="mettre sabre au clair_____1<Suj:cln|sn>",synt_head=mettre]	mettre sabre au clair_____1	Default		%default
salle comble	100	cf	[pred="faire salle comble_____1<Suj:cln|sn>",synt_head=faire]	faire salle comble_____1	Default		%default
sang et eau	100	cf	[pred="suer sang et eau_____1<Suj:cln|sn>",synt_head=suer]	suer sang et eau_____1	Default		%default
semblant de rien	100	cf	[pred="ne faire semblant de rien_____1<Suj:cln|sn>",clneg =c +,synt_head=faire]	ne faire semblant de rien_____1	Default		%default
ses _NUM ans	100	cf	[pred="faire ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire ses _NUM ans_____1	Default		%default
ses distances au égard	100	cf	[pred="prendre ses distances au égard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre ses distances au égard_____1	Default		%default
ses désirs pour des réalités	100	cf	[pred="prendre ses désirs pour des réalités_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses désirs pour des réalités_____1	Default		%default
ses jambes à leur cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
ses jambes à mon cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
ses jambes à notre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
ses jambes à son cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
ses jambes à ton cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
ses jambes à votre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
ses mains dans la merde	100	cf	[pred="mettre ses mains dans la merde_____1<Suj:cln|sn>",synt_head=mettre]	mettre ses mains dans la merde_____1	Default		%default
son amour-propre au vestiaire	100	cf	[pred="laisser son amour-propre au vestiaire_____1<Suj:cln|sn>",synt_head=laisser]	laisser son amour-propre au vestiaire_____1	Default		%default
son bonnet par dessus les moulins	100	cf	[pred="jeter son bonnet par dessus les moulins_____1<Suj:cln|sn>",synt_head=jeter]	jeter son bonnet par dessus les moulins_____1	Default		%default
son chemin dans la vie	100	cf	[pred="faire son chemin dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire son chemin dans la vie_____1	Default		%default
son cheval	100	cf	[pred="enlever son cheval_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval_____1	Default		%default
son cheval au galop	100	cf	[pred="enlever son cheval au galop_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval au galop_____1	Default		%default
son courage à deux mains	100	cf	[pred="prendre son courage à deux mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre son courage à deux mains_____1	Default		%default
son fond de culotte sur les bancs	100	cf	[pred="user son fond de culotte sur les bancs_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=user]	user son fond de culotte sur les bancs_____1	Default		%default
son froc aux orties	100	cf	[pred="jeter son froc aux orties_____1<Suj:cln|sn>",synt_head=jeter]	jeter son froc aux orties_____1	Default		%default
son fusil de épaule	100	cf	[pred="changer son fusil de épaule_____1<Suj:cln|sn>",synt_head=changer]	changer son fusil de épaule_____1	Default		%default
son linge sale en famille	100	cf	[pred="laver son linge sale en famille_____1<Suj:cln|sn>",synt_head=laver]	laver son linge sale en famille_____1	Default		%default
son mal en patience	100	cf	[pred="prendre son mal en patience_____1<Suj:cln|sn>",synt_head=prendre]	prendre son mal en patience_____1	Default		%default
son orgueil dans leur poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
son orgueil dans ma poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
son orgueil dans notre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
son orgueil dans sa poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
son orgueil dans ta poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
son orgueil dans votre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
son pied au cul	100	cf	[pred="foutre son pied au cul_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son pied au cul_____1	Default		%default
son poing dans la figure	100	cf	[pred="balancer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la figure_____1	Default		%default
son poing dans la figure	100	cf	[pred="coller son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=coller]	coller son poing dans la figure_____1	Default		%default
son poing dans la figure	100	cf	[pred="envoyer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=envoyer]	envoyer son poing dans la figure_____1	Default		%default
son poing dans la figure	100	cf	[pred="fiche son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche son poing dans la figure_____1	Default		%default
son poing dans la figure	100	cf	[pred="flanquer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=flanquer]	flanquer son poing dans la figure_____1	Default		%default
son poing dans la figure	100	cf	[pred="foutre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son poing dans la figure_____1	Default		%default
son poing dans la figure	100	cf	[pred="mettre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son poing dans la figure_____1	Default		%default
son poing dans la gueule	100	cf	[pred="balancer son poing dans la gueule_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la gueule_____1	Default		%default
son regard dans les yeux	100	cf	[pred="planter son regard dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son regard dans les yeux_____1	Default		%default
son rôle	100	cf	[pred="posséder son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder son rôle_____1	Default		%default
son salut dans la fuite	100	cf	[pred="chercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=chercher]	chercher son salut dans la fuite_____1	Default		%default
son salut dans la fuite	100	cf	[pred="rechercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=rechercher]	rechercher son salut dans la fuite_____1	Default		%default
son salut dans la fuite	100	cf	[pred="trouver son salut dans la fuite_____1<Suj:cln|sn>",synt_head=trouver]	trouver son salut dans la fuite_____1	Default		%default
son secret dans la tombe	100	cf	[pred="emporter son secret dans la tombe_____1<Suj:cln|sn>",synt_head=emporter]	emporter son secret dans la tombe_____1	Default		%default
son sujet	100	cf	[pred="posséder son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder son sujet_____1	Default		%default
son yeux dans les yeux	100	cf	[pred="planter son yeux dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son yeux dans les yeux_____1	Default		%default
son âge	100	cf	[pred="faire son âge_____1<Suj:cln|sn>",synt_head=faire]	faire son âge_____1	Default		%default
son âme	100	cf	[pred="vendre son âme_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme_____1	Default		%default
son âme au diable	100	cf	[pred="donner son âme au diable_____1<Suj:cln|sn>",synt_head=donner]	donner son âme au diable_____1	Default		%default
son âme au diable	100	cf	[pred="vendre son âme au diable_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme au diable_____1	Default		%default
son épingle du jeu	100	cf	[pred="retirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=retirer]	retirer son épingle du jeu_____1	Default		%default
son épingle du jeu	100	cf	[pred="tirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=tirer]	tirer son épingle du jeu_____1	Default		%default
son épée dans les reins	100	cf	[pred="mettre son épée dans les reins_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son épée dans les reins_____1	Default		%default
sur l'humide paille des cachots	100	cf	[pred="finir sur l'humide paille des cachots_____1<Suj:cln|sn>",synt_head=finir]	finir sur l'humide paille des cachots_____1	Default		%default
sur l'humide paille des cachots	100	cf	[pred="terminer sur l'humide paille des cachots_____1<Suj:cln|sn>",synt_head=terminer]	terminer sur l'humide paille des cachots_____1	Default		%default
sur l'échafaud	100	cf	[pred="finir sur l'échafaud_____1<Suj:cln|sn>",synt_head=finir]	finir sur l'échafaud_____1	Default		%default
sur l'échafaud	100	cf	[pred="terminer sur l'échafaud_____1<Suj:cln|sn>",synt_head=terminer]	terminer sur l'échafaud_____1	Default		%default
sur le échafaud	100	cf	[pred="terminer sur le échafaud_____1<Suj:cln|sn>",synt_head=terminer]	terminer sur le échafaud_____1	Default		%default
ta ceinture	100	cf	[pred="serrer sa ceinture_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture_____1	Default		%default
ta ceinture de un cran	100	cf	[pred="serrer sa ceinture de un cran_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture de un cran_____1	Default		%default
ta dette	100	cf	[pred="payer sa dette_____1<Suj:cln|sn>",synt_head=payer]	payer sa dette_____1	Default		%default
ta langue au chat	100	cf	[pred="donner sa langue au chat_____1<Suj:cln|sn>",synt_head=donner]	donner sa langue au chat_____1	Default		%default
ta lessive à la main	100	cf	[pred="faire sa lessive à la main_____1<Suj:cln|sn>",synt_head=faire]	faire sa lessive à la main_____1	Default		%default
ta mains dans le sang	100	cf	[pred="tremper sa mains dans le sang_____1<Suj:cln|sn>",synt_head=tremper]	tremper sa mains dans le sang_____1	Default		%default
ta part aux chiens	100	cf	[pred="ne donner sa part aux chiens_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner sa part aux chiens_____1	Default		%default
ta peau	100	cf	[pred="défendre sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau_____1	Default		%default
ta peau chèrement	100	cf	[pred="défendre sa peau chèrement_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau chèrement_____1	Default		%default
ta poudre aux moineaux	100	cf	[pred="tirer sa poudre aux moineaux_____1<Suj:cln|sn>",synt_head=tirer]	tirer sa poudre aux moineaux_____1	Default		%default
ta queue sur la table	100	cf	[pred="mettre sa queue sur la table_____1<Suj:cln|sn>",synt_head=mettre]	mettre sa queue sur la table_____1	Default		%default
ta salive	100	cf	[pred="user sa salive_____1<Suj:cln|sn>",synt_head=user]	user sa salive_____1	Default		%default
ta salive pour rien	100	cf	[pred="user sa salive pour rien_____1<Suj:cln|sn>",synt_head=user]	user sa salive pour rien_____1	Default		%default
ta tombe	100	cf	[pred="creuser sa tombe_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe_____1	Default		%default
ta tombe avec leurs dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ta tombe avec mes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ta tombe avec nos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ta tombe avec ses dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ta tombe avec tes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ta tombe avec vos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
ta tête entre leurs mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ta tête entre mes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ta tête entre nos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ta tête entre ses mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ta tête entre tes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
ta tête entre vos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
table ouverte	100	cf	[pred="tenir table ouverte_____1<Suj:cln|sn>",synt_head=tenir]	tenir table ouverte_____1	Default		%default
tagada tagada	100	cf	[pred="faire tagada tagada_____1<Suj:cln|sn>",synt_head=faire]	faire tagada tagada_____1	Default		%default
tes _NUM ans	100	cf	[pred="faire ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire ses _NUM ans_____1	Default		%default
tes distances au égard	100	cf	[pred="prendre ses distances au égard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre ses distances au égard_____1	Default		%default
tes désirs pour des réalités	100	cf	[pred="prendre ses désirs pour des réalités_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses désirs pour des réalités_____1	Default		%default
tes jambes à leur cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
tes jambes à mon cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
tes jambes à notre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
tes jambes à son cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
tes jambes à ton cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
tes jambes à votre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
tes mains dans la merde	100	cf	[pred="mettre ses mains dans la merde_____1<Suj:cln|sn>",synt_head=mettre]	mettre ses mains dans la merde_____1	Default		%default
ton amour-propre au vestiaire	100	cf	[pred="laisser son amour-propre au vestiaire_____1<Suj:cln|sn>",synt_head=laisser]	laisser son amour-propre au vestiaire_____1	Default		%default
ton bonnet par dessus les moulins	100	cf	[pred="jeter son bonnet par dessus les moulins_____1<Suj:cln|sn>",synt_head=jeter]	jeter son bonnet par dessus les moulins_____1	Default		%default
ton chemin dans la vie	100	cf	[pred="faire son chemin dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire son chemin dans la vie_____1	Default		%default
ton cheval	100	cf	[pred="enlever son cheval_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval_____1	Default		%default
ton cheval au galop	100	cf	[pred="enlever son cheval au galop_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval au galop_____1	Default		%default
ton courage à deux mains	100	cf	[pred="prendre son courage à deux mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre son courage à deux mains_____1	Default		%default
ton fond de culotte sur les bancs	100	cf	[pred="user son fond de culotte sur les bancs_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=user]	user son fond de culotte sur les bancs_____1	Default		%default
ton froc aux orties	100	cf	[pred="jeter son froc aux orties_____1<Suj:cln|sn>",synt_head=jeter]	jeter son froc aux orties_____1	Default		%default
ton fusil de épaule	100	cf	[pred="changer son fusil de épaule_____1<Suj:cln|sn>",synt_head=changer]	changer son fusil de épaule_____1	Default		%default
ton linge sale en famille	100	cf	[pred="laver son linge sale en famille_____1<Suj:cln|sn>",synt_head=laver]	laver son linge sale en famille_____1	Default		%default
ton mal en patience	100	cf	[pred="prendre son mal en patience_____1<Suj:cln|sn>",synt_head=prendre]	prendre son mal en patience_____1	Default		%default
ton orgueil dans leur poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
ton orgueil dans ma poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
ton orgueil dans notre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
ton orgueil dans sa poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
ton orgueil dans ta poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
ton orgueil dans votre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
ton pied au cul	100	cf	[pred="foutre son pied au cul_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son pied au cul_____1	Default		%default
ton poing dans la figure	100	cf	[pred="balancer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la figure_____1	Default		%default
ton poing dans la figure	100	cf	[pred="coller son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=coller]	coller son poing dans la figure_____1	Default		%default
ton poing dans la figure	100	cf	[pred="envoyer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=envoyer]	envoyer son poing dans la figure_____1	Default		%default
ton poing dans la figure	100	cf	[pred="fiche son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche son poing dans la figure_____1	Default		%default
ton poing dans la figure	100	cf	[pred="flanquer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=flanquer]	flanquer son poing dans la figure_____1	Default		%default
ton poing dans la figure	100	cf	[pred="foutre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son poing dans la figure_____1	Default		%default
ton poing dans la figure	100	cf	[pred="mettre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son poing dans la figure_____1	Default		%default
ton poing dans la gueule	100	cf	[pred="balancer son poing dans la gueule_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la gueule_____1	Default		%default
ton regard dans les yeux	100	cf	[pred="planter son regard dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son regard dans les yeux_____1	Default		%default
ton rôle	100	cf	[pred="posséder son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder son rôle_____1	Default		%default
ton salut dans la fuite	100	cf	[pred="chercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=chercher]	chercher son salut dans la fuite_____1	Default		%default
ton salut dans la fuite	100	cf	[pred="rechercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=rechercher]	rechercher son salut dans la fuite_____1	Default		%default
ton secret dans la tombe	100	cf	[pred="emporter son secret dans la tombe_____1<Suj:cln|sn>",synt_head=emporter]	emporter son secret dans la tombe_____1	Default		%default
ton sujet	100	cf	[pred="posséder son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder son sujet_____1	Default		%default
ton yeux dans les yeux	100	cf	[pred="planter son yeux dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son yeux dans les yeux_____1	Default		%default
ton âge	100	cf	[pred="faire son âge_____1<Suj:cln|sn>",synt_head=faire]	faire son âge_____1	Default		%default
ton âme	100	cf	[pred="vendre son âme_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme_____1	Default		%default
ton âme au diable	100	cf	[pred="donner son âme au diable_____1<Suj:cln|sn>",synt_head=donner]	donner son âme au diable_____1	Default		%default
ton âme au diable	100	cf	[pred="vendre son âme au diable_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme au diable_____1	Default		%default
ton épingle du jeu	100	cf	[pred="retirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=retirer]	retirer son épingle du jeu_____1	Default		%default
ton épingle du jeu	100	cf	[pred="tirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=tirer]	tirer son épingle du jeu_____1	Default		%default
ton épée dans les reins	100	cf	[pred="mettre son épée dans les reins_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son épée dans les reins_____1	Default		%default
tout en noir	100	cf	[pred="voir tout en noir_____1<Suj:cln|sn>",synt_head=voir]	voir tout en noir_____1	Default		%default
tout en rose	100	cf	[pred="voir tout en rose_____1<Suj:cln|sn>",synt_head=voir]	voir tout en rose_____1	Default		%default
tout permis	100	cf	[pred="se croire tout permis_____1<Suj:cln|sn>",@pron,synt_head=croire]	se croire tout permis_____1	Default		%default
toute la nuit dans leur lit	100	cf	[pred="se retourner toute la nuit dans son lit_____1<Suj:cln|sn>",@pron,synt_head=retourner]	se retourner toute la nuit dans son lit_____1	Default		%default
toute la nuit dans mon lit	100	cf	[pred="se retourner toute la nuit dans son lit_____1<Suj:cln|sn>",@pron,synt_head=retourner]	se retourner toute la nuit dans son lit_____1	Default		%default
toute la nuit dans notre lit	100	cf	[pred="se retourner toute la nuit dans son lit_____1<Suj:cln|sn>",@pron,synt_head=retourner]	se retourner toute la nuit dans son lit_____1	Default		%default
toute la nuit dans son lit	100	cf	[pred="se retourner toute la nuit dans son lit_____1<Suj:cln|sn>",@pron,synt_head=retourner]	se retourner toute la nuit dans son lit_____1	Default		%default
toute la nuit dans ton lit	100	cf	[pred="se retourner toute la nuit dans son lit_____1<Suj:cln|sn>",@pron,synt_head=retourner]	se retourner toute la nuit dans son lit_____1	Default		%default
toute la nuit dans votre lit	100	cf	[pred="se retourner toute la nuit dans son lit_____1<Suj:cln|sn>",@pron,synt_head=retourner]	se retourner toute la nuit dans son lit_____1	Default		%default
toutes voiles dehors	100	cf	[pred="mettre toutes voiles dehors_____1<Suj:cln|sn>",synt_head=mettre]	mettre toutes voiles dehors_____1	Default		%default
tripes et boyaux	100	cf	[pred="dégueuler tripes et boyaux_____1<Suj:cln|sn>",synt_head=dégueuler]	dégueuler tripes et boyaux_____1	Default		%default
tripes et boyaux	100	cf	[pred="rendre tripes et boyaux_____1<Suj:cln|sn>",synt_head=rendre]	rendre tripes et boyaux_____1	Default		%default
tripes et boyaux	100	cf	[pred="vomir tripes et boyaux_____1<Suj:cln|sn>",synt_head=vomir]	vomir tripes et boyaux_____1	Default		%default
trois fois rien	100	cf	[pred="coûter trois fois rien_____1<Suj:cln|sn>",synt_head=coûter]	coûter trois fois rien_____1	Default		%default
trois fois rien	100	cf	[pred="valoir trois fois rien_____1<Suj:cln|sn>",synt_head=valoir]	valoir trois fois rien_____1	Default		%default
trop de leurs forces	100	cf	[pred="présumer trop de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer trop de ses forces_____1	Default		%default
trop de mes forces	100	cf	[pred="présumer trop de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer trop de ses forces_____1	Default		%default
trop de nos forces	100	cf	[pred="présumer trop de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer trop de ses forces_____1	Default		%default
trop de ses forces	100	cf	[pred="présumer trop de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer trop de ses forces_____1	Default		%default
trop de tes forces	100	cf	[pred="présumer trop de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer trop de ses forces_____1	Default		%default
trop de vos forces	100	cf	[pred="présumer trop de ses forces_____1<Suj:cln|sn>",synt_head=présumer]	présumer trop de ses forces_____1	Default		%default
trop ou pas assez	100	cf	[pred="en faire trop ou pas assez_____1<Suj:cln|sn>",@pseudo-en,synt_head=faire]	en faire trop ou pas assez_____1	Default		%default
tête baissée dans le panneau	100	cf	[pred="donner tête baissée dans le panneau_____1<Suj:cln|sn>",synt_head=donner]	donner tête baissée dans le panneau_____1	Default		%default
un	100	cf	[pred="s'en jeter un _____1<Suj:cln|sn>",@pron,@pseudo-en,synt_head=jeter]	s'en jeter un _____1	Default		%default
un baiser sur la joue	100	cf	[pred="planter un baiser sur la joue_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter un baiser sur la joue_____1	Default		%default
un baiser sur le front	100	cf	[pred="planter un baiser sur le front_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter un baiser sur le front_____1	Default		%default
un boulet	100	cf	[pred="traîner un boulet_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet_____1	Default		%default
un boulet au pied	100	cf	[pred="traîner un boulet au pied_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet au pied_____1	Default		%default
un boulet derrière elle	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière elles	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière eux	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière lui	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière moi	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière nous	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière soi	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière toi	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un boulet derrière vous	100	cf	[pred="traîner un boulet derrière lui_____1<Suj:cln|sn>",synt_head=traîner]	traîner un boulet derrière lui_____1	Default		%default
un bruit à réveiller un mort	100	cf	[pred="faire un bruit à réveiller un mort_____1<Suj:cln|sn>",synt_head=faire]	faire un bruit à réveiller un mort_____1	Default		%default
un chat un chat	100	cf	[pred="appeler un chat un chat_____1<Suj:cln|sn>",synt_head=appeler]	appeler un chat un chat_____1	Default		%default
un chaud et froid	100	cf	[pred="prendre un chaud et froid_____1<Suj:cln|sn>",synt_head=prendre]	prendre un chaud et froid_____1	Default		%default
un cheval borgne contre un aveugle	100	cf	[pred="changer un cheval borgne contre un aveugle_____1<Suj:cln|sn>",synt_head=changer]	changer un cheval borgne contre un aveugle_____1	Default		%default
un cheval borgne contre un aveugle	100	cf	[pred="échanger un cheval borgne contre un aveugle_____1<Suj:cln|sn>",synt_head=échanger]	échanger un cheval borgne contre un aveugle_____1	Default		%default
un coup	100	cf	[pred="en prendre un coup_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre un coup_____1	Default		%default
un coup	100	cf	[pred="faire un coup_____1<Suj:cln|sn>",synt_head=faire]	faire un coup_____1	Default		%default
un coup au coeur	100	cf	[pred="faire un coup au coeur_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire un coup au coeur_____1	Default		%default
un coup au coeur	100	cf	[pred="recevoir un coup au coeur_____1<Suj:cln|sn>",synt_head=recevoir]	recevoir un coup au coeur_____1	Default		%default
un coup d'épée dans la eau	100	cf	[pred="donner un coup d'épée dans la eau_____1<Suj:cln|sn>",synt_head=donner]	donner un coup d'épée dans la eau_____1	Default		%default
un coup dans les gencives	100	cf	[pred="en prendre un coup dans les gencives_____1<Suj:cln|sn>",@pseudo-en,synt_head=prendre]	en prendre un coup dans les gencives_____1	Default		%default
un coup de poing sur la table	100	cf	[pred="donner un coup de poing sur la table_____1<Suj:cln|sn>",synt_head=donner]	donner un coup de poing sur la table_____1	Default		%default
un coup sur la tête	100	cf	[pred="prendre un coup sur la tête_____1<Suj:cln|sn>",synt_head=prendre]	prendre un coup sur la tête_____1	Default		%default
un coup sur la tête	100	cf	[pred="recevoir un coup sur la tête_____1<Suj:cln|sn>",synt_head=recevoir]	recevoir un coup sur la tête_____1	Default		%default
un coup sur le ciboulot	100	cf	[pred="recevoir un coup sur le ciboulot_____1<Suj:cln|sn>",synt_head=recevoir]	recevoir un coup sur le ciboulot_____1	Default		%default
un coup sur le cigare	100	cf	[pred="recevoir un coup sur le cigare_____1<Suj:cln|sn>",synt_head=recevoir]	recevoir un coup sur le cigare_____1	Default		%default
un coup sur le citron	100	cf	[pred="recevoir un coup sur le citron_____1<Suj:cln|sn>",synt_head=recevoir]	recevoir un coup sur le citron_____1	Default		%default
un derrière la cravate	100	cf	[pred="s'en jeter un derrière la cravate_____1<Suj:cln|sn>",@pron,@pseudo-en,synt_head=jeter]	s'en jeter un derrière la cravate_____1	Default		%default
un dîner	100	cf	[pred="faire un dîner_____1<Suj:cln|sn>",synt_head=faire]	faire un dîner_____1	Default		%default
un dîner en ville	100	cf	[pred="faire un dîner en ville_____1<Suj:cln|sn>",synt_head=faire]	faire un dîner en ville_____1	Default		%default
un e au féminin	100	cf	[pred="prendre un e au féminin_____1<Suj:cln|sn>",synt_head=prendre]	prendre un e au féminin_____1	Default		%default
un enfant dans le dos	100	cf	[pred="faire un enfant dans le dos_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=faire]	faire un enfant dans le dos_____1	Default		%default
un genou en terre	100	cf	[pred="mettre un genou en terre_____1<Suj:cln|sn>",synt_head=mettre]	mettre un genou en terre_____1	Default		%default
un goût amer	100	cf	[pred="laisser un goût amer_____1<Suj:cln|sn>",synt_head=laisser]	laisser un goût amer_____1	Default		%default
un goût amer dans la bouche	100	cf	[pred="laisser un goût amer dans la bouche_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=laisser]	laisser un goût amer dans la bouche_____1	Default		%default
un hiver au balcon	100	cf	[pred="passer un hiver au balcon_____1<Suj:cln|sn>",synt_head=passer]	passer un hiver au balcon_____1	Default		%default
un noeud à leur mouchoir	100	cf	[pred="faire un noeud à son mouchoir_____1<Suj:cln|sn>",synt_head=faire]	faire un noeud à son mouchoir_____1	Default		%default
un noeud à mon mouchoir	100	cf	[pred="faire un noeud à son mouchoir_____1<Suj:cln|sn>",synt_head=faire]	faire un noeud à son mouchoir_____1	Default		%default
un noeud à notre mouchoir	100	cf	[pred="faire un noeud à son mouchoir_____1<Suj:cln|sn>",synt_head=faire]	faire un noeud à son mouchoir_____1	Default		%default
un noeud à son mouchoir	100	cf	[pred="faire un noeud à son mouchoir_____1<Suj:cln|sn>",synt_head=faire]	faire un noeud à son mouchoir_____1	Default		%default
un noeud à ton mouchoir	100	cf	[pred="faire un noeud à son mouchoir_____1<Suj:cln|sn>",synt_head=faire]	faire un noeud à son mouchoir_____1	Default		%default
un noeud à votre mouchoir	100	cf	[pred="faire un noeud à son mouchoir_____1<Suj:cln|sn>",synt_head=faire]	faire un noeud à son mouchoir_____1	Default		%default
un pas en arrière	100	cf	[pred="faire un pas en arrière_____1<Suj:cln|sn>",synt_head=faire]	faire un pas en arrière_____1	Default		%default
un pas en avant	100	cf	[pred="faire un pas en avant_____1<Suj:cln|sn>",synt_head=faire]	faire un pas en avant_____1	Default		%default
un pavé dans la mare	100	cf	[pred="lancer un pavé dans la mare_____1<Suj:cln|sn>",synt_head=lancer]	lancer un pavé dans la mare_____1	Default		%default
un pet de travers	100	cf	[pred="faire un pet de travers_____1<Suj:cln|sn>",synt_head=faire]	faire un pet de travers_____1	Default		%default
un peu là	100	cf	[pred="se poser un peu là_____1<Suj:cln|sn>",@pron,synt_head=poser]	se poser un peu là_____1	Default		%default
un quitte ou double	100	cf	[pred="faire un quitte ou double_____1<Suj:cln|sn>",synt_head=faire]	faire un quitte ou double_____1	Default		%default
un s au pluriel	100	cf	[pred="prendre un s au pluriel_____1<Suj:cln|sn>",synt_head=prendre]	prendre un s au pluriel_____1	Default		%default
un sac sur la tête	100	cf	[pred="mettre un sac sur la tête_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre un sac sur la tête_____1	Default		%default
un secret pour personne	100	cf	[pred="n' être un secret pour personne_____1<Suj:cln|sn>",clneg =c +,synt_head=être]	n' être un secret pour personne_____1	Default		%default
un trou dans la eau	100	cf	[pred="forer un trou dans la eau_____1<Suj:cln|sn>",synt_head=forer]	forer un trou dans la eau_____1	Default		%default
un éléphant dans un couloir	100	cf	[pred="rater un éléphant dans un couloir_____1<Suj:cln|sn>",synt_head=rater]	rater un éléphant dans un couloir_____1	Default		%default
un éléphant de une mouche	100	cf	[pred="faire un éléphant de une mouche_____1<Suj:cln|sn>",synt_head=faire]	faire un éléphant de une mouche_____1	Default		%default
une affaire devant un tribunal	100	cf	[pred="porter une affaire devant un tribunal_____1<Suj:cln|sn>",synt_head=porter]	porter une affaire devant un tribunal_____1	Default		%default
une aiguille dans une botte de foin	100	cf	[pred="chercher une aiguille dans une botte de foin_____1<Suj:cln|sn>",synt_head=chercher]	chercher une aiguille dans une botte de foin_____1	Default		%default
une aiguille dans une meule de foin	100	cf	[pred="chercher une aiguille dans une meule de foin_____1<Suj:cln|sn>",synt_head=chercher]	chercher une aiguille dans une meule de foin_____1	Default		%default
une claque	100	cf	[pred="prendre une claque_____1<Suj:cln|sn>",synt_head=prendre]	prendre une claque_____1	Default		%default
une claque dans la gueule	100	cf	[pred="prendre une claque dans la gueule_____1<Suj:cln|sn>",synt_head=prendre]	prendre une claque dans la gueule_____1	Default		%default
une consolation	100	cf	[pred="trouver une consolation_____1<Suj:cln|sn>",synt_head=trouver]	trouver une consolation_____1	Default		%default
une consolation dans le travail	100	cf	[pred="trouver une consolation dans le travail_____1<Suj:cln|sn>",synt_head=trouver]	trouver une consolation dans le travail_____1	Default		%default
une entorse au règlement	100	cf	[pred="faire une entorse au règlement_____1<Suj:cln|sn>",synt_head=faire]	faire une entorse au règlement_____1	Default		%default
une flamme de colère dans le regard	100	cf	[pred="allumer une flamme de colère dans le regard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une flamme de colère dans le regard_____1	Default		%default
une flamme de colère dans les yeux	100	cf	[pred="allumer une flamme de colère dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une flamme de colère dans les yeux_____1	Default		%default
une flamme de haine dans le regard	100	cf	[pred="allumer une flamme de haine dans le regard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une flamme de haine dans le regard_____1	Default		%default
une flamme de haine dans les yeux	100	cf	[pred="allumer une flamme de haine dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une flamme de haine dans les yeux_____1	Default		%default
une flamme de rage dans le regard	100	cf	[pred="allumer une flamme de rage dans le regard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une flamme de rage dans le regard_____1	Default		%default
une flamme de rage dans les yeux	100	cf	[pred="allumer une flamme de rage dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une flamme de rage dans les yeux_____1	Default		%default
une importance	100	cf	[pred="revêtir une importance_____1<Suj:cln|sn>",synt_head=revêtir]	revêtir une importance_____1	Default		%default
une importance aux yeux	100	cf	[pred="revêtir une importance aux yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=revêtir]	revêtir une importance aux yeux_____1	Default		%default
une lueur de colère dans le regard	100	cf	[pred="allumer une lueur de colère dans le regard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une lueur de colère dans le regard_____1	Default		%default
une lueur de colère dans les yeux	100	cf	[pred="allumer une lueur de colère dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une lueur de colère dans les yeux_____1	Default		%default
une lueur de haine dans le regard	100	cf	[pred="allumer une lueur de haine dans le regard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une lueur de haine dans le regard_____1	Default		%default
une lueur de haine dans les yeux	100	cf	[pred="allumer une lueur de haine dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une lueur de haine dans les yeux_____1	Default		%default
une lueur de rage dans le regard	100	cf	[pred="allumer une lueur de rage dans le regard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une lueur de rage dans le regard_____1	Default		%default
une lueur de rage dans les yeux	100	cf	[pred="allumer une lueur de rage dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=allumer]	allumer une lueur de rage dans les yeux_____1	Default		%default
une mesure pour rien	100	cf	[pred="compter une mesure pour rien_____1<Suj:cln|sn>",synt_head=compter]	compter une mesure pour rien_____1	Default		%default
une place au soleil	100	cf	[pred="se faire une place au soleil_____1<Suj:cln|sn>",@pron,synt_head=faire]	se faire une place au soleil_____1	Default		%default
une place au soleil	100	cf	[pred="se tailler une place au soleil_____1<Suj:cln|sn>",@pron,synt_head=tailler]	se tailler une place au soleil_____1	Default		%default
une porte	100	cf	[pred="fermer une porte_____1<Suj:cln|sn>",synt_head=fermer]	fermer une porte_____1	Default		%default
une porte à clef	100	cf	[pred="fermer une porte à clef_____1<Suj:cln|sn>",synt_head=fermer]	fermer une porte à clef_____1	Default		%default
une porte à clé	100	cf	[pred="fermer une porte à clé_____1<Suj:cln|sn>",synt_head=fermer]	fermer une porte à clé_____1	Default		%default
une rose de un artichaud	100	cf	[pred="faire une rose de un artichaud_____1<Suj:cln|sn>",synt_head=faire]	faire une rose de un artichaud_____1	Default		%default
une route	100	cf	[pred="fermer une route_____1<Suj:cln|sn>",synt_head=fermer]	fermer une route_____1	Default		%default
une route à la circulation	100	cf	[pred="fermer une route à la circulation_____1<Suj:cln|sn>",synt_head=fermer]	fermer une route à la circulation_____1	Default		%default
une réponse à une demande	100	cf	[pred="fournir une réponse à une demande_____1<Suj:cln|sn>",synt_head=fournir]	fournir une réponse à une demande_____1	Default		%default
une révolte	100	cf	[pred="noyer une révolte_____1<Suj:cln|sn>",synt_head=noyer]	noyer une révolte_____1	Default		%default
une révolte dans un bain de sang	100	cf	[pred="noyer une révolte dans un bain de sang_____1<Suj:cln|sn>",synt_head=noyer]	noyer une révolte dans un bain de sang_____1	Default		%default
une tape dans le dos	100	cf	[pred="donner une tape dans le dos_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=donner]	donner une tape dans le dos_____1	Default		%default
une traite sur le avenir	100	cf	[pred="tirer une traite sur le avenir_____1<Suj:cln|sn>",synt_head=tirer]	tirer une traite sur le avenir_____1	Default		%default
une vache dans un couloir	100	cf	[pred="rater une vache dans un couloir_____1<Suj:cln|sn>",synt_head=rater]	rater une vache dans un couloir_____1	Default		%default
une épine du pied	100	cf	[pred="tirer une épine du pied_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=tirer]	tirer une épine du pied_____1	Default		%default
vertu de nécessité	100	cf	[pred="faire vertu de nécessité_____1<Suj:cln|sn>",synt_head=faire]	faire vertu de nécessité_____1	Default		%default
vite en besogne	100	cf	[pred="aller vite en besogne_____1<Suj:cln|sn>",synt_head=aller]	aller vite en besogne_____1	Default		%default
vivant dans la légende	100	cf	[pred="entrer vivant dans la légende_____1<Suj:cln|sn>",synt_head=entrer]	entrer vivant dans la légende_____1	Default		%default
volte face	100	cf	[pred="faire volte face_____1<Suj:cln|sn>",synt_head=faire]	faire volte face_____1	Default		%default
vos _NUM ans	100	cf	[pred="faire ses _NUM ans_____1<Suj:cln|sn>",synt_head=faire]	faire ses _NUM ans_____1	Default		%default
vos distances au égard	100	cf	[pred="prendre ses distances au égard_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=prendre]	prendre ses distances au égard_____1	Default		%default
vos désirs pour des réalités	100	cf	[pred="prendre ses désirs pour des réalités_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses désirs pour des réalités_____1	Default		%default
vos jambes à leur cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
vos jambes à mon cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
vos jambes à notre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
vos jambes à son cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
vos jambes à ton cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
vos jambes à votre cou	100	cf	[pred="prendre ses jambes à son cou_____1<Suj:cln|sn>",synt_head=prendre]	prendre ses jambes à son cou_____1	Default		%default
vos mains dans la merde	100	cf	[pred="mettre ses mains dans la merde_____1<Suj:cln|sn>",synt_head=mettre]	mettre ses mains dans la merde_____1	Default		%default
votre amour-propre au vestiaire	100	cf	[pred="laisser son amour-propre au vestiaire_____1<Suj:cln|sn>",synt_head=laisser]	laisser son amour-propre au vestiaire_____1	Default		%default
votre bonnet par dessus les moulins	100	cf	[pred="jeter son bonnet par dessus les moulins_____1<Suj:cln|sn>",synt_head=jeter]	jeter son bonnet par dessus les moulins_____1	Default		%default
votre ceinture	100	cf	[pred="serrer sa ceinture_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture_____1	Default		%default
votre ceinture de un cran	100	cf	[pred="serrer sa ceinture de un cran_____1<Suj:cln|sn>",synt_head=serrer]	serrer sa ceinture de un cran_____1	Default		%default
votre chemin dans la vie	100	cf	[pred="faire son chemin dans la vie_____1<Suj:cln|sn>",synt_head=faire]	faire son chemin dans la vie_____1	Default		%default
votre cheval	100	cf	[pred="enlever son cheval_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval_____1	Default		%default
votre cheval au galop	100	cf	[pred="enlever son cheval au galop_____1<Suj:cln|sn>",synt_head=enlever]	enlever son cheval au galop_____1	Default		%default
votre courage à deux mains	100	cf	[pred="prendre son courage à deux mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre son courage à deux mains_____1	Default		%default
votre dette	100	cf	[pred="payer sa dette_____1<Suj:cln|sn>",synt_head=payer]	payer sa dette_____1	Default		%default
votre fond de culotte sur les bancs	100	cf	[pred="user son fond de culotte sur les bancs_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=user]	user son fond de culotte sur les bancs_____1	Default		%default
votre froc aux orties	100	cf	[pred="jeter son froc aux orties_____1<Suj:cln|sn>",synt_head=jeter]	jeter son froc aux orties_____1	Default		%default
votre fusil de épaule	100	cf	[pred="changer son fusil de épaule_____1<Suj:cln|sn>",synt_head=changer]	changer son fusil de épaule_____1	Default		%default
votre langue au chat	100	cf	[pred="donner sa langue au chat_____1<Suj:cln|sn>",synt_head=donner]	donner sa langue au chat_____1	Default		%default
votre lessive à la main	100	cf	[pred="faire sa lessive à la main_____1<Suj:cln|sn>",synt_head=faire]	faire sa lessive à la main_____1	Default		%default
votre linge sale en famille	100	cf	[pred="laver son linge sale en famille_____1<Suj:cln|sn>",synt_head=laver]	laver son linge sale en famille_____1	Default		%default
votre mains dans le sang	100	cf	[pred="tremper sa mains dans le sang_____1<Suj:cln|sn>",synt_head=tremper]	tremper sa mains dans le sang_____1	Default		%default
votre mal en patience	100	cf	[pred="prendre son mal en patience_____1<Suj:cln|sn>",synt_head=prendre]	prendre son mal en patience_____1	Default		%default
votre orgueil dans leur poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
votre orgueil dans ma poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
votre orgueil dans notre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
votre orgueil dans sa poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
votre orgueil dans ta poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
votre orgueil dans votre poche	100	cf	[pred="mettre son orgueil dans sa poche_____1<Suj:cln|sn>",synt_head=mettre]	mettre son orgueil dans sa poche_____1	Default		%default
votre part aux chiens	100	cf	[pred="ne donner sa part aux chiens_____1<Suj:cln|sn>",clneg =c +,synt_head=donner]	ne donner sa part aux chiens_____1	Default		%default
votre peau	100	cf	[pred="défendre sa peau_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau_____1	Default		%default
votre peau chèrement	100	cf	[pred="défendre sa peau chèrement_____1<Suj:cln|sn>",synt_head=défendre]	défendre sa peau chèrement_____1	Default		%default
votre pied au cul	100	cf	[pred="foutre son pied au cul_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son pied au cul_____1	Default		%default
votre poing dans la figure	100	cf	[pred="balancer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la figure_____1	Default		%default
votre poing dans la figure	100	cf	[pred="coller son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=coller]	coller son poing dans la figure_____1	Default		%default
votre poing dans la figure	100	cf	[pred="envoyer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=envoyer]	envoyer son poing dans la figure_____1	Default		%default
votre poing dans la figure	100	cf	[pred="fiche son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=fiche]	fiche son poing dans la figure_____1	Default		%default
votre poing dans la figure	100	cf	[pred="flanquer son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=flanquer]	flanquer son poing dans la figure_____1	Default		%default
votre poing dans la figure	100	cf	[pred="foutre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=foutre]	foutre son poing dans la figure_____1	Default		%default
votre poing dans la figure	100	cf	[pred="mettre son poing dans la figure_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son poing dans la figure_____1	Default		%default
votre poing dans la gueule	100	cf	[pred="balancer son poing dans la gueule_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=balancer]	balancer son poing dans la gueule_____1	Default		%default
votre poudre aux moineaux	100	cf	[pred="tirer sa poudre aux moineaux_____1<Suj:cln|sn>",synt_head=tirer]	tirer sa poudre aux moineaux_____1	Default		%default
votre queue sur la table	100	cf	[pred="mettre sa queue sur la table_____1<Suj:cln|sn>",synt_head=mettre]	mettre sa queue sur la table_____1	Default		%default
votre regard dans les yeux	100	cf	[pred="planter son regard dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son regard dans les yeux_____1	Default		%default
votre rôle	100	cf	[pred="posséder son rôle_____1<Suj:cln|sn>",synt_head=posséder]	posséder son rôle_____1	Default		%default
votre salive	100	cf	[pred="user sa salive_____1<Suj:cln|sn>",synt_head=user]	user sa salive_____1	Default		%default
votre salive pour rien	100	cf	[pred="user sa salive pour rien_____1<Suj:cln|sn>",synt_head=user]	user sa salive pour rien_____1	Default		%default
votre salut dans la fuite	100	cf	[pred="chercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=chercher]	chercher son salut dans la fuite_____1	Default		%default
votre salut dans la fuite	100	cf	[pred="rechercher son salut dans la fuite_____1<Suj:cln|sn>",synt_head=rechercher]	rechercher son salut dans la fuite_____1	Default		%default
votre secret dans la tombe	100	cf	[pred="emporter son secret dans la tombe_____1<Suj:cln|sn>",synt_head=emporter]	emporter son secret dans la tombe_____1	Default		%default
votre sujet	100	cf	[pred="posséder son sujet_____1<Suj:cln|sn>",synt_head=posséder]	posséder son sujet_____1	Default		%default
votre tombe	100	cf	[pred="creuser sa tombe_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe_____1	Default		%default
votre tombe avec leurs dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
votre tombe avec mes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
votre tombe avec nos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
votre tombe avec ses dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
votre tombe avec tes dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
votre tombe avec vos dents	100	cf	[pred="creuser sa tombe avec ses dents_____1<Suj:cln|sn>",synt_head=creuser]	creuser sa tombe avec ses dents_____1	Default		%default
votre tête entre leurs mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
votre tête entre mes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
votre tête entre nos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
votre tête entre ses mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
votre tête entre tes mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
votre tête entre vos mains	100	cf	[pred="prendre sa tête entre ses mains_____1<Suj:cln|sn>",synt_head=prendre]	prendre sa tête entre ses mains_____1	Default		%default
votre yeux dans les yeux	100	cf	[pred="planter son yeux dans les yeux_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=planter]	planter son yeux dans les yeux_____1	Default		%default
votre âge	100	cf	[pred="faire son âge_____1<Suj:cln|sn>",synt_head=faire]	faire son âge_____1	Default		%default
votre âme	100	cf	[pred="vendre son âme_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme_____1	Default		%default
votre âme au diable	100	cf	[pred="donner son âme au diable_____1<Suj:cln|sn>",synt_head=donner]	donner son âme au diable_____1	Default		%default
votre âme au diable	100	cf	[pred="vendre son âme au diable_____1<Suj:cln|sn>",synt_head=vendre]	vendre son âme au diable_____1	Default		%default
votre épingle du jeu	100	cf	[pred="retirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=retirer]	retirer son épingle du jeu_____1	Default		%default
votre épingle du jeu	100	cf	[pred="tirer son épingle du jeu_____1<Suj:cln|sn>",synt_head=tirer]	tirer son épingle du jeu_____1	Default		%default
votre épée dans les reins	100	cf	[pred="mettre son épée dans les reins_____1<Suj:cln|sn,Objde:de-sn|en>",synt_head=mettre]	mettre son épée dans les reins_____1	Default		%default
zéro franc	100	cf	[pred="coûter zéro franc_____1<Suj:cln|sn>",synt_head=coûter]	coûter zéro franc_____1	Default		%default
zéro franc	100	cf	[pred="valoir zéro franc_____1<Suj:cln|sn>",synt_head=valoir]	valoir zéro franc_____1	Default		%default
zéro franc zéro centime	100	cf	[pred="coûter zéro franc zéro centime_____1<Suj:cln|sn>",synt_head=coûter]	coûter zéro franc zéro centime_____1	Default		%default
zéro franc zéro centime	100	cf	[pred="valoir zéro franc zéro centime_____1<Suj:cln|sn>",synt_head=valoir]	valoir zéro franc zéro centime_____1	Default		%default
à l'hospice	100	cf	[pred="finir à l'hospice_____1<Suj:cln|sn>",synt_head=finir]	finir à l'hospice_____1	Default		%default
à l'hospice	100	cf	[pred="terminer à l'hospice_____1<Suj:cln|sn>",synt_head=terminer]	terminer à l'hospice_____1	Default		%default
ça d'ici	100	cf	[pred="voir ça d'ici_____1<Suj:cln|sn>",synt_head=voir]	voir ça d'ici_____1	Default		%default
ça pour rire	100	cf	[pred="dire ça pour rire_____1<Suj:cln|sn>",synt_head=dire]	dire ça pour rire_____1	Default		%default
ça tout seul	100	cf	[pred="trouver ça tout seul_____1<Suj:cln|sn>",synt_head=trouver]	trouver ça tout seul_____1	Default		%default
