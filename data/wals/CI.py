#!/usr/bin/python
# -*- coding: utf-8 -*-

# ./CI.py matrix_Wn_replaceValue | sort -k 2 -n

from __future__ import print_function          
import fileinput
import sys
import re
import csv
import os
import collections
import random

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
def distanceBetweenAFeatToAllFeat(origLang, feat, vectOfLang):
    distance = 0
    FeatOrigLang = vectOfLang[origLang][feat]
    
    for curLang in vectOfLang:
        if(curLang == origLang):
            continue
        if(vectOfLang[curLang][feat] == FeatOrigLang):
            distance += 1
    distance /= (len(vectOfLang)-1.0)
    return distance
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 2):
        sys.stderr("Error, give the matrix of vectors from the wals")
        exit(1)
    filePathMatrixWals = sys.argv[1]

    vectOfLang = dict()
    
    with open(filePathMatrixWals, "r") as matrix:
        curLine = matrix.readline().strip()
        curLine = matrix.readline().strip()
        lenVect = len(curLine.split(","))-1.0
        while(not fileCompletelyRead(curLine)):
            curLang = curLine.split(",")[0]
            curVect = curLine.split(",")
            curVect.pop(0)
            vectOfLang[curLang]=curVect
            curLine = matrix.readline().strip()
    vectOfLang = collections.OrderedDict(sorted(vectOfLang.items()))

    for curLang in vectOfLang:
        CI = 0
        for i in range(int(lenVect)):
            CI += distanceBetweenAFeatToAllFeat(curLang, i, vectOfLang)
        CI /= lenVect
        print(curLang, CI)
