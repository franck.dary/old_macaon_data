Name : Tagger with error correction
Dicts : tagger.dicts
%CLASSIFIERS
strategy strategy.cla
tagger tagger.cla
signature signature.cla
error_tagger error_tagger.cla
%STATES
strategy strategy
signature signature
tagger tagger
error_tagger.cla
%TRANSITIONS
strategy signature MOVE signature
strategy tagger MOVE tagger
tagger error_tagger *
error_tagger tagger BACK
error_tagger strategy *
signature strategy *
