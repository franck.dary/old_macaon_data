Name : Lemmatizer Machine
Dicts : lemmatizer.dicts
%CLASSIFIERS
lemma_lookup lemmatizer_lookup.cla
lemma_rules lemmatizer_rules.cla
%STATES
lemma_lookup1 lemma_lookup
lemma_rules1 lemma_rules
%TRANSITIONS
lemma_lookup1 lemma_lookup1 +1 *
lemma_lookup1 lemma_rules1 0 NOTFOUND
lemma_rules1 lemma_lookup1 +1 *
