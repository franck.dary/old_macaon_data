#!/usr/bin/python3
# -*- coding: utf-8 -*-

#

import fileinput
import sys
import re
import csv
import os
import itertools

def fileCompletelyRead(line):
    return line == ""
    
def isInASequence(line):
    return line != "\n" and line != ""
    
def lineIsAComment(line):
    return line[0].strip() == "#"
    
if __name__ == '__main__':
    
    if(len(sys.argv) != 2):
        sys.stderr("Error, give a file with a list to sort and uniq")
        exit(1)
    fileToSortPath = sys.argv[1]
    
    with open(fileToSortPath, "r", encoding="utf8") as fileToSort:
        listToSortUniq = []
        curLine = fileToSort.readline().strip()
        while(not fileCompletelyRead(curLine)):
            listToSortUniq.append(curLine)
            curLine = fileToSort.readline().strip()
    
    for x in itertools.groupby(sorted(listToSortUniq)):
        print(x[0])
    
    
