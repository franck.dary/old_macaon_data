Name : Tagger, Lemma and Parser Machine
Dicts : tagparser.dicts
%CLASSIFIERS
tagger tagger.cla
signature signature.cla
lemma_lookup lemmatizer_lookup.cla
lemma_rules lemmatizer_rules.cla
parser parser.cla
%STATES
signature1 signature
tagger1 tagger
lemma_lookup1 lemma_lookup
lemma_rules1 lemma_rules
parser1 parser
%TRANSITIONS
signature1 tagger1 0 *
tagger1 lemma_lookup1 0 *
lemma_lookup1 parser1 0 *
lemma_lookup1 lemma_rules1 0 NOTFOUND
lemma_rules1 parser1 0 *
parser1 parser1 0 LEFT
parser1 parser1 0 EOS
parser1 parser1 0 REDUCE
parser1 parser1 0 ROOT
parser1 signature1 +1 SHIFT
parser1 signature1 +1 RIGHT
