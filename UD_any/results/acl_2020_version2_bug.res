Corpus              Metric      F1.score        Model                    
--------------------------------------------------------------------------------
Chinese-GSD         LAS         49.58[±0.65]%   tokeparser_incremental   
Chinese-GSD         LAS         49.98[±1.03]%   tokeparser               
Chinese-GSD         LAS         51.83[±0.95]%   tokeparser_sequential    

Chinese-GSD         Lemmas      87.29[±0.23]%   tokeparser_incremental   
Chinese-GSD         Lemmas      87.47[±0.05]%   tokeparser_sequential    
Chinese-GSD         Lemmas      87.59[±0.19]%   tokeparser               

Chinese-GSD         Sentences   97.19[±0.63]%   tokeparser_sequential    
Chinese-GSD         Sentences   98.20[±0.30]%   tokeparser_incremental   
Chinese-GSD         Sentences   98.57[±0.32]%   tokeparser               

Chinese-GSD         UAS         53.64[±0.99]%   tokeparser_incremental   
Chinese-GSD         UAS         54.60[±1.21]%   tokeparser               
Chinese-GSD         UAS         55.97[±1.02]%   tokeparser_sequential    

Chinese-GSD         UFeats      86.10[±0.26]%   tokeparser_incremental   
Chinese-GSD         UFeats      86.16[±0.21]%   tokeparser_sequential    
Chinese-GSD         UFeats      86.25[±0.28]%   tokeparser               

Chinese-GSD         UPOS        81.36[±0.44]%   tokeparser_incremental   
Chinese-GSD         UPOS        81.43[±0.04]%   tokeparser_sequential    
Chinese-GSD         UPOS        81.58[±0.12]%   tokeparser               

Chinese-GSD         Words       87.30[±0.24]%   tokeparser_incremental   
Chinese-GSD         Words       87.47[±0.05]%   tokeparser_sequential    
Chinese-GSD         Words       87.59[±0.19]%   tokeparser               
--------------------------------------------------------------------------------
English-EWT         LAS         67.55[±0.21]%   tokeparser               
English-EWT         LAS         67.67%          tokeparser_incremental   
English-EWT         LAS         73.53[±0.21]%   tokeparser_sequential    

English-EWT         Lemmas      93.64[±0.08]%   tokeparser               
English-EWT         Lemmas      93.74%          tokeparser_incremental   
English-EWT         Lemmas      93.89[±0.06]%   tokeparser_sequential    

English-EWT         Sentences   67.30%          tokeparser_incremental   
English-EWT         Sentences   68.05[±0.28]%   tokeparser               
English-EWT         Sentences   69.86[±0.06]%   tokeparser_sequential    

English-EWT         UAS         72.31[±0.14]%   tokeparser               
English-EWT         UAS         72.60%          tokeparser_incremental   
English-EWT         UAS         77.65[±0.34]%   tokeparser_sequential    

English-EWT         UFeats      92.95[±0.25]%   tokeparser               
English-EWT         UFeats      93.15%          tokeparser_incremental   
English-EWT         UFeats      93.40[±0.01]%   tokeparser_sequential    

English-EWT         UPOS        91.26[±0.16]%   tokeparser               
English-EWT         UPOS        91.34%          tokeparser_incremental   
English-EWT         UPOS        92.17[±0.07]%   tokeparser_sequential    

English-EWT         Words       98.15[±0.07]%   tokeparser               
English-EWT         Words       98.25%          tokeparser_incremental   
English-EWT         Words       98.27[±0.05]%   tokeparser_sequential    
--------------------------------------------------------------------------------
Finnish-TDT         LAS         66.61[±0.50]%   tokeparser               
Finnish-TDT         LAS         67.20[±0.36]%   tokeparser_incremental   
Finnish-TDT         LAS         71.27[±0.37]%   tokeparser_sequential    

Finnish-TDT         Lemmas      83.89[±0.10]%   tokeparser               
Finnish-TDT         Lemmas      83.91[±0.14]%   tokeparser_incremental   
Finnish-TDT         Lemmas      83.94[±0.13]%   tokeparser_sequential    

Finnish-TDT         Sentences   86.96[±0.60]%   tokeparser_incremental   
Finnish-TDT         Sentences   87.46[±0.88]%   tokeparser               
Finnish-TDT         Sentences   87.73[±0.32]%   tokeparser_sequential    

Finnish-TDT         UAS         73.05[±0.30]%   tokeparser               
Finnish-TDT         UAS         73.19[±0.45]%   tokeparser_incremental   
Finnish-TDT         UAS         76.36[±0.41]%   tokeparser_sequential    

Finnish-TDT         UFeats      90.20[±0.12]%   tokeparser_incremental   
Finnish-TDT         UFeats      90.32[±0.16]%   tokeparser               
Finnish-TDT         UFeats      90.73[±0.16]%   tokeparser_sequential    

Finnish-TDT         UPOS        93.02[±0.16]%   tokeparser_incremental   
Finnish-TDT         UPOS        93.06[±0.07]%   tokeparser               
Finnish-TDT         UPOS        93.26[±0.11]%   tokeparser_sequential    

Finnish-TDT         Words       99.43[±0.02]%   tokeparser               
Finnish-TDT         Words       99.43[±0.03]%   tokeparser_sequential    
Finnish-TDT         Words       99.44[±0.05]%   tokeparser_incremental   
--------------------------------------------------------------------------------
French-GSD          LAS         72.80[±0.18]%   tokeparser               
French-GSD          LAS         73.27[±0.16]%   tokeparser_incremental   
French-GSD          LAS         77.23[±0.04]%   tokeparser_sequential    

French-GSD          Lemmas      92.04[±0.05]%   tokeparser               
French-GSD          Lemmas      92.12[±0.08]%   tokeparser_incremental   
French-GSD          Lemmas      92.14[±0.05]%   tokeparser_sequential    

French-GSD          Sentences   91.80[±0.82]%   tokeparser               
French-GSD          Sentences   92.72[±0.30]%   tokeparser_incremental   
French-GSD          Sentences   93.02[±0.61]%   tokeparser_sequential    

French-GSD          UAS         77.48[±0.18]%   tokeparser               
French-GSD          UAS         77.80[±0.09]%   tokeparser_incremental   
French-GSD          UAS         81.56[±0.11]%   tokeparser_sequential    

French-GSD          UFeats      89.99[±0.10]%   tokeparser_incremental   
French-GSD          UFeats      90.02[±0.11]%   tokeparser               
French-GSD          UFeats      91.32[±0.05]%   tokeparser_sequential    

French-GSD          UPOS        91.20[±0.02]%   tokeparser               
French-GSD          UPOS        91.27[±0.03]%   tokeparser_incremental   
French-GSD          UPOS        91.90[±0.07]%   tokeparser_sequential    

French-GSD          Words       95.25[±0.03]%   tokeparser               
French-GSD          Words       95.26[±0.05]%   tokeparser_sequential    
French-GSD          Words       95.28[±0.03]%   tokeparser_incremental   
--------------------------------------------------------------------------------
French-Spoken       LAS         59.06[±0.34]%   tokeparser_incremental   
French-Spoken       LAS         59.13[±0.37]%   tokeparser               
French-Spoken       LAS         61.77[±0.23]%   tokeparser_sequential    

French-Spoken       Lemmas      92.56[±0.30]%   tokeparser_incremental   
French-Spoken       Lemmas      92.80[±0.46]%   tokeparser_sequential    
French-Spoken       Lemmas      92.82[±0.02]%   tokeparser               

French-Spoken       Sentences   10.70[±1.63]%   tokeparser_incremental   
French-Spoken       Sentences   11.30[±2.83]%   tokeparser               
French-Spoken       Sentences   20.56[±0.68]%   tokeparser_sequential    

French-Spoken       UAS         64.39[±0.54]%   tokeparser               
French-Spoken       UAS         64.54[±0.54]%   tokeparser_incremental   
French-Spoken       UAS         67.20[±0.20]%   tokeparser_sequential    

French-Spoken       UFeats      98.85[±0.36]%   tokeparser_incremental   
French-Spoken       UFeats      99.20[±0.49]%   tokeparser_sequential    
French-Spoken       UFeats      99.24[±0.10]%   tokeparser               

French-Spoken       UPOS        91.24[±0.25]%   tokeparser_incremental   
French-Spoken       UPOS        91.54[±0.22]%   tokeparser               
French-Spoken       UPOS        92.40[±0.55]%   tokeparser_sequential    

French-Spoken       Words       98.85[±0.36]%   tokeparser_incremental   
French-Spoken       Words       99.20[±0.49]%   tokeparser_sequential    
French-Spoken       Words       99.24[±0.10]%   tokeparser               
--------------------------------------------------------------------------------
Hebrew-HTB          LAS         22.21[±0.57]%   tokeparser               
Hebrew-HTB          LAS         23.95[±0.28]%   tokeparser_sequential    

Hebrew-HTB          Lemmas      51.78[±0.05]%   tokeparser               
Hebrew-HTB          Lemmas      52.00[±0.06]%   tokeparser_sequential    

Hebrew-HTB          Sentences   99.08[±0.86]%   tokeparser               
Hebrew-HTB          Sentences   99.32[±0.77]%   tokeparser_sequential    

Hebrew-HTB          UAS         26.25[±0.56]%   tokeparser               
Hebrew-HTB          UAS         27.34[±0.16]%   tokeparser_sequential    

Hebrew-HTB          UFeats      49.73[±0.13]%   tokeparser               
Hebrew-HTB          UFeats      50.45[±0.10]%   tokeparser_sequential    

Hebrew-HTB          UPOS        50.98[±0.08]%   tokeparser               
Hebrew-HTB          UPOS        51.64[±0.08]%   tokeparser_sequential    

Hebrew-HTB          Words       56.56[±0.04]%   tokeparser               
Hebrew-HTB          Words       56.67[±0.03]%   tokeparser_sequential    
--------------------------------------------------------------------------------
Norwegian-Bokmaal   LAS         75.09[±0.16]%   tokeparser               

Norwegian-Bokmaal   Lemmas      96.35[±0.11]%   tokeparser               

Norwegian-Bokmaal   Sentences   93.10[±0.52]%   tokeparser               

Norwegian-Bokmaal   UAS         79.77[±0.18]%   tokeparser               

Norwegian-Bokmaal   UFeats      93.28[±0.08]%   tokeparser               

Norwegian-Bokmaal   UPOS        94.64[±0.02]%   tokeparser               

Norwegian-Bokmaal   Words       99.73[±0.01]%   tokeparser               
--------------------------------------------------------------------------------
Romanian-RRT        LAS         74.08%          tokeparser               
Romanian-RRT        LAS         74.11[±0.06]%   tokeparser_incremental   
Romanian-RRT        LAS         77.30[±0.26]%   tokeparser_sequential    

Romanian-RRT        Lemmas      95.66%          tokeparser               
Romanian-RRT        Lemmas      95.85[±0.02]%   tokeparser_incremental   
Romanian-RRT        Lemmas      96.21[±0.12]%   tokeparser_sequential    

Romanian-RRT        Sentences   93.77%          tokeparser               
Romanian-RRT        Sentences   94.42[±0.13]%   tokeparser_incremental   
Romanian-RRT        Sentences   95.51[±0.85]%   tokeparser_sequential    

Romanian-RRT        UAS         80.98[±0.02]%   tokeparser_incremental   
Romanian-RRT        UAS         81.10%          tokeparser               
Romanian-RRT        UAS         83.56[±0.25]%   tokeparser_sequential    

Romanian-RRT        UFeats      94.56%          tokeparser               
Romanian-RRT        UFeats      94.68[±0.22]%   tokeparser_incremental   
Romanian-RRT        UFeats      95.20[±0.08]%   tokeparser_sequential    

Romanian-RRT        UPOS        95.40%          tokeparser               
Romanian-RRT        UPOS        95.48[±0.19]%   tokeparser_incremental   
Romanian-RRT        UPOS        95.98[±0.13]%   tokeparser_sequential    

Romanian-RRT        Words       99.61[±0.04]%   tokeparser_incremental   
Romanian-RRT        Words       99.64%          tokeparser               
Romanian-RRT        Words       99.66[±0.03]%   tokeparser_sequential    
--------------------------------------------------------------------------------
Turkish-IMST        LAS         47.47[±0.55]%   tokeparser_incremental   
Turkish-IMST        LAS         47.69[±1.02]%   tokeparser               
Turkish-IMST        LAS         48.61[±0.44]%   tokeparser_sequential    

Turkish-IMST        Lemmas      83.20[±0.08]%   tokeparser_sequential    
Turkish-IMST        Lemmas      83.24[±0.15]%   tokeparser_incremental   
Turkish-IMST        Lemmas      83.27[±0.18]%   tokeparser               

Turkish-IMST        Sentences   95.86[±0.44]%   tokeparser_incremental   
Turkish-IMST        Sentences   96.05[±0.20]%   tokeparser               
Turkish-IMST        Sentences   97.05[±0.03]%   tokeparser_sequential    

Turkish-IMST        UAS         54.92[±0.23]%   tokeparser_incremental   
Turkish-IMST        UAS         55.13[±1.19]%   tokeparser               
Turkish-IMST        UAS         55.77[±0.38]%   tokeparser_sequential    

Turkish-IMST        UFeats      84.78[±0.14]%   tokeparser_incremental   
Turkish-IMST        UFeats      85.19[±0.04]%   tokeparser               
Turkish-IMST        UFeats      85.54[±0.05]%   tokeparser_sequential    

Turkish-IMST        UPOS        88.83[±0.23]%   tokeparser_incremental   
Turkish-IMST        UPOS        88.96[±0.01]%   tokeparser               
Turkish-IMST        UPOS        89.10[±0.07]%   tokeparser_sequential    

Turkish-IMST        Words       96.45[±0.01]%   tokeparser_sequential    
Turkish-IMST        Words       96.46[±0.01]%   tokeparser_incremental   
Turkish-IMST        Words       96.46[±0.02]%   tokeparser               
