Name : Morpho with error correction
Dicts : morpho.dicts
%CLASSIFIERS
morpho morpho.cla
error_morpho error_morpho.cla
%STATES
morpho morpho
error_morpho error_morpho
%TRANSITIONS
morpho error_morpho 0 *
error_morpho morpho 0 BACK
error_morpho morpho 1 *
