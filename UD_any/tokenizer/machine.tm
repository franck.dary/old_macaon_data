Name : Tokenizer Machine
Dicts : tokenizer.dicts
%CLASSIFIERS
strategy strategy.cla
tokenizer tokenizer.cla
%STATES
strategy strategy
tokenizer tokenizer
%TRANSITIONS
strategy tokenizer MOVE tokenizer
tokenizer strategy *
